<?php
/**
* The base configuration for WordPress
*
* The wp-config.php creation script uses this file during the
* installation. You don't have to use the web site, you can
* copy this file to "wp-config.php" and fill in the values.
*
* This file contains the following configurations:
*
* * MySQL settings
* * Secret keys
* * Database table prefix
* * ABSPATH
*
* @link https://codex.wordpress.org/Editing_wp-config.php
*
* @package WordPress
*/

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wp_mer');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost:3307');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**
* Authentication Unique Keys and Salts.
*
* Change these to different unique phrases!
* You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
* You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
*
* @since 2.6.0
*/
define('AUTH_KEY', '5NIEhqDFFgIRJBu/E9xZcjU9N2YFkJ5j/22luCll5b0h0cPwcP9JR3at1EYsTQBX');
define('SECURE_AUTH_KEY', 'w6jtVS4urMaLsgZeCHEp5nDgd2izSdOjfmcNKasVg/uKLGEeqEeVY+LWKpCuHxHH');
define('LOGGED_IN_KEY', 'eUQ3cliWYZRqGYb8N+GkwLMgSsCDKrPBo/Z2CC2Yi+lrJYirrvYzTF2v4MKnnoxV');
define('NONCE_KEY', 'rlCf7WR6oZ9P0OITDFr/DJ0G5t/XKsh0faRUsJFWNDAG7f9fYUiJzl4yxrBJecXw');
define('AUTH_SALT', 'gcvM19m/GPPakRsq/qG6dn2vsgpT0QjWm52gdQks4k4nVp0AEEPcajmMighGOfI6');
define('SECURE_AUTH_SALT', 'UUN/+lhJz51oC554nnvq9miXx0S/yCOXTWV26dswzcXmE86P48KZpFG2kh01UKC4');
define('LOGGED_IN_SALT', '1tB6sA2IxSzePQA9nvrX3uxwlpxSKJPfCQc1lTAqQ5JM3GFx207vj8u1BEFpNAPi');
define('NONCE_SALT', 'YhKl2TBC0euno3PqsrSgA0kM6QCCR9K9zL+VFZHGpjVdKF/XsK8wa6cnKvn3Lcby');

/**
* WordPress Database Table prefix.
*
* You can have multiple installations in one database if you give each
* a unique prefix. Only numbers, letters, and underscores please!
*/
$table_prefix = 'mer_';


define('WP_ALLOW_MULTISITE', true);
/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
