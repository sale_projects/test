<?php
// This file created from scratch (with code from the main Divi Rocket plugin file)
// This file was modified by Jonathan Hall and/or others; last modified 2021-01-20

defined('ABSPATH') || die();

add_filter('divi_rocket_compat_themify-wc-product-filter_isCachingEnabled', function($data) {
	return $data && empty($_GET['wpf']);
});