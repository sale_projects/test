<?php
/*
This file includes code based on and/or copied from Divi Bars,
copyright Divi Life, released under the GNU General Public License
(GPL) version 2 or later, licensed under GPL version 3 (see
license.txt file in the ../../license directory for GPL version 3 text).

This file includes code based on and/or copied from parts of WordPress
by Automattic, released under the GNU General Public License (GPL) version 2 or later,
licensed under GPL version 3 or later (see wp-license.txt in the ../../license directory
for the license, copyright, and additional credits applicable to WordPress, and the
license.txt file in the ../../license directory for GPL version 3 text).

This file modified by Jonathan Hall; last modified 2020-05-22
*/

function divi_rocket_compat_divibars_frontendInit($data) {
	
	
	//divi-bars/class.divi-bars.core.php
	if ( has_action( 'wp_footer', array( 'DiviBars_Controller', '_init' ) ) ) {
		remove_action( 'wp_footer', array( 'DiviBars_Controller', '_init' ), 21 );
		add_action( 'wp_footer', array( 'DiviRocket_DiviBars_Controller', '_init' ), 21 );
	}
	if ( has_action( 'wp_footer', array( 'DiviBars_Controller', 'showDiviBars' ) ) ) {
		remove_action( 'wp_footer', array( 'DiviBars_Controller', 'showDiviBars' ), 2 );
		add_action( 'wp_footer', array( 'DiviRocket_DiviBars_Controller', 'showDiviBars' ), 2 );
	}
	
	
	
}

// divi-bars/includes/class.divi-bars.controller.php
class DiviRocket_DiviBars_Controller extends DiviBars_Controller {
	
	private static $lastRenderingDiviBarId;
	
	/*
	public function __construct() {
		
	}
	*/
	
	
	public static function _init() {
		/*
		update_site_option( 'dib_restore_divi_static_css_file', 'off', false );
		
		$divi_styles = DiviBars::$helper->getDiviStylesManager();
		
		// Don't include DMP custom CSS file if "Output Styles Inline" is enabled
		if ( et_get_option( 'et_pb_css_in_footer', 'off' ) === 'off' && $divi_styles ) {
			
			global $wp_filesystem;
			self::$wpfs = $wp_filesystem;
			
			$custom_divi_css = '';
				
			self::$post_id = $divi_styles[0]->post_id;
			
			foreach( $divi_styles as $divi_style ) {
			
				$custom_divi_css_exists = file_exists( $divi_style->PATH );
				
				if ( $custom_divi_css_exists !== false ) {
					
					$ctx = stream_context_create(
						array(
							'http' => array(
								'timeout' => 1
							)
						)
					);
					
					$custom_divi_css .= file_get_contents( $divi_style->PATH, 0, $ctx ); 
				}
			}
			
			// Remove #page-container from Divi Cached Inline Styles tag and cloning it to prevent issues
			$custom_divi_css = str_replace( '#page-container ', '', $custom_divi_css );
			
			self::$filename = 'et-custom-divibars-' . self::$post_id;
			self::$file_extension = '.min.css';
			self::$cache_dir = ET_Core_PageResource::get_cache_directory();
			
			$files = glob( self::$cache_dir . '/' . self::$post_id . '/' . self::$filename . '-' . '[0-9]*' . self::$file_extension, GLOB_BRACE);
			
			if ( $files ) {
				
				$relative_path = '/' . self::$post_id . '/' . basename( $files[0] );
				
			} else {
				
				$relative_path = self::createResourceFile( $custom_divi_css );
			}
			
			$relative_divi_path  = ET_Core_PageResource::get_cache_directory( 'relative' );
			$relative_divi_path .= $relative_path;

			$start = strpos( $relative_divi_path, 'cache/et' );
			$first_parse = substr( $relative_divi_path, $start );
			
			$start = strpos( $relative_divi_path, 'et-cache' );
			$second_parse = substr( $relative_divi_path, $start );
			
			$url = content_url( $second_parse );
			
			printf(
				'<link id="divibars-custom-' . self::$post_id . '" rel="stylesheet" id="%1$s" href="%2$s" />', // phpcs:ignore WordPress.WP.EnqueuedResources.NonEnqueuedStylesheet
				esc_attr( self::$slug ),
				esc_url( set_url_scheme( $url ) )
			);
		}*/
	}
	
	
	public static function showDiviBars( $params ) {
	
		// wp-includes/meta.php
		add_filter( 'get_post_metadata', array('DiviRocket_DiviBars_Controller', 'divibar_pre_render'), 1, 3 );
		
		//divi-bars/includes/class.divi-bars.controller.php
		add_filter( 'et_builder_render_layout', array('DiviRocket_DiviBars_Controller', 'divibar_content') );
		
		parent::showDiviBars( $params );
		
		// wp-includes/meta.php
		remove_filter( 'get_post_metadata', array('DiviRocket_DiviBars_Controller', 'divibar_pre_render') );
		
		//divi-bars/includes/class.divi-bars.controller.php
		remove_filter( 'et_builder_render_layout', array('DiviRocket_DiviBars_Controller', 'divibar_content') );
	}
	
	public static function divibar_pre_render( $return, $divibar_id, $meta_key ) {
		// divi-bars/includes/class.divi-bars.controller.php
		if ( $meta_key == '_et_pb_divibar_effect' ) {
			self::$lastRenderingDiviBarId = $divibar_id;
		}
		return $return;
	}
	
	public static function divibar_content( $content ) {
		ob_start();
		
		$post = get_post(self::$lastRenderingDiviBarId);
		
		if ( DiviSpaceCache::isCached($post->ID, 'all') ) {
			DiviSpaceCache::renderSection($post, 'all');
		} else {
			$contentToCache = $post->post_content;
			
			include_once(DiviSpaceCache::$pluginDirectory.'includes/DiviSpacePostCacher.php');
			new DiviSpacePostCacher($post->ID, $contentToCache, DiviSpaceCache::$postsCachePath.$post->ID.'/', true, array('all'), array(
				'builderContentWrapper' => false,
				'cssInHeaderExtra' => false
			) );
		}
		
		return ob_get_clean();
	}
	
} // end DiviBars_Controller