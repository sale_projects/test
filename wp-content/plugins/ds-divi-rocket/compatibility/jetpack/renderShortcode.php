<?php
/*
This file includes code based on and/or copied from parts of Jetpack,
copyright Automattic, released under the GNU General Public License
(GPL) version 2 or later, licensed under GPL version 3 (see ../../license.txt).

This file modified by Jonathan Hall; last modified 2020-03-03
*/

defined('ABSPATH') || die();

add_filter('divi_rocket_compat_jetpack_renderShortcode', function ($data)
{
    $instance = Jetpack_Lazy_Images::instance();

    return $instance->add_image_placeholders($data);
});


