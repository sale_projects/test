<?php
/*
This file modified by Carl Wuensche and Jonathan Hall; last modified 2020-02-21
*/

defined('ABSPATH') || die();

class DiviRocketOptimizePosts
{
    private $_db_name;
    private $_prefix;
    private $_wpdb;

    public function __construct()
    {
        global $wpdb;
        $this->_wpdb = $wpdb;
        $this->_db_name = $wpdb->dbname;
        $this->_prefix = $wpdb->prefix;
    }

    public function get_post_revision_count()
    {
        return $this->_wpdb->get_var('SELECT COUNT( * ) FROM '.$this->_prefix."posts WHERE post_type='revision' ");
    }

    public function get_auto_draft_count()
    {
        return $this->_wpdb->get_var('SELECT COUNT( * ) FROM '.$this->_prefix."posts WHERE post_status='auto-draft' ");
    }

    public function get_trashed_posts_count()
    {
        return $this->_wpdb->get_var('SELECT COUNT( * ) FROM '.$this->_prefix."posts WHERE post_status='trash' ");
    }

    public function get_orphaned_post_metadata_count()
    {
        return $this->_wpdb->get_var('SELECT COUNT( * ) FROM '.$this->_prefix.'postmeta LEFT JOIN '.$this->_prefix.'posts ON '.$this->_prefix.'posts.ID='.$this->_prefix.'postmeta.post_id WHERE '.$this->_prefix.'posts.ID IS NULL');
    }

    public function clean_revision_posts()
    {
        $clean_revision_posts = $this->_wpdb->query('DELETE FROM '.$this->_prefix."posts WHERE post_type='revision' ");
        if (0 < $this->_wpdb->rows_affected/*good3*/) {
            return true;
        }

        return false;
    }

    public function clean_auto_draft_posts()
    {
        $clean_auto_draft_posts = $this->_wpdb->query('DELETE FROM '.$this->_prefix."posts WHERE post_status='auto-draft' ");
        if (0 < $this->_wpdb->rows_affected/*good3*/) {
            return true;
        }

        return false;
    }

    public function clean_trashed_posts()
    {
        $clean_trashed_posts = $this->_wpdb->query('DELETE FROM '.$this->_prefix."posts WHERE post_status='trash' ");
        if (0 < $this->_wpdb->rows_affected/*good3*/) {
            return true;
        }

        return false;
    }

    public function clean_orphaned_posts()
    {
        $orphaned_posts = $this->_wpdb->query('DELETE FROM '.$this->_prefix.'postmeta WHERE NOT EXISTS(SELECT ID FROM '.$this->_prefix.'posts WHERE '.$this->_prefix.'posts.ID='.$this->_prefix.'postmeta.post_id)');
        if (0 < $this->_wpdb->rows_affected/*good3*/) {
            return true;
        }

        return false;
    }
}
