<?php
/*
This file modified by Carl Wuensche and Jonathan Hall; last modified 2020-02-21
*/

defined('ABSPATH') || die();

class DiviRocketOptimizeUsers
{
    private $_db_name;
    private $_prefix;
    private $_wpdb;

    public function __construct()
    {
        global $wpdb;

        $this->_wpdb = $wpdb;
        $this->_db_name = $wpdb->dbname;
        $this->_prefix = $wpdb->prefix;
    }

    public function get_orphaned_user_data_count()
    {
        return $this->_wpdb->get_var('SELECT COUNT( * ) FROM '.$this->_prefix.'usermeta LEFT JOIN '.$this->_prefix.'users ON '.$this->_prefix.'users.ID='.$this->_prefix.'usermeta.user_id WHERE '.$this->_prefix.'users.ID IS NULL');
    }

    public function clean_orphaned_user_data()
    {
        $clean_orphaned_user_data = $this->_wpdb->query('DELETE FROM '.$this->_prefix.'usermeta WHERE NOT EXISTS(SELECT ID FROM '.$this->_prefix.'users WHERE '.$this->_prefix.'users.ID='.$this->_prefix.'usermeta.user_id)');
        if (0 < $this->_wpdb->rows_affected) {
            return true;
        }

        return false;
    }
}
