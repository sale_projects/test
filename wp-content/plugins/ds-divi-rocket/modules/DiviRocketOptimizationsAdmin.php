<?php
// This file modified by Jonathan Hall, Carl Wuensche, and Dominika Rauk; last modified 2020-11-18

defined('ABSPATH') || die();

function divi_rocket_admin_init()
{
	require_once DiviRocket::$pluginDirectory.'/modules/DiviRocketDBOptimization.php';
	require_once DiviRocket::$pluginDirectory.'/modules/DiviRocketOptimizePosts.php';
	require_once DiviRocket::$pluginDirectory.'/modules/DiviRocketOptimizeTrackPingBacks.php';
	require_once DiviRocket::$pluginDirectory.'/modules/DiviRocketOptimizeUsers.php';
	require_once DiviRocket::$pluginDirectory.'/modules/DiviRocketOptimizeTerms.php';
	require_once DiviRocket::$pluginDirectory.'/modules/DiviRocketOptimizeComments.php';
	require_once DiviRocket::$pluginDirectory.'/modules/DiviRocketOptimizeRelationships.php';

	// phpcs:ignore WordPress.Security.ValidatedSanitizedInput.MissingUnslash,WordPress.Security.ValidatedSanitizedInput.InputNotSanitized
	if (isset($_POST['_wpnonce']) && wp_verify_nonce(sanitize_key($_POST['_wpnonce']), 'bulk-optimizations')) {

		if (isset($_REQUEST['action']) && 'divi-rocket-process' === $_REQUEST['action'] && !empty($_REQUEST['task'])) {
			// phpcs:ignore WordPress.Security.ValidatedSanitizedInput.MissingUnslash,WordPress.Security.ValidatedSanitizedInput.InputNotSanitized
			$tasks = (is_array($_REQUEST['task'])) ? $_REQUEST['task'] : [$_REQUEST['task']];
			$tasks = array_map('sanitize_text_field', wp_unslash($tasks)); // copied from Elegant Themes code review
			$dbOptimization = new DiviRocketDBOptimization();
			$dbOptimizePosts = new DiviRocketOptimizePosts();
			$dbOptimizeTracksPings = new DiviRocketOptimizeTrackPingBacks();
			$dbOptimizeUsers = new DiviRocketOptimizeUsers();
			$dbOptimizeTerms = new DiviRocketOptimizeTerms();
			$dbOptimizeComments = new DiviRocketOptimizeComments();
			$dbOptimizeRelationships = new DiviRocketOptimizeRelationships();
			$tasks_processed = [];
			array_push($tasks_processed, ['task_action' => 'divi-rocket-process']);
			foreach ($tasks as $task) {
				switch ($task) {
						case 'optimize_db_tables':
							if ($dbOptimization->optimizeTables()) {
								array_push($tasks_processed, ['task_name' => 'optimize_db_tables', 'result' => 'success']);
							} else {
								array_push($tasks_processed, ['task_name' => 'optimize_db_tables', 'result' => 'none']);
							}

							break;
						case 'delete_revision_posts':
							if ($dbOptimizePosts->clean_revision_posts()) {
								array_push($tasks_processed, ['task_name' => 'delete_revision_posts', 'result' => 'success']);
							} else {
								array_push($tasks_processed, ['task_name' => 'delete_revision_posts', 'result' => 'none']);
							}

							break;
						case 'delete_autodraft_posts':
							if ($dbOptimizePosts->clean_auto_draft_posts()) {
								array_push($tasks_processed, ['task_name' => 'delete_autodraft_posts', 'result' => 'success']);
							} else {
								array_push($tasks_processed, ['task_name' => 'delete_autodraft_posts', 'result' => 'none']);
							}

							break;
						case 'delete_trashed_posts':
							if ($dbOptimizePosts->clean_trashed_posts()) {
								array_push($tasks_processed, ['task_name' => 'delete_trashed_posts', 'result' => 'success']);
							} else {
								array_push($tasks_processed, ['task_name' => 'delete_trashed_posts', 'result' => 'none']);
							}

							break;
						case 'delete_comments':
							if ($dbOptimizeComments->clean_spam_trash_comments()) {
								array_push($tasks_processed, ['task_name' => 'delete_comments', 'result' => 'success']);
							} else {
								array_push($tasks_processed, ['task_name' => 'delete_comments', 'result' => 'none']);
							}

							break;
						case 'delete_unapproved_comments':
							if ($dbOptimizeComments->clean_unapproved_comments()) {
								array_push($tasks_processed, ['task_name' => 'delete_unapproved_comments', 'result' => 'success']);
							} else {
								array_push($tasks_processed, ['task_name' => 'delete_unapproved_comments', 'result' => 'none']);
							}

							break;
						case 'delete_expired_transients':
							break;
						case 'delete_pingbacks_trackbacks':
							if ($dbOptimizeTracksPings->clean_pingbacks_and_trackbacks()) {
								array_push($tasks_processed, ['task_name' => 'delete_pingbacks_trackbacks', 'result' => 'success']);
							} else {
								array_push($tasks_processed, ['task_name' => 'delete_pingbacks_trackbacks', 'result' => 'none']);
							}

							break;
						case 'clean_orphaned_posts':
							if ($dbOptimizePosts->clean_orphaned_posts()) {
								array_push($tasks_processed, ['task_name' => 'clean_orphaned_posts', 'result' => 'success']);
							} else {
								array_push($tasks_processed, ['task_name' => 'clean_orphaned_posts', 'result' => 'none']);
							}

							break;
						case 'delete_comment_metadata':
							if ($dbOptimizeComments->clean_orphaned_comments()) {
								array_push($tasks_processed, ['task_name' => 'delete_comment_metadata', 'result' => 'success']);
							} else {
								array_push($tasks_processed, ['task_name' => 'delete_comment_metadata', 'result' => 'none']);
							}

							break;
						case 'delete_orphaned_relationship_data':
							if ($dbOptimizeRelationships->clean_orphaned_relationships()) {
								array_push($tasks_processed, ['task_name' => 'delete_orphaned_relationship_data', 'result' => 'success']);
							} else {
								array_push($tasks_processed, ['task_name' => 'delete_orphaned_relationship_data', 'result' => 'none']);
							}

							break;
						case 'delete_orphaned_user_data':
							if ($dbOptimizeUsers->clean_orphaned_user_data()) {
								array_push($tasks_processed, ['task_name' => 'delete_orphaned_user_data', 'result' => 'success']);
							} else {
								array_push($tasks_processed, ['task_name' => 'delete_orphaned_user_data', 'result' => 'none']);
							}

							break;
						case 'delete_orphaned_term_data':
							if ($dbOptimizeTerms->clean_orphaned_term_data()) {
								array_push($tasks_processed, ['task_name' => 'delete_orphaned_term_data', 'result' => 'success']);
							} else {
								array_push($tasks_processed, ['task_name' => 'delete_orphaned_term_data', 'result' => 'none']);
							}

							break;
					default:
							break;
					}
			}
			set_transient('divi_rocket_tasks', $tasks_processed, 10);
		}
		
	}
}

add_action('admin_init', 'divi_rocket_admin_init');

if (!class_exists('WP_List_Table')) {
    require_once ABSPATH.'wp-admin/includes/class-wp-list-table.php';
}

/*
    Tasks to optimize:
    Database Tables (include warning about InnoDB tables because prior to a version of MySQL... 5.7 I think)
*/
class DiviRocket_Optimizations extends WP_list_Table
{
    public function __construct()
    {
        parent::__construct([
            'singular' => __('Optimization', 'divi_rocket'),
            'plural' => __('Optimizations', 'divi_rocket'),
            'ajax' => false,
        ]);
    }

    public function prepare_items()
    {
        $columns = $this->get_columns();
        $hidden = $this->get_hidden_columns();
        $sortable = $this->get_sortable_columns();
        $this->process_bulk_action();
        $data = $this->table_data();

        $this->_column_headers = [$columns, $hidden, $sortable];
        $this->items = $data;
    }

    public function column_cb($item)
    {
        return sprintf(
            '<input type="checkbox" name="%1$s" value="%2$s" />',
            'task[]',
            isset($item['id']) ? $item['id'] : ''
        );
    }

    public function get_bulk_actions()
    {
        return [
            'divi-rocket-process' => __('Process', 'divi_rocket'),
        ];
    }

    /**
     * Process our bulk actions.
     *
     * @since 1.2
     */
    public function process_bulk_action()
    {
    }

    public function get_hidden_columns()
    {
        return [];
    }

    public function get_sortable_columns()
    {
        return [];
    }

    public function table_data()
    {
        global $wpdb;
        $dbOptimization = new DiviRocketDBOptimization();
        $dbOptimizePosts = new DiviRocketOptimizePosts();
        $dbOptimizeTracksPings = new DiviRocketOptimizeTrackPingBacks();
        $dbOptimizeUsers = new DiviRocketOptimizeUsers();
        $dbOptimizeTerms = new DiviRocketOptimizeTerms();
        $dbOptimizeComments = new DiviRocketOptimizeComments();
        $dbOptimizeRelationships = new DiviRocketOptimizeRelationships();
        $data = [];
        $data[] = [
            'cb' => '<input type="checkbox" name="task[]" value="optimize_db_tables" />',
            'optimization_task' => '<h4>'.esc_html__('Optimize Tables', 'divi_rocket').'</h4>
				<p>'.esc_html($dbOptimization->get_Optimizations()).'</p>',
            'id' => 'optimize_db_tables',
        ];
        $data[] = [
            'cb' => '<input type="checkbox" name="task[]" value="delete_revision_posts" />',
            'optimization_task' => '<h4>'.esc_html__('Delete Post Revision Posts', 'divi_rocket').'</h4>
				<p>'.sprintf(esc_html__('%d revision post(s) will be deleted.', 'divi_rocket'), $dbOptimizePosts->get_post_revision_count()).'</p>',
            'id' => 'delete_revision_posts',
        ];
        $data[] = [
            'cb' => '<input type="checkbox" name="task[]" value="delete_autodraft_posts" />',
            'optimization_task' => '<h4>'.esc_html__('Delete Auto-Draft Posts', 'divi_rocket').'</h4>
				<p>'.sprintf(esc_html__('%d draft post(s) will be deleted.', 'divi_rocket'), $dbOptimizePosts->get_auto_draft_count()).'</p>',
            'id' => 'delete_autodraft_posts',
        ];
        $data[] = [
            'cb' => '<input type="checkbox" name="task[]" value="delete_trashed_posts" />',
            'optimization_task' => '<h4>'.esc_html__('Delete Trashed Posts', 'divi_rocket').'</h4>
				<p>'.sprintf(esc_html__('%d trashed post(s) will be deleted.', 'divi_rocket'), $dbOptimizePosts->get_trashed_posts_count()).'</p>',
            'id' => 'delete_trashed_posts',
        ];
        $data[] = [
            'cb' => '<input type="checkbox" name="task[]" value="delete_comments" />',
            'optimization_task' => '<h4>'.esc_html__('Delete Spam/Trashed Comments', 'divi_rocket').'</h4>
				<p>'.sprintf(esc_html__('%d spam and trashed comment(s) will be deleted.', 'divi_rocket'), $dbOptimizeComments->get_spam_trash_comment_count()).'</p>',
            'id' => 'delete_comments',
        ];
        $data[] = [
            'cb' => '<input type="checkbox" name="task[]" value="delete_unapproved_comments" />',
            'optimization_task' => '<h4>'.esc_html__('Delete Unapproved Comments', 'divi_rocket').'</h4>
				<p>'.sprintf(esc_html__('%d unapproved comment(s) will be deleted.', 'divi_rocket'), $dbOptimizeComments->get_unapproved_comment_count()).'</p>',
            'id' => 'delete_unapproved_comments',
        ];
        /*$data[] = array(
            'cb' => '<input type="checkbox" name="task[]" value="delete_expired_transients" />',
            'optimization_task' =>
                '<h4>'.esc_html__('Delete Expired Transients', 'divi_rocket' ).'</h4>',
                //<p>'.sprintf( esc_html__('%d unapproved comment(s) will be deleted.', 'divi_rocket' ), $dbOptimizeComments->get_unapproved_comment_count()).'</p>',

            'id' => 'delete_expired_transients'
        );*/
        $data[] = [
            'cb' => '<input type="checkbox" name="task[]" value="delete_pingbacks_trackbacks" />',
            'optimization_task' => '<h4>'.esc_html__('Delete Pingbacks and Trackbacks', 'divi_rocket').'</h4>
				<p>'.sprintf(esc_html__('%d pingback(s) and %d trackback(s) will be deleted.', 'divi_rocket'), $dbOptimizeTracksPings->get_pingback_count(), $dbOptimizeTracksPings->get_trackback_count()).'</p>',
            'id' => 'delete_pingbacks_trackbacks',
        ];
        $data[] = [
            'cb' => '<input type="checkbox" name="task[]" value="clean_orphaned_posts" />',
            'optimization_task' => '<h4>'.esc_html__('Delete Orphaned Post Metadata', 'divi_rocket').'</h4>
				<p>'.sprintf(esc_html__('%d orphaned post metadata entries will be deleted.', 'divi_rocket'), $dbOptimizePosts->get_orphaned_post_metadata_count()).'</p>',
            'id' => 'clean_orphaned_posts',
        ];
        $data[] = [
            'cb' => '<input type="checkbox" name="task[]" value="delete_comment_metadata" />',
            'optimization_task' => '<h4>'.esc_html__('Delete Orphaned Comment Metadata', 'divi_rocket').'</h4>
				<p>'.sprintf(esc_html__('%d orphaned comment metadata entries will be deleted.', 'divi_rocket'), $dbOptimizeComments->get_orphaned_comment_count()).'</p>',
            'id' => 'delete_comment_metadata',
        ];
        $data[] = [
            'cb' => '<input type="checkbox" name="task[]" value="delete_orphaned_relationship_data" />',
            'optimization_task' => '<h4>'.esc_html__('Delete Orphaned Relationship Data', 'divi_rocket').'</h4>
				<p>'.sprintf(esc_html__('%d orphaned relationships will be deleted.', 'divi_rocket'), $dbOptimizeRelationships->get_orphaned_relationship_count()).'</p>',
            'id' => 'delete_orphaned_relationship_data',
        ];
        $data[] = [
            'cb' => '<input type="checkbox" name="task[]" value="delete_orphaned_user_data" />',
            'optimization_task' => '<h4>'.esc_html__('Delete Orphaned User Metadata', 'divi_rocket').'</h4>
				<p>'.sprintf(esc_html__('%d orphaned user metadata entries will be deleted.', 'divi_rocket'), $dbOptimizeUsers->get_orphaned_user_data_count()).'</p>',
            'id' => 'delete_orphaned_user_data',
        ];
        $data[] = [
            'cb' => '<input type="checkbox" name="task[]" value="delete_orphaned_term_data" />',
            'optimization_task' => '<h4>'.esc_html__('Delete Orphaned Term Metadata', 'divi_rocket').'</h4>
				<p>'.sprintf(esc_html__('%d orphaned term metadata entries will be deleted.', 'divi_rocket'), $dbOptimizeTerms->get_orphaned_term_data_count()).'</p>',
            'id' => 'delete_orphaned_term_data',
        ];

        return $data;
    }

    public function get_columns()
    {
        return [
            'cb' => __('<input type="checkbox" />', 'divi_rocket'),
            'optimization_task' => __('Tasks', 'divi_rocket'),
            'result' => '',
        ];
    }

    public function column_default($item, $column_name)
    {
        switch ($column_name) {
            case 'cb':
            case 'optimization_task':
                return $item['optimization_task'];

                break;
            case 'result':
                $rocket_task_results = get_transient('divi_rocket_tasks');

                if (false !== $rocket_task_results && is_array($rocket_task_results)) {
                    $current_task_processed = false;
                    foreach ($rocket_task_results as $task_result) {
                        if (isset($task_result['task_name']) && $item['id'] == $task_result['task_name']) {
                            $current_task_processed = $task_result;
                        }
                    }
                    if (false !== $current_task_processed) {
                        if ('none' == $current_task_processed['result']) {
                            echo "<span class='no-result'>".esc_html__('No optimizations were performed.', 'divi_rocket').'</span>';
                        } elseif ('success' == $current_task_processed['result']) {
                            echo "<span class='success-result'>".esc_html__('Success!', 'divi_rocket').'</span>';
                        }
                    }
                }

            break;
            default:
                return $item[$column_name];

            break;
        }
    }

    /**
     * Display the bulk actions dropdown.
     *
     * @since 3.1.0
     *
     * @param string $which The location of the bulk actions: 'top' or 'bottom'.
     *                      This is designated as optional for backward compatibility.
     */
    protected function bulk_actions($which = '')
    {
        if (is_null($this->_actions)) {
            $this->_actions = $this->get_bulk_actions();
            /*
             * Filters the list table Bulk Actions drop-down.
             *
             * The dynamic portion of the hook name, `$this->screen->id`, refers
             * to the ID of the current screen, usually a string.
             *
             * This filter can currently only be used to remove bulk actions.
             *
             * @since 3.5.0
             *
             * @param string[] $actions An array of the available bulk actions.
             */
            $this->_actions = apply_filters("bulk_actions-{$this->screen->id}", $this->_actions);
            $two = '';
        } else {
            $two = '2';
        }

        if (empty($this->_actions)) {
            return;
        }

        echo '<label for="bulk-action-selector-'.esc_attr($which).'" class="screen-reader-text">'.esc_html__('Select bulk action', 'divi_rocket').'</label>';
        echo '<select name="action'.et_core_intentionally_unescaped($two, 'fixed_string').'" id="bulk-action-selector-'.esc_attr($which)."\" style=\"display:none;\">\n";

        foreach ($this->_actions as $name => $title) {
            $class = 'edit' === $name ? ' class="hide-if-no-js"' : '';

            echo "\t".'<option value="'.esc_attr($name).'"'.et_core_intentionally_unescaped($class, 'fixed_string').'>'.esc_html($title)."</option>\n";
        }

        echo "</select>\n";

        submit_button(__('Process Selected Tasks', 'divi_rocket'), 'action', '', false, ['id' => "doaction{$two}"]);
        echo "\n";
    }
}

class DiviRocket_Trash_Posts extends WP_list_Table
{
    public function __construct()
    {
        parent::__construct([
            'singular' => __('Trash List', 'divi_rocket'),
            'plural' => __('Trash List', 'divi_rocket'),
            'ajax' => false,
        ]);
    }

    public function prepare_items()
    {
        $columns = $this->get_columns();
        $hidden = $this->get_hidden_columns();
        $sortable = $this->get_sortable_columns();
        $this->process_bulk_action();
        $data = $this->table_data();

        $this->_column_headers = [$columns, $hidden, $sortable];
        $this->items = $data;
    }

    public function column_cb($item)
    {
        return sprintf(
            '<input type="checkbox" name="%1$s" value="%1$s" />',
            'task[]',
            (isset($item['id'])) ? $item['id'] : ''
        );
    }

    public function get_bulk_actions()
    {
        return [
            'delete' => 'Delete',
        ];
    }

    /**
     * Process our bulk actions.
     *
     * @since 1.2
     */
    public function process_bulk_action()
    {
        // $post_types = (is_array($_REQUEST['post_type'])) ? $_REQUEST['post_type'] : [$_REQUEST['post_type']];

        if ('process' === $this->current_action()) {
            $dbOptimization = new DiviRocketDBOptimization();
        }
    }

    public function get_hidden_columns()
    {
        return [];
    }

    public function get_sortable_columns()
    {
        return [];
    }

    public function table_data()
    {
        global $wpdb;
        $trashed_posts = $wpdb->get_results("SELECT DISTINCT post_type FROM wp_posts WHERE post_status='trash' GROUP BY post_type");

        $data = [];

        if (!empty($trashed_posts)) {
            foreach ($trashed_posts as $trashed_post) {
                $data[] = [
                    'cb' => '<input type="checkbox" name="post_type[]" value="'.$trashed_post->post_type.'" />',
                    'post_type' => $trashed_post->post_type,
                ];
            }
        }

        return $data;
    }

    public function get_columns()
    {
        return [
            'cb' => __('<input type="checkbox" />', 'divi_rocket'),
            'post_type' => __('Post Type', 'divi_rocket'),
        ];
    }

    public function column_default($item, $column_name)
    {
        switch ($column_name) {
            case 'cb':
            case 'post_type':
            default:
                return $item[$column_name];
        }
    }
}

class DiviRocket_Optimize_DB extends WP_list_Table
{
    public function __construct()
    {
        parent::__construct([
            'singular' => __('DB Optimization', 'divi_rocket'),
            'plural' => __('DB Optimization', 'divi_rocket'),
            'ajax' => false,
        ]);
    }

    public function prepare_items()
    {
        $columns = $this->get_columns();
        $hidden = $this->get_hidden_columns();
        $sortable = $this->get_sortable_columns();
        $this->process_bulk_action();
        $data = $this->table_data();

        $this->_column_headers = [$columns, $hidden, $sortable];
        $this->items = $data;
    }

    public function column_cb($item)
    {
        return sprintf(
            '<input type="checkbox" name="%1$s" value="%2$s" />',
            'post_type[]',
            $item['post_type']
        );
    }

    public function get_bulk_actions()
    {
        return [
            'delete' => 'Delete',
        ];
    }

    /**
     * Process our bulk actions.
     *
     * @since 1.2
     */
    public function process_bulk_action()
    {
		/*
        $post_types = (is_array($_REQUEST['post_type'])) ? $_REQUEST['post_type'] : [$_REQUEST['post_type']];

        if ('delete' === $this->current_action()) {
            global $wpdb;

            foreach ($post_types as $type) {
                $wpdb->query("DELETE FROM {$wpdb->prefix}posts WHERE post_type = '".$type."' AND post_status='trash'");
            }
        }
		*/
    }

    public function get_hidden_columns()
    {
        return [];
    }

    public function get_sortable_columns()
    {
        return [];
    }

    public function table_data()
    {
        global $wpdb;
        $divi_tables = $wpdb->get_results('SHOW TABLES');

        $data = [];

        if (!empty($divi_tables)) {
            foreach ($divi_tables as $table) {
                foreach ($table as $t) {
                    $data[] = [
                        'cb' => '<input type="checkbox" name="post_type[]" value="'.$t.'" />',
                        'table_name' => $t,
                    ];
                }
            }
        }

        return $data;
    }

    public function get_columns()
    {
        return [
            'cb' => __('<input type="checkbox" />', 'divi_rocket'),
            'table_name' => __('Divi Table', 'divi_rocket'),
        ];
    }

    public function column_default($item, $column_name)
    {
        switch ($column_name) {
            case 'cb':
            case 'table_name':
            default:
                return $item[$column_name];
        }
    }
}
