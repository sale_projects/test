<?php
/*
This file modified by Carl Wuensche and Jonathan Hall; last modified 2020-02-21
*/

defined('ABSPATH') || die();

class DiviRocketOptimizeTransients
{
    private $_db_name;
    private $_prefix;
    private $_wpdb;

    public function __construct()
    {
        global $wpdb;

        $this->_wpdb = $wpdb;
        $this->_db_name = $wpdb->dbname;
        $this->_prefix = $wpdb->prefix;
    }

    public function get_expired_transient_count()
    {
        return $this->_wpdb->get_var("SELECT COUNT( * ) FROM {$this->_prefix}options WHERE option_name LIKE '%_transient_%'");
    }

    public function delete_expired_transients()
    {
        $this->_wpdb->query("DELETE FROM {$this->_prefix}options WHERE option_name LIKE '%_transient_%' ");

        if (0 < $this->_wpdb->rows_affected/*good3*/) {
            return true;
        }

        return false;
    }
}
