<?php
/*
This file modified by Carl Wuensche and Jonathan Hall; last modified 2020-02-21
*/

defined('ABSPATH') || die();

class DiviRocketOptimizeComments
{
    private $_db_name;
    private $_prefix;
    private $_wpdb;

    public function __construct()
    {
        global $wpdb;

        $this->_wpdb = $wpdb;
        $this->_db_name = $wpdb->dbname;
        $this->_prefix = $wpdb->prefix;
    }

    public function get_orphaned_comment_count()
    {
        return $this->_wpdb->get_var('SELECT COUNT( * ) FROM '.$this->_prefix.'commentmeta LEFT JOIN '.$this->_prefix.'comments ON '.$this->_prefix.'commentmeta.comment_id='.$this->_prefix.'comments.comment_ID WHERE '.$this->_prefix.'comments.comment_ID IS NULL');
    }

    public function get_spam_trash_comment_count()
    {
        return $this->_wpdb->get_var('SELECT COUNT( * ) FROM '.$this->_prefix."comments WHERE comment_approved='spam' OR comment_approved='trash'");
    }

    public function get_unapproved_comment_count()
    {
        return $this->_wpdb->get_var('SELECT COUNT( * ) FROM '.$this->_prefix."comments WHERE comment_approved='0' ");
    }

    public function clean_spam_trash_comments()
    {
        $spam_trash_posts = $this->_wpdb->query('DELETE FROM '.$this->_prefix."comments WHERE comment_approved='spam' OR comment_approved='trash'");
        if (0 < $this->_wpdb->rows_affected/*good3*/) {
            return true;
        }

        return false;
    }

    public function clean_unapproved_comments()
    {
        $clean_unapproved_comments = $this->_wpdb->query('DELETE FROM '.$this->_prefix."comments WHERE comment_approved='0'");
        if (0 < $this->_wpdb->rows_affected/*good3*/) {
            return true;
        }

        return false;
    }

    public function clean_orphaned_comments()
    {
        $clean_orphaned_comments = $this->_wpdb->query('DELETE FROM '.$this->_prefix.'commentmeta WHERE NOT EXISTS(SELECT comment_ID FROM '.$this->_prefix.'comments WHERE '.$this->_prefix.'comments.comment_ID='.$this->_prefix.'commentmeta.comment_id)');

        if (0 < $this->_wpdb->rows_affected/*good3*/) {
            return true;
        }

        return false;
    }
}
