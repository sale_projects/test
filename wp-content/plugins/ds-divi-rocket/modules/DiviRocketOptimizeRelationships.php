<?php
/*
This file modified by Carl Wuensche and Jonathan Hall; last modified 2020-02-21
*/

defined('ABSPATH') || die();

class DiviRocketOptimizeRelationships
{
    private $_db_name;
    private $_prefix;
    private $_wpdb;

    public function __construct()
    {
        global $wpdb;

        $this->_wpdb = $wpdb;
        $this->_db_name = $wpdb->dbname;
        $this->_prefix = $wpdb->prefix;
    }

    public function get_orphaned_relationship_count()
    {
        return $this->_wpdb->get_var('SELECT COUNT( * ) FROM '.$this->_prefix.'term_relationships LEFT JOIN '.$this->_prefix.'posts ON '.$this->_prefix.'term_relationships.object_id='.$this->_prefix.'posts.ID WHERE '.$this->_prefix.'posts.ID IS NULL');
    }

    public function clean_orphaned_relationships()
    {
        $delete_orphaned_relationships = $this->_wpdb->query('DELETE FROM '.$this->_prefix.'term_relationships WHERE NOT EXISTS(SELECT ID FROM '.$this->_prefix.'posts WHERE '.$this->_prefix.'posts.ID='.$this->_prefix.'term_relationships.object_id)');
        if (0 < $this->_wpdb->rows_affected) {
            return true;
        }

        return false;
    }
}
