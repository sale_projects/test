<?php
/*
This file modified by Carl Wuensche, Jonathan Hall, and Dominika Rauk; last modified 2020-02-21
*/

defined('ABSPATH') || die();

class DiviRocketDBOptimization
{
    private $_db_name;
    private $_prefix;
    private $_wpdb;

    public function __construct()
    {
        global $wpdb;

        $this->_wpdb = $wpdb;
        $this->_db_name = $wpdb->dbname;
        $this->_prefix = $wpdb->prefix;
    }

    public function shouldExcludeInnoDb()
    {
        $dbVersion = explode('-', (string) $this->_wpdb->get_var('SELECT VERSION()'));

        return version_compare($dbVersion[0], '5.7', '<');
    }

    public function get_Optimizations()
    {
        $engine = null;
        $rows = 0;

        if ($this->shouldExcludeInnoDb()) {
            $innodb_count = $this->_wpdb->get_var($this->_wpdb->prepare("SELECT COUNT(*) FROM information_schema.`TABLES` WHERE TABLE_SCHEMA=%s AND ENGINE='InnoDB'", $this->_db_name));
            $other_count = $this->_wpdb->get_var($this->_wpdb->prepare("SELECT COUNT(*) FROM information_schema.`TABLES` WHERE TABLE_SCHEMA=%s AND ENGINE!='InnoDB'", $this->_db_name));

            return "{$other_count} tables will be optimized. Tables using the InnoDB engine ({$innodb_count}) will not be optimized.";
        }

        $count = $this->_wpdb->get_var($this->_wpdb->prepare('SELECT COUNT(*) FROM information_schema.`TABLES` WHERE TABLE_SCHEMA=%s', $this->_db_name));

        return "{$count} tables will be optimized.";
    }

    public function optimizeTables()
    {
        $tables_to_optimize = $this->_wpdb->get_results(
            $this->_wpdb->prepare(
                'SELECT * FROM information_schema.`TABLES` WHERE TABLE_SCHEMA=%s'.($this->shouldExcludeInnoDb() ? ' AND ENGINE!="InnoDB"' : ''),
                $this->_db_name
            )
        );

        $success = true;
        foreach ($tables_to_optimize as $optimize_table) {
            $success = false !== $this->_wpdb->query('OPTIMIZE TABLE '.$optimize_table->TABLE_NAME) && $success;
        }

        return $success;
    }

    public function success_notice()
    {
        ?>
        <div class="notice notice-success is-dismissible">
            <p><?php esc_html_e('Success', 'divi_rocket'); ?></p>
        </div>
<?php
    }

    public function empty_notice()
    {
        ?>
        <div class="notice notice-info error is-dismissible">
            <p><?php esc_html_e('No changes made to any table optimization.', 'divi_rocket'); ?></p>
        </div>
    <?php
    }
}
