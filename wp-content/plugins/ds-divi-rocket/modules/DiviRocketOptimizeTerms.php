<?php
/*
This file modified by Carl Wuensche and Jonathan Hall; last modified 2020-02-21
*/

defined('ABSPATH') || die();

class DiviRocketOptimizeTerms
{
    private $_db_name;
    private $_prefix;
    private $_wpdb;

    public function __construct()
    {
        global $wpdb;

        $this->_wpdb = $wpdb;
        $this->_db_name = $wpdb->dbname;
        $this->_prefix = $wpdb->prefix;
    }

    public function get_orphaned_term_data_count()
    {
        return $this->_wpdb->get_var('SELECT COUNT( * ) FROM '.$this->_prefix.'termmeta LEFT JOIN '.$this->_prefix.'terms ON '.$this->_prefix.'terms.term_id='.$this->_prefix.'termmeta.term_id WHERE '.$this->_prefix.'terms.term_id IS NULL');
    }

    public function clean_orphaned_term_data()
    {
        $delete_orphaned_relationships = $this->_wpdb->query('DELETE FROM '.$this->_prefix.'term_relationships WHERE ');
        $clean_term_data = $this->_wpdb->query('DELETE FROM '.$this->_prefix.'termmeta WHERE NOT EXISTS(SELECT term_id FROM '.$this->_prefix.'terms WHERE '.$this->_prefix.'terms.term_id='.$this->_prefix.'termmeta.term_id)');
        if (0 < $this->_wpdb->rows_affected/*good3*/) {
            return true;
        }

        return false;
    }
}
