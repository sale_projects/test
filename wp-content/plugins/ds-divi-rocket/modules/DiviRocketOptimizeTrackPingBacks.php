<?php
/*
This file modified by Carl Wuensche and Jonathan Hall; last modified 2020-02-21
*/

defined('ABSPATH') || die();

class DiviRocketOptimizeTrackPingBacks
{
    private $_db_name;
    private $_prefix;
    private $_wpdb;

    public function __construct()
    {
        global $wpdb;

        $this->_wpdb = $wpdb;
        $this->_db_name = $wpdb->dbname;
        $this->_prefix = $wpdb->prefix;
    }

    public function get_trackback_count()
    {
        return $this->_wpdb->get_var('SELECT COUNT( * ) FROM '.$this->_prefix."comments WHERE comment_type='trackback' ");
    }

    public function get_pingback_count()
    {
        return $this->_wpdb->get_var('SELECT COUNT( * ) FROM '.$this->_prefix."comments WHERE comment_type='pingback' ");
    }

    public function clean_pingbacks_and_trackbacks()
    {
        $clean_pingbacks = $this->_wpdb->query('DELETE FROM '.$this->_prefix."comments WHERE comment_type='pingback' OR comment_type='trackback'");
        if (0 < $this->_wpdb->rows_affected/*good3*/) {
            return true;
        }

        return false;
    }
}
