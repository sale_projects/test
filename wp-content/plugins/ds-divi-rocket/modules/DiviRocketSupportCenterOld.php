<?php
/*
This file includes code based on parts of the Divi theme, copyright Elegant Themes,
released under the GNU General Public License (GPL) version 2, licensed under GPL
version 3 for this project by special permission (see ../license.txt).

This file modified by Jonathan Hall, Carl Wuensche, and Dominika Rauk; last modified 2020-09-22
*/

// Quick exit if accessed directly
if (!defined('ABSPATH')) {
    exit;
}

/**
 * Elegant Themes Support Center adds a new page to the WP Admin menu.
 *
 * System Status
 * Here we note system settings that could potentially cause problems. An extended view (displaying all settings we
 * check, not just those with problematic results) can be toggled, with an option to copy this report to the
 * clipboard so it can be pasted in a support ticket.
 *
 * Elegant Themes Support
 * If Remote Access is enabled in this section, Elegant Themes Support will be granted limited access to the user's site
 * (@see ET_Core_SupportCenter::support_user_maybe_create_user()). When this is activated, a second toggle appears
 * for the user that will allow them to enable "Full Admin Privileges" which has no restrictions (only certain ET
 * Support staff will be able to request that the user enables this). Full Admin Privileges can be disabled at any
 * time, but are automatically disabled whenever the Remote Access is disabled (manually or by timeout). Time
 * remaining until Remote Access is automatically deactivated is indicated alongside the toggle. A link for
 * initiating a chat https://www.elegantthemes.com/members-area/help/ is also available in this section.
 *
 * Divi Documentation & Help
 * This section contains common help videos, articles, and a link to full documentation. This is not meant to be a
 * full service documentation center; it's mainly a launch off point.
 *
 * Divi Safe Mode
 * A quick and easy way for users and support to quickly disable plugins and scripts to see if Divi is the cause of
 * an issue. This call to action disables active plugins, custom css, child themes, scripts in the Integrations tab,
 * static css, and combination/minification of CSS and JS. When enabling this, the user will be presented with a
 * list of plugins that will be affected (disabled). Sitewide (not including the Visual Builder), there will be a
 * floating indicator in the upper right or left corner of the website that will indicate that Safe Mode is enabled
 * and will contain a link that takes you to the Support Page to disabled it.
 *
 * Logs
 * If WP_DEBUG_LOG is enabled, WordPress related errors will be archived in a log file. We load the most recent entries
 * of this log file for convienient viewing here, with a link to download the full log, as well as an option to copy
 * the log to the clipboard so it can be pasted in a support ticket.
 *
 * @author  Elegant Themes <http://www.elegantthemes.com>
 * @license GNU General Public License v2 <http://www.gnu.org/licenses/gpl-2.0.html>
 *
 * @since 3.24.1 Renamed from `ET_Support_Center` to `ET_Core_SupportCenter`.
 * @since 3.20
 */
class Divi_Rocket_ET_Core_SupportCenter extends ET_Support_Center
{
    public function add_system_report()
    {
        // Build Card :: System Status
        $report = $this->system_diagnostics_generate_report(true, 'div');
        $divEnd = strrpos($report, '</div>');

        if ($divEnd) {
            $report = substr($report, 0, $divEnd + 6);
        }

        $card_title = esc_html__('System Status', 'divi_rocket');
        $card_content = sprintf(
                '<div class="et-system-status summary">%1$s</div>'
                                     //. '<textarea id="et_system_status_plain">%2$s</textarea>'
                                     //. '<div class="et_card_cta">%3$s %4$s %5$s</div>',
                ,
                et_core_intentionally_unescaped($report, 'html')//,
                //et_core_intentionally_unescaped( $this->system_diagnostics_generate_report( true, 'plain' ), 'html' ),
                //sprintf( '<a class="full_report_show">%1$s</a>', esc_html__( 'Show Full Report', 'et-core' ) ),
                //sprintf( '<a class="full_report_hide">%1$s</a>', esc_html__( 'Hide Full Report', 'et-core' ) ),
                //sprintf( '<a class="full_report_copy">%1$s</a>', esc_html__( 'Copy Full Report', 'et-core' ) )
            );

        echo et_core_esc_previously($card_content);
    }
}
