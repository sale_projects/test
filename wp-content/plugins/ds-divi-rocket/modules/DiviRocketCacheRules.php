<?php
/*
Credits:
See main plugin file
This file modified by Carl Wuensche and Jonathan Hall; last modified 2020-02-21
*/

defined('ABSPATH') || die();

class DiviRocketCacheRules
{
    public static function erase_gzip_rules()
    {
        $htaccess_file_path = ABSPATH.'.htaccess';
        $processed_rules = true;
        $marker = 'Divi Rocket GZIP';

        insert_with_markers($htaccess_file_path, $marker, '');

        return $processed_rules;
    }

    public static function write_gzip_rules()
    {
        $htaccess_file_path = ABSPATH.'.htaccess';
        $divi_rocket_marker = 'Divi Rocket GZIP';
        $lines = [];
        $lines[] = '<IfModule mod_deflate.c>';
        $lines[] = '# Compress HTML, CSS, JavaScript, Text, XML and fonts';
        $lines[] = 'AddOutputFilterByType DEFLATE application/javascript';
        $lines[] = 'AddOutputFilterByType DEFLATE application/rss+xml';
        $lines[] = 'AddOutputFilterByType DEFLATE application/vnd.ms-fontobject';
        $lines[] = 'AddOutputFilterByType DEFLATE application/x-font';
        $lines[] = 'AddOutputFilterByType DEFLATE application/x-font-opentype';
        $lines[] = 'AddOutputFilterByType DEFLATE application/x-font-otf';
        $lines[] = 'AddOutputFilterByType DEFLATE application/x-font-truetype';
        $lines[] = 'AddOutputFilterByType DEFLATE application/x-font-ttf';
        $lines[] = 'AddOutputFilterByType DEFLATE application/x-javascript';
        $lines[] = 'AddOutputFilterByType DEFLATE application/xhtml+xml';
        $lines[] = 'AddOutputFilterByType DEFLATE application/xml';
        $lines[] = 'AddOutputFilterByType DEFLATE font/opentype';
        $lines[] = 'AddOutputFilterByType DEFLATE font/otf';
        $lines[] = 'AddOutputFilterByType DEFLATE font/ttf';
        $lines[] = 'AddOutputFilterByType DEFLATE image/svg+xml';
        $lines[] = 'AddOutputFilterByType DEFLATE image/x-icon';
        $lines[] = 'AddOutputFilterByType DEFLATE text/css';
        $lines[] = 'AddOutputFilterByType DEFLATE text/html';
        $lines[] = 'AddOutputFilterByType DEFLATE text/javascript';
        $lines[] = 'AddOutputFilterByType DEFLATE text/plain';
        $lines[] = 'AddOutputFilterByType DEFLATE text/xml';
        $lines[] = '';
        $lines[] = '# Remove browser bugs (only needed for really old browsers)';
        $lines[] = 'BrowserMatch ^Mozilla/4 gzip-only-text/html';
        $lines[] = 'BrowserMatch ^Mozilla/4\.0[678] no-gzip';
        $lines[] = 'BrowserMatch \bMSIE !no-gzip !gzip-only-text/html';
        $lines[] = 'Header append Vary User-Agent';
        $lines[] = '</IfModule>';

        insert_with_markers($htaccess_file_path, $divi_rocket_marker, $lines);
    }

    public static function erase_cache_control_rules()
    {
        $htaccess_file_path = ABSPATH.'.htaccess';
        $processed_rules = false;

        $marker = 'Divi Rocket Cache Control';
        insert_with_markers($htaccess_file_path, $marker, '');

        return true;
    }

    public static function write_cache_control_rules($expiration_time, $excludeUrls)
    {
        foreach ($excludeUrls as &$url) {
            $url = trim($url);
            if ($url && '/' == $url[0]) {
                $url = substr($url, 1);
            }
            $url = preg_quote($url);
            $url = str_replace(['\\*', '#'], ['.+', ''], $url);
        }
        $excludeRegex = implode('|', $excludeUrls);
        if ($excludeRegex) {
            $excludeRegex = '^/('.$excludeRegex.')$';
        }

        $cache_control_rules = ['<filesMatch ".(ico|pdf|flv|jpg|jpeg|png|gif|svg|js|css|swf|otf|ttf|webp)$">'];

        if ($excludeRegex) {
            $cache_control_rules[] = '<If "%{REQUEST_URI} !~ m#'.$excludeRegex.'#">';
        }

        $cache_control_rules[] = 'Header set Cache-Control "max-age='.((int) $expiration_time).', public"';

        if ($excludeRegex) {
            $cache_control_rules[] = '</If>';
        }

        $cache_control_rules[] = '</filesMatch>';

        $htaccess_file_path = ABSPATH.'.htaccess';
        insert_with_markers($htaccess_file_path, 'Divi Rocket Cache Control', $cache_control_rules);
    }
}
