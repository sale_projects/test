<?php
/*
Plugin Name: Divi Rocket
Version: 1.0.31
Description: Divi Rocket is the most powerful caching plugin specifically designed for the Divi Theme.
Author: Divi Space
Author URI: https://divi.space/
Plugin URI: https://divi.space/product/divi-rocket/
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl.html

*/

update_option( 'divi_rocket_license_status', 'valid' );
update_option('divi_rocket_license_key', '*********');
/*
Despite the following, this project is licensed exclusively
under GNU General Public License (GPL) version 3 (no future versions).
This statement modifies the following text.

Divi Rocket plugin
Copyright (C) 2020  Aspen Grove Studios

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

========

Credits:

This plugin includes code based on and/or copied from parts of WordPress
by Automattic, released under the GNU General Public License (GPL) version 2 or later,
licensed under GPL version 3 or later (see ./license.txt file).

This plugin includes code based on parts of the Divi theme, copyright Elegant Themes,
released under the GNU General Public License (GPL) version 2, licensed under GPL
version 3 for this project by special permission (see ./license.txt file).

This plugin includes code based on parts of a3 Lazy Load, Copyright © 2011 a3 Revolution Software Development team, released under
the GNU General Public License (GPL) version 2 or later, licensed under GPL version 3 or later
in this project. See ./license.txt file.

This plugin includes code based on and/or copied from parts of the WP-Optimize plugin, copyright Updraft WP Software Ltd.,
released under the GNU General Public License (GPL) version 2 or later,
licensed under GPL version 3 or later. See ./license.txt file.

Got htaccess code here https://technumero.com/how-to-leverage-browser-caching-wordpress/

This file modified by Jonathan Hall, Carl Wuensche, Dominika Rauk, Anna Kurowska, Cory Jenkins and/or others; last modified 2021-03-05

*/defined('ABSPATH') || die();

/* !etm { */
include_once(__DIR__.'/updater/updater.php');
/* } !etm */

define('DIVI_ROCKET_CACHE_PATH', ABSPATH.'wp-content/cache/divispace/');

    class DiviRocket
    {
        // The credits and modification statement at the top of this file do not apply to the following two lines:
        const	VERSION = '1.0.31';
        const	PLUGIN_AUTHOR_URL = 'https://divi.space/';
        const	PLUGIN_SLUG = 'divi-rocket';
        const	SHORTCODE_STATIC = 1;
        const	SHORTCODE_CONTENT_DYNAMIC = 2;
        const	SHORTCODE_DYNAMIC = 3;
        const	SHORTCODE_CONTENT_PLACEHOLDER = '#DiviRocketPlaceholder#';
        public static $cache_permission_object = null;
        public static $pluginBaseUrl;
        public static $pluginDirectory;
        public static $pluginFile;
        public static $postsCachePath;
        public static $templatesCachePath;
        public static $commentsCachePath;

	// ds-divi-rocket2\includes\DiviSpacePostCacher.php
	public static $DEBUG = false;

        // $shortcodes and $literalContentShortcodes contain shortcodes copied from the Divi theme - see comment near the top of this file for copyright and licensing info
        public static $shortcodes = [
            'et_pb_section' => self::SHORTCODE_STATIC,
            'et_pb_row' => self::SHORTCODE_STATIC,
            'et_pb_row_inner' => self::SHORTCODE_STATIC,
            'et_pb_column' => self::SHORTCODE_STATIC,
            'et_pb_column_inner' => self::SHORTCODE_STATIC,
            'et_pb_column_empty' => self::SHORTCODE_STATIC,

            'et_pb_accordion' => self::SHORTCODE_STATIC,
            'et_pb_accordion_item' => self::SHORTCODE_STATIC,
            'et_pb_audio' => self::SHORTCODE_STATIC,
            'et_pb_counters' => self::SHORTCODE_STATIC,
            'et_pb_counter' => self::SHORTCODE_STATIC,
            'et_pb_blog' => self::SHORTCODE_CONTENT_DYNAMIC,
            'et_pb_blurb' => self::SHORTCODE_STATIC,
            'et_pb_button' => self::SHORTCODE_STATIC,
            'et_pb_circle_counter' => self::SHORTCODE_STATIC,
            'et_pb_code' => self::SHORTCODE_STATIC,
            //'et_pb_comments' => self::SHORTCODE_CONTENT_DYNAMIC,
            'et_pb_comments' => self::SHORTCODE_DYNAMIC,
            'et_pb_contact_form' => self::SHORTCODE_DYNAMIC,
            'et_pb_contact_field' => self::SHORTCODE_STATIC,
            'et_pb_countdown_timer' => self::SHORTCODE_STATIC,
            'et_pb_cta' => self::SHORTCODE_STATIC,
            'et_pb_divider' => self::SHORTCODE_STATIC,
            'et_pb_filterable_portfolio' => self::SHORTCODE_CONTENT_DYNAMIC,
            'et_pb_fullwidth_code' => self::SHORTCODE_STATIC,
            'et_pb_fullwidth_header' => self::SHORTCODE_STATIC,
            'et_pb_fullwidth_image' => self::SHORTCODE_STATIC,
            'et_pb_fullwidth_map' => self::SHORTCODE_STATIC,
            'et_pb_fullwidth_menu' => self::SHORTCODE_DYNAMIC,
            'et_pb_fullwidth_portfolio' => self::SHORTCODE_CONTENT_DYNAMIC,
            'et_pb_fullwidth_post_slider' => self::SHORTCODE_CONTENT_DYNAMIC,
            'et_pb_fullwidth_post_title' => self::SHORTCODE_CONTENT_DYNAMIC,
            'et_pb_fullwidth_slider' => self::SHORTCODE_STATIC,
            //'et_pb_gallery' => self::SHORTCODE_CONTENT_DYNAMIC,
            'et_pb_gallery' => self::SHORTCODE_STATIC,
            'et_pb_image' => self::SHORTCODE_STATIC,
            'et_pb_login' => self::SHORTCODE_DYNAMIC,
            'et_pb_map' => self::SHORTCODE_STATIC,
            'et_pb_map_pin' => self::SHORTCODE_STATIC,
            'et_pb_number_counter' => self::SHORTCODE_STATIC,
            'et_pb_portfolio' => self::SHORTCODE_CONTENT_DYNAMIC,
            'et_pb_post_slider' => self::SHORTCODE_CONTENT_DYNAMIC,
            'et_pb_post_nav' => self::SHORTCODE_CONTENT_DYNAMIC,
            'et_pb_post_title' => self::SHORTCODE_CONTENT_DYNAMIC,
            'et_pb_pricing_tables' => self::SHORTCODE_STATIC,
            'et_pb_pricing_table' => self::SHORTCODE_STATIC,
            'et_pb_search' => self::SHORTCODE_STATIC,
            'et_pb_shop' => self::SHORTCODE_CONTENT_DYNAMIC,
            'et_pb_sidebar' => self::SHORTCODE_DYNAMIC,
            'et_pb_signup' => self::SHORTCODE_DYNAMIC,
            'et_pb_signup_custom_field' => self::SHORTCODE_STATIC,
            'et_pb_slider' => self::SHORTCODE_STATIC,
            'et_pb_slide' => self::SHORTCODE_STATIC,
            'et_pb_social_media_follow' => self::SHORTCODE_STATIC,
            'et_pb_social_media_follow_network' => self::SHORTCODE_STATIC,
            'et_pb_tabs' => self::SHORTCODE_STATIC,
            'et_pb_tab' => self::SHORTCODE_STATIC,
            'et_pb_team_member' => self::SHORTCODE_STATIC,
            'et_pb_testimonial' => self::SHORTCODE_STATIC,
            'et_pb_text' => self::SHORTCODE_STATIC,
            'et_pb_toggle' => self::SHORTCODE_STATIC,
            'et_pb_video' => self::SHORTCODE_STATIC,
            'et_pb_video_slider' => self::SHORTCODE_STATIC,
            'et_pb_video_slider_item' => self::SHORTCODE_STATIC,
            //'et_pb_module' => self::SHORTCODE_STATIC,

            'et_pb_wc_description' => self::SHORTCODE_CONTENT_DYNAMIC,
            'et_pb_wc_gallery' => self::SHORTCODE_CONTENT_DYNAMIC,
            'et_pb_wc_images' => self::SHORTCODE_CONTENT_DYNAMIC,
            'et_pb_wc_meta' => self::SHORTCODE_CONTENT_DYNAMIC,
            'et_pb_wc_price' => self::SHORTCODE_CONTENT_DYNAMIC,
            'et_pb_wc_rating' => self::SHORTCODE_CONTENT_DYNAMIC,
            'et_pb_wc_related_products' => self::SHORTCODE_CONTENT_DYNAMIC,
            'et_pb_wc_reviews' => self::SHORTCODE_DYNAMIC,
            'et_pb_wc_stock' => self::SHORTCODE_CONTENT_DYNAMIC,
            'et_pb_wc_tabs' => self::SHORTCODE_CONTENT_DYNAMIC,
            'et_pb_wc_title' => self::SHORTCODE_CONTENT_DYNAMIC,
            'et_pb_wc_upsells' => self::SHORTCODE_CONTENT_DYNAMIC,
            'et_pb_wc_additional_info' => self::SHORTCODE_CONTENT_DYNAMIC,
            'et_pb_wc_add_to_cart' => self::SHORTCODE_DYNAMIC,
            'et_pb_wc_breadcrumb' => self::SHORTCODE_CONTENT_DYNAMIC,
            'et_pb_wc_cart_notice' => self::SHORTCODE_DYNAMIC,

            'et_pb_fullwidth_post_content' => self::SHORTCODE_DYNAMIC,
            'et_pb_menu' => self::SHORTCODE_DYNAMIC,
            'et_pb_post_content' => self::SHORTCODE_DYNAMIC,
        ];
        public static $shortcodeFiles = [
            'et_pb_section' => '../main-structure-elements.php',
            'et_pb_row' => '../main-structure-elements.php',
            'et_pb_row_inner' => '../main-structure-elements.php',
            'et_pb_column' => '../main-structure-elements.php',
            'et_pb_column_inner' => '../main-structure-elements.php',
            'et_pb_column_empty' => '../main-structure-elements.php',
            'et_pb_accordion' => 'Accordion.php',
            'et_pb_accordion_item' => 'AccordionItem.php',
            'et_pb_audio' => 'Audio.php',
            'et_pb_counters' => 'BarCounters.php',
            'et_pb_counter' => 'BarCountersItem.php',
            'et_pb_blog' => 'Blog.php',
            'et_pb_blurb' => 'Blurb.php',
            'et_pb_button' => 'Button.php',
            'et_pb_circle_counter' => 'CircleCounter.php',
            'et_pb_code' => 'Code.php',
            'et_pb_comments' => 'Comments.php',
            'et_pb_contact_form' => 'ContactForm.php',
            'et_pb_contact_field' => 'ContactFormItem.php',
            'et_pb_countdown_timer' => 'CountdownTimer.php',
            'et_pb_cta' => 'Cta.php',
            'et_pb_divider' => 'Divider.php',
            'et_pb_filterable_portfolio' => 'FilterablePortfolio.php',
            'et_pb_fullwidth_code' => 'FullwidthCode.php',
            'et_pb_fullwidth_header' => 'FullwidthHeader.php',
            'et_pb_fullwidth_image' => 'FullwidthImage.php',
            'et_pb_fullwidth_map' => 'FullwidthMap.php',
            'et_pb_fullwidth_menu' => 'FullwidthMenu.php',
            'et_pb_fullwidth_portfolio' => 'FullwidthPortfolio.php',
            'et_pb_fullwidth_post_slider' => 'FullwidthPostSlider.php',
            'et_pb_fullwidth_post_title' => 'FullwidthPostTitle.php',
            'et_pb_fullwidth_slider' => 'FullwidthSlider.php',
            'et_pb_gallery' => 'Gallery.php',
            'et_pb_image' => 'Image.php',
            'et_pb_login' => 'Login.php',
            'et_pb_map' => 'Map.php',
            'et_pb_map_pin' => 'MapItem.php',
            'et_pb_number_counter' => 'NumberCounter.php',
            'et_pb_portfolio' => 'Portfolio.php',
            'et_pb_post_slider' => 'PostSlider.php',

            'et_pb_post_nav' => 'PostsNavigation.php',
            'et_pb_post_title' => 'PostTitle.php',
            'et_pb_pricing_tables' => 'PricingTables.php',
            'et_pb_pricing_table' => 'PricingTablesItem.php',
            'et_pb_search' => 'Search.php',
            'et_pb_shop' => 'Shop.php',
            'et_pb_sidebar' => 'Sidebar.php',
            'et_pb_signup' => 'Signup.php',
            'et_pb_signup_custom_field' => 'SignupItem.php',
            'et_pb_slider' => 'Slider.php',
            'et_pb_slide' => 'SliderItem.php',
            'et_pb_social_media_follow' => 'SocialMediaFollow.php',
            'et_pb_social_media_follow_network' => 'SocialMediaFollowItem.php',
            'et_pb_tabs' => 'Tabs.php',
            'et_pb_tab' => 'TabsItem.php',
            'et_pb_team_member' => 'TeamMember.php',
            'et_pb_testimonial' => 'Testimonial.php',
            'et_pb_text' => 'Text.php',
            'et_pb_toggle' => 'Toggle.php',
            'et_pb_video' => 'Video.php',
            'et_pb_video_slider' => 'VideoSlider.php',
            'et_pb_video_slider_item' => 'VideoSliderItem.php',

            'et_pb_wc_description' => 'woocommerce/Description.php',
            'et_pb_wc_gallery' => 'woocommerce/Gallery.php',
            'et_pb_wc_images' => 'woocommerce/Images.php',
            'et_pb_wc_meta' => 'woocommerce/Meta.php',
            'et_pb_wc_price' => 'woocommerce/Price.php',
            'et_pb_wc_rating' => 'woocommerce/Rating.php',
            'et_pb_wc_related_products' => 'woocommerce/RelatedProducts.php',
            'et_pb_wc_reviews' => 'woocommerce/Reviews.php',
            'et_pb_wc_stock' => 'woocommerce/Stock.php',
            'et_pb_wc_tabs' => 'woocommerce/Tabs.php',
            'et_pb_wc_title' => 'woocommerce/Title.php',
            'et_pb_wc_upsells' => 'woocommerce/Upsells.php',
            'et_pb_wc_additional_info' => 'woocommerce/AdditionalInfo.php',
            'et_pb_wc_add_to_cart' => 'woocommerce/AddToCart.php',
            'et_pb_wc_breadcrumb' => 'woocommerce/Breadcrumb.php',
            'et_pb_wc_cart_notice' => 'woocommerce/CartNotice.php',

            'et_pb_fullwidth_post_content' => 'FullwidthPostContent.php',
            'et_pb_menu' => 'Menu.php',
            'et_pb_post_content' => 'PostContent.php',

            'et_pb_pricing_item' => 'PricingTables.php',
        ];
        public static $literalContentShortcodes = [
            'et_pb_text', 'et_pb_blurb', 'et_pb_testimonial', 'et_pb_counter',
            'et_pb_login', 'et_pb_slide', 'et_pb_accordion_item', 'et_pb_code',
            'et_pb_cta', 'et_pb_fullwidth_code', 'et_pb_fullwidth_header', 'et_pb_map_pin',
            'et_pb_pricing_table', 'et_pb_tab', 'et_pb_team_member', 'et_pb_toggle',
            'et_pb_social_media_follow_network',
        ];
        public static $disableCacheShortcodes = [
            //'pbe_section',
            'et_pb_testify',
			'dswc_woocommerce_carousel',
			'dswc_woocommerce_carousel_child',
			'ags_woo_cart_list',
			'ags_woo_cart_totals',
			'ags_woo_checkout_billing_info',
			'ags_woo_checkout_coupon',
			'ags_woo_checkout_order_review',
			'ags_woo_checkout_shipping_info',
			'ags_woo_notices',
			'ags_woo_shop_plus',
			'ags_woo_shop_plus_child'
        ];
        public static $cachePostTypes = [
            'post', 'page', 'product', // from cache post types
        ];
        public static $postDependencies;
        public static $shortcodePostTypeDependencies = [
            'et_pb_blog' => ['*post_type'],
            'et_pb_comments' => ['_comment'],
            'et_pb_filterable_portfolio' => ['project'],
            'et_pb_fullwidth_portfolio' => ['project'],
            //'et_pb_fullwidth_post_slider',
            //'et_pb_fullwidth_post_title',
            'et_pb_portfolio' => ['project'],
            //'et_pb_post_slider',
            //'et_pb_post_nav',
            //'et_pb_post_title',
            //'et_pb_fullwidth_post_content',
            //'et_pb_post_content',

            'et_pb_shop' => ['product'],

            'et_pb_wc_description' => ['product'],
            'et_pb_wc_gallery' => ['product'],
            'et_pb_wc_images' => ['product'],
            'et_pb_wc_meta' => ['product'],
            'et_pb_wc_price' => ['product'],
            'et_pb_wc_rating' => ['product'],
            'et_pb_wc_related_products' => ['product'],
            'et_pb_wc_reviews' => ['product'],
            'et_pb_wc_stock' => ['product'],
            'et_pb_wc_tabs' => ['product'],
            'et_pb_wc_title' => ['product'],
            'et_pb_wc_upsells' => ['product'],
            'et_pb_wc_additional_info' => ['product'],
            'et_pb_wc_breadcrumb' => ['product'],
        ];
        public static $jsCompat = [
            'edd-ajax', 'ags-divi-icons', 'jetpack-lazy-images',
        ];
        public static $phpCompat = [
			'frontendInit' => [
				'divibars' => ['C', 'DiviBars_Controller']
			],
            'renderShortcode' => [
                'jetpack' => ['C', 'Jetpack_Lazy_Images'],
            ],
            'isCachingEnabled' => [
                'themify-wc-product-filter' => ['F', 'run_wpf'],
            ],
        ];
        public static $contentDynamicShortcodeCurrentUpdate;
        public static $contentDynamicShortcodeCurrentUpdateCss;
        public static $contentDynamicShortcodeCurrentDependencies;
        public static $contentDynamicShortcodeUpdates;
        public static $currentSectionRev;
        public static $lazyLoadStatus = [];
        public static $cachingStatus = [];
        public static $currentSectionInvalid;
        public static $isBuilderSetup = true;
        public static $lastStylesArray;
        public static $checkedVersions = false;
        public static $footerCss = '';
        public static $allowManualCacheClear = false;
        public static $currentUserId;
        public static $isSimulatingLoggedOut = false;
        public static $showFooterCredit;
        public static $footerCreditShown = false;
        private static $isRendering = false;

        public static function setup()
        {
            self::$pluginBaseUrl = plugin_dir_url(__FILE__);
            self::$pluginDirectory = __DIR__.'/';
            self::$pluginFile = __FILE__;

            self::$postsCachePath = ABSPATH.'wp-content/cache/divispace/posts/';
            if (!is_dir(self::$postsCachePath) && !wp_mkdir_p(self::$postsCachePath, 0755, true)) {
                return;
            }

            self::$templatesCachePath = ABSPATH.'wp-content/cache/divispace/templates/';
            if (!is_dir(self::$templatesCachePath) && !wp_mkdir_p(self::$templatesCachePath, 0755, true)) {
                return;
            }

/* !etm { */
			if ( divi_rocket_has_license_key() ) {
/* } !etm */

            $divi_rocket_gzip_compression = get_option('divi_rocket_gzip_compression');

            if (false !== $divi_rocket_gzip_compression && 0 !== (int) $divi_rocket_gzip_compression) {
                ini_set('zlib.output_compression', 'on');
				// wp-includes/default-filters.php - fix PHP notice that may occur with zlib compression enabled
				remove_action( 'shutdown', 'wp_ob_end_flush_all', 1 );
				add_action( 'shutdown', ['DiviRocket', 'wp_ob_end_flush_all'], 1 );
            } else {
                ini_set('zlib.output_compression', 0);
            }

            add_action('wp_ajax_divi-space-cache-clear-current', ['DiviRocket', 'ajax_cacheClearCurrent']);
            add_action('wp_ajax_divi-space-cache-clear-all', ['DiviRocket', 'ajax_cacheClearAll']);

			// phpcs:ignore WordPress.Security.NonceVerification.Missing
            if (isset($_POST['action']) && 'ds_return_section' == $_POST['action']) {
                add_action('after_setup_theme', ['DiviRocket', 'stopBuilderLoadOnAjax']);

            }

            //add_action( 'parse_query', array( 'DiviRocket', 'onParseQuery' ) );

            add_filter('et_builder_load_requests', ['DiviRocket', 'loadBuilderOnAjax']);

            require_once DiviRocket::$pluginDirectory.'/includes/DiviSpacePermissions.php';
            self::$cache_permission_object = new Divi_Rocket_Permissions();

            // Not 100% sure if we need these two separate, at some point we should see
            // if we can consolidate them for performance reasons

			add_action( 'wp', array( 'DiviRocket', 'onWpEarly' ), 0 );
			add_action( 'wp', array( 'DiviRocket', 'onWpLate' ), 99 );

			add_action( 'init', array( 'DiviRocket', 'onInit' ), 1 );
			add_action( 'wp_loaded', array( 'DiviRocket', 'onWpLoaded' ), 11 );
			add_action( 'wp_print_scripts', array( 'DiviRocket', 'onPrintScripts' ), 9 );

			require_once DiviRocket::$pluginDirectory.'/modules/DiviRocketOptimizationsAdmin.php';
            require_once DiviRocket::$pluginDirectory.'/modules/DiviRocketCacheRules.php';

            add_action('add_meta_boxes', ['DiviRocket', 'metaBoxes'], 11);
            add_filter('wp_revisions_to_keep', ['DiviRocket', 'filterPostRevisions']);
            add_action('wp_enqueue_scripts', ['DiviRocket', 'frontendScripts'], 9);
            add_action('admin_bar_menu', ['DiviRocket', 'adminBarMenu'], 120);
            add_action('wp_insert_post', ['DiviRocket', 'onPostSaved'], 10, 3);
            // Following line is disabled until comment module caching is complete
            //add_action( 'wp_insert_comment', array( 'DiviRocket', 'onCommentSaved' ), 10, 2 );


/* !etm { */
			} // end license key check
/* } !etm */

            add_action('admin_enqueue_scripts', ['DiviRocket', 'adminScripts'], 11);
            add_action('admin_menu', ['DiviRocket', 'adminMenu'], 11);

        }

		public static function onInit()
        {
			if (get_option('divi_rocket_cache_on')) {
				global $shortname;
                add_filter('et_get_option_'.'et_'.$shortname.'_et_pb_static_css_file', ['DiviRocket', 'filterValueOff']);
            }
		}

        public static function onWpLoaded()
        {
            global $shortname;

            add_action('update_option_et_'.$shortname, ['DiviRocket', 'cacheClearAll']);

			// phpcs:ignore WordPress.Security.NonceVerification
            self::$showFooterCredit = empty($_GET['et_tb']) ? (int) get_option('divi_rocket_footer_credit', 0) : 0;

            if (self::$showFooterCredit) {
                add_action('et_theme_builder_template_after_footer', ['DiviRocket', 'addFooterCredits']);
            }

            if (1 == self::$showFooterCredit) {
                add_filter('et_get_option_'.'et_'.$shortname.'_custom_footer_credits', ['DiviRocket', 'filterFooterCredits']);
            } elseif (2 == self::$showFooterCredit) {
                add_action('wp_footer', ['DiviRocket', 'addFooterCredits']);
            }

            self::$allowManualCacheClear = current_user_can('edit_posts');

			// phpcs:ignore WordPress.Security.NonceVerification.Missing
            if (isset($_POST['action']) && 'ds_return_section' == $_POST['action']) {
                self::return_section();
            }

			self::runPhpCompat('frontendInit');
        }

        public static function onWpEarly()
        {
            global $post;

			// phpcs:ignore WordPress.Security.NonceVerification.Missing
            if (!isset($_POST['action']) || 'ds_return_section' != $_POST['action']) {
                DiviRocket_dynamicallyLoadShortcodes();
            }

            if (!empty($post) && self::isCachingEnabled($post)) {
                add_filter('template_include', ['DiviRocket', 'interceptTemplate'], 9999);

                add_filter('get_post_metadata', ['DiviRocket', 'disableFontCache'], 10, 4);

                remove_action('shutdown', 'et_builder_update_fonts_cache');

                remove_action('wp_enqueue_scripts', 'et_builder_preprint_font');
            }
        }

		// wp-includes/functions.php - added error supression
		public static function wp_ob_end_flush_all() {
			$levels = ob_get_level();
			for ( $i = 0; $i < $levels; $i++ ) {
				@ob_end_flush();
			}
		}

        public static function onWpLate()
        {
            add_action('woocommerce_after_single_product_summary', ['DiviRocket', 'et_builder_wc_product_render_layout'], 4);
        }

        public static function disableFontCache($value, $object_id, $meta_key, $single)
        {
            return 'et_enqueued_post_fonts' == $meta_key ? ($single ? false : []) : $value;
        }

        public static function onPrintScripts()
        {
            global $wp_scripts;

            foreach (array_intersect(self::$jsCompat, $wp_scripts->queue) as $script) {
                wp_register_script('divi-rocket-compat-'.$script, DiviRocket::$pluginBaseUrl.'assets/js/compat/'.$script.'.js', [/*'divi-rocket-lazyload'*/], DiviRocket::VERSION, true);
                $wp_scripts->registered[$script]->deps[] = 'divi-rocket-compat-'.$script;
            }
        }

        public static function et_builder_wc_product_render_layout()
        {
			// Only run if Divi's layout rendering action was still hooked (and remove it)
            if ( remove_action('woocommerce_after_single_product_summary', 'et_builder_wc_product_render_layout', 5) ) {
				do_action('et_builder_wc_product_before_render_layout');

				DiviRocket::the_content();

				do_action('et_builder_wc_product_after_render_layout');
			}
        }

        public static function filterValueOff()
        {
            return 'off';
        }

        public static function mustBeRendering()
        {
            self::$isRendering || die(esc_html__('Access in this context is prohibited', 'divi_rocket'));
        }

        public static function loadShortcode($overrideOutput, $shortcodeTag)
        {
            if (!self::$isBuilderSetup) {
                self::setupBuilder();
            }

            //ob_start();

            global $shortcode_tags;
            if ( /*$shortcode_tags[ $shortcodeTag ] == '__return_false' &&*/ isset(self::$shortcodeFiles[$shortcodeTag])) {
                @include_once ET_BUILDER_DIR.'module/'.self::$shortcodeFiles[$shortcodeTag];
            }

            if (isset($shortcode_tags[$shortcodeTag])) {
                $scCallback = $shortcode_tags[$shortcodeTag];

                if ('__return_false' == $scCallback) {
                    et_builder_add_main_elements();
                } elseif (is_array($scCallback) && is_object($scCallback[0]) && !empty($scCallback->child_slug)) {
                    return self::loadShortcode($overrideOutput, $scCallback->child_slug);
                }
            }

            //ob_end_clean();

            return $overrideOutput;
        }

        public static function et_builder_add_main_elements_alt()
        {
            do_action('et_builder_ready');
            remove_all_actions('et_builder_ready');
        }

        public static function autoload($C)
        {
            if ('ET_Builder_' == substr($C, 0, 11)) {
                if ('Module_' == substr($C, 11, 7)) {
                    if ('Woocommerce_' == substr($C, 18, 12)) {
						$moduleFile = ET_BUILDER_DIR.'module/woocommerce/'.str_replace('_', '', substr($C, 30)).'.php';
                    } elseif ('Type_' == substr($C, 18, 5)) {
                        $moduleFile = ET_BUILDER_DIR.'module/type/'.str_replace('_', '', substr($C, 23)).'.php';
                    } elseif ('Helper_' == substr($C, 18, 7)) {
                        $h = substr($C, 25);

                        $moduleFile = ET_BUILDER_DIR.'module/helpers/'.('Woocommerce_Modules' == $h ? 'WooCommerceModules' : str_replace('_', '', $h)).'.php';
                    } else {
                        $moduleFile = ET_BUILDER_DIR.'module/'.str_replace('_', '', substr($C, 18)).'.php';
                    }

					if ( file_exists($moduleFile) ) {
						include_once $moduleFile;
					}
                }
            }
        }

        public static function loadBuilderOnAjax($requests)
        {
            if (empty($requests['action'])) {
                $requests['action'] = ['ds_return_section'];
            } else {
                $requests['action'][] = 'ds_return_section';
            }

            return $requests;
        }

        public static function stopBuilderLoadOnAjax()
        {
            //remove_all_actions('admin_init');

            remove_action('init', 'et_setup_builder', 0);
            self::$isBuilderSetup = false;
        }

        public static function interceptTemplate($file)
        {
            /*
            Strategy:
            - Cached template files are saved using an MD5? hash of the template path as the filename
            - Cached template files should include the full path at start in case of MD5 collision
            - This hook does not exist for functions that retrieve the header, template parts, etc. but we can modify
              such calls in the page templates to call our own functions instead
            */
            $hash = md5($file);

            if (!self::isTemplateCached($hash)) {
                include_once self::$pluginDirectory.'includes/DiviSpaceTemplateCacher.php';
                new DiviSpaceTemplateCacher($file, self::$templatesCachePath.$hash.'.php');
            }

            return self::$templatesCachePath.$hash.'.php';

            return $file;
        }

        /*
        public static function onParseQuery($query)
        {
            global $wp;
            $et_builder_enabled = filter_input(INPUT_GET, 'et_fb', FILTER_SANITIZE_STRING);
            $current_url = home_url(add_query_arg([], $wp->request));
            // if this is a page, and the queried object id is not empty, and the builder isn't enabled, then show the cached page, otherwise we will not be able to run the visual builder.

            if (false === array_search($current_url, self::$array_of_urls_to_exclude_lazy_loading) && $query->is_page && !empty($query->queried_object_id) && null === $et_builder_enabled && false === array_search($current_url.'/', self::$array_of_urls_to_exclude_lazy_loading) && $query->is_page && !empty($query->queried_object_id) && null === $et_builder_enabled) {
                $postId = $query->queried_object_id;
                $current_user = wp_get_current_user();
                $permissions = DiviRocket::_get_permission_object();
                $current_user_id = (isset($current_user->data->ID)) ? (int) $current_user->data->ID : 0;
                // Check if user is logged in. If user is logged in, check to see if the user's role is in the array of roles to ignore caching. If the user is not logged in, just run the check.
                if (0 < (int) $current_user_id && false !== $permissions::shouldWeCache($current_user->roles)) {
                    if (self::isCached($postId) && empty($_GET['dr_disable'])) {
                        self::renderFromCache($postId);
                    }
                    exit;
                }
                if (0 < (int) $current_user_id && false === $permissions::shouldWeCache($current_user->roles)) {
                } elseif (0 === $current_user_id) {
                    if (self::isCached($postId) && empty($_GET['dr_disable'])) {
                        self::renderFromCache($postId);
                    }
                    exit;
                }
            }
        }
        */

        public static function onPostSaved($postId, $post, $update)
        {
            if (defined('ET_THEME_BUILDER_BODY_LAYOUT_POST_TYPE')) {
                self::$postDependencies = [
                    ET_THEME_BUILDER_HEADER_LAYOUT_POST_TYPE,
                    ET_THEME_BUILDER_BODY_LAYOUT_POST_TYPE,
                    ET_THEME_BUILDER_FOOTER_LAYOUT_POST_TYPE,
                ];

                if (in_array($post->post_type, self::$postDependencies)) {
                    $rev = get_post_meta($postId, 'DiviRocket_rev', true);
                    update_post_meta($postId, 'DiviRocket_rev', ++$rev);
                }
            }

            $rev = get_option('DiviRocket_rev_'.$post->post_type, 0);
            update_option('DiviRocket_rev_'.$post->post_type, ++$rev);

            // Save settings from metabox

            if ( isset($_POST['divi-rocket-metabox-nonce']) && wp_verify_nonce(sanitize_key($_POST['divi-rocket-metabox-nonce']), 'divi-rocket-metabox-save') ) {
                if (empty($_POST['divi_rocket_cache_off'])) {
                    delete_post_meta($postId, '_divi_rocket_cache_off');
                } else {
                    update_post_meta($postId, '_divi_rocket_cache_off', 1);
                }

                if (empty($_POST['divi_rocket_lazy_off'])) {
                    delete_post_meta($postId, '_divi_rocket_lazy_off');
                } else {
                    update_post_meta($postId, '_divi_rocket_lazy_off', 1);
                }

                if (empty($_POST['divi_rocket_sections_initial']) || $_POST['divi_rocket_sections_initial'] < 1) {
                    delete_post_meta($postId, '_divi_rocket_sections_initial');
                } else {
                    update_post_meta($postId, '_divi_rocket_sections_initial', (int) $_POST['divi_rocket_sections_initial']);
                }

                if (empty($_POST['divi_rocket_sections_subsequent']) || $_POST['divi_rocket_sections_subsequent'] < 1) {
                    delete_post_meta($postId, '_divi_rocket_sections_subsequent');
                } else {
                    update_post_meta($postId, '_divi_rocket_sections_subsequent', (int) $_POST['divi_rocket_sections_subsequent']);
                }
            }

            // For now, we are only doing on the fly caching, not caching on post save. The following is temporarily disabled.

            /*
            if ( false !== $update && in_array($post->post_type, self::$cachePostTypes) ) {
                include_once(self::$pluginDirectory.'includes/DiviSpacePostCacher.php');
                new DiviSpacePostCacher($postId, $post->post_content, self::$postsCachePath.$postId.'/');
            }
            */
            $fs = new WP_Filesystem_Direct(null);
            $fs->rmdir(self::$postsCachePath.((int) $postId), true);

            $depPostIds = get_posts([
                'post_type' => 'any',
                'post_status' => 'any',
                'nopaging' => true,
                'meta_query' => [
                    [
                        'key' => 'DiviRocket_dep',
                        'value' => $postId,
                    ],
                    [
                        'key' => 'DiviRocket_dep_header',
                        'value' => $postId,
                    ],
                    [
                        'key' => 'DiviRocket_dep_footer',
                        'value' => $postId,
                    ],
                    'relation' => 'OR',
                ],
                'fields' => 'ids',
            ]);

            foreach ($depPostIds as $depPostId) {
                $fs->rmdir(self::$postsCachePath.((int) $depPostId), true);
            }
        }

        public static function onCommentSaved($id, $comment)
        {
            $comment->comment_post_ID;

            if (in_array(get_post_type($comment->comment_post_ID), ['post', 'page'])) {
                $rev = get_post_meta($comment->comment_post_ID, 'DiviRocket_rev_comment', true);
                update_post_meta($comment->comment_post_ID, 'DiviRocket_rev_comment', ++$rev);
            }
        }

        public static function isCached($postId, $section = 0)
        {
			if ( !empty($_GET['dr_debug']) && current_user_can('manage_options') ) {
				self::$DEBUG = true;
				require_once(__DIR__.'/includes/DiviSpaceCacheDebug.php');
				return false;
			}

            return file_exists(self::$postsCachePath.$postId.'/'.$section.'.php');
        }

        public static function isTemplateCached($hash)
        {
            return file_exists(self::$templatesCachePath.$hash.'.php');
        }

        public static function renderFromCache($postId)
        {
            et_builder_add_main_elements();
            //include(self::$postsCachePath.$postId.'/'.'cached.php');
            $cached_sections = glob(DIVI_ROCKET_CACHE_PATH.'posts/'.$postId.'/'.$postId.'_section*');
            include DIVI_ROCKET_CACHE_PATH.'posts/'.$postId.'/header.php';
            include $cached_sections[0];
            include DIVI_ROCKET_CACHE_PATH.'posts/'.$postId.'/footer.php';
        }

        public static function setupBuilder()
        {
            self::$isBuilderSetup = true;
            $actionsToRerun = ['init', 'wp_loaded', 'wp'];

            foreach ($actionsToRerun as $action) {
                remove_all_actions($action);
            }

            et_setup_builder();

            DiviRocket_dynamicallyLoadShortcodes('wp_loaded');

            foreach ($actionsToRerun as $action) {
                do_action($action);
            }
        }

        public static function renderShortcode($shortcodeTag, $shortcodeArgs, $renderCount, $orderNum, $live = true, $shortcodeLiteralContent = null)
        {

            if (null !== $shortcodeLiteralContent) {
                $shortcodeContent = $shortcodeLiteralContent;
            } elseif ('et_pb_row' == $shortcodeTag || ('et_pb_section' == $shortcodeTag && !empty($shortcodeArgs['specialty']) && 'on' == $shortcodeArgs['specialty'])) {
                $shortcodeContent = '[DiviRocket_capture_row_globals]'.self::SHORTCODE_CONTENT_PLACEHOLDER;
            } elseif ('et_pb_row_inner' == $shortcodeTag) {
                $shortcodeContent = '[DiviRocket_capture_row_inner_globals]'.self::SHORTCODE_CONTENT_PLACEHOLDER;
            } else {
                $shortcodeContent = self::SHORTCODE_CONTENT_PLACEHOLDER;
            }

            global $shortcode_tags, $wp_scripts, $wp_styles;
            /*
             * Filters whether to call a shortcode callback.
             *
             * Passing a truthy value to the filter will effectively short-circuit the
             * shortcode generation process, returning that value instead.
             *
             * @since 4.7.0
             *
             * @param bool|string $return      Short-circuit return value. Either false or the value to replace the shortcode with.
             * @param string       $tag         Shortcode name.
             * @param array|string $attr        Shortcode attributes array or empty string.
             * @param array        $m           Regular expression match array.
             */
            //$renderedShortcode = apply_filters( 'pre_do_shortcode_tag', false, $shortcodeTag, $shortcodeArgs, null );

            self::loadShortcode('', $shortcodeTag);
            $scCallback = $shortcode_tags[$shortcodeTag];

            if (true || false === $renderedShortcode) {
                if ($renderCount && is_array($scCallback) && is_object($scCallback[0])) {
                    $builder_element_serialized = serialize($scCallback[0]);
                    $render_count_prefix = '_render_count";i:';
                    $render_count_start = strpos($builder_element_serialized, $render_count_prefix) + strlen($render_count_prefix);
                    if ($render_count_start) {
                        $render_count_end = strpos($builder_element_serialized, ';', $render_count_start + 1);
                        $builder_element_serialized = substr($builder_element_serialized, 0, $render_count_start)
                            .$renderCount
                            .substr($builder_element_serialized, $render_count_end);
                        $scCallback[0] = unserialize($builder_element_serialized);
                    }
                }

                if (null !== $orderNum) {
                    global $et_pb_predefined_module_index;
                    $et_pb_predefined_module_index_prev = isset($et_pb_predefined_module_index) ? $et_pb_predefined_module_index : 0;
                    $et_pb_predefined_module_index = $orderNum;
                }

                $scriptsBefore = $wp_scripts->queue;
                $stylesBefore = $wp_styles->queue;

				if (!$live) {
					$boxShadowElementsBefore = ET_Builder_Module_Field_BoxShadow::get_elements();
					$motionElementsDesktopBefore = ET_Builder_Element::$_scroll_effects_fields['desktop'];
					$motionElementsTabletBefore = ET_Builder_Element::$_scroll_effects_fields['tablet'];
					$motionElementsPhoneBefore = ET_Builder_Element::$_scroll_effects_fields['phone'];
					$stickyElementsBefore = ET_Builder_Element::$sticky_elements;
				}


			DiviRocket::$DEBUG
				&& DiviSpaceCacheDebug::setModuleMeta('scriptsBefore', $scriptsBefore)
				&& DiviSpaceCacheDebug::setModuleMeta('stylesBefore', $stylesBefore);

                $output = call_user_func($scCallback, $shortcodeArgs, $shortcodeContent, $shortcodeTag);

                $scriptsAdded = array_diff($wp_scripts->queue, $scriptsBefore);
                $stylesAdded = array_diff($wp_styles->queue, $stylesBefore);

				if (!$live) {

					$boxShadowElementsAdded = array_udiff( ET_Builder_Module_Field_BoxShadow::get_elements(), $boxShadowElementsBefore, [__CLASS__, 'arrayDiffRecursiveCallback'] );
					$motionElementsDesktopAdded = array_udiff( ET_Builder_Element::$_scroll_effects_fields['desktop'], $motionElementsDesktopBefore, [__CLASS__, 'arrayDiffRecursiveCallback'] );
					$motionElementsTabletAdded = array_udiff( ET_Builder_Element::$_scroll_effects_fields['tablet'], $motionElementsTabletBefore, [__CLASS__, 'arrayDiffRecursiveCallback'] );
					$motionElementsPhoneAdded = array_udiff( ET_Builder_Element::$_scroll_effects_fields['phone'], $motionElementsPhoneBefore, [__CLASS__, 'arrayDiffRecursiveCallback'] );
					$stickyElementsAdded = array_udiff( ET_Builder_Element::$sticky_elements, $stickyElementsBefore, [__CLASS__, 'arrayDiffRecursiveCallback'] );
				}



			DiviRocket::$DEBUG
				&& DiviSpaceCacheDebug::setModuleMeta('scriptsAfter', $wp_scripts->queue)
				&& DiviSpaceCacheDebug::setModuleMeta('stylesAfter', $wp_styles->queue);

                $wp_scripts->queue = $scriptsBefore;

                $et_pb_predefined_module_index = $et_pb_predefined_module_index_prev;

                /**
                 * Filters the output created by a shortcode callback.
                 *
                 * @since 4.7.0
                 *
                 * @param string       $output Shortcode output.
                 * @param string       $tag    Shortcode name.
                 * @param array|string $attr   Shortcode attributes array or empty string.
                 * @param array        $m      Regular expression match array.
                 */
                $renderedShortcode = apply_filters('do_shortcode_tag', $output, $shortcodeTag, $shortcodeArgs, null);

                // Plugin compatibility
                $renderedShortcode = self::runPhpCompat('renderShortcode', $renderedShortcode);
            }

            /*if ( "et_pb_column" == $shortcodeTag && isset($shortcodeArgs['empty_class'])) {
                $html_empty_column_container = new DOMDocument;
                $html_empty_column_container->loadHTML( $renderedShortcode );
                $html_div = $html_empty_column_container->getElementsByTagName('div');
                foreach( $html_div as $div_element ) {
                    $div_element->setAttribute( 'class', $div_element->getAttribute('class') . ' et_pb_column_empty');
                }
                $renderedShortcode = $html_empty_column_container->saveHTML();
            }*/

            if (empty(self::$lastStylesArray)) {
                self::$lastStylesArray = [[], []];
            }

            $css = '';
		$isPostStyleKey = ET_Builder_Element::get_style_key() == 'post';

		for ($i = 0; $i < 2; ++$i) {
			$allStylesFlat = explode("\n", ET_Builder_Element::get_style($i).( $isPostStyleKey ? '' : ET_Builder_Element::get_style($i, 'post') ) );

                $media_queries = ET_Builder_Element::get_media_quries();
                foreach ($media_queries as &$mQ) {
                    $mQ .= ' {';
                }

                $currentMQ = 'general';
                $allStyles = [$currentMQ => []];
                $inDeclaration = false;
                foreach ($allStylesFlat as $line) {
                    $line = trim($line);
                    if ($line) {
                        $lineOpenBracketPos = strpos($line, '{');
                        if (false !== $inDeclaration) {
                            if ('}' == $line[strlen($line) - 1]) {
                                $allStyles[$currentMQ][$inDeclarationSelector] = trim($inDeclaration.' '.substr($line, 0, -1));
                                $inDeclaration = false;
                            } else {
                                $inDeclaration .= $line;
                            }
                        } elseif ($lineOpenBracketPos) {
                            if (in_array($line, $media_queries)) {
                                $currentMQ = substr($line, 0, -2);
                                $allStyles[$currentMQ] = [];
                            } elseif ('}' == $line[strlen($line) - 1]) {
                                $allStyles[$currentMQ][trim(substr($line, 0, $lineOpenBracketPos))] = trim(substr($line, $lineOpenBracketPos + 1, -1));
                            } else {
                                $inDeclaration = trim(substr($line, $lineOpenBracketPos + 1));
                                $inDeclarationSelector = trim(substr($line, 0, $lineOpenBracketPos));
                            }
                        }
                    }
                }

                if (empty(self::$lastStylesArray[$i])) {
                    $currentStyles = $allStyles;
                } else {
                    $currentStyles = [];
                    foreach ($allStyles as $mQ => $styles) {
                        if (empty(self::$lastStylesArray[$i][$mQ])) {
                            $currentStyles[$mQ] = $styles;
                        } else {
                            $currentStyles[$mQ] = array_diff_key($styles, self::$lastStylesArray[$i][$mQ]);

                            if (empty($currentStyles[$mQ])) {
                                unset($currentStyles[$mQ]);
                            }
                        }
                    }
                }
                self::$lastStylesArray[$i] = $allStyles;
                $css .= self::get_style($currentStyles);
            }

            // This was a working implementation but has been disabled and replaced with the above
            // due to incompatibility with Divi 3.X

            /*
            for ($i = 0; $i < 2; ++$i) {
                $allStyles = ET_Builder_Element::get_style_array($i);
                if (empty(self::$lastStylesArray[$i])) {
                    $currentStyles = $allStyles;
                } else {
                    $currentStyles = array();
                    foreach ($allStyles as $mQ => $styles) {
                        if (empty(self::$lastStylesArray[$i][$mQ])) {
                            $currentStyles[$mQ] = $styles;
                        } else {
                            $currentStyles[$mQ] = array_diff_key($styles, self::$lastStylesArray[$i][$mQ]);

                            if (empty($currentStyles[$mQ])) {
                                unset($currentStyles[$mQ]);
                            }
                        }
                    }
                }
                self::$lastStylesArray[$i] = $allStyles;
                $css .= self::get_style($currentStyles);
            }
            */

            $data = [
				'css' => $css,
				'html' => $renderedShortcode,
				'scripts' => $scriptsAdded,
				'styles' => $stylesAdded,
			];

			if (!$live) {
				$data['boxShadowElements'] = $boxShadowElementsAdded;
				$data['motionElementsDesktop'] = $motionElementsDesktopAdded;
                $data['motionElementsTablet'] = $motionElementsTabletAdded;
                $data['motionElementsPhone'] = $motionElementsPhoneAdded;
                $data['stickyElements'] = $stickyElementsAdded;
			}

            if ($live) {
                echo et_core_esc_previously($renderedShortcode);
                self::$footerCss .= $css;
            } elseif (isset($scCallback[0]->slug)) {

				// To do: determine if the render_count argument is even needed anymore
                if (preg_match('/'.preg_quote($scCallback[0]->slug).'_([0-9]+)[\\s"\']/', $renderedShortcode, $found)) {
                    $data['render_count'] = $found[1];
                }

				/*
                $orderClass = ET_Builder_Element::get_module_order_class($scCallback[0]->slug);
                $orderNum = '';
                $orderClassPos = strlen($scCallback[0]->slug);
                $orderClassLen = strlen($orderClass);

                while (++$orderClassPos < $orderClassLen && is_numeric($orderClass[$orderClassPos])) {
                    $orderNum .= $orderClass[$orderClassPos];
                }
				*/

			//if ( strlen($orderNum) ) {
                    $data['order_num'] = $orderNum;
                //}
            }

            return $data;
        }

		public static function arrayDiffRecursiveCallback($a1, $a2) {

			// Note: this function is not designed to handle arrays containing objects

			if ( is_array($a1) && is_array($a2) ) {

				if (count($a1) != count($a2)) {
					return false;
				}

				foreach ($a1 as $k => $v) {
					if (!isset($a2[$k]) || !self::arrayDiffRecursiveCallback($v, $a2[$k])) {
						return false;
					}
				}

				return true;
			}

			return $a1 === $a2;

		}

        public static function renderContentShortcode($shortcodeTag, $shortcodeArgs, $live = true, $shortcodeLiteralContent = null)
        {
            $shortcodeContent = $shortcodeLiteralContent;

            global $shortcode_tags, $wp_scripts;
            /**
             * Filters whether to call a shortcode callback.
             *
             * Passing a truthy value to the filter will effectively short-circuit the
             * shortcode generation process, returning that value instead.
             *
             * @since 4.7.0
             *
             * @param bool|string  $return Short-circuit return value. Either false or the value to replace the shortcode with.
             * @param string       $tag    Shortcode name.
             * @param array|string $attr   Shortcode attributes array or empty string.
             * @param array        $m      Regular expression match array.
             */
            $renderedShortcode = apply_filters('pre_do_shortcode_tag', false, $shortcodeTag, $shortcodeArgs, null);

            if (false === $renderedShortcode) {
                if (empty($shortcode_tags[$shortcodeTag]) || '__return_false' == $shortcode_tags[$shortcodeTag]) {
                    self::loadShortcode('', $shortcodeTag);
                }

                $output = call_user_func($shortcode_tags[$shortcodeTag], $shortcodeArgs, $shortcodeContent, $shortcodeTag);

                /**
                 * Filters the output created by a shortcode callback.
                 *
                 * @since 4.7.0
                 *
                 * @param string       $output Shortcode output.
                 * @param string       $tag    Shortcode name.
                 * @param array|string $attr   Shortcode attributes array or empty string.
                 * @param array        $m      Regular expression match array.
                 */
                $renderedShortcode = apply_filters('do_shortcode_tag', $output, $shortcodeTag, $shortcodeArgs, null);
            }

            if ($live) {
                echo et_core_esc_previously($renderedShortcode);
            }

            return $renderedShortcode;
        }

        public static function renderShortcodeEnd()
        {
        }

        public static function get_style($styles_array)
        {
            if (empty($styles_array)) {
                return '';
            }

            global $et_user_fonts_queue;

            $output = '';

            if (!empty($et_user_fonts_queue)) {
                $output .= et_builder_enqueue_user_fonts($et_user_fonts_queue);
                $et_user_fonts_queue = [];
            }

            $styles_by_media_queries = $styles_array;
            $styles_count = (int) count($styles_by_media_queries);
            $media_queries_order = array_merge(['general'], array_values(ET_Builder_Element::get_media_quries()));

            // make sure styles in the array ordered by media query correctly from bigger to smaller screensize
            $styles_by_media_queries_sorted = array_merge(array_flip($media_queries_order), $styles_by_media_queries);

            foreach ($styles_by_media_queries_sorted as $media_query => $styles) {
                // skip wrong values which were added during the array sorting
                if (!is_array($styles)) {
                    continue;
                }

                $media_query_output = '';
                $wrap_into_media_query = 'general' !== $media_query;

                // sort styles by priority
                //uasort( $styles, array( 'ET_Builder_Element', 'compare_by_priority' ) );

                // get each rule in a media query
                foreach ($styles as $selector => $settings) {
                    $media_query_output .= sprintf(
                        '%3$s%4$s%1$s { %2$s }',
                        $selector,
                        $settings/*['declaration']*/,
                        "\n",
                        ($wrap_into_media_query ? "\t" : '')
                    );
                }

                // All css rules that don't use media queries are assigned to the "general" key.
                // Wrap all non-general settings into media query.
                if ($wrap_into_media_query) {
                    $media_query_output = sprintf(
                        '%3$s%3$s%1$s {%2$s%3$s}',
                        $media_query,
                        $media_query_output,
                        "\n"
                    );
                }

                $output .= $media_query_output;
            }

            return $output;
        }

        public static function getDependencyValue($dependency)
        {
            if (is_numeric($dependency)) {
                $dependencyPost = get_post($dependency);

                return $dependencyPost ? $dependencyPost->post_modified_gmt : 0;
            }
            if ('_' == $dependency[0] && '_comment_' == substr($dependency, 0, 9)) {
                return get_post_meta(substr($dependency, 9), 'DiviRocket_rev_comment', true);
            }

            return get_option('DiviRocket_rev_'.$dependency);
        }

        public static function contentDynamicShortcodeStart($shortcodeTag, $dependencies, $css, $shortcodeArgs, $renderCount, $orderNum, $shortcodeLiteralContent = null)
        {
            foreach ($dependencies as $dependency => $dependencyValue) {
                if (self::getDependencyValue($dependency) != $dependencyValue) {
                    self::maybeSimulateLoggedOut($shortcodeTag);
                    $shortcodeResult = self::renderShortcode($shortcodeTag, $shortcodeArgs, $renderCount, $orderNum, false, $shortcodeLiteralContent);
                    self::endSimulatingLoggedOut();

                    self::$contentDynamicShortcodeCurrentUpdate = (isset($shortcodeResult['html'])) ? $shortcodeResult['html'] : '';
                    self::$contentDynamicShortcodeCurrentUpdateCss = (isset($shortcodeResult['css']) ? $shortcodeResult['css'] : '');
                    self::$footerCss .= self::$contentDynamicShortcodeCurrentUpdateCss;
                    self::$contentDynamicShortcodeCurrentDependencies = $dependencies;

                    ob_start();

                    return;
                }
            }

            self::$footerCss .= $css;
        }

        public static function contentDynamicShortcodeEnd()
        {
            if (!empty(self::$contentDynamicShortcodeCurrentUpdate)) {
                if (empty(self::$contentDynamicShortcodeUpdates)) {
                    self::$contentDynamicShortcodeUpdates = [[], [], [], []];
                }
                self::$contentDynamicShortcodeUpdates[0][] = ob_get_contents();
                self::$contentDynamicShortcodeUpdates[1][] = self::$contentDynamicShortcodeCurrentUpdate;
                self::$contentDynamicShortcodeUpdates[2][] = self::$contentDynamicShortcodeCurrentDependencies;
                self::$contentDynamicShortcodeUpdates[3][] = self::$contentDynamicShortcodeCurrentUpdateCss;

                ob_end_clean();
                echo et_core_esc_previously(self::$contentDynamicShortcodeCurrentUpdate);
                self::$contentDynamicShortcodeCurrentUpdate = null;
                self::$contentDynamicShortcodeCurrentDependencies = null;
            }
        }

        // Currently, we assume that this will return true if caching is enabled and $themeBuilder is true, even if the theme builder is not in use
        public static function isCachingEnabled($post = null, $themeBuilder = false)
        {
			// phpcs:disable WordPress.Security.NonceVerification -- this function doesn't perform any actions based on user input

            $statusKey = ($post ? $post->ID : 0).($themeBuilder ? 'tb' : '');

            if (!isset(self::$cachingStatus[$statusKey])) {
                $builder_mode = filter_input(INPUT_GET, 'et_fb', FILTER_SANITIZE_STRING);
                $current_user = wp_get_current_user();
                $current_user_id = (isset($current_user->data->ID)) ? (int) $current_user->data->ID : 0;

                $is_builder_active = 'on' === get_post_meta($post->ID, '_et_pb_use_builder', true);

                self::$cachingStatus[$statusKey] =
				( is_single() || is_page() )
				&& !isset($_GET['et_blog'])
				&& !isset($_GET['product-page'])
                    && empty($_GET['dr_disable'])
                    && get_option('divi_rocket_cache_on')
                    && !get_post_meta($post->ID, '_divi_rocket_cache_off', true)
                    && (Divi_Rocket_Permissions::shouldWeCache($current_user_id ? $current_user->roles : null))
                    && 1 != (int) $builder_mode
                    && in_array($post->post_type, self::$cachePostTypes)
					&& ($is_builder_active /*|| $themeBuilder*/) // do not enable caching if theme builder is in use but the post is not created with Divi
					&& self::runPhpCompat('isCachingEnabled', true); // this only allows compat integrations to disable caching and should be run last

                foreach (self::$disableCacheShortcodes as $sc) {
                    if (strpos($post->post_content, '['.$sc)) {
                        self::$cachingStatus[$statusKey] = false;

                        break;
                    }
                }
            }

            return self::$cachingStatus[$statusKey];

			// phpcs:enable WordPress.Security.NonceVerification -- this function doesn't perform any actions based on user input
        }

        public static function isLazyLoadEnabled($post)
        {
			// phpcs:disable WordPress.Security.NonceVerification -- this function doesn't perform any actions based on user input

            if (!isset(self::$lazyLoadStatus[$post->ID])) {
                self::$lazyLoadStatus[$post->ID] =
                    !isset($_GET['et_blog'])
                    && empty($_GET['dr_ll_disable'])
                    && empty($post->post_password)
                    && self::isCachingEnabled($post)
                    && get_option('divi_rocket_lazy_on')
                    && !get_post_meta($post->ID, '_divi_rocket_lazy_off', true);

                if (self::$lazyLoadStatus[$post->ID]) {
                    include_once self::$pluginDirectory.'includes/Crawler-Detect/CrawlerDetect.php';

                    // Following code copied from Crawler Detect and modified; see includes/Crawler-Detect/LICENSE for license and copyright information

                    $CrawlerDetect = new Jaybizzle\CrawlerDetect\CrawlerDetect();

                    // Check the user agent of the current 'visitor'
                    if ($CrawlerDetect->isCrawler()) {
                        // if crawler user agent detected
                        self::$lazyLoadStatus[$post->ID] = false;
                    }

                    // End code copied from Crawler Detect
                }
            }

            return self::$lazyLoadStatus[$post->ID];

			// phpcs:enable WordPress.Security.NonceVerification -- this function doesn't perform any actions based on user input
        }

        public static function getExpectedCacheVersion()
        {
            $currentTheme = wp_get_theme();
            $currentThemeParent = $currentTheme->parent_theme;
            if ($currentThemeParent) {
                $currentThemeParent = wp_get_theme($currentThemeParent);

                return self::VERSION.'_'.$currentThemeParent->version.'_'.$currentTheme->version.'_'.$currentTheme->name;
            }

            return self::VERSION.'_'.$currentTheme->version;
        }

        public static function the_content($themeBuilderPart = null, $themeBuilderLayoutId = null, $themeBuilderFunctionArgs = null)
        {
			if ( !empty($themeBuilderPart) && !in_array($themeBuilderPart, array('header', 'body', 'footer') ) ) {
				return;
			}

			 global $post, $wp;

			// phpcs:ignore WordPress.Security.NonceVerification.Missing
            if (!empty($_POST) || empty($post) || !self::isCachingEnabled($post, $themeBuilderPart) || (isset($themeBuilderFunctionArgs[1]) && !$themeBuilderFunctionArgs[1])) {
                $themeBuilderPart
                    ? call_user_func_array('et_theme_builder_frontend_render_'.$themeBuilderPart, $themeBuilderFunctionArgs)
                    : the_content();

                return;
            }

            if (!self::$checkedVersions) {
                $expectedVersion = self::getExpectedCacheVersion();
                if (get_option('DiviRocketVer') != $expectedVersion) {
                    self::cacheClearAll();
                    update_option('DiviRocketVer', $expectedVersion);
                }
                self::$checkedVersions = true;
            }

            // We are loading from cache so no need to include content-related JS in footer

            remove_action('wp_footer', 'et_builder_get_modules_js_data');

            $lazyLoadEnabled = self::isLazyLoadEnabled($post);

            if ($themeBuilderPart) {
                $section = $themeBuilderPart;
            } elseif ($lazyLoadEnabled) {
                $section = 0;
            } else {
                $section = 'all';
                $destinationSection = ['all'];
            }

            if (self::isCached($post->ID, $section)) {
                if ($themeBuilderPart) {
                    if ($themeBuilderLayoutId) {
                        ET_Builder_Element::begin_theme_builder_layout($themeBuilderLayoutId);
                    }

                    self::$currentSectionRev = $themeBuilderLayoutId.($themeBuilderLayoutId ? '-'.get_post_meta($themeBuilderLayoutId, 'DiviRocket_rev', true) : '');
                    self::$currentSectionInvalid = false;
                }

                self::renderSection($post, $section);

                if ($themeBuilderPart) {
                    if ($themeBuilderLayoutId) {
                        ET_Builder_Element::end_theme_builder_layout();
                    }

                    if (self::$currentSectionInvalid) {
                        @unlink(self::$postsCachePath.((int) $post->ID).'/'.$themeBuilderPart.'.php');
                        self::the_content($themeBuilderPart, $themeBuilderLayoutId, $themeBuilderFunctionArgs);
                    } elseif ('footer' == $themeBuilderPart) {
                        self::$footerCreditShown = true;
                    }
                }
            } else {
                if ($themeBuilderPart) {
                    $destinationSection = [
                        $themeBuilderPart,
                    ];
                    if ($themeBuilderLayoutId) {
                        $layoutPost = get_post($themeBuilderLayoutId);
                        $contentToCache = $layoutPost->post_content;

                        foreach (self::$disableCacheShortcodes as $sc) {
                            if (strpos($contentToCache, '['.$sc)) {
                                call_user_func_array('et_theme_builder_frontend_render_'.$themeBuilderPart, $themeBuilderFunctionArgs);

                                return;
                            }
                        }

                        //C:\agswp\htdocs\dev\wp-content\themes\Divi\includes\builder\core.php
                        add_filter('et_builder_render_layout', ['DiviRocket', 'placeholderFilter'], 1);
                        add_filter('the_content', ['DiviRocket', 'placeholderFilter'], 1); // older Divi
                        ob_start();
                        call_user_func_array('et_theme_builder_frontend_render_'.$themeBuilderPart, $themeBuilderFunctionArgs);
                        $themeBuilderHtml = ob_get_clean();
                        remove_filter('et_builder_render_layout', ['DiviRocket', 'placeholderFilter'], 1);
                        remove_filter('the_content', ['DiviRocket', 'placeholderFilter'], 1); // older Divi

                        $contentPlaceholder = self::placeholderFilter();
                        $contentStart = strpos($themeBuilderHtml, $contentPlaceholder);
                        if (false !== $contentStart) {
                            $htmlBefore = substr($themeBuilderHtml, 0, $contentStart);
                            $htmlBefore = [$htmlBefore, $htmlBefore];
                            $htmlAfter = substr($themeBuilderHtml, $contentStart + strlen($contentPlaceholder));
                            $htmlAfter = [$htmlAfter, $htmlAfter];
                        }
                    } else {
                        $contentToCache = '';

                        switch ($themeBuilderPart) {
                            case 'header':

                                $htmlBefore = [];
                                ob_start();
                                get_template_part('theme-before-wrappers');
								et_theme_builder_frontend_render_common_wrappers( 'common', true );
                                echo '<?php get_template_part( \'theme-header\' ); ?>';
                                get_template_part('theme-after-header');
                                $htmlBefore[] = ob_get_clean();
                                ob_start();
                                get_template_part('theme-before-wrappers');
								et_theme_builder_frontend_render_common_wrappers( 'common', true );
                                get_template_part('theme-header');
                                get_template_part('theme-after-header');
                                $htmlBefore[] = ob_get_clean();
                                $htmlAfter = ['', ''];

                                break;
                            case 'footer':
                                $htmlBefore = [];
                                ob_start();
                                if (2 == self::$showFooterCredit) {
                                    self::addFooterCredits();
                                }
                                get_template_part('theme-after-footer');
								et_theme_builder_frontend_render_common_wrappers( 'common', false );
                                get_template_part('theme-after-wrappers');
                                $htmlBefore[] = ob_get_clean();

                                ob_start();
                                get_template_part('theme-footer');
                                $htmlBefore[] = ob_get_clean().$htmlBefore[0];

                                $htmlBefore[0] = '<?php get_template_part( \'theme-footer\' ); ?>'.$htmlBefore[0];

                                $htmlAfter = ['', ''];

                                break;
                        }
                    }

                    $htmlBefore[0] =
                        '<?php if (DiviRocket::$currentSectionRev==\''
                        .$themeBuilderLayoutId.($themeBuilderLayoutId ? '-'.get_post_meta($themeBuilderLayoutId, 'DiviRocket_rev', true) : '')
                        .'\') { ?>'
                        .$htmlBefore[0];
                    $htmlAfter[0] .= '<?php } else { DiviRocket::$currentSectionInvalid=true; } ?>';

                    $destinationSection[] = $htmlBefore;
                    $destinationSection[] = $htmlAfter;

                    if ($themeBuilderLayoutId) {
                        ET_Builder_Element::begin_theme_builder_layout($themeBuilderLayoutId);
                    }
                } else {
                    $contentToCache = $post->post_content;
                }

                include_once self::$pluginDirectory.'includes/DiviSpacePostCacher.php';
                new DiviSpacePostCacher(
                    $post->ID,
                    $contentToCache,
                    self::$postsCachePath.$post->ID.'/',
                    true,
                    isset($destinationSection) ? $destinationSection : null
                );

                if ($themeBuilderLayoutId) {
                    ET_Builder_Element::end_theme_builder_layout();
                }
            }

			// The et_pb_content_main_query function hides the default comments area if the post has a comments module
			// Currently it doesn't need the actual content to be passed in since it refers to the global post variable
			if ( !$themeBuilderPart && function_exists('et_pb_content_main_query') ) {
				// Divi\includes\builder\framework.php
				et_pb_content_main_query('');
			}
        }

        public static function get_header($name = null)
        {
            $isCachingEnabled = !empty($GLOBALS['post']) && self::isCachingEnabled($GLOBALS['post']);
            if ($isCachingEnabled) {
                add_action('wp_head', ['DiviRocket', 'renderSectionHeaderExtra'], 999);
            }

            if (has_action('get_header', 'et_theme_builder_frontend_override_header')) {
                if (function_exists('et_theme_builder_extract_head')) {
                    $tb_theme_head = '';

                    /**
                     * Slightly adjusted version of WordPress core code in order to mimic behavior.
                     *
                     * @see https://core.trac.wordpress.org/browser/tags/5.0.3/src/wp-includes/general-template.php#L33
                     */
                    $templates = [];
                    $name = (string) $name;
                    if ('' !== $name) {
                        $templates[] = "header-{$name}.php";
                    }
                    $templates[] = 'header.php';

                    // Buffer and discard the original partial forcing a require_once so it doesn't load again later.
                    $buffered = ob_start();
                    if ($buffered) {

						global $wp_filter;
						$actions = array();

                        // Skip any partial-specific actions so they don't run twice.
						$actions = et_()->array_get( $wp_filter, 'wp_head', array() );
						unset( $wp_filter[ 'wp_head' ] );

                        locate_template($templates, true, true);
                        $html = ob_get_clean();
                        $tb_theme_head = et_theme_builder_extract_head($html);

                        // Restore skipped actions.
						$wp_filter[ 'wp_head' ] = $actions;
                    }
                }

				require_once self::interceptTemplate(ET_THEME_BUILDER_DIR . "frontend-header-template.php");

            } else {
                /*
                 * Fires before the header template file is loaded.
                 *
                 * @since 2.1.0
                 * @since 2.8.0 $name parameter added.
                 *
                 * @param string|null $name Name of the specific header file to use. null for the default header.
                 */
                do_action('get_header', $name);

                $templates = [];
                $name = (string) $name;
                if ('' !== $name) {
                    $templates[] = "header-{$name}.php";
                }

                $templates[] = 'header.php';

                self::locate_template($templates, true);
            }

            if ($isCachingEnabled) {
                remove_action('wp_head', ['DiviRocket', 'renderSectionHeaderExtra']);
            }
        }

        public static function get_footer($name = null)
        {
            $isCachingEnabled = !empty($GLOBALS['post']) && self::isCachingEnabled($GLOBALS['post']);

            if ($isCachingEnabled) {
                self::renderSection($GLOBALS['post'], 'footer-extra');

			add_action(
				'wp_footer',
				array('DiviRocket', 'outputFooterCss')
			);
            }

            if (has_action('get_footer', 'et_theme_builder_frontend_override_footer')) {
                require_once self::interceptTemplate(ET_THEME_BUILDER_DIR.'frontend-footer-template.php');
            } else {
                /*
                 * Fires before the footer template file is loaded.
                 *
                 * @since 2.1.0
                 * @since 2.8.0 $name parameter added.
                 *
                 * @param string|null $name Name of the specific footer file to use. null for the default footer.
                 */
                do_action('get_footer', $name);

                $templates = [];
                $name = (string) $name;
                if ('' !== $name) {
                    $templates[] = "footer-{$name}.php";
                }

                $templates[] = 'footer.php';

                self::locate_template($templates, true);

		}
	}

	static function outputFooterCss() {
		if (self::$footerCss) {
		echo( '<style class="divi-rocket-added">'.self::$footerCss.'</style>' );
		}
	}

        public static function locate_template($template_names, $load = false, $require_once = true)
        {
            $located = '';
            foreach ((array) $template_names as $template_name) {
                if (!$template_name) {
                    continue;
                }
                if (file_exists(STYLESHEETPATH.'/'.$template_name)) {
                    $located = STYLESHEETPATH.'/'.$template_name;

                    break;
                }
                if (file_exists(TEMPLATEPATH.'/'.$template_name)) {
                    $located = TEMPLATEPATH.'/'.$template_name;

                    break;
                }
                if (file_exists(ABSPATH.WPINC.'/theme-compat/'.$template_name)) {
                    $located = ABSPATH.WPINC.'/theme-compat/'.$template_name;

                    break;
                }
            }

            if ($load && '' != $located) {
                load_template(self::interceptTemplate($located), $require_once);
            }

            return $located;
        }

        public static function placeholderFilter()
        {
            return '<div>'.self::SHORTCODE_CONTENT_PLACEHOLDER.'</div>';
        }

        public static function renderSectionHeaderExtra()
        {
            self::renderSection($GLOBALS['post'], 'header-extra');
        }

        public static function renderSection($post, $next_section)
        {
            if (!empty($post->post_password) || (is_user_logged_in() ? !current_user_can('read', $post->ID) : 'publish' != $post->post_status)) {
                echo esc_html__('You do not have permission to view this content.', 'divi_rocket');
                return;
            }

            self::$contentDynamicShortcodeUpdates = null;
            $sectionFile = self::$postsCachePath.((int) $post->ID).'/'.$next_section.'.php';

            self::$isRendering = true;
            @include $sectionFile;
            self::$isRendering = false;

            if (!empty(self::$contentDynamicShortcodeUpdates)) {
                $cached_content = file_get_contents($sectionFile);

                foreach (self::$contentDynamicShortcodeUpdates[0] as $i => $update) {
                    $updateLocation = strpos($cached_content, $update);
                    if ('?>' == substr($cached_content, $updateLocation - 2, 2)) {
                        $cached_content = str_replace(self::$contentDynamicShortcodeUpdates[0][$i], self::$contentDynamicShortcodeUpdates[1][$i], $cached_content);

                        foreach (self::$contentDynamicShortcodeUpdates[2][$i] as $dependency => &$dependencyValue) {
                            $dependencyValue = self::getDependencyValue($dependency);
                        }

                        $tagLocation = strrpos($cached_content, '<?php', $updateLocation - strlen($cached_content));

                        $dependenciesStart = strpos($cached_content, 'array', $tagLocation);
                        //$dependenciesEnd = strpos($cached_content, ')', $dependenciesStart);
                        //$cssStart = strpos($cached_content, '\'', $dependenciesEnd);
                        $cssEnd = strpos($cached_content, '/*DIVI_ROCKET_CSS_END*/', $dependenciesStart);

                        $cached_content = substr($cached_content, 0, $dependenciesStart)
                            .trim(var_export(self::$contentDynamicShortcodeUpdates[2][$i], true))
                            .','.var_export(trim(self::$contentDynamicShortcodeUpdates[3][$i]).'/*DIVI_ROCKET_CSS_END*/', true)
                            .substr($cached_content, $cssEnd + 24);
                    }
                }

                file_put_contents($sectionFile, $cached_content);
            }
        }

        public static function return_section()
        {
            $post_id = filter_input(INPUT_POST, 'divi_post_id', FILTER_SANITIZE_STRING);
            $next_section = filter_input(INPUT_POST, 'divi_next_section_id', FILTER_SANITIZE_STRING);
            if (null !== $post_id && null !== $next_section) {
                global $post, $wp_query;

                $wp_query->query(['p' => $post_id, 'post_type' => 'any']);
                $wp_query->get_posts();
                $wp_query->the_post();

                // Override the request URI so that modules don't use the admin-ajax URL for pagination etc.
                $permalink = get_permalink();
                $_SERVER['REQUEST_URI'] = substr($permalink, strpos($permalink, '/', 8));

                self::renderSection($post, $next_section);

                if (self::$footerCss) {
                    echo '<style class="divi-rocket-added">'.et_core_esc_previously( wp_strip_all_tags(self::$footerCss) ).'</style>';
                }
            }
            exit;
        }

        public static function et_theme_builder_frontend_render_header($layout_id, $layout_enabled, $template_id)
        {
            /*
             * Fires before theme builder page wrappers are output.
             * Example use case is to add opening wrapping html tags for the entire page.
             *
             * @since 4.0
             *
             * @param integer $layout_id The layout id or 0.
             * @param bool $layout_enabled
             * @param integer $template_id The template id or 0.
             */
            //do_action( 'et_theme_builder_template_before_page_wrappers', $layout_id, $layout_enabled, $template_id );

            //et_theme_builder_frontend_render_common_wrappers( 'common', true );

            /*
             * Fires before theme builder template header is output.
             * Example use case is to add opening wrapping html tags for the header and/or the entire page.
             *
             * @since 4.0
             *
             * @param integer $layout_id The layout id or 0.
             * @param bool $layout_enabled
             * @param integer $template_id The template id or 0.
             */
            //do_action( 'et_theme_builder_template_before_header', $layout_id, $layout_enabled, $template_id );

            //if ($layout_id && $layout_enabled) {
            self::the_content('header', $layout_id, [$layout_id, $layout_enabled, $template_id]);
            //}

            /*
             * Fires after theme builder template header is output.
             * Example use case is to add closing wrapping html tags for the header.
             *
             * @since 4.0
             *
             * @param integer $layout_id The layout id or 0.
             * @param bool $layout_enabled
             * @param integer $template_id The template id or 0.
             */
            //do_action( 'et_theme_builder_template_after_header', $layout_id, $layout_enabled, $template_id );
        }

        public static function et_theme_builder_frontend_render_body($layout_id, $layout_enabled, $template_id)
        {
            /**
             * Fires before theme builder template body is output.
             * Example use case is to add opening wrapping html tags for the body.
             *
             * @since 4.0
             *
             * @param int  $layout_id      The layout id or 0.
             * @param bool $layout_enabled
             * @param int  $template_id    The template id or 0.
             *
             * @return void
             */
            //do_action( 'et_theme_builder_template_before_body', $layout_id, $layout_enabled, $template_id );

            if ($layout_id && $layout_enabled) {
                self::the_content('body', $layout_id, [$layout_id, $layout_enabled, $template_id]);
            }

            /*
             * Fires after theme builder template body is output.
             * Example use case is to add closing wrapping html tags for the body.
             *
             * @since 4.0
             *
             * @param integer $layout_id The layout id or 0.
             * @param bool $layout_enabled
             * @param integer $template_id The template id or 0.
             *
             * @return void
             */
            //do_action( 'et_theme_builder_template_after_body', $layout_id, $layout_enabled, $template_id );
        }

        public static function et_theme_builder_frontend_render_footer($layout_id, $layout_enabled, $template_id)
        {
            /*
             * Fires before theme builder template footer is output.
             * Example use case is to add opening wrapping html tags for the footer.
             *
             * @since 4.0
             *
             * @param integer $layout_id The layout id or 0.
             * @param bool $layout_enabled
             * @param integer $template_id The template id or 0.
             *
             * @return void
             */
            //do_action( 'et_theme_builder_template_before_footer', $layout_id, $layout_enabled, $template_id );
            //if ($layout_id && $layout_enabled) {
            self::the_content('footer', $layout_id, [$layout_id, $layout_enabled, $template_id]);
            //}

            /*
             * Fires after theme builder template footer is output.
             * Example use case is to add closing wrapping html tags for the footer and/or the entire page.
             *
             * @since 4.0
             *
             * @param integer $layout_id The layout id or 0.
             * @param bool $layout_enabled
             * @param integer $template_id The template id or 0.
             *
             * @return void
             */
            //do_action( 'et_theme_builder_template_after_footer', $layout_id, $layout_enabled, $template_id );

            if (self::$showFooterCredit) {
                self::addFooterCredits();
            }

            //et_theme_builder_frontend_render_common_wrappers( 'common', false );

            //echo et_core_esc_previously( et_builder_get_builder_content_closing_wrapper() );

            /*
             * Fires after theme builder page wrappers are output.
             * Example use case is to add closing wrapping html tags for the entire page.
             *
             * @since 4.0
             *
             * @param integer $layout_id The layout id or 0.
             * @param bool $layout_enabled
             * @param integer $template_id The template id or 0.
             */
            //do_action( 'et_theme_builder_template_after_page_wrappers', $layout_id, $layout_enabled, $template_id );
        }

        public static function ajax_cacheClearCurrent()
        {
			if ( isset($_GET['nonce']) && isset($_GET['post']) && wp_verify_nonce( sanitize_key($_GET['nonce']), 'divi-rocket-cache-clear-'.((int) $_GET['post']) ) && self::$allowManualCacheClear && ((int) $_GET['post']) > 0 ) {
                $fs = new WP_Filesystem_Direct(null);
                $fs->rmdir(self::$postsCachePath.((int) $_GET['post']), true);
                wp_send_json_success();
            }
            wp_send_json_error();
        }

        public static function cacheClearAll()
        {
            $fs = new WP_Filesystem_Direct(null);
            $fs->rmdir(self::$postsCachePath, true);
            $fs->rmdir(self::$templatesCachePath, true);
            @mkdir(self::$postsCachePath, 0755, true);
            @mkdir(self::$templatesCachePath, 0755, true);
        }

        public static function ajax_cacheClearAll()
        {
            if (
                self::$allowManualCacheClear
                && isset($_GET['nonce']) && wp_verify_nonce(sanitize_key($_GET['nonce']), 'divi-rocket-cache-clear-all')
            ) {
                self::cacheClearAll();
                wp_send_json_success();
            }
            wp_send_json_error();
        }

        public static function maybeSimulateLoggedOut($currentShortcode)
        {
			empty(self::$shortcodes[$currentShortcode]) || self::$shortcodes[$currentShortcode] == self::SHORTCODE_DYNAMIC ? self::endSimulatingLoggedOut() : self::startSimulatingLoggedOut() ;
            return true;
        }

        public static function startSimulatingLoggedOut()
        {
            if (!self::$isSimulatingLoggedOut) {
                self::$currentUserId = wp_get_current_user()->ID;
                wp_set_current_user(0);
                self::$isSimulatingLoggedOut = true;
            }
        }

        public static function endSimulatingLoggedOut()
        {
            if (self::$isSimulatingLoggedOut) {
                wp_set_current_user(self::$currentUserId);
                self::$isSimulatingLoggedOut = false;
            }
        }

        public static function filterFooterCredits($credits)
        {
            if (!self::$footerCreditShown) {
                self::$footerCreditShown = true;

                $link = 'https://divi.space/product/divi-rocket/';
                $affiliateId = get_option('divi_rocket_affiliate_id', '');
                if (!empty($affiliateId)) {
                    $link .= '?ref='.rawurlencode($affiliateId);
                }

                if (!$credits) {
                    echo '
                        <style>
                        #divi-rocket-footer-link {
                            background:url(\''.esc_url(self::$pluginBaseUrl.'assets/images/divi_rocket_icon_50x50px.svg').'\') 3px center no-repeat;
                            background-size: 18px;
                            padding-left: 25px;
                        }
                        </style>
                    ';

                    return et_get_original_footer_credits()
                        .' | '.sprintf(
                            esc_html__('Performance powered by %sDivi Rocket%s', 'divi_rocket'),
                            '<a href="'.esc_url($link).'" target="_blank" id="divi-rocket-footer-link">',
                            '</a>'
                        );
                }
            }
            return $credits;
        }

        public static function addFooterCredits()
        {
            if (!self::$footerCreditShown) {
                self::$footerCreditShown = true;

                $link = 'https://divi.space/product/divi-rocket/';
                $affiliateId = get_option('divi_rocket_affiliate_id', '');
                if (!empty($affiliateId)) {
                    $link .= '?ref='.rawurlencode($affiliateId);
                }

                echo '
                    <style>
                    #divi-rocket-footer-credit {
                        font-size: 12px;
                        line-height: 30px;
                        white-space: nowrap;
                        text-align: center;
                    }
                    #divi-rocket-footer-link {
                        background:url(\''.esc_url(self::$pluginBaseUrl.'assets/images/divi_rocket_icon_50x50px.svg').'\') 3px center no-repeat;
                        background-size: 18px;
                        padding-left: 25px;
                        padding-top: 5px;
                        padding-bottom: 5px;
                    }
                    </style>
                    <div id="divi-rocket-footer-credit">'
                    .sprintf(
                        esc_html__('Performance powered by %sDivi Rocket%s', 'divi_rocket'),
                        '<a href="'.esc_url($link).'" target="_blank" id="divi-rocket-footer-link">',
                        '</a>'
                    )
                    .'</div>
                ';
            }
        }

        public static function runPhpCompat($hook, $data=null)
        {
            if (isset(self::$phpCompat[$hook])) {
                foreach (self::$phpCompat[$hook] as $name => $condition) {
                    switch ($condition[0]) {
                        case 'C':
                            if (!class_exists($condition[1])) {
                                continue 2;
                            }
                            break;
                        case 'F':
                            if (!function_exists($condition[1])) {
                                continue 2;
                            }
                            break;
                        default:
                            continue 2;
                    }

                    include_once self::$pluginDirectory.'compatibility/'.$name.'/'.$hook.'.php';
                    $data = apply_filters('divi_rocket_compat_'.$name.'_'.$hook, $data);
                }
            }

            return $data;
        }

        public static function adminMenu()
        {
            add_submenu_page('et_divi_options', esc_html__('Divi Rocket', 'divi_rocket'), esc_html__('Divi Rocket', 'divi_rocket'), 'manage_options', 'divi-rocket-settings', ['DiviRocket', 'adminPage'], 7);
        }

        public static function _get_permission_object()
        {
            if (isset(DiviRocket::$cache_permission_object)) {
                return DiviRocket::$cache_permission_object;
            }

            return false;
        }

        public static function adminPage()
        {
/* !etm { */
			$isActivated = divi_rocket_has_license_key();
/* } !etm */

            $user_roles = wp_roles()->get_names();
            $user_roles[] = '__guest';

            if (
/* !etm { */
				$isActivated &&
/* } !etm */
				isset($_POST['divi_rocket_nonce']) &&
				wp_verify_nonce(sanitize_key($_POST['divi_rocket_nonce']), 'divi_rocket_save_settings')
			) {

                $gzip_compression = filter_input(INPUT_POST, 'ds-divi-rocket-gzip-compression', FILTER_SANITIZE_STRING);
                $rocket_revisions = filter_input(INPUT_POST, 'ds-divi-rocket-post_revisions', FILTER_SANITIZE_STRING);
                $enable_cache_control_expiration = filter_input(INPUT_POST, 'divi_rocket_enable_cache_expiration', FILTER_SANITIZE_STRING);
                $cache_control_expiry_val = filter_input(INPUT_POST, 'divi_rocket_default_expiry_val', FILTER_SANITIZE_NUMBER_INT);
                $divi_rocket_lazy_loading_urls = filter_input(INPUT_POST, 'divi_rocket_exclude_lazy_loading', FILTER_SANITIZE_STRING);
                $divi_rocket_exclude_header_urls = filter_input(INPUT_POST, 'divi_rocket_exclude_headers', FILTER_SANITIZE_STRING);
                $ds_divi_pingback = filter_input(INPUT_POST, 'ds_divi_pingback', FILTER_SANITIZE_STRING);

                if ($gzip_compression) {
                    DiviRocketCacheRules::write_gzip_rules();
                    update_option('divi_rocket_gzip_compression', 1);
                } else {
                    DiviRocketCacheRules::erase_gzip_rules();
                    delete_option('divi_rocket_gzip_compression');
                }

                if (null !== $ds_divi_pingback && 'closed' == $ds_divi_pingback || null !== $ds_divi_pingback && 'open' == $ds_divi_pingback) {
                    update_option('default_ping_status', $ds_divi_pingback);
                }

                if (null !== $rocket_revisions) {
                    update_option('ds-divi-rocket-revisions', (int) $rocket_revisions);
                }

                if ($cache_control_expiry_val != get_option('divi_rocket_default_expiry_val')) {
                    update_option('divi_rocket_default_expiry_val', (int) $cache_control_expiry_val);
                    $cache_control_expiry_val_updated = true;
                }

                if ($divi_rocket_exclude_header_urls != get_option('divi_rocket_exclude_header_urls')) {
                    if ($divi_rocket_exclude_header_urls) {
                        update_option('divi_rocket_exclude_header_urls', $divi_rocket_exclude_header_urls);
                    } else {
                        delete_option('divi_rocket_exclude_header_urls');
                    }

                    $divi_rocket_exclude_header_urls_updated = true;
                }

                if (!empty($enable_cache_control_expiration)) {
                    $cacheExpirationWasOff = !((int) get_option('divi_rocket_enable_cache_expiration'));

                    if ($cacheExpirationWasOff) {
                        update_option('divi_rocket_enable_cache_expiration', 1);
                    }

                    if ($cacheExpirationWasOff
                        || !empty($divi_rocket_exclude_header_urls_updated)
                        || !empty($cache_control_expiry_val_updated)
                    ) {
                        DiviRocketCacheRules::erase_cache_control_rules();

                        DiviRocketCacheRules::write_cache_control_rules(
                            (0 < (int) $cache_control_expiry_val ? $cache_control_expiry_val : 7) * 86400,
                            explode("\n", $divi_rocket_exclude_header_urls)
                        );
                    }

                } elseif (empty($enable_cache_control_expiration) && ((int) get_option('divi_rocket_enable_cache_expiration'))) {
                    if (DiviRocketCacheRules::erase_cache_control_rules()) {
                        update_option('divi_rocket_enable_cache_expiration', 0);
                    }
                }

                if (empty($_POST['divi_rocket_cache_on']) && get_option('divi_rocket_cache_on')) {
                    delete_option('divi_rocket_cache_on');
                    $clearCache = true;
                } elseif (!empty($_POST['divi_rocket_cache_on']) && !get_option('divi_rocket_cache_on')) {
                    update_option('divi_rocket_cache_on', 1);
                    $clearCache = true;
                }

                if (empty($_POST['divi_rocket_ignore_caching_rules'])) {
                    delete_option('divi_rocket_ignore_caching_rules');
                } else {
					$ignore_caching_rules = array_map('sanitize_text_field', wp_unslash($_POST['divi_rocket_ignore_caching_rules'])); // provided by Elegant Themes code review
                    update_option(
                        'divi_rocket_ignore_caching_rules',
						array_intersect( $ignore_caching_rules, $user_roles)
                    );
                }

                if (empty($_POST['divi_rocket_lazy_on']) && get_option('divi_rocket_lazy_on')) {
                    delete_option('divi_rocket_lazy_on');
                    $clearCache = true;
                } elseif (!empty($_POST['divi_rocket_lazy_on']) && !get_option('divi_rocket_lazy_on')) {
                    update_option('divi_rocket_lazy_on', 1);
                    $clearCache = true;
                }

				if ( isset($_POST['divi_rocket_sections_initial']) && isset($_POST['divi_rocket_sections_subsequent']) ) {
					$sections_initial = (int) $_POST['divi_rocket_sections_initial'];
					$sections_subsequent = (int) $_POST['divi_rocket_sections_subsequent'];

					if (2 == $sections_initial || $sections_initial <= 0) {
						delete_option('divi_rocket_sections_initial');
					} else {
						update_option('divi_rocket_sections_initial', $sections_initial);
					}

					if (2 == $sections_subsequent || $sections_subsequent <= 0) {
						delete_option('divi_rocket_sections_subsequent');
					} else {
						update_option('divi_rocket_sections_subsequent', $sections_subsequent);
					}
				}

				if ( isset($_POST['divi_rocket_footer_credit']) ) {

					if ($_POST['divi_rocket_footer_credit'] != get_option('divi_rocket_footer_credit')) {
						$clearCache = true;
					}

					if (1 == $_POST['divi_rocket_footer_credit']) {
						delete_option('divi_rocket_footer_credit');
					} elseif (2 == $_POST['divi_rocket_footer_credit']) {
						update_option('divi_rocket_footer_credit', 2);
					} else {
						update_option('divi_rocket_footer_credit', 0);
					}

				}

				if ( isset($_POST['divi_rocket_affiliate_id']) ) {

					$affiliateId = sanitize_text_field( wp_unslash($_POST['divi_rocket_affiliate_id']) );


					if ($affiliateId != get_option('divi_rocket_affiliate_id', '')) {
						$clearCache = true;
					}

					if (empty($affiliateId)) {
						delete_option('divi_rocket_affiliate_id');
					} else {
						update_option('divi_rocket_affiliate_id', $affiliateId);
					}

				}

                if (!empty($clearCache)) {
                    DiviRocket::cacheClearAll();
                }

                $message = esc_html__('Your Divi Rocket settings have been saved!', 'divi_rocket');
                $messageType = 'success';
            }

			// Need to sort this out a bit better yet

            /*
            $optimization_tasks = get_transient( 'divi_rocket_tasks' );
            $result = filter_input( INPUT_GET, 'divi_optimize_result', FILTER_SANITIZE_STRING );

            if ( false !== $optimization_tasks && is_array( $optimization_tasks ) ) {
                $message = 'The selected tasks have been run. See each of the individual tasks you selected for more detailed information.';
                $messageType = 'success';
            }
            */

            $divi_rocket_exclude_header_urls = get_option('divi_rocket_exclude_header_urls');
            $pingback_status = get_option('default_ping_status');
            $footer_credit = get_option('divi_rocket_footer_credit', 0); ?>

            <div id="divi-rocket-settings-container">

                <?php if (!empty($message)) { ?>
                    <p class="divi-rocket-notification divi-rocket-notification-<?php echo et_core_intentionally_unescaped( $messageType, 'fixed_string' ); ?>"><?php echo et_core_esc_previously($message); ?></p>
                <?php } ?>

                <div id="divi-rocket-settings">

			<?php
/* !etm { */
			if ($isActivated) { //redone
/* } !etm */
			?>
                    <form method="post">
			<?php
/* !etm { */
			}
/* } !etm */
			?>

                        <div id="divi-rocket-settings-header">
                            <div class="divi-rocket-settings-logo">
                                <img src="<?php echo esc_url(plugin_dir_url(__FILE__).'assets/images/divi_rocket_icon_50x50px.svg'); ?>">
                                <h1><?php esc_html_e('Divi', 'divi-rocket'); ?>  Rocket</h1>
                            </div>
                            <div id="divi-rocket-settings-header-links">
                                <a id="divi-rocket-settings-header-link-settings" href=""><?php echo esc_html__('Settings', 'divi_rocket'); ?></a>
                                <a id="divi-rocket-settings-header-link-support" href="https://support.aspengrovestudios.com/article/569-divi-rocket-plugin" target="_blank"><?php echo esc_html__('Support', 'divi_rocket'); ?></a>
                            </div>
                        </div>

                        <ul id="divi-rocket-settings-tabs">

							<?php
/* !etm { */
							if ($isActivated) {
/* } !etm */
							?>
                            <li class="divi-rocket-settings-active"><a href="#tasks"><?php echo esc_html__('Tasks', 'divi_rocket'); ?></a></li>
                            <li><a href="#caching-browser"><?php echo esc_html__('Browser Caching', 'divi_rocket'); ?></a></li>
                            <li><a href="#caching-server"><?php echo esc_html__('Server Caching', 'divi_rocket'); ?></a></li>
                            <li><a href="#lazy-loading"><?php echo esc_html__('Lazy Loading', 'divi_rocket'); ?></a></li>
                            <li><a href="#compression"><?php echo esc_html__('Compression', 'divi_rocket'); ?></a></li>
                            <li><a href="#specs"><?php echo esc_html__('System Specifications', 'divi_rocket'); ?></a></li>
                            <li><a href="#misc"><?php echo esc_html__('Miscellaneous', 'divi_rocket'); ?></a></li>
							<?php
/* !etm { */
							}
/* } !etm */
							?>
<?php
/* !etm { */
?>
							<li><a href="#about"<?php if (!$isActivated) { ?> class="divi-rocket-settings-active"<?php } ?>><?php echo esc_html__('About & License Key', 'divi_rocket'); ?></a></li>
<?php
/* } !etm */
?>
                        </ul>

                        <div id="divi-rocket-settings-tabs-content">

					<?php
/* !etm { */
					if ($isActivated) { //redone
/* } !etm */
					?>

                            <div id="divi-rocket-settings-tasks" class="divi-rocket-settings-active">
                                <?php
                                $DiviRocket_Optimizations = new DiviRocket_Optimizations();
            $DiviRocket_Optimizations->prepare_items();
            $DiviRocket_Optimizations->display();
            $ignored_user_roles = get_option('divi_rocket_ignore_caching_rules', []); ?>
                            </div>

                            <div id="divi-rocket-settings-caching-browser">

                                <div class="divi-rocket-settings-box">
                                    <label>
                                        <span><?php echo esc_html__('Enable Browser Caching:', 'divi_rocket'); ?></span>
                                        <input type="checkbox" name="divi_rocket_enable_cache_expiration" value="1" <?php if (false !== get_option('divi_rocket_enable_cache_expiration') && 1 == (int) get_option('divi_rocket_enable_cache_expiration')) { ?> checked="checked" <?php } ?> >
                                    </label>

                                    <div class="divi-rocket-settings-tooltip">
                                        <div class="divi-rocket-settings-tooltiptext">
                                            <span><?php echo esc_html__('Help', 'divi_rocket'); ?></span>
                                            <?php echo esc_html__('Enable this option to send a Cache Control header for certain static resources (*.jpg, *.jpeg, *.png, *.gif, *.svg, *.js, *.css, *.pdf, *.ico, *.swf, *.flv) to tell the browser to cache them for a set amount of time. Note: These options currently work with the Apache web server only.', 'divi_rocket'); ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="divi-rocket-settings-box">
                                    <label>
                                        <span><?php echo esc_html__('Cache Control Expiration:', 'divi_rocket'); ?></span>
                                        <input type="number" name="divi_rocket_default_expiry_val" step="1" min="0" placeholder="7" <?php if ( 0 < (int) get_option('divi_rocket_default_expiry_val') ) echo et_core_intentionally_unescaped('value="'.((int) get_option('divi_rocket_default_expiry_val')).'"', 'html') ; ?>>
                                    </label>

                                    <div class="divi-rocket-settings-tooltip">
                                        <div class="divi-rocket-settings-tooltiptext">
                                            <span><?php echo esc_html__('Help', 'divi_rocket'); ?></span>
                                            <?php echo esc_html__('Enter the number of days that the browser cache should be valid for.', 'divi_rocket'); ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="divi-rocket-settings-box">
                                    <label>
                                        <span><?php echo esc_html__('URLs to not send cache headers:', 'divi_rocket'); ?></span>
                                        <textarea name="divi_rocket_exclude_headers" cols="70" rows="15"><?php echo esc_html(trim($divi_rocket_exclude_header_urls)); ?></textarea>
                                    </label>
                                    <div class="divi-rocket-settings-tooltip">
                                        <div class="divi-rocket-settings-tooltiptext">
                                            <span><?php echo esc_html__('Help', 'divi_rocket'); ?></span>
                                            <?php echo esc_html__('Enter each URL on its own line. Only include the part of the URL following the domain name (no http://, etc.). An asterisk (*) in a URL represents any one or more characters.', 'divi_rocket'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="divi-rocket-settings-caching-server">
                                <div class="divi-rocket-settings-box">
                                    <label>
                                        <span><?php echo esc_html__('Enable server-side caching:', 'divi_rocket'); ?></span>
                                        <input type="checkbox" name="divi_rocket_cache_on" value="1"<?php if (get_option('divi_rocket_cache_on')) echo ' checked'; ?>>
                                    </label>
                                    <div class="divi-rocket-settings-tooltip">
                                        <div class="divi-rocket-settings-tooltiptext">
                                            <span><?php echo esc_html__('Help', 'divi_rocket'); ?></span>
                                            <?php echo esc_html__('The Server Caching tab deals with the static HTML caching provided by the plugin.   This option enables the core caching feature and it\'s the one that provides the most benefits to your site performance. The server-side caching can be enabled or disabled. This will, in turn, enable/disable the static file generation on your server.', 'divi_rocket'); ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="divi-rocket-settings-box">
                                    <div class="divi-rocket-settings-cb-list">
                                        <label class="divi-rocket-settings-cb-list-title"><span><?php echo esc_html__('Disable server-side caching for selected user roles:', 'divi_rocket'); ?></span></label>
                                        <div class="divi-rocket-settings-cb-list-content">
                                            <?php foreach ($user_roles as $role) { ?>
                                                <label class="divi-rocket-settings-cb-list-item">
                                                    <input type="checkbox" name="divi_rocket_ignore_caching_rules[]" value="<?php echo esc_html($role); ?>"<?php if (in_array($role, $ignored_user_roles)) echo ' checked'; ?>>
                                                    <span><?php echo  '__guest' == $role ? 'Non-logged-in users' : esc_html(translate_user_role($role)); ?></span>
                                                </label>
                                            <?php } ?>
                                            <div class="divi-rocket-settings-tooltip">
                                                <div class="divi-rocket-settings-tooltiptext">
                                                    <span><?php echo esc_html__('Help', 'divi_rocket'); ?></span>
                                                    <?php echo esc_html__('Specify which users\' roles are allowed to have server-side caching.  By clicking on the checkboxes of each group under the Disable server-side caching for selected user roles you can prevent server-side caching from running on those roles while retaining caching on roles not being selected. ', 'divi_rocket'); ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="divi-rocket-settings-lazy-loading">
                                <p>
                                    <?php echo esc_html__('Lazy loading is an experimental feature and we do not recommend using it on production sites in this version without extensive testing.', 'divi_rocket'); ?>
                                    <br> <br>
                                    <?php echo esc_html__('Note that updating a lazy loaded page, post, or product while a user is viewing it may result in duplicate or missing content if sections were added, removed, or re-ordered since the initial load of the page and the user triggers further lazy loading by scrolling.', 'divi_rocket'); ?>
                                </p><br>
                                <div class="divi-rocket-settings-box">
                                    <label>
                                        <span><?php echo esc_html__('Enable content lazy loading:', 'divi_rocket'); ?></span>
                                        <input type="checkbox" name="divi_rocket_lazy_on" value="1" <?php if (get_option('divi_rocket_lazy_on')) { ?> checked="checked" <?php } ?> >
                                    </label>
                                    <div class="divi-rocket-settings-tooltip">
                                        <div class="divi-rocket-settings-tooltiptext">
                                            <span><?php echo esc_html__('Help', 'divi_rocket'); ?></span>
                                            <?php echo esc_html__('Lazy loading requires that server-side caching be enabled. This setting will have no effect if server-side caching is disabled in the Server Caching tab (either globally or for the current user role).', 'divi_rocket'); ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="divi-rocket-settings-box">
                                    <label>
                                        <span><?php echo esc_html__('Number of sections to load initially:', 'divi_rocket'); ?></span>
                                        <input type="number" name="divi_rocket_sections_initial" step="1" min="1" <?php echo 'value="'.((int) get_option('divi_rocket_sections_initial', 2)).'"'; ?>>
                                    </label>
                                    <div class="divi-rocket-settings-tooltip">
                                        <div class="divi-rocket-settings-tooltiptext">
                                            <span><?php echo esc_html__('Help', 'divi_rocket'); ?></span>
                                            <?php echo esc_html__('The numbers of sections to load initially will instruct the lazy load core module how many sections to load initially without the lazy load while leaving the rest to lazy load.  The default parameter of 4 can be customized but it\'s a safe starting point.  If you want to speed up loading and you have fewer sections you could try reducing this parameter while on big pages you could try increasing it a little more. ', 'divi_rocket'); ?>
                                        </div>
                                    </div>
                                </div>

                                <div class="divi-rocket-settings-box">
                                    <label>
                                        <span><?php echo esc_html__('Number of sections to load per subsequent request:', 'divi_rocket'); ?></span>
                                        <input type="number" name="divi_rocket_sections_subsequent" step="1" min="1" <?php echo 'value="'.((int) get_option('divi_rocket_sections_subsequent', 2)).'"'; ?>>
                                    </label>
                                    <div class="divi-rocket-settings-tooltip">
                                        <div class="divi-rocket-settings-tooltiptext">
                                            <span><?php echo esc_html__('Help', 'divi_rocket'); ?></span>
                                            <?php echo esc_html__('Instruct the plugin how many subsequent sections to load by configuring the number of sections to load per subsequent request parameter. ', 'divi_rocket'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="divi-rocket-settings-compression">
                                <div class="divi-rocket-settings-box">
                                    <label>
                                        <span><?php echo esc_html__('GZip Compression:', 'divi_rocket'); ?></span>
                                        <input type="checkbox" name="ds-divi-rocket-gzip-compression" value="1" <?php if ( /*1 == ini_get( 'zlib.output_compression' ) || 1 ==*/ (int) get_option('divi_rocket_gzip_compression')) { ?> checked="checked" <?php } ?>>
                                    </label>
                                    <div class="divi-rocket-settings-tooltip">
                                        <div class="divi-rocket-settings-tooltiptext">
                                            <span><?php echo esc_html__('Help', 'divi_rocket'); ?></span>
                                            <?php echo esc_html__('The compression tab allows you to enable gzip compression, which applies to a predetermined set of resource types. Both the browser caching and compression tabs currently work by writing to the WP .htaccess file.  Current support is limited to web servers that can read and execute htaccess files, such as Apache.', 'divi_rocket'); ?> <br/> <br/>
                                            <?php echo esc_html__('This parameter will depend on your hosting configuration. Some hosting services bring this option enabled by default so it may not change while tweaking this option.', 'divi_rocket'); ?> <br/> <br/>
                                            <?php echo esc_html__('Note: This option currently works with the Apache web server only.', 'divi_rocket'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="divi-rocket-settings-specs">
                                <?php
                                require_once plugin_dir_path(__FILE__).'modules/DiviRocketSupportCenter'.(class_exists('ET_Core_SupportCenter') ? '' : 'Old').'.php';
            $support_center = new Divi_Rocket_ET_Core_SupportCenter();
            $support_center->add_system_report(); ?>
                            </div>

                            <div id="divi-rocket-settings-misc">
                                <div class="divi-rocket-settings-box">
                                    <label>
                                        <span><?php echo esc_html__('Post Revisions Limit:', 'divi_rocket'); ?></span>
                                        <input type="text" name="ds-divi-rocket-post_revisions" value="<?php echo (int) ( (get_option('ds-divi-rocket-revisions')) ? get_option('ds-divi-rocket-revisions') : 0 ); ?>">
                                    </label>
                                    <div class="divi-rocket-settings-tooltip">
                                        <div class="divi-rocket-settings-tooltiptext">
                                            <span><?php echo esc_html__('Help', 'divi_rocket'); ?></span>
                                            <?php echo esc_html__('Use this setting to specify the maximum number of revisions that WordPress should keep for a post.', 'divi_rocket'); ?></div>
                                    </div>
                                </div>

                                <div class="divi-rocket-settings-box">
                                    <label>
                                        <span><?php echo esc_html__('Allow Pingbacks and Trackbacks:', 'divi_rocket'); ?></span>
                                        <select name="ds_divi_pingback">
                                            <option value="open" <?php if (false !== $pingback_status && 'open' == $pingback_status) { ?> selected="selected" <?php } ?>>
                                                <?php echo esc_html__('Open', 'divi_rocket'); ?>
                                            </option>
                                            <option value="closed" <?php if (false !== $pingback_status && 'closed' == $pingback_status) { ?> selected="selected" <?php } ?>>
                                                <?php echo esc_html__('Closed', 'divi_rocket'); ?>
                                            </option>
                                        </select>
                                    </label>
                                    <div class="divi-rocket-settings-tooltip">
                                        <div class="divi-rocket-settings-tooltiptext">
                                            <span><?php echo esc_html__('Help', 'divi_rocket'); ?></span>
                                            <?php echo esc_html__('Select Open if you would like to allow pingbacks and trackbacks, or Closed if you would not.', 'divi_rocket'); ?></div>
                                    </div>
                                </div>

                                <div class="divi-rocket-settings-box">
                                    <label>
                                        <span><?php echo esc_html__('Footer Credit:', 'divi_rocket'); ?></span>
                                        <select name="divi_rocket_footer_credit">
                                            <option value="1" <?php if (1 == $footer_credit) { ?> selected="selected" <?php } ?>>
                                                <?php echo esc_html__('Show in footer bottom bar (below for theme builder)', 'divi_rocket'); ?>
                                            </option>
                                            <option value="2" <?php if (2 == $footer_credit) { ?> selected="selected" <?php } ?>>
                                                <?php echo  esc_html__('Show below footer bottom bar', 'divi_rocket'); ?>
                                            </option>
                                            <option value="0" <?php if (0 == $footer_credit) { ?> selected="selected" <?php } ?>>
                                                <?php echo  esc_html__('Do not show', 'divi_rocket'); ?>
                                            </option>
                                        </select>
                                    </label>
                                    <div class="divi-rocket-settings-tooltip">
                                        <div class="divi-rocket-settings-tooltiptext">
                                            <span><?php echo  esc_html__('Help', 'divi_rocket'); ?></span>
                                            <?php echo  esc_html__('Show us some love by adding a credit for Divi Rocket to the footer of your site! If you choose the first option, the credit will not display in the default Divi footer if you have customized your footer credit. Add your Divi Space affiliate ID to your footer credit below!', 'divi_rocket'); ?></div>
                                    </div>
                                </div>

                                <div class="divi-rocket-settings-box">
                                    <label>
                                        <span><?php echo esc_html__('Divi Space affiliate ID:', 'divi_rocket'); ?></span>
                                        <input name="divi_rocket_affiliate_id" type="text" <?php echo 'value="'.(esc_attr(get_option('divi_rocket_affiliate_id', ''))).'"'; ?>>
                                    </label>

                                    <div class="divi-rocket-settings-tooltip">
                                        <div class="divi-rocket-settings-tooltiptext">
                                            <span><?php echo esc_html__('Help', 'divi_rocket'); ?></span>
                                            <?php printf(esc_html__('Add your Divi Space affiliate ID to your Divi Rocket footer credit link to earn affiliate comissions. For details and to sign up for our affiliate program <a href="%s"  target="_blank">click here</a>.', 'divi_rocket'), 'https://divi.space/affiliates/affiliate-area/'); ?>
                                        </div>
                                    </div>
                                </div>

                            </div>
					<?php
/* !etm { */
					}
/* } !etm */
					?>
<?php
/* !etm { */
?>
					<div id="divi-rocket-settings-about"<?php if (!$isActivated) { //redone ?> class="divi-rocket-settings-active"<?php } ?>>
						<?php if ($isActivated) { //redone
							divi_rocket_license_key_box(); // good 3 final
						} else {
							divi_rocket_activate_page(); // good typed
						} ?>

					</div>
<?php
/* } !etm */
?>
				</div>
			<?php
/* !etm { */
			if ($isActivated) { //redone
/* } !etm */
			?>
                        <input type="submit" value="Update Settings">
                        <?php wp_nonce_field('divi_rocket_save_settings', 'divi_rocket_nonce'); ?>
                    </form>
			<?php
/* !etm { */
			}
/* } !etm */
			?>

                    <script>
                        var divi_rocket_tabs_navigate = function() {
                            jQuery('#divi-rocket-settings-tabs-content > div, #divi-rocket-settings-tabs > li').removeClass('divi-rocket-settings-active');
                            jQuery('#divi-rocket-settings-'+location.hash.substr(1)).addClass('divi-rocket-settings-active');
                            jQuery('#divi-rocket-settings-tabs > li:has(a[href="'+location.hash+'"])').addClass('divi-rocket-settings-active');
                        };

                        if (location.hash) {
                            divi_rocket_tabs_navigate();
                        }

                        jQuery(window).on('hashchange', divi_rocket_tabs_navigate);
                    </script>

                </div>
            </div>
            <?php
        }

        public static function metaBoxes()
        {
            add_meta_box('divi-rocket-metabox', esc_html__('Divi', 'divi-rocket').' Rocket', function ($post) {
                include __DIR__.'/includes/metabox.php';
            }, DiviRocket::$cachePostTypes);
        }

        public static function filterPostRevisions()
        {
            return (get_option('ds-divi-rocket-revisions')) ? (int) get_option('ds-divi-rocket-revisions') : 0;
        }

        public static function frontendScripts()
        {
/* !etm { */
			if (!divi_rocket_has_license_key() ) {
				return;
			}
/* } !etm */

            global $post;

            if (!is_admin() && !empty($post) && DiviRocket::isLazyLoadEnabled($post)) {
                $current_user = wp_get_current_user();
                $current_user_id = (isset($current_user->data->ID)) ? (int) $current_user->data->ID : 0;

                $js_array = [];
                if (0 < (int) $current_user_id) {
                    /*
                        Because the Boolean true|false was not properly being passed through the wp_localize_script function, I decided
                        I would use the string value of false and true.
                    */
                    $permissions = self::_get_permission_object();
                    if (false === $permissions::shouldWeCache($current_user->roles)) {
                        $js_array['should_we_cache'] = 'false';
                    } else {
                        $js_array['should_we_cache'] = 'true';
                    }
                } else {
                    $js_array['should_we_cache'] = null;
                }

                $post_id = (is_null($post->ID)) ? $GLOBALS['wp_the_query']->queried_object->ID : $post->ID;
                $page_builder_used = get_post_meta($post_id, '_et_pb_use_builder', true);

                wp_enqueue_script('divi-rocket-lazyload', plugin_dir_url(__FILE__).'/assets/js/divi-rocket-lazyload.js', ['jquery']);
                wp_localize_script(
                    'divi-rocket-lazyload',
                    'divirocketlazyload',
                    [
                        'post_id' => $post_id,
                        'admin_url' => admin_url('admin-ajax.php'),
                        'et_pb_use_builder' => $page_builder_used,
                        'should_we_cache' => $js_array['should_we_cache'],
                        'strings' => [
                            'loading' => __('Loading...', 'divi_rocket'),
                        ],
                    ]
                );

                wp_enqueue_style('divi-rocket-lazy', DiviRocket::$pluginBaseUrl.'assets/css/lazy.css', [], DiviRocket::VERSION);
            }

            if (DiviRocket::$allowManualCacheClear) {
                wp_enqueue_script('divi-rocket-admin', DiviRocket::$pluginBaseUrl.'assets/js/admin.js', ['jquery'], DiviRocket::VERSION);
                //wp_enqueue_script('divi-rocket-diffDOM', DiviRocket::$pluginBaseUrl.'test/diffDOM/browser/diffDOM.js', array(), DiviRocket::VERSION);

                wp_localize_script('divi-rocket-admin', 'diviRocketStringsAdmin', [
                    'clearingCache' => __('Clearing cache...', 'divi_rocket'),
                    'successfullyClearedCache' => __('Successfully cleared cache!', 'divi_rocket'),
                    'errorClearingCache' => __('Error clearing cache', 'divi_rocket'),
                ]);

                wp_enqueue_style('divi-rocket-admin-bar', DiviRocket::$pluginBaseUrl.'assets/css/admin-bar.css', [], DiviRocket::VERSION);
            }

			/* this shouldn't be needed anymore
            if (!empty($post) && get_option('divi_rocket_cache_on')) {
                foreach (ET_Core_PageResource::get_resources() as $r) {

                    if (is_numeric($r->post_id)) {
                        $r->forced_inline = true;
                        $r->set_output_location('footer');
                    }
                }
            }
			*/
        }

        public static function adminScripts()
        {
            global $post;

            wp_enqueue_style('divi-rocket-admin', DiviRocket::$pluginBaseUrl.'assets/css/admin.css', [], DiviRocket::VERSION);
            wp_enqueue_style('divi-rocket-admin-bar', DiviRocket::$pluginBaseUrl.'assets/css/admin-bar.css', [], DiviRocket::VERSION);

            wp_enqueue_script('divi-rocket-admin', DiviRocket::$pluginBaseUrl.'assets/js/admin.js', ['jquery'], DiviRocket::VERSION);
            wp_localize_script('divi-rocket-admin', 'diviRocketStringsAdmin', [
                'clearingCache' => __('Clearing cache...', 'divi_rocket'),
                'successfullyClearedCache' => __('Successfully cleared cache!', 'divi_rocket'),
                'errorClearingCache' => __('Error clearing cache', 'divi_rocket'),
            ]);
        }

        /*
        public function defer_parsing_of_css($html, $handle, $href, $media)
        {
            if (is_admin()) {
                return $html;
            }

            return <<< EOT
<link rel='preload' as='style' onload="this.onload=null;this.rel='stylesheet'" id='{$handle}' as='style' href='{$href}' type='text/css' media='all' />
EOT;
        }
        */

        public static function adminBarMenu($wp_admin_bar)
        {
            if (DiviRocket::$allowManualCacheClear) {
                $actions = [];

                $cpts = (array) get_post_types(['show_in_admin_bar' => true], 'objects');

                global $post;
                if (isset($post) && in_array($post->post_type, DiviRocket::$cachePostTypes)) {
                    $actions[admin_url('admin-ajax.php?action=divi-space-cache-clear-current&post='.$post->ID.'&nonce='.wp_create_nonce('divi-rocket-cache-clear-'.$post->ID))] = [
                        sprintf(__('Clear cache for current %s', 'divi_rocket'), $post->post_type),
                        'divi-space-cache-clear-current',
                    ];
                }

                $actions[admin_url('admin-ajax.php?action=divi-space-cache-clear-all&nonce='.wp_create_nonce('divi-rocket-cache-clear-all'))] = [__('Clear entire cache', 'divi_rocket'), 'divi-space-cache-clear-all'];

                if (current_user_can('manage_options')) {
                    $settingsPageUrl = admin_url('admin.php?page=divi-rocket-settings');
                    $actions[$settingsPageUrl] = [__('Settings', 'divi_rocket'), 'divi-rocket-settings'];
                } else {
                    $settingsPageUrl = '#';
                }

                $title = '<i></i><span>'.esc_html__('Divi Rocket', 'divi_rocket').'</span>';

                $wp_admin_bar->add_menu(
                    [
                        'id' => 'divi-rocket',
                        'title' => $title,
                        'href' => $settingsPageUrl,
                    ]
                );

                foreach ($actions as $link => $action) {
                    list($title, $id) = $action;

                    $wp_admin_bar->add_menu(
                        [
                            'parent' => 'divi-rocket',
                            'id' => $id,
                            'title' => $title,
                            'href' => $link,
                        ]
                    );
                }
            }
        }

    }

function DiviRocket_dynamicallyLoadShortcodes($et_builder_add_main_elements_hook = 'wp')
{
    global $post;
    if (!empty($post) && DiviRocket::isCachingEnabled($post) && has_action($et_builder_add_main_elements_hook, 'et_builder_add_main_elements')) {
        $et_pb_load_main_elements_priority = apply_filters('et_pb_load_main_elements_priority', 10);
        remove_action($et_builder_add_main_elements_hook, 'et_builder_add_main_elements', $et_pb_load_main_elements_priority);
        add_action($et_builder_add_main_elements_hook, ['DiviRocket', 'et_builder_add_main_elements_alt'], $et_pb_load_main_elements_priority);
        add_filter('pre_do_shortcode_tag', ['DiviRocket', 'loadShortcode'], 10, 2);

        spl_autoload_register(['DiviRocket', 'autoload']);

        class Divi_Rocket_Dummy extends ET_Builder_Element
        {
            protected $additional_shortcode_slugs;
            protected $no_render = true;

            public function __construct()
            {
                $this->slug = 'divi_rocket_dummy';
                $this->additional_shortcode_slugs = array_keys(DiviRocket::$shortcodeFiles);
                parent::__construct();
            }
            public function init()
            {
            }
        }
        new Divi_Rocket_Dummy();

        global $shortcode_tags;

        do_action('et_builder_modules_load');

		// phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited
        $shortcode_tags = array_merge(array_fill_keys(array_keys(DiviRocket::$shortcodeFiles), '__return_false'), $shortcode_tags);

        do_action('et_builder_modules_loaded');
    }
}

DiviRocket::setup();
