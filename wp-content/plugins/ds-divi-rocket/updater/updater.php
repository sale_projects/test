<?php
/* 
This file contains code from the Easy Digital Downloads Software Licensing addon.
Copyright Easy Digital Downloads; released under the GNU General Public License (GPL) version 2 or
later, licensed under GPL version 3 (see ../license/license.txt).
This file was modified by Jonathan Hall, Dominika Rauk, and/or others; last modified 2020-09-22.
*/

if (!defined('ABSPATH')) exit;

define( 'divi_rocket_STORE_URL', DiviRocket::PLUGIN_AUTHOR_URL );
define( 'divi_rocket_ITEM_NAME', 'Divi Rocket' ); // Needs to exactly match the download name in EDD
define( 'divi_rocket_PLUGIN_PAGE', 'admin.php?page=divi-rocket-settings#about' );

define('divi_rocket_BRAND_NAME', 'Divi Space');

if( !class_exists( 'divi_rocket_Plugin_Updater' ) ) {
	// load our custom updater
	include( dirname( __FILE__ ) . '/EDD_SL_Plugin_Updater.php' );
}

// Load translations
load_plugin_textdomain('aspengrove-updater', false, plugin_basename(dirname(__FILE__).'/lang'));

function divi_rocket_updater() {

	// retrieve our license key from the DB
	$license_key = trim( get_option( 'divi_rocket_license_key' ) );

	// setup the updater
	new divi_rocket_Plugin_Updater( divi_rocket_STORE_URL, DiviRocket::$pluginFile, array(
			'version' 	=> DiviRocket::VERSION, // current version number
			'license' 	=> $license_key, 		// license key (used get_option above to retrieve from DB)
			'item_name' => divi_rocket_ITEM_NAME, 	// name of this plugin
			'author' 	=> divi_rocket_BRAND_NAME,  // author of this plugin
			'beta'		=> false
		)
	);
	
	// creates our settings in the options table
	register_setting('divi_rocket_license', 'divi_rocket_license_key', 'divi_rocket_sanitize_license' );
	
	if (isset($_POST['divi_rocket_license_key_deactivate_go']) && isset($_POST['divi_rocket_license_key_deactivate'])) {
		require_once(dirname(__FILE__).'/license-key-activation.php');
		$result = divi_rocket_deactivate_license();
		if ($result !== true) {
			define('divi_rocket_DEACTIVATE_ERROR', empty($result) ? __('An unknown error has occurred. Please try again.', 'aspengrove-updater') : $result);
		}
		unset($_POST);
	}
}
add_action( 'admin_init', 'divi_rocket_updater', 0 );


function divi_rocket_has_license_key() {
	return (get_option('divi_rocket_license_status') === 'valid');
}

function divi_rocket_activate_page() {
	$license = get_option( 'divi_rocket_license_key' );
	$status  = get_option( 'divi_rocket_license_status' );
	?>
		<form method="post" action="options.php" id="divi_rocket_license_key_form">
			<div id="divi_rocket_license_key_form_logo_container">
				<a href="https://divi.space/?utm_source=<?php echo(DiviRocket::PLUGIN_SLUG); ?>&amp;utm_medium=plugin-credit-link&amp;utm_content=license-key-activate" target="_blank">
					<img src="<?php echo(plugins_url('logo.png', __FILE__)); ?>" alt="<?php echo(divi_rocket_BRAND_NAME); ?>" />
				</a>
			</div>
			
			<div id="divi_rocket_license_key_form_body">
				<div id="divi_rocket_license_key_form_title">
					<?php esc_html_e( 'Divi', 'divi-rocket' )?> Rocket
					<small>v<?php echo(DiviRocket::VERSION); ?></small>
				</div>
				
				<p>
					Thank you for purchasing <?php echo(htmlspecialchars(divi_rocket_ITEM_NAME)); ?>!<br />
					Please enter your license key below.
				</p>
				
				<?php settings_fields('divi_rocket_license'); ?>
				
				<label>
					<span><?php _e('License Key:', 'aspengrove-updater'); ?></span>
					<input name="divi_rocket_license_key" type="password" class="regular-text"<?php if (!empty($_GET['license_key'])) { ?> value="<?php echo(esc_attr($_GET['license_key'])); ?>"<?php } else if (!empty($license)) { ?> value="<?php echo(esc_attr($license)); ?>"<?php } ?> />
				</label>
				
				<?php
					if (isset($_GET['sl_activation']) && $_GET['sl_activation'] == 'false') {
						echo('<p id="divi_rocket_license_key_form_error">'.(empty($_GET['sl_message']) ? esc_html__('An unknown error has occurred. Please try again.', 'aspengrove-updater') : esc_html($_GET['sl_message'])).'</p>');
					} else if (defined('divi_rocket_DEACTIVATE_ERROR')) {
						// divi_rocket_DEACTIVATE_ERROR is already HTML escaped
						echo('<p id="divi_rocket_license_key_form_error">'.divi_rocket_DEACTIVATE_ERROR.'</p>');
					}
					
					submit_button('Continue');
				?>
			</div>
		</form>
	<?php
}

function divi_rocket_license_key_box() {
	$status  = get_option( 'divi_rocket_license_status' );
	?>
		<div id="divi_rocket_license_key_box">
			<div id="divi_rocket_license_key_form">
				<div id="divi_rocket_license_key_form_logo_container">
					<a href="https://divi.space/?utm_source=<?php echo(DiviRocket::PLUGIN_SLUG); ?>&amp;utm_medium=plugin-credit-link&amp;utm_content=license-key-status" target="_blank">
						<img src="<?php echo(plugins_url('logo.png', __FILE__)); ?>" alt="<?php echo(divi_rocket_BRAND_NAME); ?>" />
					</a>
				</div>
				
				<div id="divi_rocket_license_key_form_body">
					<div id="divi_rocket_license_key_form_title">
						<?php esc_html_e( 'Divi', 'divi-rocket' )?> Rocket
						<small>v<?php echo(DiviRocket::VERSION); ?></small>
					</div>
					
					<label>
						<span><?php esc_html_e('License Key:', 'aspengrove-updater'); ?></span>
						<input type="password" readonly="readonly" value="<?php echo(esc_html(get_option('divi_rocket_license_key'))); ?>" />
					</label>
					
					<?php
						if (defined('divi_rocket_DEACTIVATE_ERROR')) {
							echo('<p id="divi_rocket_license_key_form_error">'.divi_rocket_DEACTIVATE_ERROR.'</p>');
						}
						wp_nonce_field( 'divi_rocket_license_key_deactivate', 'divi_rocket_license_key_deactivate' );
						submit_button('Deactivate License Key', '', 'divi_rocket_license_key_deactivate_go');
					?>
				</div>
			</div>
		</div>
	<?php
}

function divi_rocket_sanitize_license( $new ) {
	if (defined('divi_rocket_LICENSE_KEY_VALIDATED')) {
		return $new;
	}
	$old = get_option( 'divi_rocket_license_key' );
	if( $old && $old != $new ) {
		delete_option( 'divi_rocket_license_status' ); // new license has been entered, so must reactivate
	}
	
	// Need to activate license here, only if submitted
	require_once(dirname(__FILE__).'/license-key-activation.php');
	divi_rocket_activate_license($new); // Always redirects
}