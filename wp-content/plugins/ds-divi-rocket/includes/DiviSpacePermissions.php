<?php
// This file modified by Carl Wuensche and Jonathan Hall; last modified 2020-02-21

defined('ABSPATH') || die();

class Divi_Rocket_Permissions
{
    private static $ignore_caching_roles = [];

    public function __construct()
    {
        $user_roles_to_ignore = get_option('divi_rocket_ignore_caching_rules');
        if (false !== $user_roles_to_ignore) {
            self::$ignore_caching_roles = array_filter(array_map('strtolower', $user_roles_to_ignore));
        }
    }

    /*
        This function adds a role to the array of roles we're using when we want to find out which roles
        we will look for when we decide not to cache. The (string)$ignore_role is a string from roles
        available in your WordPress Roles list.
    */
    public static function setIgnoreRole($ignore_role)
    {
        if (!empty($ignore_role)) {
            array_push(self::$ignore_caching_roles, $ignore_role);
        }
    }

    /*
        Returns true if the page is returning a cached version of the page. If this returns false, we will use
        whatever is in the database. We're sending an array of roles so we can check multiple roles at a time to see
        if they're in the ignore list.
    */
    public static function shouldWeCache($current_user_role)
    {
        if (null === $current_user_role) {
            return !in_array('__guest', self::$ignore_caching_roles);
        }
        if (!empty($current_user_role)) {
            foreach ($current_user_role as $role) {
                if (in_array($role, self::$ignore_caching_roles)) {
                    return false;
                }
            }
        }

        return true;
    }

    // Returns the roles we have set in the role array.
    public static function getIgnoreCachingRoles()
    {
        return self::$ignore_caching_roles;
    }
}
