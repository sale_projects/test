<?php
/*
This file includes code based on parts of the Divi theme, copyright Elegant Themes,
released under the GNU General Public License (GPL) version 2, licensed under GPL
version 3 for this project by special permission (see ../license.txt).

This file modified by Jonathan Hall and Carl Wuensche; last modified 2021-03-05
*/

defined('ABSPATH') || die();

class DiviSpacePostCacher
{
    const CACHE_FILE_START = '<?php DiviRocket::mustBeRendering(); ?>';

    private $shortcodeData = [];
    private $shortcodeEnds = [];
    private $shortcodeEndsStatic = [];
    private $globalModules = [];
    private $styleRules = [];
    private $currentRowGlobals;
    private $currentRowGlobalsPre;
    private $currentRowInnerGlobals;
    private $currentRowInnerGlobalsPre;
    private $currentModuleGlobals;
    private $currentModuleGlobalsPre;
    private $postShortcodeStructure;
    private $currentChildModuleIndex;
    private $shortcodeResults = [];
    private $headerExtra = '';
    private $contentShortcodeData = [];
    private $fontsIndex = 0;
	private $isInSpecialtyColumn = false;
	private $options = [
		'builderContentWrapper' => true,
		'cssInHeaderExtra' => true
	];
	private $orderNumPrefix;


    public function __construct($postId, $postContent, $cacheDir, $toBrowser = false, $destinationSection = null)
    {
		
        if ($destinationSection) {
            $sections_initial = 1;
            $sections_subsequent = 1;
        } else {
            $sections_initial = (int) get_post_meta($postId, '_divi_rocket_sections_initial', true);
            $sections_subsequent = (int) get_post_meta($postId, '_divi_rocket_sections_subsequent', true);

            if ($sections_initial < 1) {
                $sections_initial = (int) get_option('divi_rocket_sections_initial');
                if ($sections_initial < 1) {
                    $sections_initial = 2;
                }
            }

            if ($sections_subsequent < 1) {
                $sections_subsequent = (int) get_option('divi_rocket_sections_subsequent');
                if ($sections_subsequent < 1) {
                    $sections_subsequent = 2;
                }
            }
        }

        $cacheDocumentsStatic = [];
        $cacheCss = [];
        $cacheCssStatic = [];
        $firstSectionStart = '';
		
		$this->orderNumPrefix = 90000;
		switch ($destinationSection) {
			case 'header':
				$this->orderNumPrefix += 10000;
				break;
			case 'body':
				$this->orderNumPrefix += 20000;
				break;
			case 'footer':
				$this->orderNumPrefix += 30000;
				break;
		}

        if ($postContent) {
            add_filter('pre_do_shortcode_tag', [$this, 'processShortcode'], 10, 4);
            //$postShortcodeStructure = explode('|', strip_tags( trim( do_shortcode($postContent) ) ) );

            $this->postShortcodeStructure = explode('|', wp_strip_all_tags(trim(apply_filters('the_content', $postContent))));
            remove_filter('pre_do_shortcode_tag', [$this, 'processShortcode'], 10, 4);

            add_shortcode('DiviRocket_capture_row_globals', [$this, 'captureRowGlobals']);
            add_shortcode('DiviRocket_capture_row_inner_globals', [$this, 'captureRowInnerGlobals']);

            $cacheScripts = [];
            $cacheStyles = [];
            $cacheFonts = [];
            $cacheDocuments = [];
            $cacheBoxShadowElements = [];
            $cacheMotionElementsDesktop = [];
            $cacheMotionElementsTablet = [];
            $cacheMotionElementsPhone = [];
            $cacheStickyElements = [];
            $cacheDocument = '';
            $cacheDocumentStatic = '';
            $cssString = '';
            $cssStringStatic = '';
            $inPhpString = false;

            global $et_fonts_queue;
            $et_fonts_queue_original = isset($et_fonts_queue) ? $et_fonts_queue : [];
            $et_fonts_queue = [];

            foreach ($this->postShortcodeStructure as $structurePartNum => $structurePart) {
                $structurePart = trim($structurePart);
                $shortcodeIndex = substr($structurePart, 1);

                if (is_numeric($shortcodeIndex)) {
                    $currentShortcodeData = $this->shortcodeData[$shortcodeIndex];

                    switch ($structurePart[0]) {
                        case 'S': // Shortcode start
                        case 'S':

							DiviRocket::$DEBUG && DiviSpaceCacheDebug::setCurrentModule($shortcodeIndex, $currentShortcodeData[0]);
							
                            switch ($currentShortcodeData[0]) {
                                case 'et_pb_contact_field'/* => self::SHORTCODE_STATIC,*/:

                                    global $et_pb_contact_form_num;
                                    --$et_pb_contact_form_num;

                                    break;
                                /*
                                case 'et_pb_pricing_tables':
                                    $tableEndPos = array_search('E'.$shortcodeIndex, $this->postShortcodeStructure);

                                    if ($tableEndPos) {


                                        $this->currentModuleGlobals = array(
                                            'et_pb_pricing_tables_num' => $tableEndPos - $structurePartNum - 1
                                        );

                                    }
                                    break;
                                */
                            }

                            if (isset($currentShortcodeData[2])) {
                                switch (DiviRocket::$shortcodes[$currentShortcodeData[0]]) {
                                    case DiviRocket::SHORTCODE_STATIC:
                                        add_filter('pre_do_shortcode_tag', [$this, 'processContentShortcode'], 10, 4);
                                }

                                if (isset($this->shortcodeResults[$structurePartNum])) {
                                    $shortcodeResult = $this->shortcodeResults[$structurePartNum];
                                } else {
                                    DiviRocket::maybeSimulateLoggedOut($currentShortcodeData[0]);
                                    $shortcodeResult = DiviRocket::renderShortcode($currentShortcodeData[0], $currentShortcodeData[1], null, $this->orderNumPrefix + $shortcodeIndex, false, $currentShortcodeData[2]);
                                }

                                @remove_filter('pre_do_shortcode_tag', [$this, 'processContentShortcode'], 10, 4);
                            } else {
                                $structurePartEnd = $structurePart;
                                $structurePartEnd[0] = 'E';

                                $noFF = [
                                    'et_pb_section'/* => self::SHORTCODE_STATIC*/,
                                    'et_pb_row'/* => self::SHORTCODE_STATIC*/,
                                    'et_pb_row_inner'/* => self::SHORTCODE_STATIC*/,
                                    'et_pb_column'/* => self::SHORTCODE_STATIC*/,
                                    'et_pb_column_inner'/* => self::SHORTCODE_STATIC*/,
                                    'et_pb_column_empty'/* => self::SHORTCODE_STATIC*/,
                                    'et_pb_contact_form'/* => self::SHORTCODE_DYNAMIC*/,
                                ];

                                $endPos = array_search('E'.$shortcodeIndex, $this->postShortcodeStructure);

                                if ($endPos > $structurePartNum + 1 && !in_array($currentShortcodeData[0], $noFF)) {
                                    $innerShortcodes = '';

                                    $this->firstChildModuleIndex = $structurePartNum + 1;
                                    $this->currentChildModuleIndex = $this->firstChildModuleIndex;

                                    for ($i = $this->firstChildModuleIndex; $i < $endPos; ++$i) {
                                        $structurePart2 = trim($this->postShortcodeStructure[$i]);
                                        if ($structurePart2) {
                                            $shortcodeIndex2 = substr($structurePart2, 1);
                                            $currentShortcodeData2 = $this->shortcodeData[$shortcodeIndex2];
                                            $structurePartEnd2 = $structurePart2;
                                            $structurePartEnd2[0] = 'E';

                                            $innerShortcodes .= $currentShortcodeData2[3];
                                        }
                                    }

                                    add_filter('pre_do_shortcode_tag', [$this, 'processChildShortcode'], 10, 4);
                                    DiviRocket::maybeSimulateLoggedOut($currentShortcodeData[0]);
                                    $shortcodeResult = DiviRocket::renderShortcode(
                                        $currentShortcodeData[0],
                                        $currentShortcodeData[1],
                                        null,
                                        $this->orderNumPrefix + $shortcodeIndex,
                                        false,
                                        $innerShortcodes
                                    );
                                    remove_filter('pre_do_shortcode_tag', [$this, 'processChildShortcode'], 10, 4);

								
                                    $this->firstChildModuleIndex = null;
                                    $this->currentChildModuleIndex = null;
                                } else {
                                    /*
                                    if ($currentShortcodeData[0] == 'et_pb_pricing_tables') {

                                        add_filter('et_pb_module_content', array($this, 'setModuleGlobals'));
                                    }
                                    */

                                    if (isset($this->shortcodeResults[$structurePartNum])) {
                                        $shortcodeResult = $this->shortcodeResults[$structurePartNum];
                                    } else {
                                        DiviRocket::maybeSimulateLoggedOut($currentShortcodeData[0]);
                                        $shortcodeResult = DiviRocket::renderShortcode(
                                            $currentShortcodeData[0],
                                            $currentShortcodeData[1],
                                            null,
                                            $this->orderNumPrefix + $shortcodeIndex,
                                            false,
                                            (trim($this->postShortcodeStructure[$structurePartNum + 1]) == $structurePartEnd ? '' : null)
                                        );
                                    }

                                    if (!empty($this->currentRowGlobals)) {
                                        $this->currentRowGlobalsPre = [];
                                        foreach ($this->currentRowGlobals as $var => $value) {
                                            $this->currentRowGlobalsPre[$var] = isset($GLOBALS[$var]) ? $GLOBALS[$var] : null;
                                            $GLOBALS[$var] = $this->currentRowGlobals[$var];
                                        }

                                        $this->currentRowGlobals = null;
                                    } elseif (!empty($this->currentRowInnerGlobals)) {
                                        $this->currentRowInnerGlobalsPre = [];
                                        foreach ($this->currentRowInnerGlobals as $var => $value) {
                                            $this->currentRowInnerGlobalsPre[$var] = isset($GLOBALS[$var]) ? $GLOBALS[$var] : null;
                                            $GLOBALS[$var] = $this->currentRowInnerGlobals[$var];
                                        }

                                        $this->currentRowInnerGlobals = null;
                                    }
                                }
                            }

							
							
                            $renderedShortcode = $shortcodeResult['html'];

                            remove_filter('et_pb_module_content', [$this, 'setModuleGlobals']);
							
                            if (!empty($this->currentModuleGlobalsPre)) {
                                foreach ($this->currentModuleGlobalsPre as $var => $value) {
                                    $GLOBALS[$var] = $this->currentModuleGlobalsPre[$var];
                                }
                            }
                            $this->currentModuleGlobalsPre = null;

                            switch ($currentShortcodeData[0]) {
                                case 'et_pb_contact_field'/* => self::SHORTCODE_STATIC,*/:

                                    ++$et_pb_contact_form_num;

                                    break;
                            }

                            $cacheScripts = array_merge($cacheScripts, $shortcodeResult['scripts']);
                            $cacheStyles = array_merge($cacheStyles, $shortcodeResult['styles']);
							$cacheBoxShadowElements = array_merge($cacheBoxShadowElements, $shortcodeResult['boxShadowElements']);
							$cacheMotionElementsDesktop = array_merge($cacheMotionElementsDesktop, $shortcodeResult['motionElementsDesktop']);
							$cacheMotionElementsTablet = array_merge($cacheMotionElementsTablet, $shortcodeResult['motionElementsTablet']);
							$cacheMotionElementsPhone = array_merge($cacheMotionElementsPhone, $shortcodeResult['motionElementsPhone']);
							$cacheStickyElements = array_merge($cacheStickyElements, $shortcodeResult['stickyElements']);
							

                            $shortcodeContentStart = strpos($renderedShortcode, DiviRocket::SHORTCODE_CONTENT_PLACEHOLDER);
							
							/*
							The shortcode type is forced to be dynamic in the following cases:
								- The shortcode tag is not one of the "known" shortcode tags
								- The shortcode is a literal content shortcode AND dynamic content (@ET-DC@) is found anywhere in the shortcode
								- The shortcode is not a literal content shortocde AND dynamic content (@ET-DC@) is found anywhere in the shortcode attributes (content is not checked)
							*/
							
                            $shortcodeType =
									empty(DiviRocket::$shortcodes[$currentShortcodeData[0]])
									|| (
										false === $shortcodeContentStart
										? strpos($currentShortcodeData[3], '@ET-DC@')
										: ( is_array($currentShortcodeData[1]) && strpos( implode('', $currentShortcodeData[1]), '@ET-DC@' ) )
									)
								? DiviRocket::SHORTCODE_DYNAMIC
								: DiviRocket::$shortcodes[$currentShortcodeData[0]];

							DiviRocket::$DEBUG
								&& DiviSpaceCacheDebug::setModuleMeta('rawHtml', $shortcodeResult['html'])
								&& DiviSpaceCacheDebug::setModuleMeta('moduleScripts', $shortcodeResult['scripts'])
								&& DiviSpaceCacheDebug::setModuleMeta('moduleStyles', $shortcodeResult['styles'])
								&& DiviSpaceCacheDebug::setModuleMeta('shortcodeType', $shortcodeType);

                            if (false === $shortcodeContentStart) {
                                // This shortcode is either a literal content shortcode or it does not output its content

                                switch ($shortcodeType) {
                                    case DiviRocket::SHORTCODE_STATIC:

                                        $cacheDocument .= preg_replace_callback('/#DiviRocketShortcodePlaceholder([0-9]+)#/', [$this, 'replaceContentShortcode'], $renderedShortcode);
                                        $cacheDocumentStatic .= preg_replace_callback('/#DiviRocketShortcodePlaceholder([0-9]+)#/', [$this, 'replaceContentShortcodeStatic'], $renderedShortcode);
                                        $cssString .= $shortcodeResult['css'];
                                        $cssStringStatic .= $shortcodeResult['css'];

                                        break;
                                    case DiviRocket::SHORTCODE_CONTENT_DYNAMIC:
                                        $dependencies = isset(DiviRocket::$shortcodePostTypeDependencies[$currentShortcodeData[0]])
                                                            ? DiviRocket::$shortcodePostTypeDependencies[$currentShortcodeData[0]]
                                                            : ['post'];

                                        $dependencies2 = [];

                                        foreach ($dependencies as $i => $dependency) {
                                            if (is_numeric($dependency)) {
                                                $dependencyPost = get_post($dependency);
                                                $dependencies2[$dependency] = $dependencyPost->post_modified_gmt;
                                            } elseif ('_comment' == $dependency) {
                                                $dependencies2[$dependency] = get_post_meta($postId, 'DiviRocket_rev_comment', true);
                                                $dependency .= '_'.$postId;
                                            } else {
                                                if ('*' == $dependency[0]) {
                                                    $dependency = substr($dependency, 1);
                                                    $dependency = isset($currentShortcodeData[1][$dependency]) ? $currentShortcodeData[1][$dependency] : 'post';
                                                }

                                                $dependencies2[$dependency] = get_option('DiviRocket_rev_'.$dependency, 0);
                                            }
                                        }

                                        $cacheDocument .= '<?php DiviRocket::contentDynamicShortcodeStart(\''
                                                            .$currentShortcodeData[0].'\', '
                                                            .var_export($dependencies2, true)
                                                            .','.var_export(trim($shortcodeResult['css']).'/*DIVI_ROCKET_CSS_END*/', true)
                                                            .','.(empty($currentShortcodeData[1]) ? 'array()' : var_export($currentShortcodeData[1], true))
                                                            .', '.(isset($shortcodeResult['render_count']) ? $shortcodeResult['render_count'] : 'null')
                                                            .', '.(isset($shortcodeResult['order_num']) ? $shortcodeResult['order_num'] : 'null')
                                                            .(isset($currentShortcodeData[2]) ? ', \''.addslashes($currentShortcodeData[2]).'\'' : '')
                                                            .'); ?>';
										
										$cacheDocument .= preg_replace_callback('/#DiviRocketShortcodePlaceholder([0-9]+)#/', [$this, 'replaceContentShortcode'], $renderedShortcode);
                                        $cacheDocumentStatic .= preg_replace_callback('/#DiviRocketShortcodePlaceholder([0-9]+)#/', [$this, 'replaceContentShortcodeStatic'], $renderedShortcode);
										
                                        $cacheDocument .= '<?php DiviRocket::contentDynamicShortcodeEnd(); ?>';

                                        $cssStringStatic .= $shortcodeResult['css'];

                                        break;
                                    default:
                                        $cacheDocument .= '<?php DiviRocket::renderShortcode(\''.$currentShortcodeData[0].'\','
                                                            .(empty($currentShortcodeData[1]) ? 'array()' : var_export($currentShortcodeData[1], true))
                                                            .', '.(isset($shortcodeResult['render_count']) ? $shortcodeResult['render_count'] : 'null')
                                                            .', '.(isset($shortcodeResult['order_num']) ? $shortcodeResult['order_num'] : 'null')
                                                            .', true';

                                        if (isset($currentShortcodeData[2])) {
                                            $cacheDocument .= ', \''.addslashes($currentShortcodeData[2]).'\'';
                                        }

                                        $cacheDocument .= '); ?>';

                                        $cacheDocumentStatic .= preg_replace_callback('/#DiviRocketShortcodePlaceholder([0-9]+)#/', [$this, 'replaceContentShortcodeStatic'], $renderedShortcode);
                                        $cssStringStatic .= $shortcodeResult['css'];
                                }
                            } else { // This shortcode is not a literal content shortcode and DOES output its content
                                switch ($shortcodeType) {
                                    case DiviRocket::SHORTCODE_STATIC:

                                        $shortcodeAfterContentStart = $shortcodeContentStart + strlen(DiviRocket::SHORTCODE_CONTENT_PLACEHOLDER);
                                        $end = substr($renderedShortcode, $shortcodeAfterContentStart);
                                        $start = substr($renderedShortcode, 0, $shortcodeContentStart);

                                        $cacheDocument .= $start;
                                        $cacheDocumentStatic .= $start;
                                        $this->shortcodeEnds[$shortcodeIndex] = $end;
                                        $this->shortcodeEndsStatic[$shortcodeIndex] = $end;

                                        $cssString .= $shortcodeResult['css'];
                                        $cssStringStatic .= $shortcodeResult['css'];

                                        break;
                                    case DiviRocket::SHORTCODE_CONTENT_DYNAMIC:
                                        // This case (content dynamic shortcode with non-literal shortcode content) is not currently applicable

                                        break;
                                    default:
                                        $cacheDocument .= '<?php ob_start(); ?>';

                                        $this->shortcodeEnds[$shortcodeIndex] = '<?php DiviRocket::renderShortcode(\''.$currentShortcodeData[0].'\','
                                                            .(empty($currentShortcodeData[1]) ? 'array()' : var_export($currentShortcodeData[1], true))
                                                            .', '.(isset($shortcodeResult['render_count']) ? $shortcodeResult['render_count'] : 'null')
                                                            .', '.(isset($shortcodeResult['order_num']) ? $shortcodeResult['order_num'] : 'null')
                                                            .', true, ob_get_clean()); ?>';

                                        $shortcodeAfterContentStart = $shortcodeContentStart + strlen(DiviRocket::SHORTCODE_CONTENT_PLACEHOLDER);
                                        $this->shortcodeEndsStatic[$shortcodeIndex] = substr($renderedShortcode, $shortcodeAfterContentStart);
                                        $cacheDocumentStatic .= substr($renderedShortcode, 0, $shortcodeContentStart);

                                        $cssStringStatic .= $shortcodeResult['css'];
                                }
                            }

                            break;
                        case 'E': // Shortcode end
                        case 'E':
                            if (isset($this->shortcodeEnds[$shortcodeIndex])) {
                                $cacheDocument .= $this->shortcodeEnds[$shortcodeIndex];

                                if (isset($this->shortcodeEndsStatic[$shortcodeIndex])) {
                                    $cacheDocumentStatic .= $this->shortcodeEndsStatic[$shortcodeIndex];
                                }

                                switch ($currentShortcodeData[0]) {
                                    case 'et_pb_section':
                                        $cacheDocuments[] = $cacheDocument;
                                        $cacheDocument = '';
                                        $cacheDocumentsStatic[] = $cacheDocumentStatic;
                                        $cacheDocumentStatic = '';
                                        $cacheCss[] = $cssString;
                                        $cacheCssStatic[] = $cssStringStatic;
                                        $cssString = '';
                                        $cssStringStatic = '';

                                        $cacheFonts[] = $et_fonts_queue;
                                        $et_fonts_queue = [];

                                        // no break here, intentional because row globals check applies to specialty sections too
                                    case 'et_pb_row':
                                        if (!empty($this->currentRowGlobalsPre)) {
                                            foreach ($this->currentRowGlobalsPre as $var => $value) {
                                                $GLOBALS[$var] = $this->currentRowGlobalsPre[$var];
                                            }
                                        }
                                        $this->currentRowGlobalsPre = null;

                                        break;
                                    case 'et_pb_row_inner':
                                        if (!empty($this->currentRowInnerGlobalsPre)) {
                                            foreach ($this->currentRowInnerGlobalsPre as $var => $value) {
                                                $GLOBALS[$var] = $this->currentRowInnerGlobalsPre[$var];
                                            }
                                        }
                                        $this->currentRowInnerGlobalsPre = null;

                                        break;
                                }
                            }
                    }
                }
            }
            DiviRocket::endSimulatingLoggedOut();

            remove_shortcode('DiviRocket_capture_row_globals', [$this, 'captureRowGlobals']);
            remove_shortcode('DiviRocket_capture_row_inner_globals', [$this, 'captureRowInnerGlobals']);

            ob_start();
            et_builder_get_modules_js_data();
            $js = trim(ob_get_contents());

            if ('<script ' == substr($js, 0, 8)) {
                $js = '<script class="divi-rocket-added" '.substr($js, 8);
            }

            $firstSectionStart .= $js;
            ob_end_clean();

            if ($destinationSection && isset($destinationSection[2])) {
                $firstSectionStart .= '<div class="divi-rocket-added et_pb_ajax_pagination_container"></div>';
            }

            if ($cacheDocuments) {
                $cacheDocuments[0] = $firstSectionStart.$cacheDocuments[0];
            }

            $userCustomCss = et_pb_get_page_custom_css($postId);
            $cacheCss[0] .= $userCustomCss;
            $cacheCssStatic[0] .= $userCustomCss;

            $this->cacheSections($postId, $cacheDocuments, $cacheCss, $cacheFonts, $cacheDir, $sections_initial, $sections_subsequent, $destinationSection);

            $depKey = 'DiviRocket_dep';
            if ($destinationSection && 'all' != $destinationSection[0]) {
                $depKey .= '_'.$destinationSection[0];
            }

            delete_post_meta($postId, $depKey);
            if ($this->globalModules) {
                foreach ($this->globalModules as $moduleId) {
                    add_post_meta($postId, $depKey, $moduleId);
                }
            }

            if ($cacheScripts) {
                ob_start();

                wp_print_scripts(array_unique($cacheScripts));

                $footerExtra = trim(ob_get_contents());
                ob_end_clean();

                if ($footerExtra) {
					if ( file_exists( $cacheDir.'footer-extra.php' ) ) {
						file_put_contents($cacheDir.'footer-extra.php', $footerExtra, FILE_APPEND);
					} else {
						file_put_contents($cacheDir.'footer-extra.php', self::CACHE_FILE_START.$footerExtra);
					}
					DiviRocket::$DEBUG && DiviSpaceCacheDebug::setGeneralMeta('hasFooterExtra', 1);
                }
            }

            if ($cacheStyles) {
                ob_start();

                wp_print_styles(array_unique($cacheStyles));
                $styles = trim(ob_get_contents());
                ob_end_clean();

                if (!empty($styles)) {
                    $this->headerExtra .= $styles;
                    add_action('wp_footer', function () use ($styles) {
                        echo '<script class="divi-rocket-added">jQuery(\'head\').append(\''.et_core_esc_previously( wp_strip_all_tags( wp_slash( str_replace(["\n", "\r"], '', $styles) ) ) ).'\');</script>';
                    });
                }
            }
			
			
            if ($cacheBoxShadowElements) {
				$this->headerExtra .= '<?php ';
				foreach ($cacheBoxShadowElements as $element) {
					$this->headerExtra .= 'ET_Builder_Module_Field_BoxShadow::register_element('.var_export((string) $element, true).');';
				}
				$this->headerExtra .= ' ?>';
			}
			
            if ($cacheMotionElementsDesktop) {
				$this->headerExtra .= '<?php ET_Builder_Element::$_scroll_effects_fields[\'desktop\'] = array_merge(ET_Builder_Element::$_scroll_effects_fields[\'desktop\'], '
				.var_export($cacheMotionElementsDesktop, true)
				.'); ?>';
			}
			
            if ($cacheMotionElementsTablet) {
				$this->headerExtra .= '<?php ET_Builder_Element::$_scroll_effects_fields[\'tablet\'] = array_merge(ET_Builder_Element::$_scroll_effects_fields[\'tablet\'], '
				.var_export($cacheMotionElementsTablet, true)
				.'); ?>';
			}
			
            if ($cacheMotionElementsPhone) {
				$this->headerExtra .= '<?php ET_Builder_Element::$_scroll_effects_fields[\'phone\'] = array_merge(ET_Builder_Element::$_scroll_effects_fields[\'phone\'], '
				.var_export($cacheMotionElementsPhone, true)
				.'); ?>';
			}
			
            if ($cacheStickyElements) {
				$this->headerExtra .= '<?php ET_Builder_Element::$sticky_elements = array_merge(ET_Builder_Element::$sticky_elements, '
				.var_export($cacheStickyElements, true)
				.'); ?>';
			}

            if ($this->headerExtra) {
				
				if ( file_exists( $cacheDir.'header-extra.php' ) ) {
					file_put_contents($cacheDir.'header-extra.php', $this->headerExtra, FILE_APPEND);
				} else {
					file_put_contents($cacheDir.'header-extra.php', self::CACHE_FILE_START.$this->headerExtra);
				}
				
				DiviRocket::$DEBUG && DiviSpaceCacheDebug::setGeneralMeta('hasHeaderExtra', 1);
            }

            $et_fonts_queue = $et_fonts_queue_original;
        } else {
            // There is no post content to cache

            $this->cacheSections($postId, null, null, null, $cacheDir, $sections_initial, $sections_subsequent, $destinationSection);
        }

        if ($toBrowser) {
		
			if (DiviRocket::$DEBUG) {
				DiviSpaceCacheDebug::outputReport();
			} else {
				echo et_core_esc_previously($firstSectionStart);

				$currentFonts = [];

				if ($destinationSection) {
					$output = implode('', $cacheDocumentsStatic);
					DiviRocket::$footerCss .= implode('', $cacheCssStatic);
					if (isset($destinationSection[2])) {
						$output = $destinationSection[1][1].$output.$destinationSection[2][1];
					}

					foreach ($cacheDocumentsStatic as $i => $cacheDocument) {
						$currentFonts = array_merge($currentFonts, $cacheFonts[$i]);
					}
				} else {
					$output = '';
					foreach ($cacheDocumentsStatic as $i => $cacheDocument) {
						if ($i == $sections_initial) {
							break;
						}
						$output .= $cacheDocumentsStatic[$i];
						DiviRocket::$footerCss .= $cacheCssStatic[$i];

						$currentFonts = array_merge($currentFonts, $cacheFonts[$i]);
					}
				}

				echo et_core_esc_previously( ($destinationSection && 'all' != $destinationSection[0]) ? $output : et_builder_add_builder_content_wrapper($output) );

				$this->renderFonts($currentFonts, true);
			}
		}
    }

    public function replaceContentShortcode($placeholder)
    {
        $currentShortcodeData = $this->contentShortcodeData[$placeholder[1]];

        return '<?php DiviRocket::renderContentShortcode(\''.$currentShortcodeData[0].'\','
            .(empty($currentShortcodeData[1]) ? 'array()' : var_export($currentShortcodeData[1], true))
            .', true'
            .', '.var_export($currentShortcodeData[2], true)
            .'); ?>';
    }

    public function replaceContentShortcodeStatic($placeholder)
    {
        $currentShortcodeData = $this->contentShortcodeData[$placeholder[1]];

        return DiviRocket::renderContentShortcode(
            $currentShortcodeData[0],
            (empty($currentShortcodeData[1]) ? [] : $currentShortcodeData[1]),
            false,
            $currentShortcodeData[2]
        );
    }

    public function setModuleGlobals($filterVar)
    {
        $this->currentModuleGlobalsPre = array_intersect_key($GLOBALS, $this->currentModuleGlobals);
        foreach ($this->currentModuleGlobals as $var => $newValue) {
            $GLOBALS[$var] = $newValue;
        }
        unset($this->currentModuleGlobals);

        return $filterVar;
    }

    public function captureRowGlobals()
    {
        $this->currentRowGlobals = [];
        foreach (['et_pb_all_column_settings', 'et_pb_rendering_column_content_row'] as $var) {
            $this->currentRowGlobals[$var] = isset($GLOBALS[$var]) ? $GLOBALS[$var] : null;
        }
    }

    public function captureRowInnerGlobals()
    {
        $this->currentRowInnerGlobals = [];
        foreach (['et_pb_all_column_settings_inner', 'et_pb_rendering_column_content_row'] as $var) {
            $this->currentRowInnerGlobals[$var] = isset($GLOBALS[$var]) ? $GLOBALS[$var] : null;
        }
    }

    public function processShortcode($overrideOutput, $shortcodeTag, $shortcodeArgs, $shortcodeMatch)
    {
		
        $isLiteralContentShortcode = in_array($shortcodeTag, DiviRocket::$literalContentShortcodes);

        if (isset($shortcodeArgs['global_module'])) {
            $this->globalModules[] = $shortcodeArgs['global_module'];
			$shortcodeMatch = $this->getGlobalModule($shortcodeArgs['global_module'], $shortcodeMatch);
			$shortcodeTag = $shortcodeMatch[2];
			$shortcodeArgs = shortcode_parse_atts($shortcodeMatch[3]);
        }
		
        if (isset($shortcodeArgs['disabled']) && 'on' === $shortcodeArgs['disabled']) {
            return '';
        }
		
        $shortcodeIndex = count($this->shortcodeData);

        $data = [
            $shortcodeTag, $shortcodeArgs,
        ];

        $data[] = $isLiteralContentShortcode ? $shortcodeMatch[5] : null;
        $data[] = $shortcodeMatch[0];

        $this->shortcodeData[] = $data;

		if ( $isLiteralContentShortcode ) {
			$content = '';
		} else {
			$this->isInSpecialtyColumn = ($shortcodeTag == 'et_pb_specialty_column');
			$content = '|'.do_shortcode($shortcodeMatch[5]).'E'.$shortcodeIndex;
			$this->isInSpecialtyColumn = false;
		}
		
		return 'S'.$shortcodeIndex.$content.'|';
	}
	
	function getGlobalModule($global_module_id, $shortcodeMatch) {
		// Copied and adapted from Divi\includes\builder\class-et-builder-element.php
		
		$render_slug = $shortcodeMatch[2];
		
		// Update render_slug when rendering global rows inside Specialty sections.
		$render_slug = $this->isInSpecialtyColumn && 'et_pb_row' === $render_slug ? 'et_pb_row_inner' : $render_slug;

		$global_module_data = et_pb_load_global_module( $global_module_id, $render_slug );

		if ( '' !== $global_module_data ) {
			$unsynced_global_attributes = get_post_meta( $global_module_id, '_et_pb_excluded_global_options' );
			$use_updated_global_sync_method = ! empty( $unsynced_global_attributes );

			$unsynced_options = ! empty( $unsynced_global_attributes[0] ) ? json_decode( $unsynced_global_attributes[0], true ) : array();

			$content_synced = $use_updated_global_sync_method && ! in_array( 'et_pb_content_field', $unsynced_options );

			// support legacy selective sync system
			if ( ! $use_updated_global_sync_method ) {
				$content_synced = ! isset( $shortcodeArgs['saved_tabs'] ) || false !== strpos( $shortcodeArgs['saved_tabs'], 'general' ) || 'all' === $shortcodeArgs['saved_tabs'];
			}
			// Divi\includes\builder\class-et-builder-element.php
			
			
			// Always unsync 'next_background_color' and 'prev_background_color' options for global sections
			// They should be dynamic and reflect color of top and bottom sections.
			if ( 'et_pb_section' === $render_slug ) {
				$unsynced_options = array_merge( $unsynced_options, array( 'next_background_color', 'prev_background_color' ) );
			}
			
			$unsynced_options = array_combine($unsynced_options, $unsynced_options);
			
			preg_match( '/'.get_shortcode_regex().'/', $global_module_data, $globalShortcodeMatch );
			
			$shortcodeAttrs = shortcode_parse_atts( $shortcodeMatch[3] );
			$globalShortcodeAttrs = shortcode_parse_atts( $globalShortcodeMatch[3] );
			
			
			$newAttributes = array_merge(
				array_diff_key( $globalShortcodeAttrs, $unsynced_options ),
				array_intersect_key( $shortcodeAttrs, $unsynced_options )
			);
			
			$newAttributesString = '';
			foreach ($newAttributes as $attributeName => $attributeValue) {
				$newAttributesString .= $attributeName.'="'.str_replace('"', '\\"',  $attributeValue).'" ';
			}
			$newAttributesString = trim($newAttributesString);
			
			return [
				'['.$render_slug.' '.$newAttributesString.']'.($content_synced ? $globalShortcodeMatch[5] : $shortcodeMatch[5]).'[/'.$render_slug.']',
				'',
				$render_slug,
				$newAttributesString,
				'',
				$content_synced ? $globalShortcodeMatch[5] : $shortcodeMatch[5],
				''
			];
			
			
			if ( $content_synced ) {
				
				
				
				
				// Set the flag showing if we load inner row
				//$load_inner_row = 'et_pb_row_inner' === $render_slug;
				//$global_content = et_pb_get_global_module_content( $global_module_data, $render_slug, $load_inner_row );
			}
			
			
			
			// cleanup the shortcode string to avoid the attributes messing with content.
			//$global_content_processed = false !== $global_content ? str_replace( $global_content, '', $global_module_data ) : $global_module_data;
			//$global_atts              = shortcode_parse_atts( et_pb_remove_shortcode_content( $global_content_processed, $this->slug ) );
			// $global_atts              = $this->_encode_legacy_dynamic_content( $global_atts, $enabled_dynamic_attributes );

			// Additional content processing required for Code Modules.
			/*
			if ( in_array( $render_slug, array( 'et_pb_code', 'et_pb_fullwidth_code' ), true ) ) {
				$global_content_processed = _et_pb_code_module_prep_content( $global_content_processed );
			}
			*/

			
			
			
			
			
			
			
		}
		
    }

    public function processContentShortcode($overrideOutput, $shortcodeTag, $shortcodeArgs, $shortcodeMatch)
    {
        $shortcodeIndex = count($this->contentShortcodeData);

        $data = [
            $shortcodeTag, $shortcodeArgs, $shortcodeMatch[5],
        ];

        $this->contentShortcodeData[] = $data;

        return '#DiviRocketShortcodePlaceholder'.$shortcodeIndex.'#';
    }

    public function processChildShortcode($overrideOutput, $shortcodeTag, $shortcodeArgs, $shortcodeMatch)
    {
        remove_filter('pre_do_shortcode_tag', [$this, 'processChildShortcode'], 10, 4);

        $structurePart = trim($this->postShortcodeStructure[$this->currentChildModuleIndex]);
        $shortcodeIndex = substr($structurePart, 1);
        $currentShortcodeData = $this->shortcodeData[$shortcodeIndex];
        $structurePartEnd = $structurePart;
        $structurePartEnd[0] = 'E';

        if (isset($currentShortcodeData[2])) {
            add_filter('pre_do_shortcode_tag', [$this, 'processContentShortcode'], 10, 4);
            DiviRocket::maybeSimulateLoggedOut($currentShortcodeData[0]);
            $this->shortcodeResults[$this->currentChildModuleIndex] = DiviRocket::renderShortcode($currentShortcodeData[0], $currentShortcodeData[1], null, $this->orderNumPrefix + $shortcodeIndex, false, $currentShortcodeData[2]);
            @remove_filter('pre_do_shortcode_tag', [$this, 'processContentShortcode'], 10, 4);
        } else {
            DiviRocket::maybeSimulateLoggedOut($currentShortcodeData[0]);
            $this->shortcodeResults[$this->currentChildModuleIndex] = DiviRocket::renderShortcode(
                $currentShortcodeData[0],
                $currentShortcodeData[1],
                null,
                $this->orderNumPrefix + $shortcodeIndex,
                false,
                (trim($this->postShortcodeStructure[$this->currentChildModuleIndex + 1]) == $structurePartEnd ? '' : null)
            );
        }

        $isFirstChildModule = ($this->currentChildModuleIndex == $this->firstChildModuleIndex);
        ++$this->currentChildModuleIndex;

        add_filter('pre_do_shortcode_tag', [$this, 'processChildShortcode'], 10, 4);

        return $isFirstChildModule ? DiviRocket::SHORTCODE_CONTENT_PLACEHOLDER : '';
    }

    public function cacheSections($post_id, $post_content, $cssStrings, $fonts, $cacheDir, $sections_initial, $sections_subsequent, $destinationSection = null)
    {

		DiviRocket::$DEBUG && $destinationSection && DiviSpaceCacheDebug::setGeneralMeta('destinationSection', $destinationSection[0]);
		
		//C:\agswp\htdocs\dev\wp-includes\meta.php
		//add_filter('get_post_metadata', array('DiviRocket', 'disableFontCache'), 10, 4 );

        if (!is_dir($cacheDir)) {
            @mkdir($cacheDir, 0755);
        }

        if (!$post_content) {
            $post_content = [''];
            $cssStrings = [''];
        } elseif ($destinationSection) {
            $post_content = [implode('', $post_content)];
            $cssStrings = [implode('', $cssStrings)];

            $allFonts = [];
            foreach ($fonts as $sectionFonts) {
                $allFonts = array_merge($allFonts, $sectionFonts);
            }
            $fonts = [$allFonts];
        }

        $output = '';
        $return = '';
        $cssString = '';
        $currentFonts = [];

        foreach ($post_content as $row => $html_output) {
            $output .= $html_output;

            if ($destinationSection && isset($destinationSection[2])) {
                $output = $destinationSection[1][0].$output.$destinationSection[2][0];
                if ( $this->options['cssInHeaderExtra'] ) {
					$this->headerExtra .= '<style class="divi-rocket-added">'.$cssStrings[0].'</style>';
				} else {
					$cssString .= $cssStrings[$row];
				}
            } else {
                $cssString .= $cssStrings[$row];
            }

            if (!empty($fonts[$row])) {
                $currentFonts = array_merge($currentFonts, $fonts[$row]);
            }

            if ($row + 1 >= $sections_initial && !(($row + 1 - $sections_initial) % $sections_subsequent)) {
                $isInitial = $row + 1 == $sections_initial;
                if ($isInitial) {
                    if ($this->options['builderContentWrapper'] && (!$destinationSection || 'all' == $destinationSection[0])) {
                        $output = et_builder_add_builder_content_wrapper($output);
                    }
					if ( $this->options['cssInHeaderExtra'] ) {
						$this->headerExtra .= '<style class="divi-rocket-added">'.$cssString.'</style>';
					}
                }

                $output .= $this->renderFonts($currentFonts);
                $currentFonts = [];

                file_put_contents(
                    $cacheDir.($destinationSection ? $destinationSection[0] : (($row + 1 - $sections_initial) / $sections_subsequent)).'.php',
                    self::CACHE_FILE_START
                                        .(empty($cssString) || ($isInitial && $this->options['cssInHeaderExtra']) ? '' : '<style class="divi-rocket-added">'.$cssString.'</style>')
                                        .$output
                );

                $output = '';
                $cssString = '';
            }
        }

        if ($output) {
            $isInitial = $row + 1 < $sections_initial;
            if ($isInitial) {
                if ($this->options['builderContentWrapper'] && (!$destinationSection || 'all' == $destinationSection[0])) {
                    $output = et_builder_add_builder_content_wrapper($output);
                }
				if ( $this->options['cssInHeaderExtra'] ) {
					$this->headerExtra .= '<style class="divi-rocket-added">'.$cssString.'</style>';
				}
            }

            $output .= $this->renderFonts($currentFonts);
            $currentFonts = [];

            file_put_contents(
                $cacheDir.($row + 1 < $sections_initial ? 0 : absint(ceil(($row + 1 - $sections_initial) / $sections_subsequent))).'.php',
                self::CACHE_FILE_START
                                    .(empty($cssString) || ($isInitial && $this->options['cssInHeaderExtra']) ? '' : '<style class="divi-rocket-added">'.$cssString.'</style>')
                                    .$output
            );
        }

        //remove_filter('get_post_metadata', array('DiviRocket', 'disableFontCache'), 10, 4 );
    }

    public function renderFonts($fonts, $toBrowser = false)
    {
        global $et_fonts_queue;

        $et_fonts_queue_original = isset($et_fonts_queue) ? $et_fonts_queue : [];
        $et_fonts_queue = $fonts;
        et_builder_print_font();
        $et_fonts_queue = $et_fonts_queue_original;

        $handle = 'et-builder-googlefonts-'.++$this->fontsIndex;

        $styles = wp_styles();
        $fontStyle = $styles->query('et-builder-googlefonts');
        wp_deregister_style('et-builder-googlefonts');

        if ($fontStyle) {
            $fontStyle->handle = $handle;
            $styles->registered[$handle] = $fontStyle;

            $toBrowser || ob_start();

            wp_print_styles($handle);

            wp_deregister_style($handle);

            return $toBrowser ? null : ob_get_clean();
        }
    }
}
