<?php
// This file was modified by Jonathan Hall; last modified 2020-07-31

class DiviSpaceCacheDebug {
	
	private static $cachedModules, $currentModuleId, $generalMeta = [];
	
	public static function setCurrentModule($moduleId, $moduleTag) {
		self::$currentModuleId = $moduleId;
		if ( empty(self::$cachedModules[$moduleId]) ) {
			self::$cachedModules[$moduleId] = new DiviSpaceCacheDebugModule($moduleId, $moduleTag);
		}
	}
	
	/*
	public static function setModuleOutput($moduleId, $output) {
		self::$cachedModules[$moduleId]->setOutput($output);
	}
	*/
	
	public static function setModuleMeta($field, $value) {
		self::$cachedModules[self::$currentModuleId]->setMeta($field, $value);
		return true;
	}
	
	public static function setGeneralMeta($field, $value) {
		self::$generalMeta[$field] = $value;
		return true;
	}
	
	public static function outputReport() {
?>
<h1>Cache Report</h1>

<h2>General</h2>
<table>
	<?php foreach ( self::$generalMeta as $field => $value ) { ?>
	<tr>
		<th><?php echo( esc_html($field) ); ?></th>
		<td>
			<?php foreach ( is_array($value) ? $value : array($value) as $valueStr ) { ?>
				<code><?php echo( esc_html($valueStr) ); ?></code>
			<?php } ?>
		</td>
	</tr>
	<?php } ?>
</table>

<h2>Modules</h2>

<?php
foreach (self::$cachedModules as $moduleId => $module) {
	$module->outputReport(3);
}
?>

<?php
	}
	
}

class DiviSpaceCacheDebugModule {
	
	private $id, $tag, $output, $meta = [];
	
	function __construct($id, $tag) {
		$this->id = $id;
		$this->tag = $tag;
	}
	
	/*
	function setOutput($output) {
		$this->output = $output;
	}
	*/
	
	function setMeta($field, $value) {
		$this->meta[$field] = $value;
	}
	
	function outputReport($topHeadingLevel) {
?>
<?php echo('<h'.( (int) $topHeadingLevel ).'>['.((int) $this->id).'] '.esc_html($this->tag).'</h'.( (int) $topHeadingLevel ).'>'); ?>
<table>
	<?php foreach ( $this->meta as $field => $value ) { ?>
	<tr>
		<th><?php echo( esc_html($field) ); ?></th>
		<td>
			<?php foreach ( is_array($value) ? $value : array($value) as $valueStr ) { ?>
				<code><?php echo( esc_html($valueStr) ); ?></code>
			<?php } ?>
		</td>
	</tr>
	<?php } ?>
</table>
<?php
	}

}