<?php
/*
This file includes code based on and/or copied from parts of WordPress
by Automattic, released under the GNU General Public License (GPL) version 2 or later,
licensed under GPL version 3 or later (see ../license.txt).

This file includes code based on parts of the Divi theme, copyright Elegant Themes,
released under the GNU General Public License (GPL) version 2, licensed under GPL
version 3 for this project by special permission (see ../license.txt).

This file modified by Jonathan Hall, Carl Wuensche, and Dominika Rauk; last modified 2020-09-22

*/

defined('ABSPATH') || die();

$sections_initial = get_post_meta($post->ID, '_divi_rocket_sections_initial', true);
$sections_subsequent = get_post_meta($post->ID, '_divi_rocket_sections_subsequent', true);

wp_nonce_field('divi-rocket-metabox-save', 'divi-rocket-metabox-nonce', false);

?>
<div class="divi-rocket-settings-box">
    <label>
        <span><?php printf(esc_html__('Disable server-side caching for this %s:', 'divi_rocket'), esc_html($post->post_type)); ?></span>
        <input type="checkbox" name="divi_rocket_cache_off" value="1" <?php if (get_post_meta($post->ID, '_divi_rocket_cache_off', true)) { ?> checked="checked" <?php } ?> >
    </label>
</div>

<div class="divi-rocket-settings-box">
    <label>
        <span><?php printf(esc_html__('Disable content lazy loading for this %s:', 'divi_rocket'), esc_html($post->post_type)); ?></span>
        <input type="checkbox" name="divi_rocket_lazy_off" value="1" <?php if (get_post_meta($post->ID, '_divi_rocket_lazy_off', true)) { ?> checked="checked" <?php } ?> >
    </label>
    <div class="divi-rocket-settings-tooltip">
        <div class="divi-rocket-settings-tooltiptext">
            <span><?php echo esc_html__('Help', 'divi_rocket'); ?></span>
            <?php echo esc_html__('Lazy loading requires that server-side caching be enabled. This setting will have no effect if server-side caching is disabled in the Server Caching tab (either globally or for the current user role).', 'divi_rocket'); ?></div>
    </div>
</div>

<div class="divi-rocket-settings-box">
    <label>
        <span><?php echo esc_html__('Number of sections to load initially:', 'divi_rocket'); ?></span>
        <input type="number" name="divi_rocket_sections_initial" step="1" min="1" <?php if ($sections_initial) {
    echo 'value="'.((int) $sections_initial).'"';
} ?>>
    </label>
    <div class="divi-rocket-settings-tooltip">
        <div class="divi-rocket-settings-tooltiptext"><span><?php echo esc_html__('Help', 'divi_rocket'); ?></span>
            <?php echo esc_html__('Leave blank to use the default value.', 'divi_rocket'); ?></div>
    </div>
</div>

<div class="divi-rocket-settings-box">
<label>
	<span><?php echo esc_html__('Number of sections to load per subsequent request:', 'divi_rocket'); ?></span>
	<input type="number" name="divi_rocket_sections_subsequent" step="1" min="1" <?php if ($sections_subsequent) {
    echo 'value="'.((int) $sections_subsequent).'"';
} ?>>
</label>
    <div class="divi-rocket-settings-tooltip">
        <div class="divi-rocket-settings-tooltiptext"><span><?php echo esc_html__('Help', 'divi_rocket'); ?></span>
            <?php echo esc_html__('Leave blank to use the default value.', 'divi_rocket'); ?></div>
    </div>
</div>
