<?php
/*
This file includes code based on parts of the Divi theme, copyright Elegant Themes,
released under the GNU General Public License (GPL) version 2, licensed under GPL
version 3 for this project by special permission (see ../license.txt).

This file modified by Jonathan Hall and Carl Wuensche; last modified 2020-02-21
*/

defined('ABSPATH') || die();

class DiviSpaceTemplateCacher
{
    private $replaceFunctions = [
        'the_content',
        'et_theme_builder_frontend_render_header',
        'et_theme_builder_frontend_render_body',
        'et_theme_builder_frontend_render_footer',
        'get_header',
        'get_footer',
    ];

    public function __construct($templateFile, $cacheFile)
    {
        $functionNamesRegex = '';
        foreach ($this->replaceFunctions as $replaceFunction) {
            $functionNamesRegex .= (empty($functionNamesRegex) ? '' : '|').preg_quote($replaceFunction);
        }

        $cacheDocument = file_get_contents($templateFile);
        $cacheDocument = preg_replace_callback('/[\\s]('.$functionNamesRegex.')[\\s]*\\(/iU', [$this, 'replaceFunctionCall'], $cacheDocument);
        file_put_contents($cacheFile, $cacheDocument);
    }

    public function replaceFunctionCall($functionCall)
    {
        return ' DiviRocket::'.$functionCall[1].'(';
    }
}
