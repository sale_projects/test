/*
This file includes code based on parts of the Divi theme, copyright Elegant Themes,
released under the GNU General Public License (GPL) version 2, licensed under GPL
version 3 for this project by special permission (see ../../license.txt).

This file modified by Jonathan Hall and Carl Wuensche; last modified 2020-09-22
*/

var _divi_rocket_lazy_compat = [];
function divi_rocket_lazy_compat(cb) {
  _divi_rocket_lazy_compat.push(cb);
}

jQuery(document).ready(function($) {
  //$('style.divi-rocket-added').appendTo('head');

  function observeObject(obsObject, next_row, element, column_or_section) {
    obsObject.unobserve(element.target);

    $html = $("html");
    $html.trigger("divi_rocket_lazy_load_request");
    jQuery.post(
      location.href,
      {
        action: "ds_return_section",
        divi_post_id: divirocketlazyload.post_id,
        divi_next_section_id: parseInt(next_row)
      },
      function(data) {
        if (data) {
          $html.trigger("divi_rocket_lazy_load_receive");

          var lastSection = null,
            sectionCount = 0,
            $data = jQuery(data);

          $data.filter("style").appendTo("head");

          $data.filter("div").each(function() {
            if (this.className) {
              var indexClassStart = this.className.indexOf(column_or_section);

              if (indexClassStart != -1) {
                var index = "";
                var pos = indexClassStart + column_or_section.length;
                while (
                  this.className[pos] != " " &&
                  !isNaN(this.className[pos])
                ) {
                  index = index.concat(this.className[pos]);
                  ++pos;
                }

                if (
                  index &&
                  !jQuery("#main-content ." + column_or_section + index).length
                ) {
                  var $prevSection,
                    prevIndex = index - 1;
                  do {
                    $prevSection = jQuery(
                      "#main-content ." + column_or_section + prevIndex
                    );
                  } while (!$prevSection.length && --prevIndex >= 0);

                  if ($prevSection.length) {
                    lastSection = jQuery(this).insertAfter($prevSection);

                    if (lastSection.length) {
                      initContent(lastSection);
                    }

                    //set_lazy_content( lastSection );
                    ++sectionCount;
                  }
                }
              }
            }
          });

          window.et_reinit_waypoint_modules && et_reinit_waypoint_modules();

          if (lastSection) {
            //obsObject.observe( sectionCount < 2 ? lastSection[0] : lastSection.prev()[0] );

            let et_pb_sections = jQuery("#main-content .et_pb_section:visible");
            if (et_pb_sections.length) {
              obsObject.observe(et_pb_sections[et_pb_sections.length - 1]);
            } else {
              // Need to handle this case - no visible sections!
            }
          }
        } else {
			$('#divi-rocket-lazy-loader').remove();
			$html.trigger('divi_rocket_lazy_load_end');
	   }

	} );

  }

  function lazyDivs() {
    let observedRows = [];

    $("<div>")
      .attr("id", "divi-rocket-lazy-loader")
      .append(
        $("<div>")
          .attr("id", "divi-rocket-lazy-loader-inner")
          // HTML code from https://loading.io/css/, modified
          .html(
            '<div class="divi-rocket-lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>'
          )
      )
      .insertAfter("#main-content .et_pb_section:last");

    let next_row = 0;
    var IO = divi_rocket_IO(window);
    let o = new IO(
      function(es, obs) {
        es.forEach(function(e, index) {
          if (e.isIntersecting !== false) {
            Waypoint.refreshAll();
            ++next_row;
            if (observedRows.indexOf(next_row) == -1) {
              observedRows.push(next_row);
              observeObject(o, next_row, e, "et_pb_section_");
            }
          }
        });
      },
      {}
      //, {threshold: 0.25 }
    );

    let et_pb_sections = jQuery("#main-content .et_pb_section:visible");
    if (et_pb_sections.length) {
      o.observe(et_pb_sections[et_pb_sections.length - 1]);
    } else {
      // Need to handle this case - no visible sections!
    }
  }

  function initContent($s) {
	var selectorPre = '#' + $s.parent().closest('[id]').attr('id');
	var sClasses = $s.attr('class').split(' ');
	for (var i = 0; i < sClasses.length; ++i) {
		if ( sClasses[i].length > 14 && sClasses[i].substr(0,14) == 'et_pb_section_' ) {
        var sNum = sClasses[i].substr(14);
        if (!isNaN(sNum)) {
          selectorPre += " .et_pb_section_" + sNum;
          break;
        }
      }
    }

    var diviRocketJq = jQuery.fn.find;
    jQuery.fn.find = function() {
      if (
        (!this || !this.context || this.context == document) &&
        arguments[0] &&
        arguments[0].substr
      ) {
        var selectors = arguments[0].split(",");
        var newSelectors = [];
        for (var i = 0; i < selectors.length; ++i) {
          selectors[i] = selectors[i].trim();
				if (selectors[i].substr(0,4) != 'body' && selectors[i].substr(0,4) != 'html' && selectors[i].indexOf('#') == -1) {
					newSelectors.push( selectorPre + ' ' + selectors[i] );
					if (selectors[i][0] == '.') {
						newSelectors.push( selectorPre + selectors[i] );
					}
				} else {
					if (selectors[i] == 'body' || selectors[i] == 'html') {
						selectors[i] = '#divi-rocket-non-existent';
            } else {
              // console.log('=== Skipping selector with ID: ' + selectors[i]);
            }
            newSelectors.push(selectors[i]);
          }
        }

        arguments[0] = newSelectors.join(",");
      }

      return diviRocketJq.apply(this, arguments);
    };

	var isBuilder = 'object' === typeof window.ET_Builder;
	var topWindow = isBuilder ? window.top : window;
    var $et_window = $(topWindow);
    $et_window.off("resize", et_calculate_fullscreen_section_size);
    $et_window.off(
      "et-pb-header-height-calculated",
      et_calculate_fullscreen_section_size
    );

    et_pb_init_modules();

    jQuery.fn.find = diviRocketJq;

    // Trick Divi into initializing the grid for first page of content

    function et_get_animation_classes() {
      return [
        "et_animated",
        "infinite",
        "et-waypoint",
        "fade",
        "fadeTop",
        "fadeRight",
        "fadeBottom",
        "fadeLeft",
        "slide",
        "slideTop",
        "slideRight",
        "slideBottom",
        "slideLeft",
        "bounce",
        "bounceTop",
        "bounceRight",
        "bounceBottom",
        "bounceLeft",
        "zoom",
        "zoomTop",
        "zoomRight",
        "zoomBottom",
        "zoomLeft",
        "flip",
        "flipTop",
        "flipRight",
        "flipBottom",
        "flipLeft",
        "fold",
        "foldTop",
        "foldRight",
        "foldBottom",
        "foldLeft",
        "roll",
        "rollTop",
        "rollRight",
        "rollBottom",
        "rollLeft",
        "transformAnim"
      ];
    }

	$s.find('.et_pb_blog_grid').each(function() {
      var current_href = window.location.href;
      var this_link = $("<a>")
        .attr("href", current_href)
        .text(divirocketlazyload.strings.loading);
      var $current_module = $(this).closest(".et_pb_module");
      var module_classes = $current_module.attr("class").split(" ");
      var module_class_processed = "";
      var animation_classes = et_get_animation_classes();

      // global variable to store the cached content
      window.et_pb_ajax_pagination_cache =
        window.et_pb_ajax_pagination_cache || [];

      // construct the selector for current module
      $.each(module_classes, function(index, value) {
        // skip animation classes so no wrong href is formed afterwards
        if (
          $.inArray(value, animation_classes) !== -1 ||
          "et_had_animation" === value
        ) {
          return;
        }

        if ("" !== value.trim()) {
          module_class_processed += "." + value;
        }
      });

      window.et_pb_ajax_pagination_cache[
        current_href + module_class_processed
      ] = $current_module.find(".et_pb_ajax_pagination_container");
      window.et_pb_ajax_pagination_cache[
        current_href +
          (current_href.indexOf("?") == -1 ? "?" : "&") +
          "et_blog" +
          module_class_processed
      ] =
        window.et_pb_ajax_pagination_cache[
          current_href + module_class_processed
        ];

		window.et_pb_ajax_pagination_cache[ current_href + module_class_processed ] = $current_module.find( '.et_pb_ajax_pagination_container' );
		window.et_pb_ajax_pagination_cache[ current_href + (current_href.indexOf('?') == -1 ? '?' : '&') + 'et_blog' + module_class_processed ] = window.et_pb_ajax_pagination_cache[ current_href + module_class_processed ];
		
		
		salvattore.registerGrid( $current_module.find( '.et_pb_salvattore_content' )[0] );

      this_link
        .appendTo(
          $(this)
            .find(
              ".et_pb_ajax_pagination_container .wp-pagenavi, .et_pb_ajax_pagination_container .pagination"
            )
            .first()
        )
        .click()
        .remove();
    });

    for (var i = 0; i < _divi_rocket_lazy_compat.length; ++i) {
      _divi_rocket_lazy_compat[i]($s);
    }
  }

  function et_init_audio_modules() {
    if (typeof jQuery.fn.mediaelementplayer === "undefined") {
      return;
    }

    getOutsideVB(".et_audio_container").each(function() {
      var $this = jQuery(this);

      if ($this.find(".mejs-container:first").length > 0) {
        return;
      }

      $this.find("audio").mediaelementplayer(window._wpmejsSettings);
    });
  }

  let et_pb_use_builder = divirocketlazyload.et_pb_use_builder;

	if ( "true" === divirocketlazyload.should_we_cache || null == divirocketlazyload.should_we_cache ) {
		if ( !jQuery( '.et-l--body' ).length ) {
      lazyDivs();
    }
  }
});

/*
intersection-observer-polyfill

Copyright (c) 2016 Jeremias Menichelli

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Modified by Jonathan Hall
*/
function divi_rocket_IO(_win) {
  "use strict";

  if ("IntersectionObserver" in _win) {
    //return _win.IntersectionObserver;
  }

  /**
   * IntersectionObserver polyfill (c) 2016 @jeremenichelli | Licensed MIT
   */
  var viewportH = _win.innerHeight,
    nowOffset = Date.now();

  // error headers
  var constructError = "Failed to construct 'Intersection': ",
    observeError = "Failed to execute 'observe': ",
    unobserveError = "Failed to execute 'unobserve': ";

  // requestAnimationFrame alias
  var rAF = _win.requestAnimationFrame;

  /**
   * Returns current time
   * @method now
   * @returns {Number} Execution time
   */
  var now = function() {
    return _win.performance && _win.performance.now
      ? performance.now()
      : Date.now() - nowOffset;
  };

  /*
   * Observer constructor
   * @constructor IntersectionObserver
   * @param {Function} callback
   * @param {Object} options
   */
  var IntersectionObserver = function(callback, options) {
    var that = this;

    // throw error if callback is not a funciton
    if (typeof callback !== "function") {
      throw new TypeError(
        constructError +
          "The callback provided as parameter 1 is not a function"
      );
    }

    this.root = options.root || null;
    this.threshold = options.threshold || [0];

    // accept number as threshold option
    if (typeof this.threshold === "number") {
      this.threshold = [this.threshold];
    }

    // throw error when threshold value is out of range
    if (this.threshold[0] > 1 || this.threshold[0] < 0) {
      throw new RangeError(
        constructError + "Threshold values must be between 0 and 1"
      );
    }

    // create array for observable elements
    var elements = [];

    // debounce frame call
    var ticking = false;

    // trigger actions when elements become visible
    function trigger(target) {
      var arr = target ? [target] : elements,
        count = arr.length,
        changes = [],
        el,
        rect;

      while (count) {
        --count;

        el = arr[count];
        rect = el.getBoundingClientRect();

        if (rect.top < viewportH && rect.top >= -rect.height) {
          changes.push({
            boundingClientRect: rect,
            target: el,
            time: now()
          });
        }
      }

      // execute callback with array of changes and reference to the observer
      callback.apply(that, [changes, that]);

      // clean variables to prevent memory leaks
      arr = count = changes = el = rect = null;

      // reset ticking
      ticking = false;
    }

    function debounceTrigger() {
      if (!ticking) {
        rAF(trigger.bind(_win, null));
      }
      ticking = true;
    }

    // add element to array of observable elements
    this.observe = function(target) {
      if (target instanceof _win.Element) {
        // reincorporate scroll listener if array is empty
        if (elements.length === 0) {
          _win.addEventListener("scroll", debounceTrigger);
        }

        elements.push(target);

        // trigger in case element is already visible
        trigger(target);
      } else {
        throw new TypeError(
          observeError + "parameter 1 is not of type 'Element'"
        );
      }
    };

    // remove element from array of observable elements
    this.unobserve = function(target) {
      if (target instanceof _win.Element) {
        var index = elements.indexOf(target);

        if (index !== -1) {
          elements.splice(index, 1);
        }

        // remove scroll listener array is empty
        if (elements.length === 0) {
          _win.removeEventListener("scroll", debounceTrigger);
        }
      } else {
        throw new TypeError(
          unobserveError + "parameter 1 is not of type 'Element'"
        );
      }
    };

    // stop observing
    this.disconnect = function() {
      _win.removeEventListener("scroll", debounceTrigger);
    };
  };

  // update viewport metrics
  _win.addEventListener("resize", function() {
    viewportH = _win.innerHeight;
  });

  return IntersectionObserver;
}
// End intersection-observer-polyfill
