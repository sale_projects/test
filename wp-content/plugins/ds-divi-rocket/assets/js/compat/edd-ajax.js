/*
This file was copied from Easy Digital Downloads, copyright Easy Digital Downloads, released under the
GNU General Public License (GPL) version 2 or later, licensed in this project under
GPL version 3 or later. See ../../../license.txt.

Modified by Jonathan Hall; last modified 2020-02-17
*/

jQuery(document).ready(function($) {
  window.divi_rocket_lazy_compat &&
    divi_rocket_lazy_compat(function($newSection) {
      // Hide unneeded elements. These are things that are required in case JS breaks or isn't present
      $newSection.find(".edd-no-js").hide();
      $newSection.find("a.edd-add-to-cart").addClass("edd-has-js");
    });
}); // end document ready
