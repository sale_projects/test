/*
This file includes code based on and/or copied from parts of Jetpack,
copyright Automattic, released under the GNU General Public License
(GPL) version 2 or later, licensed under GPL version 3 (see ../../../license.txt).

This file modified by Jonathan Hall; last modified 2020-02-17
*/

jQuery(document).ready(function($) {
  window.divi_rocket_lazy_compat &&
    divi_rocket_lazy_compat(function() {
      $("body").trigger("jetpack-lazy-images-load");
    });
});
