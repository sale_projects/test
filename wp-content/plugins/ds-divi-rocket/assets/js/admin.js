jQuery(document).ready(function($) {
  $(
    "#wp-admin-bar-divi-space-cache-clear-current a, #wp-admin-bar-divi-space-cache-clear-all a"
  ).click(function() {
    var $adminBarItem = $("#wp-admin-bar-divi-rocket").addClass(
      "divi-space-cache-clear-processing"
    );
    var $adminBarItemText = $adminBarItem.find("a span");
    var adminBarItemTextOriginal = $adminBarItemText.text();
    $adminBarItemText.text(diviRocketStringsAdmin.clearingCache);

    var errorHandler = function() {
      $adminBarItem
        .removeClass("divi-space-cache-clear-processing")
        .addClass("divi-space-cache-clear-error");

      $adminBarItemText.text(diviRocketStringsAdmin.errorClearingCache);

      setTimeout(function() {
        $adminBarItem.removeClass("divi-space-cache-clear-error");
        $adminBarItemText.text(adminBarItemTextOriginal);
      }, 5000);
    };

    $.get($(this).attr("href"))
      .success(function(data) {
        if (data.success) {
          $adminBarItem
            .removeClass("divi-space-cache-clear-processing")
            .addClass("divi-space-cache-clear-success");

          $adminBarItemText.text(
            diviRocketStringsAdmin.successfullyClearedCache
          );

          setTimeout(function() {
            $adminBarItem.removeClass("divi-space-cache-clear-success");
            $adminBarItemText.text(adminBarItemTextOriginal);
          }, 5000);
        } else {
          errorHandler();
        }
      })
      .fail(errorHandler);

    return false;
  });
}); // end document ready
