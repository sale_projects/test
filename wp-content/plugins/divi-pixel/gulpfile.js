const { watch, series, src, dest } = require('gulp');
const sass = require('gulp-sass');
const rename = require("gulp-rename");
const uglify = require("gulp-uglify");
const sourcemaps = require("gulp-sourcemaps");
const browserify = require("browserify");
const babelify = require("babelify");
const source = require("vinyl-source-stream");
const buffer = require("vinyl-buffer");
const tap = require('gulp-tap');


function buildIncludesJs(){
    return buildJs('includes/js/**/*.js');
}

function buildPublicJs(){
    return buildJs('public/js/**/*.js');
}

function buildJs (path) {
    return src(path, {read: false})
      .pipe(tap(function (file) {
        console.log('bundling ' + file.path);
        file.contents = browserify(file.path, {debug: true})
            .transform(babelify)
            .bundle();
    }))
    .pipe(rename({ 
        extname: ".min.js",
        dirname: ''
    }))
    .pipe(buffer())
    .pipe(sourcemaps.init({ loadMaps: true }))
    .pipe(uglify())
    .pipe(sourcemaps.write("./"))
    .pipe(dest('dist/js'));
}

function buildPublicCSS(){
    return src('public/css/*.css')
        .pipe(sass({
            errorLogToConsole: true,
            outputStyle: 'compressed'
        }))
        .on('error', console.error.bind(console))
        .pipe(rename({suffix: '.min'}))
        .pipe(dest('dist/css'));
}

// Watcher looks at changes to given files and runs the provided function
function watcher() {
    watch(['public/js/**/*.js'], buildPublicJs);
    watch(['includes/js/**/*.js'], buildIncludesJs);
    watch(['public/css/**/*.css'], buildPublicCSS);
}


// Run "gulp watch" to execute
exports.watch = series(buildPublicJs, buildIncludesJs, buildPublicCSS, watcher);

// Run "gulp" to execute
exports.default = series(buildPublicJs, buildIncludesJs, buildPublicCSS);