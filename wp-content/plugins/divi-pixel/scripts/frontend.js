if (window.ETBuilderBackend && window.ETBuilderBackend.defaults) {
    window.ETBuilderBackend.defaults.dipi_before_after_slider = {
        before_image: window.DiviPixelBuilderData.defaults.image,
        after_image: window.DiviPixelBuilderData.defaults.image
    };
}