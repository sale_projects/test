//TODO: Make alert texts translatable
jQuery(function($) {

    window.dipi = {};

    setup_sticky_sidebar();
    setup_show_if();
    setup_dipi_switches();
    setup_sidebar();
    setup_file_uploads();
    setup_reset_settings_button();
    setup_reset_customizer_settings_button();
    setup_activate_license_button();
    setup_deactivate_license_button();
    setup_menu_styles();
    setup_export_settings_button();
    setup_import_settings_button();

    function setup_reset_settings_button() {
        $('.dipi_reset_settings_button').on('click', function() {
            let confirmation = confirm(dipi_settings.i18n.confirm_reset_settings);
            if (confirmation == true) {

                let data = {
                    action: "dipi_reset_settings",
                    nonce: dipi_settings.nonces.reset_settings
                }

                //Abort if ajax call is already running
                if (window.dipi.reset_settings_ajax !== undefined && window.dipi.reset_settings_ajax !== null && window.dipi.reset_settings_ajax.readyState !== 4) {
                    alert(dipi_settings.i18n.call_in_progress);
                    return;
                }

                window.dipi.reset_settings_ajax = $.ajax({
                    url: dipi_settings.ajax_url,
                    type: 'POST',
                    data: data,
                    success: function(response) {
                        if (response.success === false) {
                            alert("Error: " + response.data.error);
                        } else {
                            alert(response.data.success_message);
                            location.reload();
                        }
                    },
                    complete: function() {
                        window.dipi.reset_settings_ajax = null;
                    }
                });
            }
        });
    }

    function setup_reset_customizer_settings_button() {
        $('.dipi_reset_customizer_settings_button').on('click', function() {
            if (!confirm(dipi_settings.i18n.confirm_reset_customizer_settings)) {
                return;
            }

            let data = {
                action: "dipi_reset_customizer_settings",
                nonce: dipi_settings.nonces.reset_customizer_settings
            }

            //Abort if ajax call is already running
            if (window.dipi.reset_customizer_settings_ajax !== undefined && window.dipi.reset_customizer_settings_ajax !== null && window.dipi.reset_customizer_settings_ajax.readyState !== 4) {
                alert(dipi_settings.i18n.call_in_progress);
                return;
            }

            window.dipi.reset_customizer_settings_ajax = $.ajax({
                url: dipi_settings.ajax_url,
                type: 'POST',
                data: data,
                success: function(response) {
                    if (response.success === false) {
                        alert("Error: " + response.data.error);
                    } else {
                        alert(response.data.success_message);
                        // location.reload();
                    }
                },
                complete: function() {
                    window.dipi.reset_customizer_settings_ajax = null;
                }
            });
        });
    }

    function setup_activate_license_button() {
        $('.dipi_activate_license_button').on('click', function() {
            let data = {
                action: "dipi_activate_license",
                nonce: dipi_settings.nonces.activate_license,
                license: $("#dipi_license").val()
            }

            //Abort if ajax call is already running
            if (window.dipi.activate_license_ajax !== undefined && window.dipi.activate_license_ajax !== null && window.dipi.activate_license_ajax.readyState !== 4) {
                alert(dipi_settings.i18n.call_in_progress);
                return;
            }

            // let $this = $(this);
            let $button_text = $(this).find(".button_text");
            let $loading_indicator = $(this).find(".loading_indicator");
            $button_text.animate({ opacity: 0 }, 300);
            $loading_indicator.animate({ opacity: 1 }, 300);

            window.dipi.activate_license_ajax = $.ajax({
                url: dipi_settings.ajax_url,
                type: 'POST',
                data: data,
                success: function(response) {
                    if (response.success === false) {
                        alert(response.data.error);
                        return;
                    }

                    if (response.data.license_status === 'invalid') {
                        window.dipi.activate_license_ajax = null;
                        $button_text.animate({ opacity: 1 }, 300);
                        $loading_indicator.animate({ opacity: 0 }, 300);
    
                        alert(response.data.success_message);
                        return;
                    }

                    location.reload();
                }
            });
        });
    }

    function setup_deactivate_license_button() {
        $('.dipi_deactivate_license_button').on('click', function() {
            let data = {
                action: "dipi_deactivate_license",
                nonce: dipi_settings.nonces.deactivate_license
            }

            //Abort if ajax call is already running
            if (window.dipi.deactivate_license_ajax !== undefined && window.dipi.deactivate_license_ajax !== null && window.dipi.deactivate_license_ajax.readyState !== 4) {
                alert(dipi_settings.i18n.call_in_progress);
                return;
            }

            let $this = $(this);
            let $button_text = $(this).find(".button_text");
            let $loading_indicator = $(this).find(".loading_indicator");
            $button_text.animate({ opacity: 0 }, 300);
            $loading_indicator.animate({ opacity: 1 }, 300);

            window.dipi.deactivate_license_ajax = $.ajax({
                url: dipi_settings.ajax_url,
                type: 'POST',
                data: data,
                success: function(response) {
                    if (response.success === false) {
                        alert(response.data.error);
                        return;
                    }
                    location.reload();
                }
            });
        });
    }

    function setup_export_settings_button() {
        $('.dipi_export_button').on('click', function() {
            var form = $('<form></form>').attr('method', 'post');
            form.append($("<input></input>").attr('type', 'hidden').attr('name', 'nonce').attr('value', dipi_settings.nonces.export_settings));
            form.append($("<input></input>").attr('type', 'hidden').attr('name', 'action').attr('value', 'dipi_export_settings'));
            form.append($("<input></input>").attr('type', 'hidden').attr('name', 'export_settings').attr('value', get_value_from_input($('#dipi_export_settings'))));
            form.append($("<input></input>").attr('type', 'hidden').attr('name', 'export_customizer').attr('value', get_value_from_input($('#dipi_export_customizer'))));
            form.appendTo('body').submit().remove();
        });
    }

    function setup_import_settings_button() {
        $('.dipi_import_button').on('click', function() {
            var form = $('<form></form>').attr('method', 'post').attr('enctype', 'multipart/form-data');
            form.append($("<input></input>").attr('type', 'hidden').attr('name', 'nonce').attr('value', dipi_settings.nonces.import_settings));
            form.append($("<input></input>").attr('type', 'hidden').attr('name', 'action').attr('value', 'dipi_import_settings'));
            form.append($("#dipi_import_file").clone());
            form.appendTo('body').submit().remove();
        });
    }

    function setup_sticky_sidebar() {
        $(".dipi_sidebar_tabs").stickySidebar({
            topSpacing: 32,
            bottomSpacing: 0
        });
    }

    function update_sticky_sidebar() {
        // window.dipi.stickySidebar.update();
    }

    function setup_show_if() {
        $('[data-depends-on]').each(function() {
            let $currentField = $(this);
            show_or_hide_field($currentField, false);
        });

        $('[data-field-id]').on('change', 'input', setting_changed);
        $('[data-field-id]').on('change', 'select', setting_changed);

        update_separators();
    }

    function setting_changed() {
        let $field = $(this).parents('[data-field-id]');
        let field_id = $field.attr("data-field-id");

        $(`[data-depends-on-${field_id}]`).each(function() {
            let $currentField = $(this);
            show_or_hide_field($currentField, true);
        });

        update_separators();
    }

    function show_or_hide_field($currentField, animated) {
        //Hole die Felder, auf welchem dependet wird
        $dependsOn = $currentField.attr("data-depends-on").split(",");
        $shouldShow = true;
        $dependsOn.forEach(function(element) {
            let $input = $(`[data-field-id=${element}] input, [data-field-id=${element}] select`);
            let currentVal = get_value_from_input($input);
            let requiredVals = $currentField.attr(`data-depends-on-${element}`).split(",");

            if (requiredVals.indexOf(currentVal) === -1) {
                //TODO: Hide the current field
                $shouldShow = false;
            }
        });

        if ($shouldShow) {
            $currentField.attr("data-visible", "true");
            $currentField.addClass("visible");
            $currentField.slideDown(animated ? "slow" : 0);
        } else {
            $currentField.attr("data-visible", "false");
            $currentField.removeClass("visible");
            $currentField.slideUp(animated ? "slow" : 0);
        }
        //TODO: Sollten wir das erst tun, wenn slideUp/Down fertig ist?
        update_sticky_sidebar();
    }

    function update_separators() {
        $(".dipi_settings_option.dipi_separator").removeClass("dipi_separator");
        $(".dipi_settings_toggle").each(function() {
            $(this).find(".dipi_settings_option.visible:not(:last)").addClass("dipi_separator");
        });

    }

    function get_value_from_input($input) {
        switch ($input.attr("type")) {
            case "checkbox":
                return $input.prop('checked') ? "on" : "off";
            case "text":
                return $input.val();
            case "select":
                return $input.children("option:selected").val(); //FIXME: fixme
        }
        return "";
    }

    function setup_sidebar() {
        $('.dipi_settings_sidebar').on('click', '.dipi_settings_tab_control', toggle_tab_content);
        $('#dipi_toolbar_icons').on('click', '.tab-btn', toggle_tab_content);
        $("#dipi_settings .dipi_settings_tab_control:first-of-type").trigger('click');
    }

    function toggle_tab_content() {
        let clicked_tab = $(this).attr("data-tab");
        if (clicked_tab === $("#dipi_settings .dipi_settings_content .dipi_settings_tab.active").attr("data-tab")) {
            return;
        }
        //Fade out previous tab content in content area and fade in new tab content
        $("#dipi_settings .dipi_settings_content .dipi_settings_tab.active").slideUp();
        $(`#dipi_settings .dipi_settings_content .dipi_settings_tab[data-tab="${clicked_tab}"]`).slideDown();

        //Switch .active class from previous selected to new tab
        $("#dipi_settings .active[data-tab]").removeClass("active");
        $(`#dipi_settings [data-tab="${clicked_tab}"]`).addClass("active");

        //Update sticky sidebar
        update_sticky_sidebar();

        // Scroll to top
        $('html, body').animate({ scrollTop: ($('#wpbody').offset().top - $('#wpadminbar').height()) }, 'slow');
    }

    function setup_dipi_switches() {
        $(".dipi_switch").hurkanSwitch({
            'width': 90
        });
    }

    function setup_file_uploads() {
        // The "Upload" button
        $('.dipi_upload_image_button').on('click', function(e) {
            e.preventDefault();
            var button = $(this);

            let mediaFrameItem = wp.media.frames.items = wp.media({
                library: {
                    type: 'image'
                },
                multiple: false
            });
   
            mediaFrameItem.on('select', function() {
                // We set multiple to false so only get one file from the uploader
                attachment = mediaFrameItem.state().get('selection').first().toJSON();
                $(button).prev().val(attachment.url);
            });
            mediaFrameItem.open();
            return false;
        });

    }

    function setup_menu_styles() {
        setInterval(function() {
            $("#dipi_settings .dipi_menu_styles .dipi_menu_style").toggleClass("active");
        }, 1500);
    }
});