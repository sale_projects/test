jQuery(function($) {
    $(document).ready(function() {
        $('#dipi_settings_form').submit(function() {
            let $submitButton = $(".dipi_submit_button");
            let $saveText = $submitButton.find(".save-text");
            let $savedText = $submitButton.find(".saved-text");
            let $loadingIndicator = $submitButton.find(".loading-indicator");

            $saveText.animate({ opacity: 0 }, 300);
            $loadingIndicator.animate({ opacity: 1 }, 300);

            var options = $(this).serialize();
            $.post('options.php', options)
                .success(function() {})
                .error(function() {
                    alert('An error occured while saving. Please try again');
                })
                .complete(function() {
                    $loadingIndicator.animate({ opacity: 0 }, 300);
                    $savedText.animate({ opacity: 1 }, 300);
                    setTimeout(function() {
                        $savedText.animate({ opacity: 0 }, 300);
                        $saveText.animate({ opacity: 1 }, 300);
                    }, 1500);
                });
            return false;
        });

        $('.dipi-google-button').click(function(e) {
            e.preventDefault();

            var $this = $(this);
            $.ajax({
                url: dipi_vars.ajaxurl,
                data: {
                    action: 'dipi_google_review',
                    security: dipi_vars.google_nonce
                },
                type: "post",

                beforeSend: function() {
                    $this.text('Reviews Download....');
                },

                success: function(res) {
                    var t = setTimeout(function() {
                        $this.css('animation', '')
                            .removeClass('dipi-google-button')
                            .addClass('dipi-sync-disabled')
                            .text('Reviews Saved');

                        clearTimeout(t);
                    }, 1500);
                },

                error: function(res) {
                    console.error(res);
                }

            });
        });

        $('.dipi-facebook-button').click(function(e) {
            e.preventDefault();

            var $this = $(this);

            $.ajax({
                url: dipi_vars.ajaxurl,
                data: {
                    action: 'dipi_facebook_review',
                    security: dipi_vars.facebook_nonce
                },
                type: "post",

                beforeSend: function() {
                    $this.text('Reviews Download....');
                },

                success: function(res) {
                    var t = setTimeout(function() {
                        $this.css('animation', '')
                            .removeClass('dipi-facebook-button')
                            .addClass('dipi-sync-disabled')
                            .text('Reviews Saved');

                        clearTimeout(t);
                    }, 1500);
                },

                error: function(res) {
                    console.error(res);
                }
            });
        });

        $('.dipi-tippy').each(function(i, item){
            tippy(item, {
                content: item.getAttribute('data-tooltip'),
                placement: 'bottom',
                theme: 'dipi-light',
                animation: 'shift-toward'
            });
        })
    });
});