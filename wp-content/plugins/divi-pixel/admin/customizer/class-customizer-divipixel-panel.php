<?php

class Custmoizer_DiviPixel_Panel extends \WP_Customize_Panel {
    public $type  ="dipi";
    public $icon;

    public function __construct( $manager, $id, $args = array() ) {
        parent::__construct($manager, $id, $args);
        if(isset($args['icon'])){
            $this->icon = $args['icon'];
        }
    }

    protected function render() {
        ?>
        <li id="accordion-panel-<?php echo $this->id; ?>" class="accordion-section control-section control-panel control-panel-<?php echo $this->type; ?>">
            <h3 class="accordion-section-title" tabindex="0">
                <?php if(isset($this->icon)): ?>
                <?php \DiviPixel\DIPI_Customizer_API::include_icon($this->icon); ?>
                <?php endif; ?>
                <?php echo $this->title; ?>
                <span class="screen-reader-text"><?php _e( 'Press return or enter to open this panel' ); ?></span>
            </h3>
            <ul class="accordion-sub-container control-panel-content"></ul>
        </li>
        <?php
    }
    protected function render_content() {
        ?>
        <li class="panel-meta customize-info accordion-section <?php if(isset($this->description)) { echo 'cannot-expand'; } ?>">
            <button class="customize-panel-back" tabindex="-1"><span class="screen-reader-text"><?php _e( 'Back' ); ?></span></button>
            <div class="accordion-section-title">
                <span class="preview-notice">
                <?php
                    /* translators: %s: the site/panel title in the Customizer */
                    $html = sprintf('<strong class="panel-title">%1$s</strong>', $this->title);
                    echo sprintf( __( 'You are customizing %s' ), $html );
                ?>
                </span>
                <?php if(isset($this->description)): ?>
                    <button type="button" class="customize-help-toggle dashicons dashicons-editor-help" aria-expanded="false"><span class="screen-reader-text"><?php _e( 'Help' ); ?></span></button>
                <?php endif; ?>
            </div>
            <?php if(isset($this->description)): ?>
                <div class="description customize-panel-description">
                    <?php echo $this->description; ?>
                </div>
            <?php endif; ?>
            <div class="customize-control-notifications-container"></div>
        </li>
        <?php
    }
}