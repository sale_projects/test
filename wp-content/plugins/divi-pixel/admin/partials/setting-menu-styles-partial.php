<?php
$additional_class = isset($field["class"]) ? $field["class"] : "";
$options = $field["options"];
?>

<?php echo $ribbon; ?>
<div class="dipi_row">
    <div class="dipi_settings_option_description col-md-12">
        <div class="dipi_option_label">
            <?php echo $field["label"]; ?>
        </div>
        <?php if (isset($field["description"]) && '' !== $field["description"]) : ?>
        <div class="dipi_option_description">
            <?php echo $field["description"]; ?>
        </div>        
        <?php endif; ?>
        <div class="dipi_settings_option_field dipi_settings_option_field_menu_styles dipi_settings_option_field_multiple_buttons <?php echo $additional_class; ?>">
            <?php foreach($options as $option_id => $option_title) : ?>
            <div class="dipi_radio_option">
                <input type='radio'
                        name='<?php echo $id; ?>'
                        id='<?php echo $id . "_" . $option_id; ?>'
                        value='<?php echo $option_id; ?>'
                        <?php checked( $option_id, $value ); ?>
                />
                <label for='<?php echo $id . "_" . $option_id; ?>'>
                    <div class="dipi_menu_style <?php echo $option_id; ?>">
                        <span><?php echo $option_title; ?></span>
                    </div>
                </label>
            </div>
            <?php endforeach; ?> 
        </div> 
    </div>
</div>