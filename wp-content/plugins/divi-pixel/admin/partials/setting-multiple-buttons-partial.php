<?php
if(!isset($field["options"])){
    return;
}
$options = $field["options"];
$additional_class = isset($field["class"]) ? $field["class"] : "";
?>

<?php echo $ribbon; ?>
<div class="dipi_row">
    <div class="dipi_settings_option_description col-md-6">
        <div class="dipi_option_label">
            <?php echo $field["label"]; ?>
        </div>
        <?php if (isset($field["description"]) && '' !== $field["description"]) : ?>
        <div class="dipi_option_description">
            <?php echo $field["description"]; ?>
        </div>        
        <?php endif; ?>
    </div>
    <div class="dipi_settings_option_field dipi_settings_option_field_multiple_buttons col-md-6 <?php echo $additional_class; ?>">
        <?php foreach($options as $option_id => $option) : ?>
        <div class="dipi_radio_option">
            <input type='radio'
                    name='<?php echo $id; ?>'
                    id='<?php echo $id . "_" . $option_id; ?>'
                    value='<?php echo $option_id; ?>'
                    <?php checked( $option_id, $value ); ?>
            />
            <label for='<?php echo $id . "_" . $option_id; ?>'>
                <?php if(isset($option['icon'])) : ?>
                <span class="dipi_radio_option_icon <?php echo $option["icon"]; ?>"></span>
                <?php endif; ?>
                <?php if(isset($option['title'])) : ?>
                <span class="dipi_radio_option_title"><?php echo $option["title"]; ?></span>
                <?php endif; ?>
                <?php if(isset($option['description'])) : ?>
                <span class="dipi_radio_option_description"><?php echo $option["description"]; ?></span>
                <?php endif; ?>
                <?php if(isset($option['image'])) : ?>
                <img src="<?php echo  plugin_dir_url(__FILE__) . '../assets/' . $option['image']; ?>" /> <!-- TODO: maybe use a wrapper for the image? -->
                <?php endif; ?>
                <?php if(isset($option['svg'])) : ?>
                <!-- FIXME: maybe use include rather than file_get_contents as the later can cause issues on some server (at least on frontend) -->
                <?php echo file_get_contents($option['svg']); ?>
                <?php endif; ?>
            </label>
        </div>
        <?php endforeach; ?> 
    </div> 
</div>