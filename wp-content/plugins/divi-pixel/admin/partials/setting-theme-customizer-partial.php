<?php
$label = $field["label"];
$description = isset($field["description"]) && $field["description"] && '' !== $field["description"] ? $field["description"] : "";
$icon = isset($field['icon']) && $field['icon'] ? $field['icon'] : 'dp-back-to-top';
$theme_customizer_url = "customize.php";
if (isset($field['customizer_panel'])) {
    $theme_customizer_url .= "?autofocus[panel]=dipi_{$field['customizer_panel']}";
} else if (isset($field['customizer_section'])) {
    $theme_customizer_url .= "?autofocus[section]=dipi_customizer_section_{$field['customizer_section']}";
}
$theme_customizer_url = admin_url($theme_customizer_url);
$additional_class = isset($field["class"]) ? $field["class"] : "";
?>
<div class="dipi_settings_option_field_theme_customizer <?php echo $additional_class; ?>">
    <span class="theme_customizer_icon <?php echo $icon; ?>"></span>    
    <div class="dipi_settings_option_description">
        <?php echo $ribbon; ?>
        <div class="dipi_option_label">
            <?php echo $label; ?>
        </div>
        <?php if ('' !== $description) : ?>
            <div class="dipi_option_description">
                <?php echo $description; ?>
            </div>
        <?php endif; ?>
    </div>
    <div class="dipi_option_theme_customizer_button_wrapper">
        <a class="dipi_option_theme_customizer_button" href="<?php echo $theme_customizer_url; ?>" target="_blank">
            <span class="theme_customizer_url"><?php echo esc_html__('Theme Customizer', 'dipi-divi-pixel'); ?></span>&nbsp;
            <span class="button_icon dp-settings"></span>
        </a>
    </div>
</div>