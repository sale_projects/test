
<?php 
$label = $field["label"];
$description = isset($field["description"]) ? $field["description"] : '';
$library_layouts = ['-1' => ['title' => esc_html__('-- Select a Layout --', 'dipi-divi-pixel')]];
$library_layouts += $this->get_library_layouts();
?>
<?php echo $ribbon; ?>
<div class="dipi_row">
    <div class="dipi_settings_option_description col-md-6">
        <div class="dipi_option_label">
            <?php echo $label; ?>
        </div>
        <?php if ('' !== $description) : ?>
        <div class="dipi_option_description">
            <?php echo $description; ?>
        </div>        
        <?php endif; ?>
    </div>
    <div class="dipi_settings_option_field dipi_settings_option_field_select dipi_settings_option_field_library_layout col-md-6">
        <div class="dipi_select">
            <select name='<?php echo $id; ?>' id='<?php echo $id; ?>'>
            <?php foreach($library_layouts as $layout_id => $library_layout) : ?>
                <option value='<?php echo $layout_id; ?>' <?php selected( $layout_id, $value ); ?>>
                    <?php echo $library_layout['title']; ?>
                </option>
            <?php endforeach; ?>
            </select>
        </div> 
    </div> 
</div>