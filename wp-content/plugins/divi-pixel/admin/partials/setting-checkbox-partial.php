<?php
$off = isset($field["options"]["off"]) ? $field["options"]["off"] : esc_html__( 'Off', 'dipi-divi-pixel' );
$on = isset($field["options"]["on"]) ? $field["options"]["on"] : esc_html__( 'On', 'dipi-divi-pixel' );

$checked = isset($value) && ($value === true || $value === 'on' || $value === 'yes' || $value === 'true' || $value === 1) ? 'checked="checked"' : '';
$disabled = isset($field["coming_soon"]) ? 'disabled' : '';
?>

<?php echo $ribbon; ?>        
<div class="dipi_row">
    <div class="dipi_settings_option_description col-md-6">
        <div class="dipi_option_label">
            <?php echo $field["label"]; ?>
        </div>
        <?php if (isset($field["description"]) && '' !== $field["description"]) : ?>
        <div class="dipi_option_description">
            <?php echo $field["description"]; ?>
        </div>        
        <?php endif; ?>
    </div>
    <div class="dipi_settings_option_field dipi_settings_option_field_checkbox col-md-6 dipi_switch <?php echo $disabled; ?>">
        <input type='checkbox' 
                name='<?php echo $id; ?>' 
                id='<?php echo $id; ?>' 
                data-off-title='<?php echo $off; ?>'
                data-on-title='<?php echo $on; ?>' 
                <?php echo $checked ?>
                <?php echo $disabled; ?>
        />
    </div> 
</div>