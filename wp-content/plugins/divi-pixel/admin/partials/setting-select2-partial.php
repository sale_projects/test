<?php

if(!isset($field["options"])){
    return;
}

if(isset($field["computed"]) && $field["computed"] === true ){
    $options = call_user_func($field["options"]);
} else {
    $options = $field["options"];
}

?>

<?php echo $ribbon; ?>

<div class="dipi_row">
    <div class="dipi_settings_option_description col-md-6">
        <div class="dipi_option_label">
            <?php echo $field["label"]; ?>
        </div>
        <?php if (isset($field["description"]) && '' !== $field["description"]) : ?>
        <div class="dipi_option_description">
            <?php echo $field["description"]; ?>
        </div>        
        <?php endif; ?>
    </div>
    <div class="dipi_settings_option_field dipi_settings_option_field_select col-md-6">
        <div class="dipi_select2">
            <select multiple="multiple" name='<?php echo $id; ?>[]' id='<?php echo $id; ?>'>
        <?php
            foreach($options as $option_id => $option_title) : 
                $selected = is_array($value) && in_array($option_id, $value ) ? ' selected="selected" ' : '';
        ?>
                <option value='<?php echo $option_id; ?>' <?php echo $selected; ?>>
                    <?php echo $option_title; ?>
                </option>

            <?php endforeach; ?>
            </select>
        </div>  
    </div> 
</div>

<script type="text/javascript">
jQuery(function ($) {
    $(document).ready(function () {
        $('#<?php echo $id; ?>').select2({
            placeholder: "-- Select Pages --",
            tags: true
        });
    });
});
</script>