<?php
if(!isset($field["options"])){
    return;
}
$options = $field["options"];
?>

<?php echo $ribbon; ?>
<div class="dipi_row">
    <div class="dipi_settings_option_description col-md-6">
        <div class="dipi_option_label">
            <?php echo $field["label"]; ?>
        </div>
        <?php if (isset($field["description"]) && '' !== $field["description"]) : ?>
        <div class="dipi_option_description">
            <?php echo $field["description"]; ?>
        </div>        
        <?php endif; ?>
    </div>
    <div class="dipi_settings_option_field dipi_settings_option_field_select col-md-6">
        <div class="dipi_select">
            <select type="select" name='<?php echo $id; ?>' id='<?php echo $id; ?>'>
            <?php foreach($options as $option_id => $option_title) : ?>
                <option value='<?php echo esc_attr($option_id); ?>' <?php selected( esc_attr($option_id), esc_attr($value) ); ?>>
                    <?php echo $option_title; ?>
                </option>
            <?php endforeach; ?>
            </select>
        </div>  
    </div> 
</div>