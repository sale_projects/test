
<?php 
$label = $field["label"];
$description = isset($field["description"]) && $field["description"] && '' !== $field["description"] ? $field["description"] : "";
$placeholder = isset($field["placeholder"]) && $field["placeholder"] ? esc_attr($field['placeholder']) : "";
$additional_class = isset($field["class"]) ? $field["class"] : "";
?>
<?php echo $ribbon; ?>
<div class="dipi_row <?php echo $additional_class; ?>">
    <div class="dipi_settings_option_description col-md-6">
        <div class="dipi_option_label">
            <?php echo $label; ?>
        </div>
        <?php if ('' !== $description) : ?>
        <div class="dipi_option_description">
            <?php echo $description; ?>
        </div>        
        <?php endif; ?>
    </div>
    <div class="dipi_settings_option_field dipi_settings_option_field_text col-md-6">
        <input type='text' name='<?php echo $id; ?>' id='<?php echo $id; ?>' value='<?php echo esc_attr($value); ?>' placeholder='<?php echo esc_attr($placeholder); ?>'/>
    </div> 
</div>