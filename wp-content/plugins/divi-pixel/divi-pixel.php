<?php
/*
Plugin Name: Divi Pixel
Plugin URI:  https://www.divi-pixel.com
Description: Divi Pixel is an all-in-one solution for all Divi users, from absolute beginners to professionals.
Version:     2.2.1
Author:      Octolab OÜ
Author URI:  https://www.divi-pixel.com
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Text Domain: dipi-divi-pixel
Domain Path: /languages
 */

/*************
 * Constants *
 *************/
define('DIPI_PLUGIN_FILE', __FILE__);
define('DIPI_VERSION', '2.2.1');
define('DIPI_ITEM_ID', 32718);
define('DIPI_BASE', plugin_basename(DIPI_PLUGIN_FILE));
define('DIPI_DIR', plugin_dir_path(DIPI_PLUGIN_FILE));
define('DIPI_URI', plugins_url('/', DIPI_PLUGIN_FILE));
define('DIPI_PASSWORD_MASK', "************************");
define('DIPI_STORE_URL', 'https://www.divi-pixel.com');
define('DIPI_AUTHOR', 'Divi Pixel');

/*********
 * DEBUG *
 *********/
if (!function_exists('dipi_log')) {
    function dipi_log($log)
    {
        if(!defined('DIPI_DEBUG_LOG') || constant('DIPI_DEBUG_LOG') !== true){
            return;
        }

        if (is_array($log) || is_object($log)) {
            error_log("Divi Pixel: " . print_r($log, true));
        } else {
            error_log("Divi Pixel: " . $log);
        }
    }
}

/*********************
 * Plugin Activation *
 *********************/
if (!is_network_admin()) {

    // List of third party clones of the Divi Theme
    $parent_themes = [
        'Maestro',
    ];

    $theme = wp_get_theme();
    if (dipi_is_divi_builder() || 'Divi' == $theme->name || stripos($theme->parent_theme, 'Divi') !== false || in_array($theme->parent_theme, $parent_themes)) {
        require_once plugin_dir_path(__FILE__) . 'includes/plugin.php';
    }
}

function dipi_is_divi_builder(){
    $is_divi_builder =  false;
    $pluginList = get_option( 'active_plugins' );
    $plugin = 'divi-builder/divi-builder.php'; 
    $theme = wp_get_theme();
    if ( in_array( $plugin , $pluginList ) || $theme->name == 'Extra') {
        $is_divi_builder = true;
    }
    return $is_divi_builder;
}
if (!function_exists('dipi_is_vb')) {
    function dipi_is_vb()
    {
        return function_exists('et_fb_enabled') && et_fb_enabled();
    }
}