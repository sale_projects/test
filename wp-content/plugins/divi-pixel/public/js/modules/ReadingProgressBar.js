function window_scroll_reading_progress() {
    //TODO: This code could be improved to check the scrolling of the main content instead of the whole body
    let bodyScrollTop = document.body.scrollTop,
        docElementScrollTop = document.documentElement.scrollTop,
        scrollHeight = document.documentElement.scrollHeight,
        docElementCliendHeight = document.documentElement.clientHeight;

    let w = (bodyScrollTop || docElementScrollTop) / (scrollHeight - docElementCliendHeight) * 100;

    jQuery(".dipi-reading-progress-fill").css({
        width: w + "%"
    });
}

jQuery(window).on('scroll', window_scroll_reading_progress);

jQuery(window).on('load', window_scroll_reading_progress);

jQuery('.dipi_reading_progress_bar').each(function(i, v) {
    let jQuerythis = jQuery(this);
    let jQuerywrap = jQuerythis.find('.dipi-reading-progress-wrap');
    let position = jQuerythis.find('.dipi-reading-progress-wrap').data('position');

    let bgcolor = jQuerythis.find('.dipi-reading-progress-wrap').data('bgcolor');
    let fillcolor = jQuerythis.find('.dipi-reading-progress-wrap').data('color');;

    jQuerywrap.find('.dipi-reading-progress').css('background-color', bgcolor);
    jQuerywrap.find('.dipi-reading-progress-fill').css('background-color', fillcolor);

    let get_wrap = jQuerywrap.detach();

    if (position === 'bottom') {
        jQuery("body").append(get_wrap);
    } else if (position === 'top') {
        jQuery("body").append(get_wrap);
    } else {
        jQuery("#main-header").append(get_wrap);
        jQuery(".et-l--header").append(get_wrap);
    }
});


