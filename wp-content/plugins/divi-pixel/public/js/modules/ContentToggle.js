jQuery(function($) { 
    $(".dipi-content-toggle__switch").change(function() {
        const dipi_content_toggle_container = $(this).parents(".dipi-content-toggle-container")
        const first_layout = dipi_content_toggle_container.children(".dipi-content-toggle__first-layout");
        const second_layout = dipi_content_toggle_container.children(".dipi-content-toggle__second-layout");
        if (this.checked) {
            //Do stuff
            first_layout.css("display", "none")
            second_layout.css("display", "block")
        } else {
            first_layout.css("display", "block")
            second_layout.css("display", "none")
        }
        console.log('resize');
        $(window).trigger('resize');
    });
    function check_dipi_contenttoggle_lazyload_setting () {
        $(".dipi-content-toggle__content.disable_browser_lazyload img").removeAttr("loading")
        $(".dipi-content-toggle__content.disable_wprocket_lazyload img").addClass("skip-lazy")
    }
    window.addEventListener("load", function() {
        // For Safri
        check_dipi_contenttoggle_lazyload_setting()
    });
    $(document).ready(function(){
        // For Firfox
        check_dipi_contenttoggle_lazyload_setting()
    });
    window.onload = function() {
        // For Firfox
        check_dipi_contenttoggle_lazyload_setting()
    }
});