jQuery(function($) {
    $('.dipi_fancy_text').each(function() {
        let element = $(this).find(".fancy-text-wrap");
        let inAnimation = element.attr("data-in-animation");
        let outAnimation = element.attr("data-out-animation");
        let speed = element.attr("data-speed");
        let duration = element.attr("data-duration");
        element.DIPIMorphext({
            inAnimation: inAnimation,
            outAnimation: outAnimation,
            separator: '||',
            speed: parseInt(speed),
            duration: parseInt(duration)
        });
    });
});