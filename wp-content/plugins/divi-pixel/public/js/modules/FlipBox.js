jQuery(function($) {
    
    function calculateHeight($element) {
        var height = 0;
        $element.children().each(function() {
            height = height + $(this).outerHeight(true);
        });
    
        return $element.outerHeight(true) - $element.height() + height;
    }
    
    function setWrapperHeight($wrapper, $front, $back) {
        let height = Math.max(calculateHeight($front), calculateHeight($back));
        $wrapper.height(height);
    }

    $(document).ready(function(){
        $( window ).on('load',function() {
            $('.dipi_flip_box').parents('.et_pb_row').css("-webkit-transform", "translateZ(0)");

            $('.dipi_flip_box').each(function() {
                let $this = $(this);
                // Dynamic Height
                if ('on' === $this.find('.dipi-flip-box-container').attr('data-dynamic_height')) {
                    let $wrapper = $this.find(".dipi-flip-box-inner-wrapper");
                    let $front = $this.find('.dipi-flip-box-front-side-innner');
                    let $back = $this.find('.dipi-flip-box-back-side-innner');
                    
                    new ResizeSensor($this, function() {
                        setWrapperHeight($wrapper, $front, $back);
                    });

                    setWrapperHeight($wrapper, $front, $back);
                }

                // Force Square
                if ('on' === $this.find('.dipi-flip-box-container').attr('data-force_square')) {
                    let $wrapper = $this.find(".dipi-flip-box-inner-wrapper");
                    new ResizeSensor($this, function() {
                        $wrapper.height($wrapper.width());
                    });
                    $wrapper.height($wrapper.width());
                }
            });
        });
    });
});