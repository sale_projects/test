jQuery(function($) {
    $.fn.lockScroll=function(e) {
        e.preventDefault();
    }
    $.fn.dipi_masonry_gallery = function(params) {
        let $this = $(this);
        if (typeof $.magnificPopup === "undefined") {
            setTimeout(function() {
                $this.dipi_masonry_gallery(params);
            }, 300);
            return;
        }

        var masonry = $this.masonry({
            // set itemSelector so .grid-sizer is not used in layout
            itemSelector: '.grid-item',
            // use element for option
            columnWidth: '.grid-sizer',
            percentPosition: true,
            gutter: '.gutter-sizer',
        });

        $this.find('.grid-item .img-container').magnificPopup({
            // delegate: '.et_pb_gallery_image a',
            type: 'image',
            removalDelay: 500,
            fixedContentPos: false,
            gallery: {
                enabled: true,
                navigateByImgClick: true,
                tCounter: '%curr% / %total%'
            },
            mainClass: 'mfp-fade',
            zoom: {
                enabled: true, //! et_pb_custom.is_builder_plugin_used,
                duration: 500,
                opener: function(element) {
                    return element.find('img');
                }
            },
            autoFocusLast: false,
            image: {
                verticalFit: true,
                titleSrc: function(item) {
                    let title = "";
                    if (item.el.attr('data-title')) {
                        title += item.el.attr('data-title');
                    }
                    if (item.el.attr('data-caption')) {
                        title += "<small class='dipi_masonry_gallery_caption'>" + item.el.attr('data-caption') + "</small>";
                    }
                    return title;
                }
            },
            disableOn: function() {
                /*if ($(this).hasClass("show_lightbox")) {
                    return false;
                }*/
                if (window.matchMedia("(max-width: 767px)").matches) {
                    return $this.hasClass("show_lightbox_phone")
                } else if (window.matchMedia("(max-width: 980px)").matches) {
                    return $this.hasClass("show_lightbox_tablet");
                }
                else {
                    return $this.hasClass("show_lightbox");
                }           
            },
            callbacks: {
                open: function() {
                    var swidth=(window.innerWidth-$(window).width());
                    jQuery('body').addClass('noscroll');
                    /* To fix jumping, need to set scroll width as margin-right */
                    jQuery('body').css('margin-right', swidth + 'px');
                },
                close: function() {
                    jQuery('body').removeClass('noscroll');
                    jQuery('body').css('margin-right', 0);
                }
            }
        });

        $this.find('.grid-item .img-container').on('click', function(e) {
            e.preventDefault();
            //$.magnificPopup.hide();
            return true;
        });

        var layout = $.debounce(250, function() {
            masonry.masonry('layout');
        });

        if ($this.attr("data-lazy") === "true") {
            var observer = new MutationObserver(layout);
            var config = { attributes: true, childList: true, subtree: true };
            observer.observe($this[0], config);
        }

        masonry.imagesLoaded().progress(layout);

        return masonry.masonry('layout');
    };

    // Setup Masonry Galleries
    $(".dipi_masonry_gallery").each(function() {
        var clazz = $(this).attr("class").replace(/ /g, ".");
        $("." + clazz + " .grid").dipi_masonry_gallery();
    });

    $('.dipi_masonry_gallery .grid .grid-item').hover(
        function() { //handlerIn
            const icon_element = $(this).find(".dipi-mansonry-gallery-icon");
            // Restart animation
            icon_element.replaceWith(icon_element.clone());

            const title_element = $(this).find(".dipi-mansonry-gallery-title");
            // Restart animation
            title_element.replaceWith(title_element.clone());
            
            const caption_element = $(this).find(".dipi-mansonry-gallery-caption");
            // Restart animation
            caption_element.replaceWith(caption_element.clone());
        }, 
    );
});
