jQuery(function($) {
    $(".dipi-typing .dipi-typing-text").each(function() {
        var $this = $(this);
        var text = $this.data("dipi-typing-strings");
        var loop = $this.data("dipi-loop");
        var speed = $this.data("dipi-speed");
        var backspeed = $this.data("dipi-backspeed");
        var backdelay = $this.data("dipi-backdelay");
        var show_cursor = $this.data("dipi-show-cursor");
        var cursor_char = $this.data("dipi-cursor-char");

        var options = {
            strings: text,
            loop: loop,
            typeSpeed: parseFloat(speed),
            backSpeed: parseFloat(backspeed),
            backDelay: parseFloat(backdelay),
            cursorChar: cursor_char,
            contentType: "null"
        };

        new Typed(this, options);
    });


});