jQuery(function($) {

    $.fn.dipi_image_accordion = function() {

        let $this = $(this);
        let accordion_type = $this.find('.dipi_image_accordion_wrapper').data('accordion-type');
        let $accordion_children = $this.find('.dipi_image_accordion_child');

        //Setup active on load
        if (window.matchMedia("(max-width: 767px)").matches)  
        { 
            $this.find('[data-active-on-load-phone=1]').parents('.dipi_image_accordion_child').addClass('dipi-active');
        } else if (window.matchMedia("(max-width: 980px)").matches)  {
            $this.find('[data-active-on-load-tablet=1]').parents('.dipi_image_accordion_child').addClass('dipi-active');
        }
        else { 
            $this.find('[data-active-on-load=1]').parents('.dipi_image_accordion_child').addClass('dipi-active');
        }
        if (accordion_type === 'on_hover') {
            $accordion_children.on('mouseenter', function() {
                $accordion_children.removeClass('dipi-active');
                $(this).addClass('dipi-active');
            });

            $accordion_children.on('mouseleave', function() {
                $accordion_children.removeClass('dipi-active');
            });
        }

        // Setup active on click
        if (accordion_type === 'on_click') {
            $this.addClass('dipi_clickable');
            $accordion_children.click(function(e) {
                $accordion_children.removeClass('dipi-active');
                $(this).addClass('dipi-active');
            });
        }
    };

    $(".dipi_image_accordion").each(function() {
        $(this).dipi_image_accordion();
    });
});