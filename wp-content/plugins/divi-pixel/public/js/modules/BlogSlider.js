jQuery(function($) {
    $('.dipi_blog_slider').each(function(index, value) {
        let $this = $(this);
        var data = value.querySelector('.dipi-blog-slider-main').dataset;
        let selector = "." + $this.attr('class').split(' ').join('.') + " .swiper-container";
        var navigation = "on" === data.navigation && {
            nextEl: ".dipi_blog_slider .dipi-sbn" + data.ordernumber,
            prevEl: ".dipi_blog_slider .dipi-sbp" + data.ordernumber
        };

        var dynamicbullets = ('on' == data.dynamicbullets) ? true : false;
        var pagination = "on" === data.pagination && {
            el: ".dipi_blog_slider .dipi-sp" + data.ordernumber,
            clickable: true,
            dynamicBullets: dynamicbullets,
            dynamicMainBullets: 1
        };

        var cfe = {
            rotate: Number(parseInt(data.rotate)),
            stretch: 5,
            depth: 0,
            modifier: 1,
            slideShadows: data.shadow,
        };

        var mySwiper = new Swiper(selector, {
            slidesPerView: Number(data.columnsmobile),
            spaceBetween: Number(data.spacebetween_phone),
            speed: Number(data.speed),
            loop: "on" === data.loop,
            autoplay: "on" === data.autoplay && {
                delay: data.autoplayspeed
            },
            effect: data.effect,
            coverflowEffect: "coverflow" === data.effect ? cfe : null,
            navigation: navigation,
            pagination: pagination,
            centeredSlides: "on" === data.centered,
            slideClass: "dipi-blog-post",
            wrapperClass: "dipi-blog-slider-wrapper",
            setWrapperSize: true,
            observer: true,
            observeParents: true,
            observeSlideChildren: true,
            breakpoints: {
                768: {
                    slidesPerView: Number(data.columnstablet),
                    spaceBetween: Number(data.spacebetween_tablet) > 0 ? Number(data.spacebetween_tablet) : Number(0),
                },
                981: {
                    slidesPerView: Number(data.columnsdesktop),
                    spaceBetween: Number(data.spacebetween) > 0 ? Number(data.spacebetween) : Number(0),
                }
            }
        });

        if ('on' === data.pauseonhover && 'on' === data.autoplay) {
            $this.find('.swiper-container').on('mouseenter', function(e) {
                mySwiper.autoplay.stop();
            });

            $this.find('.swiper-container').on('mouseleave', function(e) {
                mySwiper.autoplay.start();
            });
        }

        $this.find(".preloading").removeClass("preloading");

    });

});