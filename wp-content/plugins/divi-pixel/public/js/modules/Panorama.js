jQuery(function ($) {
    //Panorama Setup
    document.querySelectorAll('.dipi-panorama').forEach(function(ele){
      const strings = {
          // Labels
          loadButtonLabel: 'Click to<br>Load<br>Panorama',
          loadingLabel: 'Loading...',
          bylineLabel: 'by %s',    // One substitution: author

          // Errors
          noPanoramaError: 'No panorama image was specified.',
          fileAccessError: 'Could not access one/multiple of panorama image.',  // One substitution: file URL
          // 'Could not access one/multiple of panorama images.'
          malformedURLError: 'There is something wrong with the panorama URL.',
          iOS8WebGLError: "Due to iOS 8's broken WebGL implementation, only " +
                          "progressive encoded JPEGs work for your device (this " +
                          "panorama uses standard encoding).",
          genericWebGLError: 'Your browser does not have the necessary WebGL support to display this panorama.',
          textureSizeError: 'This panorama is too big for your device! It\'s ' +
                      '%spx wide, but your device only supports images up to ' +
                      '%spx wide. Try another device.' +
                      ' (If you\'re the author, try scaling down the image.)',    // Two substitutions: image width, max image width
          unknownError: 'Unknown error. Check developer console.',
      };
        let arg = {};

        if(ele.getAttribute('data-type') === 'Equirectangular'){
            arg = {
                hfov: 120,
                strings: strings,
                "type": 'equirectangular',
                "panorama": ele.getAttribute('data-image')
            }
        } else if(ele.getAttribute('data-type') === 'Cube Map') {
            arg = {
              hfov: 120,
              strings: strings,
              "type": "cubemap",
              "cubeMap": [
                  ele.getAttribute('data-image0'),
                  ele.getAttribute('data-image1'),
                  ele.getAttribute('data-image2'),
                  ele.getAttribute('data-image3'),
                  ele.getAttribute('data-image4'),
                  ele.getAttribute('data-image5')
              ]
            }
          }

        let panorama = pannellum.viewer(ele, arg);
        if( ele.getAttribute('data-is-autoload') === "on" && elementInViewport(ele)){
            if(!panorama.isLoaded())
                processScroll(ele, panorama)
        }
        panorama.on('load', function(){
            ele.parentNode.classList.add("loaded")
        })
        addEventListener('scroll', function (){
            if(!panorama.isLoaded())
                processScroll(ele, panorama)
        });
    });

    function processScroll(el, panorama){
        if( el.getAttribute('data-is-autoload') == "on" &&
            elementInViewport(el) &&
            (el.getAttribute('data-type') === 'Equirectangular' || el.getAttribute('data-type') === 'Cube Map')) {
            panorama.loadScene()
        }
    }
    document.querySelectorAll('.dipi-panorama-video').forEach(function(ele, i){
        let autoload = false;
        if( ele.getAttribute('data-is-autoload') === "on"){
            autoload = true;
        }
        let arg = {
            autoplay : autoload,
            plugins: {
                pannellum: {}
            }
        };
        videojs(ele, arg);
        videojs.on(ele, "loadeddata", function(){
            ele.parentNode.parentNode.classList.add("loaded")
        });
    })

    $('.dipi-panorma-image2d .dipi-img-drag').each(function(){
        let $el = $(this),
            imageWidth,
            imageHeight;
            if ($el.attr('data-image-width') < $el.parent().width()){
                imageWidth = $el.parent().width();
                imageHeight = ($el.parent().width() / $el.attr('data-image-width')) * $el.attr('data-image-height')
            } else {
                imageWidth = $el.attr('data-image-width');
                imageHeight = $el.attr('data-image-height');
            }

        let opt = {
                scroll_speed: 50,
                repeat: $el.attr('data-repeat'),
                direction: $el.attr('data-direction'),
                width: imageWidth,
                height: imageHeight
            } ;
            $el.css({
                minWidth: imageWidth,
                minHeight: imageHeight,
                backgroundSize: imageWidth + 'px ' + imageHeight + 'px',
            });
            if(opt.repeat === 'Both' || opt.repeat === 'Horizontal')
                $el.css({ width: imageWidth * 3 });

            if(opt.repeat === 'Both' || opt.repeat === 'Vertical')
                $el.css({ height: imageHeight * 3 });

            mousewheel_scroll($el, opt);
            mousedrag($el, opt);
    })

    function elementInViewport(el) {
        var rect = el.getBoundingClientRect()
        return (
           rect.top    >= 0
        && rect.left   >= 0
        && rect.top <= (window.innerHeight || document.documentElement.clientHeight)
        )
    }


    function mousedrag($el, opt){
        $el.on('mousedown', function(evt){
            if(opt.direction === 'Vertical' || opt.direction === 'Both')
                vertical_scroll($el, opt, evt)
            if(opt.direction === 'Horizontal' || opt.direction === 'Both')
                horizontal_scroll($el, opt, evt)

        })
    }
    function mousewheel_scroll ($el, opt) {

        let $el_p = $el.parent();
        $el.on('mousewheel DOMMouseScroll', function(e){
            disable_body_scroll(e)
            let direction = get_scroll_direction(e);
                el_h = $el.outerHeight(),
                el_w = $el.outerWidth(),
                new_top = $el.offset().top + opt.scroll_speed,
                new_bottom = $(window).height() - (($el.offset().top - opt.scroll_speed) + el_h)
                new_left = $el.offset().left + opt.scroll_speed;
                new_right = $(window).width() - (($el.offset().left - opt.scroll_speed) + el_w)
                el_bottom = ($(window).height() - ($el.offset().top + el_h)),
                parent_bottom = ($(window).height() - ($el_p.offset().top + $el_p.outerHeight()));
                parent_right = ($(window).width() - ($el_p.offset().left + $el_p.outerWidth()));

            if(shifted = e.shiftKey){ // Shift + Scroll (Scroll Horizontal)
                if(opt.direction === 'Vertical' )
                    return;

                if(direction === 'up'){ // Scroll Up // Scroll Image To The Right
                    if( parseInt($el_p.offset().left) > parseInt(new_left) ) { // Reach Left boundries ? No
                        $el.offset({ left:  new_left })
                      } else { // Reach Left boundries ? yes
                        if(opt.repeat == 'Both' || opt.repeat == 'Horizontal'){
                            $el.offset({ left: $el_p.offset().left - parseInt(opt.width) })
                        }
                        else
                            $el.offset({ left: $el_p.offset().left })
                      }
                }
                if( direction === 'down'){ // Scroll down
                    if( parent_right > new_right ) { // Reach Right boundries ? No
                        $el.offset({
                            left:  $el.offset().left - 1 * opt.scroll_speed
                        })
                    } else { // Reach Right boundries ? yes

                        if(opt.repeat == 'Both' || opt.repeat == 'Horizontal') {
                            $el.offset({ left: $el.offset().left + parseInt(opt.width)  })
                        } else
                            $el.offset({ left: $(window).width() - (el_w + parent_right) })
                    }
                }
            } else {

                if(opt.direction === 'Horizontal')
                    return;

                if(direction === 'up'){ // Scroll Up
                    if( parent_bottom > new_bottom ) { // Reach bottom boundries ? No
                        $el.offset({
                            top:  $el.offset().top - 1 * opt.scroll_speed
                        })
                    } else { // Reach bottom boundries ? yes
                        if(opt.repeat == 'Both' || opt.repeat == 'Vertical')
                            $el.offset({ top: $el.offset().top + parseInt(opt.height) })
                        else
                            $el.offset({ top: $(window).height() - (el_h + parent_bottom) })
                    }
                }
                if( direction === 'down'){ // Scroll down
                    if($el_p.offset().top > new_top){ // Reach top boundries ? No
                        $el.offset({
                            top:  new_top
                        })
                    } else { // Reach top boundries ? Yes
                        if(opt.repeat == 'Both' || opt.repeat == 'Vertical')
                            $el.offset({ top: $el.offset().top - parseInt(opt.height) })
                        else
                            $el.offset({ top:  $el_p.offset().top })
                    }
                }
            }
        })
       }

    function get_scroll_value(e){
        if(typeof e.originalEvent.detail == 'number' && e.originalEvent.detail !== 0) {
            return e.originalEvent.detail;
          } else if (typeof e.originalEvent.wheelDelta == 'number') {
            return e.originalEvent.wheelDelta;
          }
    }
    function get_scroll_direction(e) {
        if(get_scroll_value(e) > 0)
            return 'up'
        else
            return 'down'
    }
    function disable_body_scroll(e) {
        var e0 = e.originalEvent,
        delta = e0.wheelDelta || -e0.detail;

        this.scrollTop += ( delta < 0 ? 1 : -1 ) * 30;
        e.preventDefault();
    }
    function vertical_scroll($el, opt, evt){
        let $el_p = $el.parent();
        let el_h = $el.outerHeight(),
        pos_y = $el.offset().top + el_h - evt.pageY,
        old_top = $el.offset().top; // to check mouse direction

        $el.parents().on("mousemove.vertical", function(e) {
            let new_top = e.pageY + pos_y - el_h;
            let new_bottom = ($(window).height() - ($el.offset().top + el_h));
            let parent_bottom = ($(window).height() - ($el_p.offset().top + $el_p.outerHeight()));

            $el.offset({
                top: new_top
            })
            if(old_top < new_top ){ // Mouse Direction top
                if($el_p.offset().top > new_top) { // Check top Boundries
                    $el.offset({
                        top: new_top
                    })
                } else { // Already Reach top boundries

                    if(opt.repeat == 'Both' || opt.repeat == 'Vertical')
                        $el.offset({ top: $el.offset().top - parseInt(opt.height) }) // Repeat Image
                    else
                        $el.offset({top: $el_p.offset().top }) // Stop Scrolling

                    pos_y = $el.offset().top + el_h - e.pageY
                }
            }

            if(old_top > new_top  ) { // Mouse Direction bottom
                if(parent_bottom > new_bottom) { // Reach Bottom Boundries ? No
                    $el.offset({
                        top: new_top
                    })
                } else { // Reach Bottom Boundries ? Yes

                    if(opt.repeat == 'Both' || opt.repeat == 'Vertical') // Is Repeat Set to Verticall or Both ?
                        $el.offset({ top: $el.offset().top + parseInt(opt.height) }) // Yes! Repeat Image
                    else
                        $el.offset({top: $(window).height() - (el_h + parent_bottom) }) // No! Stop Scrolling

                    pos_y = $el.offset().top + el_h - e.pageY
                }
            }
        });
        $(document).on("mouseup", function() {
            $el.parents().off("mousemove.vertical")
        });
        evt.preventDefault(); // disable selection
    }
    function horizontal_scroll($el, opt, evt){
        let $el_p = $el.parent();
        let el_h = $el.outerHeight(),
                el_w = $el.outerWidth(),
                pos_y = $el.offset().top + el_h - evt.pageY,
                pos_x = $el.offset().left + el_w - evt.pageX,
                old_left = $el.offset().left; // to check mouse direction

            $el.parents().on("mousemove.horizontal", function(e) {
                let new_left = e.pageX + pos_x - el_w;
                let el_right = ($(window).width() - ($el.offset().left + el_w));
                let parent_right = ($(window).width() - ($el_p.offset().left + $el_p.outerWidth()));

                if(old_left < new_left ) { // Mouse Direction Right
                    if($el_p.offset().left > new_left) { // Reach Left Boundries ? NO
                        $el.offset({
                            left: new_left
                        })
                    } else { // Reach Left Boundries ? Yes

                        if(opt.repeat == 'Both' || opt.repeat == 'Horizontal') // Is Repeat Set to Horizontal or Both ?
                            $el.offset({ left: $el.offset().left - parseInt(opt.width) }) // Yes! Repeat Image
                        else
                            $el.offset({left: $el_p.offset().left }) // No! Stop Scrolling

                        pos_x = $el.offset().left + el_w - e.pageX
                    }
                }else if(old_left > new_left  ) { // Mouse Direction Left

                    if(parent_right > el_right) { // Right Boundries
                        $el.offset({
                            left: new_left
                        })
                    } else { // Already Reach Right Boundries

                        if(opt.repeat == 'Both' || opt.repeat == 'Horizontal') // Is Repeat Set to Horizontal or Both ?
                            $el.offset({ left: $el.offset().left + parseInt(opt.width) }) // Yes! Repeat Image
                        else
                            $el.offset({ left: $(window).width() - (el_w + parent_right) }) // No! Stop Scrolling

                            pos_x = $el.offset().left + el_w - e.pageX
                    }
                }
                old_left = new_left
            });
            $(document).on("mouseup", function() {
                $el.parents().off("mousemove.horizontal")
            });
            evt.preventDefault(); // disable selection
    }
});
