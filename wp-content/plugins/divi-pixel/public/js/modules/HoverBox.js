jQuery(function($) {
    function check_force_square () {
        $('.dipi_hover_box').each(function() {
            let $this = $(this);
            let $container = $this.find('.dipi-hover-box-container');
            let $content = $container.find('.dipi-hover-box-content');
            let $hover = $container.find('.dipi-hover-box-hover');
            // Force Square
            if ('on' === $container.attr('data-force_square')) {
                new ResizeSensor($this, function() {
                    let width = $container.width();
                    $container.height(width);
                    $content.outerHeight(width);
                    $hover.outerHeight(width);
                });
                let width = $container.width();
                $container.height(width);
                $content.outerHeight(width);
                $hover.outerHeight(width);
            }
        });
    }
    window.addEventListener("load", function() {
        // For Safri
        check_force_square()
    });
    $(document).ready(function(){
        // For Firfox
        check_force_square()
    });
    window.onload = function() {
        // For Firfox
        check_force_square()
    }
});