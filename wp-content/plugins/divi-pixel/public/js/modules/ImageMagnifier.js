jQuery(function($) {
    $('.dipi_image_magnifier').each(function(index, value) {
        let $this = $(this);
        let data = value.querySelector('.dipi-image-magnifier').dataset;

        $($this).find('.dipi-image-magnifier img').magnify({
            speed: Number(data.speed)
        });
    });
});