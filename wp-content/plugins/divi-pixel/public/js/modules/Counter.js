(function($) {
    $.fn.dipi_counter = function(params) {
        let $this = $(this);
        var $this_counter = $(this).find(".dipi_counter_number_wrapper");
        var count_duration = $this_counter.attr('data-count-duration') ? $this_counter.attr('data-count-duration') : 2000;
        var count_to = $this_counter.attr('data-count-to') ? $this_counter.attr('data-count-to') : 0;
        var count_from = $this_counter.attr('data-count-from') ? $this_counter.attr('data-count-from') : 0;
        var forced_decimal_places = $this_counter.attr('data-decimal-places');
        var force_decimal_places = $this_counter.attr('data-force-decimal-places');
        var show_decimals = count_to % 1 !== 0 || count_from % 1 !== 0 || force_decimal_places === true || force_decimal_places === 'true';
        var decimal_places = force_decimal_places ? forced_decimal_places : Math.max(get_decimal_places(count_to), get_decimal_places(count_from));
        var counter_type = $this_counter.attr('data-counter-type') ? $this_counter.attr('data-counter-type') : 'number';
        var circle_percent = $this_counter.attr('data-circle-percent') ? parseInt($this_counter.attr('data-circle-percent')) : 100;
        var separator = $this_counter.attr('data-number-separator');
        var decimal_separator = $this_counter.attr('data-number-decimal-separator');

        var size = 0;
        var trackColor = false;
        var lineWidth = 0;
        var barColor = false;
        var lineCap = false;
        var scaleColor = false;
        var scaleLength = false;
        var rotate = 0;

        //Setup Circular counter if necessary
        if ('circle' === counter_type) {
            size = $this_counter.attr('data-circle-size') ? parseInt($this_counter.attr('data-circle-size')) : $(this).width();
            trackColor = $this_counter.attr('data-circle-track-color');
            lineWidth = Math.min($this_counter.attr('data-circle-line-width'), size);
            barColor = $this_counter.attr('data-circle-bar-color');
            lineCap = $this_counter.attr('data-circle-line-cap');
            scaleColor = $this_counter.attr('data-circle-use-scale') === 'true' ? $this_counter.attr('data-circle-scale-color') : false;
            scaleLength = $this_counter.attr('data-circle-use-scale') === 'true' ? parseInt($this_counter.attr('data-circle-scale-length')) : false;
            if (scaleLength) {
                lineWidth = Math.min($this_counter.attr('data-circle-line-width'), size - (2 * scaleLength) - 4);
            }

            rotate = parseInt($this_counter.attr('data-circle-rotate'));

            //Enable responsive circle size if no size is set
            if (!$this_counter.attr('data-circle-size')) {
                new ResizeSensor($this, function() {
                    $this.find('canvas').css('width', $this.width());
                    $this.find('canvas').css('height', $this.width());
                });
            }
        }
        $this_counter.easyPieChart({
            animate: { duration: count_duration, enabled: true },
            //easing:      'easeOutBounce',
            size: size,
            barColor: barColor,
            trackColor: trackColor,
            // trackAlpha:  trackAlpha,
            lineWidth: lineWidth,
            lineCap: lineCap,
            scaleColor: scaleColor,
            scaleLength: scaleLength,
            rotate: rotate,
            onStart: function() {},
            onStep: function(from, to, percent) {
                //Normalize percent
                if ('circle' === counter_type) {
                    percent = percent * 100 / circle_percent
                }
                var number = count_from - (count_from - count_to) * (percent / 100);
                number = !show_decimals ? Math.round(parseInt(number)) : round_to_precision(number, decimal_places).toFixed(decimal_places);
                number = format_number(number, separator, decimal_separator);
                $(this.el).find('.dipi_counter_number_number').text(number);
            },
            onStop: function(from, to) {
                var number = !show_decimals ? Math.round(parseInt(count_to)) : round_to_precision(count_to, decimal_places).toFixed(decimal_places);
                number = format_number(number, separator, decimal_separator);
                $(this.el).find('.dipi_counter_number_number').text(number);
            }
        });

        $this_counter.fadeIn('fast');

        if ($.fn.waypoint && 'yes' !== et_pb_custom.ignore_waypoints) {
            waypoint($this, {
                offset: '75%',
                handler: function() {
                    $this.dipi_counter_update();
                }
            });

            //Fallback
            waypoint($this, {
                offset: 'bottom-in-view',
                handler: function() {
                    $this.dipi_counter_update();
                }
            });
        }
    };

    $.fn.dipi_counter_update = function(params) {
        let $counter = $(this).find(".dipi_counter_number_wrapper");
        let easyPieChart = $counter.data('easyPieChart');
        if(!easyPieChart){
            return;
        }
        let delay = parseInt($counter.attr('data-count-animation-delay'));
        setTimeout(function() {
            let counter_type = $counter.attr('data-counter-type') ? $counter.attr('data-counter-type') : 'number';
            try {
                let data = $counter.data('easyPieChart');
                if(data === undefined){
                    return;
                }
                if ('number' === counter_type) {
                    $counter.data('easyPieChart').update(100);
                } else if ('circle' === counter_type) {
                    let circle_percent = $counter.attr('data-circle-percent') ? $counter.attr('data-circle-percent') : 100;
                    $counter.data('easyPieChart').update(circle_percent);
                }
            } catch (error) {
                console.error("Error in dipi_counter_update:", error);
            }
        }, delay);
    }

    $.fn.dipi_counter_destroy = function(params) {
        let $this = $(this);
        $this.find(".resize-sensor").remove();

        let $counter = $this.find(".dipi_counter_number_wrapper");
        let count_from = $counter.attr('data-count-from') ? $counter.attr('data-count-from') : 0;
        let number = Math.round(parseInt(count_from));
        $counter.removeData('easyPieChart');
        $counter.find('canvas').remove();
        $counter.find('.dipi_counter_number_number').text(number);
    }

    function get_decimal_places(num) {
        var match = ('' + num).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
        if (!match) { return 0; }
        return Math.max(0, (match[1] ? match[1].length : 0) - (match[2] ? +match[2] : 0));
    }

    function round_to_precision(number, precision) {
        if (number == 0) return 0;
        var factor = Math.pow(10, precision);
        var tempNumber = number * factor;
        var roundedTempNumber = Math.round(tempNumber);
        return roundedTempNumber / factor;
    };


    function format_number(number_value, separator, decimal_separator) {
        var numberString = number_value.toString();
        if (decimal_separator && decimal_separator !== '') {
            numberString = numberString.replace(".", decimal_separator);
        }
        return numberString.replace(/\B(?=(\d{3})+(?!\d))/g, separator);
    }

    function waypoint($element, options) {
        if (!$element.data('et_waypoint')) {
            var instances = $element.waypoint(options);
            if (instances && instances.length > 0) {
                $element.data('et_waypoint', instances[0]);
            }
        } else {
            $element.data('et_waypoint').context.refresh();
        }
    }

    // Setup Counters
    $('.dipi_counter').each(function() {
        $(this).dipi_counter();
    });
})(jQuery);