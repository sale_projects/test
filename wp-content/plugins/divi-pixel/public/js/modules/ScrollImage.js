function disable_body_scroll(e, ele) {
    var e0 = e.originalEvent,
        delta = e0.wheelDelta || -e0.detail;
    ele.scrollTop += (delta < 0 ? 1 : -1) * 30;
    e.preventDefault();
}

jQuery(function($) {
    $('.dipi_scroll_image').each(function(index, value) {

        let $this = $(this),
            data = value.querySelector('.dipi-scroll-image').dataset,
            direction = data.direction,
            type = data.type,
            reverse = data.reverse,
            scroll_element = $this.find(".dipi-image-scroll-container"),
            scroll_image = scroll_element.find('.dipi-image-scroll-image img'),
            scroll_overlay = scroll_element.find(".dipi-image-scroll-overlay"),
            scroll_value = null,
            translate = '';

        function start_scroll_image() {
            scroll_image.css("transform", translate + "(-" + scroll_value + "px)");
        }

        function end_scroll_image() {
            scroll_image.css("transform", translate + "(0px)");
        }

        function set_scroll_image() {
            if (direction === "vertical") {
                scroll_value = scroll_image.height() - scroll_element.height();
                translate = "translateY";
            } else {
                scroll_value = scroll_image.width() - scroll_element.width();
                translate = "translateX";
            }
        }

        if (type === "on_hover") {

            if (reverse === 'on') {
                set_scroll_image();
                start_scroll_image();
            }

            scroll_element.on('mouseenter',function() {
                scroll_element.removeClass("dipi-container-scroll-anim-reset");
                set_scroll_image();
                reverse === 'on' ? end_scroll_image() : start_scroll_image();
            });

            scroll_element.on('mouseleave',function() {
                reverse === 'on' ? start_scroll_image() : end_scroll_image();
            });

        } else {
            if (direction !== "vertical") {
                let scroll_animate = false;
                scroll_element.on('mousewheel DOMMouseScroll', function(e) {
                    disable_body_scroll(e, scroll_element);
                    if (scroll_animate) return;
                    scroll_animate = true;
                    let scroll_value = scroll_element.scrollLeft() + (e.originalEvent.deltaY * 2)
                    scroll_element.animate({ scrollLeft: scroll_value }, 500, function() {
                        scroll_animate = false;
                    })
                })
                scroll_overlay.css({
                    "width": scroll_image.width(),
                    "height": scroll_image.height()
                });
            }
        }


    });
});