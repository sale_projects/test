jQuery(function($){
    let mockupCords = {
        'imac-front': [
            { x: .054, y: .06 },
            { x: .054, y: .66 },
            { x: .948, y: .06 },
            { x: .948, y: .66 }
        ],
        'imac-left': [
            { x: .0534, y: .065 },
            { x: .091, y: .711 },
            { x: .876, y: .183 },
            { x: .942, y: .65 }
        ],
        'imac-right': [
            { x: .128, y: .185 },
            { x: .063, y: .666 },
            { x: .951, y: .064 },
            { x: .91, y: .726 }
        ],
        'ipad-front': [
            { x: .103, y: .109 },
            { x: .103, y: .887 },
            { x: .902, y: .109 },
            { x: .902, y: .887 }
        ],
        'ipad-left': [
            { x: .129, y: .096 },
            { x: .129, y: .896 },
            { x: .921, y: .179 },
            { x: .921, y: .808 }
        ],
        'ipad-right': [
            { x: .071, y: .182 },
            { x: .071, y: .805 },
            { x: .857, y: .099 },
            { x: .857, y: .893 }
        ],
        'macbook-front': [
            { x: .130, y: .076 },
            { x: .130, y: .864 },
            { x: .868, y: .076 },
            { x: .868, y: .864 }
        ],
        'macbook-right': [
            { x:.468, y:.177 },
            { x:.378, y:.868 },
            { x:.968, y:.084 },
            { x:.893, y:.851 }
        ],
        'macbook-left': [
            { x: .029, y: .084 },
            { x: .104, y: .851 },
            { x: .529, y: .181 },
            { x: .619, y: .869 }
        ],
        'iphone-front': [
            { x: .072, y: .128 },
            { x: .072, y: .865 },
            { x: .929, y: .128 },
            { x: .929, y: .865 }
        ],
        'iphone-right': [
            { x:.063, y:.191 },
            { x:.063, y:.868 },
            { x:.851, y:.123 },
            { x:.851, y:.859 }
        ],
        'iphone-left': [
            { x:.151, y:.124 },
            { x:.151, y:.860 },
            { x:.94, y:.192 },
            { x:.94, y:.869 }
        ],
        'iphone-front-white': [
            { x: .072, y: .128 },
            { x: .072, y: .865 },
            { x: .929, y: .128 },
            { x: .929, y: .865 }
        ],
        'iphone-right-white': [
            { x: .063, y: .191 },
            { x: .063, y: .868 },
            { x: .851, y: .123 },
            { x: .851, y: .859 }
        ],
        'iphone-left-white': [
            { x:.151, y:.124 },
            { x:.151, y:.859 },
            { x:.94, y:.192 },
            { x:.94, y:.868 }
        ]
    }

    function transform(element, cords) {
        let width = $(element).width(),
            height = $(element).height(),
            from = [
                { x: 0, y: 0 },
                { x: 0, y: height },
                { x: width, y: 0 },
                { x: width, y: height }
            ],
            to = [
                { x: cords[0].x * width, y: cords[0].y * height },
                { x: cords[1].x * width, y: cords[1].y * height },
                { x: cords[2].x * width, y: cords[2].y * height },
                { x: cords[3].x * width, y: cords[3].y * height }
            ];

        let H = numeric.transform_point(from, to);
        $(element).css({
            'transform': "matrix3d(" + (((function() {
                var _i, _results;
                _results = [];
                for (let i = _i = 0; _i < 4; i = ++_i) {
                    _results.push((function() {
                        var _j, _results1;
                        _results1 = [];
                        for (let j = _j = 0; _j < 4; j = ++_j) {
                            _results1.push(H[j][i].toFixed(20));
                        }
                        return _results1;
                    })());
                }
                return _results;
            })()).join(',')) + ")",
            'transform-origin': '0 0'
        }).addClass('transformed');
    }

    function adjustShowcase(element) {
        let $element = $(element)
        if($element.length === 0) return;
        const screen = element.querySelector('.div-pixel-mockup-screen'),
                mockup = $element.attr('data-mockup')

        $element.find('.dipi_image_showcase_child').addClass('swiper-slide')

        if (screen.swiper) {
            screen.swiper.destroy(true, true);
        }

        if (mockupCords[mockup])
            transform($(screen), mockupCords[mockup])


        var data = screen.dataset
        var cfe = {
            rotate: Number(parseInt(data.rotate)),
            stretch: 5,
            depth: 100,
            modifier: 1,
            slideShadows: true,
        };
        const showcaseSwiper = new Swiper(screen, {
            slidesPerView: 1,
            speed: Number(data.speed),
            loop: "on" === data.loop,
            autoplay: "on" === data.autoplay && {
                delay: data.autoplayspeed
            },
            effect: data.effect,
            coverflowEffect: "coverflow" === data.effect ? cfe : null,
            navigation: false,
            pagination: false,
            slideClass: "dipi_image_showcase_child",
            wrapperClass: "dipi-blog-slider-wrapper",
            setWrapperSize: true,
            observer: true,
            observeParents: true,
        });
    }

    $('.divi-pixel-mockup').length && $('.divi-pixel-mockup').each(function(i, element) {
        adjustShowcase(element);
        $(window).on("resize", function() {
            adjustShowcase(element);
        });
    })
});