window.set_high_zindex = function(el, balloon_open_class) {
    do {
        el.classList.add("dipi-ballon-on-top");
        el.classList.add(balloon_open_class);
    } while(el.parentElement && (el = el.parentElement));
}
window.removing_high_zindex = function(el, balloon_open_class) {
    do {
        el.classList.remove(balloon_open_class);
        if (!el.className.includes(" dipi-balloon-zindex-")) {
            el.classList.replace("dipi-ballon-on-top", "dipi-ballon-on-top-removing");
        }
    } while(el.parentElement && (el = el.parentElement));
}

window.remove_high_zindex = function(el) {
    do {
        el.classList.remove("dipi-ballon-on-top-removing");
    } while(el.parentElement && (el = el.parentElement));
}
