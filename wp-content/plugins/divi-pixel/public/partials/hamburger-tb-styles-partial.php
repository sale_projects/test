<?php
namespace DiviPixel;
$style = DIPI_Settings::get_option('hamburger_animation_styles');
?>

<script type="text/javascript" id="dipi-hamburger-tb-js">
jQuery(function($) {
    let $hamburger = $('<div class="dipi_hamburger hamburger <?php echo $style; ?>" style="float: right; margin-bottom: 24px; line-height: 1em;"><div class="hamburger-box"><div class="hamburger-inner"></div></div></div>');
    let mobileMenu = $(".et-l--header .mobile_menu_bar");
    mobileMenu.append($hamburger);
    $hamburger.css("float", "none");
    $hamburger.css("margin-bottom", "");
    mobileMenu.click(function(){
        $hamburger.toggleClass("is-active");
    });
});
</script>
