<div id="dipi-author-box" class="dipi-author-section">
	<div class="dipi-author-row">
		<div class="dipi-author-left">
			<?php echo get_avatar( get_the_author_meta('email') );?>
		</div>
		<div class="dipi-author-right">
			<h3><?php echo get_the_author_meta('display_name'); ?></h3>
			<p><?php echo get_the_author_meta('description'); ?></p>
		</div>
	 </div>
</div>