<?php
namespace DiviPixel;
$breakpoint_mobile = DIPI_Settings::get_mobile_menu_breakpoint();

$mobile_menu_font_weight = DIPI_Customizer::get_option('mobile_menu_font_weight');

$mobile_submenu_icon_on_collapse = DIPI_Customizer::get_option('mobile_submenu_icon_on_collapse');
$mobile_submenu_icon_on_collapse_border_radius = DIPI_Customizer::get_option('mobile_submenu_icon_on_collapse_border_radius');
$mobile_submenu_icon_on_collapse_color = DIPI_Customizer::get_option('mobile_submenu_icon_on_collapse_color');
$mobile_submenu_icon_on_collapse_background_color = DIPI_Customizer::get_option('mobile_submenu_icon_on_collapse_background_color');

$mobile_submenu_icon_on_expand = DIPI_Customizer::get_option('mobile_submenu_icon_on_expand');
$mobile_submenu_icon_on_expand_border_radius = DIPI_Customizer::get_option('mobile_submenu_icon_on_expand_border_radius');
$mobile_submenu_icon_on_expand_color = DIPI_Customizer::get_option('mobile_submenu_icon_on_expand_color');
$mobile_submenu_icon_on_expand_background_color = DIPI_Customizer::get_option('mobile_submenu_icon_on_expand_background_color');
?>

<style type="text/css" id="mobile-menu-collapse-submenu-css">
@media all and (max-width: <?php echo intval($breakpoint_mobile); ?>px) {
    body.dipi-collapse-submenu-mobile .et-l--header .et_mobile_menu li .sub-menu,
    body.dipi-collapse-submenu-mobile #main-header .et_mobile_menu li .sub-menu {
        width: 100%;
        overflow: hidden;
        max-height: 0;
        visibility: hidden !important;
    }
    
    body.dipi-collapse-submenu-mobile .et-l--header .et_mobile_menu li .dipi-collapse-closed,
    body.dipi-collapse-submenu-mobile #main-header .et_mobile_menu li .dipi-collapse-closed {
        width: 100%;
        max-height: 0px;
        display: none !important;
    }
    body.dipi-collapse-submenu-mobile .et-l--header .et_mobile_menu li .dipi-collapse-animating,
    body.dipi-collapse-submenu-mobile #main-header .et_mobile_menu li .dipi-collapse-animating {
        display: block !important;
    }

    body.dipi-collapse-submenu-mobile .et-l--header .et_mobile_menu li .dipi-collapse-opened,
    body.dipi-collapse-submenu-mobile #main-header .et_mobile_menu li .dipi-collapse-opened {
        width: 100%;
        max-height: 2000px;
        display: block !important;
        visibility: visible !important;
      
    }
    body.dipi-collapse-submenu-mobile .et-l--header .et_mobile_menu li ul.sub-menu,
    body.dipi-collapse-submenu-mobile #main-header .et_mobile_menu li ul.sub-menu{
        -webkit-transition: all 800ms ease-in-out;
        -moz-transition: all 800ms ease-in-out;
        -o-transition: all 800ms ease-in-out;
        transition: all 800ms ease-in-out;
    }

    body.dipi-collapse-submenu-mobile .et_mobile_menu li li {
        padding-left: 0 !important;
    }

    body.dipi-collapse-submenu-mobile .et-l--header .et_mobile_menu li.menu-item-has-children > a,
    body.dipi-collapse-submenu-mobile #main-header .et_mobile_menu li.menu-item-has-children > a {
        cursor: pointer;
        font-weight: <?php echo $mobile_menu_font_weight; ?> !important;
        position: relative;
    }

    body.dipi-collapse-submenu-mobile .et-l--header .et_mobile_menu li.menu-item-has-children ul li a,
    body.dipi-collapse-submenu-mobile #main-header .et_mobile_menu li.menu-item-has-children ul li a {
        font-weight: <?php echo $mobile_menu_font_weight; ?> !important;
    }


    body.dipi-collapse-submenu-mobile .et-l--header .et_mobile_menu li.menu-item-has-children>a:before,
    body.dipi-collapse-submenu-mobile #main-header .et_mobile_menu li.menu-item-has-children>a:before,
    body.dipi-collapse-submenu-mobile .et-l--header .et_mobile_menu li.menu-item-has-children>a:after,
    body.dipi-collapse-submenu-mobile #main-header .et_mobile_menu li.menu-item-has-children>a:after  {
        font-size: 18px;
        margin-right: 10px;
        display: inline-block;
        position: absolute;
        right: 5px;
        z-index: 10;
        cursor: pointer;
        font-family: "ETmodules";
        transition-timing-function: ease-in-out;
        transition-property: all;
        transition-duration: .4s;
        width: 1.6rem;
        height: 1.6rem;
        line-height: 1.6rem;
        text-align: center;
        vertical-align: middle;
    }

    /* Submenu closed */
    body.dipi-collapse-submenu-mobile .et-l--header .et_mobile_menu li.menu-item-has-children>a:before,
    body.dipi-collapse-submenu-mobile #main-header .et_mobile_menu li.menu-item-has-children>a:before {
        content: '<?php echo $mobile_submenu_icon_on_collapse ?>';
        color: <?php echo $mobile_submenu_icon_on_collapse_color ?>;
        background-color: <?php echo $mobile_submenu_icon_on_collapse_background_color ?>;
        border-radius: <?php echo $mobile_submenu_icon_on_collapse_border_radius; ?>%;
    }


    body.dipi-collapse-submenu-mobile .et-l--header .et_mobile_menu li.menu-item-has-children>a:after,
    body.dipi-collapse-submenu-mobile #main-header .et_mobile_menu li.menu-item-has-children>a:after{
        content: '<?php echo $mobile_submenu_icon_on_expand ?>';
        color: <?php echo $mobile_submenu_icon_on_expand_color ?>;
        background-color: <?php echo $mobile_submenu_icon_on_expand_background_color ?>;
        border-radius: <?php echo $mobile_submenu_icon_on_expand_border_radius; ?>%;
        transform: rotate(-90deg);
        opacity: 0;
    }

    /* Submenu opened */
    body.dipi-collapse-submenu-mobile .et-l--header .et_mobile_menu li.menu-item-has-children>a.dipi-collapse-menu:before, 
    body.dipi-collapse-submenu-mobile #main-header .et_mobile_menu li.menu-item-has-children>a.dipi-collapse-menu:before {
        transform: rotate(90deg);
        opacity: 0;
    }
    body.dipi-collapse-submenu-mobile .et-l--header .et_mobile_menu li.menu-item-has-children>a.dipi-collapse-menu:after, 
    body.dipi-collapse-submenu-mobile #main-header .et_mobile_menu li.menu-item-has-children>a.dipi-collapse-menu:after {
        transform: rotate(0deg);
        opacity: 1;
    }

    /* body.dipi-collapse-submenu-mobile .et-l--header .et_mobile_menu li.menu-item-has-children>a:before,
    body.dipi-collapse-submenu-mobile #main-header .et_mobile_menu li.menu-item-has-children>a:before */






}
</style>

<script type="text/javascript" id="mobile-menu-collapse-submenu-js">
jQuery(document).ready(function($) {
    $(window).resize(function() {
        if($(window).width() <= <?php echo intval($breakpoint_mobile); ?>) {
            $('#main-header .mobile_menu_bar, .et-l--header .mobile_menu_bar').click(function() {
				$("#main-header .et_mobile_menu li ul").removeClass('dipi-collapse-opened');
				$("#main-header .et_mobile_menu li ul").addClass('dipi-collapse-closed');

				$(".et-l--header .et_mobile_menu li ul").removeClass('dipi-collapse-opened');
                $(".et-l--header .et_mobile_menu li ul").addClass('dipi-collapse-closed');
                $(".et-l--header .et_mobile_menu li ul").prev("a").removeClass('dipi-collapse-menu')
                $("#main-header .et_mobile_menu li ul").prev("a").removeClass('dipi-collapse-menu')
            });
            
            $("#main-header .menu-item-has-children > a").removeAttr("href");
            $("#main-header .et_mobile_menu .menu-item-has-children > a").off('click').on('click', function(e) {
                $('#main-header .et_mobile_menu').attr('style', "display: block !important");
            });

            // $("#main-header .et_mobile_menu li ul").addClass('dipi-collapse-closed');
            $("#main-header .et_mobile_menu li ul").prev("a").click(function() {
                animate_submenu($(this))
            });

            $(".et-l--header .menu-item-has-children > a").removeAttr("href");

            $(".et-l--header .et_mobile_menu .menu-item-has-children > a").off('click').on('click', function(e) {
                $('.et-l--header .et_mobile_menu').attr('style', "display: block !important");
            });

            // $(".et-l--header .et_mobile_menu li ul").addClass('dipi-collapse-closed');
            $(".et-l--header .et_mobile_menu li ul").prev("a").click(function() {
                animate_submenu($(this));
            });

            function animate_submenu($el){
                if(window.dipi_submenu_animate) return;
                $el.toggleClass('dipi-collapse-menu');
                $submenu = $el.next('ul')
                window.dipi_submenu_animate = false;
                if($submenu.hasClass('dipi-collapse-closed')){
                    window.dipi_submenu_animate = true;
                    $submenu.removeClass('dipi-collapse-closed').addClass('dipi-collapse-animating')
                        setTimeout(() => {
                            $submenu.addClass('dipi-collapse-opened')
                        }, 0);

                        setTimeout(() => {
                            $submenu.stop().removeClass('dipi-collapse-animating')
                            window.dipi_submenu_animate = false;
                        }, 800);
                }else{
                    window.dipi_submenu_animate = true;
                    $submenu.removeClass('dipi-collapse-opened').addClass('dipi-collapse-animating')
                        setTimeout(() => {
                            $submenu.addClass('dipi-collapse-closed')
                        }, 0);
                        setTimeout(() => {
                            $submenu.stop().removeClass('dipi-collapse-animating')
                            window.dipi_submenu_animate = false;
                        }, 800);
                }
            }
        }
    });
});
</script>