<?php
namespace DiviPixel;

$use_fixed_logo = DIPI_Settings::get_option('fixed_logo');
$fixed_logo_image = DIPI_Settings::get_option('fixed_logo_image');
$use_mobile_logo = DIPI_Settings::get_option('mobile_logo');
$mobile_logo_url = DIPI_Settings::get_option('mobile_logo_url');
$breakpoint_mobile = DIPI_Settings::get_mobile_menu_breakpoint();
?>

<script type="text/javascript" id="dipi-logo-js">
jQuery(document).ready(function($) {
    // TODO: Implement fixed/mobile logo for Theme Builder menus
    let tbLogo = $('.et-l--header .et_pb_menu__logo img');
    
    // Get the originals
    let $mainHeader = $('#main-header');
    let $logo = $('#logo');    
    
    // Clone $logo so we can replace it rather than just change src attr (because this causes a bug in Safari browser) 
    let $mainLogo = $logo.clone();
    if($mainLogo.length) {
        $mainLogo.attr("data-logo-type", "main");
        $mainLogo.attr("data-actual-width", $mainLogo[0].naturalWidth);
        $mainLogo.attr("data-actual-height", $mainLogo[0].naturalHeight);
    }

    // Clone $logo to use in fixed header. If fixed header logo is not enabled, we simple use the original logo
    <?php if($use_fixed_logo): ?>  
        let fixedLogoUrl = "<?php echo $fixed_logo_image; ?>";
        let $fixedLogo = $logo.clone().attr("src", fixedLogoUrl)
        $fixedLogo.attr("data-logo-type", "fixed");
    <?php else: ?>
        let $fixedLogo = $logo.clone();
    <?php endif;?>
    if($fixedLogo.length) {
        $fixedLogo.attr("data-actual-width", $fixedLogo[0].naturalWidth);
        $fixedLogo.attr("data-actual-height", $fixedLogo[0].naturalHeight);
    }
    
    // Clone $logo to use in mobile. If mobile logo is not enabled, we simple use the original logo
    <?php if($use_mobile_logo): ?>
    let mobileLogoUrl = "<?php echo $mobile_logo_url; ?>";
    let $mobileLogo = $logo.clone().attr("src", mobileLogoUrl);
    if($mobileLogo.length){
        $mobileLogo.attr("data-logo-type", "mobile");
        $mobileLogo.attr("data-actual-height", $mobileLogo[0].naturalHeight);
    }
    <?php else: ?>
    let $mobileLogo = $logo.clone();
    <?php endif;?>


    // Callback to fire when window is resized or scrolled
	function dipi_logo_change() {
        if($mainLogo.length)
            $mainLogo.attr("data-actual-width", $mainLogo[0].naturalWidth);
        let $a = $(".logo_container a");
        $a.find("#logo").remove();
        if($(window).width() > <?php echo intval($breakpoint_mobile); ?>){
            if ($mainHeader.hasClass('et-fixed-header')){
                $a.append($fixedLogo);
    		} else {
                $a.append($mainLogo);
    		}
        } else {
            $a.append($mobileLogo);
        }

        // FIXME: THis causes issue with fixed logo on desktop
        // FIXME: In mobile-menu-breakpoing, for certain situations we add #dipi_logo which is a clone. We need to fix mobile logo etc. for this as wel
		// 	$('#dipi_logo').attr('src', '');
		// 	$('#logo').attr('src', '');
		// 	// $('.et-l--header .et_pb_menu__logo img').attr('src', '');
		// } else {
		// 	$('#logo').attr('src', logo_src);
		// 	// $('.et-l--header .et_pb_menu__logo img').attr('src', tbLogoUrl);
		// }

    }
    
    function callback(mutationList, observer) {
        mutationList.forEach(function(mutation){
            if('attributes' != mutation.type || 'class' !== mutation.attributeName){
                return;
            }

            dipi_logo_change();
        });
    }

    var targetNode = document.querySelector("#main-header");
    var observerOptions = {
        childList: false,
        attributes: true,
        subtree: false
    }

    if(targetNode){
        var observer = new MutationObserver(callback);
        observer.observe(targetNode, observerOptions);
    }

    // Observe resize events to switch between mobile/fixed logos
    $(window).resize(dipi_logo_change);

    // finally call the callback manually once to get started
    dipi_logo_change();
});
</script>