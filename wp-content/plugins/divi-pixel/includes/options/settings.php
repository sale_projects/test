<?php
namespace DiviPixel;

/**
 * The DIPI_Settings class is a utility class which declares all the setings fields.
 */
class DIPI_Settings
{
    private static $instance = null;
    private static $settings_prefix = 'dipi_';

    // Internal caches to reduce processing time
    private $tabs;
    private $sections;
    private $toggles;
    private $fields;
    private $pages;

    /**
     * Settings instance
     *
     * @since 1.6.0
     * @return DIPI_Settings
     */
    public static function instance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    public static function settings_prefix()
    {
        return self::$settings_prefix;
    }

    public static function is_usable_license(){
        $license_status = self::get_option('license_status');
        return $license_status === 'valid' || $license_status === 'expired';
    }

    /**
     * Default function for retrieving Divi Pixel settings from the database
     *
     * This function uses WordPress get_option() to retrieve the Divi Pixel settings in an
     * unified way. Settings might be transformed and be different from whats stored inside
     * the database. Therefore if not using this function (which should almost alawys be used).
     * You have to apply possible transformations by yourself, for example transforming on/off
     * into true/false.
     *
     * Special transformations are done to settings fields of the following types:
     * - checkbox: on/off is transformed into true/false values which can directly be used in if
     *  statements
     * - everything else: return the value stored in the DB. If no value is stored, returns the
     *  default value. If no default value is defined, returns false
     * @param string $option The option to load.
     * @return mixed Option in an usable way. False if neither option nor default value is set
     */
    public static function get_option($option)
    {
        // Load fields and check if $option exists
        $fields = self::instance()->get_fields();
        if (!isset($fields[$option])) {
            dipi_log("DIPI_Settings::get_option() - Unknown Setting: " . $option);
            return false;
        }

        // Load value of $option
        $prefix = self::settings_prefix();
        $value = get_option("{$prefix}{$option}");
        $default = isset($fields[$option]['default']) ? $fields[$option]['default'] : null;

        switch ($fields[$option]['type']) {
            // Convert on/off to boolean value
            case 'checkbox':
                if ('on' === $value) {
                    return true;
                } else if ('off' === $value) {
                    return false;
                } else if (!is_null($default)) {
                    return 'on' === $default;
                } else {
                    return false;
                }
            case 'select2':
                return empty($value) ? [] : $value;
            default:
                // By default, if $value is empty, return default if default is not null
                if (!empty($value)) {
                    return $value;
                } else if (!is_null($default)) {
                    return $default;
                } else {
                    return false;
                }
        }
    }

    public static function update_option($option, $value)
    {
        update_option(self::settings_prefix() . $option, $value);
    }

    /**
     * Globale Getter
     */

    public static function get_mobile_menu_breakpoint()
    {
        if (!self::get_option('custom_breakpoints')) {
            return 980;
        }

        $breakpoint_mobile = self::get_option('breakpoint_mobile');
        if (!$breakpoint_mobile || !is_numeric($breakpoint_mobile)) {
            return 980;
        }

        return intval($breakpoint_mobile);
    }

    public function get_tabs()
    {
        if (null === $this->tabs) {
            $this->tabs = $this->create_tabs();
        }
        return $this->tabs;
    }

    public function get_sections()
    {
        if (null === $this->sections) {
            $this->sections = $this->create_sections();
        }
        return $this->sections;
    }

    public function get_toggles()
    {
        if (null === $this->toggles) {
            $this->toggles = $this->create_toggles();
        }
        return $this->toggles;
    }

    public function get_fields()
    {
        if (null === $this->fields) {
            $this->fields = $this->create_fields();
        }
        return $this->fields;
    }

    // FIXME: Wo wird das hier überhaupt benutzt?
    private function open_customize_url($type, $panel, $url)
    {
        $args = array(
            "autofocus[$type]" => $panel,
            'url' => rawurlencode(esc_url($url)),
        );
        return add_query_arg($args, admin_url('customize.php'));
    }

    private function create_tabs()
    {
        if(!self::is_usable_license()){
            return [
                'settings' => [
                    'label' => esc_html__('Settings', 'dipi-divi-pixel'),
                    'priority' => 70,
                    'icon_class' => 'dp-settings',
                ]
            ];
        } 

        return [
            'general' => [
                'label' => esc_html__('General', 'dipi-divi-pixel'),
                'priority' => 10,
                'icon_class' => 'dp-settings',
            ],
            'blog' => [
                'label' => esc_html__('Blog', 'dipi-divi-pixel'),
                'priority' => 20,
                'icon_class' => 'dp-blog',
            ],
            'social_media' => [
                'label' => esc_html__('Social Media', 'dipi-divi-pixel'),
                'priority' => 80,
                'icon_class' => 'dp-share',
            ],
            'mobile' => [
                'label' => esc_html__('Mobile', 'dipi-divi-pixel'),
                'priority' => 30,
                'icon_class' => 'dp-devices',
            ],
            'modules' => [
                'label' => esc_html__('Modules', 'dipi-divi-pixel'),
                'priority' => 40,
                'icon_class' => 'dp-switches',
            ],
            'injector' => [
                'label' => esc_html__('Layout Injector', 'dipi-divi-pixel'),
                'priority' => 50,
                'icon_class' => 'dp-layers',
            ],
            'settings' => [
                'label' => esc_html__('Settings', 'dipi-divi-pixel'),
                'priority' => 70,
                'icon_class' => 'dp-settings',
            ],
            'import_export' => [
                'label' => esc_html__('Import Export', 'dipi-divi-pixel'),
                'priority' => 80,
                'icon_class' => 'dp-settings',
            ],
        ];
    }

    private function create_sections()
    {
        if(!self::is_usable_license()){
            return [
                'settings_general' => [
                    'label' => esc_html__('General Settings', 'dipi-divi-pixel'),
                    'priority' => 10,
                    'tab' => 'settings',
                ]
            ];
        }
        return [
            'general' => [
                'label' => esc_html__('General Settings', 'dipi-divi-pixel'),
                'priority' => 10,
                'tab' => 'general',
            ],
            'header_navigation' => [
                'label' => esc_html__('Header & Navigation', 'dipi-divi-pixel'),
                'priority' => 30,
                'tab' => 'general',
            ],
            'footer' => [
                'label' => esc_html__('Footer', 'dipi-divi-pixel'),
                'priority' => 40,
                'tab' => 'general',
            ],
            'blog_general' => [
                'label' => esc_html__('General Setting', 'dipi-divi-pixel'),
                'priority' => 10,
                'tab' => 'blog',
            ],
            'mobile_general' => [
                'label' => esc_html__('General', 'dipi-divi-pixel'),
                'priority' => 10,
                'tab' => 'mobile',
            ],
            'mobile_menu' => [
                'label' => esc_html__('Mobile Menu', 'dipi-divi-pixel'),
                'priority' => 20,
                'tab' => 'mobile',
            ],
            'custom_modules' => [
                'label' => esc_html__('Custom Modules', 'dipi-divi-pixel'),
                'priority' => 10,
                'tab' => 'modules',
            ],
            'navigation_inject' => [
                'label' => esc_html__('Navigation', 'dipi-divi-pixel'),
                'priority' => 10,
                'tab' => 'injector',
            ],
            'footer_inject' => [
                'label' => esc_html__('Footer', 'dipi-divi-pixel'),
                'priority' => 20,
                'tab' => 'injector',
            ],
            'blog_inject' => [
                'label' => esc_html__('Blog', 'dipi-divi-pixel'),
                'priority' => 30,
                'tab' => 'injector',
            ],
            'error_page_inject' => [
                'label' => esc_html__('404 Error Page', 'dipi-divi-pixel'),
                'priority' => 40,
                'tab' => 'injector',
            ],
            'settings_general' => [
                'label' => esc_html__('General Settings', 'dipi-divi-pixel'),
                'priority' => 10,
                'tab' => 'settings',
            ],
            'social_media_general' => [
                'label' => esc_html__('General', 'dipi-divi-pixel'),
                'priority' => 10,
                'tab' => 'social_media',
            ],
            'social_media_networks' => [
                'label' => esc_html__('Networks', 'dipi-divi-pixel'),
                'priority' => 10,
                'tab' => 'social_media',
            ],
            'third_party_providers' => [
                'label' => esc_html__('Third Party Providers', 'dipi-divi-pixel'),
                'priority' => 10,
                'tab' => 'settings',
            ],
            'import_export' => [
                'label' => esc_html__('Import / Export', 'dipi-divi-pixel'),
                'priority' => 10,
                'tab' => 'import_export',
            ],
        ];
    }

    private function create_toggles()
    {
        $toggles = [];
        
        if(self::is_usable_license()){
            /**
             * General Tab
             */

            //General Section
            $toggles += $this->create_toggle('general', 'general', 'login_page');
            $toggles += $this->create_toggle('general', 'general', 'browser_scrollbar');
            $toggles += $this->create_toggle('general', 'general', 'svg_upload');
            $toggles += $this->create_toggle('general', 'general', 'back_to_top');
            $toggles += $this->create_toggle('general', 'general', 'hide_projects');
            //$toggles += $this->create_toggle('general', 'general', 'custom_icons');
            $toggles += $this->create_toggle('general', 'general', 'hide_projects');
            $toggles += $this->create_toggle('general', 'general', 'hide_admin_bar');
            $toggles += $this->create_toggle('general', 'general', 'custom_map_marker');
            $toggles += $this->create_toggle('general', 'general', 'custom_preloader');
            $toggles += $this->create_toggle('general', 'general', 'testimonials');
            //Header and Navigation Section
            $toggles += $this->create_toggle('general', 'header_navigation', 'menu_styles');
            $toggles += $this->create_toggle('general', 'header_navigation', 'header_underline');
            $toggles += $this->create_toggle('general', 'header_navigation', 'shrink_header');
            $toggles += $this->create_toggle('general', 'header_navigation', 'fixed_logo');
            $toggles += $this->create_toggle('general', 'header_navigation', 'zoom_logo');
            $toggles += $this->create_toggle('general', 'header_navigation', 'menu_styles');
            //Footer Section
            $toggles += $this->create_toggle('general', 'footer', 'footer_theme_customizer');
            $toggles += $this->create_toggle('general', 'footer', 'hide_bottom_bar');
            $toggles += $this->create_toggle('general', 'footer', 'fixed_footer');
            $toggles += $this->create_toggle('general', 'footer', 'reveal_footer');

            /**
             * Blog Tab
             */
            $toggles += $this->create_toggle('blog', 'blog_general', 'blog_theme_customizer');
            $toggles += $this->create_toggle('blog', 'blog_general', 'custom_archive_page');
            $toggles += $this->create_toggle('blog', 'blog_general', 'blog_meta_icons');
            $toggles += $this->create_toggle('blog', 'blog_general', 'blog_hide_excerpt');
            $toggles += $this->create_toggle('blog', 'blog_general', 'remove_sidebar');
            $toggles += $this->create_toggle('blog', 'blog_general', 'remove_sidebar');
            $toggles += $this->create_toggle('blog', 'blog_general', 'read_more_archive');
            $toggles += $this->create_toggle('blog', 'blog_general', 'read_more_button');
            $toggles += $this->create_toggle('blog', 'blog_general', 'author_box');
            $toggles += $this->create_toggle('blog', 'blog_general', 'blog_nav');
            $toggles += $this->create_toggle('blog', 'blog_general', 'related_articles');
            $toggles += $this->create_toggle('blog', 'blog_general', 'custom_comments');

            /**
             * Mobile Tab
             */
            $toggles += $this->create_toggle('mobile', 'mobile_general', 'mobile_theme_customizer');
            $toggles += $this->create_toggle('mobile', 'mobile_general', 'custom_breakpoints');
            $toggles += $this->create_toggle('mobile', 'mobile_general', 'fixed_mobile_header');
            $toggles += $this->create_toggle('mobile', 'mobile_general', 'search_icon_mobile');
            $toggles += $this->create_toggle('mobile', 'mobile_general', 'mobile_logo');
            $toggles += $this->create_toggle('mobile', 'mobile_general', 'center_content');
            $toggles += $this->create_toggle('mobile', 'mobile_menu', 'mobile_menu_style');
            $toggles += $this->create_toggle('mobile', 'mobile_menu', 'hamburger_animation');
            $toggles += $this->create_toggle('mobile', 'mobile_menu', 'collapse_submenu');
            $toggles += $this->create_toggle('mobile', 'mobile_menu', 'mobile_cta_btn');

            /**
             * Modules Tab
             */
            $toggles += $this->create_toggle('modules', 'custom_modules', 'modules_theme_customizer');
            $toggles += $this->create_toggle('modules', 'custom_modules', 'md_masonry_gallery');
            $toggles += $this->create_toggle('modules', 'custom_modules', 'md_testimonial');
            $toggles += $this->create_toggle('modules', 'custom_modules', 'md_countdown');
            $toggles += $this->create_toggle('modules', 'custom_modules', 'md_counter');
            $toggles += $this->create_toggle('modules', 'custom_modules', 'md_flip_box');
            $toggles += $this->create_toggle('modules', 'custom_modules', 'md_button_grid');
            $toggles += $this->create_toggle('modules', 'custom_modules', 'md_before_after_slider');
            $toggles += $this->create_toggle('modules', 'custom_modules', 'md_floating_multi_images');
            $toggles += $this->create_toggle('modules', 'custom_modules', 'md_tilt_image');
            $toggles += $this->create_toggle('modules', 'custom_modules', 'md_carousel');
            $toggles += $this->create_toggle('modules', 'custom_modules', 'md_typing_text');
            $toggles += $this->create_toggle('modules', 'custom_modules', 'md_star_rating');
            $toggles += $this->create_toggle('modules', 'custom_modules', 'md_breadcrumbs');
            $toggles += $this->create_toggle('modules', 'custom_modules', 'md_pricelist');
            $toggles += $this->create_toggle('modules', 'custom_modules', 'md_image_hotspot');
            $toggles += $this->create_toggle('modules', 'custom_modules', 'md_blog_slider');
            $toggles += $this->create_toggle('modules', 'custom_modules', 'md_hover_box');
            $toggles += $this->create_toggle('modules', 'custom_modules', 'md_fancy_text');
            $toggles += $this->create_toggle('modules', 'custom_modules', 'md_accordion_image');
            $toggles += $this->create_toggle('modules', 'custom_modules', 'md_scroll_image');
            $toggles += $this->create_toggle('modules', 'custom_modules', 'md_panorama');
            $toggles += $this->create_toggle('modules', 'custom_modules', 'md_reading_progress_bar');
            $toggles += $this->create_toggle('modules', 'custom_modules', 'md_image_magnifier');
            $toggles += $this->create_toggle('modules', 'custom_modules', 'md_lottie_icon');
            $toggles += $this->create_toggle('modules', 'custom_modules', 'md_image_showcase');
            $toggles += $this->create_toggle('modules', 'custom_modules', 'md_image_mask');
            $toggles += $this->create_toggle('modules', 'custom_modules', 'md_timeline');
            $toggles += $this->create_toggle('modules', 'custom_modules', 'md_content_toggle');
            $toggles += $this->create_toggle('modules', 'custom_modules', 'md_balloon');
            $toggles += $this->create_toggle('modules', 'custom_modules', 'md_instagram');
            $toggles += $this->create_toggle('modules', 'custom_modules', 'md_popup_maker');
            $toggles += $this->create_toggle('modules', 'custom_modules', 'md_filterable_grid');
            $toggles += $this->create_toggle('modules', 'custom_modules', 'md_video_lightbox');

            /**
             * Injector Tab
             */
            $toggles += $this->create_toggle('injector', 'navigation_inject', 'inject_theme_customizer');
            $toggles += $this->create_toggle('injector', 'navigation_inject', 'nav_injector');
            $toggles += $this->create_toggle('injector', 'footer_inject', 'footer_injector');
            $toggles += $this->create_toggle('injector', 'blog_inject', 'blog_injector');
            $toggles += $this->create_toggle('injector', 'error_page_inject', 'error_page');

        }
        
        /**
         * Settings Tab
         */
        $toggles += $this->create_toggle('settings', 'settings_general', 'settings_general_license');
        
        if(self::is_usable_license()){
            $toggles += $this->create_toggle('settings', 'settings_general', 'settings_general_reset');
            $toggles += $this->create_toggle('settings', 'settings_general', 'beta_programm');
            $toggles += $this->create_toggle('settings', 'third_party_providers', 'settings_google_api');
            $toggles += $this->create_toggle('settings', 'third_party_providers', 'settings_facebook_api');

            /**
             * Import/Export Tab
             */
            $toggles += $this->create_toggle('import_export', 'import_export', 'export');
            $toggles += $this->create_toggle('import_export', 'import_export', 'import');

            /**
             * Social Media Tab
             */
            $toggles += $this->create_toggle('social_media', 'social_media_general', 'social_media_theme_customizer');
            $toggles += $this->create_toggle('social_media', 'social_media_general', 'social_media_general');
            $toggles += $this->create_toggle('social_media', 'social_media_networks', 'social_media_facebook');
            $toggles += $this->create_toggle('social_media', 'social_media_networks', 'social_media_instagram');
            $toggles += $this->create_toggle('social_media', 'social_media_networks', 'social_media_twitter');
            $toggles += $this->create_toggle('social_media', 'social_media_networks', 'social_media_youtube');
            $toggles += $this->create_toggle('social_media', 'social_media_networks', 'social_media_pinterest');
            $toggles += $this->create_toggle('social_media', 'social_media_networks', 'social_media_vimeo');
            $toggles += $this->create_toggle('social_media', 'social_media_networks', 'social_media_tumblr');
            $toggles += $this->create_toggle('social_media', 'social_media_networks', 'social_media_linkedin');
            $toggles += $this->create_toggle('social_media', 'social_media_networks', 'social_media_flickr');
            $toggles += $this->create_toggle('social_media', 'social_media_networks', 'social_media_dribbble');
            $toggles += $this->create_toggle('social_media', 'social_media_networks', 'social_media_skype');
            $toggles += $this->create_toggle('social_media', 'social_media_networks', 'social_media_google');
            $toggles += $this->create_toggle('social_media', 'social_media_networks', 'social_media_xing');
            $toggles += $this->create_toggle('social_media', 'social_media_networks', 'social_media_whatsapp');
            $toggles += $this->create_toggle('social_media', 'social_media_networks', 'social_media_snapchat');
        }

        return $toggles;
    }

    /**
     * Use this function to create a toggle
     *
     * @since 1.0
     *
     * @param string $tab This is the tab, the toggle will be added to
     * @param string $section This is the section, the toggle will be added to
     * @param string $toggle This is the id of the toggle that will be added
     */
    private function create_toggle($tab, $section, $toggle)
    {
        return [
            $toggle => [
                'tab' => $tab,
                'section' => $section,
            ],
        ];
    }

    private function create_fields()
    {
        $fields = [];
        $fields += $this->create_general_tab_fields();
        $fields += $this->create_settings_tab_fields();
        $fields += $this->create_social_media_tab_fields();
        $fields += $this->create_blog_tab_fields();
        $fields += $this->create_mobile_tab_fields();
        $fields += $this->create_modules_tab_fields();
        $fields += $this->create_injector_tab_fields();
        return $fields;
    }

    private function create_general_tab_fields()
    {
        return [
            // sample fields code to remove //
            // 'test_image' => [
            //     'label' => esc_html__('Image Option Test', 'dipi-divi-pixel'),
            //     'description' => esc_html__('Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.', 'dipi-divi-pixel'),
            //     'type' => 'file_upload',
            //     'file_type' => 'image',
            //     'tab' => 'general',
            //     'section' => 'general',
            //     'toggle' => 'test',
            //     'new' => true,
            // ],
            //'test_text' => [
            //    'label' => esc_html__('Text Option Test', 'dipi-divi-pixel'),
            //    'description' => sprintf(esc_html('My template with %1$s. Cool, eh?', 'dipi-divi-pixel'), sprintf('<a href="customize.php?autofocus[section]=dipi_customizer_section_preloader" target="_blank">%1$s</a>', esc_html('Link', 'dipi-divi-pixel'))),
            //   'type' => 'text',
            //    'tab' => 'general',
            //    'section' => 'general',
            //    'toggle' => 'test',
            //    'default' => 'some default text',
            //],
            // 'test_library_layout' => [
            //     'label' => esc_html__('Library Layout Option Test', 'dipi-divi-pixel'),
            //     'description' => esc_html__('Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.', 'dipi-divi-pixel'),
            //     'type' => 'library_layout',
            //     'tab' => 'general',
            //     'section' => 'general',
            //     'toggle' => 'test',
            //     'coming_soon' => true,
            //     'default' => '3897',
            // ],
            // 'test_checkbox' => [
            //     'label' => esc_html__('Checkbox Option Test', 'dipi-divi-pixel'),
            //     'description' => esc_html__('Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.', 'dipi-divi-pixel'),
            //     'type' => 'checkbox',
            //     'tab' => 'general',
            //     'section' => 'general',
            //     'toggle' => 'test',
            //     'default' => 'on',
            //     'options' => [
            //         'off' => 'aus',
            //         'on' => 'an',
            //     ],
            // ],
            // 'test_select' => [
            //     'label' => esc_html__('Select Option Test', 'dipi-divi-pixel'),
            //     'description' => esc_html__('Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.', 'dipi-divi-pixel'),
            //     'type' => 'select',
            //     'tab' => 'general',
            //     'section' => 'general',
            //     'toggle' => 'test',
            //     'default' => 'b',
            //     'options' => [
            //         'a' => esc_html__('Select A', 'dipi-divi-pixel'),
            //         'b' => esc_html__('Select B', 'dipi-divi-pixel'),
            //         'c' => esc_html__('Select C', 'dipi-divi-pixel'),
            //     ],
            // ],
            // 'test_settings_multiple_buttons' => [
            //     'label' => esc_html__('Multiple Buttons Option Title', 'dipi-divi-pixel'),
            //     'description' => esc_html__('Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.', 'dipi-divi-pixel'),
            //     'type' => 'multiple_buttons',
            //     'tab' => 'general',
            //     'section' => 'general',
            //     'toggle' => 'test',
            //     'class' => "some-test",
            //     'default' => 'style4',
            //     'options' => array(
            //         'style1' => [
            //             'image' => plugin_dir_url( __FILE__ ) . 'assets/ham-slider.gif',
            //         ],
            //         'style2' => [
            //             'image' => plugin_dir_url( __FILE__ ) . 'assets/ham-spring.gif',
            //         ],
            //         'style3' => [
            //             'image' => plugin_dir_url( __FILE__ ) . 'assets/ham-collapse.gif',
            //         ],
            //         'style4' => [
            //             'image' => plugin_dir_url( __FILE__ ) . 'assets/ham-vortex.gif',
            //         ],
            //         'style5' => [
            //             'icon' => 'dp-shield',
            //             'title' => esc_html__('Style 3', 'dipi-divi-pixel'),
            //             'description' => esc_html__('This is Style 2', 'dipi-divi-pixel'),
            //         ],
            //     ),
            // ],
            // 'test_theme_customizer' => [
            //     'label' => esc_html__('Theme Customizer Option Test', 'dipi-divi-pixel'),
            //     'description' => esc_html__('Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.', 'dipi-divi-pixel'),
            //     'type' => 'theme_customizer',
            //     'icon' => 'dp-divi-pixel',
            //     'panel' => 'dipi', //optional, if not set, use 'section' or go to theme customizer in general when neither is set
            //     // 'section' => 'dipi_btt_section_1', //optional, if not set, use 'panel' or go to theme customizer in general when neither is set
            //     'tab' => 'general',
            //     'section' => 'general',
            //     'toggle' => 'test',
            // ],
            //END
            'login_page' => [
                'label' => esc_html__('Custom Login Page', 'dipi-divi-pixel'),
                'description' => sprintf(esc_html('Enable this option to customize login page. You can change logo, style form, login button and more. To customize login page go to Divi Pixel %1$s', 'dipi-divi-pixel'), sprintf('<a href="customize.php?autofocus[section]=dipi_customizer_section_login_page&url=%2$s" target="_blank">%1$s</a>', esc_html('Theme Customizer', 'dipi-divi-pixel'), rawurlencode(wp_login_url()))),
                'type' => 'checkbox',
                'tab' => 'general',
                'section' => 'general',
                'toggle' => 'login_page',
            ],

            'login_page_link' => [
                'label' => esc_html__('Custom Logo Url', 'dipi-divi-pixel'),
                'placeholder' => esc_html__('Custom URL', 'dipi-divi-pixel'),
                'type' => 'text',
                'tab' => 'general',
                'section' => 'general',
                'toggle' => 'login_page',
                'show_if' => [
                    'login_page' => "on",
                ],
            ],

            'browser_scrollbar' => [
                'label' => esc_html__('Custom Browser Scrollbar', 'dipi-divi-pixel'),
                //'description' => esc_html__('Enable this option to customize browser scrollbar', 'dipi-divi-pixel'),
                'description' => sprintf(esc_html('Enable this option to customize browser scrollbar. You can change style and colors in %1$s', 'dipi-divi-pixel'), sprintf('<a href="customize.php?autofocus[section]=dipi_customizer_section_browser_scrollbar" target="_blank">%1$s</a>', esc_html('Theme Customizer', 'dipi-divi-pixel'))),
                'type' => 'checkbox',
                'tab' => 'general',
                'section' => 'general',
                'toggle' => 'browser_scrollbar',
            ],

            'svg_upload' => [
                'label' => esc_html__('Allow SVG Uploads', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to allow SVG files upload', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'general',
                'section' => 'general',
                'toggle' => 'svg_upload',
            ],
            'back_to_top' => [
                'label' => esc_html__('Customize Back To Top Button', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to customize Back to Top Button.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'general',
                'section' => 'general',
                'toggle' => 'back_to_top',
            ],
            'btt_button_style' => [
                'label' => esc_html__('Select Button Style', 'dipi-divi-pixel'),
                'description' => esc_html__('Select Back To Top button style.', 'dipi-divi-pixel'),
                'type' => 'select',
                'tab' => 'general',
                'section' => 'general',
                'toggle' => 'back_to_top',
                'default' => 'display_icon',
                'options' => [
                    'display_icon' => esc_html__('Display Icon (Default)', 'dipi-divi-pixel'),
                    'display_text' => esc_html__('Display Text', 'dipi-divi-pixel'),
                    'display_text_icon' => esc_html__('Display Text with Icon', 'dipi-divi-pixel'),
                ],
                'show_if' => [
                    'back_to_top' => "on",
                ],
            ],
            'btt_custom_link' => [
                'label' => esc_html__('Custom Back To Top Link', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to define custom back to top button link.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'general',
                'section' => 'general',
                'toggle' => 'back_to_top',
                'show_if' => [
                    'back_to_top' => "on",
                ],
            ],
            'btt_link' => [
                'label' => esc_html__('Custom Back To Top Button URL', 'dipi-divi-pixel'),
                'placeholder' => esc_html__('https://www.example.com', 'dipi-divi-pixel'),
                'type' => 'text',
                'tab' => 'general',
                'section' => 'general',
                'toggle' => 'back_to_top',
                'show_if' => [
                    'btt_custom_link' => "on",
                    'back_to_top' => "on",
                ],
            ],
            'btt_theme_customizer' => [
                'label' => esc_html__('Customize Back To Top Button', 'dipi-divi-pixel'),
                'description' => esc_html__('Use Divi Pixel Customizer to custimize Back To Top button. Change style, color and icon and make your button unique with ease!', 'dipi-divi-pixel'),
                'type' => 'theme_customizer',
                'panel' => 'dipi_btt',
                'tab' => 'general',
                'section' => 'general',
                'toggle' => 'back_to_top',
                'customizer_section' => 'back_to_top',
                'show_if' => [
                    'back_to_top' => "on",
                ],
            ],
            'hide_projects' => [
                'label' => esc_html__('Hide Projects Custom Post Type', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to hide Projects tab in WP Dashboard', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'general',
                'section' => 'general',
                'toggle' => 'hide_projects',
            ],
            'rename_projects' => [
                'label' => esc_html__('Rename Projects Custom Post Type', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to rename Projects tab in WP Dashboard', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'general',
                'section' => 'general',
                'toggle' => 'hide_projects',
                'show_if' => [
                    'hide_projects' => "off",
                ],
            ],
            'rename_projects_singular' => [
                'label' => esc_html__('Singular Name', 'dipi-divi-pixel'),
                'placeholder' => esc_html__('Project', 'dipi-divi-pixel'),
                'type' => 'text',
                'tab' => 'general',
                'section' => 'general',
                'toggle' => 'hide_projects',
                'show_if' => [
                    'rename_projects' => "on",
                    'hide_projects' => "off",
                ],
            ],
            'rename_projects_plural' => [
                'label' => esc_html__('Plural Name', 'dipi-divi-pixel'),
                'placeholder' => esc_html__('Projects', 'dipi-divi-pixel'),
                'type' => 'text',
                'tab' => 'general',
                'section' => 'general',
                'toggle' => 'hide_projects',
                'show_if' => [
                    'rename_projects' => "on",
                    'hide_projects' => "off",
                ],
            ],
            'rename_projects_slug' => [
                'label' => esc_html__('Slug', 'dipi-divi-pixel'),
                'placeholder' => esc_html__('projects', 'dipi-divi-pixel'),
                'type' => 'text',
                'tab' => 'general',
                'section' => 'general',
                'toggle' => 'hide_projects',
                'show_if' => [
                    'rename_projects' => "on",
                    'hide_projects' => "off",
                ],
            ],

            // Testimonial
            'rename_testimonials' => [
                'label' => esc_html__('Rename Testimonials Custom Post Type', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to rename Testimonials tab in WP Dashboard', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'general',
                'section' => 'general',
                'toggle' => 'testimonials',
            ],
            'rename_testimonials_singular' => [
                'label' => esc_html__('Singular Name', 'dipi-divi-pixel'),
                'placeholder' => esc_html__('Testimonial', 'dipi-divi-pixel'),
                'type' => 'text',
                'tab' => 'general',
                'section' => 'general',
                'toggle' => 'testimonials',
                'show_if' => [
                    'rename_testimonials' => "on",
                ],
            ],
            'rename_testimonials_plural' => [
                'label' => esc_html__('Plural Name', 'dipi-divi-pixel'),
                'placeholder' => esc_html__('Testimonials', 'dipi-divi-pixel'),
                'type' => 'text',
                'tab' => 'general',
                'section' => 'general',
                'toggle' => 'testimonials',
                'show_if' => [
                    'rename_testimonials' => "on",
                ],
            ],
            'rename_testimonials_slug' => [
                'label' => esc_html__('Slug', 'dipi-divi-pixel'),
                'placeholder' => esc_html__('dipi_testimonial', 'dipi-divi-pixel'),
                'type' => 'text',
                'tab' => 'general',
                'section' => 'general',
                'toggle' => 'testimonials',
                'show_if' => [
                    'rename_testimonials' => "on",
                ],
            ],

            'hide_admin_bar' => [
                'label' => esc_html__('Hide Admin Bar', 'dipi-divi-pixel'),
                'description' => esc_html__('Hide the admin bar while on the front-end and activate it by hovering over the top of the window.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'general',
                'section' => 'general',
                'toggle' => 'hide_admin_bar',
                'options' => [
                    'off' => esc_html__('No', 'dipi-divi-pixel'),
                    'on' => esc_html__('Yes', 'dipi-divi-pixel'),
                ],
            ],
            'custom_map_marker' => [
                'label' => esc_html__('Add Custom Map Marker', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to add custom map marker.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'general',
                'section' => 'general',
                'toggle' => 'custom_map_marker',
            ],
            'upload_custom_marker' => [
                'label' => esc_html__('Upload Custom Map Marker', 'dipi-divi-pixel'),
                'description' => esc_html__('Select image that will be displayed on map.', 'dipi-divi-pixel'),
                'type' => 'file_upload',
                'file_type' => 'image',
                'tab' => 'general',
                'section' => 'general',
                'toggle' => 'custom_map_marker',
                'show_if' => [
                    'custom_map_marker' => "on",
                ],
            ],
            'custom_map_marker_anchor' => [
                'label' => esc_html__('Map Marker Anchor', 'dipi-divi-pixel'),
                'description' => esc_html__('The anchor defines which part of the map marker is placed at the geo location. A default baloon like map marker for example would have its anchor at the bottom center of the image, where the pointy tip of the baloon is.', 'dipi-divi-pixel'),
                'type' => 'select',
                'tab' => 'general',
                'section' => 'general',
                'toggle' => 'custom_map_marker',
                'default' => 'bottom_center',
                'options' => [
                    'top_left' => esc_html__('Top Left', 'dipi-divi-pixel'),
                    'top_center' => esc_html__('Top Center', 'dipi-divi-pixel'),
                    'top_right' => esc_html__('Top Right', 'dipi-divi-pixel'),
                    'center_left' => esc_html__('Center Left', 'dipi-divi-pixel'),
                    'center_center' => esc_html__('Center', 'dipi-divi-pixel'),
                    'center_right' => esc_html__('Center Right', 'dipi-divi-pixel'),
                    'bottom_left' => esc_html__('Bottom Left', 'dipi-divi-pixel'),
                    'bottom_center' => esc_html__('Bottom Center', 'dipi-divi-pixel'),
                    'bottom_right' => esc_html__('Bottom Right', 'dipi-divi-pixel'),
                ],
                'show_if' => [
                    'custom_map_marker' => "on",
                ],
            ],
            'custom_preloader' => [
                'label' => esc_html__('Add Preloader', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to add preloader to your website.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'general',
                'section' => 'general',
                'toggle' => 'custom_preloader',
            ],
            'custom_preloader_style' => [
                'label' => esc_html__('Select Preloader', 'dipi-divi-pixel'),
                'description' => esc_html__('Select preloader you would like to display on your website.', 'dipi-divi-pixel'),
                'type' => 'preloaders',
                'tab' => 'general',
                'section' => 'general',
                'toggle' => 'custom_preloader',
                'class' => "dipi_preloaders",
                'show_if' => [
                    'custom_preloader' => "on",
                    'custom_preloader_image' => "off",
                ],
            ],
            'custom_preloader_image' => [
                'label' => esc_html__('Upload Custom Preloader', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to upload custom preloader.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'general',
                'section' => 'general',
                'toggle' => 'custom_preloader',
                'show_if' => [
                    'custom_preloader' => "on",
                ],
            ],
            'upload_preloader' => [
                'label' => esc_html__('Upload Custom Preloader', 'dipi-divi-pixel'),
                'description' => esc_html__('Upload custom preloader image to be displayed on your website.', 'dipi-divi-pixel'),
                'type' => 'file_upload',
                'file_type' => 'image',
                'tab' => 'general',
                'section' => 'general',
                'toggle' => 'custom_preloader',
                'show_if' => [
                    'custom_preloader_image' => "on",
                    'custom_preloader' => "on",
                ],
            ],
            'custom_preloader_homepage' => [
                'label' => esc_html__('Display on Homepage Only', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to display preloader on homepage only.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'general',
                'section' => 'general',
                'toggle' => 'custom_preloader',
                'show_if' => [
                    'custom_preloader' => "on",
                ],
            ],
            'preloader_customizer' => [
                'label' => esc_html__('Customize Preloader in Divi Pixel Customizer', 'dipi-divi-pixel'),
                'description' => esc_html__('To customize your preloader size, colors go to Theme Customizer', 'dipi-divi-pixel'),
                'type' => 'theme_customizer',
                'customizer_section' => 'preloader',
                'tab' => 'general',
                'icon' => 'dp-preloader',
                'section' => 'general',
                'toggle' => 'custom_preloader',
                'show_if' => [
                    'custom_preloader' => "on",
                ],
            ],
            'menu_styles' => [
                'label' => esc_html__('Customize Header & Navigation Styles', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to customize header & navigation styles.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'general',
                'section' => 'header_navigation',
                'toggle' => 'menu_styles',
            ],
            'enable_menu_hover_styles' => [
                'label' => esc_html__('Hover Animation', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to add custom hover animations to the main menu.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'general',
                'section' => 'header_navigation',
                'toggle' => 'menu_styles',
                'show_if' => [
                    'menu_styles' => "on",
                ],
            ],
            'menu_hover_styles' => [
                'label' => esc_html__('Select Animation Style', 'dipi-divi-pixel'),
                'description' => esc_html__('Select hover animation style for your main menu links.', 'dipi-divi-pixel'),
                'type' => 'menu_styles',
                'class' => 'dipi_menu_styles',
                'tab' => 'general',
                'section' => 'header_navigation',
                'toggle' => 'menu_styles',
                'default' => 'three_dots',
                'show_if' => [
                    'menu_styles' => "on",
                    'enable_menu_hover_styles' => "on",
                ],
                'default' => 'three_dots',
                'options' => array(
                    'three_dots' => esc_html__('Three Dots', 'dipi-divi-pixel'),
                    'filled_background' => esc_html__('Filled Background', 'dipi-divi-pixel'),
                    'slide_up_below' => esc_html__('Slide Up Below', 'dipi-divi-pixel'),
                    'slide_down_below' => esc_html__('Slide Down Below', 'dipi-divi-pixel'),
                    'grow_below_left' => esc_html__('Grow Below Left', 'dipi-divi-pixel'),
                    'grow_below_center' => esc_html__('Grow Below Center', 'dipi-divi-pixel'),
                    'grow_below_right' => esc_html__('Grow Below Right', 'dipi-divi-pixel'),
                    'grow_above_and_below_left' => esc_html__('Grow Both Left', 'dipi-divi-pixel'),
                    'grow_above_and_below_center' => esc_html__('Grow Both Center', 'dipi-divi-pixel'),
                    'grow_above_and_below_right' => esc_html__('Grow Both Right', 'dipi-divi-pixel'),
                    'bracketed_out' => esc_html__('Bracketed Out', 'dipi-divi-pixel'),
                    'bracketed_in' => esc_html__('Bracketed In', 'dipi-divi-pixel'),

                ),
            ],
            'custom_dropdown' => [
                'label' => esc_html__('Custom Menu Dropdown', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to add custom menu dropdown styles and animation.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'general',
                'section' => 'header_navigation',
                'toggle' => 'menu_styles',
                'show_if' => [
                    'menu_styles' => "on",
                ],
            ],

            'menu_button' => [
                'label' => esc_html__('Add CTA Button to Main Menu', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to add custom button to main menu.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'general',
                'section' => 'header_navigation',
                'toggle' => 'menu_styles',
                'show_if' => [
                    'menu_styles' => "on",
                ],
            ],

            'menu_button_text' => [
                'label' => esc_html__('Button Text', 'dipi-divi-pixel'),
                'placeholder' => esc_html__('Button Text', 'dipi-divi-pixel'),
                'type' => 'text',
                'tab' => 'general',
                'default' => 'Click Here',
                'section' => 'header_navigation',
                'toggle' => 'menu_styles',
                'show_if' => [
                    'menu_styles' => "on",
                    'menu_button' => "on",
                ],
            ],

            'menu_button_url' => [
                'label' => esc_html__('Button URL', 'dipi-divi-pixel'),
                'placeholder' => esc_html__('https://www.example.com', 'dipi-divi-pixel'),
                'type' => 'text',
                'tab' => 'general',
                'default' => '#',
                'section' => 'header_navigation',
                'toggle' => 'menu_styles',
                'show_if' => [
                    'menu_styles' => "on",
                    'menu_button' => "on",
                ],
            ],

            'menu_button_placement' => [
                'label' => esc_html__('Apply Menu Button', 'dipi-divi-pixel'),
                'description' => esc_html__('Select place where to display menu button.', 'dipi-divi-pixel'),
                'type' => 'select',
                'tab' => 'general',
                'section' => 'header_navigation',
                'toggle' => 'menu_styles',
                'default' => 'a',
                'show_if' => [
                    'menu_styles' => "on",
                    'menu_button' => "on",
                ],
                'options' => [
                    'a' => esc_html__('Last Menu Item', 'dipi-divi-pixel'),
                    'b' => esc_html__('First Menu Item', 'dipi-divi-pixel'),
                ],
            ],
            'menu_button_classname' => [
                'label' => esc_html__('Button CSS Class', 'dipi-divi-pixel'),
                'type' => 'text',
                'tab' => 'general',
                'default' => '',
                'section' => 'header_navigation',
                'toggle' => 'menu_styles',
                'show_if' => [
                    'menu_styles' => "on",
                    'menu_button' => "on",
                ],
            ],
            'cta_btn_new_tab' => [
                'label' => esc_html__('Open CTA Button in New Tab', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option if you want Menu Button to open in new tab.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'general',
                'section' => 'header_navigation',
                'toggle' => 'menu_styles',
                'show_if' => [
                    'menu_styles' => "on",
                    'menu_button' => "on",
                ],
            ],
            'mobile_cta_btn' => [
                'label' => esc_html__('Hide CTA Button on Mobiles', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option if you do not want to display Menu Button on mobiles.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'general',
                'section' => 'header_navigation',
                'toggle' => 'menu_styles',
                'show_if' => [
                    'menu_styles' => "on",
                    'menu_button' => "on",
                ],
            ],

            'menu_customizer' => [
                'label' => esc_html__('Customize Menu in Divi Pixel Customizer', 'dipi-divi-pixel'),
                'description' => esc_html__('To customize your main menu and menu dropdown go to Divi Pixel Customizer', 'dipi-divi-pixel'),
                'type' => 'theme_customizer',
                'customizer_panel' => 'header',
                'tab' => 'general',
                'icon' => 'dp-header',
                'section' => 'header_navigation',
                'toggle' => 'menu_styles',
                'show_if' => [
                    'menu_styles' => "on",
                ],
            ],
            'header_underline' => [
                'label' => esc_html__('Remove Main Header Shadow', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to remove main header shadow.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'general',
                'section' => 'header_navigation',
                'toggle' => 'header_underline',
            ],
            'shrink_header' => [
                'label' => esc_html__('Do Not Shrink Header on Scroll', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to prevent header shrinking on scroll.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'general',
                'section' => 'header_navigation',
                'toggle' => 'shrink_header',
                'options' => [
                    'off' => esc_html__('No', 'dipi-divi-pixel'),
                    'on' => esc_html__('Yes', 'dipi-divi-pixel'),
                ],
            ],
            'fixed_logo' => [
                'label' => esc_html__('Change Logo on Scroll', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to display different logo on fixed navigation.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'general',
                'section' => 'header_navigation',
                'toggle' => 'fixed_logo',
                'options' => [
                    'off' => esc_html__('No', 'dipi-divi-pixel'),
                    'on' => esc_html__('Yes', 'dipi-divi-pixel'),
                ],
            ],
            'fixed_logo_image' => [
                'label' => esc_html__('Upload Fixed Menu Logo', 'dipi-divi-pixel'),
                'description' => esc_html__('Select logo image that will be displayed after scroll.', 'dipi-divi-pixel'),
                'type' => 'file_upload',
                'file_type' => 'image',
                'tab' => 'general',
                'section' => 'header_navigation',
                'toggle' => 'fixed_logo',
                'show_if' => [
                    'fixed_logo' => "on",
                ],
            ],
            'zoom_logo' => [
                'label' => esc_html__('Zoom Logo on Hover', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to add zoom hover effect to logo.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'general',
                'section' => 'header_navigation',
                'toggle' => 'zoom_logo',
                'options' => [
                    'off' => esc_html__('No', 'dipi-divi-pixel'),
                    'on' => esc_html__('Yes', 'dipi-divi-pixel'),
                ],
            ],

            'footer_theme_customizer' => [
                'label' => esc_html__('Customize Footer', 'dipi-divi-pixel'),
                'description' => esc_html__('You can customize footer bottom bar, footer menu and social media links in the Divi Pixel Theme Customizer.', 'dipi-divi-pixel'),
                'type' => 'theme_customizer',
                'icon' => 'dp-footer',
                'class' => 'first_customizer_field',
                'tab' => 'general',
                'section' => 'footer',
                'toggle' => 'footer_theme_customizer',
                'customizer_panel' => 'footer',
            ],

            'footer_layout' => [
                'label' => esc_html__('Use Custom Footer Layout', 'dipi-divi-pixel'),
                'description' => esc_html__('Display Divi Library item as a footer', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'injector',
                'section' => 'footer_inject',
                'toggle' => 'footer_injector',
            ],

            'select_footer_layout' => [
                'label' => esc_html__('Select footer layout', 'dipi-divi-pixel'),
                'placeholder' => esc_html__('Divi Library Dropdown', 'dipi-divi-pixel'),
                'type' => 'library_layout',
                'tab' => 'injector',
                'section' => 'footer_inject',
                'toggle' => 'footer_injector',
                'show_if' => [
                    'footer_layout' => "on",
                ],
            ],

            'hide_bottom_bar' => [
                'label' => esc_html__('Hide Footer Bottom Bar', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to hide bottom footer bar', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'general',
                'section' => 'footer',
                'toggle' => 'hide_bottom_bar',
            ],

            'fixed_footer' => [
                'label' => esc_html__('Force Footer to Bottom', 'dipi-divi-pixel'),
                'description' => esc_html__('Keep footer fixed at bottom of page, even on pages with little content.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'general',
                'section' => 'footer',
                'toggle' => 'fixed_footer',
            ],
            'reveal_footer' => [
                'label' => esc_html__('Reveal Footer Effect', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable Footer Reveal Effect', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'general',
                'section' => 'footer',
                'toggle' => 'reveal_footer',
            ],
            'reveal_desktop' => [
                'label' => esc_html__('Disable on Desktop', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option if you do not want to display the Footer Reveal effect on desktope devices.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'options' => [
                    'off' => esc_html__('No', 'dipi-divi-pixel'),
                    'on' => esc_html__('Yes', 'dipi-divi-pixel'),
                ],
                'show_if' => [
                    'reveal_footer' => "on",
                ],
                'tab' => 'general',
                'section' => 'footer',
                'toggle' => 'reveal_footer',
            ],
            'reveal_tablet' => [
                'label' => esc_html__('Disable on Tablet', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option if you do not want to display the Footer Reveal effect on tablet devices.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'options' => [
                    'off' => esc_html__('No', 'dipi-divi-pixel'),
                    'on' => esc_html__('Yes', 'dipi-divi-pixel'),
                ],
                'show_if' => [
                    'reveal_footer' => "on",
                ],
                'tab' => 'general',
                'section' => 'footer',
                'toggle' => 'reveal_footer',
            ],
            'reveal_phone' => [
                'label' => esc_html__('Disable on Phone', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option if you do not want to display the Footer Reveal effect on phone devices.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'options' => [
                    'off' => esc_html__('No', 'dipi-divi-pixel'),
                    'on' => esc_html__('Yes', 'dipi-divi-pixel'),
                ],
                'show_if' => [
                    'reveal_footer' => "on",
                ],
                'tab' => 'general',
                'section' => 'footer',
                'toggle' => 'reveal_footer',
            ],

            'footer_reveal_posts_type' => [
                'label' => esc_html__('Disable Posts Type', 'dipi-divi-pixel'),
                'description' => esc_html__('Disable Footer Reveal Effect on Posts Type', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'show_if' => [
                    'reveal_footer' => "on",
                ],
                'tab' => 'general',
                'section' => 'footer',
                'toggle' => 'reveal_footer',
            ],

            'footer_reveal_pages_type' => [
                'label' => esc_html__('Disable Pages Type', 'dipi-divi-pixel'),
                'description' => esc_html__('Disable Footer Reveal Effect on Pages Type', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'show_if' => [
                    'reveal_footer' => "on",
                ],
                'tab' => 'general',
                'section' => 'footer',
                'toggle' => 'reveal_footer',
            ],

            'footer_reveal_projects_type' => [
                'label' => esc_html__('Disable Projects Type', 'dipi-divi-pixel'),
                'description' => esc_html__('Disable Footer Reveal Effect on Projects Type', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'show_if' => [
                    'reveal_footer' => "on",
                ],
                'tab' => 'general',
                'section' => 'footer',
                'toggle' => 'reveal_footer',
            ],

            'footer_reveal_testimonials_type' => [
                'label' => esc_html__('Disable Testimonials Type', 'dipi-divi-pixel'),
                'description' => esc_html__('Disable Footer Reveal Effect on Testimonials Type', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'show_if' => [
                    'reveal_footer' => "on",
                ],
                'tab' => 'general',
                'section' => 'footer',
                'toggle' => 'reveal_footer',
            ],

        ];
    }

    private function create_blog_tab_fields()
    {
        return [
            'blog_theme_customizer' => [
                'label' => esc_html__('Customize Blog Archives', 'dipi-divi-pixel'),
                'description' => esc_html__('With Divi Pixel you can customizer blog archives & categories pages. You can select blog layout, add Related Artciles, customize comments section and more! To customize your Blog go to Theme Customizer.', 'dipi-divi-pixel'),
                'type' => 'theme_customizer',
                'icon' => 'dp-blog',
                'class' => 'first_customizer_field',
                'tab' => 'blog',
                'section' => 'blog_general',
                'toggle' => 'blog_theme_customizer',
                'customizer_panel' => 'blog',
            ],
            'custom_archive_page' => [
                'label' => esc_html__('Custom Archive Page Style ', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to set custom archive page style.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'blog',
                'section' => 'blog_general',
                'toggle' => 'custom_archive_page',
            ],
            'custom_archive_styles' => [
                'label' => esc_html__('Select Archive Page Layout', 'dipi-divi-pixel'),
                'description' => esc_html__('Select layout for archive, categories, author, tags and search results pages.', 'dipi-divi-pixel'),
                'type' => 'multiple_buttons',
                'tab' => 'blog',
                'section' => 'blog_general',
                'toggle' => 'custom_archive_page',
                'class' => "some-test",
                'show_if' => [
                    'custom_archive_page' => "on",
                ],
                'options' => array(
                    'style1' => [
                        'image' => 'dipi-blog-layout-01.png',
                    ],
                    'style2' => [
                        'image' => 'dipi-blog-layout-02.png',
                    ],
                    'style3' => [
                        'image' => 'dipi-blog-layout-03.png',
                    ],
                    'style4' => [
                        'image' => 'dipi-blog-layout-04.png',
                    ],
                    'style5' => [
                        'image' => 'dipi-blog-layout-05.png',
                    ],
                    'style6' => [
                        'image' => 'dipi-blog-layout-06.png',
                    ],
                ),
            ],
            'hide_excerpt_text' => [
                'label' => esc_html__('Hide Excerpt Text', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to hide excerpt text on single post archive/category pages.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'blog',
                'section' => 'blog_general',
                'toggle' => 'blog_hide_excerpt',
            ],
            'blog_meta_icons' => [
                'label' => esc_html__('Add icons to meta text', 'dipi-divi-pixel'),
                'description' => sprintf(esc_html('Enable this option to add date, author and comments icons to meta text and customize them using Divi Pixel %1$s', 'dipi-divi-pixel'), sprintf('<a href="customize.php?autofocus[section]=dipi_customizer_section_blog_archives" target="_blank">%1$s</a>', esc_html('Theme Customizer', 'dipi-divi-pixel'))),
                'type' => 'checkbox',
                'tab' => 'blog',
                'section' => 'blog_general',
                'toggle' => 'blog_meta_icons',
            ],
            'remove_sidebar' => [
                'label' => esc_html__('Remove Sidebar', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to remove sidebar from archive pages.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'blog',
                'section' => 'blog_general',
                'toggle' => 'remove_sidebar',
            ],
            'remove_sidebar_line' => [
                'label' => esc_html__('Remove Sidebar Border', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to remove sidebar border line.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'blog',
                'section' => 'blog_general',
                'toggle' => 'remove_sidebar',
                'show_if' => [
                    'remove_sidebar' => "off",
                ],
            ],
            'add_read_more_archive' => [
                'label' => esc_html__('Add Read More Button', 'dipi-divi-pixel'),
                'description' => sprintf(esc_html('Add Read More button to single posts on archive pages. To customize single blog post button go to Divi Pixel %1$s', 'dipi-divi-pixel'), sprintf('<a href="customize.php?autofocus[section]=dipi_customizer_section_blog_archives_btn" target="_blank">%1$s</a>', esc_html('Theme Customizer', 'dipi-divi-pixel'))),
                'type' => 'checkbox',
                'tab' => 'blog',
                'section' => 'blog_general',
                'toggle' => 'read_more_archive',
            ],
            'read_more_button_style' => [
                'label' => esc_html__('Button Style', 'dipi-divi-pixel'),
                'description' => esc_html__('Select Read More button style.', 'dipi-divi-pixel'),
                'type' => 'select',
                'tab' => 'blog',
                'section' => 'blog_general',
                'toggle' => 'read_more_archive',
                'default' => 'text_icon',
                'options' => [
                    'only_text' => esc_html__('Display Only Text', 'dipi-divi-pixel'),
                    'text_icon' => esc_html__('Display Text with Icon', 'dipi-divi-pixel'),
                    'only_icon' => esc_html__('Display Only Icon', 'dipi-divi-pixel'),
                ],
                'show_if' => [
                    'add_read_more_archive' => "on",
                ],
            ],
            'read_more_button' => [
                'label' => esc_html__('Change Read More Button Text', 'dipi-divi-pixel'),
                'description' => esc_html__('Replace the default Read More button text with custom text.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'blog',
                'section' => 'blog_general',
                'toggle' => 'read_more_archive',
                'show_if' => [
                    'add_read_more_archive' => "on",
                    'read_more_button_style' => ['text_icon', 'only_text'],
                ],
            ],
            'read_more_button_text' => [
                'label' => esc_html__('Read More Button Text', 'dipi-divi-pixel'),
                'placeholder' => esc_html__('Read More', 'dipi-divi-pixel'),
                'type' => 'text',
                'tab' => 'blog',
                'section' => 'blog_general',
                'toggle' => 'read_more_archive',
                'show_if' => [
                    'add_read_more_archive' => "on",
                    'read_more_button' => "on",
                    'read_more_button_style' => ['text_icon', 'only_text'],
                ],
            ],
            'author_box' => [
                'label' => esc_html__('Add Author Box', 'dipi-divi-pixel'),
                'description' => sprintf(esc_html('Enable this option to add author box below posts and customize it using Divi Pixel %1$s', 'dipi-divi-pixel'), sprintf('<a href="customize.php?autofocus[section]=dipi_customizer_section_blog_author_box" target="_blank">%1$s</a>', esc_html('Theme Customizer', 'dipi-divi-pixel'))),
                'type' => 'checkbox',
                'tab' => 'blog',
                'section' => 'blog_general',
                'toggle' => 'author_box',
            ],
            'blog_nav' => [
                'label' => esc_html__('Add Blog Navigation', 'dipi-divi-pixel'),
                'description' => sprintf(esc_html('Enable this option to display prev/next links below posts and customize it using Divi Pixel %1$s', 'dipi-divi-pixel'), sprintf('<a href="customize.php?autofocus[section]=dipi_customizer_section_blog_navigation" target="_blank">%1$s</a>', esc_html('Theme Customizer', 'dipi-divi-pixel'))),
                'type' => 'checkbox',
                'tab' => 'blog',
                'section' => 'blog_general',
                'toggle' => 'blog_nav',
            ],
            'blog_nav_prev' => [
                'label' => esc_html__('Previous Post Button Text', 'dipi-divi-pixel'),
                'description' => esc_html__('Add custom text for Previous Post button link.', 'dipi-divi-pixel'),
                'placeholder' => esc_html__('Previous Article', 'dipi-divi-pixel'),
                'default' => esc_html__('Previous Article', 'dipi-divi-pixel'),
                'type' => 'text',
                'tab' => 'blog',
                'section' => 'blog_general',
                'toggle' => 'blog_nav',
                'show_if' => [
                    'blog_nav' => "on",
                ],
            ],
            'blog_nav_next' => [
                'label' => esc_html__('Next Post Button Text', 'dipi-divi-pixel'),
                'description' => esc_html__('Add custom text for Next Post button link.', 'dipi-divi-pixel'),
                'placeholder' => esc_html__('Next Article', 'dipi-divi-pixel'),
                'default' => esc_html__('Next Article', 'dipi-divi-pixel'),
                'type' => 'text',
                'tab' => 'blog',
                'section' => 'blog_general',
                'toggle' => 'blog_nav',
                'show_if' => [
                    'blog_nav' => "on",
                ],
            ],
            'related_articles' => [
                'label' => esc_html__('Related Articles', 'dipi-divi-pixel'),
                'description' => sprintf(esc_html('Enable this option to display Related Articles below posts and customize it using Divi Pixel %1$s', 'dipi-divi-pixel'), sprintf('<a href="customize.php?autofocus[section]=dipi_customizer_section_blog_related_posts" target="_blank">%1$s</a>', esc_html('Theme Customizer', 'dipi-divi-pixel'))),
                'type' => 'checkbox',
                'tab' => 'blog',
                'section' => 'blog_general',
                'toggle' => 'related_articles',
            ],
            'related_articles_heading' => [
                'label' => esc_html__('Related Articles Heading Text', 'dipi-divi-pixel'),
                'description' => esc_html__('Add custom heading to your Related Articles section.', 'dipi-divi-pixel'),
                'placeholder' => esc_html__('Related Articles', 'dipi-divi-pixel'),
                'type' => 'text',
                'tab' => 'blog',
                'section' => 'blog_general',
                'toggle' => 'related_articles',
                'show_if' => [
                    'related_articles' => "on",
                ],
            ],
            'related_articles_limit' => [
                'label' => esc_html__('Number of Articles', 'dipi-divi-pixel'),
                'description' => esc_html__('Select number of posts displayed in Related Articles section.', 'dipi-divi-pixel'),
                'placeholder' => esc_html__('6', 'dipi-divi-pixel'),
                'type' => 'text',
                'tab' => 'blog',
                'section' => 'blog_general',
                'toggle' => 'related_articles',
                'show_if' => [
                    'related_articles' => "on",
                ],
            ],
            'enable_custom_comments' => [
                'label' => esc_html__('Customize Comments Section', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to customize blog post comments form. ', 'dipi-divi-pixel'),
                'description' => sprintf(esc_html('Enable this option to customize blog post comments section. To change form appearance go to Divi Pixel %1$s', 'dipi-divi-pixel'), sprintf('<a href="customize.php?autofocus[section]=dipi_customizer_section_blog_comments" target="_blank">%1$s</a>', esc_html('Theme Customizer', 'dipi-divi-pixel'))),
                'type' => 'checkbox',
                'tab' => 'blog',
                'section' => 'blog_general',
                'toggle' => 'custom_comments',
            ],
            'custom_comments_title' => [
                'label' => esc_html__('Comments Section Title', 'dipi-divi-pixel'),
                'description' => esc_html__('Add custom heading to your websites Comments section.', 'dipi-divi-pixel'),
                'placeholder' => esc_html__('Comments', 'dipi-divi-pixel'),
                'type' => 'text',
                'tab' => 'blog',
                'section' => 'blog_general',
                'toggle' => 'custom_comments',
                'show_if' => [
                    'custom_comments' => "on",
                ],
            ],
        ];
    }

    private function create_mobile_tab_fields()
    {
        return [
            'mobile_theme_customizer' => [
                'label' => esc_html__('Customize Mobile Menu with Ease', 'dipi-divi-pixel'),
                'description' => esc_html__('Divi Pixel offers tons of Mobile Menu customization. Select hamburger icon animation, change mobile menu colors, fonts and animation using Theme Customizer.', 'dipi-divi-pixel'),
                'type' => 'theme_customizer',
                'icon' => 'dp-devices',
                'class' => 'first_customizer_field',
                'tab' => 'mobile',
                'section' => 'mobile_general',
                'toggle' => 'mobile_theme_customizer',
                'customizer_panel' => 'mobile',
            ],

            'custom_breakpoints' => [
                'label' => esc_html__('Enable Custom Menu Breakpoint', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to set custom breakpoint for mobile menu.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'mobile',
                'section' => 'mobile_general',
                'toggle' => 'custom_breakpoints',
            ],

            'breakpoint_mobile' => [
                'label' => esc_html__('Display Mobile Menu Below', 'dipi-divi-pixel'),
                'description' => esc_html__('Choose below which width the mobile menu should show. On screens which are wider than the defined width, the normal desktop menu will be displayed. The number configured here is inclusive. For example, if you choose 980, the desktop menu will show on screens with a width of at least 981px.', 'dipi-divi-pixel'),
                'placeholder' => esc_html__('980', 'dipi-divi-pixel'),
                'type' => 'text',
                'tab' => 'mobile',
                'section' => 'mobile_general',
                'toggle' => 'custom_breakpoints',
                'show_if' => [
                    'custom_breakpoints' => "on",
                ],
            ],
            'fixed_mobile_header' => [
                'label' => esc_html__('Fixed Mobile Header', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to display fixed header on mobiles.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'mobile',
                'section' => 'mobile_general',
                'toggle' => 'fixed_mobile_header',
            ],
            'search_icon_mobile' => [
                'label' => esc_html__('Hide Search Icon on Mobiles', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to hide search icon on mobiles.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'mobile',
                'section' => 'mobile_general',
                'toggle' => 'search_icon_mobile',
            ],
            'mobile_logo' => [
                'label' => esc_html__('Change Logo on Mobiles', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to display custom logo on mobile devices.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'mobile',
                'section' => 'mobile_general',
                'toggle' => 'mobile_logo',
            ],
            'mobile_logo_url' => [
                'label' => esc_html__('Mobile Logo', 'dipi-divi-pixel'),
                'description' => esc_html__('Upload the logo you want to use on mobile devices.', 'dipi-divi-pixel'),
                'type' => 'file_upload',
                'file_type' => 'image',
                'tab' => 'mobile',
                'section' => 'mobile_general',
                'toggle' => 'mobile_logo',
                'show_if' => [
                    'mobile_logo' => "on",
                ],
            ],
            'mobile_menu_style' => [
                'label' => esc_html__('Enable Custom Mobile Menu Style', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to display custom mobile menu.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'mobile',
                'section' => 'mobile_menu',
                'toggle' => 'mobile_menu_style',
            ],
            'mobile_menu_fullscreen' => [
                'label' => esc_html__('Fullscreen Mobile Menu', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to display fullscreen mobile menu.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'mobile',
                'section' => 'mobile_menu',
                'toggle' => 'mobile_menu_style',
                'options' => [
                    'off' => esc_html__('No', 'dipi-divi-pixel'),
                    'on' => esc_html__('Yes', 'dipi-divi-pixel'),
                ],
                'show_if' => [
                    'mobile_menu_style' => "on",
                ],
            ],
            'hamburger_animation' => [
                'label' => esc_html__('Add Hamburger Icon Animation', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to add animation to mobile menu icon.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'mobile',
                'section' => 'mobile_menu',
                'toggle' => 'hamburger_animation',
                'options' => [
                    'off' => esc_html__('No', 'dipi-divi-pixel'),
                    'on' => esc_html__('Yes', 'dipi-divi-pixel'),
                ],
            ],
            'hamburger_animation_styles' => [
                'label' => esc_html__('Select Animation Style', 'dipi-divi-pixel'),
                'description' => esc_html__('Select animation style you would like to display after mobile menu icon is clicked.', 'dipi-divi-pixel'),
                'type' => 'multiple_buttons',
                'tab' => 'mobile',
                'section' => 'mobile_menu',
                'toggle' => 'hamburger_animation',
                //'class' => "some-test",
                'show_if' => [
                    'hamburger_animation' => "on",
                ],
                'default' => 'style1',
                'options' => array(
                    'hamburger--slider' => [
                        'image' => 'ham-slider.gif',
                        'title' => esc_html__('Slider', 'dipi-divi-pixel'),
                    ],
                    'hamburger--squeeze' => [
                        'image' => 'ham-squeeze.gif',
                        'title' => esc_html__('Squeeze', 'dipi-divi-pixel'),
                    ],
                    'hamburger--spin' => [
                        'image' => 'ham-spin.gif',
                        'title' => esc_html__('Spin', 'dipi-divi-pixel'),
                    ],
                    'hamburger--elastic' => [
                        'image' => 'ham-elastic.gif',
                        'title' => esc_html__('Elastic', 'dipi-divi-pixel'),
                    ],
                    'hamburger--collapse' => [
                        'image' => 'ham-collapse.gif',
                        'title' => esc_html__('Collapse', 'dipi-divi-pixel'),
                    ],
                    'hamburger--stand' => [
                        'image' => 'ham-stand.gif',
                        'title' => esc_html__('Stand', 'dipi-divi-pixel'),
                    ],
                    'hamburger--spring' => [
                        'image' => 'ham-spring.gif',
                        'title' => esc_html__('Spring', 'dipi-divi-pixel'),
                    ],
                    'hamburger--minus' => [
                        'image' => 'ham-minus.gif',
                        'title' => esc_html__('Minus', 'dipi-divi-pixel'),
                    ],
                    'hamburger--vortex' => [
                        'image' => 'ham-vortex.gif',
                        'title' => esc_html__('Vortex', 'dipi-divi-pixel'),
                    ],
                ),
            ],
            'hamburger_animation_customizer' => [
                'label' => esc_html__('Customize Mobile Menu Icon', 'dipi-divi-pixel'),
                'description' => esc_html__('Use Divi Pixel Customizer to custimize mobile menu icon. Change style, color and icon and make your mobile menu unique with ease!', 'dipi-divi-pixel'),
                'type' => 'theme_customizer',
                //'panel' => 'dipi_btt', //optional, if not set, use 'section' or go to theme customizer in general when neither is set
                // 'section' => 'dipi_btt_section_1', //optional, if not set, use 'panel' or go to theme customizer in general when neither is set
                'tab' => 'mobile',
                'section' => 'mobile_menu',
                'icon' => 'dp-hamburger',
                'toggle' => 'hamburger_animation',
                'customizer_section' => 'mobile_menu_effects',
                'show_if' => [
                    'hamburger_animation' => "on",
                ],
            ],

            'collapse_submenu' => [
                'label' => esc_html__('Collapse Submenu Items on Mobiles', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to collapse submenu items on mobiles.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'mobile',
                'section' => 'mobile_menu',
                'toggle' => 'collapse_submenu',
                'options' => [
                    'off' => esc_html__('No', 'dipi-divi-pixel'),
                    'on' => esc_html__('Yes', 'dipi-divi-pixel'),
                ],
            ],
        ];
    }

    private function create_modules_tab_fields()
    {
        return [
            //Modules Theme Customizer Toggle
            'modules_theme_customizer' => [
                'label' => esc_html__('Hide Custom Modules', 'dipi-divi-pixel'),
                'description' => esc_html__('Divi Pixel comes with 28 custom modules which by default are added to your Divi Builder. In this section you can hide specific modules which you think might not be be used on your website.', 'dipi-divi-pixel'),
                'type' => 'theme_customizer',
                'icon' => 'dp-switches',
                'class' => 'first_customizer_field no_button',
                'tab' => 'modules',
                'section' => 'custom_modules',
                'toggle' => 'modules_theme_customizer',
            ],
            // Modules Options
            'md_masonry_gallery' => [
                'label' => esc_html__('Hide Masonry Gallery', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to hide Masonry Gallery module from the Divi Builder.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'modules',
                'section' => 'custom_modules',
                'toggle' => 'md_masonry_gallery',
                'options' => [
                    'off' => esc_html__('No', 'dipi-divi-pixel'),
                    'on' => esc_html__('Yes', 'dipi-divi-pixel'),
                ],
            ],
            'md_testimonial' => [
                'label' => esc_html__('Hide Testimonial', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to hide Testimonial module from the Divi Builder.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'modules',
                'section' => 'custom_modules',
                'toggle' => 'md_testimonial',
                'options' => [
                    'off' => esc_html__('No', 'dipi-divi-pixel'),
                    'on' => esc_html__('Yes', 'dipi-divi-pixel'),
                ],
            ],
            'md_countdown' => [
                'label' => esc_html__('Hide Countdown', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to hide Countdown module from the Divi Builder.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'modules',
                'section' => 'custom_modules',
                'toggle' => 'md_countdown',
                'options' => [
                    'off' => esc_html__('No', 'dipi-divi-pixel'),
                    'on' => esc_html__('Yes', 'dipi-divi-pixel'),
                ],
            ],
            'md_blog_slider' => [
                'label' => esc_html__('Hide Blog Slider', 'diro-divi-pixel'),
                'description' => esc_html__('Enable this option to hide Blog Slider module from the Divi Builder.', 'diro-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'modules',
                'section' => 'custom_modules',
                'toggle' => 'md_blog_slider',
                'options' => [
                    'off' => esc_html__('No', 'diro-divi-pixel'),
                    'on' => esc_html__('Yes', 'diro-divi-pixel'),
                ],
            ],
            'md_counter' => [
                'label' => esc_html__('Hide Counter', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to hide Counter module from the Divi Builder.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'modules',
                'section' => 'custom_modules',
                'toggle' => 'md_counter',
                'options' => [
                    'off' => esc_html__('No', 'dipi-divi-pixel'),
                    'on' => esc_html__('Yes', 'dipi-divi-pixel'),
                ],
            ],
            'md_flip_box' => [
                'label' => esc_html__('Hide Flip Box', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to hide FlipBox module from the Divi Builder.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'modules',
                'section' => 'custom_modules',
                'toggle' => 'md_flip_box',
                'options' => [
                    'off' => esc_html__('No', 'dipi-divi-pixel'),
                    'on' => esc_html__('Yes', 'dipi-divi-pixel'),
                ],
            ],
            'md_button_grid' => [
                'label' => esc_html__('Hide Button Grid', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to hide Button Grid module from the Divi Builder.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'modules',
                'section' => 'custom_modules',
                'toggle' => 'md_button_grid',
                'options' => [
                    'off' => esc_html__('No', 'dipi-divi-pixel'),
                    'on' => esc_html__('Yes', 'dipi-divi-pixel'),
                ],
            ],
            'md_before_after_slider' => [
                'label' => esc_html__('Hide Before/After Slider', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to hide Before/After Slider module from the Divi Builder.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'modules',
                'section' => 'custom_modules',
                'toggle' => 'md_before_after_slider',
                'options' => [
                    'off' => esc_html__('No', 'dipi-divi-pixel'),
                    'on' => esc_html__('Yes', 'dipi-divi-pixel'),
                ],
            ],
            'md_floating_multi_images' => [
                'label' => esc_html__('Hide Floating Multi Images', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to hide Floating Multi Images module from the Divi Builder.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'modules',
                'section' => 'custom_modules',
                'toggle' => 'md_floating_multi_images',
                'options' => [
                    'off' => esc_html__('No', 'dipi-divi-pixel'),
                    'on' => esc_html__('Yes', 'dipi-divi-pixel'),
                ],
            ],
            'md_tilt_image' => [
                'label' => esc_html__('Hide Tilt Image', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to hide Tilt Image module from the Divi Builder.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'modules',
                'section' => 'custom_modules',
                'toggle' => 'md_tilt_image',
                'options' => [
                    'off' => esc_html__('No', 'dipi-divi-pixel'),
                    'on' => esc_html__('Yes', 'dipi-divi-pixel'),
                ],
            ],
            'md_carousel' => [
                'label' => esc_html__('Hide Carousel', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to hide Carousel module from the Divi Builder.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'modules',
                'section' => 'custom_modules',
                'toggle' => 'md_carousel',
                'options' => [
                    'off' => esc_html__('No', 'dipi-divi-pixel'),
                    'on' => esc_html__('Yes', 'dipi-divi-pixel'),
                ],
            ],
            'md_typing_text' => [
                'label' => esc_html__('Hide Typing Text Effect', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to hide Typing Text Effect module from the Divi Builder.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'modules',
                'section' => 'custom_modules',
                'toggle' => 'md_typing_text',
                'options' => [
                    'off' => esc_html__('No', 'dipi-divi-pixel'),
                    'on' => esc_html__('Yes', 'dipi-divi-pixel'),
                ],
            ],
            'md_star_rating' => [
                'label' => esc_html__('Hide Star Rating', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to hide Star Rating module from the Divi Builder.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'modules',
                'section' => 'custom_modules',
                'toggle' => 'md_star_rating',
                'options' => [
                    'off' => esc_html__('No', 'dipi-divi-pixel'),
                    'on' => esc_html__('Yes', 'dipi-divi-pixel'),
                ],
            ],
            'md_breadcrumbs' => [
                'label' => esc_html__('Hide Breadcrumbs', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to hide Breadcrumbs module from the Divi Builder.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'modules',
                'section' => 'custom_modules',
                'toggle' => 'md_breadcrumbs',
                'options' => [
                    'off' => esc_html__('No', 'dipi-divi-pixel'),
                    'on' => esc_html__('Yes', 'dipi-divi-pixel'),
                ],
            ],
            'md_pricelist' => [
                'label' => esc_html__('Hide Price List', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to hide Price List module from the Divi Builder.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'modules',
                'section' => 'custom_modules',
                'toggle' => 'md_pricelist',
                'options' => [
                    'off' => esc_html__('No', 'dipi-divi-pixel'),
                    'on' => esc_html__('Yes', 'dipi-divi-pixel'),
                ],
            ],
            'md_image_hotspot' => [
                'label' => esc_html__('Hide Image Hotspot', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to hide Image Hotspot module from the Divi Builder.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'modules',
                'section' => 'custom_modules',
                'toggle' => 'md_image_hotspot',
                'options' => [
                    'off' => esc_html__('No', 'dipi-divi-pixel'),
                    'on' => esc_html__('Yes', 'dipi-divi-pixel'),
                ],
            ],
            'md_hover_box' => [
                'label' => esc_html__('Hide Hover Box', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to hide Hover Box module from the Divi Builder.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'modules',
                'section' => 'custom_modules',
                'toggle' => 'md_hover_box',
                'options' => [
                    'off' => esc_html__('No', 'dipi-divi-pixel'),
                    'on' => esc_html__('Yes', 'dipi-divi-pixel'),
                ],
            ],
            'md_fancy_text' => [
                'label' => esc_html__('Hide Fancy Text', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to hide Fancy Text module from the Divi Builder.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'modules',
                'section' => 'custom_modules',
                'toggle' => 'md_fancy_text',
                'options' => [
                    'off' => esc_html__('No', 'dipi-divi-pixel'),
                    'on' => esc_html__('Yes', 'dipi-divi-pixel'),
                ],
            ],
            'md_accordion_image' => [
                'label' => esc_html__('Hide Accordion Image', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to hide Accordion Image module from the Divi Builder.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'modules',
                'section' => 'custom_modules',
                'toggle' => 'md_accordion_image',
                'options' => [
                    'off' => esc_html__('No', 'dipi-divi-pixel'),
                    'on' => esc_html__('Yes', 'dipi-divi-pixel'),
                ],
            ],
            'md_scroll_image' => [
                'label' => esc_html__('Hide Scroll Image', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to hide Scroll Image module from the Divi Builder.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'modules',
                'section' => 'custom_modules',
                'toggle' => 'md_scroll_image',
                'options' => [
                    'off' => esc_html__('No', 'dipi-divi-pixel'),
                    'on' => esc_html__('Yes', 'dipi-divi-pixel'),
                ],
            ],
            'md_panorama' => [
                'label' => esc_html__('Hide Panorama Module', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to hide Panorama module from the Divi Builder.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'modules',
                'section' => 'custom_modules',
                'toggle' => 'md_panorama',
                'options' => [
                    'off' => esc_html__('No', 'dipi-divi-pixel'),
                    'on' => esc_html__('Yes', 'dipi-divi-pixel'),
                ],
            ],
            'md_reading_progress_bar' => [
                'label' => esc_html__('Hide Reading Progress Bar', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to hide Reading Progress Bar module from the Divi Builder.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'modules',
                'section' => 'custom_modules',
                'toggle' => 'md_reading_progress_bar',
                'options' => [
                    'off' => esc_html__('No', 'dipi-divi-pixel'),
                    'on' => esc_html__('Yes', 'dipi-divi-pixel'),
                ],
            ],
            'md_image_magnifier' => [
                'label' => esc_html__('Hide Image Magnifier', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to hide Image Magnifier module from the Divi Builder.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'modules',
                'section' => 'custom_modules',
                'toggle' => 'md_image_magnifier',
                'options' => [
                    'off' => esc_html__('No', 'dipi-divi-pixel'),
                    'on' => esc_html__('Yes', 'dipi-divi-pixel'),
                ],
            ],
            'md_lottie_icon' => [
                'label' => esc_html__('Hide Lottie Icon', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to hide Lottie Icon module from the Divi Builder.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'modules',
                'section' => 'custom_modules',
                'toggle' => 'md_lottie_icon',
                'options' => [
                    'off' => esc_html__('No', 'dipi-divi-pixel'),
                    'on' => esc_html__('Yes', 'dipi-divi-pixel'),
                ],
            ],
            'md_image_showcase' => [
                'label' => esc_html__('Hide Image Showcase', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to hide Image Showcase module from the Divi Builder.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'modules',
                'section' => 'custom_modules',
                'toggle' => 'md_image_showcase',
                'options' => [
                    'off' => esc_html__('No', 'dipi-divi-pixel'),
                    'on' => esc_html__('Yes', 'dipi-divi-pixel'),
                ],
            ],
            'md_image_mask' => [
                'label' => esc_html__('Hide Image Mask', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to hide Image Mask module from the Divi Builder.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'modules',
                'section' => 'custom_modules',
                'toggle' => 'md_image_mask',
                'options' => [
                    'off' => esc_html__('No', 'dipi-divi-pixel'),
                    'on' => esc_html__('Yes', 'dipi-divi-pixel'),
                ],
            ],
             'md_timeline' => [
                'label' => esc_html__('Hide Timeline', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to hide Timeline module from the Divi Builder.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'modules',
                'section' => 'custom_modules',
                'toggle' => 'md_timeline',
                'options' => [
                    'off' => esc_html__('No', 'dipi-divi-pixel'),
                    'on' => esc_html__('Yes', 'dipi-divi-pixel'),
                ],
            ],
            'md_content_toggle' => [
                'label' => esc_html__('Hide Content Toggle', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to hide Content Toggle module from the Divi Builder.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'modules',
                'section' => 'custom_modules',
                'toggle' => 'md_content_toggle',
                'options' => [
                    'off' => esc_html__('No', 'dipi-divi-pixel'),
                    'on' => esc_html__('Yes', 'dipi-divi-pixel'),
                ],
            ],
            'md_balloon' => [
                'label' => esc_html__('Hide Balloon Module', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to hide Balloon module from the Divi Builder.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'modules',
                'section' => 'custom_modules',
                'toggle' => 'md_balloon',
                'coming_soon' => true,
                'options' => [
                    'off' => esc_html__('No', 'dipi-divi-pixel'),
                    'on' => esc_html__('Yes', 'dipi-divi-pixel'),
                ],
            ],
            'md_instagram' => [
                'label' => esc_html__('Hide Instagram', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to hide Instagram module from the Divi Builder.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'modules',
                'section' => 'custom_modules',
                'toggle' => 'md_instagram',
                'coming_soon' => true,
                'options' => [
                    'off' => esc_html__('No', 'dipi-divi-pixel'),
                    'on' => esc_html__('Yes', 'dipi-divi-pixel'),
                ],
            ],
            'md_popup_maker' => [
                'label' => esc_html__('Hide Popup Maker', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to hide Popup Maker Custom Post Type', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'modules',
                'section' => 'custom_modules',
                'toggle' => 'md_popup_maker',
                'coming_soon' => true,
                'options' => [
                    'off' => esc_html__('No', 'dipi-divi-pixel'),
                    'on' => esc_html__('Yes', 'dipi-divi-pixel'),
                ],
            ],
            'md_filterable_grid' => [
                'label' => esc_html__('Hide Filterable Grid', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to hide Filterable Grid module from the Divi Builder.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'modules',
                'section' => 'custom_modules',
                'toggle' => 'md_filterable_grid',
                'coming_soon' => true,
                'options' => [
                    'off' => esc_html__('No', 'dipi-divi-pixel'),
                    'on' => esc_html__('Yes', 'dipi-divi-pixel'),
                ],
            ],
            'md_video_lightbox' => [
                'label' => esc_html__('Hide Video Lightbox', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to hide Video Lightbox module from the Divi Builder.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'modules',
                'section' => 'custom_modules',
                'toggle' => 'md_video_lightbox',
                'coming_soon' => true,
                'options' => [
                    'off' => esc_html__('No', 'dipi-divi-pixel'),
                    'on' => esc_html__('Yes', 'dipi-divi-pixel'),
                ],
            ],
        ];
    }

    private function create_injector_tab_fields()
    {
        return [
            'inject_theme_customizer' => [
                'label' => esc_html__('Layout Injector', 'dipi-divi-pixel'),
                'description' => esc_html__('Divi Pixel allows you to inject Divi Library items in places where by default it is not possible. Add custom layouts to your Divi website and display them globally with ease.', 'dipi-divi-pixel'),
                'type' => 'theme_customizer',
                'icon' => 'dp-layers',
                'class' => 'first_customizer_field no_button',
                'tab' => 'injector',
                'section' => 'navigation_inject',
                'toggle' => 'inject_theme_customizer',
            ],
            'before_nav_layout' => [
                'label' => esc_html__('Before Navigation Layout', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to display Divi Library item before main navigation.', 'dipi-divi-pixel'),
                'type' => 'library_layout',
                'tab' => 'injector',
                'section' => 'navigation_inject',
                'toggle' => 'nav_injector',
            ],
            'after_nav_layout' => [
                'label' => esc_html__('After Navigation Layout', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to display Divi Library item after main navigation.', 'dipi-divi-pixel'),
                'type' => 'library_layout',
                'tab' => 'injector',
                'section' => 'navigation_inject',
                'toggle' => 'nav_injector',
            ],
            'nav_layout_homepage' => [
                'label' => esc_html__('Display on Homepage Only', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to display Divi Library item on homepage only.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'injector',
                'section' => 'navigation_inject',
                'toggle' => 'nav_injector',
                'show_if' => [
                    'nav_layout_custom' => "off",
                ],
            ],
            'nav_layout_custom' => [
                'label' => esc_html__('Display on Specific Pages', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to display Divi Library item on specific pages only.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'injector',
                'section' => 'navigation_inject',
                'toggle' => 'nav_injector',
                'show_if' => [
                    'nav_layout_homepage' => "off",
                ],
            ],

            'nav_specific_pages' => [
                'label' => esc_html__('Select Pages', 'dipi-divi-pixel'),
                'description' => esc_html__('Select specific pages to show custom navigation', 'dipi-divi-pixel'),
                'type' => 'select2',
                'tab' => 'injector',
                'section' => 'navigation_inject',
                'toggle' => 'nav_injector',
                'computed' => true,
                'options' => [$this, 'get_pages'],
                'show_if' => [
                    'nav_layout_homepage' => "off",
                    'nav_layout_custom' => "on",
                ],
            ],

            'before_footer_layout' => [
                'label' => esc_html__('Before Footer Layout', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to display Divi Library item before footer.', 'dipi-divi-pixel'),
                'type' => 'library_layout',
                'tab' => 'injector',
                'section' => 'footer_inject',
                'toggle' => 'footer_injector',
            ],

            'after_footer_layout' => [
                'label' => esc_html__('After Footer Layout', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to display Divi Library item after footer.', 'dipi-divi-pixel'),
                'type' => 'library_layout',
                'tab' => 'injector',
                'section' => 'footer_inject',
                'toggle' => 'footer_injector',
            ],

            'footer_layout_homepage' => [
                'label' => esc_html__('Display on Homepage Only', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to display Divi Library item on homepage only.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'injector',
                'section' => 'footer_inject',
                'toggle' => 'footer_injector',
                'show_if' => [
                    'footer_layout_custom' => "off",
                ],
            ],

            'footer_layout_custom' => [
                'label' => esc_html__('Display on Specific Pages', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to display Divi Library item only on specific pages.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'injector',
                'section' => 'footer_inject',
                'toggle' => 'footer_injector',
                'show_if' => [
                    'footer_layout_homepage' => "off",
                ],
            ],

            'footer_specific_pages' => [
                'label' => esc_html__('Select Pages', 'dipi-divi-pixel'),
                'description' => esc_html__('Select specific pages to show custom footer', 'dipi-divi-pixel'),
                'type' => 'select2',
                'tab' => 'injector',
                'section' => 'footer_inject',
                'toggle' => 'footer_injector',
                //TODO: Compute
                'computed' => true,
                'options' => [$this, 'get_pages'],
                'show_if' => [
                    'footer_layout_homepage' => "off",
                    'footer_layout_custom' => "on",
                ],
            ],

            'error_page' => [
                'label' => esc_html__('Custom 404 Page', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to select custom 404 page from Divi Library.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'injector',
                'section' => 'error_page_inject',
                'toggle' => 'error_page',
            ],

            'select_error_page' => [
                'label' => esc_html__('Select 404 Page', 'dipi-divi-pixel'),
                'description' => esc_html__('Select Divi Library layout for 404 page.', 'dipi-divi-pixel'),
                'type' => 'library_layout',
                'tab' => 'injector',
                'section' => 'error_page_inject',
                'toggle' => 'error_page',
                'show_if' => [
                    'error_page' => "on",
                ],
            ],

            'error_page_header' => [
                'label' => esc_html__('Hide Header', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to hide header on error page.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'injector',
                'section' => 'error_page_inject',
                'toggle' => 'error_page',
                'show_if' => [
                    'error_page' => "on",
                ],
            ],

            'error_page_footer' => [
                'label' => esc_html__('Hide Footer', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to hide footer on error page.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'injector',
                'section' => 'error_page_inject',
                'toggle' => 'error_page',
                'show_if' => [
                    'error_page' => "on",

                ],
            ],

            'after_nav_post_layout' => [
                'label' => esc_html__('Single Post Header Layout - After Nav', 'dipi-divi-pixel'),
                'description' => esc_html__('This layout will be displayed globally after the main header and navigation on single blog post.', 'dipi-divi-pixel'),
                'type' => 'library_layout',
                'tab' => 'injector',
                'section' => 'blog_inject',
                'toggle' => 'blog_injector',
            ],

            'after_nav_archives' => [
                'label' => esc_html__('Archive Page Header Layout - After Nav', 'dipi-divi-pixel'),
                'description' => esc_html__('This layout will be displayed globally after the main header and navigation on archive pages.', 'dipi-divi-pixel'),
                'type' => 'library_layout',
                'tab' => 'injector',
                'section' => 'blog_inject',
                'toggle' => 'blog_injector',
            ],
            'after_nav_categories' => [
                'label' => esc_html__('Categories Header Layout - After Nav', 'dipi-divi-pixel'),
                'description' => esc_html__('This layout will be displayed globally after the main header and navigation on categories pages.', 'dipi-divi-pixel'),
                'type' => 'library_layout',
                'tab' => 'injector',
                'section' => 'blog_inject',
                'toggle' => 'blog_injector',
            ],
            'after_nav_search' => [
                'label' => esc_html__('Search Results Layout - After Nav', 'dipi-divi-pixel'),
                'description' => esc_html__('This layout will be displayed globally after the main header and navigation on search results page.', 'dipi-divi-pixel'),
                'type' => 'library_layout',
                'tab' => 'injector',
                'section' => 'blog_inject',
                'toggle' => 'blog_injector',
            ],
        ];
    }

    private function create_settings_tab_fields()
    {
        return [
            'license' => [
                'label' => esc_html__('License Key', 'dipi-divi-pixel'),
                'description' => esc_html__('Enter your license key here.', 'dipi-divi-pixel'),
                'type' => 'password',
                // 'type' => 'callback',
                // 'callback' => [$this, 'callback_license'],
                'tab' => 'settings',
                'section' => 'settings_general',
                'toggle' => 'settings_general_license',
                'sanitize_callback' => [$this, 'sanitize_license'],
            ],
            'license_status' => [
                'type' => 'skip',
                'default' => 'invalid',
            ],
            'license_limit' => [
                'type' => 'skip',
                'default' => 0, 
            ],
            'license_site_count' => [
                'type' => 'skip',
                'default' => 0, 
            ],

            'dipi_settings_reset_button' => [
                'label' => esc_html__('Reset Divi Pixel Settings', 'dipi-divi-pixel'),
                'description' => esc_html__('Reset all settings to their default state. Be aware that this canno be undone. We highly advise you to export your settings before resetting them, in case you want to roll back.', 'dipi-divi-pixel'),
                'type' => 'callback',
                'callback' => [$this, 'callback_reset_settings'],
                'tab' => 'settings',
                'section' => 'settings_general',
                'toggle' => 'settings_general_reset',
            ],

            'dipi_settings_reset_customizer_button' => [
                'label' => esc_html__('Reset Divi Pixel Customizer Settings', 'dipi-divi-pixel'),
                'description' => esc_html__('Reset all customizer settings to their default state. Be aware that this canno be undone. We highly advise you to export your settings before resetting them, in case you want to roll back.', 'dipi-divi-pixel'),
                'type' => 'callback',
                'callback' => [$this, 'callback_reset_customizer_settings'],
                'tab' => 'settings',
                'section' => 'settings_general',
                'toggle' => 'settings_general_reset',
            ],

            'enable_beta_program' => [
                'label' => esc_html__('Enable Beta Updates', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to get access to the latest beta builds. Usually you don\'t want this to be enabled on your production environment but if you encounter issues, our support team might ask you to enable this option.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'settings',
                'section' => 'settings_general',
                'toggle' => 'beta_programm',
                'options' => [
                    'off' => esc_html__('No', 'dipi-divi-pixel'),
                    'on' => esc_html__('Yes', 'dipi-divi-pixel'),
                ],
            ],

            'google_place_id' => [
                'label' => esc_html__('Google Place ID', 'dipi-divi-pixel'),
                'description' => esc_html__('Place IDs uniquely identify a place in the Google Places database and on Google Maps.', 'dipi-divi-pixel'),
                'placeholder' => esc_html__('Enter Google Place ID', 'dipi-divi-pixel'),
                'type' => 'text',
                'tab' => 'settings',
                'section' => 'third_party_providers',
                'toggle' => 'settings_google_api',
            ],

            'google_api_key' => [
                'label' => esc_html__('Google API Key', 'dipi-divi-pixel'),
                'description' => esc_html__('To use the Maps JavaScript API you must have an API key. The API key is a unique identifier that is used to authenticate requests associated with your project for usage and billing purposes.', 'dipi-divi-pixel'),
                'placeholder' => esc_html__('Enter Google API Key', 'dipi-divi-pixel'),
                'type' => 'text',
                'tab' => 'settings',
                'section' => 'third_party_providers',
                'toggle' => 'settings_google_api',
            ],

            'facebook_page_id' => [
                'label' => esc_html__('Facebook Page ID', 'dipi-divi-pixel'),
                'description' => esc_html__('Facebook requires you to input the Page ID when you fetch facebook reviews.', 'dipi-divi-pixel'),
                'placeholder' => esc_html__('Enter Facebook Page ID', 'dipi-divi-pixel'),
                'type' => 'text',
                'tab' => 'settings',
                'section' => 'third_party_providers',
                'toggle' => 'settings_facebook_api',
            ],

            'facebook_page_access_token' => [
                'label' => esc_html__('Facebook Page Access Token', 'dipi-divi-pixel'),
                'description' => sprintf(esc_html__('This kind of access token is similar to user access tokens, except that they provide permission to APIs that read, write or modify the data belonging to a Facebook Page. To obtain a page access token you need to start by obtaining a user access token and asking for the pages_show_list or manage_pages permission. Once you have the user access token you then get the page access token via the Graph API. To get page token %1$s', 'dipi-divi-pixel'), sprintf('<a href="https://divi-pixel.com/my-account/facebook-access-token/" target="_blank">%1$s</a>', esc_html('CLICK HERE', 'dipi-divi-pixel'))),
                'placeholder' => esc_html__('Enter FB Access Token', 'dipi-divi-pixel'),
                'type' => 'text',
                'tab' => 'settings',
                'section' => 'third_party_providers',
                'toggle' => 'settings_facebook_api',
            ],

            'export_settings' => [
                'label' => esc_html__('Export Settings Panel', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to export Divi Pixel plugin settings.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'import_export',
                'section' => 'import_export',
                'toggle' => 'export',
                'options' => [
                    'off' => esc_html__('No', 'dipi-divi-pixel'),
                    'on' => esc_html__('Yes', 'dipi-divi-pixel'),
                ],
            ],
            'export_customizer' => [
                'label' => esc_html__('Export Theme Customizer', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to export Divi Pixel Customizer settings.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'tab' => 'import_export',
                'section' => 'import_export',
                'toggle' => 'export',
                'options' => [
                    'off' => esc_html__('No', 'dipi-divi-pixel'),
                    'on' => esc_html__('Yes', 'dipi-divi-pixel'),
                ],
            ],
            'export_button' => [
                'label' => esc_html__('Export', 'dipi-divi-pixel'),
                'type' => 'callback',
                'description' => esc_html__('After choosing which options you would like to export, click the button to generate and dowload the export file.', 'dipi-divi-pixel'),
                'callback' => [$this, 'callback_export'],
                'tab' => 'import_export',
                'section' => 'import_export',
                'toggle' => 'export',
            ],
            'import_button' => [
                'label' => esc_html__('Import', 'dipi-divi-pixel'),
                'type' => 'callback',
                'description' => esc_html__('Choose the file your want to import and click the import button.', 'dipi-divi-pixel'),
                'callback' => [$this, 'callback_import'],
                'tab' => 'import_export',
                'section' => 'import_export',
                'toggle' => 'import',
            ],
        ];
    }

    private function create_social_media_tab_fields()
    {
        $fields = [

            //Social Media Theme Customizer Toggle
            'social_icons_theme_customizer' => [
                'label' => esc_html__('Customize Social Media Icons', 'dipi-divi-pixel'),
                'description' => esc_html__('With Divi Pixel you can select where and what social icons will be displayed on your website. You can also style your icons just like you want with a few clicks! To customize your social media icons go to Theme Customizer.', 'dipi-divi-pixel'),
                'type' => 'theme_customizer',
                'icon' => 'dp-share',
                'class' => 'first_customizer_field',
                'tab' => 'social_media',
                'section' => 'social_media_general',
                'toggle' => 'social_media_theme_customizer',
            ],

            'use_dipi_social_icons' => [
                'label' => esc_html__('Enable Divi Pixel Social Icons', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to use the Divi Pixel social icons. Make sure to disable the Divi social icons under Divi → Theme Options → General.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'options' => [
                    'off' => esc_html__('No', 'dipi-divi-pixel'),
                    'on' => esc_html__('Yes', 'dipi-divi-pixel'),
                ],
                'tab' => 'social_media',
                'section' => 'social_media_general',
                'toggle' => 'social_media_general',
            ],

            //Social Media General Toggle
            'social_links_new_tab' => [
                'label' => esc_html__('Open Social Links in New Tab', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to open social links in new window.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'options' => [
                    'off' => esc_html__('No', 'dipi-divi-pixel'),
                    'on' => esc_html__('Yes', 'dipi-divi-pixel'),
                ],
                'tab' => 'social_media',
                'section' => 'social_media_general',
                'toggle' => 'social_media_general',
                'show_if' => [
                    'use_dipi_social_icons' => 'on',
                ],
            ],
            'social_icons_individual_location' => [
                'label' => esc_html__('Individual Icon Locations', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to individually choose the display location of each icon.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'options' => [
                    'off' => esc_html__('No', 'dipi-divi-pixel'),
                    'on' => esc_html__('Yes', 'dipi-divi-pixel'),
                ],
                'tab' => 'social_media',
                'section' => 'social_media_general',
                'toggle' => 'social_media_general',
                'show_if' => [
                    'use_dipi_social_icons' => 'on',
                ],
            ],
            'social_icons_menu' => [
                'label' => esc_html__('Show in Header', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to show social media icons in header primary or secondary menu.', 'dipi-divi-pixel'),
                'type' => 'select',
                'tab' => 'social_media',
                'section' => 'social_media_general',
                'toggle' => 'social_media_general',
                'default' => 'none',
                'options' => [
                    'none' => esc_html__('Don\'t show in Header', 'dipi-divi-pixel'),
                    'primary' => esc_html__('Primary Menu', 'dipi-divi-pixel'),
                    'secondary' => esc_html__('Secondary Menu', 'dipi-divi-pixel'),
                ],
                'show_if' => [
                    'use_dipi_social_icons' => 'on',
                    'social_icons_individual_location' => 'off',
                ],
            ],
            'social_icons_mobile_menu' => [
                'label' => esc_html__('Show in Mobile Menu', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to show the icons in the mobile menu.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'options' => [
                    'off' => esc_html__('No', 'dipi-divi-pixel'),
                    'on' => esc_html__('Yes', 'dipi-divi-pixel'),
                ],
                'tab' => 'social_media',
                'section' => 'social_media_general',
                'toggle' => 'social_media_general',
                'show_if' => [
                    'use_dipi_social_icons' => 'on',
                    'social_icons_individual_location' => 'off',
                ],
            ],
            'social_icons_footer' => [
                'label' => esc_html__('Show in Footer', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to show the icons in the footer.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'options' => [
                    'off' => esc_html__('No', 'dipi-divi-pixel'),
                    'on' => esc_html__('Yes', 'dipi-divi-pixel'),
                ],
                'tab' => 'social_media',
                'section' => 'social_media_general',
                'toggle' => 'social_media_general',
                'show_if' => [
                    'use_dipi_social_icons' => 'on',
                    'social_icons_individual_location' => 'off',
                ],
            ],
        ];

        $fields += $this->create_social_network_fields('facebook', esc_html__('Facebook', 'dipi-divi-pixel'));
        $fields += $this->create_social_network_fields('instagram', esc_html__('Instagram', 'dipi-divi-pixel'));
        $fields += $this->create_social_network_fields('twitter', esc_html__('Twitter', 'dipi-divi-pixel'));
        $fields += $this->create_social_network_fields('youtube', esc_html__('YouTube', 'dipi-divi-pixel'));
        $fields += $this->create_social_network_fields('pinterest', esc_html__('Pinterest', 'dipi-divi-pixel'));
        $fields += $this->create_social_network_fields('vimeo', esc_html__('Vimeo', 'dipi-divi-pixel'));
        $fields += $this->create_social_network_fields('tumblr', esc_html__('Tumblr', 'dipi-divi-pixel'));
        $fields += $this->create_social_network_fields('linkedin', esc_html__('LinkedIn', 'dipi-divi-pixel'));
        $fields += $this->create_social_network_fields('flickr', esc_html__('Flickr', 'dipi-divi-pixel'));
        $fields += $this->create_social_network_fields('dribbble', esc_html__('Dribbble', 'dipi-divi-pixel'));
        $fields += $this->create_social_network_fields('skype', esc_html__('Skype', 'dipi-divi-pixel'));
        $fields += $this->create_social_network_fields('google', esc_html__('Google', 'dipi-divi-pixel'));
        $fields += $this->create_social_network_fields('xing', esc_html__('Xing', 'dipi-divi-pixel'));
        $fields += $this->create_social_network_fields('whatsapp', esc_html__('WhatsApp', 'dipi-divi-pixel'));
        $fields += $this->create_social_network_fields('snapchat', esc_html__('Snapchat', 'dipi-divi-pixel'));
        return $fields;
    }

    private function create_social_network_fields($id, $label)
    {
        return [
            "social_media_{$id}" => [
                'label' => $label,
                'placeholder' => sprintf('%1$s %2$s', $label, esc_html__('URL', 'dipi-divi-pixel')),
                'type' => 'text',
                'tab' => 'social_media',
                'class' => 'center_title',
                'section' => "social_media_networks",
                'toggle' => "social_media_{$id}",
            ],
            "social_media_{$id}_menu" => [
                'label' => esc_html__('Show in Header', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to show social media icons in header primary or secondary menu.', 'dipi-divi-pixel'),
                'type' => 'select',
                'tab' => 'social_media',
                'section' => "social_media_networks",
                'toggle' => "social_media_{$id}",
                'default' => 'none',
                'options' => [
                    'none' => esc_html__('Don\'t show in Header', 'dipi-divi-pixel'),
                    'primary' => esc_html__('Primary Menu', 'dipi-divi-pixel'),
                    'secondary' => esc_html__('Secondary Menu', 'dipi-divi-pixel'),
                ],
                'show_if' => [
                    'use_dipi_social_icons' => 'on',
                    'social_icons_individual_location' => 'on',
                ],
            ],
            "social_media_{$id}_footer" => [
                'label' => esc_html__('Show in Footer', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to show the icon in the footer.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'options' => [
                    'off' => esc_html__('No', 'dipi-divi-pixel'),
                    'on' => esc_html__('Yes', 'dipi-divi-pixel'),
                ],
                'tab' => 'social_media',
                'section' => "social_media_networks",
                'toggle' => "social_media_{$id}",
                'show_if' => [
                    'use_dipi_social_icons' => 'on',
                    'social_icons_individual_location' => 'on',
                ],
            ],
            "social_media_{$id}_mobile_menu" => [
                'label' => esc_html__('Show in Mobile Menu', 'dipi-divi-pixel'),
                'description' => esc_html__('Enable this option to show the icon in the mobile menu.', 'dipi-divi-pixel'),
                'type' => 'checkbox',
                'options' => [
                    'off' => esc_html__('No', 'dipi-divi-pixel'),
                    'on' => esc_html__('Yes', 'dipi-divi-pixel'),
                ],
                'tab' => 'social_media',
                'section' => "social_media_networks",
                'toggle' => "social_media_{$id}",
                'show_if' => [
                    'use_dipi_social_icons' => 'on',
                    'social_icons_individual_location' => 'on',
                ],
            ],
        ];
    }

    public function callback_reset_settings($field_id, $field)
    {
        // echo "Crazy, I am a callback setting with complete customized rendering. I was created by the field ". $field_id;
        $label = $field["label"];
        $description = isset($field["description"]) && $field["description"] && '' !== $field["description"] ? $field["description"] : "";
        $placeholder = isset($field["placeholder"]) && $field["placeholder"] ? esc_attr($field['placeholder']) : "";
        ?>
        <div class="dipi_row">
            <div class="dipi_settings_option_description col-md-6">
                <div class="dipi_option_label">
                    <?php echo $label; ?>
                </div>
                <?php if ('' !== $description): ?>
                    <div class="dipi_option_description">
                        <?php echo $description; ?>
                    </div>
                <?php endif;?>
            </div>
            <div class="dipi_settings_option_field dipi_settings_option_field_button col-md-6">
                <button type='button' class="button dipi_reset_settings_button" name='<?php echo $field_id; ?>' id='<?php echo $field_id; ?>'>
                    <?php echo esc_html__('Reset', 'dipi-divi-pixel'); ?>
                </button>
            </div>
        </div>
        <?php
    }

    public function callback_reset_customizer_settings($field_id, $field)
    {
        // echo "Crazy, I am a callback setting with complete customized rendering. I was created by the field ". $field_id;
        $label = $field["label"];
        $description = isset($field["description"]) && $field["description"] && '' !== $field["description"] ? $field["description"] : "";
        $placeholder = isset($field["placeholder"]) && $field["placeholder"] ? esc_attr($field['placeholder']) : "";
        ?>
        <div class="dipi_row">
            <div class="dipi_settings_option_description col-md-6">
                <div class="dipi_option_label">
                    <?php echo $label; ?>
                </div>
                <?php if ('' !== $description): ?>
                    <div class="dipi_option_description">
                        <?php echo $description; ?>
                    </div>
                <?php endif;?>
            </div>
            <div class="dipi_settings_option_field dipi_settings_option_field_button col-md-6">
                <button type='button' class="button dipi_reset_customizer_settings_button" name='<?php echo $field_id; ?>' id='<?php echo $field_id; ?>'>
                    <?php echo esc_html__('Reset', 'dipi-divi-pixel'); ?>
                </button>
            </div>
        </div>
        <?php
    }

    public function callback_license($id, $field)
    {
        $value = self::get_option($id);
        $license_status = self::get_option('license_status');
        $license_limit = self::get_option('license_limit');
        $license_site_count = self::get_option('license_site_count');
        
        $license_limit = $license_limit == 0 ? esc_html__('unlimited', 'dipi-divi-pixel') : $license_limit;

        $label = $field["label"];
        $description = isset($field["description"]) && $field["description"] && '' !== $field["description"] ? $field["description"] : "";
        $placeholder = isset($field["placeholder"]) && $field["placeholder"] ? esc_attr($field['placeholder']) : "";
        $additional_class = isset($field["class"]) ? $field["class"] : "";
        $value = empty($value) ? '' : constant("DIPI_PASSWORD_MASK");

        ?>
        <div class="dipi_row <?php echo $additional_class; ?>">
            <div class="dipi_settings_option_description col-md-6">
                <div class="dipi_option_label">
                    <?php echo $label; ?>
                </div>
                <?php if ('' !== $description) : ?>
                <div class="dipi_option_description">
                    <?php echo $description; ?>
                </div>        
                <?php endif; ?>
            </div>
            <div class="dipi_settings_option_field dipi_settings_option_field_text col-md-6">
                <input type='password' 
                    name='<?php echo $id; ?>' 
                    id='<?php echo $id; ?>' 
                    value='<?php echo $value; ?>' 
                    placeholder='<?php echo $placeholder; ?>'/>
                <div>
                    <button type='button' class="button dipi_deactivate_license_button" >
                        <div class="button_text">
                            <?php echo esc_html__('Deactivate License', 'dipi-divi-pixel'); ?>
                        </div>
                        <div class="ball-pulse loading_indicator" style="opacity: 0; position: absolute; height: 19px;">
                            <div></div>
                            <div></div>
                            <div></div>
                        </div>
                    </button>
                    <div>
                        <span class="dipi_license_status_indicator"></span>
                        <?php echo $license_status; ?> - <?php echo $license_site_count; ?> / <?php echo $license_limit; ?>
                    </div> 
                </div> 
            </div> 
        </div>  


        <div>---------------------------
        </div>  

        <div class="dipi_row">
            <div class="dipi_settings_option_description col-md-6"> 
                    <span class="dipi_license_status_icon dipi_license_status_<?php echo $license_status; ?>"></span>
                    <?php echo $license_status; ?> - <?php echo $license_site_count; ?> / <?php echo $license_limit; ?>
            </div>
            <div class="dipi_settings_option_field dipi_settings_option_field_button col-md-6">
                <button type='button' class="button dipi_activate_license_button">
                    <div class="button_text">
                        <?php echo esc_html__('Activate License', 'dipi-divi-pixel'); ?>
                    </div>
                    <div class="ball-pulse loading_indicator" style="opacity: 0; position: absolute; height: 19px;">
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>
                </button>
                <button type='button' class="button dipi_deactivate_license_button">
                    <div class="button_text">
                        <?php echo esc_html__('Deactivate License', 'dipi-divi-pixel'); ?>
                    </div>
                    <div class="ball-pulse loading_indicator" style="opacity: 0; position: absolute; height: 19px;">
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>
                </button>
            </div>
        </div>
        
        <?php


    }

    public function callback_export($field_id, $field)
    {
        $label = $field["label"];
        $description = $field["description"];
        ?>
        <div class="dipi_row">
            <div class="dipi_settings_option_description col-md-6">
                <div class="dipi_option_label">
                    <?php echo $label; ?>
                </div>
                <?php if ('' !== $description): ?>
                    <div class="dipi_option_description">
                        <?php echo $description; ?>
                    </div>
                <?php endif;?>
            </div>
            <div class="dipi_settings_option_field dipi_settings_option_field_button col-md-6">
                <button type='button' class="button dipi_export_button" name='<?php echo $field_id; ?>' id='<?php echo $field_id; ?>'>
                    <div class="button_text">
                        <?php echo esc_html__('Export', 'dipi-divi-pixel'); ?>
                    </div>
                    <div class="ball-pulse loading_indicator" style="opacity: 0; position: absolute; height: 19px;">
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>
                </button>
            </div>
        </div>
        <?php
    }

    public function callback_import($field_id, $field)
    {
        $label = $field["label"];
        $description = $field["description"];
        ?>
        <div class="dipi_row">
            <div class="dipi_settings_option_description col-md-6">
                <div class="dipi_option_label">
                    <?php echo $label; ?>
                </div>
                <?php if ('' !== $description): ?>
                    <div class="dipi_option_description">
                        <?php echo $description; ?>
                    </div>
                <?php endif;?>
            </div>
            <div class="dipi_settings_option_field dipi_settings_option_field_button col-md-6">
                <input type="file" name="dipi_import_file" id="dipi_import_file" />
                <button type='button' class="button dipi_import_button" name='<?php echo $field_id; ?>' id='<?php echo $field_id; ?>'>
                    <div class="button_text">
                        <?php echo esc_html__('Import', 'dipi-divi-pixel'); ?>
                    </div>
                    <div class="ball-pulse loading_indicator" style="opacity: 0; position: absolute; height: 19px;">
                        <div></div>
                        <div></div>
                        <div></div>
                    </div>
                </button>
            </div>
        </div>
        <?php
    }

    public function sanitize_license($license)
    {
        if ($license === constant("DIPI_PASSWORD_MASK")) {
            return self::get_option('license');
        } else {
            return $license;
        }
    }

    public static function sanitize_checkbox($input)
    {
        if (isset($input) && 'on' === $input) {
            return 'on';
        } else {
            return 'off';
        }
    }

    public function get_pages()
    {
        // FIXME: Call recursive function to load all pages or use DB Query
        if (is_null($this->pages)) {
            $this->pages = [];
            foreach (get_pages() as $page) {
                $title = !empty($page->post_title) ? $page->post_title : esc_html__('(no title)', 'dipi-divi-pixel');
                $this->pages[$page->ID] = $title;

                foreach (get_pages(['child_of' => $page->ID]) as $child_page) {
                    $title = !empty($child_page->post_title) ? $child_page->post_title : esc_html__('(no title)', 'dipi-divi-pixel');
                    $this->pages[$child_page->ID] = $child_page->post_title;
                }
            }
        }

        return $this->pages;
    }
}
