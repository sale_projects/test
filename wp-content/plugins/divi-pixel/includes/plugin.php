<?php
namespace DiviPixel;

class DIPI_Plugin
{
    private $admin;
    private $public;

    public function __construct()
    {
        $this->load_dependencies();

        $this->admin = new DIPI_Admin();
        $this->public = new DIPI_Public();

        new \EDD_SL_Plugin_Updater(constant('DIPI_STORE_URL'), constant('DIPI_PLUGIN_FILE'), array(
            'version' => constant('DIPI_VERSION'), // current version number
            'license' => trim(DIPI_Settings::get_option('license')),
            'item_id' => constant('DIPI_ITEM_ID'),
            'author' => constant('DIPI_AUTHOR'),
            'url' => home_url(),
            'beta' => DIPI_Settings::get_option('enable_beta_program'),
        ));

        add_action('init', [$this, 'init']);
        add_action('wp_enqueue_scripts', [$this, 'enqueue_scripts']);
        add_action('admin_enqueue_scripts', [$this, 'enqueue_scripts']);
        add_action('divi_extensions_init', [$this, 'divi_extensions_init']);
        add_action('et_head_meta', [$this, 'wp_head']);
        add_action('manage_dipi_testimonial_posts_custom_column', [$this, 'custom_dipi_testimonial_column'], 5, 2);

        add_filter('manage_dipi_testimonial_posts_columns', [$this, 'set_custom_edit_dipi_testimonial_columns'], 5);
        add_filter('wp_prepare_attachment_for_js', [$this, 'prepare_attachment_for_js'], 10, 3);
        
        register_activation_hook(DIPI_PLUGIN_FILE, [$this, 'dipi_plugin_activate']);
        register_deactivation_hook(DIPI_PLUGIN_FILE, [$this, 'dipi_plugin_deactivate']);
    }

    public function init(){
        wp_register_script('dipi_builder_utils', plugins_url('dist/js/builder_utils.min.js', constant('DIPI_PLUGIN_FILE')), [], constant('DIPI_VERSION'));
    }

    public function prepare_attachment_for_js($response, $attachment, $meta){
        if ( $response['mime'] == 'image/svg+xml' ) {
            $dimensions = $this->svg_dimensions( get_attached_file( $attachment->ID ) );
    
            if ( $dimensions ) {
                $response = array_merge( $response, $dimensions );
            }
    
            $possible_sizes = apply_filters( 'image_size_names_choose', array(
                'full'      => __( 'Full Size' ),
                'thumbnail' => __( 'Thumbnail' ),
                'medium'    => __( 'Medium' ),
                'large'     => __( 'Large' ),
            ) );
    
            $sizes = array();
    
            foreach ( $possible_sizes as $size => $label ) {
                $default_height = 2000;
                $default_width  = 2000;
    
                if ( 'full' === $size && $dimensions ) {
                    $default_height = $dimensions['height'];
                    $default_width  = $dimensions['width'];
                }
    
                $sizes[ $size ] = array(
                    'height'      => get_option( "{$size}_size_w", $default_height ),
                    'width'       => get_option( "{$size}_size_h", $default_width ),
                    'url'         => $response['url'],
                    'orientation' => 'portrait',
                );
            }
    
            $response['sizes'] = $sizes;
            $response['icon']  = $response['url'];
        }
    
        return $response;
    }

    private function svg_dimensions( $svg ) {
        $svg    = @simplexml_load_file( $svg );
        $width  = 0;
        $height = 0;
        if ( $svg ) {
            $attributes = $svg->attributes();
            if ( isset( $attributes->width, $attributes->height ) && is_numeric( $attributes->width ) && is_numeric( $attributes->height ) ) {
                $width  = floatval( $attributes->width );
                $height = floatval( $attributes->height );
            } elseif ( isset( $attributes->viewBox ) ) {
                $sizes = explode( ' ', $attributes->viewBox );
                if ( isset( $sizes[2], $sizes[3] ) ) {
                    $width  = floatval( $sizes[2] );
                    $height = floatval( $sizes[3] );
                }
            } else {
                return false;
            }
        }
    
        return array(
            'width'       => $width,
            'height'      => $height,
            'orientation' => ( $width > $height ) ? 'landscape' : 'portrait'
        );
    }

    public function divi_extensions_init()
    {
        require_once plugin_dir_path(dirname(__FILE__)) . 'includes/divi-extension.php';
    }

    public function dipi_plugin_activate()
    {
        if (!wp_next_scheduled('dipi_save_google_reviews_hook')) {
            wp_schedule_event(time(), 'twicedaily', 'dipi_save_google_reviews_hook');
        }
    
        if (!wp_next_scheduled('dipi_save_facebook_reviews_hook')) {
            wp_schedule_event(time(), 'twicedaily', 'dipi_save_facebook_reviews_hook');
        }

        if (!wp_next_scheduled('dipi_check_license_status')) {
            wp_schedule_event(time(), 'hourly', 'dipi_check_license_status', [true]);
        }

        flush_rewrite_rules();
    }

    public function dipi_plugin_deactivate()
    {
        wp_clear_scheduled_hook('dipi_save_google_reviews_hook');
        wp_clear_scheduled_hook('dipi_save_facebook_reviews_hook');
        wp_clear_scheduled_hook('dipi_check_license_status');
        flush_rewrite_rules();
    }

    private function load_dependencies()
    {
        // Load Settings API so we can use it in admin.php as well as on the frontend
        require_once plugin_dir_path(__FILE__) . 'options/settings.php';
        require_once plugin_dir_path(__FILE__) . 'options/customizer.php';

        //FIXME: For what do even use those? Also there is a bug where admin notices are re-appearing all the time. Maybe remove this
        require_once plugin_dir_path(dirname(__FILE__)) . 'includes/persist-admin-notices-dismissal/persist-admin-notices-dismissal.php';

        // Load admin and public areas
        require_once plugin_dir_path(dirname(__FILE__)) . 'admin/admin.php';
        require_once plugin_dir_path(dirname(__FILE__)) . 'public/public.php';

        // TODO: This is a backend only function, isn't it? If so, move to admin.php as we don't want to load that unnecessarily on the frontend
        if (!DIPI_Settings::get_option('md_testimonial')) {
            require_once plugin_dir_path(__FILE__) . 'modules/Testimonial/cpt.php';
            require_once plugin_dir_path(__FILE__) . 'modules/Testimonial/google-review.php';
            require_once plugin_dir_path(__FILE__) . 'modules/Testimonial/facebook-review.php';
        }

        /* Automatic plugin updates */
        if (!class_exists('EDD_SL_Plugin_Updater')) {
            include plugin_dir_path(__FILE__) . 'edd/EDD_SL_Plugin_Updater.php';
        }
    }

    public function wp_head()
    {
        if (DIPI_Settings::get_option('hamburger_animation')) {
            include plugin_dir_path(dirname(__FILE__)) . 'includes/partials/global-hamburger-styles-partial.php';
        }

    }

    public function enqueue_scripts()
    {
        //TODO: Wenn die Option für custom hamburger an ist, hamburger script und style enqueuen
        if (DIPI_Settings::get_option('hamburger_animation')) {
            wp_enqueue_style("dipi_hamburgers_css", plugin_dir_url(__FILE__) . 'css/hamburgers.min.css', [], "1.1.3", 'all');
            // wp_enqueue_script("dipi_hamburgers_js", plugin_dir_url(__FILE__) . 'js/hamburgers.js', ["jquery"], "1.0.0", false);
            add_action('et_header_top', [$this, 'add_hamburger']);
        }


        //FIXME: Improve this to only load those scripts and styles on pages where Divi Builder is active. maybe there is a
        //way to check if vb is currently showing, either as VB on frontend or BB on backend
        if(is_admin() || dipi_is_vb()){
            wp_enqueue_script('dipi_builder_utils');
            
            //Scripts which are used by modules in the Divi Builder also need to be enqueued in frontend (VB) and backend (Builder)
            wp_enqueue_script('easypiechart');
            wp_enqueue_script('dipi_masonry_gallery');
            wp_enqueue_script('dipi_counter');
            wp_enqueue_script('dipi_countdown');
            wp_enqueue_script('dipi_morphext');

            // Module styles which are needed in the Divi Builder
            wp_enqueue_style('dipi_pannellum');
            wp_enqueue_style('dipi_swiper');
            wp_enqueue_style('dipi_videojs');
            wp_enqueue_style('dipi_animate');
        }
    }

    public function add_hamburger()
    {
        include plugin_dir_path(dirname(__FILE__)) . 'includes/partials/hamburger-styles-partial.php';
    }

    public function set_custom_edit_dipi_testimonial_columns($columns)
    {

        $columns['testimonial_name'] = esc_html__('Name', 'dipi-divi-pixel');
        $columns['testimonial_type'] = esc_html__('Type', 'dipi-divi-pixel');
        $columns['company_name'] = esc_html__('Company', 'dipi-divi-pixel');
        $columns['testimonial_star'] = esc_html__('Rating', 'dipi-divi-pixel');

        return $columns;

    }

    public function custom_dipi_testimonial_column($column, $post_id)
    {
        switch ($column) {
            case 'testimonial_name':
                echo get_post_meta($post_id, 'testimonial_name', true);
                break;

            case 'testimonial_type':
                echo get_post_meta($post_id, 'testimonial_type', true);
                break;

            case 'company_name':
                echo get_post_meta($post_id, 'company_name', true);
                break;

            case 'testimonial_star':
                echo get_post_meta($post_id, 'testimonial_star', true);
                break;
        }
    }

}

new DIPI_Plugin();
