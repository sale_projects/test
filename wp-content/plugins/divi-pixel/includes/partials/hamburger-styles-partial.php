<?php 
namespace DiviPixel;
$style = DIPI_Settings::get_option('hamburger_animation_styles'); 
?>

<div class="dipi_hamburger hamburger <?php echo $style; ?>" style="display:none; float: right; margin-bottom: 24px; line-height: 1em;">
    <div class="hamburger-box">
        <div class="hamburger-inner"></div>
    </div>
</div>

<script type="text/javascript" id="dipi-hamburger-js">
jQuery(function($) {
    // Default Divi Header
    let $hamburger = $(".dipi_hamburger");
    let $body = $("body");
    if ($body.hasClass("et_header_style_slide") || $body.hasClass("et_header_style_fullscreen")) {
        
        $mobileMenuBar = $("#et-top-navigation .mobile_menu_bar");
        $mobileMenuBar.hide();
        $hamburger.click(function() {
            $hamburger.toggleClass("is-active");
            $mobileMenuBar.click();
        })

        document.addEventListener("click", function(event) {
            var $element = $(event.target);
            if (!$element.hasClass("et_toggle_fullscreen_menu")) {
                return;
            }
            if ($element.parent().attr('id') === 'et-top-navigation') {
                return;
            }
            $hamburger.toggleClass("is-active");
        });
    } else {
        let mobileMenu = $(".mobile_menu_bar_toggle");
        mobileMenu.append($hamburger.detach());
        $hamburger.css("float", "none");
        $hamburger.css("margin-bottom", "");
        mobileMenu.click(function() {
            $hamburger.toggleClass("is-active");
        })
    }

});

jQuery(window).on('load', function() {
    jQuery('.dipi_hamburger').show();
});
</script>