<?php

class DIPI_PriceListItem extends DIPI_Builder_Module
{

    public $slug = 'dipi_price_list_item';
    public $vb_support = 'on';
    public $type = 'child';
    public $child_title_var = 'title';
    public $child_title_fallback_var = 'admin';
	
	public function init()
    {
        $this->name = esc_html__('Pixel Price List', 'dipi-divi-pixel');
    }

    public function get_settings_modal_toggles()
    {
        return [
            'general' => [
                'toggles' => [
                    'content' => esc_html__('Content', 'dipi-divi-pixel'),
                ],
            ],

             'advanced' => [
                'toggles' => [
                    'text' => [
                        'title' => esc_html__('Text', 'et_builder'),
                        'tabbed_subtoggles' => true,
                        'sub_toggles' => [
                            'title' => [
                                'name' => 'Title',
                                'icon' => 'title',
                            ],
                            'price' => [
                                'name' => 'Price',
                                'icon' => 'price',
                            ],
                            'description' => [
                                'name' => 'Description',
                                'icon' => 'description',
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }

    public function get_fields()
    {
        return array(
            
            'title' => array(
                'label' => esc_html__('Title', 'dipi-divi-pixel'),
                'type' => 'text',
                'description' => esc_html__('The title of the item.', 'dipi-divi-pixel'),
                'toggle_slug' => 'content',
                'default'    => esc_html__('Title', 'dipi-divi-pixel'),
            ),

            'price' => array(
                'label' => esc_html__('Price', 'dipi-divi-pixel'),
                'type' => 'text',
                'description' => esc_html__('The price of the item.', 'dipi-divi-pixel'),
                'toggle_slug' => 'content',
                'default'    => esc_html__('$9.95', 'dipi-divi-pixel'),
            ),

            'content' => array(
                'label' => esc_html__('Description', 'dipi-divi-pixel'),
                'type' => 'tiny_mce',
                'description' => esc_html__('The (optional) description for this item.', 'dipi-divi-pixel'),
                'toggle_slug' => 'content',
            ),

            'image' => array(
                'label' => esc_html__('Image', 'dipi-divi-pixel'),
                'type' => 'upload',
                'description' => esc_html__('The (optional) image for this item.', 'dipi-divi-pixel'),
                'toggle_slug' => 'content',
                'upload_button_text' => esc_attr__('Upload Image', 'dipi-divi-pixel'),
                'choose_text' => esc_attr__('Choose Image', 'dipi-divi-pixel'),
                'update_text' => esc_attr__('Update Image', 'dipi-divi-pixel'),
                'hide_metadata' => true,
            ),

            'img_alt' => array(
                'label'       => esc_html__( 'Image Alt Text', 'dipi-divi-pixel' ),
                'type'        => 'text',
                'description' => esc_html__( 'Define the HTML ALT text for your image here.', 'dipi-divi-pixel'),
                'toggle_slug' => 'content',
            )
            
        );
    }

    public function get_advanced_fields_config()
    {
        $advanced_fields = [];
        $advanced_fields['fonts'] = false;
        $advanced_fields['text'] = false;
        $advanced_fields['text_shadow'] = false;
        $advanced_fields['filters'] = false;

        $advanced_fields['fonts']['title'] = [
            'label' => esc_html__('Title', 'dipi-divi-pixel'),
            'toggle_slug' => 'text',
            'sub_toggle' => 'title',
            'line_height' => array(
                'range_settings' => array(
                    'default' => '1em',
                    'min' => '1',
                    'max' => '3',
                    'step' => '0.1',
                ),
            ),
            'header_level' => [
                'default' => 'h2',
            ],
            'font_size' => array(
                'default' => '22px',
                'range_settings' => array(
                    'min' => '1',
                    'max' => '100',
                    'step' => '1',
                ),
            ),
            'css' => array(
                'main' => '%%order_class%% .dipi_price_list_title',
            ),
        ];

        $advanced_fields['fonts']['price'] = [
            'label' => esc_html__('Price', 'dipi-divi-pixel'),
            'toggle_slug' => 'text',
            'sub_toggle' => 'price',
            'line_height' => array(
                'range_settings' => array(
                    'default' => '1em',
                    'min' => '1',
                    'max' => '3',
                    'step' => '1',
                ),
            ),
            'font_size' => array(
                'default' => '22px',
                'range_settings' => array(
                    'min' => '1',
                    'max' => '100',
                    'step' => '1',
                ),
            ),
            'css' => array(
                'main' => '%%order_class%% .dipi_price_list_price',
            ),
        ];

        $advanced_fields['fonts']['description'] = [
            'label' => esc_html__('Description', 'dipi-divi-pixel'),
            'toggle_slug' => 'text',
            'sub_toggle' => 'description',
            'line_height' => array(
                'default' => '1em',
                'range_settings' => array(
                    'min' => '1',
                    'max' => '3',
                    'step' => '0.1',
                ),
            ),
            'font_size' => array(
                'default' => '16px',
                'range_settings' => array(
                    'min' => '1',
                    'max' => '100',
                    'step' => '1',
                ),
            ),
            'css' => array(
                'main' => '%%order_class%% .dipi_price_list_content',
            ),
        ];

        $advanced_fields['margin_padding'] = [
            'css' => [
                'important' => 'all',
            ],
        ];

        return $advanced_fields;
	}

    public function _render_image() {
        if (!$this->props['image'] || "" === $this->props['image']){
            return;
        } else {
            $img_alt = $this->props['img_alt'];
            $output = sprintf(
                '<div class="dipi_price_list_image_wrapper">
                    <img src="%1$s" alt="%2$s"/>
                </div>',
                $this->props['image'],
                $img_alt
            );

            return $output;

        }
    }

    public function render( $attrs, $content = null, $render_slug ) {
        
        $title_level = $this->props['title_level'] ? $this->props['title_level'] : 'h2';

		$output = sprintf(
			'<div class="dipi_price_list_item_wrapper">
        		%1$s
        		<div class="dipi_price_list_text_wrapper">
        			<div class="dipi_price_list_header">
        				<%5$s class="dipi_price_list_title">%2$s</%5$s>
                        <div class="dipi_price_list_separator"></div>
        				<div class="dipi_price_list_price">%3$s</div>
        			</div>
        			<div class="dipi_price_list_content">
                        %4$s
        			</div>
        		</div>
        	</div>',
        	$this->_render_image(),
        	$this->props['title'],
            $this->props['price'],
            $this->sanitize_content($this->props['content']),
            esc_attr($title_level)
        );

        return $output;
    }
    
    public function apply_css($render_slug) {
        ET_Builder_Element::set_style( $render_slug, array(
            'selector' => "%%order_class%% .dipi_price_list_separator",
            'declaration' => "border-bottom-style: 'dotted';",
        ));
    }

}
new DIPI_PriceListItem;