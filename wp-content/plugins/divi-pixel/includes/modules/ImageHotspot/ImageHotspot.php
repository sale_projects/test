<?php

class DIPI_ImageHotspot extends DIPI_Builder_Module {

	public $slug = 'dipi_image_hotspot';
	public $vb_support = 'on';

   protected $module_credits = array(
		'module_uri' => 'https://divi-pixel.com/modules/image-hotspot',
		'author' => 'Divi Pixel',
		'author_uri' => 'https://divi-pixel.com',
	);

	public function init() {
		$this->icon_path = plugin_dir_path(__FILE__) . " ";
		$this->name = esc_html__('Pixel Image Hotspot', 'dipi-divi-pixel');
		$this->child_slug = 'dipi_image_hotspot_child';
		$this->icon_path = plugin_dir_path(__FILE__) . "dp-image-hotspot.svg";
		$this->main_css_element = '%%order_class%%.dipi_image_hotspot';
	}

	public function get_settings_modal_toggles() 
	{
		return [
			'general' => [
				'toggles' => [
					'image' => esc_html__('Image', 'dipi-divi-pixel'),
				],
			],
			'advanced' => [
				'toggles' => [],
			],
		];
	}

	public function get_custom_css_fields_config() {

        $fields = [];

        $fields['hotspot_img'] = [
            'label'    => esc_html__('Hotspot Image', 'dipi-divi-pixel'),
            'selector' => '%%order_class%% .dipi-hotspot-image',
        ];

        $fields['hotspot_icon'] = [
            'label'    => esc_html__('Hotspot Icon', 'dipi-divi-pixel'),
            'selector' => '%%order_class%% .dipi-hotspot-icon',
        ];

        $fields['tooltip_img'] = [
            'label'    => esc_html__('Tooltip Image', 'dipi-divi-pixel'),
            'selector' => '%%order_class%% .dipi-tooltip-image',
        ];

        $fields['tooltip_icon'] = [
            'label'    => esc_html__('Tooltip Icon', 'dipi-divi-pixel'),
            'selector' => '%%order_class%% .dipi-tooltip-icon',
        ];
        
        $fields['title'] = [
            'label'    => esc_html__('Tooltip Title', 'dipi-divi-pixel'),
            'selector' => '%%order_class%% .dipi-tooltip-title',
        ];

        $fields['description'] = [
            'label'    => esc_html__('Tooltip Desc', 'dipi-divi-pixel'),
            'selector' => '%%order_class%% .dipi-tooltip-desc',
        ];

        $fields['button'] = [
            'label'    => esc_html__('Tooltip Button', 'dipi-divi-pixel'),
            'selector' => '%%order_class%% .dipi-tooltip-button',
        ];

        return $fields;
    }

	public function get_fields() 
	{

		$fields = [];

		$fields['img_src'] = [
            'type'               => 'upload',
            'option_category'    => 'basic_option',
			'hide_metadata'      => true,
			'upload_button_text' => esc_attr__('Upload an image', 'dipi-divi-pixel'),
            'choose_text' => esc_attr__('Choose an Image', 'dipi-divi-pixel'),
            'update_text' => esc_attr__('Set As Image', 'dipi-divi-pixel'),
            'description' => esc_html__('Upload an image to display in the module.', 'dipi-divi-pixel'),
            'dynamic_content'    => 'image',
            'toggle_slug'        => 'image'
        ];

        $fields["img_alt"] = [
			'label'       => esc_html__( 'Image Alt Text', 'dipi-divi-pixel' ),
			'type'        => 'text',
			'description' => esc_html__( 'Define the HTML ALT text for your image here.', 'dipi-divi-pixel'),
			'toggle_slug' => 'image'
        ];

		return $fields;
	}

	public function get_advanced_fields_config() 
	{

		$advanced_fields = [];

		$advanced_fields['fonts'] = false;
		$advanced_fields['text'] = false;
		$advanced_fields['text_shadow'] = false;
        $advanced_fields['link_options'] = false;

		$advanced_fields['margin_padding'] = [
		  'css' => [
			'margin' => '%%order_class%%',
			'padding' => '%%order_class%%',
			'important' => 'all',
		  ],
		];
		
		return $advanced_fields;
	}

	public function render($attrs, $content = null, $render_slug) {

        if(!isset($this->props['img_src']) || '' === $this->props['img_src']){
            return '';
        }

		$output = sprintf('
			<div class="dipi-image-hotspot">
				<img src="%2$s" class="dipi-hotspot-bg-image-main" alt="%3$s">
				%1$s
			</div>',
			$this->content,
			esc_attr($this->props['img_src']),
			esc_attr($this->props['img_alt'])
		);

		return $output;
	}
}

new DIPI_ImageHotspot;
