<?php
function rocket_lazyload_exclude_dipi_imageshowcase_class( array $attributes  ) {

    $attributes[] = 'class="div-pixel-mockup-img';

    return $attributes;
}
add_filter( 'rocket_lazyload_excluded_attributes', 'rocket_lazyload_exclude_dipi_imageshowcase_class' );
class DIPI_ImageShowcase extends DIPI_Builder_Module
{

    public $slug = 'dipi_image_showcase';
    public $vb_support = 'on';

    protected $module_credits = array(
        'module_uri' => 'https://divi-pixel.com/modules/masonry-gallery',
        'author' => 'Divi Pixel',
        'author_uri' => 'https://divi-pixel.com',
    );

    public function init()
    {
        $this->icon_path = plugin_dir_path(__FILE__) . "dp-image-showcase.svg";
        $this->name = esc_html__('Pixel Image Showcase', 'dipi-divi-pixel');
        $this->child_slug = 'dipi_image_showcase_child';
        $this->settings_modal_toggles = [
            'general' => [
                'toggles' => [
                    'main_content' => esc_html__('Image Showcase Settings', 'dipi-divi-pixel'),
                    'carousel' => esc_html__('Carousel Settings', 'dipi-divi-pixel')
                ],
            ],
        ];
    }

    public function get_fields()
    {
        $fields = [];
        $fields['mockup'] = [
            'label' => esc_html__('Showcase Mockup', 'dipi-divi-pixel'),
            'type' => 'select',
            'options' => array(
                esc_html__('iMac Front', 'dipi-divi-pixel'),
                esc_html__('iMac Left', 'dipi-divi-pixel'),
                esc_html__('iMac Right', 'dipi-divi-pixel'),
                esc_html__('iPad Front', 'dipi-divi-pixel'),
                esc_html__('iPad Left', 'dipi-divi-pixel'),
                esc_html__('iPad Right', 'dipi-divi-pixel'),
                esc_html__('MacBook Front', 'dipi-divi-pixel'),
                esc_html__('MacBook Right', 'dipi-divi-pixel'),
                esc_html__('MacBook Left', 'dipi-divi-pixel'),
                esc_html__('iPhone Front', 'dipi-divi-pixel'),
                esc_html__('iPhone Right', 'dipi-divi-pixel'),
                esc_html__('iPhone Left', 'dipi-divi-pixel'),
                esc_html__('iPhone Front White', 'dipi-divi-pixel'),
                esc_html__('iPhone Right White', 'dipi-divi-pixel'),
                esc_html__('iPhone Left White', 'dipi-divi-pixel'),
            ),
            'default' => 'iMac Front',
            'option_category' => 'basic_option',
            'toggle_slug' => 'main_content',
            'data_type' => '',
        ];
        $fields['effect'] = [
            'label' => esc_html__('Effect', 'dipi-divi-pixel'),
            'type' => 'select',
            'option_category' => 'layout',
            'options' => [
                'coverflow' => esc_html__('Coverflow', 'dipi-divi-pixel'),
                'slide' => esc_html__('Slide', 'dipi-divi-pixel'),
                'fade' => esc_html__('Fade', 'dipi-divi-pixel'),
            ],
            'default' => 'slide',
            'toggle_slug' => 'carousel',
        ];
        $fields['rotate'] = [
            'label' => esc_html('Rotate', 'dipi-divi-pixel'),
            'type' => 'range',
            'range_settings ' => [
                'min' => '0',
                'max' => '100',
                'step' => '1',
            ],
            'default' => '50',
            'show_if' => [
                'effect' => 'coverflow',
            ],
            'validate_unit' => false,
            'toggle_slug' => 'carousel',
        ];
        $fields['speed'] = [
            'label' => esc_html__('Transition Duration', 'dipi-divi-pixel'),
            'type' => 'range',
            'range_settings' => [
                'min' => '1',
                'max' => '5000',
                'step' => '100',
            ],
            'default' => 500,
            'validate_unit' => false,
            'toggle_slug' => 'carousel',
        ];

        $fields['loop'] = [
            'label' => esc_html__('Loop', 'dipi-divi-pixel'),
            'type' => 'yes_no_button',
            'option_category' => 'configuration',
            'options' => [
                'on' => esc_html__('Yes', 'dipi-divi-pixel'),
                'off' => esc_html__('No', 'dipi-divi-pixel'),
            ],
            'default' => 'off',
            'toggle_slug' => 'carousel',
        ];

        $fields['autoplay'] = [
            'label' => esc_html__('Autoplay', 'dipi-divi-pixel'),
            'type' => 'yes_no_button',
            'options' => [
                'off' => esc_html__('No', 'dipi-divi-pixel'),
                'on' => esc_html__('Yes', 'dipi-divi-pixel'),
            ],
            'default' => 'off',
            'toggle_slug' => 'carousel',
        ];

        $fields['pause_on_hover'] = [
            'label' => esc_html__('Pause on Hover', 'dipi-divi-pixel'),
            'type' => 'yes_no_button',
            'options' => [
                'off' => esc_html__('No', 'dipi-divi-pixel'),
                'on' => esc_html__('Yes', 'dipi-divi-pixel'),
            ],
            'show_if' => [
                'autoplay' => 'on',
            ],
            'toggle_slug' => 'carousel',
            'default' => 'on',
        ];

        $fields['autoplay_speed'] = [
            'label' => esc_html__('Autoplay Speed', 'dipi-divi-pixel'),
            'type' => 'range',
            'range_settings' => array(
                'min' => '1',
                'max' => '10000',
                'step' => '500',
            ),
            'default' => 5000,
            'validate_unit' => false,
            'show_if' => array(
                'autoplay' => 'on',
            ),
            'toggle_slug' => 'carousel',
        ];

        return $fields;
    }

    public function get_plugin_url($args = array(), $conditional_tags = array(), $current_page = array())
    {
        return DIPI_URI;
    }
    public function get_advanced_fields_config()
    {
        $advanced_fields = [
            'text' => false,
            'text_shadow' => false,
            'fonts' => false,
        ];

        return $advanced_fields;
    }
    public function get_carousel_content()
    {
        return $this->content;
    }

    public function render($attrs, $content = null, $render_slug)
    {
        wp_enqueue_script('dipi_image_showcase');
        wp_enqueue_style('dipi_swiper');

        $get_carousel_content = $this->get_carousel_content();

        $speed = $this->props['speed'];
        $loop = $this->props['loop'];
        $autoplay = $this->props['autoplay'];
        $autoplay_speed = $this->props['autoplay_speed'];
        $pause_on_hover = $this->props['pause_on_hover'];

        $effect = $this->props['effect'];
        $rotate = $this->props['rotate'];
        $options['data-columnsdesktop'] = esc_attr(1);

        $options['data-loop'] = esc_attr($loop);
        $options['data-speed'] = esc_attr($speed);
        $options['data-navigation'] = esc_attr('false');
        $options['data-pagination'] = esc_attr('false');
        $options['data-autoplay'] = esc_attr($autoplay);
        $options['data-autoplayspeed'] = esc_attr($autoplay_speed);
        $options['data-pauseonhover'] = esc_attr($pause_on_hover);
        $options['data-effect'] = esc_attr($effect);
        $options['data-rotate'] = esc_attr($rotate);

        $options = implode(
            " ",
            array_map(
                function ($k, $v) {
                    return "{$k}='{$v}'";
                },
                array_keys($options),
                $options
            )
        );

        $mockup_name = str_replace(' ', '-', strtolower($this->props['mockup']));
        $mockup_url = sprintf('%1$spublic/assets/mockups/%2$s.png', constant('DIPI_URI'), $mockup_name);
        
        $output = sprintf(
            '<div class="divi-pixel-mockup" data-mockup="%4$s">
                <div class="div-pixel-mockup-screen %4$s" %2$s>
                    <div class="dipi-blog-slider-wrapper swiper-wrapper">
                            %1$s
                    </div>
                </div>
                <img src="%3$s" class="div-pixel-mockup-img %4$s" alt="%4$s"/>
            </div>',
            $get_carousel_content,
            $options, 
            $mockup_url,
            $mockup_name
        );

        return $output;
    }
}
new DIPI_ImageShowcase;
