<?php

class DIPI_FloatingMultiImagesChild extends DIPI_Builder_Module
{

    public function init()
    {

        $this->name = esc_html__('Floating Image', 'dipi-divi-pixel');
        $this->plural = esc_html__('Floating Images', 'dipi-divi-pixel');
        $this->slug = 'dipi_floating_multi_images_child';
        $this->vb_support = 'on';
        $this->type = 'child';
        $this->child_title_var = 'item_label';
        $this->advanced_setting_title_text = esc_html__('New Image', 'dipi-divi-pixel');
        $this->settings_text = esc_html__('Image Settings', 'dipi-divi-pixel');
        $this->main_css_element = '%%order_class%%';

        $this->settings_modal_toggles = [
            'general' => [
                'toggles' => [
                    'main_content' => esc_html__('Image', 'dipi-divi-pixel'),
                    'position' => esc_html__('Image Position', 'dipi-divi-pixel'),
                    'animation' => esc_html__('Animation', 'dipi-divi-pixel'),
                ],
            ],
            'advanced' => [
                'toggles' => [
                    'borders' => esc_html__('Border', 'dipi-divi-pixel'),
                ],
            ],
        ];

    }

    public function get_fields()
    {

		$fields = [];
		
		$fields["item_label"] = [
			'label' => esc_html__('Admin Label', 'dipi-divi-pixel'),
			'description' => esc_html__('The label is used in the parent module to identify this item.', 'dipi-divi-pixel'),
            'type' => 'text',
            'toggle_slug' => 'main_content',
            'option_category' => 'basic_option',
        ];

        $fields['img_src'] = [
            'type' => 'upload',
            'hide_metadata' => true,
            'upload_button_text' => esc_attr__('Upload an image', 'dipi-divi-pixel'),
            'choose_text' => esc_attr__('Choose an Image', 'dipi-divi-pixel'),
            'update_text' => esc_attr__('Set As Image', 'dipi-divi-pixel'),
            'toggle_slug' => 'main_content',
            'option_category' => 'basic_option',
        ];

        $fields["img_alt"] = [
            'label' => esc_html__('Image Alt Text', 'dipi-divi-pixel'),
            'type' => 'text',
            'default' => '',
            'description' => esc_html__('Define the HTML ALT text for your image here.', 'dipi-divi-pixel'),
            'toggle_slug' => 'main_content',
            'option_category' => 'basic_option',
        ];

        $fields['use_img_link'] = [
            'label' => esc_html__('Add Image Link', 'dipi-divi-pixel'),
            'type' => 'yes_no_button',
            'options' => [
                'on' => esc_html__('Yes', 'dipi-divi-pixel'),
                'off' => esc_html__('No', 'dipi-divi-pixel')
            ],
            'toggle_slug' => 'main_content',
            'option_category' => 'configuration',
        ];
        
        $fields["img_link"] = [
            'label' => esc_html__('Image Link', 'dipi-divi-pixel'),
            'type' => 'text',
            'show_if' => [
                'use_img_link' => 'on'
            ],
            'toggle_slug' => 'main_content',
            'option_category' => 'configuration',
        ];
        
        $fields["img_link_target"] = [
            'label' => esc_html__('Image Link Target', 'dipi-divi-pixel'),
            'type' => 'select',
            'default' => 'off',
            'options' => array(
                'off' => esc_html__('Same Window', 'dipi-divi-pixel'),
                'on' => esc_html__('New Window', 'dipi-divi-pixel'),
            ),
            'show_if' => [
                'use_img_link' => 'on'
            ],
            'toggle_slug' => 'main_content',
            'option_category' => 'configuration',
        ];

        $fields['horizontal_position'] = [
            'label' => esc_attr__('Horizontal Position', 'dipi-divi-pixel'),
            'type' => 'range',
            'option_category' => 'layout',
            'mobile_options' => true,
            'validate_unit' => true,
            'default' => '0%',
            'default_unit' => '%',
            'range_settings' => [
                'min' => '-400',
                'max' => '400',
                'step' => '1',
            ],
            'responsive' => true,
            'toggle_slug' => 'position',
        ];

        $fields['vertical_position'] = [
            'label' => esc_attr__('Vertical Position', 'dipi-divi-pixel'),
            'type' => 'range',
            'option_category' => 'layout',
            'mobile_options' => true,
            'validate_unit' => true,
            'default' => '0%',
            'default_unit' => '%',
            'range_settings' => [
                'min' => '-400',
                'max' => '400',
                'step' => '1',
            ],
            'responsive' => true,
            'toggle_slug' => 'position',
        ];

        $fields['fmi_effect'] = [
            'label' => esc_html__('Effect', 'dipi-divi-pixel'),
            'type' => 'select',
            'option_category' => 'configuration',
            'default' => 'updown',
            'options' => [
                'updown' => esc_html__('Up Down', 'dipi-divi-pixel'),
                'leftright' => esc_html__('Left Right', 'dipi-divi-pixel'),
            ],
            'toggle_slug' => 'animation',
        ];

        $fields['fmi_delay'] = [
            'label' => esc_html__('Interval Delay', 'dipi-divi-pixel'),
            'type' => 'range',
            'option_category' => 'configuration',
            'default' => '0ms',
            'default_on_front' => '0ms',
            'default_unit' => 'ms',
            'range_settings' => [
                'min' => '-21000',
                'max' => '5000',
                'step' => '50',
            ],
            'toggle_slug' => 'animation',
        ];

        $fields['fmi_speed'] = [
            'label' => esc_html__('Speed', 'dipi-divi-pixel'),
            'type' => 'range',
            'option_category' => 'configuration',
            'default' => '5000ms',
            'default_on_front' => '5000ms',
            'default_unit' => 'ms',
            'range_settings' => [
                'min' => '0',
                'max' => '10000',
                'step' => '50',
            ],
            'toggle_slug' => 'animation',
        ];

        return $fields;

    }

    public function get_advanced_fields_config()
    {

        $advanced_fields = [];

        $advanced_fields['fonts'] = false;
        $advanced_fields['text'] = false;
        $advanced_fields['button'] = false;
        $advanced_fields['link_optons'] = false;

        $advanced_fields["borders"]["item"] = [
            'css' => [
                'main' => [
                    'border_radii' => "%%order_class%% img",
                    'border_styles' => "%%order_class%% img",
                ],
            ],
            'toggle_slug' => 'borders',
        ];

        $advanced_fields['box_shadow']["default"] = [
            'css' => [
                'main' => '%%order_class%% img',
            ],
        ];
        $advanced_fields['height'] = [
            'css' => [
                'main' => "$this->main_css_element img",
            ],
        ];
        $advanced_fields['transform'] = [
            'css' => [
                'main' => "$this->main_css_element img",
            ],
        ];
        return $advanced_fields;
    }

    public function render($attrs, $content = null, $render_slug)
    {

        $img_src = $this->props['img_src'];
        $img_alt = $this->props['img_alt'];

        $img_pathinfo = pathinfo($img_src);
        $is_img_svg = isset($img_pathinfo['extension']) ? 'svg' === $img_pathinfo['extension'] : false;
        if ($is_img_svg):
            ET_Builder_Element::set_style($render_slug, array(
                'selector' => '%%order_class%%',
                'declaration' => 'width: 100%;',
            ));
        endif;

        // Image horizontal positioning
        $horizontal_position = $this->props['horizontal_position'];
        $horizontal_position_tablet = $this->props['horizontal_position_tablet'];
        $horizontal_position_phone = $this->props['horizontal_position_phone'];
        $horizontal_position_last_edited = $this->props['horizontal_position_last_edited'];
        $horizontal_position_responsive_status = et_pb_get_responsive_status($horizontal_position_last_edited);

        if ('' !== $horizontal_position) {
            ET_Builder_Element::set_style($render_slug, array(
                'selector' => '%%order_class%%',
                'declaration' => sprintf('left: %1$s !important;', $horizontal_position),
            ));
        }

        if ('' !== $horizontal_position_tablet && $horizontal_position_responsive_status) {
            ET_Builder_Element::set_style($render_slug, array(
                'selector' => '%%order_class%%',
                'declaration' => sprintf('left: %1$s !important;', $horizontal_position_tablet),
                'media_query' => ET_Builder_Element::get_media_query('max_width_980'),
            ));
        }

        if ('' !== $horizontal_position_phone && $horizontal_position_responsive_status) {
            ET_Builder_Element::set_style($render_slug, array(
                'selector' => '%%order_class%%',
                'declaration' => sprintf('left: %1$s !important;', $horizontal_position_phone),
                'media_query' => ET_Builder_Element::get_media_query('max_width_767'),
            ));
        }

        // Image vertical positioning
        $vertical_position = $this->props['vertical_position'];
        $vertical_position_tablet = $this->props['vertical_position_tablet'];
        $vertical_position_phone = $this->props['vertical_position_phone'];
        $vertical_position_last_edited = $this->props['vertical_position_last_edited'];
        $vertical_position_responsive_status = et_pb_get_responsive_status($vertical_position_last_edited);

        if ('' !== $vertical_position) {
            ET_Builder_Element::set_style($render_slug, array(
                'selector' => '%%order_class%%',
                'declaration' => sprintf('top: %1$s !important;', $vertical_position),
            ));
        }

        if ('' !== $vertical_position_tablet && $vertical_position_responsive_status) {
            ET_Builder_Element::set_style($render_slug, array(
                'selector' => '%%order_class%%',
                'declaration' => sprintf('top: %1$s !important;', $vertical_position_tablet),
                'media_query' => ET_Builder_Element::get_media_query('max_width_980'),
            ));
        }

        if ('' !== $vertical_position_phone && $vertical_position_responsive_status) {
            ET_Builder_Element::set_style($render_slug, array(
                'selector' => '%%order_class%%',
                'declaration' => sprintf('top: %1$s !important;', $vertical_position_phone),
                'media_query' => ET_Builder_Element::get_media_query('max_width_767'),
            ));
        }

        ET_Builder_Element::set_style($render_slug, array(
            'selector' => '%%order_class%%',
            'declaration' => "position: absolute !important;",
        ));

        // Animation Name
        ET_Builder_Element::set_style($render_slug, array(
            'selector' => '%%order_class%%',
            'declaration' => sprintf(
                'animation-name: dipi-%1$s-effect !important;',
                esc_html($this->props['fmi_effect'])
            ),
        ));

        // Animation Duration
        ET_Builder_Element::set_style($render_slug, array(
            'selector' => '%%order_class%%',
            'declaration' => sprintf(
                'animation-duration: %1$s !important;',
                esc_html($this->props['fmi_speed'])
            ),
        ));

        // Animation Delay
        ET_Builder_Element::set_style($render_slug, array(
            'selector' => "%%order_class%%",
            'declaration' => sprintf(
                'animation-delay: %1$s !important;',
                esc_html($this->props['fmi_delay'])
            ),
        ));

        $use_img_link    = $this->props['use_img_link'];
        $img_link        = $this->props['img_link'];
        $img_link_target = $this->props['img_link_target']  === 'on' ? '_blank' : '_self';

        $start_link_wrap = 'on' == $use_img_link ? sprintf(
            '<a href="%1$s" target="%2$s">', 
            $img_link, 
            $img_link_target
        ) : '';
        
        $end_link_wrap = 'on' == $use_img_link ? '</a>' : '';

        return sprintf(
            '%3$s
            <img src="%1$s" alt="%2$s"/>
            %4$s',
            esc_attr($img_src),
            esc_attr($img_alt),
            $start_link_wrap,
            $end_link_wrap
        );

    }
}

new DIPI_FloatingMultiImagesChild;
