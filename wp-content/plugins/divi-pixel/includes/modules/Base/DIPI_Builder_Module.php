<?php

class DIPI_Builder_Module extends ET_Builder_Module
{
    protected function dipi_get_responsive_prop($property, $default = '', $default_if_empty = true)
    {
        $responsive_prop = [];
        $responsive_enabled = isset($this->props["{$property}_last_edited"]) ? et_pb_get_responsive_status($this->props["{$property}_last_edited"]) : false;
        if (!isset($this->props[$property]) || ($default_if_empty && '' === $this->props[$property])) {
            $responsive_prop["desktop"] = $default;
        } else {
            $responsive_prop["desktop"] = $this->props[$property];
        }

        if (!$responsive_enabled || !isset($this->props["{$property}_tablet"]) || '' === $this->props["{$property}_tablet"]) {
            $responsive_prop["tablet"] = $responsive_prop["desktop"];
        } else {
            $responsive_prop["tablet"] = $this->props["{$property}_tablet"];
        }

        if (!$responsive_enabled || !isset($this->props["{$property}_phone"]) || '' === $this->props["{$property}_phone"]) {
            $responsive_prop["phone"] = $responsive_prop["tablet"];
        } else {
            $responsive_prop["phone"] = $this->props["{$property}_phone"];
        }

        return $responsive_prop;
    }

    protected function sanitize_content($content)
    {
        return preg_replace('/^<\/p>(.*)<p>/s', '$1', $content);
    }

    protected function startsWith($string, $startString)
    {
        // if(!$string || strlen($string) < strlen($startString)){
        //     return false;
        // }

        $len = strlen($startString);
        return (substr($string, 0, $len) === $startString);
    }

    protected static function render_library_layout($layoutId){
    
        $module_slugs = ET_Builder_Element::get_module_slugs_by_post_type();
        $uuid = uniqid();
        // TODO: This array could be cached as it never changes (unlike the replacements which need the uuid)
        $map_to_regex = function($value) { return '/' . $value . '_(\d+)(,|\.|:| |")/'; };
        $regex = array_map($map_to_regex, $module_slugs);
    
        $map_to_replacements = function($value) use ($uuid) { return 'dipi_' . $uuid . '_' . $value . '_${1}${2}'; };
        $replacements = array_map($map_to_replacements, $module_slugs);
        
        $divi_library_shortcode = do_shortcode('[et_pb_section global_module="' . $layoutId . '"][/et_pb_section]');
        $divi_library_shortcode .= '<style type="text/css">' . ET_Builder_Element::get_style() . '</style>';
        ET_Builder_Element::clean_internal_modules_styles(false);

        return preg_replace( $regex, $replacements, $divi_library_shortcode );
    }
}