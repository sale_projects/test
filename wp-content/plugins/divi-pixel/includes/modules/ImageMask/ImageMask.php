<?php
require_once plugin_dir_path(__FILE__) . 'Decorations.php';
require_once plugin_dir_path(__FILE__) . 'Shapes.php';

class DIPI_ImageMask extends DIPI_Builder_Module {

  public $slug       = 'dipi_image_mask';
  public $vb_support = 'on';
  private $decorations = null;
  private $shapes = null;
  private static $layerID = 0;
  private function getLayerId($prefix = 'SVG_'){
      self::$layerID++;
      return $prefix . self::$layerID;
  }
  protected $module_credits = array(
      'module_uri' => 'https://divi-pixel.com/modules/image-mask',
      'author' => 'Divi Pixel',
      'author_uri' => 'https://divi-pixel.com',
  );

  
  public function init() {
    $this->decorations = new DIPI_SVG_Decorations();
    $this->shapes = new DIPI_SVG_Shapes();
    $this->icon_path = plugin_dir_path(__FILE__) . "dp-image-mask.svg";
    $this->name = esc_html__( 'Pixel Image Mask', 'dipi-divi-pixel' );
  }

  public function get_settings_modal_toggles() {
		$toggles = [
			'general' => []
		];
		$toggles['general']['toggles'] = [
      'main_content' => esc_html__( 'Mask Settings', 'dipi-divi-pixel' ),
			'layer_1'		=> ['title' => esc_html__( 'Border Layer', 'dipi-divi-pixel' )],
			'layer_2'		=> ['title' => esc_html__( 'Decoration Element 1', 'dipi-divi-pixel' )],
			'layer_3'		=> ['title' => esc_html__( 'Decoration Element 2', 'dipi-divi-pixel' )]
		];
		return $toggles;
	}

	public function get_fields() {
    $fields = [];

    $fields['image'] = [
      'label'           => esc_html__( 'Image', 'dipi-divi-pixel' ),
      'type'            => 'upload',
      'upload_button_text' => esc_attr__('Upload Image', 'dipi-divi-pixel'),
      'choose_text' => esc_attr__('Chose Image', 'dipi-divi-pixel'),
      'update_text' => esc_attr__('Update Image', 'dipi-divi-pixel'),
      'hide_metadata' => true,
      'option_category' => 'basic_option',
      'toggle_slug'     => 'main_content',
      'dynamic_content' => 'image'
    ];

    $fields['shape'] = [
      'label'           => esc_html__( 'Mask Shape', 'dipi-divi-pixel' ),
      'type'            => 'select',
       'options' => array(
          'Shape1' => esc_html__( 'Shape 1', 'dipi-divi-pixel' ),
          'Shape2' => esc_html__( 'Shape 2', 'dipi-divi-pixel' ),
          'Shape3' => esc_html__( 'Shape 3', 'dipi-divi-pixel' ),
          'Shape4' => esc_html__( 'Shape 4', 'dipi-divi-pixel' ),
          'Shape5' => esc_html__( 'Shape 5', 'dipi-divi-pixel' ),
          'Shape6' => esc_html__( 'Shape 6', 'dipi-divi-pixel' ),
          'Shape7' => esc_html__( 'Shape 7', 'dipi-divi-pixel' ),
          'Shape8' => esc_html__( 'Shape 8', 'dipi-divi-pixel' ),
          'Shape9' => esc_html__( 'Shape 9', 'dipi-divi-pixel' ),
          'Shape10' => esc_html__( 'Shape 10', 'dipi-divi-pixel' ),
          'Shape11' => esc_html__( 'Shape 11', 'dipi-divi-pixel' ),
          'Shape12' => esc_html__( 'Shape 12', 'dipi-divi-pixel' )
        ),
      'default'	=>  'Shape1',
      'option_category' => 'basic_option',
      'toggle_slug'     => 'main_content'
    ];

    $fields['image_width'] = [
      'label'           => esc_html__( 'Image Width', 'dipi-divi-pixel' ),
      'type'            => 'range',
      'option_category' => 'basic_option',
      'toggle_slug'     => 'main_content',
      'default'         => '100%',
      'fixed_unit'      => '%',
      'range_settings'  => [
        'min' => '0',
        'max' => '200'
      ]
    ];

    $fields['image_horz'] = [
      'label'           => esc_html__( 'Image Horizontal Position', 'dipi-divi-pixel' ),
      'type'            => 'range',
      'option_category' => 'basic_option',
      'toggle_slug'     => 'main_content',
      'default'         => '0',
      'unitless'        => true,
      'range_settings'  => [
        'min' => -200,
        'max' => 200
      ]
    ];

    $fields['image_vert'] = [
      'label'           => esc_html__( 'Image Vertical Position', 'dipi-divi-pixel' ),
      'type'            => 'range',
      'option_category' => 'basic_option',
      'toggle_slug'     => 'main_content',
      'default'         => '0',
      'unitless'        => true,
      'range_settings'  => [
        'min' => -200,
        'max' => 200
      ]
    ];

    $fields['layer_1_enable'] = [
      'label'         => esc_html__( 'Enable Border Layer', 'dipi-divi-pixel' ),
      'type'          => 'yes_no_button',
      'options'		=> [
        'off' 	=> esc_html__( 'No', 'et_builder' ),
        'on' 	=> esc_html__( 'Yes', 'et_builder' )
      ],
      'default'	=>  'off',
      'option_category' => 'basic_option',
      'toggle_slug'     => 'layer_1'
    ];

    $fields['layer_1_background_type'] = [
      'label'           => esc_html__( 'Background Style', 'dipi-divi-pixel' ),
      'type'            => 'select',
       'options' => array(
          esc_html__( 'Solid Color', 'dipi-divi-pixel' ),
          esc_html__( 'Gradient', 'dipi-divi-pixel' )
        ),
      'default'	=>  'Solid Color',
      'option_category' => 'basic_option',
      'toggle_slug'     => 'layer_1',
      'data_type' => '',
      'show_if' => array(
        'layer_1_enable' => 'on'
      )
    ];

    $fields['layer_1_background_color'] = [
      'label'           => esc_html__( 'Background Color', 'dipi-divi-pixel' ),
      'type'            => 'color-alpha',
      'default'	=>  '#000',
      'option_category' => 'basic_option',
      'toggle_slug'     => 'layer_1',
      'show_if' => array(
        'layer_1_enable' => 'on',
        'layer_1_background_type' => 'Solid Color'
      )
    ];

    $fields['layer_1_gradient_color_start'] = [
      'label'           => esc_html__( 'Gradient Color Start', 'dipi-divi-pixel' ),
      'type'            => 'color-alpha',
      'default'	=>  '#fff',
      'option_category' => 'basic_option',
      'toggle_slug'     => 'layer_1',
      'show_if' => array(
        'layer_1_enable' => 'on',
        'layer_1_background_type' => 'Gradient'
      )
    ];

    $fields['layer_1_gradient_color_end'] = [
      'label'           => esc_html__( 'Gradient Color End', 'dipi-divi-pixel' ),
      'type'            => 'color-alpha',
      'default'	=>  '#fff',
      'option_category' => 'basic_option',
      'toggle_slug'     => 'layer_1',
      'show_if' => array(
        'layer_1_enable' => 'on',
        'layer_1_background_type' => 'Gradient'
      )
    ];

    

    $fields['layer_2_enable'] = [
      'label'         => esc_html__( 'Enable Decoration Element', 'dipi-divi-pixel' ),
      'type'          => 'yes_no_button',
      'options'		=> [
        'off' 	=> esc_html__( 'No', 'et_builder' ),
        'on' 	=> esc_html__( 'Yes', 'et_builder' )
      ],
      'default'	=>  'off',
      'option_category' => 'basic_option',
      'toggle_slug'     => 'layer_2'
    ];

    $fields['docration_element_1'] = [
      'label'           => esc_html__( 'Decoraction Element', 'dipi-divi-pixel' ),
      'type'            => 'select',
      'options' => array(
        'DottedSquare'  => esc_html__( 'Dotted Square', 'dipi-divi-pixel' ),
        'DottedCircle'  => esc_html__( 'Dotted Circle', 'dipi-divi-pixel' ),
        'DottedTraingle'  => esc_html__( 'Dotted Traingle', 'dipi-divi-pixel' ),
        'DottedShape'  => esc_html__( 'Dotted Shape', 'dipi-divi-pixel' ),
        'StrokeTriangle' => esc_html__( 'Stroke Triangle', 'dipi-divi-pixel' ),
        'StrokeCircle'  => esc_html__( 'Stroke Circle', 'dipi-divi-pixel' ),
        'StrokeSquare'  => esc_html__( 'Stroke Square', 'dipi-divi-pixel' ),
        'AbstractSquare'  => esc_html__( 'Abstract Square', 'dipi-divi-pixel' ),
        'AbstractCircle'  => esc_html__( 'Abstract Circle', 'dipi-divi-pixel' ),
        'FilledCircle'  => esc_html__( 'Filled Circle', 'dipi-divi-pixel' ),
        'FilledSquare'  => esc_html__( 'Filled Square', 'dipi-divi-pixel' ),
        'FilledTriangle'  => esc_html__( 'Filled Triangle', 'dipi-divi-pixel' ),
        'LinedSquare'  => esc_html__( 'Lined Square', 'dipi-divi-pixel' ),
        'LinedCircle'  => esc_html__( 'Lined Circle', 'dipi-divi-pixel' ),
        'LinedTriangle'      => esc_html__( 'Lined Triangle', 'dipi-divi-pixel' )
      ),
      'default'	=>  'LinedCircle',
      'option_category' => 'basic_option',
      'toggle_slug'     => 'layer_2',
      'show_if' => array(
        'layer_2_enable' => 'on'
      )
    ];

    $fields['layer_2_above_image'] = [
      'label'         => esc_html__( 'Show Above Image', 'dipi-divi-pixel' ),
      'type'          => 'yes_no_button',
      'options'		=> [
        'off' 	=> esc_html__( 'No', 'et_builder' ),
        'on' 	=> esc_html__( 'Yes', 'et_builder' )
      ],
      'default'	=>  'on',
      'option_category' => 'basic_option',
      'toggle_slug'     => 'layer_2',
      'show_if' => array(
        'layer_2_enable' => 'on'
      )
    ];

    $fields['layer_2_background_color'] = [
      'label'           => esc_html__( 'Background Color', 'dipi-divi-pixel' ),
      'type'            => 'color-alpha',
      'default'	=>  '#000',
      'option_category' => 'basic_option',
      'toggle_slug'     => 'layer_2',
      'show_if' => array(
        'layer_2_enable' => 'on'
      )
    ];

    $fields['layer_2_horz'] = [
      'label'           => esc_html__( 'Horizontal Position', 'dipi-divi-pixel' ),
      'type'            => 'range',
      'option_category' => 'basic_option',
      'toggle_slug'     => 'layer_2',
      'default'         => '25%',
      'fixed_unit'      => '%',
      'range_settings'  => [
        'min' => -100,
        'max' => 100
      ],
      'show_if' => array(
        'layer_2_enable' => 'on'
      )
    ];

    $fields['layer_2_vert'] = [
      'label'           => esc_html__( 'Vertical Position', 'dipi-divi-pixel' ),
      'type'            => 'range',
      'option_category' => 'basic_option',
      'toggle_slug'     => 'layer_2',
      'default'         => '-25%',
      'fixed_unit'      => '%',
      'range_settings'  => [
        'min' => -100,
        'max' => 100
      ],
      'show_if' => array(
        'layer_2_enable' => 'on'
      )
    ];

    $fields['layer_2_scale'] = [
      'label'           => esc_html__( 'Scale', 'dipi-divi-pixel' ),
      'type'            => 'range',
      'option_category' => 'basic_option',
      'toggle_slug'     => 'layer_2',
      'default'         => '1',
      'unitless'        => true,
      'range_settings'  => [
        'step' => '0.1',
        'min' => '-3',
        'max' => '3'
      ],
      'show_if' => array(
        'layer_2_enable' => 'on'
      )
    ];

    $fields['layer_2_rotate'] = [
      'label'           => esc_html__( 'Rotate', 'dipi-divi-pixel' ),
      'type'            => 'range',
      'option_category' => 'basic_option',
      'toggle_slug'     => 'layer_2',
      'default'         => '0',
      'unitless'        => true,
      'range_settings'  => [
        'step' => '1',
        'min' => '0',
        'max' => '360'
      ],
      'show_if' => array(
        'layer_2_enable' => 'on'
      )
    ];

    $fields['layer_3_enable'] = [
      'label'         => esc_html__( 'Enable Decoration Element', 'dipi-divi-pixel' ),
      'type'          => 'yes_no_button',
      'options'		=> [
        'off' 	=> esc_html__( 'No', 'et_builder' ),
        'on' 	=> esc_html__( 'Yes', 'et_builder' )
      ],
      'default'	=>  'off',
      'option_category' => 'basic_option',
      'toggle_slug'     => 'layer_3'
    ];

    $fields['docration_element_2'] = [
      'label'           => esc_html__( 'Decoraction Element', 'dipi-divi-pixel' ),
      'type'            => 'select',
      'options' => array(
        'StrokeTriangle' => esc_html__( 'Stroke Triangle', 'dipi-divi-pixel' ),
        'StrokeCircle'  => esc_html__( 'Stroke Circle', 'dipi-divi-pixel' ),
        'StrokeSquare'  => esc_html__( 'Stroke Square', 'dipi-divi-pixel' ),
        'DottedSquare'  => esc_html__( 'Dotted Square', 'dipi-divi-pixel' ),
        'DottedCircle'  => esc_html__( 'Dotted Circle', 'dipi-divi-pixel' ),
        'DottedTraingle'  => esc_html__( 'Dotted Traingle', 'dipi-divi-pixel' ),
        'DottedShape'  => esc_html__( 'Dotted Shape', 'dipi-divi-pixel' ),
        'AbstractSquare'  => esc_html__( 'Abstract Square', 'dipi-divi-pixel' ),
        'AbstractCircle'  => esc_html__( 'Abstract Circle', 'dipi-divi-pixel' ),
        'FilledCircle'  => esc_html__( 'Filled Circle', 'dipi-divi-pixel' ),
        'FilledSquare'  => esc_html__( 'Filled Square', 'dipi-divi-pixel' ),
        'FilledTriangle'  => esc_html__( 'Filled Triangle', 'dipi-divi-pixel' ),
        'LinedSquare'  => esc_html__( 'Lined Square', 'dipi-divi-pixel' ),
        'LinedCircle'  => esc_html__( 'Lined Circle', 'dipi-divi-pixel' ),
        'LinedTriangle'      => esc_html__( 'Lined Triangle', 'dipi-divi-pixel' )
      ),
      'default'	=>  'LinedCircle',
      'option_category' => 'basic_option',
      'toggle_slug'     => 'layer_3',
      'show_if' => array(
        'layer_3_enable' => 'on'
      )
    ];

    $fields['layer_3_above_image'] = [
      'label'         => esc_html__( 'Show Above Image', 'dipi-divi-pixel' ),
      'type'          => 'yes_no_button',
      'options'		=> [
        'off' 	=> esc_html__( 'No', 'et_builder' ),
        'on' 	=> esc_html__( 'Yes', 'et_builder' )
      ],
      'default'	=>  'on',
      'option_category' => 'basic_option',
      'toggle_slug'     => 'layer_3',
      'show_if' => array(
        'layer_3_enable' => 'on'
      )
    ];

    $fields['layer_3_background_color'] = [
      'label'           => esc_html__( 'Background Color', 'dipi-divi-pixel' ),
      'type'            => 'color-alpha',
      'default'	=>  '#000',
      'option_category' => 'basic_option',
      'toggle_slug'     => 'layer_3',
      'show_if' => array(
        'layer_3_enable' => 'on'
      )
    ];

    $fields['layer_3_horz'] = [
      'label'           => esc_html__( 'Horizontal Position', 'dipi-divi-pixel' ),
      'type'            => 'range',
      'option_category' => 'basic_option',
      'toggle_slug'     => 'layer_3',
      'default'         => '-15%',
      'fixed_unit'      => '%',
      'range_settings'  => [
        'min' => -100,
        'max' => 100
      ],
      'show_if' => array(
        'layer_3_enable' => 'on'
      )
    ];

    $fields['layer_3_vert'] = [
      'label'           => esc_html__( 'Vertical Position', 'dipi-divi-pixel' ),
      'type'            => 'range',
      'option_category' => 'basic_option',
      'toggle_slug'     => 'layer_3',
      'default'         => '30%',
      'fixed_unit'      => '%',
      'range_settings'  => [
        'min' => -100,
        'max' => 100
      ],
      'show_if' => array(
        'layer_3_enable' => 'on'
      )
    ];

    $fields['layer_3_scale'] = [
      'label'           => esc_html__( 'Scale', 'dipi-divi-pixel' ),
      'type'            => 'range',
      'option_category' => 'basic_option',
      'toggle_slug'     => 'layer_3',
      'default'         => '1',
      'unitless'        => true,
      'range_settings'  => [
        'step' => '0.1',
        'min' => '-3',
        'max' => '3'
      ],
      'show_if' => array(
        'layer_3_enable' => 'on'
      )
    ];

    $fields['layer_3_rotate'] = [
      'label'           => esc_html__( 'Rotate', 'dipi-divi-pixel' ),
      'type'            => 'range',
      'option_category' => 'basic_option',
      'toggle_slug'     => 'layer_3',
      'default'         => '0',
      'unitless'        => true,
      'range_settings'  => [
        'step' => '1',
        'min' => '0',
        'max' => '360'
      ],
      'show_if' => array(
        'layer_3_enable' => 'on'
      )
    ];


    return $fields;
  }

  public function get_advanced_fields_config() {
    $advanced_fields = [
            'text' => false,
            'text_shadow' => false,
            'fonts' => false,
    ];

    $advanced_fields['background'] = [
			     'css' => [
				        'main' => '%%order_class%% .fake-class'
			     ],
			     'hover' => false,
			     'hover_enabled' => 'off',
			     'background__hover_enabled' => 'off',
			     'use_background_video' => false
		];

    return $advanced_fields;
  }

  public function render( $attrs, $content = null, $render_slug ) {
 
    if($this->props["layer_1_enable"] === 'on' && $this->props["layer_1_background_type"] === 'Solid Color' ){
      ET_Builder_Element::set_style($render_slug, [
        'selector' => '%%order_class%% .st1',
        'declaration' => "fill:" . $this->props["layer_1_background_color"]
      ]);
    }

    if($this->props["layer_2_enable"] === 'on') {
      ET_Builder_Element::set_style($render_slug, [
        'selector' => '%%order_class%% .s02',
        'declaration' => "fill:" . $this->props["layer_2_background_color"]
      ]);
    }
    
    if($this->props["layer_3_enable"] === 'on') {
      ET_Builder_Element::set_style($render_slug, [
        'selector' => '%%order_class%% .s03',
        'declaration' => "fill:" . $this->props["layer_3_background_color"]
      ]);
    }
 
    $layerZeroGradient ='';
    $layerOneGradient = '';

    $gradId2 = $this->getLayerId('GRAD_');

    if( $this->props["layer_1_enable"] === 'on' && $this->props["layer_1_background_type"] === 'Gradient'){
      $layerOneGradient = sprintf('
        <linearGradient id="%3$s" x1="0%%" y1="0%%" x2="100%%" y2="0%%">
          <stop offset="0%%" style="stop-color: %1$s;stop-opacity: 1" />
          <stop offset="100%%" style="stop-color: %2$s;stop-opacity: 1" />
        </linearGradient>',
          $this->props["layer_1_gradient_color_start"],
          $this->props["layer_1_gradient_color_end"],
          $gradId2
        );
          
    }

    $style = '';
    // if($this->props["layer_1_enable"] === 'on')
    //   $style .= '.st1 {fill: '.$this->props["layer_1_background_color"].'}'; 

    // if($this->props["layer_2_enable"] === 'on')
    //   $style .= '.s02 {fill: '.$this->props["layer_2_background_color"].'}'; 
    
    // if($this->props["layer_3_enable"] === 'on')
    //   $style .= '.s03 {fill: '.$this->props["layer_3_background_color"].'}'; 
    
    $deco_1 = '';
    $deco_2 = '';
    $bottom_layers = '';
    $top_layers = '';

    if($this->props["layer_2_enable"] === 'on')
      $deco_1 = $this->decorations->decoration($this->props["docration_element_1"], "s02", $this->props["layer_2_horz"], $this->props["layer_2_vert"] , $this->props["layer_2_scale"], $this->props["layer_2_rotate"]);
    
      
    if($this->props["layer_3_enable"] === 'on')
      $deco_2 = $this->decorations->decoration($this->props["docration_element_2"], "s03", $this->props["layer_3_horz"], $this->props["layer_3_vert"] , $this->props["layer_3_scale"], $this->props["layer_3_rotate"]);
    
    if($this->props["layer_2_above_image"] === 'on')
      $top_layers .= $deco_1;
    else
      $bottom_layers .= $deco_1;
    
    if($this->props["layer_3_above_image"] === 'on')
      $top_layers .= $deco_2;
    else
      $bottom_layers .= $deco_2;
    
    $main_layer = $this->shapes->shape( 
        $this->props['shape'],
        $this->props['image'],
        $this->props['image_width'],
        $this->props['image_horz'],
        $this->props['image_vert'],
        $this->props['layer_1_enable'],
        $gradId2,
        uniqid()
      );

    return sprintf('
      <div class="dipi-image-mask--mask">
        <svg
          width="100%%"
          height="100%%"
          xmlns="http://www.w3.org/2000/svg"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          viewBox="0 0 1000 1000">
            <defs>
              %1$s
            </defs>
            <style>
              %2$s
            </style>
            %3$s
            %4$s
            %5$s
        </svg>
      </div>',
      $layerOneGradient,
      $style,
      $bottom_layers,
      $main_layer,
      $top_layers
    );
  }


}
new DIPI_ImageMask;
