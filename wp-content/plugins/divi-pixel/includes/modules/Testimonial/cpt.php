<?php

add_action('init', 'dipi_testimonial_post_type');

function dipi_testimonial_post_type() {
    
    $labels = [
        'name'                  => _x( 'Testimonials', 'Post Type General Name', 'dipi-divi-pixel' ),
        'singular_name'         => _x( 'Testimonial', 'Post Type Singular Name', 'dipi-divi-pixel' ),
        'menu_name'             => __( 'Testimonials', 'dipi-divi-pixel' ),
        'name_admin_bar'        => __( 'Testimonial', 'dipi-divi-pixel' ),
        'archives'              => __( 'Item Archives', 'dipi-divi-pixel' ),
        'attributes'            => __( 'Item Attributes', 'dipi-divi-pixel' ),
        'parent_item_colon'     => __( 'Parent Item:', 'dipi-divi-pixel' ),
        'all_items'             => __( 'All Items', 'dipi-divi-pixel' ),
        'add_new_item'          => __( 'Add New Item', 'dipi-divi-pixel' ),
        'add_new'               => __( 'Add New', 'dipi-divi-pixel' ),
        'new_item'              => __( 'New Item', 'dipi-divi-pixel' ),
        'edit_item'             => __( 'Edit Item', 'dipi-divi-pixel' ),
        'update_item'           => __( 'Update Item', 'dipi-divi-pixel' ),
        'view_item'             => __( 'View Item', 'dipi-divi-pixel' ),
        'view_items'            => __( 'View Items', 'dipi-divi-pixel' ),
        'search_items'          => __( 'Search Item', 'dipi-divi-pixel' ),
        'not_found'             => __( 'Not found', 'dipi-divi-pixel' ),
        'not_found_in_trash'    => __( 'Not found in Trash', 'dipi-divi-pixel' ),
        'featured_image'        => __( 'Featured Image', 'dipi-divi-pixel' ),
        'set_featured_image'    => __( 'Set featured image', 'dipi-divi-pixel' ),
        'remove_featured_image' => __( 'Remove featured image', 'dipi-divi-pixel' ),
        'use_featured_image'    => __( 'Use as featured image', 'dipi-divi-pixel' ),
        'insert_into_item'      => __( 'Insert into item', 'dipi-divi-pixel' ),
        'uploaded_to_this_item' => __( 'Uploaded to this item', 'dipi-divi-pixel' ),
        'items_list'            => __( 'Items list', 'dipi-divi-pixel' ),
        'items_list_navigation' => __( 'Items list navigation', 'dipi-divi-pixel' ),
        'filter_items_list'     => __( 'Filter items list', 'dipi-divi-pixel' ),
    ];

    $args = [
        'label'                 => __( 'Testimonial', 'dipi-divi-pixel' ),
        'description'           => __( 'Testimonial Description', 'dipi-divi-pixel' ),
        'labels'                => $labels,
        'supports'              => array( 'title', 'editor' ),
        'taxonomies'            => array( 'testimonial_cat', 'testimonial_tag' ),
        'hierarchical'          => false,
        'public'                => true,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'menu_position'         => 5,
        'menu_icon'             => 'dashicons-format-quote',
        'show_in_admin_bar'     => true,
        'show_in_nav_menus'     => true,
        'can_export'            => true,
        'has_archive'           => false,
        'exclude_from_search'   => true,
        'publicly_queryable'    => true,
        'capability_type'       => 'page',
        'map_meta_cap'          => true
    ];

    register_post_type('dipi_testimonial', $args );
}

// Register Custom Taxonomy
function testimonial_category() {

    $labels = array(
        'name'                       => _x( 'Categories', 'Taxonomy General Name', 'dipi-divi-pixel' ),
        'singular_name'              => _x( 'Category', 'Taxonomy Singular Name', 'dipi-divi-pixel' ),
        'menu_name'                  => __( 'Categories', 'dipi-divi-pixel' ),
        'all_items'                  => __( 'All Items', 'dipi-divi-pixel' ),
        'parent_item'                => __( 'Parent Item', 'dipi-divi-pixel' ),
        'parent_item_colon'          => __( 'Parent Item:', 'dipi-divi-pixel' ),
        'new_item_name'              => __( 'New Item Name', 'dipi-divi-pixel' ),
        'add_new_item'               => __( 'Add New Item', 'dipi-divi-pixel' ),
        'edit_item'                  => __( 'Edit Item', 'dipi-divi-pixel' ),
        'update_item'                => __( 'Update Item', 'dipi-divi-pixel' ),
        'view_item'                  => __( 'View Item', 'dipi-divi-pixel' ),
        'separate_items_with_commas' => __( 'Separate items with commas', 'dipi-divi-pixel' ),
        'add_or_remove_items'        => __( 'Add or remove items', 'dipi-divi-pixel' ),
        'choose_from_most_used'      => __( 'Choose from the most used', 'dipi-divi-pixel' ),
        'popular_items'              => __( 'Popular Items', 'dipi-divi-pixel' ),
        'search_items'               => __( 'Search Items', 'dipi-divi-pixel' ),
        'not_found'                  => __( 'Not Found', 'dipi-divi-pixel' ),
        'no_terms'                   => __( 'No items', 'dipi-divi-pixel' ),
        'items_list'                 => __( 'Items list', 'dipi-divi-pixel' ),
        'items_list_navigation'      => __( 'Items list navigation', 'dipi-divi-pixel' ),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
    );
    register_taxonomy( 'testimonial_cat', ['dipi_testimonial'], $args );
}

add_action( 'init', 'testimonial_category', 0 );

function testimonial_tags() {

    $labels = array(
        'name'                       => _x( 'Tags', 'Taxonomy General Name', 'dipi-divi-pixel' ),
        'singular_name'              => _x( 'Tag', 'Taxonomy Singular Name', 'dipi-divi-pixel' ),
        'menu_name'                  => __( 'Tags', 'dipi-divi-pixel' ),
        'all_items'                  => __( 'All Items', 'dipi-divi-pixel' ),
        'parent_item'                => __( 'Parent Item', 'dipi-divi-pixel' ),
        'parent_item_colon'          => __( 'Parent Item:', 'dipi-divi-pixel' ),
        'new_item_name'              => __( 'New Item Name', 'dipi-divi-pixel' ),
        'add_new_item'               => __( 'Add New Item', 'dipi-divi-pixel' ),
        'edit_item'                  => __( 'Edit Item', 'dipi-divi-pixel' ),
        'update_item'                => __( 'Update Item', 'dipi-divi-pixel' ),
        'view_item'                  => __( 'View Item', 'dipi-divi-pixel' ),
        'separate_items_with_commas' => __( 'Separate items with commas', 'dipi-divi-pixel' ),
        'add_or_remove_items'        => __( 'Add or remove items', 'dipi-divi-pixel' ),
        'choose_from_most_used'      => __( 'Choose from the most used', 'dipi-divi-pixel' ),
        'popular_items'              => __( 'Popular Items', 'dipi-divi-pixel' ),
        'search_items'               => __( 'Search Items', 'dipi-divi-pixel' ),
        'not_found'                  => __( 'Not Found', 'dipi-divi-pixel' ),
        'no_terms'                   => __( 'No items', 'dipi-divi-pixel' ),
        'items_list'                 => __( 'Items list', 'dipi-divi-pixel' ),
        'items_list_navigation'      => __( 'Items list navigation', 'dipi-divi-pixel' ),
    );

    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => false,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
    );

    register_taxonomy( 'testimonial_tag', ['dipi_testimonial'], $args );

}
add_action( 'init', 'testimonial_tags', 0 );
