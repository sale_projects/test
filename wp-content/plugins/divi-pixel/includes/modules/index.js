import DIPI_MasonryGallery from './MasonryGallery/MasonryGallery';
import DIPI_FlipBox from './FlipBox/FlipBox';
import DIPI_TiltImage from './TiltImage/TiltImage';
import DIPI_FloatingMultiImages from './FloatingMultiImages/FloatingMultiImages';
import DIPI_FloatingMultiImagesChild from './FloatingMultiImagesChild/FloatingMultiImagesChild';
import DIPI_Counter from './Counter/Counter';
import DIPI_TypingText from './TypingText/TypingText';
import DIPI_BeforeAfterSlider from './BeforeAfterSlider/BeforeAfterSlider';
import DIPI_Carousel from './Carousel/Carousel';
import DIPI_CarouselChild from './CarouselChild/CarouselChild';
import DIPI_Breadcrumbs from './Breadcrumbs/Breadcrumbs';
import DIPI_StarRating from './StarRating/StarRating';
import DIPI_ButtonGrid from './ButtonGrid/ButtonGrid';
import DIPI_ButtonGridChild from './ButtonGridChild/ButtonGridChild';
import DIPI_Testimonial from './Testimonial/Testimonial';
import DIPI_BlogSlider from './BlogSlider/BlogSlider';
import DIPI_ImageHotspot from './ImageHotspot/ImageHotspot';
import DIPI_ImageHotspotChild from './ImageHotspotChild/ImageHotspotChild';
import DIPI_PriceList from './PriceList/PriceList';
import DIPI_PriceListItem from './PriceListItem/PriceListItem';
import DIPI_Countdown from './Countdown/Countdown';
import DIPI_FancyText from './FancyText/FancyText';
import DIPI_FancyTextChild from './FancyTextChild/FancyTextChild';
import DIPI_HoverBox from './HoverBox/HoverBox';
import DIPI_Balloon from './Balloon/Balloon';
import DIPI_ImageAccordion from './ImageAccordion/ImageAccordion';
import DIPI_ImageAccordionChild from './ImageAccordionChild/ImageAccordionChild';
import DIPI_ImageMagnifier from './ImageMagnifier/ImageMagnifier';
import DIPI_Panorama from './Panorama/Panorama';
import DIPI_ScrollImage from './ScrollImage/ScrollImage';
import DIPI_LottieIcon from './LottieIcon/LottieIcon';
import DIPI_ReadingProgressBar from './ReadingProgressBar/ReadingProgressBar';
import DIPI_ImageMask from './ImageMask/ImageMask';
import DIPI_ImageShowcase from './ImageShowcase/ImageShowcase';
import DIPI_ImageShowcaseChild from './ImageShowcaseChild/ImageShowcaseChild';
import DIPI_TimeLine from './TimeLine/TimeLine';
import DIPI_TimeLineItem from './TimeLineItem/TimeLineItem';
import DIPI_ContentToggle from './ContentToggle/ContentToggle';

export default [
    DIPI_MasonryGallery,
    DIPI_FlipBox,
    DIPI_TiltImage,
    DIPI_FloatingMultiImages,
    DIPI_FloatingMultiImagesChild,
    DIPI_Counter,
    DIPI_BeforeAfterSlider,
    DIPI_Carousel,
    DIPI_CarouselChild,
    DIPI_TypingText,
    DIPI_Breadcrumbs,
    DIPI_StarRating,
    DIPI_ButtonGrid,
    DIPI_ButtonGridChild,
    DIPI_Testimonial,
    DIPI_Countdown,
    DIPI_ImageHotspot,
    DIPI_ImageHotspotChild,
    DIPI_PriceList,
    DIPI_PriceListItem,
    DIPI_FancyText,
    DIPI_FancyTextChild,
    DIPI_HoverBox,
    DIPI_Balloon,
    DIPI_BlogSlider,
    DIPI_ImageAccordion,
    DIPI_ImageAccordionChild,
    DIPI_ImageMagnifier,
    DIPI_ScrollImage,
    DIPI_LottieIcon,
    DIPI_ReadingProgressBar,
    DIPI_Panorama,
    DIPI_ImageMask,
    DIPI_ImageShowcase,
    DIPI_ImageShowcaseChild,
    DIPI_TimeLine,
    DIPI_TimeLineItem,
    DIPI_ContentToggle
];