<?php

class DIPI_BeforeAfterSlider extends DIPI_Builder_Module
{

    public $slug = 'dipi_before_after_slider';
    public $vb_support = 'on';

    protected $module_credits = array(
        'module_uri' => 'https://divi-pixel.com/modules/before-after',
        'author' => 'Divi Pixel',
        'author_uri' => 'https://divi-pixel.com',
    );

    public function init()
    {
        $this->icon_path = plugin_dir_path(__FILE__) . "dp-before-after.svg";
        $this->name = esc_html__('Pixel Before After Slider', 'dipi-divi-pixel');
        $this->main_css_element = '%%order_class%%.dipi_before_after_slider';
    }

    public function get_settings_modal_toggles()
    {
        return [
            'general' => [
                'toggles' => [
                    'image' => esc_html__('Images', 'dipi-divi-pixel'),
                    'labels' => esc_html__('Labels', 'dipi-divi-pixel'),
                ],
            ],
            'advanced' => [
                'toggles' => [
                    'slider' => esc_html__('Slider', 'dipi-divi-pixel'),
                    'labels' => esc_html__('Labels', 'dipi-divi-pixel'),
                    'overlay' => esc_html__('Overlay', 'dipi-divi-pixel'),
                ],
            ],
        ];
    }

    public function get_fields()
    {
        return array(
            'before_image' => array(
                'label' => esc_html__('Before Image', 'dipi-divi-pixel'),
                'type' => 'upload',
                'option_category' => 'basic_option',
                'upload_button_text' => esc_attr__('Upload an image', 'dipi-divi-pixel'),
                'choose_text' => esc_attr__('Choose an Image', 'dipi-divi-pixel'),
                'update_text' => esc_attr__('Set As Image', 'dipi-divi-pixel'),
                'description' => esc_html__('Upload an image to display in the module.', 'dipi-divi-pixel'),
                'toggle_slug' => 'image',
            ),

            'before_image_alt' => array(
                'label' => esc_html__('Before Image Alt Text', 'dipi-divi-pixel'),
                'type' => 'text',
                'description' => esc_html__('Define the HTML ALT text for the image.', 'dipi-divi-pixel'),
                'toggle_slug' => 'image',
            ),

            'after_image' => array(
                'label' => esc_html__('After Image', 'dipi-divi-pixel'),
                'type' => 'upload',
                'option_category' => 'basic_option',
                'upload_button_text' => esc_attr__('Upload an image', 'dipi-divi-pixel'),
                'choose_text' => esc_attr__('Choose an Image', 'dipi-divi-pixel'),
                'update_text' => esc_attr__('Set As Image', 'dipi-divi-pixel'),
                'description' => esc_html__('Upload an image to display in the module.', 'dipi-divi-pixel'),
                'toggle_slug' => 'image',
            ),

            'after_image_alt' => array(
                'label' => esc_html__('After Image Alt Text', 'dipi-divi-pixel'),
                'type' => 'text',
                'option_category' => 'basic_option',
                'description' => esc_html__('Define the HTML ALT text for the image.', 'dipi-divi-pixel'),
                'toggle_slug' => 'image',
            ),

            /* Slider Settings Labels */

            'before_label' => array(
                'label' => esc_html__('Before Label', 'dipi-divi-pixel'),
                'type' => 'text',
                'option_category' => 'basic_option',
                'toggle_slug' => 'labels',
                'description' => esc_html__('The label for the before image.', 'dipi-divi-pixel'),
                'default' => esc_html__('Before', 'dipi-divi-pixel'),
            ),

            'after_label' => array(
                'label' => esc_html__('After Label', 'dipi-divi-pixel'),
                'type' => 'text',
                'option_category' => 'basic_option',
                'toggle_slug' => 'labels',
                'description' => esc_html__('The label for the after image.', 'dipi-divi-pixel'),
                'default' => esc_html__('After', 'dipi-divi-pixel'),
            ),

            'always_show_labels' => array(
                'label' => esc_html__('Always Show Label', 'dipi-divi-pixel'),
                'type' => 'yes_no_button',
                'option_category' => 'basic_option',
                'toggle_slug' => 'labels',
                'tab_slug' => 'advanced',
                'default' => 'off',
                'options'           => array(
                    'off' => esc_html__( 'Off', 'dipi-divi-pixel' ),
                    'on'  => esc_html__( 'On', 'dipi-divi-pixel' ),
                  ),
                'description' => esc_html__('Whether to always show the labels or only show them on hover.', 'dipi-divi-pixel'),
            ),

            //TODO: Label BG, Label Padding, Label Border Radius

            'before_label_bg_color' => [
                'label' => esc_html__('Before Label Bg Color', 'dipi-divi-pixel'),
                'type' => 'color-alpha',
                'default' => "rgba(255, 255, 255, 0.2)",
                'toggle_slug' => 'labels',
                'tab_slug' => 'advanced',
            ],

            'after_label_bg_color' => [
                'label' => esc_html__('After Label Bg Color', 'dipi-divi-pixel'),
                'type' => 'color-alpha',
                'default' => "rgba(255, 255, 255, 0.2)",
                'toggle_slug' => 'labels',
                'tab_slug' => 'advanced',
            ],

            /* Slider Settings Overlay */
            'enable_overlay' => array(
                'label' => esc_html__('Enable Overlay', 'dipi-divi-pixel'),
                'type' => 'yes_no_button',
                'toggle_slug' => 'overlay',
                'tab_slug' => 'advanced',
                'default' => 'on',
                'options'           => array(
                    'off' => esc_html__( 'Off', 'dipi-divi-pixel' ),
                    'on'  => esc_html__( 'On', 'dipi-divi-pixel' ),
                  ),
                'description' => esc_html__('Whether or not to show the overlay on hover.', 'dipi-divi-pixel'),
            ),

            'overlay_color' => [
                'label' => esc_html__('Overlay  Color', 'dipi-divi-pixel'),
                'type' => 'color-alpha',
                'default' => "rgba(0, 0, 0, 0.5)",
                'toggle_slug' => 'overlay',
                'tab_slug' => 'advanced',
            ],

            /* Slider Settings General */

            'direction' => array(
                'label' => esc_html__('Slider Direction', 'dipi-divi-pixel'),
                'type' => 'select',
                'option_category' => 'basic_option',
                'options' => array(
                    'horizontal' => 'Horizontal',
                    'vertical' => 'Vertical',
                ),
                'toggle_slug' => 'slider',
                'tab_slug' => 'advanced',
                'default' => 'horizontal',
                'description' => esc_html__('The direction of the slider.', 'dipi-divi-pixel'),
            ),

            'offset' => array(
                'label' => esc_html__('Slider Start Offset', 'dipi-divi-pixel'),
                'type' => 'range',
                'option_category' => 'layout',
                'default' => '50',
                'toggle_slug' => 'slider',
                'tab_slug' => 'advanced',
                'unitless' => true,
                'range_settings' => array(
                    'min' => '0',
                    'max' => '100',
                    'step' => '1',
                ),
                'description' => esc_html__('The initial offset of the slider in percent.', 'dipi-divi-pixel'),
            ),
            
            'slider_color' => [
                'label' => esc_html__('Slider Color', 'dipi-divi-pixel'),
                'type' => 'color-alpha',
                'default' => "#ffffff",
                'toggle_slug' => 'slider',
                'tab_slug' => 'advanced',
            ],

            'slider_handle_color' => [
                'label' => esc_html__('Handle Color', 'dipi-divi-pixel'),
                'type' => 'color-alpha',
                'default' => "#ffffff",
                'toggle_slug' => 'slider',
                'tab_slug' => 'advanced',
            ],
            
            'slider_handle_bg_color' => [
                'label' => esc_html__('Handle BG Color', 'dipi-divi-pixel'),
                'type' => 'color-alpha',
                'toggle_slug' => 'slider',
                'tab_slug' => 'advanced',
            ],
            
            'slider_handle_icon_color' => [
                'label' => esc_html__('Handle Icon Color', 'dipi-divi-pixel'),
                'type' => 'color-alpha',
                'toggle_slug' => 'slider',
                'tab_slug' => 'advanced',
                'default' => "#ffffff",
            ],
        );
    }

    public function get_advanced_fields_config()
    {
        return [

            'fonts' => false,
            'text' => false,
            'text_shadow' => false,

            'fonts' => [
                'labels' => [
                    'label' => esc_html__('Title', 'dipi-divi-pixel'),
                    'css' => [
                        'main' => "{$this->main_css_element} .dipi_before_after_slider_label:before",
                    ],
                    'important' => 'all',
                    'toggle_slug' => 'labels',
                    'hide_text_align' => true
                ],
            ],

        ];
    }

    public function get_custom_css_fields_config(){
        return [
            'dss_image_before' => [
                'label' => 'Before Image',
                'selector' => '%%order_class%% .dipi_before_after_slider_before',
            ],
            'dss_image_after' => [
                'label' => 'After Image',
                'selector' => '%%order_class%% .dipi_before_after_slider_after',
            ],
            'dss_label_before' => [
                'label' => 'Before Label',
                'selector' => '%%order_class%% .dipi_before_after_slider_overlay .dipi_before_after_slider_before_label:before',
            ],
            'dss_label_after' => [
                'label' => 'After Label',
                'selector' => '%%order_class%% .dipi_before_after_slider_overlay .dipi_before_after_slider_after_label:before',
            ],
            'dss_overlay' => [
                'label' => 'Overlay',
                'selector' => '%%order_class%% .dipi_before_after_slider_overlay',
            ],
        ];
    }

    public function render($attrs, $content = null, $render_slug)
    {
        wp_enqueue_script('dipi_before_after_slider');
        $options = htmlspecialchars(json_encode($this->dipi_before_after_slider()), ENT_QUOTES, 'UTF-8');

        $this->render_css($render_slug);

        return sprintf(
            '<div class="dipi_before_after_slider_container" data-options="%1$s">
            </div>',
            $options
        );
    }

    public function dipi_before_after_slider()
    {
        return [
            "before_image"     => $this->props["before_image"],
            "before_image_alt" => $this->props["before_image_alt"],
            "before_label"     => $this->props["before_label"],
            "after_image"      => $this->props["after_image"],
            "after_image_alt"  => $this->props["after_image_alt"],
            "after_label"      => $this->props["after_label"],
            "offset"           => $this->props["offset"],
            "direction"        => $this->props["direction"]
        ];
    }

    public function render_css($render_slug){
        if("on" === $this->props["always_show_labels"]) {
            ET_Builder_Element::set_style($render_slug, [
                'selector' => "%%order_class%% .dipi_before_after_slider_overlay .dipi_before_after_slider_after_label, %%order_class%% .dipi_before_after_slider_overlay .dipi_before_after_slider_before_label",
                'declaration' => "opacity: 1 !important;",
            ]);
        }

        ET_Builder_Element::set_style($render_slug, [
            'selector' => "%%order_class%% .dipi_before_after_slider_overlay .dipi_before_after_slider_before_label:before",
            'declaration' => "background: {$this->props['before_label_bg_color']};",
        ]);

        ET_Builder_Element::set_style($render_slug, [
            'selector' => "%%order_class%% .dipi_before_after_slider_overlay .dipi_before_after_slider_after_label:before",
            'declaration' => "background: {$this->props['after_label_bg_color']};",
        ]);
        
        if("on" ===$this->props["enable_overlay"]) {            
            ET_Builder_Element::set_style($render_slug, [
                'selector' => "%%order_class%% .dipi_before_after_slider_overlay:hover",
                'declaration' => "background: {$this->props['overlay_color']};",
            ]);
        }

        //Slider handle color
        ET_Builder_Element::set_style($render_slug, [
            'selector' => "%%order_class%% .dipi_before_after_slider_handle:before, %%order_class%%  .dipi_before_after_slider_handle:after",
            'declaration' => "background: {$this->props['slider_color']};"
        ]);
                
        ET_Builder_Element::set_style($render_slug, [
            'selector' => "%%order_class%% .dipi_before_after_slider_horizontal .dipi_before_after_slider_handle:before",
            'declaration' => "-webkit-box-shadow: 0 3px 0 {$this->props['slider_handle_color']}, 0px 0px 12px rgba(51, 51, 51, 0.5);
                            -moz-box-shadow: 0 3px 0 {$this->props['slider_handle_color']}, 0px 0px 12px rgba(51, 51, 51, 0.5);
                            box-shadow: 0 3px 0 {$this->props['slider_handle_color']}, 0px 0px 12px rgba(51, 51, 51, 0.5);"
        ]);

        ET_Builder_Element::set_style($render_slug, [
            'selector' => "%%order_class%% .dipi_before_after_slider_horizontal .dipi_before_after_slider_handle:after",
            'declaration' => "-webkit-box-shadow: 0 -3px 0 {$this->props['slider_handle_color']}, 0px 0px 12px rgba(51, 51, 51, 0.5);
                            -moz-box-shadow: 0 -3px 0 {$this->props['slider_handle_color']}, 0px 0px 12px rgba(51, 51, 51, 0.5);
                            box-shadow: 0 -3px 0 {$this->props['slider_handle_color']}, 0px 0px 12px rgba(51, 51, 51, 0.5);"
        ]);

        ET_Builder_Element::set_style($render_slug, [
            'selector' => "%%order_class%% .dipi_before_after_slider_vertical .dipi_before_after_slider_handle:before",
            'declaration' => "-webkit-box-shadow: 3px 0 0 {$this->props['slider_handle_color']}, 0px 0px 12px rgba(51, 51, 51, 0.5);
                            -moz-box-shadow: 3px 0 0 {$this->props['slider_handle_color']}, 0px 0px 12px rgba(51, 51, 51, 0.5);
                            box-shadow: 3px 0 0 {$this->props['slider_handle_color']}, 0px 0px 12px rgba(51, 51, 51, 0.5);"
        ]);

        ET_Builder_Element::set_style($render_slug, [
            'selector' => "%%order_class%% .dipi_before_after_slider_vertical .dipi_before_after_slider_handle:after",
            'declaration' => "-webkit-box-shadow: -3px 0 0 {$this->props['slider_handle_color']}, 0px 0px 12px rgba(51, 51, 51, 0.5);
                            -moz-box-shadow: -3px 0 0 {$this->props['slider_handle_color']}, 0px 0px 12px rgba(51, 51, 51, 0.5);
                            box-shadow: -3px 0 0 {$this->props['slider_handle_color']}, 0px 0px 12px rgba(51, 51, 51, 0.5);"
        ]);

        ET_Builder_Element::set_style($render_slug, [
            'selector' => "%%order_class%% .dipi_before_after_slider_handle",
            'declaration' => "border-color: {$this->props['slider_handle_color']};"
        ]);

        ET_Builder_Element::set_style($render_slug, [
            'selector' => "%%order_class%% .dipi_before_after_slider_handle",
            'declaration' => "background: {$this->props['slider_handle_bg_color']};"
        ]);

        //Arrow of handle
        ET_Builder_Element::set_style($render_slug, [
            'selector' => "%%order_class%% .dipi_before_after_slider_left_arrow",
            'declaration' => "border-right-color: {$this->props['slider_handle_icon_color']};"
        ]);

        ET_Builder_Element::set_style($render_slug, [
            'selector' => "%%order_class%% .dipi_before_after_slider_right_arrow",
            'declaration' => "border-left-color: {$this->props['slider_handle_icon_color']};"
        ]);

        ET_Builder_Element::set_style($render_slug, [
            'selector' => "%%order_class%% .dipi_before_after_slider_up_arrow",
            'declaration' => "border-bottom-color: {$this->props['slider_handle_icon_color']};"
        ]);

        ET_Builder_Element::set_style($render_slug, [
            'selector' => "%%order_class%% .dipi_before_after_slider_down_arrow",
            'declaration' => "border-top-color: {$this->props['slider_handle_icon_color']};"
        ]);

    }
}

new DIPI_BeforeAfterSlider;
