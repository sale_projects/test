<?php

add_filter("et_pb_all_fields_unprocessed_dipi_blog_slider", function ($fields_unprocessed) {
    $fields_unprocessed['button_icon']['computed_affects'] = ['__blogposts'];
    $fields_unprocessed['button_use_icon']['computed_affects'] = ['__blogposts'];
    return $fields_unprocessed;
});

// TODO: Why are we using this base class? If it's not 100% required, we should use DIPI_Builder_Module
class DIPI_Blog_Slider extends ET_Builder_Module_Type_PostBased
{

    public function init()
    {
        $this->icon_path = plugin_dir_path(__FILE__) . "dp-blog-slider.svg";
        $this->name = esc_html__('Pixel Blog Slider', 'dipi-divi-pixel');
        $this->plural = esc_html__('Pixel Blog Slider', 'dipi-divi-pixel');
        $this->slug = 'dipi_blog_slider';
        $this->vb_support = 'on';
        $this->main_css_element = '%%order_class%% .dipi_blog_slider';
        $this->settings_modal_toggles = [
            'general' => [
                'toggles' => [
                    'main_content' => esc_html__('Content', 'dipi-divi-pixel'),
                    'elements' => esc_html__('Elements', 'dipi-divi-pixel'),
                    'carousel' => esc_html__('Slider Settings', 'dipi-divi-pixel'),
                ],
            ],

            'advanced' => [
                'toggles' => [
                    'image' => esc_html__('Image', 'dipi-divi-pixel'),
                    'blog_item' => esc_html__('Blog Item', 'dipi-divi-pixel'),
                    'blog_texts' => [
                        'sub_toggles' => [
                            'title' => [
                                'name' => 'Title',
                            ],
                            'body' => [
                                'name' => 'Body',
                            ],
                            'cat' => [
                                'name' => 'Cat',
                            ],
                            'author' => [
                                'name' => 'Author',
                            ],
                        ],
                        'tabbed_subtoggles' => true,
                        'title' => esc_html__('Blog Texts', 'dipi-divi-pixel'),
                    ],
                    'blog_date' => esc_html__('Blog Date', 'dipi-divi-pixel'),
                    'navigation' => esc_html__('Navigation', 'dipi-divi-pixel'),
                    'pagination' => esc_html__('Pagination', 'dipi-divi-pixel'),
                ],
            ],

        ];
    }

    public function get_custom_css_fields_config()
    {
        $custom_css_fields = [];

        $custom_css_fields['category'] = [
            'label' => esc_html__('Category', 'dipi-divi-pixel'),
            'selector' => '.dipi-categories, .dipi-categories a',
        ];

        $custom_css_fields['author'] = [
            'label' => esc_html__('Author', 'dipi-divi-pixel'),
            'selector' => '.dipi-author',
        ];

        $custom_css_fields['month'] = [
            'label' => esc_html__('Date Month', 'dipi-divi-pixel'),
            'selector' => '.dipi-month',
        ];

        $custom_css_fields['day'] = [
            'label' => esc_html__('Date Day', 'dipi-divi-pixel'),
            'selector' => '.dipi-day',
        ];

        $custom_css_fields['year'] = [
            'label' => esc_html__('Date Year', 'dipi-divi-pixel'),
            'selector' => '.dipi-year',
        ];

        $custom_css_fields['title'] = [
            'label' => esc_html__('Title', 'dipi-divi-pixel'),
            'selector' => '.dipi-entry-title',
        ];

        $custom_css_fields['content'] = [
            'label' => esc_html__('Description', 'dipi-divi-pixel'),
            'selector' => '.dipi-post-text',
        ];

        $custom_css_fields['button'] = [
            'label' => esc_html__('Button', 'dipi-divi-pixel'),
            'selector' => '.dipi-more-link',
        ];

        $custom_css_fields['comments'] = [
            'label' => esc_html__('Comments Count', 'dipi-divi-pixel'),
            'selector' => '.dipi-comments',
        ];

        $custom_css_fields['arrow_nav'] = [
            'label' => esc_html__('Arrow Navigation', 'dipi-divi-pixel'),
            'selector' => '%%order_class%% .swiper-arrow-button',
        ];

        $custom_css_fields['arrow_icon'] = [
            'label' => esc_html__('Arrow Icon', 'dipi-divi-pixel'),
            'selector' => '%%order_class%% .swiper-button-next:after, %%order_class%% .swiper-button-prev:after',
        ];

        $custom_css_fields['pagination_default'] = [
            'label' => esc_html__('Pagination Default', 'dipi-divi-pixel'),
            'selector' => '.swiper-pagination-bullet',
        ];

        $custom_css_fields['pagination_active'] = [
            'label' => esc_html__('Pagination Active', 'dipi-divi-pixel'),
            'selector' => '.swiper-pagination-bullet-active',
        ];

        return $custom_css_fields;
    }

    public function get_advanced_fields_config()
    {
        $advanced_fields = [];

        $advanced_fields['fonts'] = false;
        $advanced_fields['text'] = false;
        $advanced_fields['text_shadow'] = false;
        $advanced_fields['link_options'] = false;

        $advanced_fields["box_shadow"]["item"] = [
            'css' => [
                'main' => "%%order_class%% .dipi-blog-post",
            ],
            'toggle_slug' => 'blog_item',
        ];

        $advanced_fields["borders"]["item"] = [
            'css' => [
                'main' => [
                    'border_radii' => "%%order_class%% .dipi-blog-post",
                    'border_styles' => "%%order_class%% .dipi-blog-post",
                ],
            ],
            'toggle_slug' => 'blog_item',
        ];

        $advanced_fields['fonts']['header'] = [
            'label' => esc_html__('Title', 'dipi-divi-pixel'),
            'css' => [
                'main' => "%%order_class%% .dipi-entry-title",
                'font' => "%%order_class%% .dipi-entry-title a",
                'color' => "%%order_class%% .dipi-entry-title a",
                'hover' => "%%order_class%% .dipi-entry-title:hover, %%order_class%% .dipi-entry-title:hover a",
                'color_hover' => "%%order_class%% .dipi-entry-title:hover a",
                'important' => 'all',
            ],

            'header_level' => [
                'default' => 'h2',
                'computed_affects' => [
                    '__blogposts',
                ],
            ],
            'toggle_slug' => 'blog_texts',
            'sub_toggle' => 'title',
        ];

        $advanced_fields['fonts']['body'] = [
            'label' => esc_html__('Body', 'dipi-divi-pixel'),
            'css' => [
                'main' => "%%order_class%% .dipi-post-text",
                'color' => "%%order_class%% .dipi-post-text *, %%order_class%% .dipi-post-text",
                'line_height' => "%%order_class%% .dipi-post-text p",
                'important' => 'all'
            ],
            'toggle_slug' => 'blog_texts',
            'sub_toggle' => 'body',
        ];

        $advanced_fields['fonts']['cat'] = [
            'label' => esc_html__('Cat', 'dipi-divi-pixel'),
            'css' => [
                'main' => "%%order_class%% .dipi-categories",
                'color' => "%%order_class%% .dipi-categories a",
            ],

            'toggle_slug' => 'blog_texts',
            'sub_toggle' => 'cat',
        ];

        $advanced_fields['fonts']['author'] = [
            'label' => esc_html__('Author', 'dipi-divi-pixel'),
            'css' => [
                'main' => "%%order_class%% .dipi-author .author, %%order_class%% .dipi-author .author a",
            ],
            'hide_text_align' => true,
            'toggle_slug' => 'blog_texts',
            'sub_toggle' => 'author',
        ];

        $advanced_fields['fonts']['date'] = [
            'label' => esc_html__('Date', 'dipi-divi-pixel'),
            'css' => [
                'main' => "%%order_class%% .dipi-date",
            ],
            'hide_text_align' => true,
            'hide_font_size' => true,
            'hide_line_height' => true,
            'hide_letter_spacing' => true,
            'toggle_slug' => 'blog_date',
        ];

        $advanced_fields['button']["button"] = [
            'label' => esc_html__('Read More', 'dipi-divi-pixel'),
            'css' => [
                'main' => "%%order_class%% .dipi-more-link",
                'important' => true,
            ],
            'box_shadow' => [
                'css' => [
                    'main' => "%%order_class%% .dipi-more-link"
                ]
            ],
            'use_alignment' => true,
        ];

        return $advanced_fields;
    }

    public function get_fields()
    {

        $fields = [];

        $fields['author_align'] = [
            'label' => esc_html__('Author Alignment', 'dipi-divi-pixel'),
            'type' => 'text_align',
            'options' => et_builder_get_text_orientation_options(['justified']),
            'options_icon' => 'module_align',
            'tab_slug' => 'advanced',
            'toggle_slug' => 'blog_texts',
            'sub_toggle' => 'author',
        ];

        $fields['posts_number'] = [
            'label' => esc_html__('Post Count', 'dipi-divi-pixel'),
            'type' => 'text',
            'option_category' => 'configuration',
            'computed_affects' => ['__blogposts'],
            'toggle_slug' => 'main_content',
            'default' => 10,
        ];

        $fields['include_categories'] = [
            'label' => esc_html__('Included Categories', 'dipi-divi-pixel'),
            'type' => 'categories',
            'meta_categories' => array(
                'all' => esc_html__('All Categories', 'dipi-divi-pixel'),
                'current' => esc_html__('Current Category', 'dipi-divi-pixel'),
            ),
            'option_category' => 'basic_option',
            'renderer_options' => array(
                'use_terms' => false,
            ),
            'toggle_slug' => 'main_content',
            'computed_affects' => ['__blogposts'],
        ];

        $fields['show_thumbnail'] = [
            'label' => esc_html__('Show Featured Image', 'dipi-divi-pixel'),
            'type' => 'yes_no_button',
            'options' => array(
                'on' => esc_html__('Yes', 'dipi-divi-pixel'),
                'off' => esc_html__('No', 'dipi-divi-pixel'),
            ),
            'computed_affects' => ['__blogposts'],
            'toggle_slug' => 'elements',
            'default_on_front' => 'on',
        ];

        $fields['image_clickable'] = [
            'label' => esc_html__('Clickable Image', 'dipi-divi-pixel'),
            'type' => 'yes_no_button',
            'default' => 'on',
            'options' => array(
                'on' => esc_html__('Yes', 'dipi-divi-pixel'),
                'off' => esc_html__('No', 'dipi-divi-pixel'),
            ),
            'show_if' => [
                'show_thumbnail' => 'on',
            ],
            'computed_affects' => ['__blogposts'],
            'toggle_slug' => 'elements',
        ];

        $fields['use_thumbnail_height'] = [
            'label' => esc_html__('Featured Image Height', 'dipi-divi-pixel'),
            'type' => 'yes_no_button',
            'default' => 'off',
            'options' => [
                'on' => esc_html__('Yes', 'dipi-divi-pixel'),
                'off' => esc_html__('No', 'dipi-divi-pixel'),
            ],
            'show_if' => [
                'show_thumbnail' => 'on',
            ],
            'toggle_slug' => 'elements',
        ];

        $fields['thumbnail_height'] = [
            'label' => esc_html__('Featured Image Height', 'dipi-divi-pixel'),
            'type' => 'range',
            'default' => '400px',
            'default_unit' => 'px',
            'mobile_options' => true,
            'show_if' => [
                'show_thumbnail' => 'on',
                'use_thumbnail_height' => 'on',
            ],
            'range_settings' => [
                'min' => '0',
                'max' => '600',
                'step' => '10',
            ],
            'toggle_slug' => 'elements',
        ];

        $fields['excerpt_length'] = [
            'label' => esc_html__('Content Length', 'dipi-divi-pixel'),
            'type' => 'text',
            'default' => '170',
            'toggle_slug' => 'main_content',
            'computed_affects' => ['__blogposts'],
        ];

        $fields['show_more'] = [
            'label' => esc_html__('Show Read More Button', 'dipi-divi-pixel'),
            'type' => 'yes_no_button',
            'options' => array(
                'off' => esc_html__('No', 'dipi-divi-pixel'),
                'on' => esc_html__('Yes', 'dipi-divi-pixel'),
            ),
            'toggle_slug' => 'elements',
            'default' => 'off',
            'computed_affects' => ['__blogposts'],
        ];

        $fields['show_more_text'] = [
            'label' => esc_html__('Read More Text', 'dipi-divi-pixel'),
            'type' => 'text',
            'default' => 'Read More',
            'show_if' => [
                'show_more' => 'on',
            ],
            'toggle_slug' => 'elements',
            'computed_affects' => ['__blogposts'],
        ];

        $fields['show_author'] = [
            'label' => esc_html__('Show Author', 'dipi-divi-pixel'),
            'type' => 'yes_no_button',
            'options' => array(
                'on' => esc_html__('Yes', 'dipi-divi-pixel'),
                'off' => esc_html__('No', 'dipi-divi-pixel'),
            ),
            'toggle_slug' => 'elements',
            'default_on_front' => 'on',
            'computed_affects' => ['__blogposts'],
        ];

        $fields['show_date'] = [
            'label' => esc_html__('Show Date', 'dipi-divi-pixel'),
            'type' => 'yes_no_button',
            'options' => array(
                'on' => esc_html__('Yes', 'dipi-divi-pixel'),
                'off' => esc_html__('No', 'dipi-divi-pixel'),
            ),
            'affects' => [
                'date_circle_icon',
                'date_circle_color',
                'date_circle_border',
                'date_circle_border_color',
            ],
            'toggle_slug' => 'elements',
            'default_on_front' => 'on',
            'computed_affects' => ['__blogposts'],
        ];

        $fields["date_circle_color"] = [
            'label' => esc_html__('Date Background Color', 'dipi-divi-pixel'),
            'type' => 'color-alpha',
            'custom_color' => true,
            'depends_show_if' => 'on',
            'tab_slug' => 'advanced',
            'toggle_slug' => 'blog_date',
        ];

        $fields["date_circle_icon"] = [
            'label' => esc_html__('Show Circle', 'dipi-divi-pixel'),
            'type' => 'yes_no_button',
            'option_category' => 'configuration',
            'default' => 'off',
            'options' => array(
                'on' => esc_html__('Yes', 'dipi-divi-pixel'),
                'off' => esc_html__('No', 'dipi-divi-pixel'),
            ),
            'depends_show_if' => 'on',
            'tab_slug' => 'advanced',
            'toggle_slug' => 'blog_date',
        ];

        $fields["date_circle_border"] = [
            'label' => esc_html__('Show Border', 'dipi-divi-pixel'),
            'type' => 'yes_no_button',
            'option_category' => 'configuration',
            'default' => 'off',
            'options' => [
                'on' => esc_html__('Yes', 'dipi-divi-pixel'),
                'off' => esc_html__('No', 'dipi-divi-pixel'),
            ],
            'depends_show_if' => 'on',
            'tab_slug' => 'advanced',
            'toggle_slug' => 'blog_date',
        ];

        $fields["date_circle_border_color"] = [
            'label' => esc_html__('Circle Border Color', 'dipi-divi-pixel'),
            'type' => 'color-alpha',
            'custom_color' => true,
            'show_if' => [
                'date_circle_border' => 'on',
            ],
            'depends_show_if' => 'on',
            'tab_slug' => 'advanced',
            'toggle_slug' => 'blog_date',
        ];

        $fields['date_right_space'] = [
            'label' => esc_html__('Date Right Space', 'dipi-divi-pixel'),
            'type' => 'range',
            'default' => '0px',
            'default_unit' => 'px',
            'default_on_front' => '0px',
            'allowed_units' => array('%', 'px'),
            'range_settings' => [
                'min' => '1',
                'max' => '50',
                'step' => '1',
            ],
            'mobile_options' => true,
            'responsive' => true,
            'tab_slug' => 'advanced',
            'toggle_slug' => 'blog_date',
        ];

        $fields['date_top_space'] = [
            'label' => esc_html__('Date Top Space', 'dipi-divi-pixel'),
            'type' => 'range',
            'default' => '0px',
            'default_unit' => 'px',
            'default_on_front' => '0px',
            'allowed_units' => array('%', 'px'),
            'range_settings' => [
                'min' => '1',
                'max' => '50',
                'step' => '1',
            ],
            'mobile_options' => true,
            'responsive' => true,
            'tab_slug' => 'advanced',
            'toggle_slug' => 'blog_date',
        ];

        $fields['show_categories'] = [
            'label' => esc_html__('Show Categories', 'dipi-divi-pixel'),
            'type' => 'yes_no_button',
            'option_category' => 'configuration',
            'options' => array(
                'on' => esc_html__('Yes', 'dipi-divi-pixel'),
                'off' => esc_html__('No', 'dipi-divi-pixel'),
            ),
            'toggle_slug' => 'elements',
            'default_on_front' => 'on',
            'computed_affects' => ['__blogposts'],
        ];

        $fields['show_comments'] = [
            'label' => esc_html__('Show Comment Count', 'dipi-divi-pixel'),
            'type' => 'yes_no_button',
            'option_category' => 'configuration',
            'options' => array(
                'on' => esc_html__('Yes', 'dipi-divi-pixel'),
                'off' => esc_html__('No', 'dipi-divi-pixel'),
            ),
            'toggle_slug' => 'elements',
            'default_on_front' => 'off',
            'computed_affects' => ['__blogposts'],
        ];

        $fields['show_excerpt'] = [
            'label' => esc_html__('Show Content', 'dipi-divi-pixel'),
            'type' => 'yes_no_button',
            'options' => array(
                'on' => esc_html__('Yes', 'dipi-divi-pixel'),
                'off' => esc_html__('No', 'dipi-divi-pixel'),
            ),
            'default_on_front' => 'on',
            'toggle_slug' => 'elements',
            'computed_affects' => ['__blogposts'],
        ];

        $fields['columns'] = [
            'label' => esc_html__('Number of Columns', 'dipi-divi-pixel'),
            'type' => 'range',
            'default' => '4',
            'default_on_front' => '4',
            'range_settings' => [
                'min' => '1',
                'max' => '12',
                'step' => '1',
            ],
            'unitless' => true,
            'mobile_options' => true,
            'responsive' => true,
            'toggle_slug' => 'carousel',
        ];

        $fields['space_between'] = [
            'label' => esc_html__('Spacing', 'dipi-divi-pixel'),
            'type' => 'range',
            'default' => '50',
            'range_settings' => [
                'min' => '5',
                'max' => '100',
                'step' => '1',
            ],
            'unitless' => true,
            'mobile_options' => true,
            'responsive' => true,
            'toggle_slug' => 'carousel',
        ];

        $fields['container_padding'] = [
            'label' => esc_html__('Container Padding', 'dipi-divi-pixel'),
            'type' => 'custom_margin',
            'default' => '30px|30px|30px|30px',
            'mobile_options' => true,
            'responsive' => true,
            'tab_slug' => 'advanced',
            'toggle_slug' => 'margin_padding',
        ];

        $fields['item_padding'] = [
            'label' => esc_html__('Item Padding', 'dipi-divi-pixel'),
            'type' => 'custom_margin',
            'default' => '0px|0px|20px|0px',
            'mobile_options' => true,
            'responsive' => true,
            'tab_slug' => 'advanced',
            'toggle_slug' => 'margin_padding',
        ];

        $fields['effect'] = [
            'label' => esc_html__('Effect', 'dipi-divi-pixel'),
            'type' => 'select',
            'option_category' => 'layout',
            'options' => [
                'coverflow' => esc_html__('Coverflow', 'dipi-divi-pixel'),
                'slide' => esc_html__('Slide', 'dipi-divi-pixel'),
            ],
            'default' => 'slide',
            'toggle_slug' => 'carousel',
        ];

        $fields['slide_shadows'] = [
            'label' => esc_html__('Slide Shadow', 'dipi-divi-pixel'),
            'type' => 'yes_no_button',
            'options' => [
                'on' => esc_html__('Yes', 'dipi-divi-pixel'),
                'off' => esc_html__('No', 'dipi-divi-pixel'),
            ],
            'default' => 'on',
            'show_if' => [
                'effect' => 'coverflow',
            ],
            'toggle_slug' => 'carousel',
        ];

        $fields["shadow_overlay_color"] = [
            'label' => esc_html__('Slide Item Color', 'dipi-divi-pixel'),
            'type' => 'color-alpha',
            'show_if' => [
                'effect' => 'coverflow',
            ],
            'tab_slug' => 'advanced',
            'toggle_slug' => 'overlay',
        ];

        $fields['rotate'] = [
            'label' => esc_html__('Rotate', 'dipi-divi-pixel'),
            'type' => 'range',
            'range_settings ' => [
                'min' => '0',
                'max' => '100',
                'step' => '1',
            ],
            'default' => '50',
            'show_if' => [
                'effect' => 'coverflow',
            ],
            'validate_unit' => true,
            'toggle_slug' => 'carousel',
        ];

        $fields['speed'] = [
            'label' => esc_html__('Transition Duration', 'dipi-divi-pixel'),
            'type' => 'range',
            'range_settings' => [
                'min' => '1',
                'max' => '5000',
                'step' => '100',
            ],
            'default' => 500,
            'validate_unit' => false,
            'toggle_slug' => 'carousel',
        ];

        $fields['loop'] = [
            'label' => esc_html__('Loop', 'dipi-divi-pixel'),
            'type' => 'yes_no_button',
            'option_category' => 'configuration',
            'options' => [
                'on' => esc_html__('Yes', 'dipi-divi-pixel'),
                'off' => esc_html__('No', 'dipi-divi-pixel'),
            ],
            'default' => 'off',
            'toggle_slug' => 'carousel',
        ];

        $fields['autoplay'] = [
            'label' => esc_html__('Autoplay', 'dipi-divi-pixel'),
            'type' => 'yes_no_button',
            'options' => [
                'off' => esc_html__('No', 'dipi-divi-pixel'),
                'on' => esc_html__('Yes', 'dipi-divi-pixel'),
            ],
            'default' => 'off',
            'toggle_slug' => 'carousel',
        ];

        $fields['pause_on_hover'] = [
            'label' => esc_html__('Pause on Hover', 'dipi-divi-pixel'),
            'type' => 'yes_no_button',
            'options' => [
                'off' => esc_html__('No', 'dipi-divi-pixel'),
                'on' => esc_html__('Yes', 'dipi-divi-pixel'),
            ],
            'show_if' => [
                'autoplay' => 'on',
            ],
            'toggle_slug' => 'carousel',
            'default' => 'on',
        ];

        $fields['autoplay_speed'] = [
            'label' => esc_html__('Autoplay Speed', 'dipi-divi-pixel'),
            'type' => 'range',
            'range_settings' => array(
                'min' => '1',
                'max' => '10000',
                'step' => '500',
            ),
            'default' => 5000,
            'validate_unit' => false,
            'show_if' => array(
                'autoplay' => 'on',
            ),
            'toggle_slug' => 'carousel',
        ];

        $fields['navigation'] = [
            'label' => esc_html__('Navigation', 'dipi-divi-pixel'),
            'type' => 'yes_no_button',
            'options' => [
                'off' => esc_html__('No', 'dipi-divi-pixel'),
                'on' => esc_html__('Yes', 'dipi-divi-pixel'),
            ],
            'toggle_slug' => 'carousel',
            'default' => 'off',
        ];

        $fields['pagination'] = [
            'label' => esc_html__('Pagination', 'dipi-divi-pixel'),
            'type' => 'yes_no_button',
            'options' => [
                'off' => esc_html__('No', 'dipi-divi-pixel'),
                'on' => esc_html__('Yes', 'dipi-divi-pixel'),
            ],
            'toggle_slug' => 'carousel',
            'default' => 'off',
        ];

        $fields['dynamic_bullets'] = [
            'label' => esc_html__('Dynamic Bullets', 'dipi-divi-pixel'),
            'type' => 'yes_no_button',
            'options' => [
                'off' => esc_html__('No', 'dipi-divi-pixel'),
                'on' => esc_html__('Yes', 'dipi-divi-pixel'),
            ],
            'toggle_slug' => 'carsousel',
            'default' => 'on',
        ];

        $fields['centered'] = [
            'label' => esc_html__('Centered', 'dipi-divi-pixel'),
            'type' => 'yes_no_button',
            'option_category' => 'configuration',
            'options' => array(
                'on' => esc_html__('Yes', 'dipi-divi-pixel'),
                'off' => esc_html__('No', 'dipi-divi-pixel'),
            ),
            'default' => 'off',
            'toggle_slug' => 'carousel',
        ];

        $fields['navigation_prev_icon_yn'] = [
            'label' => esc_html__('Prev Nav Custom Icon', 'dipi-divi-pixel'),
            'type' => 'yes_no_button',
            'options' => [
                'off' => esc_html__('No', 'dipi-divi-pixel'),
                'on' => esc_html__('Yes', 'dipi-divi-pixel'),
            ],
            'default' => 'off',
            'tab_slug' => 'advanced',
            'toggle_slug' => 'navigation',
        ];

        $fields['navigation_prev_icon'] = [
            'label' => esc_html__('Select Previous Nav icon', 'dipi-divi-pixel'),
            'type' => 'select_icon',
            'class' => array('et-pb-font-icon'),
            'default' => '8',
            'show_if' => ['navigation_prev_icon_yn' => 'on'],
            'tab_slug' => 'advanced',
            'toggle_slug' => 'navigation',
        ];

        $fields['navigation_next_icon_yn'] = [
            'label' => esc_html__('Next Nav Custom Icon', 'dipi-divi-pixel'),
            'type' => 'yes_no_button',
            'options' => array(
                'off' => esc_html__('No', 'dipi-divi-pixel'),
                'on' => esc_html__('Yes', 'dipi-divi-pixel'),
            ),
            'default' => 'off',
            'tab_slug' => 'advanced',
            'toggle_slug' => 'navigation',
        ];

        $fields['navigation_next_icon'] = [
            'label' => esc_html__('Select Next Nav icon', 'dipi-divi-pixel'),
            'type' => 'select_icon',
            'class' => array('et-pb-font-icon'),
            'default' => '9',
            'show_if' => ['navigation_next_icon_yn' => 'on'],
            'tab_slug' => 'advanced',
            'toggle_slug' => 'navigation',
        ];

        $fields['navigation_size'] = [
            'label' => esc_html__('Icon Size', 'dipi-divi-pixel'),
            'type' => 'range',
            'range_settings' => array(
                'min' => '1',
                'max' => '100',
                'step' => '1',
            ),
            'default' => 50,
            'validate_unit' => false,
            'tab_slug' => 'advanced',
            'toggle_slug' => 'navigation',
        ];

        $fields['navigation_padding'] = [
            'label' => esc_html__('Icon Padding', 'dipi-divi-pixel'),
            'type' => 'range',
            'range_settings' => [
                'min' => '1',
                'max' => '100',
                'step' => '1',
            ],
            'default' => 10,
            'validate_unit' => false,
            'tab_slug' => 'advanced',
            'toggle_slug' => 'navigation',
        ];

        $fields['navigation_color'] = [
            'label' => esc_html__('Arrow Color', 'dipi-divi-pixel'),
            'type' => 'color-alpha',
            'default' => et_builder_accent_color(),
            'tab_slug' => 'advanced',
            'toggle_slug' => 'navigation',
        ];

        $fields['navigation_bg_color'] = [
            'label' => esc_html__('Arrow Background', 'dipi-divi-pixel'),
            'type' => 'color-alpha',
            'tab_slug' => 'advanced',
            'toggle_slug' => 'navigation',
        ];

        $fields['navigation_circle'] = [
            'label' => esc_html__('Circle Arrow', 'dipi-divi-pixel'),
            'type' => 'yes_no_button',
            'options' => array(
                'off' => esc_html__('No', 'dipi-divi-pixel'),
                'on' => esc_html__('Yes', 'dipi-divi-pixel'),
            ),
            'default' => 'off',
            'tab_slug' => 'advanced',
            'toggle_slug' => 'navigation',
        ];

        $fields['navigation_position_left'] = [
            'label' => esc_html__('Left Navigation Postion', 'dipi-divi-pixel'),
            'type' => 'range',
            'default' => '-66px',
            'default_on_front' => '-66px',
            'default_unit' => 'px',
            'allowed_units' => array('%', 'em', 'rem', 'px', 'cm', 'mm', 'in', 'pt', 'pc', 'ex', 'vh', 'vw'),
            'range_settings' => [
                'min' => '-200',
                'max' => '200',
                'step' => '1',
            ],
            'mobile_options' => true,
            'responsive' => true,
            'tab_slug' => 'advanced',
            'toggle_slug' => 'navigation',
        ];

        $fields['navigation_position_right'] = [
            'label' => esc_html__('Right Navigation Postion', 'dipi-divi-pixel'),
            'type' => 'range',
            'default' => '-66px',
            'default_on_front' => '-66px',
            'default_unit' => 'px',
            'allowed_units' => array('%', 'em', 'rem', 'px', 'cm', 'mm', 'in', 'pt', 'pc', 'ex', 'vh', 'vw'),
            'range_settings' => [
                'min' => '-200',
                'max' => '200',
                'step' => '1',
            ],
            'mobile_options' => true,
            'responsive' => true,
            'tab_slug' => 'advanced',
            'toggle_slug' => 'navigation',
        ];

        $fields['pagination_position'] = [
            'label' => esc_html__('Pagination Postion', 'dipi-divi-pixel'),
            'type' => 'range',
            'default' => '-40',
            'range_settings' => [
                'min' => '-200',
                'max' => '200',
                'step' => '1',
            ],
            'unitless' => true,
            'show_if' => ['pagination' => 'on'],
            'tab_slug' => 'advanced',
            'toggle_slug' => 'pagination',
        ];

        $fields['pagination_color'] = [
            'label' => esc_html__('Pagination Color', 'dipi-divi-pixel'),
            'type' => 'color-alpha',
            'default' => '#d8d8d8',
            'show_if' => ['pagination' => 'on'],
            'tab_slug' => 'advanced',
            'toggle_slug' => 'pagination',
        ];

        $fields['pagination_active_color'] = [
            'label' => esc_html__('Pagination Active Color', 'dipi-divi-pixel'),
            'type' => 'color-alpha',
            'default' => et_builder_accent_color(),
            'show_if' => ['pagination' => 'on'],
            'tab_slug' => 'advanced',
            'toggle_slug' => 'pagination',
        ];

        $fields['image_animation'] = [
            'label' => esc_html__('Image Animation', 'dipi-divi-pixel'),
            'type' => 'select',
            'default' => 'none',
            'options' => [
                'none' => esc_html__('None', 'dipi-divi-pixel'),
                'zoomin' => esc_html__('Zoom In', 'dipi-divi-pixel'),
                'zoomout' => esc_html__('Zoom Out', 'dipi-divi-pixel'),
                'rotate' => esc_html__('Rotate', 'dipi-divi-pixel'),
            ],
            'tab_slug' => 'advanced',
            'toggle_slug' => 'image',
            'computed_affects' => ['__blogposts'],
        ];

        $fields['__blogposts'] = [
            'type' => 'computed',
            'computed_callback' => ['DIPI_Blog_Slider', 'get_blog_posts'],
            'computed_depends_on' => array(
                'posts_number',
                'include_categories',
                'image_animation',
                'show_thumbnail',
                'image_clickable',
                'show_more',
                'show_more_text',
                'show_author',
                'show_date',
                'show_categories',
                'show_comments',
                'show_excerpt',
                'excerpt_length',
                'header_level',
                'button_icon',
                'button_use_icon',
            ),
            'computed_minimum' => array(
                'posts_number',
            ),
        ];

        $additional_options = [];

        $additional_options['item_bg_color'] = [
            'label' => esc_html__('Item Background', 'dipi-divi-pixel'),
            'type' => 'background-field',
            'base_name' => "item_bg",
            'context' => "item_bg",
            'option_category' => 'layout',
            'custom_color' => true,
            'default' => ET_Global_Settings::get_value('all_buttons_bg_color'),
            'depends_show_if' => 'on',
            'tab_slug' => 'advanced',
            'toggle_slug' => "blog_item",
            'background_fields' => array_merge(

                ET_Builder_Element::generate_background_options(
                    'item_bg',
                    'gradient',
                    "advanced",
                    "blog_item",
                    "item_bg_gradient"
                ),

                ET_Builder_Element::generate_background_options(
                    "item_bg",
                    "color",
                    "advanced",
                    "blog_item",
                    "item_bg_color"
                )
            ),
        ];

        $additional_options = array_merge(
            $additional_options,
            $this->generate_background_options(
                "item_bg",
                'skip',
                "advanced",
                "blog_item",
                "item_bg_gradient"
            )
        );

        $additional_options = array_merge(
            $additional_options,
            $this->generate_background_options(
                "item_bg",
                'skip',
                "advanced",
                "blog_item",
                "item_bg_color"
            )
        );

        $additional_options['overlay_bg_color'] = [
            'label' => esc_html__('Overlay Background', 'dipi-divi-pixel'),
            'type' => 'background-field',
            'base_name' => "overlay_bg",
            'context' => "overlay_bg",
            'option_category' => 'layout',
            'custom_color' => true,
            'default' => ET_Global_Settings::get_value('all_buttons_bg_color'),
            'depends_show_if' => 'on',
            'tab_slug' => 'advanced',
            'toggle_slug' => "image",
            'hover' => 'tabs',
            'background_fields' => array_merge(

                ET_Builder_Element::generate_background_options(
                    'overlay_bg',
                    'gradient',
                    "advanced",
                    "image",
                    "overlay_bg_gradient"
                ),

                ET_Builder_Element::generate_background_options(
                    "overlay_bg",
                    "color",
                    "advanced",
                    "image",
                    "overlay_bg_color"
                )
            ),
        ];

        $additional_options = array_merge(
            $additional_options,
            $this->generate_background_options(
                "overlay_bg",
                'skip',
                "advanced",
                "image",
                "overlay_bg_gradient"
            )
        );

        $additional_options = array_merge(
            $additional_options,
            $this->generate_background_options(
                "overlay_bg",
                'skip',
                "advanced",
                "image",
                "overlay_bg_color"
            )
        );

        return array_merge($fields, $additional_options);
    }

    public static function get_blog_posts($args = [], $conditional_tags = [], $current_page = [])
    {

        $defaults = [
            'posts_number' => '',
            'include_categories' => '',
            'image_animation' => '',
            'show_thumbnail' => '',
            'image_clickable' => '',
            'show_author' => '',
            'show_date' => '',
            'show_categories' => '',
            'show_comments' => '',
            'show_excerpt' => '',
            'excerpt_length' => '',
            'header_level' => '',
            'show_more' => '',
            'button_use_icon' => '',
            'button_icon' => '',
            'button_use_icon' => '',
        ];

        $args = wp_parse_args($args, $defaults);
        $processed_header_level = et_pb_process_header_level($args['header_level'], 'h2');
        $processed_header_level = esc_html($processed_header_level);

        $query_args = [
            'posts_per_page' => intval($args['posts_number']),
            'post_status' => 'publish',
            'post_type' => 'post',
        ];

        $is_single = et_fb_conditional_tag('is_single', $conditional_tags);
        $post_id = isset($current_page['id']) ? (int) $current_page['id'] : 0;
        $query_args['cat'] = implode(',', self::filter_include_categories($args['include_categories'], $post_id));

        $image_animation_class = 'dipi-' . $args['image_animation'];

        // Get query
        $q = new WP_Query($query_args);

        ob_start();
        if ($q->have_posts()) {
            while ($q->have_posts()) {
                $q->the_post();
                include dirname(__FILE__) . '/templates/dipi-blog-post.php';
            }
        }

        if (!$posts = ob_get_clean()) {
            $posts = self::get_no_results_template(et_core_esc_previously($processed_header_level));
        }

        return $posts;
    }

    public function render($attrs, $content = null, $render_slug)
    {
        wp_enqueue_script('dipi_blog_slider');
        wp_enqueue_style('dipi_swiper');

        $posts_number = $this->props['posts_number'];
        $include_categories = $this->props['include_categories'];
        $show_thumbnail = $this->props['show_thumbnail'];
        $image_clickable = $this->props['image_clickable'];
        $show_author = $this->props['show_author'];
        $show_date = $this->props['show_date'];
        $show_categories = $this->props['show_categories'];
        $show_comments = $this->props['show_comments'];
        $show_excerpt = $this->props['show_excerpt'];
        $excerpt_length = $this->props['excerpt_length'];
        $show_more = $this->props['show_more'];
        $show_more_text = $this->props['show_more_text'];
        $header_level = $this->props['header_level'];
        $image_animation = $this->props['image_animation'];
        $button_use_icon = $this->props['button_use_icon'];
        $button_icon = $this->props['button_icon'];

        $blog_content = self::get_blog_posts($args = [
            'posts_number' => $posts_number,
            'include_categories' => $include_categories,
            'show_thumbnail' => $show_thumbnail,
            'image_animation' => $image_animation,
            'image_clickable' => $image_clickable,
            'show_author' => $show_author,
            'show_date' => $show_date,
            'show_categories' => $show_categories,
            'show_comments' => $show_comments,
            'show_excerpt' => $show_excerpt,
            'excerpt_length' => $excerpt_length,
            'show_more' => $show_more,
            'show_more_text' => $show_more_text,
            'button_icon' => $button_icon,
            'button_use_icon' => $button_use_icon,
        ]);

        $columns_desktop = $this->props['columns'];
        $columns_tablet = $this->props['columns_tablet'] ? $this->props['columns_tablet'] : $columns_desktop;
        $columns_phone = $this->props['columns_phone'] ? $this->props['columns_phone'] : $columns_tablet;

        if($columns_desktop === "4" && $columns_tablet === "4" && $columns_phone === "4") {
            $columns_tablet = "2";
            $columns_phone = "1";
        }

        $space_between = $this->props['space_between'];
        $space_between_tablet = ($this->props['space_between_tablet']) ? $this->props['space_between_tablet'] : $space_between;
        $space_between_phone = ($this->props['space_between_phone']) ? $this->props['space_between_phone'] : $space_between_tablet;

        $speed = $this->props['speed'];
        $loop = $this->props['loop'];
        $centered = $this->props['centered'];
        $autoplay = $this->props['autoplay'];
        $autoplay_speed = $this->props['autoplay_speed'];
        $pause_on_hover = $this->props['pause_on_hover'];
        $navigation = $this->props['navigation'];
        $pagination = $this->props['pagination'];
        $effect = $this->props['effect'];
        $rotate = $this->props['rotate'];
        $dynamic_bullets = $this->props['dynamic_bullets'];
        $order_class = self::get_module_order_class($render_slug);
        $order_number = str_replace('_', '', str_replace($this->slug, '', $order_class));
        $slide_shadows = ('on' === $this->props['slide_shadows']) ? esc_attr('true') : esc_attr('false');

        $this->apply_css($render_slug);

        $options = sprintf('
		    data-columnsmobile="%1$s"
		    data-columnstablet="%2$s"
		    data-columnsdesktop="%3$s"
		    data-spacebetween=%4$s
		    data-loop= %5$s
		    data-speed=%6$s
		    data-navigation=%7$s
		    data-pagination=%8$s
		    data-autoplay=%9$s
		    data-autoplayspeed=%10$s
		    data-pauseonhover=%11$s
		    data-effect=%12$s
		    data-rotate=%13$s
		    data-dynamicbullets=%14$s
		    data-ordernumber=%15$s
		    data-centered=%16$s
		    data-spacebetween_tablet=%17$s
		    data-spacebetween_phone=%18$s
		    data-shadow=%19$s',
             esc_attr( $columns_phone ),
             esc_attr( $columns_tablet ),
             esc_attr( $columns_desktop ),
             esc_attr( $space_between),
             esc_attr( $loop),
             esc_attr( $speed),
             esc_attr( $navigation),
             esc_attr( $pagination),
             esc_attr( $autoplay),
             esc_attr( $autoplay_speed),
             esc_attr( $pause_on_hover),
             esc_attr( $effect),
             esc_attr( $rotate),
             esc_attr( $dynamic_bullets),
             esc_attr( $order_number),
             esc_attr( $centered),
             esc_attr( $space_between_tablet),
             esc_attr( $space_between_phone),
             esc_attr( $slide_shadows)
        );

        $data_next_icon = $this->props['navigation_next_icon'];
        $data_prev_icon = $this->props['navigation_prev_icon'];
        $data_next_icon = sprintf('data-icon="%1$s"', esc_attr(et_pb_process_font_icon($data_next_icon)));
        $data_prev_icon = sprintf('data-icon="%1$s"', esc_attr(et_pb_process_font_icon($data_prev_icon)));
        $next_icon = 'on' === $this->props['navigation_next_icon_yn'] ? $data_next_icon : 'data-icon="9"';
        $prev_icon = 'on' === $this->props['navigation_prev_icon_yn'] ? $data_prev_icon : 'data-icon="8"';

        $navigation = ($this->props['navigation'] == 'on') ? sprintf(
            '<div class="swiper-button-next swiper-arrow-button dipi-sbn%1$s" %2$s></div>
		    <div class="swiper-button-prev swiper-arrow-button dipi-sbp%1$s" %3$s></div>',
            $order_number,
            $next_icon,
            $prev_icon
        ) : '';

        $pagination = ($this->props['pagination'] == 'on') ? sprintf(
            '<div class="swiper-pagination dipi-sp%1$s"></div>',
            $order_number
        ) : '';

        $output = sprintf('
		    <div class="dipi-blog-slider-main preloading" %2$s>
		        <div class="swiper-container">
		            <div class="dipi-blog-slider-wrapper">
		                %1$s
		            </div>
		        </div>
		        %3$s
		        <div class="swiper-container-horizontal">
		            %4$s
		        </div>
		    </div>',
            $blog_content,
            $options,
            $navigation,
            $pagination
        );

        return $output;
    }

    public function apply_css($render_slug)
    {

        $this->_dipi_thumbnail_height($render_slug);

        $show_more = $this->props['show_more'];
        $show_comments = $this->props['show_comments'];
        $button_alignment = $this->props['button_alignment'];

        if ($show_more === 'on' && $show_comments === 'on') {

            ET_Builder_Element::set_style($render_slug, array(
                'selector' => '%%order_class%% .dipi-comments',
                'declaration' => 'position: absolute; right: 0; bottom: 0; transform: translate(-50%, -40%);',
            ));

            if ($button_alignment === 'right') {
                ET_Builder_Element::set_style($render_slug, array(
                    'selector' => '%%order_class%% .dipi-bottom-content',
                    'declaration' => 'padding-right: 65px; ',
                ));
            }
        }

        if ($button_alignment === 'left') {

            ET_Builder_Element::set_style($render_slug, array(
                'selector' => '%%order_class%% .dipi-bottom-content',
                'declaration' => 'justify-content: flex-start !important;',
            ));

        } elseif ($button_alignment === 'center') {

            ET_Builder_Element::set_style($render_slug, array(
                'selector' => '%%order_class%% .dipi-bottom-content',
                'declaration' => 'justify-content: center !important;',
            ));

        } else if ($button_alignment === 'right') {
            ET_Builder_Element::set_style($render_slug, array(
                'selector' => '%%order_class%% .dipi-bottom-content',
                'declaration' => 'justify-content: flex-end ;',
            ));

        }

        $slide_shadows = $this->props['slide_shadows'];
        $shadow_overlay_color = $this->props['shadow_overlay_color'];

        if ($slide_shadows == 'on') {
            ET_Builder_Element::set_style($render_slug, array(
                'selector' => '%%order_class%% .dipi-carousel-main .swiper-container-3d .swiper-slide-shadow-left',
                'declaration' => 'background-image: -webkit-gradient(linear, right top, left top, from(' . $shadow_overlay_color . '), to(rgba(0, 0, 0, 0))); background-image: -webkit-linear-gradient(right, ' . $shadow_overlay_color . ', rgba(0, 0, 0, 0)); background-image: -o-linear-gradient(right, ' . $shadow_overlay_color . ', rgba(0, 0, 0, 0)); background-image: linear-gradient(to left, ' . $shadow_overlay_color . ', rgba(0, 0, 0, 0));',
            ));

            ET_Builder_Element::set_style($render_slug, array(
                'selector' => '%%order_class%% .dipi-carousel-main .swiper-container-3d .swiper-slide-shadow-right',
                'declaration' => 'background-image: -webkit-gradient(linear, left top, right top, from(' . $shadow_overlay_color . '), to(rgba(0, 0, 0, 0))); background-image: -webkit-linear-gradient(left, ' . $shadow_overlay_color . ', rgba(0, 0, 0, 0));background-image: -o-linear-gradient(left, ' . $shadow_overlay_color . ', rgba(0, 0, 0, 0)); background-image: linear-gradient(to right, ' . $shadow_overlay_color . ', rgba(0, 0, 0, 0));',
            ));

            ET_Builder_Element::set_style($render_slug, array(
                'selector' => '%%order_class%% .dipi-carousel-main .swiper-container-3d .swiper-slide-shadow-top',
                'declaration' => 'background-image: -webkit-gradient(linear, left bottom, left top, from(' . $shadow_overlay_color . '), to(rgba(0, 0, 0, 0))); background-image: -webkit-linear-gradient(bottom, ' . $shadow_overlay_color . ', rgba(0, 0, 0, 0)); background-image: -o-linear-gradient(bottom, ' . $shadow_overlay_color . ', rgba(0, 0, 0, 0)); background-image: linear-gradient(to top, ' . $shadow_overlay_color . ', rgba(0, 0, 0, 0));',
            ));

            ET_Builder_Element::set_style($render_slug, array(
                'selector' => '%%order_class%% .dipi-carousel-main .swiper-container-3d .swiper-slide-shadow-bottom',
                'declaration' => ' background-image: -webkit-gradient(linear, left top, left bottom, from(' . $shadow_overlay_color . '), to(rgba(0, 0, 0, 0))); background-image: -webkit-linear-gradient(top, ' . $shadow_overlay_color . ', rgba(0, 0, 0, 0)); background-image: -o-linear-gradient(top, ' . $shadow_overlay_color . ', rgba(0, 0, 0, 0));background-image: linear-gradient(to bottom, ' . $shadow_overlay_color . ', rgba(0, 0, 0, 0));',
            ));
        }

        $author_align = $this->props['author_align'];

        if ($author_align == 'left') {
            ET_Builder_Element::set_style($render_slug, [
                'selector' => "%%order_class%% .dipi-blog-post .dipi-author",
                'declaration' => "justify-content: flex-start;",
            ]);
        }

        if ($author_align == 'right') {
            ET_Builder_Element::set_style($render_slug, [
                'selector' => "%%order_class%% .dipi-blog-post .dipi-author",
                'declaration' => "justify-content: flex-end;",
            ]);
        }

        if ($author_align == 'center') {
            ET_Builder_Element::set_style($render_slug, [
                'selector' => "%%order_class%% .dipi-blog-post .dipi-author",
                'declaration' => "justify-content: center;",
            ]);
        }

        $container_class = "%%order_class%% .swiper-container";
        $navigation_position_left_class = "%%order_class%% .swiper-button-prev";
        $navigation_position_right_class = "%%order_class%% .swiper-button-next";

        $important = false;

        $container_padding = explode('|', $this->props['container_padding']);
        $container_padding_tablet = explode('|', $this->props['container_padding_tablet']);
        $container_padding_phone = explode('|', $this->props['container_padding_phone']);
        $container_padding_last_edited = $this->props['container_padding_last_edited'];
        $container_padding_responsive_status = et_pb_get_responsive_status($container_padding_last_edited);

        if ('' !== $container_padding) {
            ET_Builder_Element::set_style($render_slug, array(
                'selector' => $container_class,
                'declaration' => sprintf('padding-top: %1$s !important; padding-right:%2$s !important; padding-bottom:%3$s !important; padding-left:%4$s !important;', $container_padding[0], $container_padding[1], $container_padding[2], $container_padding[3]),
            ));
        }

        if ('' !== $container_padding_tablet && $container_padding_responsive_status) {
            ET_Builder_Element::set_style($render_slug, array(
                'selector' => $container_class,
                'declaration' => sprintf('padding-top: %1$s !important; padding-right:%2$s !important; padding-bottom:%3$s !important; padding-left:%4$s !important;', $container_padding_tablet[0], $container_padding_tablet[1], $container_padding_tablet[2], $container_padding_tablet[3]),
                'media_query' => ET_Builder_Element::get_media_query('max_width_980'),
            ));
        }

        if ('' !== $container_padding_phone && $container_padding_responsive_status) {
            ET_Builder_Element::set_style($render_slug, array(
                'selector' => $container_class,
                'declaration' => sprintf('padding-top: %1$s !important; padding-right:%2$s !important; padding-bottom:%3$s !important; padding-left:%4$s !important;', $container_padding_phone[0], $container_padding_phone[1], $container_padding_phone[2], $container_padding_phone[3]),
                'media_query' => ET_Builder_Element::get_media_query('max_width_767'),
            ));
        }

        $item_padding = explode('|', $this->props['item_padding']);
        $item_padding_tablet = explode('|', $this->props['item_padding_tablet']);
        $item_padding_phone = explode('|', $this->props['item_padding_phone']);
        $item_padding_last_edited = $this->props['item_padding_last_edited'];
        $item_padding_responsive_status = et_pb_get_responsive_status($item_padding_last_edited);

        if ('' !== $item_padding) {
            ET_Builder_Element::set_style($render_slug, array(
                'selector' => "%%order_class%% .dipi-blog-post",
                'declaration' => sprintf('padding-top: %1$s !important; padding-right:%2$s !important; padding-bottom:%3$s !important; padding-left:%4$s !important;', $item_padding[0], $item_padding[1], $item_padding[2], $item_padding[3]),
            ));
        }

        if ('' !== $item_padding_tablet && $item_padding_responsive_status) {
            ET_Builder_Element::set_style($render_slug, array(
                'selector' => "%%order_class%% .dipi-blog-post",
                'declaration' => sprintf('padding-top: %1$s !important; padding-right:%2$s !important; padding-bottom:%3$s !important; padding-left:%4$s !important;', $item_padding_tablet[0], $item_padding_tablet[1], $item_padding_tablet[2], $item_padding_tablet[3]),
                'media_query' => ET_Builder_Element::get_media_query('max_width_980'),
            ));
        }

        if ('' !== $item_padding_phone && $item_padding_responsive_status) {
            ET_Builder_Element::set_style($render_slug, array(
                'selector' => "%%order_class%% .dipi-blog-post",
                'declaration' => sprintf('padding-top: %1$s !important; padding-right:%2$s !important; padding-bottom:%3$s !important; padding-left:%4$s !important;', $item_padding_phone[0], $item_padding_phone[1], $item_padding_phone[2], $item_padding_phone[3]),
                'media_query' => ET_Builder_Element::get_media_query('max_width_767'),
            ));
        }

        $navigation_position_left = $this->props['navigation_position_left'];
        $navigation_position_left_tablet = $this->props['navigation_position_left_tablet'];
        $navigation_position_left_phone = $this->props['navigation_position_left_phone'];
        $navigation_position_left_last_edited = $this->props['navigation_position_left_last_edited'];
        $navigation_position_left_responsive_status = et_pb_get_responsive_status($navigation_position_left_last_edited);

        if ('' !== $navigation_position_left) {
            ET_Builder_Element::set_style($render_slug, array(
                'selector' => $navigation_position_left_class,
                'declaration' => sprintf('left: %1$s !important;', $navigation_position_left),
            ));
        }

        if ('' !== $navigation_position_left_tablet && $navigation_position_left_responsive_status) {
            ET_Builder_Element::set_style($render_slug, array(
                'selector' => $navigation_position_left_class,
                'declaration' => sprintf('left: %1$s !important;', $navigation_position_left_tablet),
                'media_query' => ET_Builder_Element::get_media_query('max_width_980'),
            ));
        }

        if ('' !== $navigation_position_left_phone && $navigation_position_left_responsive_status) {
            ET_Builder_Element::set_style($render_slug, array(
                'selector' => $navigation_position_left_class,
                'declaration' => sprintf('left: %1$s !important;', $navigation_position_left_phone),
                'media_query' => ET_Builder_Element::get_media_query('max_width_767'),
            ));
        }

        $navigation_position_right = $this->props['navigation_position_right'];
        $navigation_position_right_tablet = $this->props['navigation_position_right_tablet'];
        $navigation_position_right_phone = $this->props['navigation_position_right_phone'];
        $navigation_position_right_last_edited = $this->props['navigation_position_right_last_edited'];
        $navigation_position_right_responsive_status = et_pb_get_responsive_status($navigation_position_right_last_edited);

        if ('' !== $navigation_position_right) {
            ET_Builder_Element::set_style($render_slug, array(
                'selector' => $navigation_position_right_class,
                'declaration' => sprintf('right: %1$s !important;', $navigation_position_right),
            ));
        }

        if ('' !== $navigation_position_right_tablet && $navigation_position_right_responsive_status) {
            ET_Builder_Element::set_style($render_slug, array(
                'selector' => $navigation_position_right_class,
                'declaration' => sprintf('right: %1$s !important;', $navigation_position_right_tablet),
                'media_query' => ET_Builder_Element::get_media_query('max_width_980'),
            ));
        }

        if ('' !== $navigation_position_right_phone && $navigation_position_right_responsive_status) {
            ET_Builder_Element::set_style($render_slug, array(
                'selector' => $navigation_position_right_class,
                'declaration' => sprintf('right: %1$s !important;', $navigation_position_right_phone),
                'media_query' => ET_Builder_Element::get_media_query('max_width_767'),
            ));
        }

        if ('' !== $this->props['navigation_color']) {
            ET_Builder_Element::set_style($render_slug, array(
                'selector' => '%%order_class%% .swiper-button-next:after, %%order_class%% .swiper-button-next:before, %%order_class%% .swiper-button-prev:after, %%order_class%% .swiper-button-prev:before',
                'declaration' => sprintf('color: %1$s!important;', $this->props['navigation_color']),
            ));
        }

        if ('' !== $this->props['navigation_bg_color']) {
            ET_Builder_Element::set_style($render_slug, array(
                'selector' => '%%order_class%% .swiper-button-next, %%order_class%% .swiper-button-prev',
                'declaration' => sprintf('background: %1$s!important;', $this->props['navigation_bg_color']),
            ));
        }

        if ('' !== $this->props['navigation_size']) {
            ET_Builder_Element::set_style($render_slug, array(
                'selector' => '%%order_class%% .swiper-button-next, %%order_class%% .swiper-button-prev',
                'declaration' => sprintf(
                    'width: %1$spx !important; height: %1$spx !important;',
                    $this->props['navigation_size']),
            ));
        }

        if ('' !== $this->props['navigation_size']) {
            ET_Builder_Element::set_style($render_slug, array(
                'selector' => '%%order_class%% .swiper-button-next:after, %%order_class%% .swiper-button-next:before, %%order_class%% .swiper-button-prev:after, %%order_class%% .swiper-button-prev:before',
                'declaration' => sprintf('font-size: %1$spx !important;', $this->props['navigation_size']),
            ));
        }

        if ('' !== $this->props['navigation_padding']) {
            ET_Builder_Element::set_style($render_slug, array(
                'selector' => '%%order_class%% .swiper-button-next, %%order_class%% .swiper-button-prev',
                'declaration' => sprintf(
                    'padding: %1$spx !important;',
                    $this->props['navigation_padding']),
            ));
        }

        if ('on' == $this->props['navigation_circle']) {
            ET_Builder_Element::set_style($render_slug, array(
                'selector' => '%%order_class%% .swiper-button-next, %%order_class%% .swiper-button-prev',
                'declaration' => 'border-radius: 50% !important;',
            ));
        }

        if ('' !== $this->props['pagination_color']) {
            ET_Builder_Element::set_style($render_slug, array(
                'selector' => '%%order_class%% .swiper-pagination-bullet',
                'declaration' => sprintf(
                    'background: %1$s!important;', $this->props['pagination_color']),
            ));
        }

        if ('' !== $this->props['pagination_active_color']) {
            ET_Builder_Element::set_style($render_slug, array(
                'selector' => '%%order_class%% .swiper-pagination-bullet.swiper-pagination-bullet-active',
                'declaration' => sprintf(
                    'background: %1$s;', $this->props['pagination_active_color']),
            ));
        }

        if ('' !== $this->props['pagination_position']) {
            ET_Builder_Element::set_style($render_slug, array(
                'selector' => '%%order_class%% .swiper-container-horizontal > .swiper-pagination-bullets, %%order_class%% .swiper-pagination-fraction, %%order_class%% .swiper-pagination-custom',
                'declaration' => sprintf(
                    'bottom: %1$spx;',
                    $this->props['pagination_position']),
            ));
        }

        if ('on' == $this->props['date_circle_icon']) {

            ET_Builder_Element::set_style($render_slug, array(
                'selector' => '%%order_class%% .dipi-date',
                'declaration' => 'border-radius: 100px; ',
            ));

        }

        ET_Builder_Element::set_style($render_slug, array(
            'selector' => '%%order_class%% .dipi-date',
            'declaration' => sprintf(
                'background-color: %1$s !important; ',
                esc_html($this->props['date_circle_color'])
            ),
        ));

        if ('on' == $this->props['date_circle_border']) {
            ET_Builder_Element::set_style($render_slug, array(
                'selector' => '%%order_class%% .dipi-date',
                'declaration' => sprintf(
                    'border-width:3px; border-style:solid; border-color: %1$s !important;',
                    $this->props['date_circle_border']),
            ));
        }

        $date_right_space = $this->props['date_right_space'];
        $date_right_space_tablet = $this->props['date_right_space_tablet'];
        $date_right_space_phone = $this->props['date_right_space_phone'];
        $date_right_space_last_edited = $this->props['date_right_space_last_edited'];
        $date_right_space_responsive_status = et_pb_get_responsive_status($date_right_space_last_edited);

        if ('' !== $date_right_space) {
            ET_Builder_Element::set_style($render_slug, array(
                'selector' => "%%order_class%% .dipi-date",
                'declaration' => sprintf('right: %1$s !important;', $date_right_space),
            ));
        }

        if ('' !== $date_right_space_tablet && $date_right_space_responsive_status) {
            ET_Builder_Element::set_style($render_slug, array(
                'selector' => "%%order_class%% .dipi-date",
                'declaration' => sprintf('right: %1$s !important;', $date_right_space_tablet),
                'media_query' => ET_Builder_Element::get_media_query('max_width_980'),
            ));
        }

        if ('' !== $date_right_space_phone && $date_right_space_responsive_status) {
            ET_Builder_Element::set_style($render_slug, array(
                'selector' => "%%order_class%% .dipi-date",
                'declaration' => sprintf('right: %1$s !important;', $date_right_space_phone),
                'media_query' => ET_Builder_Element::get_media_query('max_width_767'),
            ));
        }

        $date_top_space = $this->props['date_top_space'];
        $date_top_space_tablet = $this->props['date_top_space_tablet'];
        $date_top_space_phone = $this->props['date_top_space_phone'];
        $date_top_space_last_edited = $this->props['date_top_space_last_edited'];
        $date_top_space_responsive_status = et_pb_get_responsive_status($date_top_space_last_edited);

        if ('' !== $date_top_space) {
            ET_Builder_Element::set_style($render_slug, array(
                'selector' => "%%order_class%% .dipi-date",
                'declaration' => sprintf('top: %1$s !important;', $date_top_space),
            ));
        }

        if ('' !== $date_top_space_tablet && $date_top_space_responsive_status) {
            ET_Builder_Element::set_style($render_slug, array(
                'selector' => "%%order_class%% .dipi-date",
                'declaration' => sprintf('top: %1$s !important;', $date_top_space_tablet),
                'media_query' => ET_Builder_Element::get_media_query('max_width_980'),
            ));
        }

        if ('' !== $date_top_space_phone && $date_top_space_responsive_status) {
            ET_Builder_Element::set_style($render_slug, array(
                'selector' => "%%order_class%% .dipi-date",
                'declaration' => sprintf('top: %1$s !important;', $date_top_space_phone),
                'media_query' => ET_Builder_Element::get_media_query('max_width_767'),
            ));
        }

        $overlay_bg_image = [];
        $overlay_bg_style = '';
        $use_overlay_bg_gradient = $this->props["overlay_bg_use_color_gradient"];
        $overlay_bg_type = $this->props["overlay_bg_color_gradient_type"];
        $overlay_bg_direction = $this->props["overlay_bg_color_gradient_direction"];
        $overlay_bg_direction_radial = $this->props["overlay_bg_color_gradient_direction_radial"];
        $overlay_bg_start = $this->props["overlay_bg_color_gradient_start"];
        $overlay_bg_end = $this->props["overlay_bg_color_gradient_end"];
        $overlay_bg_start_position = $this->props["overlay_bg_color_gradient_start_position"];
        $overlay_bg_end_position = $this->props["overlay_bg_color_gradient_end_position"];
        $overlay_bg_overlays_image = $this->props["overlay_bg_color_gradient_overlays_image"];

        if ('on' === $use_overlay_bg_gradient) {

            $overlay_direction = $overlay_bg_type === 'linear' ? $overlay_bg_direction : "circle at $overlay_bg_direction_radial";
            $overlay_start_position = et_sanitize_input_unit($overlay_bg_start_position, false, '%');
            $overlay_end_position = et_sanitize_input_unit($overlay_bg_end_position, false, '%');
            $overlay_gradient_bg = "{$overlay_bg_type}-gradient($overlay_direction, {$overlay_bg_start} {$overlay_start_position},{$overlay_bg_end} {$overlay_end_position})";

            if (!empty($overlay_gradient_bg)) {
                $overlay_bg_image[] = $overlay_gradient_bg;
            }

        }

        if (!empty($overlay_bg_image)) {
            if ('on' !== $overlay_bg_overlays_image) {
                $overlay_bg_image = array_reverse($overlay_bg_image);
            }

            $overlay_bg_style .= sprintf(
                'background-image: %1$s !important;',
                esc_html(join(', ', $overlay_bg_image))
            );
        }

        $overlay_bg_color = $this->props["overlay_bg_color"];
        if ('' !== $overlay_bg_color) {
            $overlay_bg_style .= sprintf(
                'background-color: %1$s !important; ',
                esc_html($overlay_bg_color)
            );
        }

        if ('' !== $overlay_bg_style) {
            ET_Builder_Element::set_style($render_slug, array(
                'selector' => "%%order_class%% .dipi-blog-post-overlay",
                'declaration' => rtrim($overlay_bg_style),
            ));
        }

        // Overlay Hover
        if (et_builder_is_hover_enabled("overlay_bg_color", $this->props)) {

            $ob_image_hover = [];
            $ob_style_hover = '';

            if (isset($this->props["overlay_bg_use_color_gradient__hover"]) && 'on' === $this->props["overlay_bg_use_color_gradient__hover"]) {

                $ob_type_hover = isset($this->props["overlay_bg_color_gradient_type__hover"]) ? $this->props["overlay_bg_color_gradient_type__hover"] : 'linear';
                $ob_direction_hover = isset($this->props["overlay_bg_color_gradient_direction__hover"]) ? $this->props["overlay_bg_color_gradient_direction__hover"] : '180deg';
                $ob_direction_radial_hover = isset($this->props["overlay_bg_color_gradient_direction_radial__hover"]) ? $this->props["overlay_bg_color_gradient_direction_radial__hover"] : 'circle';
                $ob_start_hover = isset($this->props["overlay_bg_color_gradient_start__hover"]) ? $this->props["overlay_bg_color_gradient_start__hover"] : '#2b87da';
                $ob_end_hover = isset($this->props["overlay_bg_color_gradient_end__hover"]) ? $this->props["overlay_bg_color_gradient_end__hover"] : '#29c4a9';
                $ob_start_position_hover = isset($this->props["overlay_bg_color_gradient_start_position__hover"]) ? $this->props["overlay_bg_color_gradient_start_position__hover"] : '0%';
                $ob_end_position_hover = isset($this->props["overlay_bg_color_gradient_end_position__hover"]) ? $this->props["overlay_bg_color_gradient_end_position__hover"] : '100%';
                $ob_overlays_image_hover = isset($this->props["overlay_bg_color_gradient_overlays_image__hover"]) ? $this->props["overlay_bg_color_gradient_overlays_image__hover"] : 'off';

                $overlay_direction_hover = $ob_type_hover === 'linear' ? $ob_direction_hover : "circle at {$ob_direction_radial_hover}";
                $overlay_start_position_hover = et_sanitize_input_unit($ob_start_position_hover, false, '%');
                $overlay_end_position_hover = et_sanitize_input_unit($ob_end_position_hover, false, '%');

                $overlay_gradient_bg_hover = "
					{$ob_type_hover}-gradient($overlay_direction_hover,
					{$ob_start_hover}
					{$overlay_start_position_hover},
					{$ob_end_hover}
					{$overlay_end_position_hover}
				)";

                if (!empty($overlay_gradient_bg_hover)) {

                    $ob_image_hover[] = $overlay_gradient_bg_hover;

                }

            }

            if (!empty($ob_image_hover)) {
                if ('on' !== $ob_overlays_image_hover) {
                    $ob_image_hover = array_reverse($ob_image_hover);
                }

                $ob_style_hover .= sprintf(
                    'background-image: %1$s !important;',
                    esc_html(join(', ', $ob_image_hover))
                );
            }

            $ob_color_hover = $this->props["overlay_bg_color__hover"];

            if ('' !== $ob_color_hover) {
                $ob_style_hover .= sprintf(
                    'background-color: %1$s !important; ',
                    esc_html($ob_color_hover)
                );
            }

            if ('' !== $ob_style_hover) {
                ET_Builder_Element::set_style($render_slug, array(
                    'selector' => '%%order_class%% .dipi-blog-post:hover .dipi-blog-post-overlay',
                    'declaration' => rtrim($ob_style_hover),
                ));
            }

        }

        /**
         * Item Background
         */
        $item_bg_image = [];
        $item_bg_style = '';

        $use_bg_gradient = $this->props["item_bg_use_color_gradient"];
        $bg_type = $this->props["item_bg_color_gradient_type"];
        $bg_direction = $this->props["item_bg_color_gradient_direction"];
        $bg_direction_radial = $this->props["item_bg_color_gradient_direction_radial"];
        $bg_start = $this->props["item_bg_color_gradient_start"];
        $bg_end = $this->props["item_bg_color_gradient_end"];
        $bg_start_position = $this->props["item_bg_color_gradient_start_position"];
        $bg_end_position = $this->props["item_bg_color_gradient_end_position"];
        $bg_overlays_image = $this->props["item_bg_color_gradient_overlays_image"];

        if ('on' === $use_bg_gradient) {
            $direction = $bg_type === 'linear' ? $bg_direction : "circle at { $bg_direction_radial }";
            $start_position = et_sanitize_input_unit($bg_start_position, false, '%');
            $end_position = et_sanitize_input_unit($bg_end_position, false, '%');
            $gradient_bg = "{$bg_type}-gradient( {$direction}, {$bg_start} {$start_position},{$bg_end} {$end_position} )";

            if (!empty($gradient_bg)) {

                $item_bg_image[] = $gradient_bg;

            }

            $has_bg_gradient = true;

        } else {

            $has_bg_gradient = false;

        }

        if (!empty($item_bg_image)) {

            if ('on' !== $bg_overlays_image) {
                $item_bg_image = array_reverse($item_bg_image);
            }

            $item_bg_style .= sprintf('background-image: %1$s !important;', esc_html(join(', ', $item_bg_image)));

        }

        if (!$has_bg_gradient) {

            $item_bg_color = $this->props["item_bg_color"];
            if ('' !== $item_bg_color) {
                $item_bg_style .= sprintf('background-color: %1$s !important; ', esc_html($item_bg_color));
            }

        }

        if ('' !== $item_bg_style) {
            ET_Builder_Element::set_style($render_slug, array(
                'selector' => "%%order_class%% .dipi-blog-post",
                'declaration' => rtrim($item_bg_style),
            ));
        }
    }

    private function _dipi_thumbnail_height($render_slug)
    {
        $thumbnail_height = $this->props['thumbnail_height'];
        $thumbnail_height_tablet = $this->props['thumbnail_height_tablet'] ? $this->props['thumbnail_height_tablet'] : $thumbnail_height;
        $thumbnail_height_phone = $this->props['thumbnail_height_phone'] ? $this->props['thumbnail_height_phone'] : $thumbnail_height_tablet;
        $thumbnail_height_last_edited = $this->props['thumbnail_height_last_edited'];
        $thumbnail_height_responsive_status = et_pb_get_responsive_status($thumbnail_height_last_edited);

        if ('on' === $this->props['use_thumbnail_height']) {
            ET_Builder_Element::set_style($render_slug, [
                'selector' => '%%order_class%% img.wp-post-image',
                'declaration' => "height: {$thumbnail_height} !important;",
            ]);

            if ('' !== $thumbnail_height_tablet && $thumbnail_height_responsive_status) {
                ET_Builder_Element::set_style($render_slug, [
                    'selector' => '%%order_class%% img.wp-post-image',
                    'declaration' => "height: {$thumbnail_height_tablet} !important;",
                    'media_query' => ET_Builder_Element::get_media_query('max_width_980'),
                ]);
            }

            if ('' !== $thumbnail_height_phone && $thumbnail_height_responsive_status) {
                ET_Builder_Element::set_style($render_slug, [
                    'selector' => '%%order_class%% img.wp-post-image',
                    'declaration' => "height: {$thumbnail_height_phone} !important;",
                    'media_query' => ET_Builder_Element::get_media_query('max_width_767'),
                ]);
            }
        }
    }

}

new DIPI_Blog_Slider;
