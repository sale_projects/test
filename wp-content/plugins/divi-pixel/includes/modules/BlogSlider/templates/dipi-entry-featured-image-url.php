<div class="dipi-entry-featured-image-url <?php echo $image_animation_class; ?>">

	<?php if('on' === $args['image_clickable']) : ?>
	<a class="dipi-blog-post-overlay-link" href="<?php echo esc_url(get_permalink()); ?>"></a>
	<?php endif; ?>

    <div class="dipi-blog-post-overlay">
        <?php printf('%1$s', et_core_esc_wp($author));?>
    </div>

    <?php echo $blog_thumbnail; ?>

</div>