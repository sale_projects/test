<<?php echo $header_element; ?> class="dipi-entry-title">
    <a href="<?php esc_url(the_permalink());?>">
        <?php the_title();?>
    </a>
</<?php echo $header_element; ?>>