<div class="dipi-post-content">
    <div class="dipi-categories">
        <?php echo et_core_esc_wp($categories); ?>
    </div>
    <?php include dirname(__FILE__) . '/dipi-entry-title.php';?>
    <?php include dirname(__FILE__) . '/dipi-post-text.php';?>
</div><!-- dipi-post-content -->