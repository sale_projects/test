<?php
$excerpt_length = '' !== $args['excerpt_length'] ? intval($args['excerpt_length']) : 70;
$width = (int) apply_filters('et_pb_blog_image_width', 1080);
$height = (int) apply_filters('et_pb_blog_image_height', 675);
$blog_thumbnail = get_the_post_thumbnail(get_the_ID(), [$width, $height]);

global $authordata;

$author = 'on' === $args['show_author'] ? sprintf(
    '<div class="dipi-author">
        <span class="author vcard">By <img src=" %1$s" /> %2$s</span>
    </div>',
    esc_url(get_avatar_url($authordata->ID)),
    et_pb_get_the_author_posts_link()
) : '';

$date = 'on' === $args['show_date'] ?
sprintf(
    '<div class="dipi-date">
            <span class="dipi-month">%1$s</span>
            <span class="dipi-day">%2$s</span>
            <span class="dipi-year">%3$s</span>
        </div>',
    get_the_date('M'),
    get_the_date('d'),
    get_the_date('Y')
) : '';

$categories = 'on' === $args['show_categories'] ? et_builder_get_the_term_list(', ') : '';

$comment_icon = '<svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="comment" class="svg-inline--fa fa-comment fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M256 32C114.6 32 0 125.1 0 240c0 47.6 19.9 91.2 52.9 126.3C38 405.7 7 439.1 6.5 439.5c-6.6 7-8.4 17.2-4.6 26S14.4 480 24 480c61.5 0 110-25.7 139.1-46.3C192 442.8 223.2 448 256 448c141.4 0 256-93.1 256-208S397.4 32 256 32zm0 368c-26.7 0-53.1-4.1-78.4-12.1l-22.7-7.2-19.5 13.8c-14.3 10.1-33.9 21.4-57.5 29 7.3-12.1 14.4-25.7 19.9-40.2l10.6-28.1-20.6-21.8C69.7 314.1 48 282.2 48 240c0-88.2 93.3-160 208-160s208 71.8 208 160-93.3 160-208 160z"></path></svg>';
$comments = 'on' === $args['show_comments'] ?
et_core_maybe_convert_to_utf_8(
    sprintf(
        '<a href="%3$s" class="dipi-comments"> <span class="comment-icon">%1$s</span> <span>%2$s</span> </a>',
        $comment_icon,
        number_format_i18n(get_comments_number()),
        esc_url(get_the_permalink())
    )
) : '';

$header_element = et_core_esc_previously($processed_header_level);

?>

<div class="dipi-blog-post clearfix">

<?php
if ('on' === $args['show_thumbnail']) {
    include dirname(__FILE__) . '/dipi-entry-featured-image-url.php';
    printf('%1$s', et_core_esc_wp($date));
} else {
    include dirname(__FILE__) . '/dipi-blog-post-meta.php';
}

ET_Builder_Element::clean_internal_modules_styles();

include dirname(__FILE__) . '/dipi-post-content.php';

$button_use_icon = $args['button_use_icon'];
$button_icon     = $args['button_icon'];

$data_icon       = '$';
$data_icon_class = '';

if('on' === $button_use_icon) {
    $data_icon       = $button_icon ? et_pb_process_font_icon($button_icon) : '$';
    $data_icon_class = 'et_pb_custom_button_icon';
}

$more = 'on' === $args['show_more'] ? sprintf(
    '<a href="%1$s" class="et_pb_button %4$s dipi-more-link" data-icon="%3$s">%2$s</a>',
    esc_url(get_permalink()),
    $args['show_more_text'],
    esc_attr($data_icon),
    $data_icon_class
) : '';

// Uses $more and $comments
if(!empty($more) || !empty($comments))
    include dirname(__FILE__) . '/dipi-bottom-content.php';
?>

</div><!-- dipi-blog-post -->