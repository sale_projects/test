<?php

/**
 * TODO:
 * -Pagination
 * -Setting to allow sorting images by title (a-z, z-a, random)
 * -Setting to apply box shadow to images
 */

class DIPI_MasonryGallery extends DIPI_Builder_Module
{

    public $slug = 'dipi_masonry_gallery';
    public $vb_support = 'on';

    protected $module_credits = array(
        'module_uri' => 'https://divi-pixel.com/modules/masonry-gallery',
        'author' => 'Divi Pixel',
        'author_uri' => 'https://divi-pixel.com',
    );
    public function apply_custom_margin_padding($function_name, $slug, $type, $class, $important = true)
    {
        $slug_value = $this->props[$slug];
        $slug_value_tablet = $this->props[$slug . '_tablet'];
        $slug_value_phone = $this->props[$slug . '_phone'];
        $slug_value_last_edited = $this->props[$slug . '_last_edited'];
        $slug_value_responsive_active = et_pb_get_responsive_status($slug_value_last_edited);

        if (isset($slug_value) && !empty($slug_value)) {
            ET_Builder_Element::set_style($function_name, array(
                'selector' => $class,
                'declaration' => et_builder_get_element_style_css($slug_value, $type, $important),
            ));
        }

        if (isset($slug_value_tablet) && !empty($slug_value_tablet) && $slug_value_responsive_active) {
            ET_Builder_Element::set_style($function_name, array(
                'selector' => $class,
                'declaration' => et_builder_get_element_style_css($slug_value_tablet, $type, $important),
                'media_query' => ET_Builder_Element::get_media_query('max_width_980'),
            ));
        }

        if (isset($slug_value_phone) && !empty($slug_value_phone) && $slug_value_responsive_active) {
            ET_Builder_Element::set_style($function_name, array(
                'selector' => $class,
                'declaration' => et_builder_get_element_style_css($slug_value_phone, $type, $important),
                'media_query' => ET_Builder_Element::get_media_query('max_width_767'),
            ));
        }
    }
    public function init()
    {
        $this->icon_path = plugin_dir_path(__FILE__) . "dp-grid.svg";
        $this->name = esc_html__('Pixel Masonry Gallery', 'dipi-divi-pixel');
        $this->settings_modal_toggles = [
            'general' => [
                'toggles' => [
                    'images' => esc_html__('Images', 'dipi-divi-pixel'),
                    'overlay' => esc_html__('Overlay', 'dipi-divi-pixel'),
                    'overlay_animation' => array(
                        'title' => esc_html__('Overlay Animation', 'dipi-divi-pixel'),
                        'tabbed_subtoggles' => true,
                        'sub_toggles' => [
                            'icon' => [
                                'name' => 'Icon',
                            ],
                            'title' => [
                                'name' => 'Title',
                            ],
                            'caption' => [
                                'name' => 'Caption',
                            ]
                        ],
                    ),
                ],
            ],
            'advanced' => [
                'toggles' => [
                    'grid' => esc_html__('Grid', 'dipi-divi-pixel'),
                    'grid_items' => esc_html__('Grid Items', 'dipi-divi-pixel'),
                    'overlay' => esc_html__('Overlay', 'dipi-divi-pixel'),
                    'text_group' => array(
                        'title' => esc_html__('Overlay Text', 'dipi-divi-pixel'),
                        'tabbed_subtoggles' => true,
                        'sub_toggles' => [
                            'title' => [
                                'name' => 'Title',
                            ],
                            'caption' => [
                                'name' => 'Caption',
                            ]
                        ],
                    ),
                ],
            ],
        ];
        $this->custom_css_fields = array(
            'overlay_icon' => array(
                'label' => esc_html__('Overlay Icon', 'dipi-divi-pixel'),
                'selector' => '.dipi-mansonry-gallery-icon',
            ),
            'overlay_title' => array(
                'label' => esc_html__('Overlay Title', 'dipi-divi-pixel'),
                'selector' => '.dipi-mansonry-gallery-title',
            ),
            'overlay_caption' => array(
                'label' => esc_html__('Overlay Caption', 'dipi-divi-pixel'),
                'selector' => '.dipi-mansonry-gallery-caption',
            ),
        );
    }

    public function get_fields()
    {
        $et_accent_color = et_builder_accent_color();

        $fields = [];

        $fields["images"] = [
            'label' => esc_html__('Gallery Images', 'dipi-divi-pixel'),
            'type' => 'upload-gallery',
            'option_category' => 'basic_option',
            'toggle_slug' => 'images',
            'computed_affects'   => array(
                '__gallery',
            ),
        ];

        $fields['gallery_orderby'] = array(
            'label'   => esc_html__( 'Order By', 'dipi-divi-pixel'),
            'type'    => $this->is_loading_bb_data() ? 'hidden' : 'select',
            'options' => array(
                ''     => esc_html__( 'Default', 'dipi-divi-pixel'),
                'rand' => esc_html__( 'Random', 'dipi-divi-pixel'),
            ),
            'default' => 'off',
            'class'   => array( 'et-pb-gallery-ids-field' ),
            'computed_affects' => array(
                '__gallery',
            ),
            'toggle_slug' => 'images',
        );
        $fields["show_lightbox"] = [
            'label' => esc_html__('Show Lightbox', 'dipi-divi-pixel'),
            'type' => 'yes_no_button',
            'option_category' => 'basic_option',
            'default' => 'on',
            'options' => array(
                'off' => esc_html__('No', 'dipi-divi-pixel'),
                'on' => esc_html__('Yes', 'dipi-divi-pixel'),
            ),
            'toggle_slug' => 'images',
            'description' => esc_html__('Whether or not to show lightbox.', 'dipi-divi-pixel'),
            'computed_affects'   => array(
                '__gallery',
            ),
            'mobile_options' => true,
        ];
        $fields["title_in_lightbox"] = [
            'label' => esc_html__('Show Image Title in Lightbox', 'dipi-divi-pixel'),
            'type' => 'yes_no_button',
            'option_category' => 'basic_option',
            'default' => 'off',
            'options' => array(
                'off' => esc_html__('Off', 'dipi-divi-pixel'),
                'on' => esc_html__('On', 'dipi-divi-pixel'),
            ),
            'toggle_slug' => 'images',
            'description' => esc_html__('Whether or not to show the image title in the lightbox. The title is automatically loaded from the media library.', 'dipi-divi-pixel'),
            'computed_affects'   => array(
                '__gallery',
            ),
            'show_if' => [
                'show_lightbox' => 'on',
            ],               
        ];
       
        $fields["caption_in_lightbox"] = [
            'label' => esc_html__('Show Image Caption in Lightbox', 'dipi-divi-pixel'),
            'type' => 'yes_no_button',
            'option_category' => 'basic_option',
            'default' => 'off',
            'options' => array(
                'off' => esc_html__('Off', 'dipi-divi-pixel'),
                'on' => esc_html__('On', 'dipi-divi-pixel'),
            ),
            'toggle_slug' => 'images',
            'description' => esc_html__('Whether or not to show the image caption in the lightbox. The caption is automatically loaded from the media library.', 'dipi-divi-pixel'),
            'computed_affects'   => array(
                '__gallery',
            ),
            'show_if' => [
                'show_lightbox' => 'on',
            ],               
        ];

        $fields["use_overlay"] = [
            'label' => esc_html__('Use Overlay', 'dipi-divi-pixel'),
            'type' => 'yes_no_button',
            'option_category' => 'basic_option',
            'toggle_slug' => 'overlay',
            'options' => array(
                'off' => esc_html__('Off', 'dipi-divi-pixel'),
                'on' => esc_html__('On', 'dipi-divi-pixel'),
            ),
            'computed_affects'   => array(
                '__gallery',
            ),
            'mobile_options' => true,
        ];
        $fields["overlay_align_horizontal"] = [
            'label' => esc_html__('Horizontal Align', 'dipi-divi-pixel'),
            'type' => 'select',
            'option_category' => 'basic_option',
            'default' => 'center',
            'options' => array(
                'flex-start' => esc_html__('Left', 'dipi-divi-pixel'),
                'center' => esc_html__('Center', 'dipi-divi-pixel'),
                'flex-end' => esc_html__('Right', 'dipi-divi-pixel'),
            ),
            'toggle_slug' => 'overlay',
            'show_if' => [
                'use_overlay' => 'on'
            ]
        ];

        $fields["overlay_align_vertical"] = [
            'label' => esc_html__('Vertical Align', 'dipi-divi-pixel'),
            'type' => 'select',
            'option_category' => 'basic_option',
            'default' => 'center',
            'options' => array(
                'flex-start' => esc_html__('Top', 'dipi-divi-pixel'),
                'center' => esc_html__('Center', 'dipi-divi-pixel'),
                'flex-end' => esc_html__('Bottom', 'dipi-divi-pixel'),
            ),
            'toggle_slug' => 'overlay',
            'show_if' => [
                'use_overlay' => 'on',
            ],
        ];
        $fields["icon_in_overlay"] = [
            'label' => esc_html__('Show Icon in Overlay', 'dipi-divi-pixel'),
            'type' => 'yes_no_button',
            'option_category' => 'basic_option',
            'default' => 'on',
            'options' => array(
                'off' => esc_html__('Off', 'dipi-divi-pixel'),
                'on' => esc_html__('On', 'dipi-divi-pixel'),
            ),
            'toggle_slug' => 'overlay',
            'description' => esc_html__('Whether or not to show the Icon in the Overlay. The title is automatically loaded from the media library.', 'dipi-divi-pixel'),
            'computed_affects'   => array(
                '__gallery',
            ),
        ];    
        $fields['hover_icon'] = array(
            'label' => esc_html__('Overlay Icon', 'dipi-divi-pixel'),
            'type'                => 'select_icon',
            'option_category'     => 'configuration',
            'class'               => array( 'et-pb-font-icon' ),
            'option_category' => 'configuration',
            'default' => '',
            'toggle_slug' => 'overlay',
            'show_if' => [
                'icon_in_overlay' => 'on'
            ],
            'computed_affects'   => array(
                '__gallery',
            ),
        );
           
        $fields["title_in_overlay"] = [
            'label' => esc_html__('Show Image Title in Overlay', 'dipi-divi-pixel'),
            'type' => 'yes_no_button',
            'option_category' => 'basic_option',
            'default' => 'off',
            'options' => array(
                'off' => esc_html__('Off', 'dipi-divi-pixel'),
                'on' => esc_html__('On', 'dipi-divi-pixel'),
            ),
            'toggle_slug' => 'overlay',
            'description' => esc_html__('Whether or not to show the image title in the Overlay. The title is automatically loaded from the media library.', 'dipi-divi-pixel'),
            'computed_affects'   => array(
                '__gallery',
            ),
        ];
       
        $fields["caption_in_overlay"] = [
            'label' => esc_html__('Show Image Caption in Overlay', 'dipi-divi-pixel'),
            'type' => 'yes_no_button',
            'option_category' => 'basic_option',
            'default' => 'off',
            'options' => array(
                'off' => esc_html__('Off', 'dipi-divi-pixel'),
                'on' => esc_html__('On', 'dipi-divi-pixel'),
            ),
            'toggle_slug' => 'overlay',
            'description' => esc_html__('Whether or not to show the image caption in the lightbox. The caption is automatically loaded from the media library.', 'dipi-divi-pixel'),
            'computed_affects'   => array(
                '__gallery',
            ),
        ];
        $fields["icon_animation"] = [
            'label'            => esc_html__('Icon Animation', 'dipi-divi-pixel'),
            'type'             => 'select',
            'default'          => 'fadeInUp',
            'options' => [
                'fadeIn'  => esc_html__('Fade In', 'dipi-divi-pixel'),
                'fadeInLeft'  => esc_html__('Fade In Left', 'dipi-divi-pixel'),
                'fadeInRight' => esc_html__('Fade In Right', 'dipi-divi-pixel'),
                'fadeInUp'    => esc_html__('Fade In Up', 'dipi-divi-pixel'),
                'fadeInDown'  => esc_html__('Fade In Down', 'dipi-divi-pixel'),
                'zoomIn'       => esc_html__('Grow', 'dipi-divi-pixel'),
                'bounceIn' => esc_html__('Bounce In', 'dipi-divi-pixel'),
                'bounceInLeft' => esc_html__('Bounce In Left', 'dipi-divi-pixel'),
                'bounceInRight' => esc_html__('Bounce In Right', 'dipi-divi-pixel'),
                'bounceInUp' => esc_html__('Boune In Up', 'dipi-divi-pixel'),
                'bounceInDown' => esc_html__('BouneIn Down', 'dipi-divi-pixel'),
                'flipInX' => esc_html__('FlipInX', 'dipi-divi-pixel'),
                'flipInY' => esc_html__('FlipInY', 'dipi-divi-pixel'),
                'jackInTheBox' => esc_html__('JackInThe Box', 'dipi-divi-pixel'),
                'rotateIn'  => esc_html__('RotateIn', 'dipi-divi-pixel'),
                'rotateInDownLeft' => esc_html__('RotateInDownLeft', 'dipi-divi-pixel'),
                'rotateInUpLeft' => esc_html__('RotateInUpLeft', 'dipi-divi-pixel'),
                'rotateInDownRight' => esc_html__('RotateInDownRight', 'dipi-divi-pixel'),
                'rotateInUpRight' => esc_html__('RotateInUpRight', 'dipi-divi-pixel'),
            ],
            'sub_toggle' => 'title', 
            'computed_affects'   => array(
                '__gallery',
            ),
            'show_if' => [
                'use_overlay' => 'on',
            ],            
            'toggle_slug' => 'overlay_animation',
            'sub_toggle' => 'icon', 
        ];
        $fields["title_animation"] = [
            'label'            => esc_html__('Title Animation', 'dipi-divi-pixel'),
            'type'             => 'select',
            'default'          => 'fadeInUp',
            'options' => [
                'fadeIn'  => esc_html__('Fade In', 'dipi-divi-pixel'),
                'fadeInLeft'  => esc_html__('Fade In Left', 'dipi-divi-pixel'),
                'fadeInRight' => esc_html__('Fade In Right', 'dipi-divi-pixel'),
                'fadeInUp'    => esc_html__('Fade In Up', 'dipi-divi-pixel'),
                'fadeInDown'  => esc_html__('Fade In Down', 'dipi-divi-pixel'),
                'zoomIn'       => esc_html__('Grow', 'dipi-divi-pixel'),
                'bounceIn' => esc_html__('Bounce In', 'dipi-divi-pixel'),
                'bounceInLeft' => esc_html__('Bounce In Left', 'dipi-divi-pixel'),
                'bounceInRight' => esc_html__('Bounce In Right', 'dipi-divi-pixel'),
                'bounceInUp' => esc_html__('Boune In Up', 'dipi-divi-pixel'),
                'bounceInDown' => esc_html__('BouneIn Down', 'dipi-divi-pixel'),
                'flipInX' => esc_html__('FlipInX', 'dipi-divi-pixel'),
                'flipInY' => esc_html__('FlipInY', 'dipi-divi-pixel'),
                'jackInTheBox' => esc_html__('JackInThe Box', 'dipi-divi-pixel'),
                'rotateIn'  => esc_html__('RotateIn', 'dipi-divi-pixel'),
                'rotateInDownLeft' => esc_html__('RotateInDownLeft', 'dipi-divi-pixel'),
                'rotateInUpLeft' => esc_html__('RotateInUpLeft', 'dipi-divi-pixel'),
                'rotateInDownRight' => esc_html__('RotateInDownRight', 'dipi-divi-pixel'),
                'rotateInUpRight' => esc_html__('RotateInUpRight', 'dipi-divi-pixel'),
            ],
            'sub_toggle' => 'title', 
            'computed_affects'   => array(
                '__gallery',
            ),
            'show_if' => [
                'use_overlay' => 'on',
            ],            
            'toggle_slug' => 'overlay_animation',
            'sub_toggle' => 'title', 
        ];
        $fields['icon_delay'] = [
            'label' => esc_html__('Interval Delay', 'dipi-divi-pixel'),
            'type' => 'range',
            'option_category' => 'configuration',
            'default' => '100ms',
            'default_on_front' => '0ms',
            'default_unit' => 'ms',
            'range_settings' => [
                'min' => '0',
                'max' => '3000',
                'step' => '100',
            ],
            'validate_unit' => true,
            'toggle_slug' => 'overlay_animation',
            'sub_toggle' => 'icon', 
            'show_if' => [
                'use_overlay' => 'on',
            ],            
        ];

        $fields['icon_speed'] = [
            'label' => esc_html__('Speed', 'dipi-divi-pixel'),
            'type' => 'range',
            'option_category' => 'configuration',
            'default' => '600ms',
            'default_on_front' => '600ms',
            'default_unit' => 'ms',
            'range_settings' => [
                'min' => '0',
                'max' => '2000',
                'step' => '100',
            ],
            'validate_unit' => true,
            'toggle_slug' => 'overlay_animation',
            'sub_toggle' => 'icon', 
            'show_if' => [
                'use_overlay' => 'on',
            ],            
        ];
        $fields['title_delay'] = [
            'label' => esc_html__('Interval Delay', 'dipi-divi-pixel'),
            'type' => 'range',
            'option_category' => 'configuration',
            'default' => '100ms',
            'default_on_front' => '100ms',
            'default_unit' => 'ms',
            'range_settings' => [
                'min' => '0',
                'max' => '3000',
                'step' => '100',
            ],
            'validate_unit' => true,
            'toggle_slug' => 'overlay_animation',
            'sub_toggle' => 'title', 
            'show_if' => [
                'use_overlay' => 'on',
            ],            
        ];

        $fields['title_speed'] = [
            'label' => esc_html__('Speed', 'dipi-divi-pixel'),
            'type' => 'range',
            'option_category' => 'configuration',
            'default' => '600ms',
            'default_on_front' => '600ms',
            'default_unit' => 'ms',
            'range_settings' => [
                'min' => '0',
                'max' => '2000',
                'step' => '100',
            ],
            'validate_unit' => true,
            'toggle_slug' => 'overlay_animation',
            'sub_toggle' => 'title', 
            'show_if' => [
                'use_overlay' => 'on',
            ],            
        ];
        $fields["caption_animation"] = [
            'label'            => esc_html__('Caption Animation', 'dipi-divi-pixel'),
            'type'             => 'select',
            'default'          => 'fadeInUp',
            'options' => [
                'fadeIn'  => esc_html__('Fade In', 'dipi-divi-pixel'),
                'fadeInLeft'  => esc_html__('Fade In Left', 'dipi-divi-pixel'),
                'fadeInRight' => esc_html__('Fade In Right', 'dipi-divi-pixel'),
                'fadeInUp'    => esc_html__('Fade In Up', 'dipi-divi-pixel'),
                'fadeInDown'  => esc_html__('Fade In Down', 'dipi-divi-pixel'),
                'zoomIn'       => esc_html__('Grow', 'dipi-divi-pixel'),
                'bounceIn' => esc_html__('Bounce In', 'dipi-divi-pixel'),
                'bounceInLeft' => esc_html__('Bounce In Left', 'dipi-divi-pixel'),
                'bounceInRight' => esc_html__('Bounce In Right', 'dipi-divi-pixel'),
                'bounceInUp' => esc_html__('Boune In Up', 'dipi-divi-pixel'),
                'bounceInDown' => esc_html__('BouneIn Down', 'dipi-divi-pixel'),
                'flipInX' => esc_html__('FlipInX', 'dipi-divi-pixel'),
                'flipInY' => esc_html__('FlipInY', 'dipi-divi-pixel'),
                'jackInTheBox' => esc_html__('JackInThe Box', 'dipi-divi-pixel'),
                'rotateIn'  => esc_html__('RotateIn', 'dipi-divi-pixel'),
                'rotateInDownLeft' => esc_html__('RotateInDownLeft', 'dipi-divi-pixel'),
                'rotateInUpLeft' => esc_html__('RotateInUpLeft', 'dipi-divi-pixel'),
                'rotateInDownRight' => esc_html__('RotateInDownRight', 'dipi-divi-pixel'),
                'rotateInUpRight' => esc_html__('RotateInUpRight', 'dipi-divi-pixel'),
            ],
            'toggle_slug' => 'overlay_animation',
            'sub_toggle' => 'caption', 
            'computed_affects'   => array(
                '__gallery',
            ),
            'show_if' => [
                'use_overlay' => 'on',
            ],            
        ];
        $fields['caption_delay'] = [
            'label' => esc_html__('Interval Delay', 'dipi-divi-pixel'),
            'type' => 'range',
            'option_category' => 'configuration',
            'default' => '400ms',
            'default_on_front' => '400ms',
            'default_unit' => 'ms',
            'range_settings' => [
                'min' => '0',
                'max' => '3000',
                'step' => '100',
            ],
            'validate_unit' => true,
            'toggle_slug' => 'overlay_animation',
            'sub_toggle' => 'caption', 
            'show_if' => [
                'use_overlay' => 'on',
            ],            
        ];

        $fields['caption_speed'] = [
            'label' => esc_html__('Speed', 'dipi-divi-pixel'),
            'type' => 'range',
            'option_category' => 'configuration',
            'default' => '600ms',
            'default_on_front' => '600ms',
            'default_unit' => 'ms',
            'range_settings' => [
                'min' => '0',
                'max' => '2000',
                'step' => '100',
            ],
            'validate_unit' => true,
            'toggle_slug' => 'overlay_animation',
            'sub_toggle' => 'caption', 
            'show_if' => [
                'use_overlay' => 'on',
            ],            
        ];
        $fields["columns"] = [
            'label' => esc_html__('Columns', 'dipi-divi-pixel'),
            'type' => 'range',
            'option_category' => 'basic_option',
            'toggle_slug' => 'grid',
            'tab_slug' => 'advanced',
            'default' => '4',
            'range_settings' => array(
                'min' => '1',
                'max' => '10',
                'step' => '1',
            ),
            'mobile_options' => true,
            'responsive' => true,
            'unitless' => true,
            'computed_affects'   => array(
                '__gallery',
            ),
        ];

        $fields["gutter"] = [
            'label' => esc_html__('Gutter', 'dipi-divi-pixel'),
            'type' => 'range',
            'option_category' => 'layout',
            'toggle_slug' => 'grid',
            'tab_slug' => 'advanced',
            'default' => '10',
            'range_settings' => array(
                'min' => '0',
                'max' => '100',
                'step' => '1',
            ),
            'mobile_options' => true,
            'responsive' => true,
            'unitless' => true,
            'computed_affects'   => array(
                '__gallery',
            ),
        ];

        $fields["show_overflow"] = [
            'label' => esc_html__('Show Overflow', 'dipi-divi-pixel'),
            'description' => esc_html__('Hide or show the overflow of the module. Useful if you want to use box shadows on the images but be aware that too much gutter can cause weird effects on mobiles due to the extra margin of the module. In this case, you should set the overflow of the row or section to hidden.', 'dipi-divi-pixel'),
            'type' => 'yes_no_button',
            'option_category' => 'layout',
            'toggle_slug' => 'grid',
            'tab_slug' => 'advanced',
            'default' => 'off',
            'options' => array(
                'off' => esc_html__('Off', 'dipi-divi-pixel'),
                'on' => esc_html__('On', 'dipi-divi-pixel'),
            ),
        ];

        $fields["overlay_bg_color"] = [
            'label' => esc_html__('Overlay Color', 'dipi-divi-pixel'),
            'type' => 'background-field',
            'base_name' => "overlay_bg",
            'context' => "overlay_bg",
            'custom_color' => true,
            'default' => 'rgba(21,2,42,0.3)',
            'tab_slug' => 'advanced',
            'toggle_slug' => 'overlay',
            'background_fields' => array_merge(

                ET_Builder_Element::generate_background_options(
                    'overlay_bg',
                    'gradient',
                    "advanced",
                    "overlay",
                    "overlay_bg_gradient"
                ),

                ET_Builder_Element::generate_background_options(
                    "overlay_bg",
                    "color",
                    "advanced",
                    "overlay",
                    "overlay_bg_color"
                )
            ),
            'show_if' => [
                'use_overlay' => 'on',
            ],
        ];

        $fields['overlay_icon_color'] = [
            'label' => esc_html__(' Overlay Icon Color', 'dipi-divi-pixel'),
            'type' => 'color-alpha',
            'custom_color' => true,
            'tab_slug' => 'advanced',
            'default' => '',
            'show_if' => [
                'use_overlay' => 'on',
            ],
            'toggle_slug' => 'overlay',
            'description' => esc_html__('Color of the overlay icon. The overlay icon is centered horizontally and vertically over the image.', 'dipi-divi-pixel'),
        ];
        $fields['overlay_icon_use_circle'] = [
            'label' => esc_html__('Circle Icon', 'dipi-divi-pixel'),
            'type' => 'yes_no_button',
            'option_category' => 'configuration',
            'options' => [
                'off' => esc_html__('No', 'dipi-divi-pixel'),
                'on' => esc_html__('Yes', 'dipi-divi-pixel'),
            ],
            'tab_slug' => 'advanced',
            'toggle_slug' => 'overlay',
            'description' => esc_html__('Here you can choose whether icon set above should display within a circle.', 'dipi-divi-pixel'),
            'default' => 'off',
            'show_if' => [
                'use_overlay' => 'on',
            ],
            'computed_affects'   => array(
                '__gallery',
            ),
        ];
        $fields['overlay_icon_circle_color'] = [
            'default' => $et_accent_color,
            'label' => esc_html__('Circle Color', 'dipi-divi-pixel'),
            'type' => 'color-alpha',
            'description' => esc_html__('Here you can define a custom color for the icon circle.', 'dipi-divi-pixel'),
            'show_if' => [
                'use_overlay' => 'on',
                'overlay_icon_use_circle' => 'on',
            ],
            'tab_slug' => 'advanced',
            'toggle_slug' => 'overlay',
            'hover' => 'tabs',
            'sticky' => true,
            'default' => '#F2F3F3',
        ];
        $fields['overlay_icon_use_circle_border'] = [
            'label' => esc_html__('Show Circle Border', 'dipi-divi-pixel'),
            'type' => 'yes_no_button',
            'option_category' => 'layout',
            'options' => [
                'off' => esc_html__('No', 'dipi-divi-pixel'),
                'on' => esc_html__('Yes', 'dipi-divi-pixel'),
            ],
            'description' => esc_html__('Here you can choose whether if the icon circle border should display.', 'dipi-divi-pixel'),
            'show_if' => [
                'use_overlay' => 'on',
                'overlay_icon_use_circle' => 'on',
            ],
            'tab_slug' => 'advanced',
            'toggle_slug' => 'overlay',
            'default_on_front' => 'off',
            'computed_affects'   => array(
                '__gallery',
            ),
        ];
        $fields['overlay_icon_circle_border_color'] = [
            'default' => $et_accent_color,
            'label' => esc_html__('Circle Border Color', 'dipi-divi-pixel'),
            'type' => 'color-alpha',
            'description' => esc_html__('Here you can define a custom color for the icon circle border.', 'dipi-divi-pixel'),
            'show_if' => array(
                'use_overlay' => 'on',
                'overlay_icon_use_circle_border' => 'on',
            ),
            'tab_slug' => 'advanced',
            'toggle_slug' => 'overlay',
            'hover' => 'tabs',
            'sticky' => true,
            'default' => '#000',
        ];
        $fields['overlay_icon_use_icon_font_size'] = [
            'label' => esc_html__('Use Icon Font Size', 'dipi-divi-pixel'),
            'description' => esc_html__('If you would like to control the size of the icon, you must first enable this option.', 'dipi-divi-pixel'),
            'type' => 'yes_no_button',
            'option_category' => 'font_option',
            'options' => [
                'off' => esc_html__('No', 'dipi-divi-pixel'),
                'on' => esc_html__('Yes', 'dipi-divi-pixel'),
            ],
            'show_if' => [
                'use_overlay' => 'on',
            ],
            'tab_slug' => 'advanced',
            'toggle_slug' => 'overlay',
            'default_on_front' => 'off',
        ];
        $fields['overlay_icon_font_size'] = [
            'label' => esc_html__('Icon Font Size', 'dipi-divi-pixel'),
            'description' => esc_html__('Control the size of the icon by increasing or decreasing the font size.', 'dipi-divi-pixel'),
            'type' => 'range',
            'option_category' => 'font_option',
            'tab_slug' => 'advanced',
            'toggle_slug' => 'overlay',
            'default' => '96px',
            'default_unit' => 'px',
            'default_on_front' => '',
            'allowed_units' => array('%', 'em', 'rem', 'px', 'cm', 'mm', 'in', 'pt', 'pc', 'ex', 'vh', 'vw'),
            'range_settings' => array(
                'min' => '1',
                'max' => '120',
                'step' => '1',
            ),
            'show_if' => [
                'use_overlay' => 'on',
                'overlay_icon_use_icon_font_size' => 'on',
            ],
            'mobile_options' => true,
            'sticky' => true,
            'responsive' => true,
            'hover' => 'tabs',
        ];
        $fields["__gallery"] = [
            'type' => 'computed',
            'computed_callback' => array('DIPI_MasonryGallery', 'render_images'),
            'computed_depends_on' => array(
                'images',
                'show_lightbox',
                'title_in_lightbox',
                'caption_in_lightbox',
                'icon_in_overlay',
                'title_in_overlay',
                'caption_in_overlay',
                'overlay_icon_use_circle',
                'overlay_icon_use_circle_border',
                'gallery_orderby',
                'hover_icon',
                'use_overlay',
                'icon_animation',
                'title_animation',
                'caption_animation'
            ),
            'computed_minimum' => array(
                'images',
            ),
        ];


        $fields['use_thumbnails'] = [
            'label' => esc_html__('Use Responsive Thumbnails', 'dipi-divi-pixel'),
            'description' => esc_html__('Whether or not to use custom sized thumbnails on different devices. If this option is disabled, the full size image will be used as thumbnail.', 'dipi-divi-pixel'),
            'type' => 'yes_no_button',
            'option_category' => 'basic_option',
            'toggle_slug' => 'images',
            'default' => 'off',
            'options' => array(
                'off' => esc_html__('Off', 'dipi-divi-pixel'),
                'on' => esc_html__('On', 'dipi-divi-pixel'),
            ),
        ];

        $fields['image_size_desktop'] = [
            'label' => esc_html__('Image Size (Desktop)', 'dipi-divi-pixel'),
            'type' => 'select',
            'option_category' => 'basic_option',
            'default' => 'full',
            'options' => $this->dipi_get_image_sizes(),
            'toggle_slug' => 'images',
            'description' => 'Here you can choose the image size to use. If you are using very large images, consider using a thumbnail size to speed up page loading time.',
            'show_if' => [
                'use_thumbnails' => 'on',
            ],
        ];

        $fields['image_size_tablet'] = [
            'label' => esc_html__('Image Size (Tablet)', 'dipi-divi-pixel'),
            'type' => 'select',
            'option_category' => 'basic_option',
            'default' => 'full',
            'options' => $this->dipi_get_image_sizes(),
            'toggle_slug' => 'images',
            'description' => 'Here you can choose the image size to use. If you are using very large images, consider using a thumbnail size to speed up page loading time.',
            'show_if' => [
                'use_thumbnails' => 'on',
            ],
        ];

        $fields['image_size_phone'] = [
            'label' => esc_html__('Image Size (Phone)', 'dipi-divi-pixel'),
            'type' => 'select',
            'option_category' => 'basic_option',
            'default' => 'full',
            'options' => $this->dipi_get_image_sizes(),
            'toggle_slug' => 'images',
            'description' => 'Here you can choose the image size to use. If you are using very large images, consider using a thumbnail size to speed up page loading time.',
            'show_if' => [
                'use_thumbnails' => 'on',
            ],
        ];

        $fields['fix_lazy'] = [
            'label' => esc_html__('Fix Lazy Loading Images', 'ds-suit-material'),
            'description' => esc_html__('Whether or not to use apply a fix for lazy loading images. Only activate this setting, if you encounter issues with the gallery in combination with lazy loading images plugins like Jetpack.', 'ds-suit'),
            'type' => 'yes_no_button',
            'option_category' => 'basic_option',
            'toggle_slug' => 'grid',
            'default' => 'off',
            'options' => array(
                'off' => esc_html__('Off', 'ds-suit-material'),
                'on' => esc_html__('On', 'ds-suit-material'),
            ),
        ];
        $fields['overlay_padding'] = [
            'label' => __('Overlay Padding', 'et_builder'),
            'type' => 'custom_margin',
            'description' => __('Set Padding of Overlay.', 'et_builder'),
            'tab_slug' => 'advanced',
            'toggle_slug' => 'margin_padding',
            'default' => '30px|30px|30px|30px',
            'mobile_options' => true,
        ];
        $fields = array_merge(
            $fields,
            $this->generate_background_options(
                "overlay_bg",
                'skip',
                "advanced",
                "image",
                "overlay_bg_gradient"
            )
        );
        $fields = array_merge(
            $fields,
            $this->generate_background_options(
                "overlay_bg",
                'skip',
                "advanced",
                "image",
                "overlay_bg_color"
            )
        );
        return $fields;
    }

    public function get_advanced_fields_config()
    {

        $advanced_fields = [];

        $advanced_fields["text"] = false;
        $advanced_fields["text_shadow"] = false;
        $advanced_fields["fonts"]["header"] = [
            'label' => esc_html__('Title', 'dipi-divi-pixel'),
            'css' => [
                'main' => "%%order_class%% .dipi-mansonry-gallery-title",
                'hover' => "%%order_class%%:hover .dipi-mansonry-gallery-title",
            ],
            'header_level' => [
                'default' => 'h4',
            ],
            'hide_text_align' => true,
            'toggle_slug' => 'text_group',
            'sub_toggle' => 'title', 
        ];
        $advanced_fields["fonts"]["caption"] = [
            'label' => esc_html__('Caption', 'dipi-divi-pixel'),
            'css' => [
                'main' => "%%order_class%% .dipi-mansonry-gallery-caption",
                'hover' => "%%order_class%%:hover .dipi-mansonry-gallery-caption",
            ],
            'hide_text_align' => true,
            'toggle_slug' => 'text_group',
            'sub_toggle' => 'caption', 
        ];

        $advanced_fields["borders"]["default"] = [
            'css' => [
                'main' => [
                    'border_radii' => "%%order_class%%",
                    'border_styles' => "%%order_class%%",
                ],
            ],
        ];

        $advanced_fields["borders"]["grid_item"] = [
            'label_prefix' => esc_html__('Grid Item', 'dipi-divi-pixel'),
            'toggle_slug' => 'grid_items',
            'tab_slug' => 'advanced',
            'css' => [
                'main' => [
                    'border_radii' => "%%order_class%% .grid .grid-item.et_pb_gallery_image",
                    'border_styles' => "%%order_class%% .grid .grid-item.et_pb_gallery_image",
                ],
            ],
        ];

        $advanced_fields["box_shadow"]["images"] = [
            'label' => esc_html__('Grid Item Box Shadow', 'dipi-divi-pixel'),
            'toggle_slug' => 'grid_items',
            'tab_slug' => 'advanced',
            'css' => [
                'main' => "%%order_class%% .grid .grid-item.et_pb_gallery_image",
            ],
        ];

        $advanced_fields['margin_padding'] = [
            'css' => [
                'important' => 'all',
            ],
        ];

        return $advanced_fields;
    }

    public function dipi_get_image_sizes()
    {
        global $_wp_additional_image_sizes;
        $sizes = array();
        $get_intermediate_image_sizes = get_intermediate_image_sizes();
        foreach ($get_intermediate_image_sizes as $_size) {
            if (in_array($_size, array('thumbnail', 'medium', 'large'))) {
                $sizes[$_size]['width'] = get_option($_size . '_size_w');
                $sizes[$_size]['height'] = get_option($_size . '_size_h');
                $sizes[$_size]['crop'] = (bool) get_option($_size . '_crop');
            } elseif (isset($_wp_additional_image_sizes[$_size])) {
                $sizes[$_size] = array(
                    'width' => $_wp_additional_image_sizes[$_size]['width'],
                    'height' => $_wp_additional_image_sizes[$_size]['height'],
                    'crop' => $_wp_additional_image_sizes[$_size]['crop'],
                );
            }
        }

        $image_sizes = array(
            'full' => esc_html__('Full Size', 'dipi-divi-pixel'),
        );
        foreach ($sizes as $sizeKey => $sizeValue) {
            $image_sizes[$sizeKey] = sprintf(
                '%1$s (%2$s x %3$s,%4$s cropped)',
                $sizeKey,
                $sizeValue["width"],
                $sizeValue["height"],
                ($sizeValue["crop"] == false ? ' not' : '')

            );
        }

        return $image_sizes;
    }

    private static function get_attachment_image($attachment_id, $image_size, $fallback_url)
    {
        $attachment = wp_get_attachment_image_src($attachment_id, $image_size);
        if ($attachment) {
            return $attachment[0];
        } else {
            return $fallback_url;
        }
    }

    static function render_images($args = array(), $conditional_tags = array(), $current_page = array())
    {
        $defaults = [
            'images' => '',
            'gallery_orderby' => '',
            'title_in_lightbox' => 'off',
            'caption_in_lightbox' => 'off',
            'icon_in_overlay'    => 'on',
            'title_in_overlay'  => 'off',
            'caption_in_overlay'    => 'off',
            'use_overlay' => 'off',
            'hover_icon' => '',
            'image_size_desktop' => 'full',
            'image_size_tablet' => 'full',
            'image_size_phone' => 'full',
        ];

        $args = wp_parse_args($args, $defaults);
        

        $icon_animation = $args['icon_animation'];
        $title_animation = $args['title_animation'];
        $caption_animation = $args['caption_animation'];

        $items = [
            '<div class="grid-sizer"></div>',
            '<div class="gutter-sizer"></div>',
        ];


        $attachment_ids = explode(",", $args["images"]);

        if('rand' === $args['gallery_orderby']) {
            // echo "every day I'm shuffling";
            shuffle($attachment_ids);
        } else {
            // echo "no shuffle today";
        }

        $overlay_output = '';
		
        $overlay_icon_classes[] = 'dipi-mansonry-gallery-icon';
        $overlay_icon_use_circle = $args['overlay_icon_use_circle'];
        $overlay_icon_use_circle_border = $args['overlay_icon_use_circle_border'];
        if ('on' === $overlay_icon_use_circle) {
            $overlay_icon_classes[] = 'dipi-mansonry-gallery-icon-circle';
        }

        if ('on' === $overlay_icon_use_circle && 'on' === $overlay_icon_use_circle_border) {
            $overlay_icon_classes[] = 'dipi-mansonry-gallery-icon-circle-border';
        }
        $data_icon = '' !== $args['hover_icon']
        ? sprintf(
            ' data-icon="%1$s"',
            esc_attr( et_pb_process_font_icon( $args['hover_icon'] ) ),
            esc_attr($args['hover_icon'])
        )
        : 'data-no-icon';        
        //Check which image sizes to use
        
        foreach ($attachment_ids as $attachment_id) {
            $attachment = wp_get_attachment_image_src($attachment_id, "full");
            if(!$attachment){
                continue;
            }

            $image = $attachment[0];
            $image_desktop_url = DIPI_MasonryGallery::get_attachment_image($attachment_id, $args['image_size_desktop'], $image);
            $image_tablet_url = DIPI_MasonryGallery::get_attachment_image($attachment_id, $args['image_size_tablet'], $image);
            $image_phone_url = DIPI_MasonryGallery::get_attachment_image($attachment_id, $args['image_size_phone'], $image);

            $image_alt = get_post_meta($attachment_id, '_wp_attachment_image_alt', true);
            $image_title = get_the_title($attachment_id);

            $icon_html = '';
            if ('on' === $args["icon_in_overlay"]) {
                $icon_html = sprintf(
                    '<div class="et-pb-icon %1$s %3$s animated %4$s"%2$s></div>',
                    ( '' !== $args['hover_icon'] ? ' et_pb_inline_icon' : '' ),
                    'on' === $args["icon_in_overlay"] ? $data_icon : '',
                    implode(' ', $overlay_icon_classes),
                    $icon_animation
                );
            }

            $title_html = '';
            if ('on' === $args["title_in_overlay"] && '' !== $image_title) {
                $title_html = sprintf(
                    '<h4 class="dipi-mansonry-gallery-title animated %2$s">
                        %1$s
                    </h4>',
                    $image_title,
                    $title_animation
                );
            }

            $caption = wp_get_attachment_caption($attachment_id);
            $caption_html = '';
            if ( 'on' === $args["caption_in_overlay"] && '' !== $caption) {
                $caption_html = sprintf(
                    '<div class="dipi-mansonry-gallery-caption animated %2$s">
                        %1$s
                    </div>',
                    $caption,
                    $caption_animation
                );
            }

            $overlay_output = sprintf(
                '<span class="dipi_masonry_gallery_overlay background">
                </span>
                <span class="dipi_masonry_gallery_overlay content" style="transition-duration: 0ms;">
                    %1$s
                    %2$s
                    %3$s
                </span>',
                $icon_html,
                $title_html,
                $caption_html
            );
            $items[] = sprintf(
                '<div class="grid-item et_pb_gallery_image">
                    <div class="img-container" href="%1$s"%4$s%5$s>
                        <img src="%1$s" 
                             alt="%2$s" 
                             srcset="%9$s 768w, %8$s 980w, %7$s 1024w"
                             sizes="(max-width: 768px) 768px, (max-width: 980px) 980px, 1024px" />
                        %6$s
                    </div>
                </div>',
                $image,
                $image_alt,
                $image_title,
                'on' === $args["title_in_lightbox"] ?  " data-title='$image_title'" : '',
                'on' === $args["caption_in_lightbox"] ?  " data-caption='" . wp_get_attachment_caption($attachment_id) . "'" : '', #5
                et_core_esc_previously($overlay_output),
                $image_desktop_url,
                $image_tablet_url,
                $image_phone_url                
            );
        }
        return implode("", $items);
    }

    public function render($attrs, $content = null, $render_slug)
    {
        wp_enqueue_script('dipi_masonry_gallery');
        wp_enqueue_style('dipi_animate');
        $this->dipi_apply_css($render_slug);
        $use_overlay               = $this->props['use_overlay'];
        $use_overlay_values        = et_pb_responsive_options()->get_property_values( $this->props, 'use_overlay' );
        $use_overlay_tablet        = isset( $use_overlay_values['tablet'] ) ? $use_overlay_values['tablet'] : '';
        $use_overlay_phone         = isset( $use_overlay_values['phone'] ) ? $use_overlay_values['phone'] : '';

        $show_overlay_classes = ($use_overlay === 'on') ? 'show_overlay' : 'hide_overlay';
        if (!empty($use_overlay_tablet)) {
            $show_overlay_classes .= ($use_overlay_tablet === 'on') ? ' show_overlay_tablet' : ' hide_overlay_tablet';
        }
        if (!empty($use_overlay_phone)) {
            $show_overlay_classes .= ($use_overlay_phone === 'on') ? ' show_overlay_phone' : ' hide_overlay_phone';
        }


        $show_lightbox               = $this->props['show_lightbox'];
        $show_lightbox_values        = et_pb_responsive_options()->get_property_values( $this->props, 'show_lightbox' );
        
        $show_lightbox_tablet        = isset( $show_lightbox_values['tablet'] ) && !empty( $show_lightbox_values['tablet'] )? $show_lightbox_values['tablet'] : $show_lightbox;
        $show_lightbox_phone         = isset( $show_lightbox_values['phone'] ) && !empty( $show_lightbox_values['phone'] )? $show_lightbox_values['phone'] : $show_lightbox_tablet;

        $show_lightboxclasses = ($show_lightbox === 'on') ? 'show_lightbox' : 'hide_lightbox';
        if (!empty($show_lightbox_tablet)) {
            $show_lightboxclasses .= ($show_lightbox_tablet === 'on') ? ' show_lightbox_tablet' : ' hide_lightbox_tablet';
        }
        if (!empty($show_lightbox_phone)) {
            $show_lightboxclasses .= ($show_lightbox_phone === 'on') ? ' show_lightbox_phone' : ' hide_lightbox_phone';
        }
        
        // $render_options = [
        //     'images' => $this->props["images"],
        //     'gallery_orderby' => $this->props["gallery_orderby"],
        //     'title_in_lightbox' => $this->props["title_in_lightbox"],
        //     'caption_in_lightbox' => $this->props["caption_in_lightbox"],
        //     'hover_icon' => $this->props["hover_icon"],
        //     'use_overlay' => $this->props["use_overlay"],
        // ];

        // if('on' === $this->props['use_thumbnails']) {
        //     $render_options['image_size_desktop'] = $this->props['image_size_desktop'];
        //     $render_options['image_size_tablet'] = $this->props['image_size_tablet'];
        //     $render_options['image_size_phone'] = $this->props['image_size_phone'];
        // }

        $items = DIPI_MasonryGallery::render_images($this->props);

        return sprintf(
            '<div class="grid %3$s %4$s" data-lazy="%2$s">
                %1$s
             </div>',
            $items,
            $this->props["fix_lazy"] === 'on' ? esc_attr("true") : esc_attr("false"),
            $show_lightboxclasses,
            $show_overlay_classes
        );
    }

    public function dipi_apply_css($render_slug)
    {

        $columns = $this->props["columns"];
        $columns_responsive_active = isset($this->props["columns_last_edited"]) && et_pb_get_responsive_status($this->props["columns_last_edited"]);
        $columns_tablet = $columns_responsive_active && $this->props["columns_tablet"] ? $this->props["columns_tablet"] : $columns;
        $columns_phone = $columns_responsive_active && $this->props["columns_phone"] ? $this->props["columns_phone"] : $columns_tablet;

        $gutter = $this->props["gutter"];
        $gutter_responsive_active = isset($this->props["gutter_last_edited"]) && et_pb_get_responsive_status($this->props["gutter_last_edited"]);
        $gutter_tablet = $gutter_responsive_active && $this->props["gutter_tablet"] ? $this->props["gutter_tablet"] : $gutter;
        $gutter_phone = $gutter_responsive_active && $this->props["gutter_phone"] ? $this->props["gutter_phone"] : $gutter_tablet;
        
        //Width of grid items
        ET_Builder_Element::set_style($render_slug, [
            'selector' => '%%order_class%% .grid-sizer, %%order_class%% .grid-item',
            'declaration' => "width: calc((100% - ({$columns} - 1) * {$gutter}px) / {$columns});",
        ]);

        ET_Builder_Element::set_style($render_slug, [
            'selector' => '%%order_class%% .grid-sizer, %%order_class%% .grid-item',
            'declaration' => "width: calc((100% - ({$columns_tablet} - 1) * {$gutter_tablet}px) / {$columns_tablet});",
            'media_query' => ET_Builder_Element::get_media_query('max_width_980'),
        ]);

        ET_Builder_Element::set_style($render_slug, [
            'selector' => '%%order_class%% .grid-sizer, %%order_class%% .grid-item',
            'declaration' => "width: calc((100% - ({$columns_phone} - 1) * {$gutter_phone}px) / {$columns_phone});",
            'media_query' => ET_Builder_Element::get_media_query('max_width_767'),
        ]);

        //Gutter of grid items
        ET_Builder_Element::set_style($render_slug, [
            'selector' => '%%order_class%% .grid-item',
            'declaration' => "margin-bottom: {$gutter}px;",
        ]);

        ET_Builder_Element::set_style($render_slug, [
            'selector' => '%%order_class%% .grid-item',
            'declaration' => "margin-bottom: {$gutter_tablet}px;",
            'media_query' => ET_Builder_Element::get_media_query('max_width_980'),
        ]);

        ET_Builder_Element::set_style($render_slug, [
            'selector' => '%%order_class%% .grid-item',
            'declaration' => "margin-bottom: {$gutter_phone}px;",
            'media_query' => ET_Builder_Element::get_media_query('max_width_767'),
        ]);

        ET_Builder_Element::set_style($render_slug, [
            'selector' => '%%order_class%% .gutter-sizer',
            'declaration' => "width: {$gutter}px;",
        ]);

        ET_Builder_Element::set_style($render_slug, [
            'selector' => '%%order_class%% .gutter-sizer',
            'declaration' => "width: {$gutter_tablet}px;",
            'media_query' => ET_Builder_Element::get_media_query('max_width_980'),
        ]);

        ET_Builder_Element::set_style($render_slug, [
            'selector' => '%%order_class%% .gutter-sizer',
            'declaration' => "width: {$gutter_phone}px;",
            'media_query' => ET_Builder_Element::get_media_query('max_width_767'),
        ]);

        //Remove gutter from outer grid
        ET_Builder_Element::set_style($render_slug, [
            'selector' => '%%order_class%% .grid',
            'declaration' => "margin-bottom: -{$gutter}px;",
        ]);

        ET_Builder_Element::set_style($render_slug, [
            'selector' => '%%order_class%% .grid',
            'declaration' => "margin-bottom: -{$gutter_tablet}px;",
            'media_query' => ET_Builder_Element::get_media_query('max_width_980'),
        ]);

        ET_Builder_Element::set_style($render_slug, [
            'selector' => '%%order_class%% .grid',
            'declaration' => "margin-bottom: -{$gutter_phone}px;",
            'media_query' => ET_Builder_Element::get_media_query('max_width_767'),
        ]);

        if('on' === $this->props["show_overflow"]){
            ET_Builder_Element::set_style($render_slug, [
                'selector' => '%%order_class%%.dipi_masonry_gallery, %%order_class%%.dipi_masonry_gallery .grid-item',
                'declaration' => "overflow: visible !important;",
            ]); 
        }
        if('on' === $this->props["use_overlay"]){
            $overlay_bg_image = [];
            $overlay_bg_style = '';
            $use_overlay_bg_gradient = $this->props["overlay_bg_use_color_gradient"];
            $overlay_bg_type = $this->props["overlay_bg_color_gradient_type"];
            $overlay_bg_direction = $this->props["overlay_bg_color_gradient_direction"];
            $overlay_bg_direction_radial = $this->props["overlay_bg_color_gradient_direction_radial"];
            $overlay_bg_start = $this->props["overlay_bg_color_gradient_start"];
            $overlay_bg_end = $this->props["overlay_bg_color_gradient_end"];
            $overlay_bg_start_position = $this->props["overlay_bg_color_gradient_start_position"];
            $overlay_bg_end_position = $this->props["overlay_bg_color_gradient_end_position"];
            $overlay_bg_overlays_image = $this->props["overlay_bg_color_gradient_overlays_image"];
            $overlay_icon_use_circle = $this->props['overlay_icon_use_circle'];
            $overlay_icon_use_circle_border = $this->props['overlay_icon_use_circle_border'];
            $overlay_icon_use_icon_font_size = $this->props['overlay_icon_use_icon_font_size'];
            $overlay_icon_selector = '%%order_class%%.dipi_masonry_gallery .grid .grid-item .dipi_masonry_gallery_overlay .dipi-mansonry-gallery-icon';
            $overlay_icon_circle_selector = '%%order_class%%.dipi_masonry_gallery .grid .grid-item .dipi_masonry_gallery_overlay .dipi-mansonry-gallery-icon.dipi-mansonry-gallery-icon-circle';
            $overlay_selector = '%%order_class%%.dipi_masonry_gallery .grid .grid-item .dipi_masonry_gallery_overlay.content';
            $overlay_selector_background = '%%order_class%%.dipi_masonry_gallery .grid .grid-item .dipi_masonry_gallery_overlay.background';
            $icon_delay = $this->props['icon_delay'];
            $icon_speed = $this->props['icon_speed'];
            $title_delay = $this->props['title_delay'];
            $title_speed = $this->props['title_speed'];
            $caption_delay = $this->props['caption_delay'];
            $caption_speed = $this->props['caption_speed'];
            $hover_icon_selector = "%%order_class%%.dipi_masonry_gallery .grid .grid-item:hover .dipi_masonry_gallery_overlay .dipi-mansonry-gallery-icon";
            $hover_title_selector = "%%order_class%%.dipi_masonry_gallery .grid .grid-item:hover .dipi_masonry_gallery_overlay .dipi-mansonry-gallery-title";
            $hover_caption_selector = "%%order_class%%.dipi_masonry_gallery .grid .grid-item:hover .dipi_masonry_gallery_overlay .dipi-mansonry-gallery-caption";

            ET_Builder_Element::set_style($render_slug, [
                'selector' => $hover_icon_selector,
                'declaration' => "animation-duration: {$icon_speed} !important;",
            ]);
            ET_Builder_Element::set_style($render_slug, [
                'selector' => $hover_icon_selector,
                'declaration' => "animation-delay: {$icon_delay} !important;",
            ]);

            ET_Builder_Element::set_style($render_slug, [
                'selector' => $hover_title_selector,
                'declaration' => "animation-duration: {$title_speed} !important;",
            ]);
            ET_Builder_Element::set_style($render_slug, [
                'selector' => $hover_title_selector,
                'declaration' => "animation-delay: {$title_delay} !important;",
            ]);

            ET_Builder_Element::set_style($render_slug, [
                'selector' => $hover_caption_selector,
                'declaration' => "animation-duration: {$caption_speed} !important;",
            ]);

            ET_Builder_Element::set_style($render_slug, [
                'selector' => $hover_caption_selector,
                'declaration' => "animation-delay: {$caption_delay} !important;",
            ]);            

            $this->apply_custom_margin_padding(
                $render_slug,
                'overlay_padding',
                'padding',
                $overlay_selector
            );

            if ('on' === $use_overlay_bg_gradient) {

                $overlay_direction = $overlay_bg_type === 'linear' ? $overlay_bg_direction : "circle at $overlay_bg_direction_radial";
                $overlay_start_position = et_sanitize_input_unit($overlay_bg_start_position, false, '%');
                $overlay_end_position = et_sanitize_input_unit($overlay_bg_end_position, false, '%');
                $overlay_gradient_bg = "{$overlay_bg_type}-gradient($overlay_direction, {$overlay_bg_start} {$overlay_start_position},{$overlay_bg_end} {$overlay_end_position})";

                if (!empty($overlay_gradient_bg)) {
                    $overlay_bg_image[] = $overlay_gradient_bg;
                }

            }

            if (!empty($overlay_bg_image)) {
                if ('on' !== $overlay_bg_overlays_image) {
                    $overlay_bg_image = array_reverse($overlay_bg_image);
                }

                $overlay_bg_style .= sprintf(
                    'background-image: %1$s !important;',
                    esc_html(join(', ', $overlay_bg_image))
                );
            }

            $overlay_bg_color = $this->props["overlay_bg_color"];
            if ('' !== $overlay_bg_color) {
                $overlay_bg_style .= sprintf(
                    'background-color: %1$s !important; ',
                    esc_html($overlay_bg_color)
                );
            }

            if ('' !== $overlay_bg_style) {
                ET_Builder_Element::set_style($render_slug, array(
                    'selector' => $overlay_selector_background,
                    'declaration' => rtrim($overlay_bg_style),
                ));
            }

            $text_align_style = sprintf(
                'text-align: %1$s !important;',
                $this->props['overlay_align_horizontal'] === 'flex-start' ? 'left' : 
                ($this->props['overlay_align_horizontal'] === 'flex-end' ? 'right' : 'center' )
            );
            ET_Builder_Element::set_style($render_slug, array(
                'selector' => $overlay_selector,
                'declaration' => $text_align_style,
            ));
            $this->generate_styles(
                array(
                    'base_attr_name' => 'overlay_align_horizontal',
                    'selector' => $overlay_selector,
                    'css_property' => 'align-items',
                    'render_slug' => $render_slug,
                    'type' => 'select',
                )
            );

            $this->generate_styles(
                array(
                    'base_attr_name' => 'overlay_align_vertical',
                    'selector' => $overlay_selector,
                    'css_property' => 'justify-content',
                    'render_slug' => $render_slug,
                    'type' => 'select',
                )
            );

            // Overlay Icon
            if ('off' !== $overlay_icon_use_icon_font_size) {
                $this->generate_styles(
                    array(
                        'base_attr_name' => 'overlay_icon_font_size',
                        'selector' => $overlay_icon_selector,
                        'css_property' => 'font-size',
                        'render_slug' => $render_slug,
                        'type' => 'range',
                    )
                );
            }
            $this->generate_styles(
                array(
                    'base_attr_name' => 'overlay_icon_color',
                    'selector' => $overlay_icon_selector,
                    'css_property' => 'color',
                    'render_slug' => $render_slug,
                    'type' => 'color',
                )
            );

            if ('on' === $overlay_icon_use_circle) {
                $this->generate_styles(
                    array(
                        'base_attr_name' => 'overlay_icon_circle_color',
                        'selector' => $overlay_icon_circle_selector,
                        'css_property' => 'background-color',
                        'render_slug' => $render_slug,
                        'type' => 'color',
                    )
                );

                if ('on' === $overlay_icon_use_circle_border) {
                    $this->generate_styles(
                        array(
                            'base_attr_name' => 'overlay_icon_circle_border_color',
                            'selector' => $overlay_icon_circle_selector,
                            'css_property' => 'border-color',
                            'render_slug' => $render_slug,
                            'type' => 'color',
                        )
                    );
                }
            }
        }
    }

}

new DIPI_MasonryGallery;
