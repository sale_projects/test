<?php

if ( ! class_exists( 'ET_Builder_Element' ) ) {
	return;
}

require_once plugin_dir_path(__FILE__) . 'modules/Base/DIPI_Builder_Module.php';

if (!\DiviPixel\DIPI_Settings::get_option('md_masonry_gallery')){
	require_once plugin_dir_path(__FILE__) . 'modules/MasonryGallery/MasonryGallery.php';
}

if (!\DiviPixel\DIPI_Settings::get_option('md_flip_box')){
	require_once plugin_dir_path(__FILE__) . 'modules/FlipBox/FlipBox.php';
}

if (!\DiviPixel\DIPI_Settings::get_option('md_counter')){
	require_once plugin_dir_path(__FILE__) . 'modules/Counter/Counter.php';
}

if (!\DiviPixel\DIPI_Settings::get_option('md_tilt_image')){
	require_once plugin_dir_path(__FILE__) . 'modules/TiltImage/TiltImage.php';
}

if (!\DiviPixel\DIPI_Settings::get_option('md_floating_multi_images')){
	require_once plugin_dir_path(__FILE__) . 'modules/FloatingMultiImagesChild/FloatingMultiImagesChild.php';
	require_once plugin_dir_path(__FILE__) . 'modules/FloatingMultiImages/FloatingMultiImages.php';
}

if (!\DiviPixel\DIPI_Settings::get_option('md_before_after_slider')){
	require_once plugin_dir_path(__FILE__) . 'modules/BeforeAfterSlider/BeforeAfterSlider.php';
}

if (!\DiviPixel\DIPI_Settings::get_option('md_carousel')) {
	require_once plugin_dir_path(__FILE__) . 'modules/CarouselChild/CarouselChild.php';
	require_once plugin_dir_path(__FILE__) . 'modules/Carousel/Carousel.php';
}

if (!\DiviPixel\DIPI_Settings::get_option('md_typing_text')) {
	require_once plugin_dir_path(__FILE__) . 'modules/TypingText/TypingText.php';
}

if (!\DiviPixel\DIPI_Settings::get_option('md_before_after_slider')) {
    require_once plugin_dir_path(__FILE__) . 'modules/BeforeAfterSlider/BeforeAfterSlider.php';
}

if (!\DiviPixel\DIPI_Settings::get_option('md_breadcrumbs')) {
	require_once plugin_dir_path(__FILE__) . 'modules/Breadcrumbs/Breadcrumbs.php';
}

if (!\DiviPixel\DIPI_Settings::get_option('md_star_rating')) {
	require_once plugin_dir_path(__FILE__) . 'modules/StarRating/StarRating.php';
}

if (!\DiviPixel\DIPI_Settings::get_option('md_button_grid')) {
	require_once plugin_dir_path(__FILE__) . 'modules/ButtonGridChild/ButtonGridChild.php';
	require_once plugin_dir_path(__FILE__) . 'modules/ButtonGrid/ButtonGrid.php';
}

if (!\DiviPixel\DIPI_Settings::get_option('md_image_hotspot')) {
	require_once plugin_dir_path(__FILE__) . 'modules/ImageHotspot/ImageHotspot.php';
	require_once plugin_dir_path(__FILE__) . 'modules/ImageHotspotChild/ImageHotspotChild.php';
}

if (!\DiviPixel\DIPI_Settings::get_option('md_testimonial')) {
	require_once plugin_dir_path(__FILE__) . 'modules/Testimonial/Testimonial.php';
}

if (!\DiviPixel\DIPI_Settings::get_option('md_countdown')) {
	require_once plugin_dir_path(__FILE__) . 'modules/Countdown/Countdown.php';
}

if (!\DiviPixel\DIPI_Settings::get_option('md_pricelist')) {
	require_once plugin_dir_path(__FILE__) . 'modules/PriceList/PriceList.php';
	require_once plugin_dir_path(__FILE__) . 'modules/PriceListItem/PriceListItem.php';
}

if (!\DiviPixel\DIPI_Settings::get_option('md_fancy_text')) {
	require_once plugin_dir_path(__FILE__) . 'modules/FancyText/FancyText.php';
	require_once plugin_dir_path(__FILE__) . 'modules/FancyTextChild/FancyTextChild.php';
}

if (!\DiviPixel\DIPI_Settings::get_option('md_hover_box')) {
	require_once plugin_dir_path(__FILE__) . 'modules/HoverBox/HoverBox.php';
}

if (!\DiviPixel\DIPI_Settings::get_option('md_blog_slider')) {
    require_once plugin_dir_path(__FILE__) . 'modules/BlogSlider/BlogSlider.php';
}

if(!\DiviPixel\DIPI_Settings::get_option('md_balloon')){
	require_once plugin_dir_path(__FILE__) . 'modules/Balloon/Balloon.php';
}

if (!\DiviPixel\DIPI_Settings::get_option('md_accordion_image')) {
	require_once plugin_dir_path(__FILE__) . 'modules/ImageAccordion/ImageAccordion.php';
	require_once plugin_dir_path(__FILE__) . 'modules/ImageAccordionChild/ImageAccordionChild.php';
}

if (!\DiviPixel\DIPI_Settings::get_option('md_scroll_image')) {
	require_once plugin_dir_path(__FILE__) . 'modules/ScrollImage/ScrollImage.php';
}

if (!\DiviPixel\DIPI_Settings::get_option('md_panorama')) {
	require_once plugin_dir_path(__FILE__) . 'modules/Panorama/Panorama.php';
}

if (!\DiviPixel\DIPI_Settings::get_option('md_reading_progress_bar')) {
	require_once plugin_dir_path(__FILE__) . 'modules/ReadingProgressBar/ReadingProgressBar.php';
}

if (!\DiviPixel\DIPI_Settings::get_option('md_image_magnifier')) {
	require_once plugin_dir_path(__FILE__) . 'modules/ImageMagnifier/ImageMagnifier.php';
}

if (!\DiviPixel\DIPI_Settings::get_option('md_lottie_icon')) {
	require_once plugin_dir_path(__FILE__) . 'modules/LottieIcon/LottieIcon.php';
}

if (!\DiviPixel\DIPI_Settings::get_option('md_image_mask')) {
	require_once plugin_dir_path(__FILE__) . 'modules/ImageMask/ImageMask.php';
}

if (!\DiviPixel\DIPI_Settings::get_option('md_image_showcase')) {
	require_once plugin_dir_path(__FILE__) . 'modules/ImageShowcase/ImageShowcase.php';
	require_once plugin_dir_path(__FILE__) . 'modules/ImageShowcaseChild/ImageShowcaseChild.php';
}

if (!\DiviPixel\DIPI_Settings::get_option('md_timeline')) {
	require_once plugin_dir_path(__FILE__) . 'modules/TimeLine/TimeLine.php';
	require_once plugin_dir_path(__FILE__) . 'modules/TimeLineItem/TimeLineItem.php';
}

if (!\DiviPixel\DIPI_Settings::get_option('md_content_toggle')) {
	require_once plugin_dir_path(__FILE__) . 'modules/ContentToggle/ContentToggle.php';
}