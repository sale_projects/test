<?php
add_shortcode( 'DP_Grid_View_Event', 'dem_grid_view_event' );
add_shortcode( 'DP_List_View_Event', 'dem_list_view_event' );
add_shortcode( 'DP_Slider_View_Event', 'dem_slider_view_event' );

/*************Grid view *****************/
function dem_grid_view_event( $atts ) {
	$dem_themename = dem_theme_name();
	$divi_dem_grid_column_layout 	= et_get_option($dem_themename.'_dem_grid_column_layout','col3');
	$divi_dem_grid_column_orderby 	= et_get_option($dem_themename.'_dem_grid_column_orderby','date_desc');
	$dem_display_time 				= et_get_option($dem_themename.'_dem_display_time','twhr');
	$divi_dem_ticket_start_from 	= et_get_option($dem_themename.'_dem_ticket_start_from','Ticket Start From'); 
	$dem_evt_to 					= et_get_option($dem_themename.'_dem_evt_to','to'); 
	$dem_evt_at						= et_get_option($dem_themename.'_dem_evt_at','at'); 
	$dem_evt_on 					= et_get_option($dem_themename.'_dem_evt_on','on'); 
	$atts = shortcode_atts( array(
	'dem_post_per_page' 		=> '6', // how many post
	'dem_event_view_style' 		=> 'style1', // title goes to here
	'dem_show_pagination'		=> 'on', //show pagination on/off
	'dem_show_upcoming_events'  => 'off',
	'dem_event_by_category'     => '',
	'dem_event_by_tag'          => '',
	'dem_show_past_events'  	=> 'off'
	),$atts );

	$dem_post_per_page 			= $atts['dem_post_per_page'];
	$dem_event_view_style 		= $atts['dem_event_view_style'];
	$dem_show_pagination 		= $atts['dem_show_pagination'];
	$dem_show_upcomming_events 	= $atts['dem_show_upcoming_events'];
	$dem_event_by_tag 			= $atts['dem_event_by_tag'];
	$dem_event_by_category 		= $atts['dem_event_by_category'];
	$dem_show_past_events 		= $atts['dem_show_past_events'];

	$event_args = array();
	/*************Post Per Page*****************/
	if ( empty ( $dem_post_per_page ) ){
		$event_args['posts_per_page'] = '6' ;
	}else{
		$event_args['posts_per_page'] = (int) $dem_post_per_page;
	}
	
	// pagination
	if ( get_query_var('paged') ) {
		$paged = (int)get_query_var('paged');
	} elseif ( get_query_var('page') ) {
		$paged = (int) get_query_var('page');
	} else {
		$paged = 1;
	}

	$event_tax_query = array();
	// by category
	if ( !empty ( $dem_event_by_category ) ) {
		$event_by_category  = explode(',',$dem_event_by_category );
		$event_tax_query[] =  array(
			'taxonomy' => 'event_category',
			'field'    => 'term_id',
			'terms'    => $event_by_category,
			'operator' => 'IN'
		);
	}
	// by tag
	if ( !empty ( $dem_event_by_tag ) ) {
		$event_by_tag = explode(',',$dem_event_by_tag );
		$event_tax_query[] =  array(
			'taxonomy' => 'event_tag',
			'field'    => 'term_id',
			'terms'    => $event_by_tag,
			'operator' => 'IN'
		);
	}
	if (count($event_tax_query) > 1) {
		$event_args['relation'] = 'AND';
	}   
	// orderby & order
	if ( 'date_desc' !== $divi_dem_grid_column_orderby ) {
		switch( $divi_dem_grid_column_orderby ) {
			case 'date_asc' :
				$event_args['orderby'] = 'date';
				$event_args['order'] = 'ASC';
			break;
			case 'title_asc' :
				$event_args['orderby'] = 'title';
				$event_args['order'] = 'ASC';
			break;
			case 'title_desc' :
				$event_args['orderby'] = 'title';
				$event_args['order'] = 'DESC';
			break;
			case 'rand' :
				$event_args['orderby'] = 'rand';
			break;
			case 'event_start_desc' :
				$event_args['orderby']  = 'meta_value_num';
				$event_args['order'] 	= 'DESC';
				$event_args['meta_key'] = 'dp_event_start_date';
			break;
			case 'event_start_asc' :
				$event_args['orderby']  = 'meta_value_num';
				$event_args['order'] 	= 'ASC';
				$event_args['meta_key'] = 'dp_event_start_date';
			break;
		}
	}else{
		$event_args['orderby'] = 'date';
		$event_args['order'] = 'DESC';
	}

	$event_args['post_type'] 	= 'dp_events';
	$event_args['paged'] 		= $paged;
	$event_args['tax_query']    = $event_tax_query; 

	//show upcomming events
	if($dem_show_upcomming_events == 'on' && $dem_show_past_events == 'off' ){
		$current_date = gmdate('d-m-Y');
		$event_args['meta_query']  = array(
			array(
				'key' 		=> 'dp_event_end_date',
				'value' 	=>  strtotime($current_date),
				'compare' 	=> '>=', 
			),

		);
	}	
	//show past events
	if( $dem_show_past_events == 'on' && $dem_show_upcomming_events == 'off'){
		$current_date = gmdate('d-m-Y');
		$event_args['meta_query']  = array(
			array(
				'key' 		=> 'dp_event_end_date',
				'value' 	=>  strtotime($current_date),
				'compare' 	=> '<=', 
			),

		);
	}

	$event_posts = new WP_Query( $event_args );
	
	// Find Files From Child Theme. If Found then call from theme otherwise files call from plugin
	$dem_template_path =  get_stylesheet_directory() . '/divi-eventmanager';
	$dem_grid_path =  $dem_template_path.'/grid';
	$dem_css_path = $dem_template_path.'/css/grid';
	$dem_css_url =  get_stylesheet_directory_uri().'/divi-eventmanager/css/grid'; 
	if($dem_show_pagination == 'on' ){
		wp_enqueue_style('dem_event_pagination_css');
	}
	if ( file_exists( $dem_css_path . '/dem_grid_'.esc_attr( $dem_event_view_style ).'.css' ) )
	{
		wp_enqueue_style('dem_grid_view_event_sc_'.esc_attr( $dem_event_view_style ), $dem_css_url.'/dem_grid_'.esc_attr( $dem_event_view_style ).'.css', array(), NULL);
	}else{
		wp_enqueue_style('dem_grid_view_event_sc_'.esc_attr( $dem_event_view_style ), DEM_PLUGIN_URL.'assets/css/shortcode-layout-css/grid/dem_grid_'.esc_attr( $dem_event_view_style ).'.min.css', array(), NULL);
	}
	wp_enqueue_script('dem_equalheight');

	$event_view_content = '';

	if ( $divi_dem_grid_column_layout == 'col4'){
		$column_class = 'et_pb_column_1_4';
		$condition = '4';
	}else if ( $divi_dem_grid_column_layout == 'col3'){
		$column_class = 'et_pb_column_1_3';
		$condition = '3';
	} else if ( $divi_dem_grid_column_layout == 'col2'){
		$column_class = 'et_pb_column_1_2';
		$condition = '2';
	} else{
		$column_class = 'et_pb_column_1_3';
		$condition = '3';
	}
	if($event_posts ->have_posts()) : 
		$event_view_content .= '<div class="et_pb_section et_pb_section_grid_view">';
		$event_view_content .= '<div class="et_pb_row dem_grid_'.esc_attr( $dem_event_view_style ).' et_pb_gutters2">';
		$i=1;
		while( $event_posts ->have_posts() ) : 
			$last_child = '';
			if ( $i % $condition == 0 ){ $last_child = 'et-last-child'; }
			$event_posts->the_post();
			if ( $divi_dem_grid_column_layout == 'col2'){
				$dp_event_thumb = wp_get_attachment_image_src(get_post_thumbnail_id(), 'large');
			}else{ 
				$dp_event_thumb = wp_get_attachment_image_src(get_post_thumbnail_id(), 'dem_grid_400_400');
			}
			if( $dp_event_thumb[0] != ''){ 
				$image_path = $dp_event_thumb[0] ;
			}else{ 
				$image_path = DEM_PLUGIN_URL. '/assets/images/default.png';
			}
			ob_start(); 
			$event_thumbnail_image_url 		= $image_path ;
			$event_permalink 				= get_the_permalink(get_the_ID());
			$event_title 					= get_the_title(get_the_ID());
			$event_start_date 				= get_post_meta(get_the_ID(), 'dp_event_start_date',true);
			$event_end_date 				= get_post_meta(get_the_ID(), 'dp_event_end_date',true);
			$event_start_time 				= get_post_meta(get_the_ID(), 'dp_event_start_time',true);
			$event_end_time 				= get_post_meta(get_the_ID(), 'dp_event_end_time',true);
			$event_venue					= get_post_meta(get_the_ID() ,'dpevent_address', true );
			$event_city                     = get_post_meta(get_the_ID() ,'dpevent_city', true );
			$event_state                    = get_post_meta(get_the_ID() ,'dpevent_state', true );
			$event_country                  = get_post_meta(get_the_ID() ,'dpevent_country', true );
			$event_ticket_cost 				= get_post_meta(get_the_ID(), 'dpevent_cost_name', true);
			$event_ticket_cost_currency		= get_post_meta(get_the_ID(), 'dp_event_currency_symbol_name', true);
			$event_ticket_currency_position	= get_post_meta(get_the_ID(), 'dp_event_currency_prefix_suffix', true);
			$event_readmore_btn_text		= et_get_option($dem_themename.'_dem_grid_readmore_text','Read More');
			$event_content 					= get_the_excerpt();   
			$event_venue_address = '';
			if( !empty ( $event_venue ) ){
				$event_venue_address .= $event_venue.', ';
			}
			if( !empty ( $event_city ) ){
				$event_venue_address .= $event_city.', ';
			}
			if( !empty ( $event_state ) ){
				$event_venue_address .= $event_state.', ';
			}
			if( !empty ( $event_country ) ){
				$event_venue_address .= $event_country.', ';
			}

			if ( file_exists( $dem_grid_path . '/dem_grid_'.esc_attr( $dem_event_view_style ).'.php' ) )
			{
				include $dem_grid_path. '/dem_grid_'.esc_attr( $dem_event_view_style ).'.php';
			}else{
				include DEM_PLUGIN_PATH. 'include/shortcode-layout-style/grid/dem_grid_'.esc_attr( $dem_event_view_style ).'.php';
			}
			
			$event_view_content .= ob_get_contents();
			ob_end_clean();
			$i++;		 
	endwhile;
	wp_reset_postdata(); 
	$event_view_content .= "</div></div>";
endif;

/************* Pagination *****************/
if($dem_show_pagination == 'on' && $event_posts->max_num_pages > 1){
	$event_view_content .= '<div class="et_pb_row_custom_pagination pagination-container"><nav class="navigation dem_pagination"><div class="nav-links">';
	$event_view_content .= paginate_links(array(
		'base' => get_pagenum_link(1) . '%_%',
		'format' => 'page/%#%',
		'current' => max( 1, $paged ),
		'total' => $event_posts->max_num_pages,
		'prev_text' => __( '<span class="et-pb-icon">&#x34;</span>', 'dpevent' ),
		'next_text' => __( '<span class="et-pb-icon">&#x35;</span>', 'dpevent' ),
	));
	$event_view_content .=  '</div></nav></div>';
	}
	return  $event_view_content;
}

/************* List view *****************/
function dem_list_view_event($atts){
	 $dem_themename = dem_theme_name();
	 $divi_dem_list_column_orderby = et_get_option($dem_themename.'_dem_list_column_orderby','date_desc');
	 $dem_display_time = et_get_option($dem_themename.'_dem_display_time','twhr');
		$dem_evt_to 					= et_get_option($dem_themename.'_dem_evt_to','to'); 
		$dem_evt_at						= et_get_option($dem_themename.'_dem_evt_at','at'); 
		$dem_evt_on 					= et_get_option($dem_themename.'_dem_evt_on','on');
	 $atts = shortcode_atts( array(
			'dem_post_per_page' 	=> '6', // how many post
			'dem_event_view_style' 	=> 'style1', // title goes to here
			'dem_show_pagination'	=> 'on', //show pagination on/off
			'dem_show_upcoming_events' => 'off',
			'dem_event_by_category'     => '',
			'dem_event_by_tag'          => '',
			'dem_show_past_events'  	=> 'off'
		),$atts );
	
	 $dem_post_per_page 			= $atts['dem_post_per_page'];
	 $dem_event_view_style 			= $atts['dem_event_view_style'];
	 $dem_show_pagination 			= $atts['dem_show_pagination'];
	 $dem_show_upcomming_events 	= $atts['dem_show_upcoming_events'];
	 $dem_event_by_tag 				= $atts['dem_event_by_tag'];
	 $dem_event_by_category 		= $atts['dem_event_by_category'];
	 $dem_show_past_events 			= $atts['dem_show_past_events'];
	
	 $event_args = array();
	
	 /*************Post Per Page*****************/
	 if ( empty ( $dem_post_per_page )){
		  $event_args['posts_per_page'] = '6' ;
	 }else{
		  $event_args['posts_per_page'] = (int) $dem_post_per_page;
	 }
	 // pagination
	 if ( get_query_var('paged') ) {
	  $paged = (int) get_query_var('paged');
	 } elseif ( get_query_var('page') ) {
	  $paged = (int) get_query_var('page');
	 } else {
	  $paged = 1;
	 }
	 
	$event_tax_query = array();
	//by category
	if (!empty ( $dem_event_by_category ) ) {
	 	  $event_by_category  = explode(',',$dem_event_by_category );
		  $event_tax_query[] =  array(
		   'taxonomy' => 'event_category',
		   'field'    => 'term_id',
		   'terms'    => $event_by_category,
		   'operator' => 'IN'
		);
	}
	//by tag
	if (!empty ( $dem_event_by_tag ) ) {
		  $event_by_tag = explode(',',$dem_event_by_tag );
		  $event_tax_query[] =  array(
		   'taxonomy' => 'event_tag',
		   'field'    => 'term_id',
		   'terms'    => $event_by_tag,
		   'operator' => 'IN'
		);
	}
	
	if (count($event_tax_query) > 1) {
	  	$event_args['relation'] = 'AND';
	}   
	
	// order & orderby
	if ( 'date_desc' !== $divi_dem_list_column_orderby ) {
	  switch( $divi_dem_list_column_orderby ) {
	   case 'date_asc' :
		   $event_args['orderby'] = 'date';
		   $event_args['order'] = 'ASC';
	   break;
	   case 'title_asc' :
		   $event_args['orderby'] = 'title';
		   $event_args['order'] = 'ASC';
	   break;
	   case 'title_desc' :
		   $event_args['orderby'] = 'title';
		   $event_args['order'] = 'DESC';
	   break;
	   case 'rand' :
	  	 	$event_args['orderby'] = 'rand';
	   break;
	   case 'event_start_desc' :
		   $event_args['orderby'] = 'meta_value_num';
		   $event_args['order'] = 'DESC';
		   $event_args['meta_key'] = 'dp_event_start_date';
	   break;
	   case 'event_start_asc' :
		   $event_args['orderby'] = 'meta_value_num';
		   $event_args['order'] = 'ASC';
		   $event_args['meta_key'] = 'dp_event_start_date';
	   break;
	}
	}else{
	  $event_args['orderby'] = 'date';
	  $event_args['order'] = 'DESC';
	}
	
	$event_args['post_type'] 	= 'dp_events';
	$event_args['paged'] 		= $paged;
	$event_args['tax_query']    = $event_tax_query; 

	// show upcomming events
	if($dem_show_upcomming_events == 'on' && $dem_show_past_events == 'off' ){
	  	 $current_date = gmdate('d-m-Y');
		  $event_args['meta_query']  = array(
		   array(
			'key' 		=> 'dp_event_end_date',
			'value' 	=>  strtotime($current_date),
			'compare' 	=> '>=', 
			),
		  );
	}	
	// show past events
	if( $dem_show_past_events == 'on' && $dem_show_upcomming_events == 'off'){
		  $current_date = gmdate('d-m-Y');
		  $event_args['meta_query']  = array(
		   array(
			'key' 		=> 'dp_event_end_date',
			'value' 	=>  strtotime($current_date),
			'compare' 	=> '<=', 
			),
		  );
	}
	
	/************* End Post Per Page*****************/
	$event_posts = new WP_Query( $event_args );
    // Find Files From Child Theme. If Found then call from theme otherwise files call from plugin 
	$dem_template_path =  get_stylesheet_directory() . '/divi-eventmanager';
	$dem_list_path =  $dem_template_path.'/list';
	$dem_css_path = $dem_template_path.'/css/list';
	$dem_css_url =  get_stylesheet_directory_uri().'/divi-eventmanager/css/list'; 
	if($dem_show_pagination == 'on' ){
		wp_enqueue_style('dem_event_pagination_css');
	}
	if ( file_exists( $dem_css_path . '/dem_list_'.esc_attr( $dem_event_view_style ).'.css' ) )
	{
	  wp_enqueue_style('dem_list_view_event_sc_'.esc_attr( $dem_event_view_style ), $dem_css_url.'/dem_list_'.esc_attr( $dem_event_view_style ).'.css', array(), NULL);
	}else{
	  wp_enqueue_style('dem_list_view_event_sc_'.esc_attr( $dem_event_view_style ), DEM_PLUGIN_URL.'assets/css/shortcode-layout-css/list/dem_list_'.esc_attr( $dem_event_view_style ).'.min.css', array(), NULL);
	}
	wp_enqueue_script( 'dem_equalheight', DEM_PLUGIN_URL.'assets/js/dem_equalheight.js', array(),'1.0.0',true);
	
	$event_view_content = '';
	
	if($event_posts ->have_posts()) : 
	  $event_view_content .= '<div class="et_pb_section et_pb_section_list_view">';
	  $style3_class = '';
	  if ( $dem_event_view_style == 'style3'){ $style3_class = 'dem_list3'; } 
	  $event_view_content .= '<div class="et_pb_row dem_list_'.esc_attr( $dem_event_view_style ).'  '.$style3_class.' ">';
	
	global $et_fb_processing_shortcode_object;
	$global_processing_original_value = $et_fb_processing_shortcode_object;
	while($event_posts ->have_posts()) : 
	   $event_posts ->the_post();
	   $dp_event_thumb = wp_get_attachment_image_src(get_post_thumbnail_id(), 'dem_list_300_300');
	   if( $dp_event_thumb[0] != ''){ 
			$image_path = $dp_event_thumb[0] ;
	   }else{ 
			$image_path = DEM_PLUGIN_URL. '/assets/images/default.png';
	   }  
	$event_content_len 				= strlen(get_the_content());
	if ($event_content_len > 100){
		$et_fb_processing_shortcode_object = false;
		$event_content 				= wpautop( et_delete_post_first_video( strip_shortcodes( truncate_post( 100, false, '', true ) ) ) );
		$et_fb_processing_shortcode_object = $global_processing_original_value;
	}else{
		$et_fb_processing_shortcode_object = false;
		$post_content 				= et_strip_shortcodes( et_delete_post_first_video( get_the_content() ), true );
		$event_content 				= apply_filters( 'the_content', $post_content );
		$et_fb_processing_shortcode_object = $global_processing_original_value;
	}	
	ob_start();
	$event_thumbnail_image_url 		= $image_path;
	$event_thumbnail_image_large_url= get_the_post_thumbnail_url( get_the_ID(),'full' );
	$event_permalink 				= get_the_permalink(get_the_ID());
	$event_title 					= get_the_title(get_the_ID());
	$event_start_date 				= get_post_meta(get_the_ID(), 'dp_event_start_date',true);
	$event_end_date 				= get_post_meta(get_the_ID(), 'dp_event_end_date',true);
	$event_start_time 				= get_post_meta(get_the_ID(), 'dp_event_start_time',true);
	$event_end_time 				= get_post_meta(get_the_ID(), 'dp_event_end_time',true);
	$event_venue					= get_post_meta(get_the_ID() ,'dpevent_address', true );
	$event_city                     = get_post_meta(get_the_ID() ,'dpevent_city', true );
	$event_state                    = get_post_meta(get_the_ID() ,'dpevent_state', true );
	$event_country                  = get_post_meta(get_the_ID() ,'dpevent_country', true );
	$event_ticket_cost 				= get_post_meta(get_the_ID(), 'dpevent_cost_name', true);
	$event_ticket_cost_currency		= get_post_meta(get_the_ID(), 'dp_event_currency_symbol_name', true);
	$event_ticket_currency_position	= get_post_meta(get_the_ID(), 'dp_event_currency_prefix_suffix', true);
	$event_readmore_btn_text		= et_get_option($dem_themename.'_dem_grid_readmore_text','Read More');
	$event_content 					= get_the_excerpt();   
	  
	$event_venue_address = '';
	if( !empty ( $event_venue ) ){
		$event_venue_address .= $event_venue.', ';
	}
	if( !empty ( $event_city ) ){
		$event_venue_address .= $event_city.', ';
	}
	if( !empty ( $event_state ) ){
		$event_venue_address .= $event_state.', ';
	}
	if( !empty ( $event_country ) ){
		$event_venue_address .= $event_country.', ';
	}

	if ( file_exists( $dem_list_path . '/dem_list_'.esc_attr( $dem_event_view_style ).'.php' ) )
	{
		include $dem_list_path. '/dem_list_'.esc_attr( $dem_event_view_style ).'.php';
	}else{
		include DEM_PLUGIN_PATH. 'include/shortcode-layout-style/list/dem_list_'.esc_attr( $dem_event_view_style ).'.php';
	}
	$event_view_content .= ob_get_contents();
	ob_end_clean();
	endwhile;
	wp_reset_postdata(); 
	$event_view_content .= "</div></div>";
	endif;
	
	/************* Pagination *****************/
	if($dem_show_pagination == 'on' && $event_posts->max_num_pages > 1){
	  $event_view_content .= '<div class="et_pb_row_custom_pagination pagination-container"><nav class="navigation dem_pagination"><div class="nav-links">';
	   $event_view_content .= paginate_links(array(
		   'base' => get_pagenum_link(1) . '%_%',
		   'format' => 'page/%#%',
		   'current' => max( 1, $paged ),
		   'total' => $event_posts->max_num_pages,
		   'prev_text' => __( '<span class="et-pb-icon">&#x34;</span>', 'dpevent' ),
		   'next_text' => __( '<span class="et-pb-icon">&#x35;</span>', 'dpevent' ),
		));
	  $event_view_content .=  '</div></nav></div>';
	}
	return  $event_view_content;
}
/************* Slider view *****************/
function dem_slider_view_event($atts){
	 $dem_themename = dem_theme_name();
	 $divi_dem_slider_column_layout = et_get_option($dem_themename.'_dem_slider_column_layout','col3');
	 $divi_dem_slider_column_orderby = et_get_option($dem_themename.'_dem_slider_column_orderby','date_desc');
	 $dem_display_time = et_get_option($dem_themename.'_dem_display_time','twhr');
	 $dem_evt_to 					= et_get_option($dem_themename.'_dem_evt_to','to'); 
	 $atts = shortcode_atts( array(
			'dem_no_of_events' 	=> '6', // how many post
			'dem_event_view_style' 	=> 'style1', // title goes to here
			'dem_show_upcoming_events' => 'off',
			'dem_event_by_category'     => '',
			'dem_event_by_tag'          => '',
			'dem_show_past_events'  	=> 'off'
		),$atts );
	
	 $dem_post_per_page 			= $atts['dem_no_of_events'];
	 $dem_event_view_style 			= $atts['dem_event_view_style'];
	 $dem_show_upcomming_events 	= $atts['dem_show_upcoming_events'];
	 $dem_event_by_tag 				= $atts['dem_event_by_tag'];
	 $dem_event_by_category 		= $atts['dem_event_by_category'];
	 $dem_show_past_events 			= $atts['dem_show_past_events'];
	
	 $event_view_content = '';
	 $event_view_content .= '<div class="et_pb_section et_section_regular et_pb_section_slider_view">';
	 $event_args = array();
	 /*************Post Per Page*****************/
	 if ( empty ( $dem_post_per_page ) ){
	  $event_args['posts_per_page'] = '-1' ;
	 }else{
	  $event_args['posts_per_page'] = (int) $dem_post_per_page;
	 }
	// pagination
	if ( get_query_var('paged') ) {
	  $paged = (int) get_query_var('paged');
	} elseif ( get_query_var('page') ) {
	  $paged = (int) get_query_var('page');
	} else {
	  $paged = 1;
	}
	$event_tax_query = array();
	//by_category
	if (!empty ( $dem_event_by_category ) ) {
	  $event_by_category  = explode(',',$dem_event_by_category );
		  $event_tax_query[] =  array(
		   'taxonomy' => 'event_category',
		   'field'    => 'term_id',
		   'terms'    => $event_by_category,
		   'operator' => 'IN'
		  );
	}
	// by_tag
	if (!empty ( $dem_event_by_tag ) ) {
	  $event_by_tag = explode(',',$dem_event_by_tag );
	   $event_tax_query[] =  array(
		   'taxonomy' => 'event_tag',
		   'field'    => 'term_id',
		   'terms'    => $event_by_tag,
		   'operator' => 'IN'
		);
	}
	if (count($event_tax_query) > 1) {
	  $event_args['relation'] = 'AND';
	}   
	
	if ( 'date_desc' !== $divi_dem_slider_column_orderby ) {
	  switch( $divi_dem_slider_column_orderby ) {
	   case 'date_asc' :
		   $event_args['orderby'] = 'date';
		   $event_args['order'] = 'ASC';
	   break;
	   case 'title_asc' :
		   $event_args['orderby'] = 'title';
		   $event_args['order'] = 'ASC';
	   break;
	   case 'title_desc' :
		   $event_args['orderby'] = 'title';
		   $event_args['order'] = 'DESC';
	   break;
	   case 'rand' :
	   	   $event_args['orderby'] = 'rand';
	   break;
	   case 'event_start_desc' :
		   $event_args['orderby'] = 'meta_value_num';
		   $event_args['order'] = 'DESC';
		   $event_args['meta_key'] = 'dp_event_start_date';
	   break;
	   case 'event_start_asc' :
		   $event_args['orderby'] = 'meta_value_num';
		   $event_args['order'] = 'ASC';
		   $event_args['meta_key'] = 'dp_event_start_date';
	   break;
	 }
	}else{
	  $event_args['orderby'] = 'date';
	  $event_args['order'] = 'DESC';
	}
	
	$event_args['post_type'] 	= 'dp_events';
	$event_args['paged'] 		= $paged;
	$event_args['tax_query']    = $event_tax_query;
	//show_upcomming_events
	if($dem_show_upcomming_events == 'on' && $dem_show_past_events == 'off' ){
	 $current_date = gmdate('d-m-Y');
	  $event_args['meta_query']  = array(
		   array(
			'key' 		=> 'dp_event_end_date',
			'value' 	=>  strtotime($current_date),
			'compare' 	=> '>=', 
			),
		);
	}
	//show_past_events	
	if( $dem_show_past_events == 'on' && $dem_show_upcomming_events == 'off'){
	  $current_date = gmdate('d-m-Y');
	  $event_args['meta_query']  = array(
		   array(
			'key' 		=> 'dp_event_end_date',
			'value' 	=>  strtotime($current_date),
			'compare' 	=> '<=', 
		),
		);
	}
	/************* End Post Per Page*****************/
	$event_posts = new WP_Query( $event_args );
	if ( $divi_dem_slider_column_layout == 'col4'){
	  $condition = '4';
	}else if ( $divi_dem_slider_column_layout == 'col3'){
	  $condition = '3';
	} else if ( $divi_dem_slider_column_layout == 'col2'){
	  $condition = '2';
	} else{
	  $condition = '3';
	}
	// Find Files From Child Theme. If Found then call from theme otherwise files call from plugin 
	$dem_template_path =  get_stylesheet_directory() . '/divi-eventmanager';
	$dem_slider_path =  $dem_template_path.'/slider';
	$dem_css_path = $dem_template_path.'/css/slider';
	$dem_css_url =  get_stylesheet_directory_uri().'/divi-eventmanager/css/slider'; 
	
	wp_enqueue_style('dem_slider_owl_carousel_min_css');
	wp_enqueue_style('dem_slider_owl_theme_min_css');
	wp_enqueue_script('dem_equalheight');
	wp_enqueue_script('dem_slider_owl_corousel_min_js');
	
	if ( file_exists( $dem_css_path . '/dem_slider_'.esc_attr( $dem_event_view_style ).'.css' ) )
	{
	  wp_enqueue_style('dem_slider_sc_'.esc_attr( $dem_event_view_style ), $dem_css_url.'/dem_slider_'.esc_attr( $dem_event_view_style ).'.css', array(), NULL);
	}else{
	  wp_enqueue_style('dem_slider_sc_'.esc_attr( $dem_event_view_style ), DEM_PLUGIN_URL.'assets/css/shortcode-layout-css/slider/dem_slider_'.esc_attr( $dem_event_view_style ).'.min.css', array(), NULL);
	}
	wp_localize_script( 'dem_slider_owl_corousel_min_js', 'dpevent_data',  array( 'column_layout' =>  $condition  ) );
	
	$event_view_content = '';
	
	if($event_posts ->have_posts()) :
	
	$style3_class = '';
	if ( $dem_event_view_style == 'style3'){ $style3_class = 'dem_list3'; } 
	$event_view_content .= '<div class="dem_slider_'.esc_attr( $dem_event_view_style ).' '.$style3_class.' owl-carousel owl-theme">';
	  	
	while($event_posts ->have_posts()) : 
	   $event_posts ->the_post(); 
	   if ( $dem_event_view_style == 'style1'){
			$dp_event_thumb = wp_get_attachment_image_src(get_post_thumbnail_id(), 'dem_slider1_500_232');	
	   } elseif ( $dem_event_view_style == 'style2'){
			$dp_event_thumb = wp_get_attachment_image_src(get_post_thumbnail_id(), 'dem_grid_400_400');
	   } elseif ( $dem_event_view_style == 'style3'){
			$dp_event_thumb = wp_get_attachment_image_src(get_post_thumbnail_id(), 'dem_slider3_400_300');
	   }	
		if( $dp_event_thumb[0] != ''){ 
			$image_path = $dp_event_thumb[0] ;
		}else{ 
			$image_path = DEM_PLUGIN_URL. '/assets/images/default.png';
		}
	
		$terms 		= get_the_terms( get_the_ID(), 'event_category' );
		$terms_meta = [];
		if ( ! empty( $terms ) ) {
			foreach ( $terms as $term ) { $terms_meta[] = $term->name; }
		}
	
		if ( ! empty( $terms_meta ) ) {
			$terms_string = implode( ', ', $terms_meta );
		} else {
			$terms_string = '';
		}
		
		ob_start();
		
		$event_thumbnail_image_url 		= $image_path;
		$event_permalink 				= get_the_permalink(get_the_ID());
		$event_title 					= get_the_title(get_the_ID());
		$event_start_date 				= get_post_meta(get_the_ID(), 'dp_event_start_date',true);
		$event_end_date 				= get_post_meta(get_the_ID(), 'dp_event_end_date',true);
		$event_start_time 				= get_post_meta(get_the_ID(), 'dp_event_start_time',true);
		$event_end_time 				= get_post_meta(get_the_ID(), 'dp_event_end_time',true);
		$event_venue					= get_post_meta(get_the_ID() ,'dpevent_address', true );
		$event_city                     = get_post_meta(get_the_ID() ,'dpevent_city', true );
		$event_state                    = get_post_meta(get_the_ID() ,'dpevent_state', true );
		$event_country                  = get_post_meta(get_the_ID() ,'dpevent_country', true );
		$event_ticket_cost 				= get_post_meta(get_the_ID(), 'dpevent_cost_name', true);
		$event_ticket_cost_currency		= get_post_meta(get_the_ID(), 'dp_event_currency_symbol_name', true);
		$event_ticket_currency_position	= get_post_meta(get_the_ID(), 'dp_event_currency_prefix_suffix', true);
		$event_readmore_btn_text		= et_get_option($dem_themename.'_dem_grid_readmore_text','Read More');
		$event_content 					= get_the_excerpt();   
			
		$event_venue_address = '';
		if( !empty ( $event_venue ) ){
			$event_venue_address .= $event_venue.', ';
		}
		if( !empty ( $event_city ) ){
			$event_venue_address .= $event_city.', ';
		}
		if( !empty ( $event_state ) ){
			$event_venue_address .= $event_state.', ';
		}
		if( !empty ( $event_country ) ){
			$event_venue_address .= $event_country.', ';
		}
		if ( file_exists( $dem_slider_path . '/dem_slider_'.esc_attr( $dem_event_view_style ).'.php' ) )
		{
			include $dem_slider_path. '/dem_slider_'.esc_attr( $dem_event_view_style ).'.php';
		}else{
			include DEM_PLUGIN_PATH. 'include/shortcode-layout-style/slider/dem_slider_'.esc_attr( $dem_event_view_style ).'.php';
		}
		$event_view_content .= ob_get_contents();
		ob_end_clean();
	
		 endwhile;
	     wp_reset_postdata(); 
	     $event_view_content .= "</div>";
	  endif;
	return $event_view_content;
}

add_shortcode( 'DP_Single_Event_Start_Date', 'dp_event_start_date' );
function dp_event_start_date( $atts ) {
	$dem_themename = dem_theme_name();
	$atts = shortcode_atts( array(
		'dem_start_date_format' 		=> '', 
	),$atts );
	$dem_start_date_format 			= esc_attr ( $atts['dem_start_date_format'] );
	$event_start_date           	= esc_attr ( get_post_meta(get_the_ID(), 'dp_event_start_date',true) );
	if ( $dem_start_date_format != '' && !empty($dem_start_date_format) ){
		return  date_i18n($dem_start_date_format, $event_start_date);
	}else{
		return  date_i18n('F d, Y', $event_start_date);
	}
}
add_shortcode( 'DP_Single_Event_End_Date', 'dp_event_end_date' );
function dp_event_end_date( $atts ) {
	$atts = shortcode_atts( array(
		'dem_end_date_format' 		=> '', 
	),$atts );
	$dem_end_date_format 			= $atts['dem_end_date_format'];
	$dp_event_end_date              = esc_attr ( get_post_meta(get_the_ID(), 'dp_event_end_date',true) );
	if ( $dem_end_date_format != '' && !empty($dem_end_date_format) ){
		return  date_i18n($dem_end_date_format, $dp_event_end_date);
	}else{
		return  date_i18n('F d, Y', $dp_event_end_date);
	}
}
add_shortcode( 'DP_Single_Event_Start_Time', 'dp_event_start_time' );
function dp_event_start_time( $atts ) {
	$dem_themename = dem_theme_name();
	$dem_display_time 			   = et_get_option($dem_themename.'_dem_display_time','twhr');
	$dp_event_start_time           = esc_attr ( get_post_meta(get_the_ID(), 'dp_event_start_time',true) );
	if ( $dp_event_start_time != '' && !empty($dp_event_start_time) ){
		if ( $dem_display_time == 'twhr' ){
			return  date_i18n("h:i a",  strtotime($dp_event_start_time));
		}else{
			return  date_i18n("H:i",  strtotime($dp_event_start_time));
		}
	}else{
		return  '-';
	}
}
add_shortcode( 'DP_Single_Event_End_Time', 'dp_event_end_time' );
function dp_event_end_time( $atts ) {
	$dem_themename = dem_theme_name();
	$dem_display_time = et_get_option($dem_themename.'_dem_display_time','twhr');
	$dp_event_end_time = esc_attr ( get_post_meta(get_the_ID(), 'dp_event_end_time',true) );
	if ( $dp_event_end_time != '' && !empty($dp_event_end_time) ){
		if ( $dem_display_time == 'twhr' ){
			return  date_i18n("h:i a",  strtotime($dp_event_end_time));
		}else{
			return  date_i18n("H:i",  strtotime($dp_event_end_time));
		}
	}else{
		return  '-';
	}
}
add_shortcode( 'DP_Single_Event_Category', 'dp_single_event_category' );
function dp_single_event_category( $atts ) {
	$event_cats          = get_the_terms( get_the_ID(), 'event_category' );
    $event_cat_meta = [];
    if ( ! empty( $event_cats ) ) {
        foreach ( $event_cats as $event_cat ) {
            $event_cat_meta[] = $event_cat->name;
        }
    }
    if ( ! empty( $event_cat_meta ) ) {
        $event_cat_string = implode( ', ', $event_cat_meta );
    } else {
        $event_cat_string = '';
    }
	if ( $event_cat_string != '' && !empty($event_cat_string) ){
		return  $event_cat_string;
	}else{
		return  '-';
	}
}
add_shortcode( 'DP_Single_Event_Tag', 'dp_single_event_tag' );
function dp_single_event_tag( $atts ) {
	$event_tags         = get_the_terms( get_the_ID(), 'event_tag' );
 	$event_tag_meta = [];
    if ( ! empty( $event_tags ) ) {
        foreach ( $event_tags as $event_tag ) { $event_tag_meta[] = $event_tag->name; }
    }
    if ( ! empty( $event_tag_meta ) ) {
        $event_tag_string = implode( ', ', $event_tag_meta );
    } else {
        $event_tag_string = '';
    }
	if ( $event_tag_string != '' && !empty($event_tag_string) ){
		return  $event_tag_string;
	}else{
		return  '-';
	}
}
add_shortcode( 'DP_Single_Event_Organizer_Name', 'dp_event_organizer_name' );
function dp_event_organizer_name( $atts ) {
	 $event_organizer_name           = get_post_meta(get_the_ID(), 'dpevent_organizer_name', true);
	if ( $event_organizer_name != '' && !empty($event_organizer_name) ){
		return  esc_attr ( $event_organizer_name );
	}else{
		return  '-';
	}
}
add_shortcode( 'DP_Single_Event_Email_Address', 'dp_event_email_address' );
function dp_event_email_address( $atts ) {
	 $dpevent_email_id           = get_post_meta(get_the_ID(), 'dpevent_email_id', true);
	if ( $dpevent_email_id != '' && !empty($dpevent_email_id) ){
		return  esc_attr ( $dpevent_email_id );
	}else{
		return  '-';
	}
}
add_shortcode( 'DP_Single_Event_Venue_Address', 'dp_event_venue_address' );
function dp_event_venue_address( $atts ) {
	$event_venue                  = get_post_meta(get_the_ID() ,'dpevent_address', true );
    $event_city                   = get_post_meta(get_the_ID() ,'dpevent_city', true );
    $event_state                  = get_post_meta(get_the_ID() ,'dpevent_state', true );
    $event_country                = get_post_meta(get_the_ID() ,'dpevent_country', true );
    $event_pincode                = get_post_meta(get_the_ID() ,'dpevent_pincode', true );
	$event_venue_address = '';
	if( !empty ( $event_venue ) ){
		$event_venue_address .= esc_attr ( $event_venue ).', ';
	}
	if( !empty ( $event_city )){
		$event_venue_address .= esc_attr ( $event_city ).', ';
	}
	if( !empty ( $event_pincode ) ){
		$event_venue_address .= esc_attr ( $event_pincode ).', ';
	}
	if( !empty ( $event_state ) ){
		$event_venue_address .= esc_attr ( $event_state ).', ';
	}
	if( !empty ( $event_country ) ){
		$event_venue_address .= esc_attr ( $event_country ).', ';
	}
	if ( $event_venue_address != '' && !empty( $event_venue_address ) ){
		return  rtrim($event_venue_address,', ');
	}else{
		return  '-';
	}
}
add_shortcode( 'DP_Single_Event_Phone_Number', 'dp_event_phone_number' );
function dp_event_phone_number( $atts ) {
    $event_phone_number             =  get_post_meta(get_the_ID(), 'dpevent_phone_number', true);
	if ( $event_phone_number != '' && !empty($event_phone_number) ){
		return  esc_attr ( $event_phone_number );
	}else{
		return  '-';
	}
}
add_shortcode( 'DP_Single_Event_Website', 'dp_event_website' );
function dp_event_website( $atts ) {
    $dpevent_website             = get_post_meta(get_the_ID(), 'dpevent_website', true);
	if ( $dpevent_website != '' && !empty($dpevent_website) ){
		return  esc_url( $dpevent_website );
	}else{
		return  '-';
	}
}
add_shortcode( 'DP_Single_Event_Price', 'dp_event_price' );
function dp_event_price( $atts ) {
    $event_cost_name                =  get_post_meta(get_the_ID(), 'dpevent_cost_name', true);
    $event_currency_prefix_suffix   =  get_post_meta(get_the_ID(), 'dp_event_currency_prefix_suffix', true);
    $event_currency_symbol_name     =  get_post_meta(get_the_ID(), 'dp_event_currency_symbol_name', true);
	
	if ( $event_cost_name && $event_currency_symbol_name){
		if($event_currency_prefix_suffix == 'suffix'){  
			return ' '.esc_attr ( $event_cost_name.$event_currency_symbol_name );
		} else { 
			return ' '.esc_attr ( $event_currency_symbol_name.$event_cost_name ); 
		}
	}else{
		return  '-';
	}
}
add_shortcode( 'DP_Single_Remaining_Ticket', 'dp_event_remaining_ticket' );
function dp_event_remaining_ticket( $atts ) {
    $event_noticket                 = get_post_meta(get_the_ID() ,'dpevent_noticket', true );
	if ( $event_noticket != '' && !empty($event_noticket) ){
		return  esc_attr ( $event_noticket );
	}else{
		return  '-';
	}
}
add_shortcode( 'DP_Single_Social_Icon', 'dp_event_social_icon' );
function dp_event_social_icon( $atts ) {
	$event_social_icon = '<ul class="et_pb_module et_pb_social_media_follow event_et_pb_social_media_follow"> <li> <a target="blank" href="http://www.facebook.com/share.php?u='.esc_url( get_permalink(get_the_ID()) ).'&title='.esc_attr ( get_the_title(get_the_ID()) ).'"><i class="et-pb-icon social-icon facebook-icon event_social_icon" >&#xe0aa;</i></a></li><li> <a target="blank" href="https://twitter.com/share?text='.rawurlencode(esc_attr ( get_the_title(get_the_ID())) ).'&url='.esc_url( get_permalink(get_the_ID()) ).'"><i class="et-pb-icon social-icon twitter-icon event_social_icon" >&#xe0ab;</i></a></li><li> <a target="blank" href="https://www.linkedin.com/shareArticle?mini=true&url='.esc_url( get_permalink(get_the_ID()) ).'&title='.rawurlencode(esc_attr ( get_the_title(get_the_ID()) )).'&source='.esc_url(home_url('/')).'"><i class="et-pb-icon social-icon linkedin-icon event_social_icon" >&#xe0b4;</i></a></li></ul>'; 
	return $event_social_icon;			
}
add_shortcode( 'DP_Single_Event_Map_Address', 'dp_event_map_address' );
function dp_event_map_address( $atts ) {
	$event_venue                  = get_post_meta(get_the_ID() ,'dpevent_address', true );
    $event_city                   = get_post_meta(get_the_ID() ,'dpevent_city', true );
    $event_state                  = get_post_meta(get_the_ID() ,'dpevent_state', true );
    $event_country                = get_post_meta(get_the_ID() ,'dpevent_country', true );
    $event_pincode                = get_post_meta(get_the_ID() ,'dpevent_pincode', true );
	$event_venue_address = '';
	if( !empty ( $event_venue ) ){
		$event_venue_address .= esc_attr ( $event_venue ).', ';
	}
	if( !empty ( $event_city )){
		$event_venue_address .= esc_attr ( $event_city ).', ';
	}
	if( !empty ( $event_pincode ) ){
		$event_venue_address .= esc_attr ( $event_pincode ).', ';
	}
	if( !empty ( $event_state ) ){
		$event_venue_address .= esc_attr ( $event_state ).', ';
	}
	if( !empty ( $event_country ) ){
		$event_venue_address .= esc_attr ( $event_country ).', ';
	}
	$seach_data = rtrim($event_venue_address,", "); 
	$map_iframe = '<iframe id="map_iframe"  frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.it/maps?q='.esc_attr ($seach_data).'&output=embed"></iframe>';
	return $map_iframe;
}
add_shortcode( 'DP_Single_Event_Gallery', 'dp_event_gallery' );
function dp_event_gallery( $atts ) {
$atts = shortcode_atts( array(
		'gallery_column' 		=> '4', 
	),$atts );
	$gallery_column 			= $atts['gallery_column'];
	$dp_event_gallery_html = '';
    $values = get_post_custom( get_the_ID() );
    $dpevent_cust_imagebox = isset( $values['dpevent_cust_image_id'] ) ? esc_attr( $values['dpevent_cust_image_id'][0] ) : "0";
	$i = 1;
	if( $dpevent_cust_imagebox != "0" ){ 
	$dp_event_gallery_html .= '<div class="popup-gallery">';
	for($i=0; $i<$dpevent_cust_imagebox+1; $i++ ):
           $dpevent_cust_gallery_upload_attach_id = isset( $values['dpevent_cust_imagebox'.$i] ) ? esc_attr( $values['dpevent_cust_imagebox'.$i][0] ) : "";
           $dpevent_cust_gallery_upload_image_src = wp_get_attachment_image_src( $dpevent_cust_gallery_upload_attach_id, 'medium');
		   $dpevent_cust_gallery_upload_image_src_large = wp_get_attachment_image_src( $dpevent_cust_gallery_upload_attach_id, 'large');
           $dpevent_cust_galleryCheckImg = "none";
           if ( !empty($dpevent_cust_gallery_upload_image_src[0]))
           {
                  $dpevent_cust_galleryCheckImg = "inline-block";
            }
           
           $dp_event_gallery_html .= '<a class="popup_col popup-col-'.$gallery_column.'" href="'.esc_url( $dpevent_cust_gallery_upload_image_src_large[0] ).'" title="'.esc_attr ( get_the_title(get_the_ID()) ).'"><img src="'.esc_url( $dpevent_cust_gallery_upload_image_src[0] ).'" alt="'. esc_attr ( get_the_title(get_the_ID()) ).'"> </a>';
    endfor; 
    $dp_event_gallery_html .= '</div>';
	}
	return $dp_event_gallery_html;
}
add_shortcode( 'DP_Single_Booking_Form', 'dp_event_booking_form' );
function dp_event_booking_form( $atts ) {
    $dem_themename 					= dem_theme_name();
	$dp_event_page_booking_en_ds 	= get_post_meta(get_the_ID(), 'dp_event_page_booking_en_ds', true );
	if ( $dp_event_page_booking_en_ds == 'default' ){
		$divi_dem_hide_form 			= et_get_option($dem_themename.'_dem_hide_form','off');
	}else if( $dp_event_page_booking_en_ds == 'Yes' ){
		$divi_dem_hide_form 			= 'off';
	}else if( $dp_event_page_booking_en_ds == 'No' ){
		$divi_dem_hide_form 			= 'on';
	}else{
		$divi_dem_hide_form 			= et_get_option($dem_themename.'_dem_hide_form','off');
	}
	$divi_dem_evt_suc_msg 			= et_get_option($dem_themename.'_dem_evt_suc_msg','Your Booking Order Placed Successfully!');
	$divi_dem_evt_fail_msg 			= et_get_option($dem_themename.'_dem_evt_fail_msg','Your Booking Order Placed Fail!');
	$divi_dem_evt_inq_suc_msg 		= et_get_option($dem_themename.'_dem_evt_inq_suc_msg','Your Inquiry Submitted Successfully!');
	$divi_dem_evt_inq_fail_msg 		= et_get_option($dem_themename.'_dem_evt_inq_fail_msg','Your Inquiry Is Fail!');
	$event_cost_name                = get_post_meta(get_the_ID(), 'dpevent_cost_name', true);
    $event_currency_prefix_suffix   = get_post_meta(get_the_ID(), 'dp_event_currency_prefix_suffix', true);
    $event_currency_symbol_name     = get_post_meta(get_the_ID(), 'dp_event_currency_symbol_name', true);
	$event_noticket                 = get_post_meta(get_the_ID() ,'dpevent_noticket', true );
	$dp_event_form_type      		= get_post_meta(get_the_ID(), 'dp_event_form_type', true);
	$event_end_date 				= get_post_meta(get_the_ID(), 'dp_event_end_date',true);
	$dem_event_hide_expired_booked 	= et_get_option($dem_themename.'_dem_event_hide_expired_booked','on');	
	if ( $dp_event_form_type != '' ){ $form_type = $dp_event_form_type ;}else{$form_type = 'default';}
	$success_msg = '';
    if($event_currency_prefix_suffix == 'suffix'){ 
   		 $ticket_prices = $event_cost_name.' '.$event_currency_symbol_name;
    } else { 
  		 $ticket_prices = $event_currency_symbol_name.' '.$event_cost_name; 
    }
	if ( $divi_dem_hide_form != 'on' ){
	$divi_dem_paypal_mode 			= et_get_option($dem_themename.'_dem_paypal_mode','Live');
	$divi_dem_paypal_email_address  = et_get_option($dem_themename.'_dem_paypal_email_address','');
	$divi_dem_display_form 			= et_get_option($dem_themename.'_dem_display_form','Booking');
	$divi_dem_frm_name 					= et_get_option($dem_themename.'_dem_frm_name','Name');
	$divi_dem_frm_emailaddress 			= et_get_option($dem_themename.'_dem_frm_emailaddress','Email Address');
	$divi_dem_frm_telno					= et_get_option($dem_themename.'_dem_frm_telno','Telephone no');
	$divi_dem_frm_no_of_tickets 		= et_get_option($dem_themename.'_dem_frm_no_of_tickets','Number of Tickets');
	$divi_dem_frm_price_tickets 		= et_get_option($dem_themename.'_dem_frm_price_tickets','Ticket Price'); 
	$divi_dem_hide_tel_no_frm 			= et_get_option($dem_themename.'_dem_hide_tel_no_frm','off');
	$divi_dem_price_inquiry_form		= et_get_option($dem_themename.'_dem_price_inquiry_form','off');	
	$dem_event_fully_booked 			= et_get_option($dem_themename.'_dem_event_fully_booked','on');	
	if ( $form_type == 'default' ){
		$divi_dem_display_form = $divi_dem_display_form ;
	}else if ( $form_type == 'free' ){
		$divi_dem_display_form = 'Inquiry' ;
	}else if ( $form_type == 'paid' ){ 
		$divi_dem_display_form = 'Booking' ;
	}else{
		$divi_dem_display_form = $divi_dem_display_form ;
	}
	global $wpdb;
	$table = $wpdb->prefix.'dem_order';
	if($divi_dem_display_form == 'Booking'){
		if ( isset( $_GET['_wpnonce'] ) && !wp_verify_nonce( sanitize_key( $_GET['_wpnonce'] ), 'dem_response_verify' ) ) {
			$success_msg = $divi_dem_evt_fail_msg;
		}else{
				if ( isset($_REQUEST['payment_status']) && $_REQUEST['payment_status'] == 'Completed'){
					$dem_order_id = isset( $_REQUEST["oid"] ) ? intval( $_REQUEST["oid"] ) : '';
					if ( !empty ( $dem_order_id ) ){
						$payment_status = isset( $_REQUEST["payment_status"] ) ? sanitize_key( $_REQUEST["payment_status"] ) : '';
						$item_number = isset( $_REQUEST["item_number"] ) ? intval ( $_REQUEST["item_number"] ) : ( isset( $_REQUEST["item_number1"] ) ?  intval( $_REQUEST["item_number1"] ) : '' );
						$buy_ticket = isset( $_REQUEST["quantity"] ) ? intval ( $_REQUEST["quantity"] ) : ( isset( $_REQUEST["quantity1"] ) ?  intval( $_REQUEST["quantity1"] ) : '' );
						$wpdb->update($table, array('dem_booking_status'=>$payment_status ), array('dem_booking_id' =>$dem_order_id));
						$dpevent_noticket     =  get_post_meta($item_number,'dpevent_noticket', true );
						$remaining_ticket 	  =  $dpevent_noticket - $buy_ticket;
						update_post_meta( $item_number, 'dpevent_noticket', esc_attr ($remaining_ticket) );
						
						$order_detail = $wpdb->get_row( $wpdb->prepare( "SELECT  *  FROM {$wpdb->prefix}dem_order WHERE  dem_booking_id= %d ",$item_number), ARRAY_A  ); 
							if ( ! empty($order_detail) ){
							$dem_detail_name 							= $order_detail['dem_user_name'];
							$dem_detail_email_id 						= $order_detail['dem_user_email'];
							$dem_detail_phone_no 						= $order_detail['dem_user_tel'];
							$dem_detail_event_id 						= $order_detail['dem_event_id'];
							$dem_detail_event_title 					= $order_detail['dem_event_title'];
							$dem_detail_ticket_currency_symbol_name 	= $order_detail['dem_event_currency'];
							$dem_detail_ticket_currency_symbol_position = $order_detail['dem_event_cu_pos'];
							$dem_detail_single_ticket_prices 			= $order_detail['dem_event_ticket_price'];
							$dem_detail_no_of_ticket 					= $order_detail['dem_event_no_of_ticket'];
							$dem_detail_total_ticket_price 				= $order_detail['dem_event_total_price'];
							$dem_booking_date 							= $order_detail['dem_booking_date'];
							$dem_booking_status 						= $order_detail['dem_booking_status'];
							
							$enable_email_customization 	= dem_email_customization_op_get_value('enable_email_customization','No');
							$cc_email_customization 		= dem_email_customization_op_get_value('cc_email_customization','');
							
							$useremail_content   			= dem_email_customization_op_get_value('b_useremail','');
							$adminemail_content   			= dem_email_customization_op_get_value('b_adminemail','');
							$admin_subject_email_customization = dem_email_customization_op_get_value('b_admin_subject_email_customization',''); 
							$user_subject_email_customization  = dem_email_customization_op_get_value('b_user_subject_email_customization','');
							
							$field_custom_field1 		= get_post_meta( $dem_detail_event_id, 'dpevent_custom_field1', true );
							$field_custom_field2 		= get_post_meta( $dem_detail_event_id, 'dpevent_custom_field2', true );
							$field_custom_field3 		= get_post_meta( $dem_detail_event_id, 'dpevent_custom_field3', true );
							
							
							if ( $enable_email_customization == 'Yes' && $admin_subject_email_customization != '' && $adminemail_content != '' && $user_subject_email_customization != '' && $useremail_content !='' ){
							
								$subject_items_for_replacement = array('{field_email_name}', '{field_email_eventname}' );
								$subject_replacement_items = array($dem_detail_name,$dem_detail_event_title );
								
								 $items_for_replacement = array('{field_email_name}',
								 '{field_email_emailaddress}',
								 '{field_email_phone}',
								 '{field_email_eventname}',
								 '{field_email_eventid}',
								 '{field_email_price}',
								 '{field_email_nooftickets}',
								 '{field_email_totalprice}',
								 '{field_email_status}',
								 '{field_email_date}',
								 '{field_custom_field1}',
								 '{field_custom_field2}',
								 '{field_custom_field3}'
								 );
								 $replacement_items = array($dem_detail_name,
								 $dem_detail_email_id,
								 $dem_detail_phone_no,
								 $dem_detail_event_title,
								 $dem_detail_event_id,
								 $dem_detail_ticket_currency_symbol_name." ".$dem_detail_single_ticket_prices,
								 $dem_detail_no_of_ticket,
								 $dem_detail_total_ticket_price,
								 $dem_booking_status,
								 $dem_booking_date,
								 $field_custom_field1,
								 $field_custom_field2,
								 $field_custom_field3
								 );
								 $adminemail_content_msg = '';$useremail_content_msg= '';
								 //Admin Email
								 $admin_subject_email_customization_msg = str_replace($subject_items_for_replacement, $subject_replacement_items, $admin_subject_email_customization);
								 $adminemail_content_msg .= str_replace($items_for_replacement, $replacement_items, $adminemail_content);
								 
								 $enable_admin_email_customization = dem_email_customization_op_get_value('enable_admin_email_customization','No');
								 $dpevent_admin_email_id_value 		= get_post_meta( $dem_detail_event_id, 'dpevent_admin_email_id', true );
								 if ( $enable_admin_email_customization == 'Yes' && !empty($dpevent_admin_email_id_value ) ){
									 $admin_to = $dpevent_admin_email_id_value;
								 }else{
									$admin_to = get_option('admin_email');
								 }
								 $admin_from = $dem_detail_name." <".$dem_detail_email_id.">";
								 $admin_subject = $admin_subject_email_customization_msg;
								 $admin_txt = $adminemail_content_msg;
								 $plain_txt = $adminemail_content_msg;
								 
								 # Setup mime boundary
								$mime_boundary = 'Multipart_Boundary_x'.md5(time()).'x';
								
								# Setup header
								$headers  = "From: {$from}\n";
								if ( $cc_email_customization != '' ){
								$headers .= "Cc: {$cc_email_customization}\n";
								}
								$headers .= "MIME-Version: 1.0\n";
								$headers .= "Content-Type: multipart/alternative; boundary=\"{$mime_boundary}\"\r\n";
								$headers .= "Content-Transfer-Encoding: 7bit\r\n";
								
								$message  = "This is a multi-part message in mime format.\n\n";
									
								# Add in plain text version
								$message.= "--{$mime_boundary}\n";
								$message.= "Content-Type: text/plain; charset=\"charset=us-ascii\"\n";
								$message.= "Content-Transfer-Encoding: 7bit\n\n";
								$message.= $plain_txt;
								$message.= "\n\n";
								
								# Add in HTML version
								$message.= "--{$mime_boundary}\n";
								$message.= "Content-Type: text/html; charset=\"UTF-8\"\n";
								$message.= "Content-Transfer-Encoding: 7bit\n\n";
								$message.= $admin_txt;
								$message.= "\n\n";
								
								# End email
								$message .= "--{$mime_boundary}--\n";;
						
								$result = mail($admin_to, $admin_subject, $message, $headers);
								 
								 // User Email
								$user_subject_email_customization_msg = str_replace($subject_items_for_replacement, $subject_replacement_items, $user_subject_email_customization);
								$useremail_content_msg .= str_replace($items_for_replacement, $replacement_items, $useremail_content);
								$email_to = $dem_detail_email_id;
								$email_from = "<".get_option('admin_email').">";
								$email_subject = $user_subject_email_customization_msg;
								$email_txt = $useremail_content_msg;
								$email_plain_txt = $useremail_content_msg;
							
								# Setup mime boundary
								$email_mixed_boundary = 'Multipart_Boundary_x'.md5(time()).'x';
								
								# Setup header
								$email_headers  = "From: {$email_from}\n";
								$email_headers .= "MIME-Version: 1.0\n";
								$email_headers .= "Content-Type: multipart/alternative; boundary=\"{$email_mixed_boundary}\"\n";
								$email_headers .= "Content-Transfer-Encoding: 7bit\n";
								
								$email_message  = "This is a multi-part message in mime format.\n\n";
									
								# Add in plain text version
								$email_message .= "--{$email_mixed_boundary}\n";
								$email_message .= "Content-Type: text/plain; charset=\"charset=us-ascii\"\n";
								$email_message .= "Content-Transfer-Encoding: 7bit\n\n";
								$email_message .= $email_plain_txt;
								$email_message .= "\n\n";
								
								# Add in HTML version
								$email_message .= "--{$email_mixed_boundary}\n";
								$email_message .= "Content-Type: text/html; charset=\"UTF-8\"\n";
								$email_message .= "Content-Transfer-Encoding: 7bit\n\n";
								$email_message .= $email_txt;
								$email_message .= "\n\n";
								
								# End email
								$email_message .= "--{$email_mixed_boundary}--\n";
								$resultr = mail($email_to, $email_subject, $email_message, $email_headers);
								
							}
						}
						$success_msg = $divi_dem_evt_suc_msg;
						
					 ?>
					 <script>
						setTimeout(function(){  window.location.href = "<?php echo esc_url( get_permalink( get_the_ID() ).'?suc=1#dem_detail_ticket_booking' ); ?>"; }, 500);
					</script>
			<?php } 
			 }
		}
	}
	if($divi_dem_display_form == 'Inquiry'){
		if ( isset( $_GET['_wpnonce'] ) && !wp_verify_nonce( sanitize_key( $_GET['_wpnonce'] ), 'dem_response_verify' ) ) {
			$success_msg =$divi_dem_evt_inq_fail_msg;
		}else{
				if ( isset($_REQUEST['Inquiry']) && $_REQUEST['Inquiry'] == 'verify' && (isset( $_REQUEST["InquiryId"] ) && is_numeric( $_REQUEST['InquiryId'] )) ){
				$dem_rt_if_form = et_get_option($dem_themename.'_dem_rt_if_form','off');
				if (  $dem_rt_if_form == 'on' ){
					$dem_order_id = isset( $_REQUEST["InquiryId"] ) ? intval( $_REQUEST["InquiryId"] ) : '';
					$InquiryEventId = isset( $_REQUEST["InquiryEventId"] ) ? intval( $_REQUEST["InquiryEventId"] ) : '';
					$buy_ticket = isset( $_REQUEST["InquiryTicketQty"] ) ? intval( $_REQUEST["InquiryTicketQty"] ) : '';
					$dpevent_noticket     = get_post_meta($InquiryEventId ,'dpevent_noticket', true );
					$remaining_ticket 	  = $dpevent_noticket - $buy_ticket;
					update_post_meta( $InquiryEventId, 'dpevent_noticket', esc_attr ( $remaining_ticket ) );
				}
				$success_msg = $divi_dem_evt_inq_suc_msg;
				?>
				<script>
						setTimeout(function(){  window.location.href = "<?php echo esc_url( get_permalink( get_the_ID() ).'?suc=1#dem_detail_ticket_booking' ); ?>"; }, 500);
				</script>
				<?php
				}
		}
	}
	if( isset($_REQUEST['suc']) && $_REQUEST['suc'] == '1' && is_numeric($_REQUEST['suc']) ){
		if($divi_dem_display_form == 'Booking'){
			$success_msg = $divi_dem_evt_suc_msg;
		}else{
			$success_msg = $divi_dem_evt_inq_suc_msg;
		}
	}
	ob_start();
    ?>    
    <div class="dem_detail1_ticket_booking" id="dem_detail_ticket_booking">
        <div class="et_pb_module et_pb_text dem_detail1_ticket_booking_title">
            <h3 class= "dem_detail1_ticket_booking_title_text">  <?php 
				$divi_dem_evt_tickets = et_get_option($dem_themename.'_dem_evt_tickets','Get Tickets');
				esc_html_e( $divi_dem_evt_tickets, 'dpevent' ); ?>
			</h3>
        </div>
        <div id="dem_detail1_contact_form">
				<?php  if($success_msg){ ?><div class="et-pb-contact-message"> <?php echo "<p>".esc_html($success_msg)."</p>"; ?> </div> <?php }?>
                <div class="et_pb_contact">
                <h6 class="dem_avilable_ticket"><?php 
				$divi_dem_evt_rm_tickets = et_get_option($dem_themename.'_dem_evt_rm_tickets','Remaining Tickets');
				esc_html_e( $divi_dem_evt_rm_tickets, 'dpevent' ); ?>: <strong><?php echo esc_attr ($event_noticket); ?></strong>
				</h6>
                   
                <?php if($divi_dem_display_form == 'Booking'){
                        $get_action_option = "https://www.sandbox.paypal.com/cgi-bin/webscr";
                        if ( $divi_dem_paypal_mode == 'Live'){
                            $get_action_option = "https://www.paypal.com/cgi-bin/webscr";
                       }

                  ?>     
                    <form id="dem_ticket_booking_paypal" class=" clearfix" method="post" action="<?php echo esc_url ( $get_action_option ); ?>">
                        <?php 
                            $book_nonce = wp_create_nonce( 'dem_ticket_booking_paypal_nonce' );
                        ?>
                       
                        <p class="et_pb_contact_field dem_ticket_booking_left">
                            <label for="dem_detail_name" class="et_pb_contact_form_label"><?php esc_html_e( $divi_dem_frm_name, 'dpevent' ); ?></label>
                            <input type="text" id="dem_detail_name" class="input" value="" name="dem_detail_name"  placeholder="<?php esc_html_e( $divi_dem_frm_name, 'dpevent' ); ?>" required>
                        </p>

                        <p class="et_pb_contact_field dem_ticket_booking_left" >
                            <label for="dem_detail_email_id" class="et_pb_contact_form_label"><?php esc_html_e( $divi_dem_frm_emailaddress, 'dpevent' ); ?></label>
                            <input type="email" id="dem_detail_email_id" class="input" value="" name="dem_detail_email_id"  placeholder="<?php esc_html_e( $divi_dem_frm_emailaddress, 'dpevent' ); ?>" required>
                        </p>
						<?php if ( $divi_dem_hide_tel_no_frm == 'on' ){?>
							<input type="hidden" id="dem_detail_phone_no" class="input" value="1111111111" name="dem_detail_phone_no" placeholder="<?php esc_html_e( $divi_dem_frm_telno, 'dpevent' ); ?>" maxlength="15" minlength="10" required>
						<?php }else{ ?>
                        <p class="et_pb_contact_field dem_ticket_booking_left" >
                            <label for="dem_detail_phone_no" class="et_pb_contact_form_label"><?php esc_html_e( $divi_dem_frm_telno, 'dpevent' ); ?></label>
                            <input type="text" id="dem_detail_phone_no" class="input" value="" name="dem_detail_phone_no" placeholder="<?php esc_html_e( $divi_dem_frm_telno, 'dpevent' ); ?>" maxlength="10" minlength="10" required>
                        </p>
						<?php } ?>
                       <p class="et_pb_contact_field dem_ticket_booking_left" >
                            <label for="dem_detail_no_of_ticket" class="et_pb_contact_form_label"><?php esc_html_e( $divi_dem_frm_no_of_tickets, 'dpevent' ); ?></label>
                            <select id="dem_detail_no_of_ticket" class="et_pb_contact_select input" name="dem_detail_no_of_ticket" >
                                <?php 
                                    $dpevent_noticket_allowtobook   = get_post_meta(get_the_ID(), 'dpevent_noticket_allowtobook', true);
									$dpevent_noticket_allowtobook_qty 	= ( $dpevent_noticket_allowtobook != '' ) ? $dpevent_noticket_allowtobook : '10';
									 $x = 1;$max_no_tickets = esc_attr($dpevent_noticket_allowtobook_qty);
                                     for ($x = 1; $x<= $max_no_tickets; $x++) {  
                                ?>
                                    <option value="<?php echo esc_attr($x); ?>"><?php echo esc_attr($x); ?></option>
                                <?php
                                     }
                                ?>
                            </select>
                        </p>                  
                        <p class="et_pb_contact_field   et_pb_contact_field_half dem_ticket_booking_left" data-id="ticket_price" data-type="ticket_price">
                            <label for="dem_detail_ticket_price" class="et_pb_contact_form_label"><?php esc_html_e( $divi_dem_frm_price_tickets, 'dpevent' ); ?></label>
                            <input type="text" id="dem_detail_total_ticket_price" class="input" value="<?php echo esc_attr( $ticket_prices ); ?>" name="dem_detail_total_ticket_price" readonly>
                        </p>
                             <input type="hidden" id="dem_detail_single_ticket_prices" name="dem_detail_single_ticket_prices" value="<?php echo esc_attr($event_cost_name); ?>">
                            <input type="hidden" id="dem_detail_ticket_currency_symbol_position" name="dem_detail_ticket_currency_symbol_position" value="<?php echo esc_attr( $event_currency_prefix_suffix ); ?>">
                            <input type="hidden" id="dem_detail_ticket_currency_symbol_name" name="dem_detail_ticket_currency_symbol_name" value="<?php echo esc_attr($event_currency_symbol_name ); ?>">
                            <input type="hidden" id="dem_detail_event_id" name="dem_detail_event_id" value="<?php echo esc_attr ( get_the_ID() ); ?>">
                            <input type="hidden" id="dem_detail_event_title" name="dem_detail_event_title" value="<?php echo esc_attr ( get_the_title() ); ?>">
                            <input type="hidden" id="dem_detail_event_order_date" name="dem_detail_event_order_date" value="<?php echo esc_attr( gmdate("Y-m-d") ); ?>">
                            <input type='hidden' value='<?php echo esc_attr ($book_nonce); ?>' name='dem_ticket_booking_paypal' id="dem_ticket_booking_paypal_nonce"/>

                            <input type="hidden" name="cmd" value="_xclick">
                            <input type="hidden" name="rm" value="2" />
                            <input type="hidden" name="charset" value="utf-8"/>
                            <input type="hidden" name="business" value="<?php echo esc_attr ( $divi_dem_paypal_email_address ); ?>">
                            <input type="hidden" name="item_name" value="<?php echo esc_attr ( get_the_title( get_the_ID() ) ); ?>">
                            <input type="hidden" name="item_number" value="<?php echo esc_attr ( get_the_ID() ); ?>">
                            <input type="hidden" name="amount" value="<?php echo esc_attr ( $event_cost_name ); ?>"><!-- Rs -->
                            <input type="hidden" name="quantity" value="1"><!-- number of ticket --> 
                            <input type="hidden" name="currency_code" value="<?php echo esc_attr ( $event_currency_symbol_name ); ?>">
                            <input type="hidden" name="no_shipping" value="0">
                            <input type="hidden" name="no_note" value="1">
                            <input type="hidden" name="lc" value="US">
							<?php $dem_paypal_return_url = et_get_option($dem_themename.'_dem_paypal_return',esc_url ( get_permalink(get_the_ID()) ));
								if ( $dem_paypal_return_url != '' ){$dem_paypal_return_url = esc_url ($dem_paypal_return_url);}else{$dem_paypal_return_url = esc_url ( get_permalink(get_the_ID()) );}
								?>
                            <input name = "return" value = "<?php echo esc_url ($dem_paypal_return_url ); ?>" type = "hidden">   
                            <input name = "cancel_return" value = "<?php echo  esc_url ( get_permalink(get_the_ID()) ); ?>" type = "hidden">
                        <div>
						<?php $divi_dem_evt_btn = et_get_option($dem_themename.'_dem_evt_btn','Pay with PayPal!');
						$dem_evt_fully_booked_msg = et_get_option($dem_themename.'_dem_evt_fully_booked_msg','This Event is fully booked.Please contact to website Owner');
						if ( $dem_event_fully_booked == 'on' && $event_noticket == 0 ){
							 echo esc_html_e( $dem_evt_fully_booked_msg, 'dpevent' ); 
						}else{
							    $dem_current_date = gmdate('d-m-Y');
								$dem_evt_expired_msg = et_get_option($dem_themename.'_dem_evt_expired_msg','This Event is Expired.Please contact to website Owner');	
								if ( $dem_event_hide_expired_booked == 'on' &&  strtotime($dem_current_date) > $event_end_date ){
									 echo esc_html_e( $dem_evt_expired_msg, 'dpevent' ); 
									
								}else{
								?>
								<input type="submit" class="et_pb_contact_submit dem_events_paypal_button" value="<?php esc_html_e( $divi_dem_evt_btn, 'dpevent' );?>" >
								<?php
								}
						} ?>
                        </div>
                    </form>
                <?php }
                else{
                    ?>
                   <form id="dem_ticket_inquiry" class=" clearfix" method="post" action="#" >
                        <?php 
                            $book_nonce = wp_create_nonce( 'dem_ticket_booking_paypal_nonce' );
                        ?>
                        <p class="et_pb_contact_field dem_ticket_booking_left">
                            <label for="dem_detail_name" class="et_pb_contact_form_label"><?php esc_html_e( $divi_dem_frm_name, 'dpevent' ); ?></label>
                            <input type="text" id="dem_detail_name" class="input" value="" name="dem_detail_name"  placeholder="<?php esc_html_e( $divi_dem_frm_name, 'dpevent' ); ?>" required>
                        </p>

                        <p class="et_pb_contact_field dem_ticket_booking_left" >
                            <label for="dem_detail_email_id" class="et_pb_contact_form_label"><?php esc_html_e( $divi_dem_frm_emailaddress, 'dpevent' ); ?></label>
                            <input type="email" id="dem_detail_email_id" class="input" value="" name="dem_detail_email_id"  placeholder="<?php esc_html_e( $divi_dem_frm_emailaddress, 'dpevent' ); ?>" required>
                        </p>
						<?php if ( $divi_dem_hide_tel_no_frm == 'on' ){?>
							<input type="hidden" id="dem_detail_phone_no" class="input" value="1111111111" name="dem_detail_phone_no" placeholder="<?php esc_html_e( $divi_dem_frm_telno, 'dpevent' ); ?>" maxlength="15" minlength="10" required>
						<?php }else{ ?>
                        <p class="et_pb_contact_field dem_ticket_booking_left" >
                            <label for="dem_detail_phone_no" class="et_pb_contact_form_label"><?php esc_html_e( $divi_dem_frm_telno, 'dpevent' ); ?></label>
                            <input type="text" id="dem_detail_phone_no" class="input" value="" name="dem_detail_phone_no" placeholder="<?php esc_html_e( $divi_dem_frm_telno, 'dpevent' ); ?>"  maxlength="15" minlength="10" required>
                        </p>
						<?php } 
						$dem_rt_if_form = et_get_option($dem_themename.'_dem_rt_if_form','off');
						if (  $dem_rt_if_form == 'on' ){ ?>
						 <p class="et_pb_contact_field dem_ticket_booking_left" >
                            <label for="dem_detail_no_of_ticket" class="et_pb_contact_form_label"><?php esc_html_e( $divi_dem_frm_no_of_tickets, 'dpevent' ); ?></label>
                            <select id="dem_detail_no_of_ticket" class="et_pb_contact_select input" name="dem_detail_no_of_ticket" >
                                <?php 
                                    $dpevent_noticket_allowtobook   = get_post_meta(get_the_ID(), 'dpevent_noticket_allowtobook', true);
									$dpevent_noticket_allowtobook_qty 	= ( $dpevent_noticket_allowtobook != '' ) ? $dpevent_noticket_allowtobook : '10';
									 $x = 1;$max_no_tickets = esc_attr($dpevent_noticket_allowtobook_qty);
                                     for ($x = 1; $x<= $max_no_tickets; $x++) {  
                                ?>
                                    <option value="<?php echo esc_attr($x); ?>"><?php echo esc_attr($x); ?></option>
                                <?php
                                     }
                                ?>
                            </select>
                        </p> 
						 <?php } if (  $divi_dem_price_inquiry_form == 'on' && $dem_rt_if_form == 'on' ){ ?>
						 <p class="et_pb_contact_field   et_pb_contact_field_half dem_ticket_booking_left" data-id="ticket_price" data-type="ticket_price">
                            <label for="dem_detail_ticket_price" class="et_pb_contact_form_label"><?php esc_html_e( $divi_dem_frm_price_tickets, 'dpevent' ); ?></label>
                            <input type="text" id="dem_detail_total_ticket_price" class="input" value="<?php echo esc_attr( $ticket_prices ); ?>" name="dem_detail_total_ticket_price" readonly>
                        </p>
                             <input type="hidden" id="dem_detail_single_ticket_prices" name="dem_detail_single_ticket_prices" value="<?php echo esc_attr($event_cost_name); ?>">
                            <input type="hidden" id="dem_detail_ticket_currency_symbol_position" name="dem_detail_ticket_currency_symbol_position" value="<?php echo esc_attr( $event_currency_prefix_suffix ); ?>">
                            <input type="hidden" id="dem_detail_ticket_currency_symbol_name" name="dem_detail_ticket_currency_symbol_name" value="<?php echo esc_attr($event_currency_symbol_name ); ?>">
							<?php } ?>
							
                            <input type="hidden" id="dem_detail_event_id" name="dem_detail_event_id" value="<?php echo  esc_attr ( get_the_ID() ); ?>">
                            <input type="hidden" id="dem_detail_event_title" name="dem_detail_event_title" value="<?php echo esc_attr ( get_the_title( get_the_ID() ) ); ?>">
                            <input type="hidden" id="dem_detail_event_order_date" name="dem_detail_event_order_date" value="<?php echo esc_attr(gmdate("Y-m-d")); ?>">
                            <input type='hidden' value='<?php echo esc_attr($book_nonce); ?>' name='dem_ticket_booking_paypal' id="dem_ticket_booking_paypal_nonce"/>
							<?php $dem_paypal_return_url = et_get_option($dem_themename.'_dem_paypal_return',esc_url ( get_permalink(get_the_ID()) ));
								if ( $dem_paypal_return_url != '' ){$dem_paypal_return_url = esc_url ($dem_paypal_return_url);}else{$dem_paypal_return_url = esc_url ( get_permalink(get_the_ID()) );}
							?>
                            <input name="return" value= "<?php echo esc_url( $dem_paypal_return_url ); ?>" type = "hidden"> 
							
                            <div>
							<?php $divi_dem_evt_btn = et_get_option($dem_themename.'_dem_evt_btn','Pay with PayPal!');
							$dem_evt_fully_booked_msg = et_get_option($dem_themename.'_dem_evt_fully_booked_msg','This Event is fully booked.Please contact to website Owner');
							if ( $dem_event_fully_booked == 'on' && $event_noticket == 0 ){
								 echo esc_html_e( $dem_evt_fully_booked_msg, 'dpevent' ); 
							}else{
								$dem_current_date = gmdate('d-m-Y');
								$dem_evt_expired_msg = et_get_option($dem_themename.'_dem_evt_expired_msg','This Event is Expired.Please contact to website Owner');	
								if ( $dem_event_hide_expired_booked == 'on' &&  strtotime($dem_current_date) > $event_end_date ){
									 echo esc_html_e( $dem_evt_expired_msg, 'dpevent' ); 
									
								}else{
								?>
								<input type="submit" class="et_pb_contact_submit dem_events_paypal_button" value="<?php esc_html_e( $divi_dem_evt_btn, 'dpevent' );?>" >
								<?php
								}
							} ?> 
                        </div>  
                    </form>
                  <?php } ?>    
                </div> <!-- .et_pb_contact -->
        </div> <!-- .et_pb_contact_form_container -->
    </div> 
<?php } 
	$dp_event_booking_form = ob_get_contents();
	ob_end_clean();
	return $dp_event_booking_form;
}
?>