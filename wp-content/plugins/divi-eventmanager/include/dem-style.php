<?php
	$divi_pg_enable_style = dem_general_view_op_get_value('divi_pg_enable_style','No');
	$divi_slider_enable_style = dem_general_view_op_get_value('divi_slider_enable_style','No');
	
	$divi_grid_style1_enable_style = dem_grid_view_op_get_value('divi_grid_style1_enable_style','No');
	$divi_grid_style2_enable_style = dem_grid_view_op_get_value('divi_grid_style2_enable_style','No');
	$divi_grid_style3_enable_style = dem_grid_view_op_get_value('divi_grid_style3_enable_style','No');
	
	 $divi_list_style1_enable_style = dem_list_view_op_get_value('divi_list_style1_enable_style','No');
	 $divi_list_style2_enable_style = dem_list_view_op_get_value('divi_list_style2_enable_style','No');
	 $divi_list_style3_enable_style = dem_list_view_op_get_value('divi_list_style3_enable_style','No');
	 
	 $divi_slider_style1_enable_style = dem_slider_view_op_get_value('divi_slider_style1_enable_style','No');
	 $divi_slider_style2_enable_style = dem_slider_view_op_get_value('divi_slider_style2_enable_style','No');
	 $divi_slider_style3_enable_style = dem_slider_view_op_get_value('divi_slider_style3_enable_style','No');
	 
	 $divi_detailpage_style1_enable_style = dem_detail_view_op_get_value('divi_detailpage_style1_enable_style','No');
	 $divi_detailpage_style2_enable_style = dem_detail_view_op_get_value('divi_detailpage_style2_enable_style','No');
	 $divi_detailpage_custom_enable_style = dem_detail_view_op_get_value('divi_detailpage_custom_enable_style','No');
	 
	 if ( $divi_pg_enable_style ==  'Yes') {
			$divi_dem_pg_active_text_color = dem_general_view_op_get_value('divi_dem_pg_active_text_color','#40d9f1');
			$divi_dem_pg_text_color = dem_general_view_op_get_value('divi_dem_pg_text_color','#fff');
			$divi_dem_pg_bg_color = dem_general_view_op_get_value('divi_dem_pg_bg_color','#000');
	 }
	 if ( $divi_slider_enable_style ==  'Yes') {
		$divi_dem_slider_active_dot_color = dem_general_view_op_get_value('divi_dem_slider_active_dot_color','#869791');
		$divi_dem_slider_dot_color = dem_general_view_op_get_value('divi_dem_slider_dot_color','#D6D6D6');
		$divi_dem_slider_dot_hover_color = dem_general_view_op_get_value('divi_dem_slider_dot_hover_color','#869791'); 
	}
	if ( $divi_grid_style1_enable_style ==  'Yes') {
		$divi_grid_style1_heading_color = dem_grid_view_op_get_value('divi_grid_style1_heading_color','#fff');
		$divi_grid_style1_address_color = dem_grid_view_op_get_value('divi_grid_style1_address_color','#fff');
		$divi_grid_style1_date_color = dem_grid_view_op_get_value('divi_grid_style1_date_color','#fff');
		$divi_grid_style1_bk_overlay_color = dem_grid_view_op_get_value('divi_grid_style1_bk_overlay_color','#0000008a');
		$divi_grid_style1_boxshaow_hover_color = dem_grid_view_op_get_value('divi_grid_style1_boxshaow_hover_color','#5B1F78');
		$divi_grid_style1_boxshaow_hover_color_hex2rgba = dem_hex2rgba($divi_grid_style1_boxshaow_hover_color, 0.7);
	}
	if ( $divi_grid_style2_enable_style ==  'Yes') {
		$divi_grid_style2_heading_color = dem_grid_view_op_get_value('divi_grid_style2_heading_color','#333');
		$divi_grid_style2_address_color = dem_grid_view_op_get_value('divi_grid_style2_address_color','#666'); 
		$divi_grid_style2_price_color = dem_grid_view_op_get_value('divi_grid_style2_price_color','#ed4a43');
		$divi_grid_style2_btn_text_color = dem_grid_view_op_get_value('divi_grid_style2_btn_text_color','#333');
		$divi_grid_style2_btn_bk_color = dem_grid_view_op_get_value('divi_grid_style2_btn_bk_color','#fff');
		$divi_grid_style2_btn_text_hover_color = dem_grid_view_op_get_value('divi_grid_style2_btn_text_hover_color','#fff');
		$divi_grid_style2_btn_bk_hover_color = dem_grid_view_op_get_value('divi_grid_style2_btn_bk_hover_color','#ed4a43');
		$divi_grid_style2_btn_border_color = dem_grid_view_op_get_value('divi_grid_style2_btn_border_color','#ed4a43');
		$divi_grid_style2_border_color = dem_grid_view_op_get_value('divi_grid_style2_border_color','#999');
		$divi_grid_style2_boxshadow_color = dem_grid_view_op_get_value('divi_grid_style2_boxshadow_color','#00000038');
		$divi_grid_style2_boxshadow_hover_color = dem_grid_view_op_get_value('divi_grid_style2_boxshadow_hover_color','#999');
	}
	if ( $divi_grid_style3_enable_style ==  'Yes') {
		$divi_grid_style3_heading_color = dem_grid_view_op_get_value('divi_grid_style3_heading_color','#202020');
		$divi_grid_style3_heading_hover_color = dem_grid_view_op_get_value('divi_grid_style3_heading_hover_color','#40d9f1');
		$divi_grid_style3_address_color = dem_grid_view_op_get_value('divi_grid_style3_address_color','#767676'); 
		$divi_grid_style3_day_color = dem_grid_view_op_get_value('divi_grid_style3_day_color','#CCCCCC');
		$divi_grid_style3_date_color = dem_grid_view_op_get_value('divi_grid_style3_date_color','#666');
		$divi_grid_style3_description_color = dem_grid_view_op_get_value('divi_grid_style3_description_color','#767676');
		$divi_grid_style3_border_color = dem_grid_view_op_get_value('divi_grid_style3_border_color','#e2e2e2'); 
		$divi_grid_style3_boxshadow_color = dem_grid_view_op_get_value('divi_grid_style3_boxshadow_color','#000000');
		$divi_grid_style3_boxshadow_hover_color = dem_grid_view_op_get_value('divi_grid_style3_boxshadow_hover_color','#000000');
		$divi_grid_style3_boxshadow_color_hex2rgba = dem_hex2rgba($divi_grid_style3_boxshadow_color, 0.3);
		$divi_grid_style3_boxshadow_hover_color_hex2rgba = dem_hex2rgba($divi_grid_style3_boxshadow_hover_color, 0.15);
 	}
	if ( $divi_list_style1_enable_style ==  'Yes') {
		$divi_list_style1_heading_color = dem_list_view_op_get_value('divi_list_style1_heading_color','#555');
		$divi_list_style1_address_color = dem_list_view_op_get_value('divi_list_style1_address_color','#aeaeae'); 
		$divi_list_style1_date_color = dem_list_view_op_get_value('divi_list_style1_date_color','#aeaeae');
		$divi_list_style1_icon_color = dem_list_view_op_get_value('divi_list_style1_icon_color','#59b390');
		$divi_list_style1_border_color = dem_list_view_op_get_value('divi_list_style1_border_color','#59b390');
		$divi_list_style1_boxshaow_color = dem_list_view_op_get_value('divi_list_style1_boxshaow_color','#000000');
		$divi_list_style1_boxshaow_hover_color = dem_list_view_op_get_value('divi_list_style1_boxshaow_hover_color','#000000');
		$divi_list_style1_boxshaow_color_hex2rgba = dem_hex2rgba($divi_list_style1_boxshaow_color, 0.3);
		$divi_list_style1_boxshaow_hover_color_hex2rgba = dem_hex2rgba($divi_list_style1_boxshaow_hover_color, 0.2);
	}
	if ( $divi_list_style2_enable_style ==  'Yes') {
		$divi_list_style2_heading_color = dem_list_view_op_get_value('divi_list_style2_heading_color','#0d1d31');
		$divi_list_style2_heading_hover_color = dem_list_view_op_get_value('divi_list_style2_heading_hover_color','#ed4a43');
		$divi_list_style2_address_color = dem_list_view_op_get_value('divi_list_style2_address_color','#435469');
		$divi_list_style2_description_color = dem_list_view_op_get_value('divi_list_style2_description_color','#666');
		$divi_list_style2_price_color = dem_list_view_op_get_value('divi_list_style2_price_color','#ed4a43'); 
		$divi_list_style2_btn_text_color = dem_list_view_op_get_value('divi_list_style2_btn_text_color','#333');
		$divi_list_style2_btn_bk_color = dem_list_view_op_get_value('divi_list_style2_btn_bk_color','#fff');
		$divi_list_style2_btn_text_hover_color = dem_list_view_op_get_value('divi_list_style2_btn_text_hover_color','#fff');
		$divi_list_style2_btn_bk_hover_color = dem_list_view_op_get_value('divi_list_style2_btn_bk_hover_color','#ed4a43');
		$divi_list_style2_btn_border_color = dem_list_view_op_get_value('divi_list_style2_btn_border_color','#333');
		$divi_list_style2_border_color = dem_list_view_op_get_value('divi_list_style2_border_color','#eee');
	}
	if ( $divi_list_style3_enable_style ==  'Yes') {
		$divi_list_style3_heading_color = dem_list_view_op_get_value('divi_list_style3_heading_color','#292929');
		$divi_list_style3_heading_hover_color = dem_list_view_op_get_value('divi_list_style3_heading_hover_color','#40d9f1');
		$divi_list_style3_address_color = dem_list_view_op_get_value('divi_list_style3_address_color','#292929'); 
		$divi_list_style3_price_color = dem_list_view_op_get_value('divi_list_style3_price_color','#292929');
		$divi_list_style3_icon_color = dem_list_view_op_get_value('divi_list_style3_icon_color','#40d9f1');
		$divi_list_style3_date_color = dem_list_view_op_get_value('divi_list_style3_date_color','#444');
		$divi_list_style3_description_color = dem_list_view_op_get_value('divi_list_style3_description_color','#666'); 
		$divi_list_style3_border_color = dem_list_view_op_get_value('divi_list_style3_border_color','#e9e9e9');
		$divi_list_style3_boxshadow_color = dem_list_view_op_get_value('divi_list_style3_boxshadow_color','#000000'); 
		$divi_list_style3_boxshadow_color_hex2rgba = dem_hex2rgba($divi_list_style3_boxshadow_color, 0.2);
	}
	if ( $divi_slider_style1_enable_style ==  'Yes') {
		$divi_slider_style1_heading_color = dem_slider_view_op_get_value('divi_slider_style1_heading_color','#59b390cf');
		$divi_slider_style1_icon_color = dem_slider_view_op_get_value('divi_slider_style1_icon_color','#59b390cf'); 
		$divi_slider_style1_date_color = dem_slider_view_op_get_value('divi_slider_style1_date_color','#666');
		$divi_slider_style1_bk_overlay_color = dem_slider_view_op_get_value('divi_slider_style1_bk_overlay_color','#59b390cf');
		$divi_slider_style1_bk_overlay_icon_color = dem_slider_view_op_get_value('divi_slider_style1_bk_overlay_icon_color','#fff');
		$divi_slider_style1_bk_overlay_icon_bk_color = dem_slider_view_op_get_value('divi_slider_style1_bk_overlay_icon_bk_color','#f56719'); 
		$divi_slider_style1_arrow_btn_color = dem_slider_view_op_get_value('divi_slider_style1_arrow_btn_color','#fff');
		$divi_slider_style1_arrow_btn_hover_color = dem_slider_view_op_get_value('divi_slider_style1_arrow_btn_hover_color','#fff');  
		$divi_slider_style1_arrow_btn_bk_color = dem_slider_view_op_get_value('divi_slider_style1_arrow_btn_bk_color','#59b390cf');
		$divi_slider_style1_arrow_btn_bk_hover_color = dem_slider_view_op_get_value('divi_slider_style1_arrow_btn_bk_hover_color','#f56719');
	}
	if ( $divi_slider_style2_enable_style ==  'Yes') {	
		$divi_slider_style2_heading_color = dem_slider_view_op_get_value('divi_slider_style2_heading_color','#fff');
		$divi_slider_style2_address_color = dem_slider_view_op_get_value('divi_slider_style2_address_color','#fff');
		$divi_slider_style2_date_color = dem_slider_view_op_get_value('divi_slider_style2_date_color','#fff');
		$divi_slider_style2_description_color = dem_slider_view_op_get_value('divi_slider_style2_description_color','#fff');  
		$divi_slider_style2_bk_overlay_color = dem_slider_view_op_get_value('divi_slider_style2_bk_overlay_color','#fff'); 
		$divi_slider_style2_boxshaow_hover_color = dem_slider_view_op_get_value('divi_slider_style2_boxshaow_hover_color','#fff');
		$divi_slider_style2_bk_overlay_color_hex2rgba = dem_hex2rgba($divi_slider_style2_bk_overlay_color, 0.5);
		$divi_slider_style2_boxshaow_hover_color_hex2rgba = dem_hex2rgba($divi_slider_style2_boxshaow_hover_color, 0.2);
	 }
	 if ( $divi_slider_style3_enable_style ==  'Yes') {
		$divi_slider_style3_heading_color = dem_slider_view_op_get_value('divi_slider_style3_heading_color','#666');
		$divi_slider_style3_date_color = dem_slider_view_op_get_value('divi_slider_style3_date_color','#666');
		$divi_slider_style3_border_color = dem_slider_view_op_get_value('divi_slider_style3_border_color','#eeeeee');
		$divi_slider_style3_btn_text_color = dem_slider_view_op_get_value('divi_slider_style3_btn_text_color','#000');
		$divi_slider_style3_btn_text_hover_color = dem_slider_view_op_get_value('divi_slider_style3_btn_text_hover_color','#fff');
		$divi_slider_style3_btn_bk_color = dem_slider_view_op_get_value('divi_slider_style3_btn_bk_color','#fff');
		$divi_slider_style3_btn_bk_hover_color = dem_slider_view_op_get_value('divi_slider_style3_btn_bk_hover_color','#e0bc73');
     }
	 if ( $divi_detailpage_style1_enable_style ==  'Yes') {
		$divi_detailpage_style1_bk_color = dem_detail_view_op_get_value('divi_detailpage_style1_bk_color','#7ccbe6');
		$divi_detailpage_style1_txt_color = dem_detail_view_op_get_value('divi_detailpage_style1_txt_color','#fff');
		$divi_detailpage_style1_event_org_color = dem_detail_view_op_get_value('divi_detailpage_style1_event_org_color','#fff');
		$divi_detailpage_style1_event_section_heading_color = dem_detail_view_op_get_value('divi_detailpage_style1_event_section_heading_color','#fff');
		$divi_detailpage_style1_event_section_icon_color = dem_detail_view_op_get_value('divi_detailpage_style1_event_section_icon_color','#666');
		$divi_detailpage_style1_event_section_information_color = dem_detail_view_op_get_value('divi_detailpage_style1_event_section_information_color','#333');
		$divi_detailpage_style1_event_section_desc_color = dem_detail_view_op_get_value('divi_detailpage_style1_event_section_desc_color','#333');
		$divi_detailpage_style1_event_shareicon_color = dem_detail_view_op_get_value('divi_detailpage_style1_event_shareicon_color','#595fde');
		$divi_detailpage_style1_event_shareicon_hover_color = dem_detail_view_op_get_value('divi_detailpage_style1_event_shareicon_hover_color','#222');
		$divi_detailpage_style1_border_color = dem_detail_view_op_get_value('divi_detailpage_style1_border_color','#eee'); 
		$divi_detailpage_style1_boxshaow_color = dem_detail_view_op_get_value('divi_detailpage_style1_boxshaow_color','#00000');  
		$divi_detailpage_style1_frmbtn_text_color = dem_detail_view_op_get_value('divi_detailpage_style1_frmbtn_text_color','#fff'); 
		$divi_detailpage_style1_frmbtn_hover_text_color = dem_detail_view_op_get_value('divi_detailpage_style1_frmbtn_hover_text_color','#fff');
		$divi_detailpage_style1_frmbtn_btn_bk_color = dem_detail_view_op_get_value('divi_detailpage_style1_frmbtn_btn_bk_color','#716851');
		$divi_detailpage_style1_frmbtn_btn_bk_hover_color = dem_detail_view_op_get_value('divi_detailpage_style1_frmbtn_btn_bk_hover_color','#e0bc73');
		$divi_detailpage_style1_frmbtn_field_bk_color = dem_detail_view_op_get_value('divi_detailpage_style1_frmbtn_field_bk_color','#eee'); 
		$divi_detailpage_style1_frmbtn_field_label_color = dem_detail_view_op_get_value('divi_detailpage_style1_frmbtn_field_label_color','#999');
		$divi_detailpage_style1_boxshaow_color_hex2rgba = dem_hex2rgba($divi_detailpage_style1_boxshaow_color, 0.4);
		$divi_detailpage_style1_frmbtn_field_nlabel_color = dem_detail_view_op_get_value('divi_detailpage_style1_frmbtn_field_nlabel_color','');
	 }
	  if ( $divi_detailpage_style2_enable_style ==  'Yes') {
		$divi_detailpage_style2_bk_color = dem_detail_view_op_get_value('divi_detailpage_style2_bk_color','#fff');
		$divi_detailpage_style2_txt_color = dem_detail_view_op_get_value('divi_detailpage_style2_txt_color','#fff');
		$divi_detailpage_style2_event_org_color = dem_detail_view_op_get_value('divi_detailpage_style2_event_org_color','#fafafa');
		$divi_detailpage_style2_event_section_heading_color = dem_detail_view_op_get_value('divi_detailpage_style2_event_section_heading_color','#333');
		$divi_detailpage_style2_event_section_label_color = dem_detail_view_op_get_value('divi_detailpage_style2_event_section_label_color','#666');
		$divi_detailpage_style2_event_section_information_color = dem_detail_view_op_get_value('divi_detailpage_style2_event_section_information_color','#666');
		$divi_detailpage_style2_event_section_desc_color = dem_detail_view_op_get_value('divi_detailpage_style2_event_section_desc_color','#333');
		$divi_detailpage_style2_event_shareicon_color = dem_detail_view_op_get_value('divi_detailpage_style2_event_shareicon_color','#595fde');
		$divi_detailpage_style2_event_shareicon_hover_color = dem_detail_view_op_get_value('divi_detailpage_style2_event_shareicon_hover_color','#222');
		$divi_detailpage_style2_border_color = dem_detail_view_op_get_value('divi_detailpage_style2_border_color','#eee');
		$divi_detailpage_style2_boxshaow_color = dem_detail_view_op_get_value('divi_detailpage_style2_boxshaow_color','#00000');
		$divi_detailpage_style2_frmbtn_text_color = dem_detail_view_op_get_value('divi_detailpage_style2_frmbtn_text_color','#fff');
		$divi_detailpage_style2_frmbtn_hover_text_color = dem_detail_view_op_get_value('divi_detailpage_style2_frmbtn_hover_text_color','#fff'); 
		$divi_detailpage_style2_frmbtn_btn_bk_color = dem_detail_view_op_get_value('divi_detailpage_style2_frmbtn_btn_bk_color','#716851');
		$divi_detailpage_style2_frmbtn_btn_bk_hover_color = dem_detail_view_op_get_value('divi_detailpage_style2_frmbtn_btn_bk_hover_color','#e0bc73');
		$divi_detailpage_style2_frmbtn_field_bk_color = dem_detail_view_op_get_value('divi_detailpage_style2_frmbtn_field_bk_color','#eee'); 
		$divi_detailpage_style2_frmbtn_field_label_color = dem_detail_view_op_get_value('divi_detailpage_style2_frmbtn_field_label_color','#999');
		$divi_detailpage_style2_boxshaow_color_hex2rgba = dem_hex2rgba($divi_detailpage_style2_boxshaow_color, 0.4);
		$divi_detailpage_style2_frmbtn_field_nlabel_color = dem_detail_view_op_get_value('divi_detailpage_style2_frmbtn_field_nlabel_color','');
     }
	  if ( $divi_detailpage_custom_enable_style ==  'Yes') {
		$divi_detailpage_custom_frmbtn_text_color = dem_detail_view_op_get_value('divi_detailpage_custom_frmbtn_text_color','#fff'); 
		$divi_detailpage_custom_frmbtn_hover_text_color = dem_detail_view_op_get_value('divi_detailpage_custom_frmbtn_hover_text_color','#fff');
		$divi_detailpage_custom_frmbtn_btn_bk_color = dem_detail_view_op_get_value('divi_detailpage_custom_frmbtn_btn_bk_color','#716851');
		$divi_detailpage_custom_frmbtn_btn_bk_hover_color = dem_detail_view_op_get_value('divi_detailpage_custom_frmbtn_btn_bk_hover_color','#e0bc73');
		$divi_detailpage_custom_frmbtn_field_bk_color = dem_detail_view_op_get_value('divi_detailpage_custom_frmbtn_field_bk_color','#eee'); 
		$divi_detailpage_custom_frmbtn_field_label_color = dem_detail_view_op_get_value('divi_detailpage_custom_frmbtn_field_label_color','#999');
		$divi_detailpage_custom_frmbtn_field_nlabel_color = dem_detail_view_op_get_value('divi_detailpage_custom_frmbtn_field_nlabel_color','#999');
		$divi_detailpage_custom_heading_color = dem_detail_view_op_get_value('divi_detailpage_custom_heading_color','');
		$divi_detailpage_custom_boxshaow_color = dem_detail_view_op_get_value('divi_detailpage_custom_boxshaow_color','');
		$divi_detailpage_custom_boxshaow_color_hex2rgba = dem_hex2rgba($divi_detailpage_custom_boxshaow_color, 0.4);
		$divi_detailpage_custom_event_shareicon_color = dem_detail_view_op_get_value('divi_detailpage_custom_event_shareicon_color',''); 
		$divi_detailpage_custom_event_shareicon_hover_color = dem_detail_view_op_get_value('divi_detailpage_custom_event_shareicon_hover_color','');

	 }
	$dem_themename = dem_theme_name();
	$dem_detailpage_view = et_get_option($dem_themename.'_dem_detail_view_style','style1');
	$divi_dem_display_form_label = dem_general_view_op_get_value('divi_dem_display_form_label','No'); 
?>
<style type="text/css">

<?php if ( $divi_pg_enable_style ==  'Yes') { ?>
.navigation.dem_pagination a.page-numbers { color:<?php echo esc_attr( $divi_dem_pg_text_color) ;?>  !important;background:<?php echo esc_attr( $divi_dem_pg_bg_color) ;?>;}.navigation.dem_pagination span.page-numbers { color:<?php echo esc_attr( $divi_dem_pg_active_text_color) ;?>;}.navigation.dem_pagination a.page-numbers:hover, .navigation.dem_pagination span.page-numbers{background:<?php echo esc_attr( $divi_dem_pg_bg_color ) ;?> !important;}.navigation.dem_pagination a.page-numbers:hover{ color:<?php echo esc_attr( $divi_dem_pg_text_color) ;?>  !important;}
<?php } if ( $divi_slider_enable_style ==  'Yes') { ?>
.owl-theme .owl-dots .owl-dot span{background: <?php echo esc_attr( $divi_dem_slider_dot_color) ;?>;}.owl-theme .owl-dots .owl-dot:hover span { background:  <?php echo esc_attr( $divi_dem_slider_dot_hover_color) ;?>;}.owl-theme .owl-dots .owl-dot.active span{ background:  <?php echo esc_attr( $divi_dem_slider_active_dot_color) ;?>;} 
<?php } if ( $divi_grid_style1_enable_style ==  'Yes') { ?>
.dem_grid_style1 div.dem_grid_style1_main_content .dem_grid_style1_detail .dem_grid_style1_title .dem_title{color: <?php echo esc_attr( $divi_grid_style1_heading_color) ;?> !important;}.dem_grid_style1 div.dem_grid_style1_main_content .dem_grid_style1_detail .dem_grid_style1_venue{color: <?php echo esc_attr( $divi_grid_style1_address_color) ;?> !important;}.dem_grid_style1 div.dem_grid_style1_main_content .dem-event-date{color: <?php echo esc_attr( $divi_grid_style1_date_color) ;?> !important;}.dem_grid_style1_main_content a {  background-color: <?php echo esc_attr( $divi_grid_style1_bk_overlay_color) ;?>;}.dem_grid_style1 div.dem_grid_style1_main_content:hover { box-shadow: 0 0 10px 0 <?php echo esc_attr( $divi_grid_style1_boxshaow_hover_color_hex2rgba) ;?>;}
<?php } if ( $divi_grid_style2_enable_style ==  'Yes') { ?>
.dem_grid_style2 .dem_grid_style2_container .dem_grid_style2_title h3{color: <?php echo esc_attr( $divi_grid_style2_heading_color) ;?>;}.dem_grid_style2_event_detail .dem_grid_style2_event_date_time_venue{color: <?php echo esc_attr( $divi_grid_style2_address_color) ;?>;}.dem_grid_style2_event_detail .dem_grid_style2_event_cost{color: <?php echo esc_attr( $divi_grid_style2_price_color) ;?>;}.dem_grid_style2_event_detail .dem_grid_style2_book_now a.et_pb_button{color: <?php echo esc_attr( $divi_grid_style2_btn_text_color) ;?>;background-color: <?php echo esc_attr( $divi_grid_style2_btn_bk_color) ;?>;}.dem_grid_style2_event_detail .dem_grid_style2_book_now a.et_pb_button:hover{color: <?php echo esc_attr( $divi_grid_style2_btn_text_hover_color) ;?>;background-color: <?php echo esc_attr( $divi_grid_style2_btn_bk_hover_color) ;?>;border-color:<?php echo esc_attr( $divi_grid_style2_btn_border_color) ;?> ;}.dem_grid_style2 .dem_grid_style2_container{border-color:<?php echo esc_attr( $divi_grid_style2_border_color) ;?> ;box-shadow: 5px 5px 0px <?php echo esc_attr( $divi_grid_style2_boxshadow_color) ;?> ;}.dem_grid_style2 .dem_grid_style2_container:hover{box-shadow: 0px 5px 25px <?php echo esc_attr( $divi_grid_style2_boxshadow_hover_color) ;?> ;}
<?php } if ( $divi_grid_style3_enable_style ==  'Yes') { ?>
.dem_grid_style3 .dem_column_grid_view .dem_grid_style3_event_content h4.dem_grid_style3_event_title{color: <?php echo esc_attr( $divi_grid_style3_heading_color) ;?>;}
.dem_grid_style3 .dem_column_grid_view .dem_grid_style3_event_content h4.dem_grid_style3_event_title a:hover {color: <?php echo esc_attr( $divi_grid_style3_heading_hover_color) ;?>;}.dem_grid_style3 .dem_grid_style3_venue{color: <?php echo esc_attr( $divi_grid_style3_address_color) ;?>;}.dem_grid_style3 .dem_column_grid_view .dem_grid_style3_date_city_day .dem_grid_style3_day{color: <?php echo esc_attr( $divi_grid_style3_day_color) ;?>;}.dem_grid_style3 .dem_column_grid_view .dem_grid_style3_date_city_day .dem_grid_style3_date_month,.dem_grid_style3 .dem_column_grid_view .dem_grid_style3_date_city_day .dem_grid_style3_city{color: <?php echo esc_attr( $divi_grid_style3_date_color) ;?>;}.dem_grid_style3 .dem_column_grid_view .dem_grid_style3_event_content {color: <?php echo esc_attr( $divi_grid_style3_description_color) ;?>;}.dem_grid_style3 .dem_grid_style3_container{ box-shadow: 0 5px 15px <?php echo esc_attr( $divi_grid_style3_boxshadow_color_hex2rgba) ;?>;border-color: <?php echo esc_attr( $divi_grid_style3_border_color) ;?>;}.dem_grid_style3 .dem_grid_style3_container:hover {box-shadow: 0 3px 10px <?php echo esc_attr( $divi_grid_style3_boxshadow_hover_color_hex2rgba) ;?>;}
<?php } if ( $divi_list_style1_enable_style ==  'Yes') { ?>
.dem_list_style1 .dem_column_list_view a{color: <?php echo esc_attr( $divi_list_style1_heading_color) ;?>;}.dem_list_style1 .dem_column_list_view a .dem_event_detail .dem_event_time{color: <?php echo esc_attr( $divi_list_style1_address_color) ;?>;}.dem_list_style1 .dem_column_list_view a .dem_event_date,.dem_list_style1 .dem_column_list_view a .dem_event_date .dem_event_month{color: <?php echo esc_attr( $divi_list_style1_date_color) ;?>;}.dem_list_style1 .dem_column_list_view_style1 .dem_event_detail .dem_event_time i.et-pb-icon, .dem_list_style1 .dem_column_list_view_style1 .dem_event_detail .dem_event_venue i.et-pb-icon,.dem_list_style1 .dem_column_list_view a:hover .dem_event_icon_arrow_right{color: <?php echo esc_attr( $divi_list_style1_icon_color) ;?>;}.dem_list_style1 .dem_column_list_view{box-shadow: 0 0 10px 0 <?php echo esc_attr( $divi_list_style1_boxshaow_color_hex2rgba) ;?>;}.dem_list_style1 .dem_column_list_view:hover { box-shadow: 0 5px 25px 0 <?php echo esc_attr( $divi_list_style1_boxshaow_hover_color_hex2rgba) ;?>}.dem_list_style1 .dem_column_list_view a .dem_event_date { border-top-color: <?php echo esc_attr( $divi_list_style1_border_color) ;?>;border-bottom-color:<?php echo esc_attr( $divi_list_style1_border_color) ;?>;}
<?php } if ( $divi_list_style2_enable_style ==  'Yes') { ?>
.dem_list_style2 .dem_column_list_view_style2_right h3.dem_event_title{color: <?php echo esc_attr( $divi_list_style2_heading_color) ;?>;}.dem_list_style2 .dem_column_list_view_style2_right h3.dem_event_title a:hover{color: <?php echo esc_attr( $divi_list_style2_heading_hover_color) ;?>;}.dem_list_style2 .dem_column_list_view_style2_right .dem_event_date_time{color: <?php echo esc_attr( $divi_list_style2_address_color) ;?>;}.dem_list_style2 .dem_event_content{color: <?php echo esc_attr( $divi_list_style2_description_color) ;?>;}.dem_list_style2 .dem_column_list_view_style2_right .dem_event_ticket{color: <?php echo esc_attr( $divi_list_style2_price_color) ;?>;}.dem_list_style2 .dem_column_list_view_style2_right .dem_event_book_now a.et_pb_button{color: <?php echo esc_attr( $divi_list_style2_btn_text_color) ;?>;background-color: <?php echo esc_attr( $divi_list_style2_btn_bk_color) ;?>;border-color: <?php echo esc_attr( $divi_list_style2_btn_border_color) ;?>;}.dem_list_style2 .dem_column_list_view_style2_right .dem_event_book_now a.et_pb_button:hover{color: <?php echo esc_attr( $divi_list_style2_btn_text_hover_color) ;?>;background-color: <?php echo esc_attr( $divi_list_style2_btn_bk_hover_color) ;?>;}.dem_list_style2 .dem_divider.et_pb_divider:before { border-top-color: <?php echo esc_attr( $divi_list_style2_border_color) ;?>;}
<?php } if ( $divi_list_style3_enable_style ==  'Yes') { ?>
.dem_list_style3 .dem_list_style3_item .dem_list_style3_column_content .dem_list_style3_title a{color: <?php echo esc_attr( $divi_list_style3_heading_color) ;?>;}.dem_list_style3 .dem_list_style3_item .dem_list_style3_column_content .dem_list_style3_title a:hover{color: <?php echo esc_attr( $divi_list_style3_heading_hover_color) ;?>;}.dem_list_style3_info_inner span{color: <?php echo esc_attr( $divi_list_style3_address_color) ;?>;}.dem_list_style3 .dem_list_style3_item .dem_list_style3_info .dem_list_style3_ticket_cost span{color: <?php echo esc_attr( $divi_list_style3_price_color) ;?>;}.dem_list_style3 .dem_list_style3_item .dem_list_style3_info .dem_list_style3_info_inner i.et-pb-icon{color: <?php echo esc_attr( $divi_list_style3_icon_color) ;?>;}.dem_list_style3 .dem_list_style3_item .dem_list_style3_info .dem_date span,.dem_list_style3 .dem_list_style3_item .dem_list_style3_info .dem_event_time {color: <?php echo esc_attr( $divi_list_style3_date_color) ;?>;}.dem_list_style3 .dem_list_style3_description{color: <?php echo esc_attr( $divi_list_style3_description_color) ;?>;}.dem_list_style3_event { border-color:<?php echo esc_attr( $divi_list_style3_border_color) ;?>;box-shadow: 0 5px 15px 0 <?php echo esc_attr( $divi_list_style3_boxshadow_color_hex2rgba) ;?>;}
<?php } if ( $divi_slider_style1_enable_style ==  'Yes') { ?>
.dem_slider_style1 .dem_column_slider_view .dpevent_slider_title a { color:<?php echo esc_attr( $divi_slider_style1_heading_color) ;?>;}.dem_slider_style1 .dem_column_slider_view .dem_slider_style1_event_detail .dem_slider_style1_date i, .dem_slider_style1  .dem_column_slider_view .dem_slider_style1_event_detail .dem_slider_style1_time i { color:<?php echo esc_attr( $divi_slider_style1_icon_color) ;?>;}.dem_slider_style1 .dem_column_slider_view .dem_slider_style1_event_detail .dem_slider_style1_date,.dem_slider_style1 .dem_column_slider_view .dem_slider_style1_event_detail .dem_slider_style1_time { color:<?php echo esc_attr( $divi_slider_style1_date_color) ;?>;}.dem_slider_style1 .dem_column_slider_view .dem_slider_style1_image .dem_slider_style1_image_overlay { background: <?php echo esc_attr( $divi_slider_style1_bk_overlay_color) ;?> none repeat scroll 0 0;}.dem_slider_style1 .dem_column_slider_view .dem_slider_style1_image .dem_slider_style1_image_overlay .dem_slider_style1_image_content i.et-pb-icon { color:<?php echo esc_attr( $divi_slider_style1_bk_overlay_icon_color) ;?>;}.dem_slider_style1 .dem_column_slider_view .dem_slider_style1_image .dem_slider_style1_image_box .dem_slider_style1_image_content a { background: <?php echo esc_attr( $divi_slider_style1_bk_overlay_icon_bk_color) ;?> none repeat scroll 0 0;}.dem_slider_style1 .dem_slider_style1_button a { color:<?php echo esc_attr( $divi_slider_style1_arrow_btn_color) ;?>;background:<?php echo esc_attr( $divi_slider_style1_arrow_btn_bk_color) ;?>;border: 2px solid <?php echo esc_attr( $divi_slider_style1_arrow_btn_bk_color) ;?>;}.dem_slider_style1 .dem_slider_style1_button a:hover { background:<?php echo esc_attr( $divi_slider_style1_arrow_btn_bk_hover_color) ;?> !important;color:<?php echo esc_attr( $divi_slider_style1_arrow_btn_hover_color) ;?> !important;}	
<?php } if ( $divi_slider_style2_enable_style ==  'Yes') { ?>
.dem_slider_style2 .dem_slider_style2_main_content .dem_slider_style2_detail .dem_slider_style2_title .dpevent_slider_title{ color:<?php echo esc_attr( $divi_slider_style2_heading_color) ;?>;}.dem_slider_style2 .dem_slider_style2_main_content .dem_slider_style2_detail .dem_slider_style2_venue {color:<?php echo esc_attr( $divi_slider_style2_address_color) ;?>;}.dem_slider_style2 .dem_slider_style2_main_content .dem-event-date { color:<?php echo esc_attr( $divi_slider_style2_date_color) ;?>;}.dem_slider_style2 .dem_slider_style2_event_text { color:<?php echo esc_attr( $divi_slider_style2_description_color) ;?>;}.dem_slider_style2 .dem_slider_style2_main_content a:before { background: <?php echo esc_attr( $divi_slider_style2_bk_overlay_color_hex2rgba) ;?>;}.dem_slider_style2 .dem_slider_style2_main_content .dem_slider_style2_detail .dem_slider_style2_title .dpevent_slider_title a { color:<?php echo esc_attr( $divi_slider_style2_boxshaow_hover_color) ;?>;}.dem_slider_style2 .dem_slider_style2_main_content:hover { box-shadow: 0 5px 25px 0 <?php echo esc_attr( $divi_slider_style2_boxshaow_hover_color_hex2rgba) ;?>;}
<?php } if ( $divi_slider_style3_enable_style ==  'Yes') { ?>
.dem_slider_style3 .dem_slider_style3_event_info .dem_event_date{ color:<?php echo esc_attr( $divi_slider_style3_date_color) ;?>;}.dem_slider_style3 .dem_event_date,.dem_slider_style3 .dpevent_slider_title{ color:<?php echo esc_attr( $divi_slider_style3_heading_color) ;?>;}.dem_slider_style3 .dem_slider_style3_item{ border-color:<?php echo esc_attr( $divi_slider_style3_border_color) ;?>;}.dem_slider_style3 .dem_slider_style3_event_view_more{ color:<?php echo esc_attr( $divi_slider_style3_btn_text_color) ;?>;background-color:<?php echo esc_attr( $divi_slider_style3_btn_bk_color) ;?>;border-color:<?php echo esc_attr( $divi_slider_style3_btn_bk_color) ;?> ; }.dem_slider_style3 .dem_slider_style3_event_view_more:hover{color:<?php echo esc_attr( $divi_slider_style3_btn_text_hover_color) ;?>;background-color:<?php echo esc_attr( $divi_slider_style3_btn_bk_hover_color) ;?> !important;border-color:<?php echo esc_attr( $divi_slider_style3_btn_bk_hover_color) ;?> !important;}

<?php } if ( $dem_detailpage_view == 'style1' && $divi_detailpage_style1_enable_style ==  'Yes' ){?>
.single-dp_events.et-db #et-boc .et_pb_fullwidth_header,.single-dp_events .et_pb_fullwidth_header{ background-color:<?php echo esc_attr( $divi_detailpage_style1_bk_color) ;?> !important;}.single-dp_events .dem-detail-style1,.single-dp_events #dem_detail_ticket_booking{ background-color:<?php echo esc_attr( $divi_detailpage_style1_event_org_color) ;?>;box-shadow: 0 5px 10px <?php echo esc_attr( $divi_detailpage_style1_boxshaow_color_hex2rgba) ;?>;}.single-dp_events .dem-detail-style1 h3,#dem_detail_ticket_booking h3.dem_detail_style1_ticket_booking_title_text,.dem_detail_style1_google_map h3.dem_detail_style1_title,.event_share h3.dem_detail_style1_title,.image_gallery h3.dem_detail_style1_image_title{ color:<?php echo esc_attr( $divi_detailpage_style1_event_section_heading_color) ;?>;}.single-dp_events.et-db #et-boc .et_pb_module.et_pb_text .et_pb_text_inner table tr td i,.single-dp_events .et_pb_module.et_pb_text .et_pb_text_inner table tr td i{ color:<?php echo esc_attr( $divi_detailpage_style1_event_section_icon_color) ;?>;}.single-dp_events.et-db #et-boc .event_share ul.et_pb_social_media_follow li a i.event_social_icon,.single-dp_events .event_share ul.et_pb_social_media_follow li a i.event_social_icon{ color:<?php echo esc_attr( $divi_detailpage_style1_event_shareicon_color) ;?>;}.single-dp_events.et-db #et-boc .event_share ul.et_pb_social_media_follow li a:hover .event_social_icon,.single-dp_events .event_share ul.et_pb_social_media_follow li a:hover .event_social_icon{ color:<?php echo esc_attr( $divi_detailpage_style1_event_shareicon_hover_color) ;?>;}.single-dp_events .dem_detail_style1_event_content ,.single-dp_events .dem-detail-style1 td,.dem_avilable_ticket{ color:<?php echo esc_attr( $divi_detailpage_style1_event_section_information_color) ;?>;}.single-dp_events.et-db #et-boc .dem_detail_style1_event_content{ color:<?php echo esc_attr( $divi_detailpage_style1_event_section_desc_color) ;?>;}.single-dp_events.et-db #et-boc h1.et_pb_module_header,.single-dp_events h1.et_pb_module_header{color:<?php echo esc_attr( $divi_detailpage_style1_txt_color) ;?>;}.single-dp_events.et-db #et-boc .et_pb_module.et_pb_text .et_pb_text_inner table tr td,.single-dp_events .et_pb_module.et_pb_text .et_pb_text_inner table tr td,.single-dp_events.et-db #et-boc .et_pb_module.et_pb_text .et_pb_text_inner table tr th,.single-dp_events .et_pb_module.et_pb_text .et_pb_text_inner table tr th{ border-color:<?php echo esc_attr( $divi_detailpage_style1_border_color) ;?>;}.single-dp_events .dem_events_paypal_button,.single-dp_events .dem_events_submit_button{border-color:<?php echo esc_attr( $divi_detailpage_style1_frmbtn_btn_bk_color) ;?>;background-color:<?php echo esc_attr( $divi_detailpage_style1_frmbtn_btn_bk_color) ;?>;color:<?php echo esc_attr( $divi_detailpage_style1_frmbtn_text_color) ;?>;}.single-dp_events .dem_events_paypal_button:hover,.single-dp_events .dem_events_submit_button:hover {border-color:<?php echo esc_attr( $divi_detailpage_style1_frmbtn_btn_bk_hover_color) ;?>;background-color:<?php echo esc_attr( $divi_detailpage_style1_frmbtn_btn_bk_hover_color) ;?>;color:<?php echo esc_attr( $divi_detailpage_style1_frmbtn_hover_text_color) ;?>;}.single-dp_events #dem_detail_ticket_booking .et_pb_contact_select,.single-dp_events #dem_detail_ticket_booking .et_pb_contact p input,.single-dp_events #dem_detail_ticket_booking .et_pb_contact p textarea{background-color:<?php echo esc_attr( $divi_detailpage_style1_frmbtn_field_bk_color) ;?>;}.single-dp_events #dem_detail_ticket_booking .et_pb_contact_select,.single-dp_events #dem_detail_ticket_booking .et_pb_contact p input,.single-dp_events #dem_detail_ticket_booking .et_pb_contact p textarea{color:<?php echo esc_attr( $divi_detailpage_style1_frmbtn_field_label_color) ;?>;}.single-dp_events.et-db #et-boc .et-l .et_pb_contact_form_label { color:<?php echo esc_attr( $divi_detailpage_style1_frmbtn_field_nlabel_color)  ;?> !important;}
<?php } 
if ( $dem_detailpage_view == 'style2' && $divi_detailpage_style2_enable_style ==  'Yes' ){?>
.single-dp_events.et-db #et-boc .et_pb_fullwidth_header,.single-dp_events .et_pb_fullwidth_header{ background-color:<?php echo esc_attr( $divi_detailpage_style2_bk_color) ;?> !important;}.single-dp_events.et-db #et-boc .dem_detail_style2_header_title h2.et_pb_module_header,.single-dp_events .dem_detail_style2_header_title h2.et_pb_module_header,.single-dp_events.et-db #et-boc .header-content h4,.single-dp_events .header-content h4,.dem-detail-style2-header, .dem_event_date_start, .dem_event_date_end, .dem_event_cost{color:<?php echo esc_attr( $divi_detailpage_style2_txt_color) ;?>}.single-dp_events.et-db #et-boc .et_pb_row.dem_event_detail_style2_table,.single-dp_events .et_pb_row.dem_event_detail_style2_table ,.single-dp_events #dem_detail_style2_contact_form{background: <?php echo esc_attr( $divi_detailpage_style2_event_org_color) ;?>;border:1px solid <?php echo esc_attr( $divi_detailpage_style2_border_color) ;?>;box-shadow: 0 2px 5px <?php echo esc_attr( $divi_detailpage_style2_boxshaow_color_hex2rgba) ;?>;}.single-dp_events.et-db #et-boc .dem_detail_style2_image_gallery .et_pb_text_inner h3.dem_detail_style2_image_gallery_title,.single-dp_events .dem_detail_style2_image_gallery .et_pb_text_inner h3.dem_detail_style2_image_gallery_title, .single-dp_events.et-db #et-boc .dem_detail_style2_social .et_pb_text_inner h3.dem_detail_style2_social_title,.single-dp_events .dem_detail_style2_social .et_pb_text_inner h3.dem_detail_style2_social_title, .single-dp_events.et-db #et-boc .dem_detail_style2_ticket_booking_title .et_pb_text_inner h3.dem_detail_style2_ticket_booking_title_text,.single-dp_events .dem_detail_style2_ticket_booking_title .et_pb_text_inner h3.dem_detail_style2_ticket_booking_title_text,.single-dp_events.et-db #et-boc .dem_event_detail_style2_table .et_pb_text_inner h3,.single-dp_events .dem_event_detail_style2_table .et_pb_text_inner h3{color:<?php echo esc_attr( $divi_detailpage_style2_event_section_heading_color) ;?>;}.single-dp_events.et-db #et-boc .dem_event_detail_style2_table .et_pb_module.et_pb_text .et_pb_text_inner table tr th,.single-dp_events .dem_event_detail_style2_table .et_pb_module.et_pb_text .et_pb_text_inner table tr th{color:<?php echo esc_attr( $divi_detailpage_style2_event_section_label_color) ;?>;}.single-dp_events.et-db #et-boc .dem_event_detail_style2_table .et_pb_module.et_pb_text .et_pb_text_inner table tr td,.single-dp_events .dem_event_detail_style2_table .et_pb_module.et_pb_text .et_pb_text_inner table tr td,.single-dp_events.et-db #et-boc .dem_event_detail_style2_table .et_pb_module.et_pb_text .et_pb_text_inner table tr td a,.single-dp_events .dem_event_detail_style2_table .et_pb_module.et_pb_text .et_pb_text_inner table tr td a,.dem_detail_style2_ticket_booking .dem_avilable_ticket{color:<?php echo esc_attr( $divi_detailpage_style2_event_section_information_color) ;?>;}.single-dp_events .dem_events_paypal_button,.single-dp_events .dem_events_submit_button{border-color:<?php echo esc_attr( $divi_detailpage_style2_frmbtn_btn_bk_color) ;?>;background-color:<?php echo esc_attr( $divi_detailpage_style2_frmbtn_btn_bk_color) ;?>;color:<?php echo esc_attr( $divi_detailpage_style2_frmbtn_text_color) ;?>;}.single-dp_events .dem_events_paypal_button:hover,.single-dp_events .dem_events_submit_button:hover {border-color:<?php echo esc_attr( $divi_detailpage_style2_frmbtn_btn_bk_hover_color) ;?>;background-color:<?php echo esc_attr( $divi_detailpage_style2_frmbtn_btn_bk_hover_color) ;?>;color:<?php echo esc_attr( $divi_detailpage_style2_frmbtn_hover_text_color) ;?>;}.single-dp_events #dem_detail_ticket_booking .et_pb_contact_select,.single-dp_events #dem_detail_ticket_booking .et_pb_contact p input,.single-dp_events #dem_detail_ticket_booking .et_pb_contact p textarea{background-color:<?php echo esc_attr( $divi_detailpage_style2_frmbtn_field_bk_color) ;?>;}.single-dp_events #dem_detail_ticket_booking .et_pb_contact_select,.single-dp_events #dem_detail_ticket_booking .et_pb_contact p input,.single-dp_events #dem_detail_ticket_booking .et_pb_contact p textarea{color:<?php echo esc_attr( $divi_detailpage_style2_frmbtn_field_label_color) ;?>;}.single-dp_events.et-db #et-boc .dem_detail_style2_social ul.et_pb_social_media_follow li a,.single-dp_events .dem_detail_style2_social ul.et_pb_social_media_follow li a{ color:<?php echo esc_attr( $divi_detailpage_style2_event_shareicon_color) ;?>;}.single-dp_events.et-db #et-boc .dem_detail_style2_social ul.et_pb_social_media_follow li a:hover,.single-dp_events .dem_detail_style2_social ul.et_pb_social_media_follow li a:hover{ color:<?php echo esc_attr( $divi_detailpage_style2_event_shareicon_hover_color) ;?>;}.single-dp_events.et-db #et-boc .dem_style2_description{ color:<?php echo esc_attr( $divi_detailpage_style2_event_section_desc_color) ;?>;}.single-dp_events.et-db #et-boc .et-l .et_pb_contact_form_label { color:<?php echo esc_attr( $divi_detailpage_style2_frmbtn_field_nlabel_color)  ;?> !important;}

<?php } 
if ( $dem_detailpage_view == 'customstyle' && $divi_detailpage_custom_enable_style ==  'Yes' ){?>
.single-dp_events .dem_events_paypal_button,.single-dp_events .dem_events_submit_button{border-color:<?php echo esc_attr( $divi_detailpage_custom_frmbtn_btn_bk_color) ;?>;background-color:<?php echo esc_attr( $divi_detailpage_custom_frmbtn_btn_bk_color) ;?>;color:<?php echo esc_attr( $divi_detailpage_custom_frmbtn_text_color) ;?>;}.single-dp_events .dem_events_paypal_button:hover,.single-dp_events .dem_events_submit_button:hover {border-color:<?php echo esc_attr( $divi_detailpage_custom_frmbtn_btn_bk_hover_color) ;?>;background-color:<?php echo esc_attr( $divi_detailpage_custom_frmbtn_btn_bk_hover_color) ;?>;color:<?php echo esc_attr( $divi_detailpage_custom_frmbtn_hover_text_color) ;?>;}.single-dp_events #dem_detail_ticket_booking .et_pb_contact_select,.single-dp_events #dem_detail_ticket_booking .et_pb_contact p input,.single-dp_events #dem_detail_ticket_booking .et_pb_contact p textarea{background-color:<?php echo esc_attr( $divi_detailpage_custom_frmbtn_field_bk_color) ;?> !important;}.single-dp_events #dem_detail_ticket_booking .et_pb_contact_select,.single-dp_events #dem_detail_ticket_booking .et_pb_contact p input,.single-dp_events #dem_detail_ticket_booking .et_pb_contact p textarea{color:<?php echo esc_attr( $divi_detailpage_custom_frmbtn_field_label_color)  ;?> !important;}.single-dp_events.et-db #et-boc .et-l .et_pb_contact_form_label { color:<?php echo esc_attr( $divi_detailpage_custom_frmbtn_field_nlabel_color)  ;?> !important;}
.single-dp_events.et-db #et-boc ul.event_et_pb_social_media_follow li a i.event_social_icon,.single-dp_events ul.event_et_pb_social_media_follow li a i.event_social_icon{ color:<?php echo esc_attr( $divi_detailpage_custom_event_shareicon_color) ;?>;}.single-dp_events.et-db #et-boc ul.event_et_pb_social_media_follow li a:hover .event_social_icon,.single-dp_events ul.event_et_pb_social_media_follow li a:hover .event_social_icon{ color:<?php echo esc_attr($divi_detailpage_custom_event_shareicon_hover_color) ;?>;}.single-dp_events #dem_detail_ticket_booking{ box-shadow: 0 5px 10px <?php echo esc_attr( $divi_detailpage_custom_boxshaow_color_hex2rgba) ;?>;}.single-dp_events #dem_detail_ticket_booking h3,.single-dp_events #dem_detail_ticket_booking .dem_avilable_ticket{ color:<?php echo esc_attr( $divi_detailpage_custom_heading_color) ;?>;}
<?php }
if ( $divi_dem_display_form_label  ==  'Yes' ){?>
.single-dp_events.et-db #et-boc .et-l .et_pb_contact_form_label { display: block !important;}
<?php }?>
</style>