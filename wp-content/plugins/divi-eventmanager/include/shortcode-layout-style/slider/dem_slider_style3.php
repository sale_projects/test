<?php 
$event_view_content .= '<div class="dem_slider_style3_item item it_js dem_column_grid_view">';
$event_view_content .= '<div class="dem_slider_style3_item_inner item_inner">';
if ( !empty ( $event_thumbnail_image_url ) ){
	$event_view_content .= '<div class="dem_slider_style3_event_image">';
	$event_view_content .= '<img  src="'.esc_url( $event_thumbnail_image_url ).'" alt="'.esc_attr( $event_title ) .'">';
	$event_view_content .= '</div>';
}
$event_view_content .= '<div class="dem_slider_style3_event_info">';
if ( !empty ( $event_start_date ) ){
	$event_view_content .= '<span class="dem_event_date">'.date_i18n('d ', esc_attr( $event_start_date )).date_i18n('F,Y', esc_attr( $event_start_date )).'</span>';
}	
$event_view_content .= '<h4 class="dem_event_title"><a href="'.esc_url( $event_permalink ).'">'.esc_attr( $event_title ).'</a></h4>';
$event_view_content .= '<div class="dem_slider_style3_view_more"><a class="dem_slider_style3_event_view_more" href="'.esc_url( $event_permalink ).'">'.esc_attr( $event_readmore_btn_text ).'</a></div>';
$event_view_content .= '</div>';
$event_view_content .= '</div>';
$event_view_content .= '</div>';		