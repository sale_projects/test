<?php 
$event_view_content .= '<div class="item it_js">';
$event_view_content .= '<div class="dem_column_slider_view dem_column_grid_view ">';
if ( !empty ( $event_thumbnail_image_url ) ): 
	$event_view_content .= '<div class="dem_slider_style1_image"><a href="'.esc_url( $event_permalink ).'"><img src="'.esc_url( $event_thumbnail_image_url ).'" alt="'.esc_attr( $event_title ).'"></a>';
	$event_view_content .= '<div class="dem_slider_style1_image_overlay">';	
	$event_view_content .= '<div class="dem_slider_style1_image_box">';
	$event_view_content .= '<div class="dem_slider_style1_image_content"><a href="'.esc_url( $event_permalink ).'"><i class="et-pb-icon">&#xe02c;</i></a></div>';
	$event_view_content .= '</div>';
	$event_view_content .= '</div>';
	$event_view_content .= '</div>';
endif;
// Title
$event_view_content .= '<h5 class="dem_slider_style1_title"><a href="'.esc_url( $event_permalink ).'">'.esc_attr( $event_title ) .'</a></h5>';
$event_view_content .= '<div class="dem_slider_style1_event_detail">';
if( !empty ( $event_start_date ) ){
	$event_view_content .= '<span class="dem_slider_style1_date "><i class="et-pb-icon">&#xe025;</i>'.date_i18n('j M,Y', esc_attr( $event_start_date )).'</span>';
}
if( !empty ( $event_start_date ) && !empty ( $event_start_time ) && !empty ( $event_end_time ) ){
	if ( $dem_display_time == 'twhr' ){
		$event_view_content .= '<span class="dem_slider_style1_time">'.date_i18n('l', esc_attr( $event_start_date )).', '.date_i18n("h:i a",  strtotime(esc_attr( $event_start_time ))).' '.esc_attr($dem_evt_to).' '.date_i18n("h:i a",  strtotime(esc_attr( $event_end_time ))).'</span>';
	}else{
		$event_view_content .= '<span class="dem_slider_style1_time">'.date_i18n('l', esc_attr( $event_start_date )).', '.date_i18n("H:i",strtotime(esc_attr( $event_start_time ))).' '.esc_attr($dem_evt_to).' '.date_i18n("H:i",strtotime(esc_attr( $event_end_time ))).'</span>';
	}
}
$event_view_content .= '</div>';
$event_view_content .= '</div>';
$event_view_content .= '<div class="dem_slider_style1_button"><a href="'.esc_url( $event_permalink ).'" class="et_pb_button" ><i class="et-pb-icon ">&#x24;</i></a></div>';
$event_view_content .= '</div>'; 	