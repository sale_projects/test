<?php 
$event_view_content .= '<div class="dem_slider_style2_main_content item it_js">';
$event_view_content .= '<a href="'.esc_url( $event_permalink ).'"  style=" background-image: url('.esc_url( $event_thumbnail_image_url ).'); "> ';       
$event_view_content .= '<span class="dem_slider_style2_detail"><span class="dem_slider_style2_title"><span class="dem_title">'.esc_attr( $event_title ).'</span></span><span class="dem_slider_style2_venue"><i class="et-pb-icon">&#xe01d;</i>'.esc_attr( $event_venue_address ).'</span></span>';
if( !empty ( $event_start_date ) && !empty ( $event_start_time ) && !empty ( $event_end_time ) ){
	if ( $dem_display_time == 'twhr' ){
		$event_date_t = date_i18n("h:i a",  strtotime(esc_attr( $event_start_time ))).' '.esc_attr($dem_evt_to).' '.date_i18n("h:i a",  strtotime(esc_attr( $event_end_time )));
	}else{
		$event_date_t = date_i18n("H:i",strtotime(esc_attr( $event_start_time ))).' '.esc_attr($dem_evt_to).' '.date_i18n("H:i",strtotime(esc_attr( $event_end_time )));
	}
	$event_view_content .= '<span class="dem-event-date"><span class="dem-event-day">'.date_i18n('d', esc_attr( $event_start_date )).'</span><span class="dem-event-month">'.date_i18n('F,Y',esc_attr( $event_start_date )).'</span><span class="dem-event-time">'.date_i18n('l',esc_attr(  $event_start_date )).', '.esc_attr( $event_date_t ).'</span></span>';
}
$event_view_content .=  '<div class="dem_slider_style2_event_text">'.wp_kses_post( $event_content ).'</div>';
$event_view_content .= '</a>';
$event_view_content .= '</div>';		 	