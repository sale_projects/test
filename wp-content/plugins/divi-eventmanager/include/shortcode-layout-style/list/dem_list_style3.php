<?php 				
$event_view_content .= '<div class="dem_list_style3_event">';
$event_view_content .= '<div class="et_pb_row  dem_list_style3_item ">';
$event_view_content .= '<div class="et_pb_column et_pb_column_1_4 dem_list_style3_column_image dem_column_grid_view">';
if($event_thumbnail_image_url){
		 $event_view_content .= '<div class="dem_list_style3_image  dem_list_style3_desktop"><a href="'.esc_url( $event_permalink ).'"><img src="'.esc_url( $event_thumbnail_image_url ).'" alt="'.esc_attr( $event_title ).'"></a></div>';
		 $event_view_content .= '<div class="dem_list_style3_image dem_list_style3_mobile"><a href="'.esc_url( $event_permalink ).'"><img src="'.esc_url( $event_thumbnail_image_large_url ).'" alt="'.esc_attr( $event_title ).'"></a></div>';
}
$event_view_content .= '</div>';
$event_view_content .= '<div class="et_pb_column et_pb_column_1_2 dem_list_style3_column_content dem_column_grid_view">';
$event_view_content .= '<div class="dem_list_style3_content">';
$event_view_content .= '<h3 class="dem_list_style3_title"><a href="'.esc_url( $event_permalink ).'">'.esc_attr( $event_title ).'</a></h3>';
$event_view_content .= '<div class="dem_list_style3_description">'.wp_kses_post( $event_content ).'</div>';
$event_view_content .= '</div></div>';
$event_view_content .= '<div class="et_pb_column et_pb_column_1_4 dem_list_style3_info dem_column_grid_view">';
$event_view_content .= '<div class="dem_list_style3_info_inner">';
if( $event_start_date ){
	$event_view_content .= '<div class="dem_date"><i class="et-pb-icon">&#xe023;</i><span>'.date_i18n('d ', esc_attr( $event_start_date )).' '.date_i18n(' F, Y',  esc_attr( $event_start_date )).'</span></div>';
}
if( $event_ticket_cost && $event_ticket_cost_currency ){
	if($event_ticket_currency_position == 'suffix'){
$event_view_content .= '<div class="dem_list_style3_ticket_cost"><i class="et-pb-icon">&#xe100;</i><span>'.esc_attr( $event_ticket_cost.$event_ticket_cost_currency ).'</span></div>';
	}else{
$event_view_content .= '<div class="dem_list_style3_ticket_cost"><i class="et-pb-icon">&#xe100;</i><span>'.esc_attr( $event_ticket_cost_currency.$event_ticket_cost ).'</span></div>';
	}
}	
if( $event_start_time && $event_end_time && $event_start_date){
	if ( $dem_display_time == 'twhr' ){
		$event_view_content .= '<span class="dem_event_time"><i class="et-pb-icon">&#x7d;</i>'.date_i18n('l', esc_attr( $event_start_date )).', '.date_i18n("h:i a",  strtotime(esc_attr( $event_start_time ))).' '.esc_attr($dem_evt_to).' '.date_i18n("h:i a",  strtotime(esc_attr( $event_end_time ))).'</span>';
	}else{
		$event_view_content .= '<span class="dem_event_time"><i class="et-pb-icon">&#x7d;</i>'.date_i18n('l', esc_attr( $event_start_date )).', '.date_i18n("H:i",strtotime(esc_attr( $event_start_time ))).' '.esc_attr($dem_evt_to).' '.date_i18n("H:i",strtotime(esc_attr( $event_end_time ))).'</span>';
	}
}
if( $event_venue_address ){
	$event_view_content .= '<div class="dem_list_style3_venue"><i class="et-pb-icon">&#xe01d;</i><span>'.esc_attr( rtrim($event_venue_address,', ') ).'</span></div>';
}
$event_view_content .= '</div></div></div></div>';   