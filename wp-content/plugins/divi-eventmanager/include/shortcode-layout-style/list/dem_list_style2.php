<?php 
$divi_dem_ticket_start_from = et_get_option($dem_themename.'_dem_ticket_start_from','Ticket Start From');
$event_view_content .= '<div class="et_pb_row dem_event_item">';
$event_view_content .= '<div class="et_pb_column et_pb_column_1_3 dem_column_list_view_style2_left">';
$event_view_content .= '<a href="'.esc_url( $event_permalink ).'" >';
$event_view_content .= '<div class="dem_event_media_image dem_list_style2_desktop"><img src="'.esc_url( $event_thumbnail_image_url ).'" alt="'.esc_attr( $event_title ).'"></div>';
$event_view_content .= '<div class="dem_event_media_image dem_list_style2_mobile"><img src="'.esc_url( $event_thumbnail_image_large_url ).'" alt="'.esc_attr( $event_title ).'"></div>';
$event_view_content .= '</a>';
$event_view_content .= '</div>';
$event_view_content .= '<div class="et_pb_column et_pb_column_2_3 dem_column_list_view_style2_right">';	
$event_view_content .= '<h3 class="dem_event_title"><a href="'.esc_url( $event_permalink ).'">'.esc_attr( $event_title ) .'</a></h3>';	
if($event_start_date  && $event_start_time && $event_end_time && $event_venue_address){	
		if ( $dem_display_time == 'twhr' ){
			$event_date_t = date_i18n("h:i a",  strtotime(esc_attr( $event_start_time ))).' - '.date_i18n("h:i a",  strtotime(esc_attr( $event_end_time )));
		}else{
			$event_date_t = date_i18n("H:i",strtotime(esc_attr( $event_start_time ))).' - '.date_i18n("H:i",strtotime(esc_attr( $event_end_time )));
		}
$event_view_content .=  '<p class="dem_event_date_time"><i class="et-pb-icon">&#xe023;</i>&nbsp;'.date_i18n('d ', esc_attr( $event_start_date )).' '.date_i18n(' F, Y',  esc_attr( $event_start_date )).' '.esc_attr($dem_evt_at).' '.esc_attr( $event_date_t ).' '.esc_attr($dem_evt_on).' '.esc_attr( rtrim($event_venue_address,', ') ).'</p>';
}
if( $event_content ){
	$event_view_content .= '<p class="dem_event_content">'.wp_kses_post( $event_content ).'</p>';
}
if( $event_ticket_cost && $event_ticket_cost_currency ){
	if($event_ticket_currency_position == 'suffix'){
		$event_view_content .=  '<p class="dem_event_ticket">'.esc_attr( $divi_dem_ticket_start_from.' '.$event_ticket_cost.$event_ticket_cost_currency ).'</p>';
	}else{
		$event_view_content .=  '<p class="dem_event_ticket">'.esc_attr( $divi_dem_ticket_start_from.' '.$event_ticket_cost_currency.$event_ticket_cost ).'</p>';
	}
}
$event_view_content .=  '<p class="dem_event_book_now"><a href="'.esc_url( $event_permalink ).'" class="et_pb_button">'.esc_attr( $event_readmore_btn_text ).'</a></p>';
$event_view_content .= '</div>'; 
$event_view_content .= '</div>';   
$event_view_content .= '<div class="et_pb_divider dem_divider"></div>';	