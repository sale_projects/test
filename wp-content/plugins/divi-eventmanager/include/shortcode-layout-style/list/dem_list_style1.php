<?php 
$event_view_content .= '<div class="et_pb_column et_pb_column_4_4 dem_column_list_view dem_column_list_view_style1">';
$event_view_content .= '<a class="dem_event_item_link" href="'.esc_url( $event_permalink  ).'">';
if($event_start_date){		
$event_view_content .= '<span class="dem_event_date">
									<span class="dem_event_day">'.date_i18n('d', esc_attr( $event_start_date )).'</span>
									<span class="dem_event_month">'.date_i18n('F', esc_attr( $event_start_date )).'</span>
									<span class="dem_event_year">'.date_i18n('Y', esc_attr( $event_start_date )).'</span>
						</span>';
}
$event_view_content .= '<span class="dem_event_detail"><span class="dem_event_detail_inner">';
if( $event_start_time && $event_end_time && $event_start_date){
	if ( $dem_display_time == 'twhr' ){
			$event_view_content .= '<span class="dem_event_time"><i class="et-pb-icon">&#x7d;</i>&nbsp;'.date_i18n('l', esc_attr( $event_start_date )).', '.date_i18n("h:i a",  strtotime(esc_attr( $event_start_time ))).' '.esc_attr($dem_evt_to).' '.date_i18n("h:i a",  strtotime(esc_attr( $event_end_time ))).'</span>';
		}else{
			$event_view_content .= '<span class="dem_event_time"><i class="et-pb-icon">&#x7d;</i>&nbsp;'.date_i18n('l', esc_attr( $event_start_date )).', '.date_i18n("H:i",strtotime(esc_attr( $event_start_time ))).' '.esc_attr($dem_evt_to).' '.date_i18n("H:i",strtotime(esc_attr( $event_end_time ))).'</span>';
		}
}
$event_view_content .= '<span class="dem_event_title"><span class="dem-event-title">'.esc_attr( $event_title ).'</span></span>';
if($event_venue_address){
	$event_view_content .= '<span class="dem_event_venue"><i class="et-pb-icon">&#xe081;</i>&nbsp;'.esc_attr( rtrim($event_venue_address,', ') ).'</span>';
}
if($event_content){
	$event_view_content .= '<p class="dem_event_content">'.wp_kses_post( $event_content ).'</p>';
}
$event_view_content .= '</span></span>';
$event_view_content .= '<i class="et-pb-icon dem_event_icon_arrow_right">&#x39;</i>';  
$event_view_content .= '</a>';
$event_view_content .= '</div>';