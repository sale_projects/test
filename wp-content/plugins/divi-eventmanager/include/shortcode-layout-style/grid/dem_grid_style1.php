<?php   
$event_view_content .= '<div class="et_pb_column '.$column_class.' ' .$last_child.' ">';
$event_view_content .= '<div class="dem_grid_style1_main_content">';
$event_view_content .= '<a href="'.esc_url( $event_permalink ).'" class="dem_column_grid_view" style=" background-image: url('.esc_url( $event_thumbnail_image_url ).'); "> ';
$event_view_content .= '<span class="dem_grid_style1_detail">
				        			<span class="dem_grid_style1_title">
				        				<span class="dem_title">'.esc_attr( $event_title ).'</span>
			        				</span>
				                    <span class="dem_grid_style1_venue">
				                    		<i class="et-pb-icon">&#xe081;</i>'.esc_attr( rtrim($event_venue_address,', ') ).  '
		                    		</span>
				         </span>';
if( $event_start_date ){
	$event_view_content .= '<span class="dem-event-date"><span class="dem-event-day">'.date_i18n('d', esc_attr( $event_start_date )).'</span><span class="dem-event-month">'.date_i18n('F,Y',  esc_attr( $event_start_date )).'</span>';
	if( $event_start_time && $event_end_time ){
		if ( $dem_display_time == 'twhr' ){
					$event_view_content .= '<span class="dem-event-time">'.date_i18n('l', esc_attr( $event_start_date )).', '.date_i18n("h:i a",  strtotime(esc_attr( $event_start_time ))).' '.esc_attr($dem_evt_to).' '.date_i18n("h:i a",  strtotime(esc_attr( $event_end_time ))).'</span>';
		}else{
						$event_view_content .= '<span class="dem-event-time">'.date_i18n('l', esc_attr( $event_start_date )).', '.date_i18n("H:i",strtotime(esc_attr( $event_start_time ))).' '.esc_attr($dem_evt_to).' '.date_i18n("H:i",strtotime(esc_attr( $event_end_time ))).'</span>';
		}
	}
	$event_view_content .= '</span>';
}
$event_view_content .= '</a>';
$event_view_content .= '</div></div>';