<?php 
$event_view_content .= '<div class="et_pb_column '.$column_class.' ' .$last_child.' ">';
$event_view_content .= '<div class="dem_grid_style3_container dem_column_grid_view">';
$event_view_content .= '<div class="dem_grid_style3_date_city_day">';
if($event_start_date){
	$event_view_content .= '<div class="dem_grid_style3_date_month">'.date_i18n('d F', esc_attr( $event_start_date )).'</div>';
}
if($event_city){
	$event_view_content .= '<div class="dem_grid_style3_city">'.esc_attr( $event_city ).'</div>';
}
if($event_start_date){
	$event_view_content .= '<div class="dem_grid_style3_day">'.date_i18n('l', esc_attr( $event_start_date )).'</div>';
}
$event_view_content .= '</div>';
$event_view_content .= '<div class="dem_grid_style3_event_content">';
$event_view_content .= '<h4 class="dem_grid_style3_event_title"><a href="'.esc_url( $event_permalink ).'" >'.esc_attr( $event_title ).'</a></h4>';
if( $event_venue_address != '' ){
	$event_view_content .= '<p class="dem_grid_style3_venue"><strong><i class="et-pb-icon">&#xe01d;</i>'.esc_attr( rtrim($event_venue_address,', ') ).'</strong></p>';
}
$event_view_content .=  '<div class="dem_grid3_event_text">'.wp_kses_post( $event_content ).'</div>';						
$event_view_content .= '</div></div>';
$event_view_content .= '</div>';