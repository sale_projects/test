<?php 
$event_view_content .= '<div class="et_pb_column '.$column_class.' ' .$last_child.' ">';
$event_view_content .= '<div class="dem_grid_style2_container dem_column_grid_view">';
if ( $event_thumbnail_image_url ): 
	$event_view_content .= '<a href="'.esc_url( $event_permalink ).'" >';
	$event_view_content .= '<div class="dem_grid_style2_image"><img src="'.esc_url( $event_thumbnail_image_url ).'" alt="'.esc_attr( $event_title ).'"></div>';
	$event_view_content .= '</a>';
endif;
$event_view_content .= '<div class="dem_grid_style2_event_detail">';
$event_view_content .= '<div class="dem_grid_style2_title"><a href="'.esc_url( $event_permalink ).'" ><h3>'.esc_attr( $event_title ).'</h3></a></div>';
if($event_start_date && $event_venue_address && $event_end_time){
		if ( $dem_display_time == 'twhr' ){
			$event_date_t = date_i18n("h:i a",  strtotime(esc_attr( $event_start_time ))).' '.esc_attr($dem_evt_to).' '.date_i18n("h:i a",  strtotime(esc_attr( $event_end_time )));
		}else{
			$event_date_t = date_i18n("H:i",strtotime(esc_attr( $event_start_time ))).' '.esc_attr($dem_evt_to).' '.date_i18n("H:i",strtotime(esc_attr( $event_end_time )));
		}
		$event_view_content .= '<p class="dem_grid_style2_event_date_time_venue"><i class="et-pb-icon">&#xe01d;</i>'.date_i18n('d ', esc_attr( $event_start_date )).' '.date_i18n(' F, Y',  esc_attr( $event_start_date )).' '.esc_attr($dem_evt_at).' '.esc_attr( $event_date_t ).' '.esc_attr($dem_evt_on).' '.esc_attr( rtrim($event_venue_address,', ') ).'</p>';
}
if( $event_ticket_cost && $event_ticket_cost_currency ){
		if($event_ticket_currency_position == 'suffix'){
			$event_view_content .=  '<p class="dem_grid_style2_event_cost">'.esc_attr( $divi_dem_ticket_start_from.' '.$event_ticket_cost.$event_ticket_cost_currency ).'</p>';
		}else{
			$event_view_content .=  '<p class="dem_grid_style2_event_cost">'.esc_attr( $divi_dem_ticket_start_from.' '.$event_ticket_cost_currency.$event_ticket_cost ).'</p>';
		}
}
$event_view_content .=  '<p class="dem_grid_style2_book_now"><a href="'.esc_url( $event_permalink ).'" class="et_pb_button">'.esc_attr( $event_readmore_btn_text ).'</a></p>';
$event_view_content .= '</div></div></div>';