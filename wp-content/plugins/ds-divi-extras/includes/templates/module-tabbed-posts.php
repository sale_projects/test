<?php
/* This file is copied from the Extra theme by Elegant Themes, released under the GNU General Public License version 2 or later, licensed under the GNU General Public License version 3 or later. See ../../license.txt for license text. Modified 2020-05-14 by Jonathan Hall */

// phpcs:disable -- all code in this file from line 7 onward is a direct copy from the Extra theme with minimal automated change(s); assuming all escaping, etc., has already been done where needed
?>

<?php $id_attr = '' !== $module_id ? sprintf( ' id="%1$s"', esc_attr( $module_id ) ) : ''; ?>
<div <?php echo $id_attr ?> class="module tabbed-post-module et_pb_extra_module <?php echo esc_attr( $module_class ); ?>" style="border-top-color:<?php echo esc_attr( $border_top_color ); ?>">
	<div class="tabs clearfix">
		<ul>
			<?php
			foreach ( $terms as $tab_id => $term ) {
				$no_term_color_tab_nav_class = is_customize_preview() && $term['color'] === extra_global_accent_color() ? 'no-term-color-tab' : '';
			?>
			<li id="category-tab-<?php echo esc_attr( $tab_id ); ?>" class="et-accent-color-parent-term <?php echo esc_attr( $no_term_color_tab_nav_class ); ?>" data-tab-id="<?php echo esc_attr( $tab_id ); ?>" data-term-color="<?php echo esc_attr( $term['color'] ); ?>" ripple="" ripple-inverse="">
				<span>
					<?php echo esc_html( $term['name'] ); ?>
				</span>
			</li>
			<?php } ?>
		</ul>
		<div class="tab-nav">
			<span class="prev arrow" title="<?php esc_attr_e( 'Previous Tab', 'extra' ); ?>"></span>
			<span class="next arrow" title="<?php esc_attr_e( 'Next Tab', 'extra' ); ?>"></span>
		</div>
	</div>

	<div class="tab-contents">
		<?php echo $content; ?>
	</div>
</div>
