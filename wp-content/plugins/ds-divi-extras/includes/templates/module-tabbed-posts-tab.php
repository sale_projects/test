<?php
/* This file is copied from the Extra theme by Elegant Themes, released under the GNU General Public License version 2 or later, licensed under the GNU General Public License version 3 or later. See ../../license.txt for license text. Modified 2020-05-14 by Jonathan Hall */

// phpcs:disable -- all code in this file from line 7 onward is a direct copy from the Extra theme with minimal automated change(s); assuming all escaping, etc., has already been done where needed
?>

<?php if ( empty( $module_posts ) ) return; ?>
<div class="tab-content tab-content-<?php echo esc_attr( $tab_id ); ?> <?php esc_attr_e( $module_class ); ?>">
	<?php require ($postsContentTemplate = locate_template('module-posts-content.php')) ? $postsContentTemplate : dirname(__FILE__).'/module-posts-content.php'; ?>
</div>
