/* The following functions contain code from WP and Divi Icons Pro (fb.js) by Aspen Grove Studios */
function ds_divi_extras_onCreateElementWithClass(className, callback) {
	var MO = window.MutationObserver ? window.MutationObserver : window.WebkitMutationObserver;
	if (MO) {
		(new MO(function(events) {
			jQuery.each(events, function(i, event) {
				if (event.addedNodes && event.addedNodes.length) {
					var $nodes = jQuery(event.addedNodes);
					
					$nodes.filter('.' + className).each(function(i, node) {
						callback(node);
					});
					$nodes.find('.' + className).each(function(i, node) {
						callback(node);
					});
				}
			});
		})).observe(document.body, {characterData: false, childList: true, subtree: true});
	}
}

ds_divi_extras_onCreateElementWithClass('et-fb-modal__support-notice', function(node) {
	var ourModuleTitles = [
		'Ad',
		'Ads',
		'Featured Posts Slider',
		'Posts',
		'Blog Feed Standard',
		'Blog Feed Masonry',
		'Posts Carousel',
		'Tab',
		'Tabbed Posts'
	];
	var $node = jQuery(node);
	var $modal = $node.closest('.et-fb-modal');
	var modalTitle = $modal.find('.et-fb-modal__title').text();
	if ( modalTitle.endsWith(' Settings') ) {
		var moduleTitle = modalTitle.substring(0, modalTitle.length - 9);
		if (ourModuleTitles.indexOf(moduleTitle) !== -1) {
			$node.remove();
			$modal.addClass('ags-divi-extras-module-settings');
		}
		
	}
	
});
