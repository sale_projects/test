<?php
if ( ! defined( 'ABSPATH' ) ) exit;

function divi_mobile_dm_menu_inline_css() {
  $titan = TitanFramework::getInstance( 'divi-mobile-menu' );


$divi_mobile_menu_bg_color = $titan->getOption( 'divi_mobile_menu_bg_color' );
$set_mobile_menu_side_appear = $titan->getOption( 'set_mobile_menu_side_appear' );
$set_mobile_menu_side_max_width = $titan->getOption( 'set_mobile_menu_side_max_width' );

if ($set_mobile_menu_side_appear == "right") {
  $scroll_section_r_l = 'right: -20vw;';
  $scroll_section_r_l_dis = 'right: 6vw;';
  $menu_before = 'right: -150vh;';
  $menu_before_dis = 'right: -90vh;';
} else {
  $scroll_section_r_l = 'left: -20vw;';
  $scroll_section_r_l_dis = 'left: 6vw;';
  $menu_before = 'left: -150vh;';
  $menu_before_dis = 'left: -90vh;';

}


$divi_mobile_menu_bg_image = $titan->getOption( 'divi_mobile_menu_bg_image' );
$divi_mobile_menu_bg_image_size = $titan->getOption( 'divi_mobile_menu_bg_image_size' );
$divi_mobile_menu_bg_image_pos = $titan->getOption( 'divi_mobile_menu_bg_image_pos' );
$divi_mobile_menu_bg_image_repeat = $titan->getOption( 'divi_mobile_menu_bg_image_repeat' );
$divi_mobile_menu_bg_image_blend = $titan->getOption( 'divi_mobile_menu_bg_image_blend' );
if ( is_numeric( $divi_mobile_menu_bg_image ) ) {
    $imageAttachment = wp_get_attachment_image_src( $divi_mobile_menu_bg_image, 'full' );
    $imageSrc = $imageAttachment[0];
  } else {
    $imageSrc = '';
  }

  if ($imageSrc !== "") {
    $menu_background_image = '
    background-image: url('.$imageSrc.')!important;
    background-size: '.$divi_mobile_menu_bg_image_size.';
    background-position: '.$divi_mobile_menu_bg_image_pos.';
    background-repeat: '.$divi_mobile_menu_bg_image_repeat.';
    background-blend-mode:  '.$divi_mobile_menu_bg_image_blend.';
  }';
  } else {
    $menu_background_image = '';
  }


  $css_mobile = '<style id="divi-mobile-menu-dm-inline-styles">';

  $css_mobile .= '

  .menu-wrap {
    max-width: inherit !important;
}

  .menu-wrap__inner {
    background-image: none !important;
  }

  body.show-menu .menu-wrap {
    z-index: 99999;
}

body.show-menu .divi-mobile-menu #open-button {
  z-index: 99999;
}

  body .menu-wrap {
background-color: transparent !important;
width: 100% !important;
  }



  body .menu-wrap::before {
    background-color: '.$divi_mobile_menu_bg_color.';
    height: 0vh;
    width: 0vh;
    '.$menu_before.'
    content: "";
    display: block !important;
    border-radius: 100vh;
    z-index: 0;
    top: -100vh;
    position: absolute;
    -webkit-transition: all 1s ease;
    transition: all 1s ease;
}

body.show-menu .menu-wrap::before {
  '.$menu_before_dis.'
  height: 150vh;
  width: 150vh;
  top: -20vh;
}

.scroll_section {
  '.$scroll_section_r_l.'
  -webkit-transition: all 1s ease-out 0s;
  transition: all 1s ease-out 0s;
  z-index: -1;
  position: absolute;
  opacity: 0;
  top: 50%;
  transform: translateY(-50%);
  overflow: hidden;
  width: 100%;
  max-width: '.$set_mobile_menu_side_max_width.'px;
}

body.show-menu .scroll_section {
  '.$scroll_section_r_l_dis.'
  z-index: 20;
  -webkit-transition: all 1s ease 0.5s;
  transition: all 1s ease 0.5s;
  opacity: 1;
}

body .menu-wrap::before {
  '.$menu_background_image.'
}

  ';


  $css_mobile .= '</style>';
  //minify it
  $css_mobile_min = str_replace( array("\r\n", "\r", "\n", "\t", '  ', '    ', '    '), '', $css_mobile );
  echo $css_mobile_min; // phpcs:ignore

}
add_action('wp_head', 'divi_mobile_dm_menu_inline_css');