<?php
if ( ! defined( 'ABSPATH' ) ) exit;
function divi_mobile_menu_burge_manu_js() {
?>

<script>
(function() {

	var bodyEl = document.body,
		dm_nav = document.getElementById( 'dm_nav' ),
		content = document.getElementById( 'et-main-area' ),
		openbtn = document.getElementById( 'open-button' ),
		closebtn = document.getElementById( 'close-button' ),
		isOpen = false;

	function init() {
		initEvents();
	}

	function initEvents() {
		openbtn.addEventListener( 'click', toggleMenu );
		if( closebtn ) {
			closebtn.addEventListener( 'click', toggleMenu );
		}

		// close the menu element if the target it´s not the menu element or one of its descendants..
		content.addEventListener( 'click', function(ev) {
			var target = ev.target;
			if( isOpen && target !== openbtn ) {
				toggleMenu();
			}
		} );
	}

	function toggleMenu() {
		if( isOpen ) {
			classie.remove( bodyEl, 'show-menu' );
				classie.remove( openbtn, 'is-active' );
				classie.remove( dm_nav, 'active' );
				setTimeout(function (){
				classie.add( dm_nav, 'menuclosed' );
				}, 800);
		}
		else {
			classie.add( bodyEl, 'show-menu' );
				classie.add( openbtn, 'is-active' );
				classie.add( dm_nav, 'active' );
				classie.remove( dm_nav, 'menuclosed' );
		}
		isOpen = !isOpen;
	}

	init();

})();
</script>

<script>



</script>

<?php
}
add_action('wp_footer', 'divi_mobile_menu_burge_manu_js');
 ?>
