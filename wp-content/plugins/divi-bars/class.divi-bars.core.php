<?php

	class DiviBars {
		
		private static $initiated = false;
		
		/**
		 * Holds an instance of DiviBars Helper class
		 *
		 * @since 1.0
		 * @var DiviBars_Helper
		 */
		public static $helper;
		
		protected static $divibarsList;
		
		protected static $isMobileDevice = false;
		
		protected static $isTabletDevice = false;
		
		public static function init() {
			
			if ( ! self::$initiated ) {
				
				self::load_resources();
				
				self::$helper = new DiviBars_Helper();
				
				self::init_hooks();
				
				// Register the Custom Divi Bar Post Type
				self::register_cpt();
			}
		}
		
		/**
		 * Initializes WordPress hooks
		 */
		protected static function init_hooks() {
			
			self::$initiated = true;
			
			// Add custom column in post type
			add_filter( 'manage_edit-divi_bars_columns', array( 'DiviBars', 'setup_divibars_columns') ) ;
			add_action( 'manage_divi_bars_posts_custom_column', array( 'DiviBars', 'manage_divibars_columns' ), 10, 2 );
			
			// Add styles
			add_action( 'wp_print_styles', array( 'DiviBars', 'add_styles') );
			
			// Add scripts
			add_action( 'wp_enqueue_scripts', array( 'DiviBars', 'add_scripts') );
			
			// Ajax ready
			add_action( 'wp_head', array( 'DiviBars', 'add_var_ajaxurl') );
			add_action( 'wp_ajax_ajax_divibars_callback', array( 'DiviBars_Ajax', 'ajax_divibars_callback') );
			add_action( 'wp_ajax_nopriv_ajax_divibars_callback', array( 'DiviBars_Ajax', 'ajax_divibars_callback') );
			
			// Register widget
			add_action( 'widgets_init', array( 'DiviBars', 'register_widget') );
			
			add_action( 'wp_head', array( 'DiviBars', 'prevent_playable_tags') );
			add_action( 'wp_head', array( 'DiviBars', 'getAllDivibars') );
			
			// Divi Bars call
			add_action( 'wp_footer', array( 'DiviBars_Controller', '_init' ), 21 );
			add_action( 'wp_footer', array( 'DiviBars_Controller', 'showDiviBars' ), 2 );
		}
		
		
		public static function getAllDivibars() {
			
			$function_exception = 'showDiviBars';
			
			$divibars = array();
			
			try {
			
				/* Search Divi divibar in current post */
				global $post;
				
				if ( $post !== NULL ) {
					
					$post_content = $post->post_content;
					$matches = array();
					$pattern = '/id="(.*?divibars_[0-9]+)"/';
					preg_match_all( $pattern, $post_content, $matches );
					
					$divibars_divibar_ = $matches[1];
					
					$matches = array();
					$pattern = '/id="(.*?divibars_unique_id_[0-9]+)"/';
					preg_match_all( $pattern, $post_content, $matches );
					
					$divibars_divibar_unique_id_ = $matches[1];
					
					$matches = array();
					$pattern = '/(\S+)class=["\']?((?:.divibar\-[0-9]+(?!["\']?\s+(?:\S+)=|[>"\']))+.)["\']?/';
					preg_match_all( $pattern, $post_content, $matches );
					
					$divibars_class_divibar = $matches[1];
					
					$divibars_in_post = $divibars_divibar_ + $divibars_divibar_unique_id_ + $divibars_class_divibar;
					
					$divibars_in_post = array_filter( array_map( 'DiviBars_Helper::prepareBars', $divibars_in_post ) );
					
					if ( is_array( $divibars_in_post ) && count( $divibars_in_post ) > 0 ) {
						
						$divibars_in_post = array_flip( $divibars_in_post );
					}
				}
				else {
					
					$divibars_in_post = array();
				}
				
				
				/* Search Divi divibar in active menus */
				$theme_locations = get_nav_menu_locations();
				
				$divibars_in_menus = array();
				
				$divibars['divibars_in_menus'] = array();
				
				if ( is_array( $theme_locations ) && count( $theme_locations ) > 0 ) {
					
					$divibars_in_menus = array();
					
					foreach( $theme_locations as $theme_location => $theme_location_value ) {
						
						$menu = get_term( $theme_locations[$theme_location], 'nav_menu' );
						
						// menu exists?
						if( !is_wp_error($menu) ) {
							
							$menu_items = wp_get_nav_menu_items($menu->term_id);
							
							foreach ( (array) $menu_items as $key => $menu_item ) {
								
								$url = $menu_item->url;
								
								if ( $url ) {
									
									$extract_id = self::$helper->prepareBars( $url );
									
									if ( $extract_id ) {
										
										$divibars_in_menus[ $extract_id ] = 1;
									}
								}
								
								/* Search Divi divibar in menu classes */
								if ( count( $menu_item->classes ) > 0 && $menu_item->classes[0] != '' ) {
									
									foreach ( $menu_item->classes as $key => $class ) {
										
										if ( $class != '' ) {
											
											$extract_id = self::$helper->prepareBars( $class );
											
											if ( $extract_id ) {
											
												$divibars_in_menus[ $extract_id ] = 1;
											}
										}
									}
								}
								
								/* Search Divi divibar in Link Relationship (XFN) */
								if ( !empty( $menu_item->xfn ) ) {
									
									$extract_id = self::$helper->prepareBars( $menu_item->xfn );
									
									if ( $extract_id ) {
									
										$divibars_in_menus[ $extract_id ] = 1;
									}
								}
							}
						}
						else {
							
							
						}
					}
				}
				$divibars_in_menus = array_filter( $divibars_in_menus );
				$divibars['divibars_in_menus'] = $divibars_in_menus;
				
				
				/* Search CSS Triggers in all Divi divibars */
				$divibars_with_css_trigger = array();
				
				$divibars['divibars_with_css_trigger'] = array();
				
				$dwct_posts = DiviBars_Model::getDivibars('css_trigger');
				
				if ( isset( $dwct_posts[0] ) ) {
					
					foreach( $dwct_posts as $dv_post ) {
						
						$post_id = $dv_post->ID;
						
						$get_css_selector = get_post_meta( $post_id, 'dib_css_selector' );
							
						$css_selector = $get_css_selector[0];
						
						if ( $css_selector != '' ) {
						
							$divibars_with_css_trigger[ $post_id ] = $css_selector;
						}
					}
					
					$divibars['divibars_with_css_trigger'] = $divibars_with_css_trigger;
				}
				
				
				/* Search URL Triggers in all Divibars */
				$divibars_with_url_trigger = array();
				
				$divibars['divibars_with_url_trigger'] = array();
				
				$dwut_posts = DiviBars_Model::getDivibars('enableurltrigger');
				
				if ( isset( $dwut_posts[0] ) ) {
					
					foreach( $dwut_posts as $dv_post ) {
						
						$post_id = $dv_post->ID;
						
						$divibars_with_url_trigger[ $post_id ] = 1;
					}
				}
				$divibars_with_url_trigger = array_filter( $divibars_with_url_trigger );
				$divibars['divibars_with_url_trigger'] = $divibars_with_url_trigger;
				
				
				/* Search Automatic Triggers in all Divi divibars */
				
				// Server-Side Device Detection with Browscap
				require_once( plugin_dir_path( __FILE__ ) . 'includes/php-libraries/Browscap/Browscap.php' );
				$browscap = new Browscap( plugin_dir_path( __FILE__ ) . '/includes/php-libraries/Browscap/Cache/' );
				$browscap->doAutoUpdate = false;
				$current_browser = $browscap->getBrowser();
				
				self::$isMobileDevice = $current_browser->isMobileDevice;
				
				self::$isTabletDevice = $current_browser->isTablet;
				
				$divibars_with_automatic_trigger = array();
				
				$divibars['divibars_with_automatic_trigger'] = array();
				
				$dwat_posts = DiviBars_Model::getDivibars('automatic_trigger');
				
				if ( isset( $dwat_posts[0] ) ) {
					
					foreach( $dwat_posts as $dv_post ) {
						
						$post_id = $dv_post->ID;
					
						$at_disablemobile = get_post_meta( $post_id, 'divibars_automatictrigger_disablemobile' );
						$at_disabletablet = get_post_meta( $post_id, 'divibars_automatictrigger_disabletablet' );
						$at_disabledesktop = get_post_meta( $post_id, 'divibars_automatictrigger_disabledesktop' );
						$onceperload = get_post_meta( $post_id, 'divibars_automatictrigger_onceperload', true );
						
						if ( isset( $onceperload[0] ) ) {
							
							$onceperload = $onceperload[0];
							
						} else {
							
							$onceperload = 1;
						}
						
						if ( isset( $at_disablemobile[0] ) ) {
							
							$at_disablemobile = $at_disablemobile[0];
							
						} else {
							
							$at_disablemobile = 0;
						}
						
						if ( isset( $at_disabletablet[0] ) ) {
							
							$at_disabletablet = $at_disabletablet[0];
							
						} else {
							
							$at_disabletablet = 0;
						}
						
						if ( isset( $at_disabledesktop[0] ) ) {
							
							$at_disabledesktop = $at_disabledesktop[0];
							
						} else {
							
							$at_disabledesktop = 0;
						}
						
						$printSettings = 1;
						if ( $at_disablemobile && self::$isMobileDevice ) {
							
							$printSettings = 0;
						}
						
						if ( $at_disabletablet && self::$isTabletDevice ) {
							
							$printSettings = 0;
						}
						
						if ( $at_disabledesktop && !self::$isMobileDevice && !self::$isTabletDevice ) {
							
							$printSettings = 0;
						}
						
						if ( $printSettings ) {
							
							$at_type = get_post_meta( $post_id, 'divibars_automatictrigger', true );
							$at_timed = get_post_meta( $post_id, 'divibars_automatictrigger_timed_value', true );
							$at_scroll_from = get_post_meta( $post_id, 'divibars_automatictrigger_scroll_from_value', true );
							$at_scroll_to = get_post_meta( $post_id, 'divibars_automatictrigger_scroll_to_value', true );
							
							if ( $at_type != '' ) {
								
								switch ( $at_type ) {
									
									case 'divibar-timed':
										$at_value = $at_timed;
									break;
									
									case 'divibar-scroll':
										$at_value = $at_scroll_from . ':' . $at_scroll_to;
									break;
									
									default:
										$at_value = $at_type;
								}
								
								$at_settings = json_encode( array( 'at_type' => $at_type, 'at_value' => $at_value, 'at_onceperload' => $onceperload ) );
								
								$divibars_with_automatic_trigger[ $post_id ] = $at_settings;
							}
						}
					}
				}
				
				$divibars_with_automatic_trigger = array_filter( $divibars_with_automatic_trigger );
				$divibars['divibars_with_automatic_trigger'] = $divibars_with_automatic_trigger;
				
				$divibars['ids'] = $divibars_in_post + $divibars_in_menus + $divibars_with_css_trigger + $divibars_with_url_trigger + $divibars_with_automatic_trigger;
				
				if ( is_array( $divibars['ids'] ) && count( $divibars['ids'] ) > 0 ) {
					
					add_filter( 'body_class', function ( $classes )
					{
						$classes[] = 'divibar-active';
						return $classes;
						
					}, 20, 2 );
				}
			
			} catch (Exception $e) {
			
				self::log( $e );
			}
			
			self::$divibarsList = $divibars;
		}
		
		
		protected static function load_resources() {
			
			require_once( DIVI_BARS_PLUGIN_DIR . '/includes/class.divi-bars.controller.php' );
			require_once( DIVI_BARS_PLUGIN_DIR . '/includes/class.divi-bars.model.php' );
			require_once( DIVI_BARS_PLUGIN_DIR . '/includes/class.divi-bars.helper.php' );
			require_once( DIVI_BARS_PLUGIN_DIR . '/includes/class.divi-bars.ajax.php' );
		}
		
		
		public static function register_cpt() {
			
			$labels = array(
				'name' => _x( 'Divi Bars', 'divi_bars' ),
				'singular_name' => _x( 'Divi Bars', 'divi_bars' ),
				'add_new' => _x( 'Add New', 'divi_bars' ),
				'add_new_item' => _x( 'Add New Divi Bar', 'divi_bars' ),
				'edit_item' => _x( 'Edit Divi Bar', 'divi_bars' ),
				'new_item' => _x( 'New Divi Bar', 'divi_bars' ),
				'view_item' => _x( 'View Divi Bar', 'divi_bars' ),
				'search_items' => _x( 'Search Divi Bars', 'divi_bars' ),
				'not_found' => _x( 'No Divi Bars found', 'divi_bars' ),
				'not_found_in_trash' => _x( 'No Divi Bars found in Trash', 'divi_bars' ),
				'parent_item_colon' => _x( 'Parent Divi Bar:', 'divi_bars' ),
				'menu_name' => _x( 'Divi Bars', 'divi_bars' ),
			);
			
			$args = array(
				'labels' => $labels,
				'hierarchical' => true,
				'supports' => array( 'title', 'editor', 'author' ),
				'public' => true,
				'show_ui' => true,
				'show_in_menu' => true,
				'menu_position' => 5,
				'show_in_nav_menus' => true,
				'exclude_from_search' => true,
				'has_archive' => true,
				'query_var' => true,
				'can_export' => true,
				'rewrite' => true,
				'capability_type' => 'post'
			);
			
			register_post_type( 'divi_bars', $args );
		}
		
		
		public static function setup_divibars_columns( $columns ) {

			$columns = array(
				'cb' => '<input type="checkbox" />',
				'title' => __( 'Title' ),
				'unique_identifier' => __( 'CSS ID' ),
				'unique_menu_id' => __( 'Menu ID' ),
				'author' => __( 'Author' ),
				'date' => __( 'Date' )
			);

			return $columns;
		}
		
		
		public static function manage_divibars_columns( $column, $post_id ) {
			
			global $post;
			
			switch( $column ) {
				
				/* If displaying the 'unique-indentifier' column. */
				case 'unique_identifier':
				
					/* Get the post meta. */
					$post_slug = "divibars_unique_id_$post->ID";
					
					echo $post_slug;
					
					break;
					
				case 'unique_menu_id':
				
					/* Get the post meta. */
					$post_slug = "unique_divibars_menu_id_$post->ID";
					
					echo $post_slug;
					
					break;
					
				default:
				
					break;
			}
		}
		
		
		public static function register_widget() {
			
			register_sidebar( array(
				'name' => __( 'Divi Bars - Global', 'theme-slug' ),
				'id' => 'divi-bars_global_widget',
				'description' => __( '', 'theme-slug' )
			) );
		}
		
		
		// Register all needed stylesheets
		public static function add_styles() {
			
			
			wp_register_style('DiviBars-main', DIVI_BARS_PLUGIN_URL . 'assets/css/main.css' );
			wp_enqueue_style('DiviBars-main');
		}
		
		
		// Register all needed scripts
		public static function add_scripts() {
			
			wp_enqueue_script('jquery');
		
			wp_register_script('DiviBars-main', DIVI_BARS_PLUGIN_URL . 'assets/js/main.js', array('jquery'), DIVI_BARS_VERSION, TRUE);
			wp_enqueue_script('DiviBars-main');
			
			wp_register_script('DiviBars-main-helper', DIVI_BARS_PLUGIN_URL . 'assets/js/main.helper.js', array('jquery', 'DiviBars-main'), DIVI_BARS_VERSION, TRUE );
			wp_enqueue_script('DiviBars-main-helper');
			
			wp_register_script('snap_svg_js', plugins_url('assets/js/snap.svg-min.js', __FILE__),array("jquery"));
			wp_enqueue_script('snap_svg_js');
			
			wp_register_script('modernizr_js', plugins_url('assets/js/modernizr.custom.js', __FILE__),array("jquery"));
			wp_enqueue_script('modernizr_js');
			
			wp_register_script('jquery_transit', plugins_url('assets/js/jquery.transit.min.js', __FILE__),array("jquery"));
			wp_enqueue_script('jquery_transit');
			
			wp_register_script('actual', plugins_url('assets/js/actual.min.js', __FILE__),array("jquery"));
			wp_enqueue_script('actual');
		}
		
		
		public static function add_var_ajaxurl() {
		?>
		<script type="text/javascript">
		var ajax_url = '<?php print admin_url('admin-ajax.php'); ?>';
		</script>
		<?php
		}
		
		
		public static function prevent_playable_tags() {
			?>
			<script type="text/javascript">
				function dibTogglePlayableTags( divibar_id, wait ) {
				
					var $ = jQuery;
					
					if ( !divibar_id  ) {
						
						divibar_id = "";
					}
					
					if ( !wait  ) {
						
						wait = 1;
					}
					
					/* Prevent playable tags load content before divibar call */
					setTimeout(function() {
						
						$( divibar_id + ".divibars").find("iframe").each(function() { 
						
							var iframeParent = $(this).parent();
							var iframe = $(this).prop("outerHTML");
							var src = iframe.match(/src=[\'"]?((?:(?!\/>|>|"|\'|\s).)+)"/)[0];
							
							src = src.replace("src", "data-src");
							iframe = iframe.replace(/src=".*?"/i, "src=\"about:blank\" data-src=\"\"" );
							
							if ( src != "data-src=\"about:blank\"" ) {
								iframe = iframe.replace("data-src=\"\"", src );
							}
							
							$( iframe ).insertAfter( $(this) );
							
							$(this).remove();
						});
						
					}, wait);
					
					$( divibar_id + ".divibars").find("video").each(function() {
						$(this).get(0).pause();
					});
					
					$( divibar_id + ".divibars").find("audio").each(function() {
						
						this.pause();
						this.currentTime = 0;
					});
				}
				
				dibTogglePlayableTags( '', 1000 );
			</script>
			<?php
		}
		
		
		/**
		 * Log debugging infoormation to the error log.
		 *
		 * @param string $e The Exception object
		 */
		protected static function log( $e = FALSE ) {
			
			$data_log = $e;
			
			if ( is_object( $e ) ) {
				
				$data_log = sprintf( "Exception: \n %s \n", $e->getMessage() . "\r\n\r\n" . $e->getFile() . "\r\n" . 'Line:' . $e->getLine() );
			}
			
			if ( apply_filters( 'divibars_log', defined( 'WP_DEBUG' ) && WP_DEBUG && defined( 'WP_DEBUG_LOG' ) && WP_DEBUG_LOG ) ) {
				error_log( print_r( compact( 'data_log' ), true ) );
			}
		}
	}