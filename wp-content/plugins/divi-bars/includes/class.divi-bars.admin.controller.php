<?php

	class DiviBars_Admin_Controller {
		
		protected static $_show_errors = FALSE;
		
		public static function get_wp_posts() {
			
			if ( isset( $_POST['q'] ) ) {
			
				$q = stripslashes( $_POST['q'] );
			
			} else {
				
				return;
			}
			
			
			if ( isset( $_POST['page'] ) ) {
				
				$page = (int) $_POST['page'];
				
			} else {
				
				$page = 1;
			}
			
			
			if ( isset( $_POST['json'] ) ) {
				
				$json = (int) $_POST['json'];
				
			} else {
				
				$json = 0;
			}
			
			$data = null;
			
			if ( ! function_exists( 'et_get_registered_post_type_options' ) ) {
				
				$data = json_encode(
				
					array(
						'total_count' => 1,
						'items' => array( 0 => (object) array( 'id' => 0, 'post_title' => 'This functionality requires Divi.' ) )
					)
				);
					
				die( $data );
			}
			
			$post_types = et_get_registered_post_type_options();
			
			$excluded_post_types = array( 'attachment', 'revision', 'nav_menu_item', 'custom_css', 'et_pb_layout', 'divi_bars', 'divi_overlay', 'divi_mega_pro', 'customize_changeset' );
			
			$post_types = array_diff( array_keys( $post_types ), $excluded_post_types );
			
			$posts = array();
			
			$total_count = 0;
			
			$args = array(
				'post_title_like' => $q,
				'post_type' => $post_types,
				'cache_results'  => false,
				'posts_per_page' => 7,
				'paged' => $page,
				'orderby' => 'id',
				'order' => 'DESC'
			);
			$query = new WP_Query( $args );
			
			$get_posts = $query->get_posts();
			
			$posts = array_merge( $posts, $get_posts );
			
			$total_count = (int) $query->found_posts;
			
			$posts = self::keysToLower( $posts );
			
			$fields = array( 'post_title', 'id' );
			
			$json_posts = array();
			foreach( $posts as $post ) {
				
				$post_filtered = array();
				
				foreach( $fields as $field ) {
					$post_filtered[ $field ] = $post->$field;
				}
				
				$json_posts[] = $post_filtered;
			}
			
			if ( $json ) {
				
				header( 'Content-type: application/json' );
				$data = json_encode(
				
					array(
						'total_count' => $total_count,
						'items' => $json_posts
					)
				);
				
				die( $data );
			}
			
			return $posts;
		}
		
		
		private static function keysToLower( &$obj )
		{
			$type = (int) is_object($obj) - (int) is_array($obj);
			if ($type === 0) return $obj;
			foreach ($obj as $key => &$val)
			{
				$element = self::keysToLower($val);
				switch ($type)
				{
				case 1:
					if (!is_int($key) && $key !== ($keyLowercase = strtolower($key)))
					{
						unset($obj->{$key});
						$key = $keyLowercase;
					}
					$obj->{$key} = $element;
					break;
				case -1:
					if (!is_int($key) && $key !== ($keyLowercase = strtolower($key)))
					{
						unset($obj[$key]);
						$key = $keyLowercase;
					}
					$obj[$key] = $element;
					break;
				}
			}
			return $obj;
		}
		
		
		public static function add_meta_boxes() {
			
			$screen = get_current_screen();
			
			if ( $screen->post_type == 'divi_bars' ) {
				
				if ( 'add' != $screen->action ) {
					
					add_meta_box( 
						'divibars_manualtriggers', 
						esc_html__( 'Manual Triggers', 'DiviBars' ), 
						array( 'DiviBars_Admin_Controller', 'manualtriggers_callback' ), 
						'divi_bars', 
						'side', 
						'high' 
					);
				}
				
				$status = get_option( 'divilife_edd_divibars_license_status' );
				$check_license = divilife_edd_divibars_check_license( TRUE );
				if ( ( isset( $check_license->license ) && $check_license->license != 'valid' && 'add' == $screen->action ) 
					|| ( isset( $check_license->license ) && $check_license->license != 'valid' && 'edit' == $_GET['action'] ) 
					|| ( $status === false && 'add' == $screen->action ) 
					|| ( $status === false && 'edit' == $_GET['action'] ) 
					) {
					
					$message = '';
					$base_url = admin_url( 'edit.php?post_type=divi_bars&page=divibars-settings' );
					$redirect = add_query_arg( array( 'sl_activation' => 'false', 'message' => urlencode( $message ) ), $base_url );
					
					wp_redirect( $redirect );
					exit();
				}
				
				add_meta_box( 
					'divibars_displaylocations_meta_box1', 
					esc_html__( 'Display Locations', 'DiviBars' ), 
					array( 'DiviBars_Admin_Controller', 'divibars_displaylocations_callback' ),
					'divi_bars', 
					'side'
				);
				
				add_meta_box( 
					'divibars_moresettings_meta_box2', 
					esc_html__( 'Additional Settings', 'DiviBars' ), 
					array( 'DiviBars_Admin_Controller', 'divibars_moresettings_callback' ),
					'divi_bars', 
					'side'
				);
				
				add_meta_box( 
					'divibars_closecustoms_meta_box3', 
					esc_html__( 'Close Button Customizations', 'DiviBars' ), 
					array( 'DiviBars_Admin_Controller', 'divibars_closecustoms_callback' ), 
					'divi_bars', 
					'side'
				);
				
				add_meta_box( 
					'divibars_automatictriggers4', 
					esc_html__( 'Automatic Triggers', 'DiviBars' ), 
					array( 'DiviBars_Admin_Controller', 'divibars_automatictriggers_callback' ), 
					'divi_bars', 
					'side'
				);
				
				add_meta_box( 
					'divibars_color_picker', 
					'Divibars Background', 
					array( 'DiviBars_Admin_Controller', 'divibars_colorbox_callback' ), 
					'divi_bars'
				);
			}
		}
		
		
		public static function manualtriggers_callback( $post ) {
			?>
			<div class="custom_meta_box">
				<p>
					<label class="label-color-field"><p>CSS ID:</label>
					divibars_unique_id_<?php print $post->ID ?></p>
				</p>
				<div class="clear"></div> 
			</div> 
			<div class="custom_meta_box">
				<p>
					<label class="label-color-field"><p>Menu ID:</label>
					unique_divibars_menu_id_<?php print $post->ID ?></p>
				</p>
				<div class="clear"></div> 
			</div>
			<?php
		}
		
		
		public static function divibars_displaylocations_callback( $post ) {
		
			wp_nonce_field( 'divibars_displaylocations', 'divibars_displaylocations_nonce' );
			
			$at_pages = get_post_meta( $post->ID, 'at_pages', true );
			$selectedpages = get_post_meta( $post->ID, 'at_pages_selected' );
			$selectedexceptpages = get_post_meta( $post->ID, 'at_pagesexception_selected' );
			if( $at_pages == '' ) {
				
				$at_pages = 'all';
			}
			
			?>
			<div class="custom_meta_box">
				<div class="at_pages">
					<select name="post_at_pages" class="at_pages chosen divibars-filter-by-pages" data-dropdownshowhideblock="1">
						<option value="all"<?php if ( $at_pages == 'all' ) { ?> selected="selected"<?php } ?> data-showhideblock=".do-list-exceptionpages-container">All pages</option>
						<option value="specific"<?php if ( $at_pages == 'specific' ) { ?> selected="selected"<?php } ?> data-showhideblock=".do-list-pages-container">Only specific pages</option>
					</select>
					<div class="do-list-pages-container<?php if ( $at_pages == 'specific' ) { ?> do-show<?php } ?>">
						<select name="post_at_pages_selected[]" class="do-list-pages" data-placeholder="Choose posts or pages..." multiple tabindex="3">
						<?php
							if ( isset( $selectedpages[0] ) && is_array( $selectedpages[0] )  ) {
								
								foreach( $selectedpages[0] as $selectedidx => $selectedvalue ) {
									
									$post_title = get_the_title( $selectedvalue );
									
									print '<option value="' . $selectedvalue . '" selected="selected">' . $post_title . '</option>';
								}
							}
						?>
						</select>
					</div>
					<div class="do-list-exceptionpages-container<?php if ( $at_pages == 'all' ) { ?> do-show<?php } ?>">
						<h4 class="db-exceptedpages">Add Exceptions:</h4>
						<select name="post_at_exceptionpages_selected[]" class="do-list-pages" data-placeholder="Choose posts or pages..." multiple tabindex="3">
						<?php
							if ( isset( $selectedexceptpages[0] ) && is_array( $selectedexceptpages[0] )  ) {
								
								foreach( $selectedexceptpages[0] as $selectedidx => $selectedvalue ) {
									
									$post_title = get_the_title( $selectedvalue );
									
									print '<option value="' . $selectedvalue . '" selected="selected">' . $post_title . '</option>';
								}
							}
						?>
						</select>
					</div>
				</div>
				<div class="clear"></div> 
			</div>
			<?php
		}
		
		
		public static function divibars_colorbox_callback( $post ) {
			
			wp_nonce_field( 'divibars_color_box', 'divibars_color_box_nonce' );
			$color = get_post_meta( $post->ID, 'post_divibars_bg_color', true );
			$fontcolor = get_post_meta( $post->ID, 'post_divibars_font_color', true );
			?>
			<div class="custom_meta_box">
				<p>
					<label class="label-color-field">Select Background Color: </label>
					<input class="cs-wp-color-picker" type="text" name="post_bg" value="<?php echo $color; ?>"/>
				</p>
				<div class="clear"></div> 
			</div> 
			<div class="custom_meta_box">
				<p>
					<label class="label-color-field">Select Font Color: </label>
					<input class="color-field" type="text" name="post_font_color" value="<?php echo $fontcolor; ?>"/>
				</p>
				<div class="clear"></div> 
			</div> 
			<script>
				(function( $ ) {
					// Add Color Picker to all inputs that have 'color-field' class
					$(function() {
						$('.color-field').wpColorPicker();
					});
				})( jQuery );
			</script>
			<?php
		}
		
		
		public static function divibars_effects_callback( $post ) {
			
			$post_id = get_the_ID();
			$divibar_effect = get_post_meta( $post_id, 'divibar_effect', true );
			$divibar_effects = array(
				'divibars-hugeinc'   => esc_html__( 'Fade & Slide', 'Divi' ),
				'divibars-corner'    => esc_html__( 'Corner', 'Divi' ),
				'divibars-slidedown' => esc_html__( 'Slide down', 'Divi' ),
				'divibars-scale' => esc_html__( 'Scale', 'Divi' ),
				'divibars-door' => esc_html__( 'Door', 'Divi' ),
				'divibars-contentpush' => esc_html__( 'Content Push', 'Divi' ),
				'divibars-contentscale' => esc_html__( 'Content Scale', 'Divi' ),
				'divibars-cornershape' => esc_html__( 'Corner Shape', 'Divi' ),
				'divibars-boxes' => esc_html__( 'Little Boxes', 'Divi' ),
				'divibars-simplegenie' => esc_html__( 'Simple Genie', 'Divi' ),
				'divibars-genie' => esc_html__( 'Genie', 'Divi' ),
			);
			?>
			<p class="et_pb_page_settings et_pb_single_title">
				<label for="divibar_effect" class="divibar-label-effect"><?php esc_html_e( 'Select DiviBars Animation', 'Divi' ); ?>: </label>
				<select id="divibar_effect" name="divibar_effect" class="chosen">
				<?php
				foreach ( $divibar_effects as $divibar_value => $divibar_name ) {
					printf( '<option value="%2$s"%3$s>%1$s</option>',
						esc_html( $divibar_name ),
						esc_attr( $divibar_value ),
						selected( $divibar_value, $divibar_effect, false )
					);
				} ?>
				</select>
			</p>
			
		<?php }
		
		
		public static function divibars_moresettings_callback( $post ) {
		
			wp_nonce_field( 'divibars_moresettings', 'divibars_moresettings_nonce' );
			
			$css_selector = get_post_meta( $post->ID, 'dib_css_selector', true );
			
			$pushpage = get_post_meta( $post->ID, 'post_pushpage' );
			if( !isset( $pushpage[0] ) ) {
				
				$pushpage[0] = '1';
			}
			
			$screenfixed = get_post_meta( $post->ID, 'post_screenfixed' );
			if( !isset( $screenfixed[0] ) ) {
				
				$screenfixed[0] = '0';
			}
			
			$enableurltrigger = get_post_meta( $post->ID, 'dib_enableurltrigger' );
			if( !isset( $enableurltrigger[0] ) ) {
				
				$enableurltrigger[0] = '0';
			}
			
			$divibar_placement = get_post_meta( $post->ID, 'dib_post_placement', true );
			
			?>
			<div class="custom_meta_box">
				<p>
					<label>CSS Selector Trigger:</label>
					<input class="css_selector" type="text" name="dib_css_selector" value="<?php echo $css_selector; ?>"/>
				</p>
				<div class="clear"></div> 
			</div>
			<?php
			
			if( !isset( $divibar_placement ) ) {
				
				$divibar_placement = 'top';
			}
			
			$divibar_ats = array(
				'top'   => esc_html__( 'Top', 'DiviBars' ),
				'bottom'    => esc_html__( 'Bottom', 'DiviBars' )
			);
			?>
			<div class="custom_meta_box">
				<p class="divibar_placement et_pb_single_title">
					<label for="dib_post_placement">Position:</label>
					<select id="dib_post_placement" name="dib_post_placement" class="post_divibar_placement chosen">
					<?php
					foreach ( $divibar_ats as $at_value => $at_name ) {
						printf( '<option value="%2$s"%3$s>%1$s</option>',
							esc_html( $at_name ),
							esc_attr( $at_value ),
							selected( $at_value, $divibar_placement, false )
						);
					} ?>
					</select>
				</p>
			</div>
			
			<div class="custom_meta_box">
				<p>
					<input name="post_pushpage" type="checkbox" id="post_pushpage" value="1" <?php checked( $pushpage[0], 1 ); ?> /> Push Page Up/Down
				</p>
				<div class="clear"></div> 
			</div>
			
			<div class="custom_meta_box moresettings_screenfixed<?php if ( $divibar_placement == 'bottom' ) { ?> do-hide<?php } ?>">
				<p>
					<input name="post_screenfixed" type="checkbox" id="post_screenfixed" value="1" <?php checked( $screenfixed[0], 1 ); ?> /> Do not Fix to Top
				</p>
				<div class="clear"></div> 
			</div>
			
			<div class="custom_meta_box">
				<p>
					<label for="dib_enableurltrigger"></label>
					<input name="dib_enableurltrigger" type="checkbox" class="dib_enableurltrigger" data-showhideblock=".dib_enableurltrigger_filters" value="1" <?php checked( $enableurltrigger[0], 1 ); ?> /> Enable URL Trigger
				</p>
				<div class="clear"></div> 
			</div>
			<?php
		}
		
		
		public static function divibars_closecustoms_callback( $post ) {
			
			wp_nonce_field( 'divibars_closecustoms', 'divibars_closecustoms_nonce' );
			
			$close_cookie = get_post_meta( $post->ID, 'post_closebtn_cookie', true );
			$textcolor = get_post_meta( $post->ID, 'post_closebtn_text_color', true );
			$bgcolor = get_post_meta( $post->ID, 'post_closebtn_bg_color', true );
			$fontsize = get_post_meta( $post->ID, 'post_closebtn_fontsize', true );
			$borderradius = get_post_meta( $post->ID, 'post_closebtn_borderradius', true );
			$padding = get_post_meta( $post->ID, 'post_closebtn_padding', true );
			
			if( $close_cookie == '' ) {
				
				$close_cookie = 0;
			}
			
			if( $fontsize == '' ) {
				
				$fontsize = 25;
			}
			
			$hideclosebtn = get_post_meta( $post->ID, 'post_do_hideclosebtn' );
			if( !isset( $hideclosebtn[0] ) ) {
				
				$hideclosebtn[0] = '0';
			}
			
			$customizeclosebtn = get_post_meta( $post->ID, 'post_do_customizeclosebtn' );
			if( !isset( $customizeclosebtn[0] ) ) {
				
				$customizeclosebtn[0] = '0';
			}
			
			?>
			<div class="custom_meta_box">
				<p>
					<label>Close Button Cookie:</label>
					<input class="post_closebtn_cookie" type="text" name="post_closebtn_cookie" value="<?php echo $close_cookie; ?>" readonly="readonly"> days
				</p>
				<div id="slider-closebtn-cookie" class="slider-bar"></div>
			</div>
			
			<div class="custom_meta_box">
				<p>
					<input name="post_do_hideclosebtn" type="checkbox" id="post_do_hideclosebtn" value="1" <?php checked( $hideclosebtn[0], 1 ); ?> /> Hide Main Close Button
				</p>
				<div class="clear"></div> 
			</div>
			
			<div class="custom_meta_box">
				<p>
					<input name="post_do_customizeclosebtn" type="checkbox" id="post_do_customizeclosebtn" value="1" data-showhideblock=".enable_customizations" <?php checked( $customizeclosebtn[0], 1 ); ?> /> Customize Close Button
				</p>
				<div class="enable_customizations<?php if ( $customizeclosebtn[0] == 1 ) { ?> do-show<?php } ?>">
					<div class="custom_meta_box">
						<p>
							<label class="label-color-field">Text color:</label>
							<input class="closebtn-text-color" type="text" name="post_closebtn_text_color" value="<?php echo $textcolor; ?>"/>
						</p>
						<div class="clear"></div> 
					</div> 
					<div class="custom_meta_box">
						<p>
							<label class="label-color-field">Background color:</label>
							<input class="closebtn-bg-color" type="text" name="post_closebtn_bg_color" value="<?php echo $bgcolor; ?>"/>
						</p>
						<div class="clear"></div> 
					</div>
					<div class="custom_meta_box">
						<p>
							<label>Font size:</label>
							<input class="post_closebtn_fontsize" type="text" name="post_closebtn_fontsize" value="<?php echo $fontsize; ?>" readonly="readonly" > px
						</p>
						<div id="slider-closebtn-fontsize" class="slider-bar"></div>
					</div>
					<div class="custom_meta_box">
						<p>
							<label>Border radius:</label>
							<input class="post_closebtn_borderradius" type="text" name="post_closebtn_borderradius" value="<?php echo $borderradius; ?>" readonly="readonly" > %
						</p>
						<div id="slider-closebtn-borderradius" class="slider-bar"></div>
					</div>
					<div class="custom_meta_box">
						<p>
							<label>Padding:</label>
							<input class="post_closebtn_padding" type="text" name="post_closebtn_padding" value="<?php echo $padding; ?>" readonly="readonly" > px
						</p>
						<div id="slider-closebtn-padding" class="slider-bar"></div>
					</div>
					<div class="custom_meta_box">
						<p>
							<label>Preview:</label>
						</p>
						<button type="button" class="divibars-customclose-btn"><span>&times;</span></button>
					</div>
				</div>
				<div class="clear"></div> 
			</div>
			<?php
		}
		
		
		public static function divibars_automatictriggers_callback( $post ) {
			
			$post_id = get_the_ID();
			
			$disablemobile = get_post_meta( $post_id, 'divibars_automatictrigger_disablemobile' );
			$disabletablet = get_post_meta( $post_id, 'divibars_automatictrigger_disabletablet' );
			$disabledesktop = get_post_meta( $post_id, 'divibars_automatictrigger_disabledesktop' );
			
			$onceperload = get_post_meta( $post_id, 'divibars_automatictrigger_onceperload' );
			
			$enable_scheduling = get_post_meta( $post_id, 'divibars_enable_scheduling' );
			$date_start = get_post_meta( $post->ID, 'divibars_date_start', true );
			$date_end = get_post_meta( $post->ID, 'divibars_date_end', true );
			$date_start = DiviBars_Admin::$helper->convertDateToUserTimezone( $date_start );
			$date_end = DiviBars_Admin::$helper->convertDateToUserTimezone( $date_end );
			
			$time_start = get_post_meta( $post->ID, 'divibars_time_start', true );
			$time_end = get_post_meta( $post->ID, 'divibars_time_end', true );
			
			$divibar_at_selected = get_post_meta( $post_id, 'divibars_automatictrigger', true );
			$divibar_ats = array(
				'divibar-timed'   => esc_html__( 'Timed Delay', 'DiviBars' ),
				'divibar-scroll'    => esc_html__( 'Scroll Percentage', 'DiviBars' ),
				'divibar-exit' => esc_html__( 'Exit Intent', 'DiviBars' ),
			);
			
			for( $a = 1; $a <= 7; $a++ ) {
				
				$daysofweek[$a] = get_post_meta( $post_id, 'divibars_scheduling_daysofweek_' . $a );
				
				if ( !isset( $daysofweek[$a][0] ) ) {
					
					$daysofweek[$a][0] = '0';
				}
				else {
					
					$daysofweek[$a] = $daysofweek[$a][0];
				}
			}
			
			if ( !isset( $disablemobile[0] ) ) {
				
				$disablemobile[0] = '0';
			}
			
			if ( !isset( $disabletablet[0] ) ) {
				
				$disabletablet[0] = '0';
			}
			
			if ( !isset( $disabledesktop[0] ) ) {
				
				$disabledesktop[0] = '0';
			}
			
			
			if ( !isset( $onceperload[0] ) ) {
				
				$onceperload[0] = '1';
			}
			
			if ( !isset( $enable_scheduling[0] ) ) {
				
				$enable_scheduling[0] = 0;
			}
			?>
			<p class="divi_automatictrigger_settings et_pb_single_title">
				<label for="post_divibar_automatictrigger"></label>
				<select id="post_divibar_automatictrigger" name="post_divibar_automatictrigger" class="post_divibar_automatictrigger chosen">
					<option value="">None</option>
				<?php
				foreach ( $divibar_ats as $at_value => $at_name ) {
					printf( '<option value="%2$s"%3$s>%1$s</option>',
						esc_html( $at_name ),
						esc_attr( $at_value ),
						selected( $at_value, $divibar_at_selected, false )
					);
				} ?>
				</select>
			</p>
			
			<?php
			
				$at_timed = get_post_meta( $post->ID, 'divibars_automatictrigger_timed_value', true );
				$at_scroll_from = get_post_meta( $post->ID, 'divibars_automatictrigger_scroll_from_value', true );
				$at_scroll_to = get_post_meta( $post->ID, 'divibars_automatictrigger_scroll_to_value', true );
			?>
			<div class="divi_automatictrigger_timed<?php if ( $divibar_at_selected == 'divibar-timed' ) { ?> do-show<?php } ?>">
				<p>
					<label>Specify timed delay (in seconds):</label>
					<input class="post_at_timed" type="text" name="post_at_timed" value="<?php echo $at_timed; ?>"/>
				</p>
				<div class="clear"></div> 
			</div>
			
			<div class="divi_automatictrigger_scroll<?php if ( $divibar_at_selected == 'divibar-scroll' ) { ?> do-show<?php } ?>">
				<p>Specify in pixels or percentage:</p>
				<div class="at-scroll-settings">
					<label for="post_at_scroll_from">From:</label>
					<input class="post_at_scroll" type="text" name="post_at_scroll_from" value="<?php echo $at_scroll_from; ?>"/>
					<label for="post_at_scroll_to">to:</label>
					<input class="post_at_scroll" type="text" name="post_at_scroll_to" value="<?php echo $at_scroll_to; ?>"/>
				</div> 
				<div class="clear"></div> 
			</div>
			
			<div class="custom_meta_box">
				<p>
					<input name="post_at_disablemobile" type="checkbox" id="post_at_disablemobile" value="1" <?php checked( $disablemobile[0], 1 ); ?> /> Disable On Mobile
				</p>
				<div class="clear"></div> 
			</div>
			
			<div class="custom_meta_box">
				<p>
					<input name="post_at_disabletablet" type="checkbox" id="post_at_disabletablet" value="1" <?php checked( $disabletablet[0], 1 ); ?> /> Disable On Tablet
				</p>
				<div class="clear"></div> 
			</div>
			
			<div class="custom_meta_box">
				<p>
					<input name="post_at_disabledesktop" type="checkbox" id="post_at_disabledesktop" value="1" <?php checked( $disabledesktop[0], 1 ); ?> /> Disable On Desktop
				</p>
				<div class="clear"></div> 
			</div>
			
			<div class="custom_meta_box">
				<p>
					<input name="post_at_onceperload" type="checkbox" id="post_at_onceperload" value="1" <?php checked( $onceperload[0], 1 ); ?> />
					 Display once per page load
				</p>
				<div class="clear"></div> 
			</div>
			
			<div class="custom_meta_box">
				<div class="custom_meta_box">
					<p class="divibar_placement et_pb_single_title">
						<label for="dib_enable_scheduling">Set Scheduling:</label>
						<select name="dib_enable_scheduling" class="chosen divibars-enable-scheduling" data-dropdownshowhideblock="1">
							<option value="0"<?php if ( $enable_scheduling[0] == 0 ) { ?> selected="selected"<?php } ?>>Disabled</option>
							<option value="1"<?php if ( $enable_scheduling[0] == 1 ) { ?> selected="selected"<?php } ?> data-showhideblock=".dib-onetime">
							<?php print esc_html__( 'Start &amp; End Time', 'DiviBars' ) ?>
							</option>
							<option value="2"<?php if ( $enable_scheduling[0] == 2 ) { ?> selected="selected"<?php } ?> data-showhideblock=".dib-recurring">
							<?php print esc_html__( 'Recurring Scheduling', 'DiviBars' ) ?>
							</option>
						</select>
					</p>
				</div>
				
				<div class="row dib-onetime<?php if ( $enable_scheduling[0] == 1 ) { ?> do-show<?php } ?>">
					<div class="col-xs-12">
						<label>
							Start date <br/>
							<input type="text" name="dib_date_start" value="<?php print $date_start; ?>" class="form-control">
						</label>
					</div>
					<div class="col-xs-12">
						<label>
							End date <br/>
							<input type="text" name="dib_date_end" value="<?php print $date_end; ?>" class="form-control">
						</label>
					</div>
				</div>
				
				<div class="row dib-recurring<?php if ( $enable_scheduling[0] == 2 ) { ?> do-show<?php } ?>">
					<div class="col-xs-12">
						<p><input name="divibars_scheduling_daysofweek[]" type="checkbox" id="divibars_scheduling_daysofweek" value="1" <?php checked( $daysofweek[1][0], 1 ); ?> /> Monday</p>
						<p><input name="divibars_scheduling_daysofweek[]" type="checkbox" id="divibars_scheduling_daysofweek" value="2" <?php checked( $daysofweek[2][0], 1 ); ?> /> Tuesday</p>
						<p><input name="divibars_scheduling_daysofweek[]" type="checkbox" id="divibars_scheduling_daysofweek" value="3" <?php checked( $daysofweek[3][0], 1 ); ?> /> Wednesday</p>
						<p><input name="divibars_scheduling_daysofweek[]" type="checkbox" id="divibars_scheduling_daysofweek" value="4" <?php checked( $daysofweek[4][0], 1 ); ?> /> Thursday</p>
						<p><input name="divibars_scheduling_daysofweek[]" type="checkbox" id="divibars_scheduling_daysofweek" value="5" <?php checked( $daysofweek[5][0], 1 ); ?> /> Friday</p>
						<p><input name="divibars_scheduling_daysofweek[]" type="checkbox" id="divibars_scheduling_daysofweek" value="6" <?php checked( $daysofweek[6][0], 1 ); ?> /> Saturday</p>
						<p><input name="divibars_scheduling_daysofweek[]" type="checkbox" id="divibars_scheduling_daysofweek" value="7" <?php checked( $daysofweek[7][0], 1 ); ?> /> Sunday</p>
					</div>
					<div class="col-sm-6">
						<label>
							Start Time <br/>
							<input type="text" name="dib_time_start" value="<?php print $time_start; ?>" class="form-control">
						</label>
						<div id="datetimepicker12"></div>
					</div>
					<div class="col-sm-6">
						<label>
							End Time <br/>
							<input type="text" name="dib_time_end" value="<?php print $time_end; ?>" class="form-control">
						</label>
					</div>
				</div>
				
				<div class="row">
					<div class="col-xs-12">
						<div class="dib-recurring-user-msg alert alert-danger">
							
						</div>
					</div>
				</div>
				
				<div class="clear"></div> 
			</div>
			
			<?php
		}
		
		
		public static function save_post( $post_id, $post ) {
			
			global $pagenow;

			if ( 'post.php' != $pagenow ) return $post_id;

			if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
				return $post_id;

			$post_type = get_post_type_object( $post->post_type );
			if ( ! current_user_can( $post_type->cap->edit_post, $post_id ) )
				return $post_id;
			
			$post_value = '';
			
			if ( isset( $_POST['post_bg'] ) ) {
				update_post_meta( $post_id, 'post_divibars_bg_color', sanitize_text_field( $_POST['post_bg'] ) );
			}
			if ( isset( $_POST['post_font_color'] ) ) {
				update_post_meta( $post_id, 'post_divibars_font_color', sanitize_text_field( $_POST['post_font_color'] ) );
			}
			
			
			/* Display Locations */
			if ( isset( $_POST['post_at_pages'] ) ) {
				
				$post_at_pages = sanitize_text_field( $_POST['post_at_pages'] );
				update_post_meta( $post_id, 'at_pages', $post_at_pages );
			}
			
			if ( $post_at_pages == 'specific' ) {
				
				if ( isset( $_POST['post_at_pages_selected'] ) ) {
					update_post_meta( $post_id, 'at_pages_selected', $_POST['post_at_pages_selected'] );
				}
			}
			else {
				
				update_post_meta( $post_id, 'at_pages_selected', '' );
			}
			
			if ( isset( $_POST['post_at_exceptionpages_selected'] ) ) {
			
				update_post_meta( $post_id, 'at_pagesexception_selected', $_POST['post_at_exceptionpages_selected'] );
			}
			
			
			/* Additional Settings */
			if ( isset( $_POST['dib_css_selector'] ) ) {
				update_post_meta( $post_id, 'dib_css_selector', sanitize_text_field( $_POST['dib_css_selector'] ) );
			}
			
			if ( isset( $_POST['dib_post_placement'] ) ) {
				update_post_meta( $post_id, 'dib_post_placement', sanitize_text_field( $_POST['dib_post_placement'] ) );
			}
			
			if ( isset( $_POST['post_pushpage'] ) ) {
				
				$post_pushpage = 1;
				
			} else {
				
				$post_pushpage = 0;
			}
			update_post_meta( $post_id, 'post_pushpage', $post_pushpage );
			
			
			if ( isset( $_POST['dib_enableurltrigger'] ) ) {
				
				$dib_enableurltrigger = 1;
				
			} else {
				
				$dib_enableurltrigger = 0;
			}
			update_post_meta( $post_id, 'dib_enableurltrigger', $dib_enableurltrigger );
			
			
			
			if ( isset( $_POST['post_screenfixed'] ) ) {
				
				$post_screenfixed = 1;
				
			} else {
				
				$post_screenfixed = 0;
			}
			update_post_meta( $post_id, 'post_screenfixed', $post_screenfixed );
			
			
			if ( isset( $_POST['post_divibar_automatictrigger'] ) ) {
				
				update_post_meta( $post_id, 'divibars_automatictrigger', sanitize_text_field( $_POST['post_divibar_automatictrigger'] ) );
			
				if ( isset( $_POST['post_at_timed'] ) ) {
					update_post_meta( $post_id, 'divibars_automatictrigger_timed_value', sanitize_text_field( $_POST['post_at_timed'] ) );
				}
				
				if ( isset( $_POST['post_at_scroll_from'] ) || isset( $_POST['post_at_scroll_to'] ) ) {
					update_post_meta( $post_id, 'divibars_automatictrigger_scroll_from_value', sanitize_text_field( $_POST['post_at_scroll_from'] ) );
					update_post_meta( $post_id, 'divibars_automatictrigger_scroll_to_value', sanitize_text_field( $_POST['post_at_scroll_to'] ) );
				}
				
				if ( isset( $_POST['post_at_disablemobile'] ) ) {
					
					$post_at_disablemobile = 1;
					
				} else {
					
					$post_at_disablemobile = 0;
				}
				
				if ( isset( $_POST['post_at_disabletablet'] ) ) {
					
					$post_at_disabletablet = 1;
					
				} else {
					
					$post_at_disabletablet = 0;
				}
				
				if ( isset( $_POST['post_at_disabledesktop'] ) ) {
					
					$post_at_disabledesktop = 1;
					
				} else {
					
					$post_at_disabledesktop = 0;
				}
				
				if ( isset( $_POST['post_at_onceperload'] ) ) {
					
					$post_at_onceperload = 1;
					
				} else {
					
					$post_at_onceperload = 0;
				}
				
				update_post_meta( $post_id, 'divibars_automatictrigger_onceperload', $post_at_onceperload );
				
			} else {
				
				$post_at_disablemobile = 0;
				$post_at_disabletablet = 0;
				$post_at_disabledesktop = 0;
			}
			update_post_meta( $post_id, 'divibars_automatictrigger_disablemobile', $post_at_disablemobile );
			update_post_meta( $post_id, 'divibars_automatictrigger_disabletablet', $post_at_disabletablet );
			update_post_meta( $post_id, 'divibars_automatictrigger_disabledesktop', $post_at_disabledesktop );
			
			/* Close Button Customizations */
			if ( isset( $_POST['post_closebtn_cookie'] ) ) {
				update_post_meta( $post_id, 'post_closebtn_cookie', sanitize_text_field( $_POST['post_closebtn_cookie'] ) );
			}
			
			if ( isset( $_POST['post_do_hideclosebtn'] ) ) {
				
				$post_do_hideclosebtn = 1;
				
			} else {
				
				$post_do_hideclosebtn = 0;
			}
			update_post_meta( $post_id, 'post_do_hideclosebtn', $post_do_hideclosebtn );
			
			if ( isset( $_POST['post_do_customizeclosebtn'] ) ) {
				
				$post_do_customizeclosebtn = 1;
				
			} else {
				
				$post_do_customizeclosebtn = 0;
			}
			update_post_meta( $post_id, 'post_do_customizeclosebtn', $post_do_customizeclosebtn );
			
			if ( isset( $_POST['post_closebtn_text_color'] ) ) {
				update_post_meta( $post_id, 'post_closebtn_text_color', sanitize_text_field( $_POST['post_closebtn_text_color'] ) );
			}
			
			if ( isset( $_POST['post_closebtn_bg_color'] ) ) {
				update_post_meta( $post_id, 'post_closebtn_bg_color', sanitize_text_field( $_POST['post_closebtn_bg_color'] ) );
			}
			
			if ( isset( $_POST['post_closebtn_fontsize'] ) ) {
				update_post_meta( $post_id, 'post_closebtn_fontsize', sanitize_text_field( $_POST['post_closebtn_fontsize'] ) );
			}
			
			if ( isset( $_POST['post_closebtn_borderradius'] ) ) {
				update_post_meta( $post_id, 'post_closebtn_borderradius', sanitize_text_field( $_POST['post_closebtn_borderradius'] ) );
			}
			
			if ( isset( $_POST['post_closebtn_padding'] ) ) {
				update_post_meta( $post_id, 'post_closebtn_padding', sanitize_text_field( $_POST['post_closebtn_padding'] ) );
			}
			
			if ( isset( $_POST['dib_enable_scheduling'] ) ) {
				
				$enable_scheduling = (int) $_POST['dib_enable_scheduling'];
				
			} else {
				
				$enable_scheduling = 0;
			}
			update_post_meta( $post_id, 'divibars_enable_scheduling', $enable_scheduling );
			
			/* Save Scheduling */
			if ( $enable_scheduling ) {
				
				$divibars_settings = get_option( 'dib_settings' );
				
				$timezone = DIVI_SERVER_TIMEZONE;
				
				$wp_timezone = get_option('timezone_string');
				
				if ( $wp_timezone !== false ) {
					
					$timezone = $wp_timezone;
				}
				
				if ( $enable_scheduling == 1 ) {
					
					if ( isset( $_POST['dib_date_start'] ) ) {
						
						$date_string = DiviBars_Admin::$helper->convertDateToUTC( $_POST['dib_date_start'], $timezone );
						
						update_post_meta( $post_id, 'divibars_date_start', sanitize_text_field( $date_string ) );
					}
					
					if ( isset( $_POST['dib_date_end'] ) ) {
						
						$date_string = DiviBars_Admin::$helper->convertDateToUTC( $_POST['dib_date_end'], $timezone );
						
						update_post_meta( $post_id, 'divibars_date_end', sanitize_text_field( $date_string ) );
					}
				}
				
				if ( $enable_scheduling == 2 ) {
					
					if ( isset( $_POST['dib_time_start'] ) ) {
						
						$date_string = $_POST['dib_time_start'];
						
						update_post_meta( $post_id, 'divibars_time_start', sanitize_text_field( $date_string ) );
					}
					
					if ( isset( $_POST['dib_time_end'] ) ) {
						
						$date_string = $_POST['dib_time_end'];
						
						update_post_meta( $post_id, 'divibars_time_end', sanitize_text_field( $date_string ) );
					}
					
					if ( isset( $_POST['divibars_scheduling_daysofweek'] ) ) {
					
						$daysofweek = array_map( 'sanitize_text_field', wp_unslash( $_POST['divibars_scheduling_daysofweek'] ) );
						
						// Reset all daysofweek values
						for( $a = 1; $a <= 7; $a++ ) {
							update_post_meta( $post_id, 'divibars_scheduling_daysofweek_' . $a, 0 );
						}
						
						foreach( $daysofweek as $day ) {
							update_post_meta( $post_id, 'divibars_scheduling_daysofweek_' . $day, 1);
						}
					}
				}
			}
		}
		
		
		public static function add_admin_submenu() {
			
			// Admin page
			add_submenu_page( 'edit.php?post_type=divi_bars', 'Divi Bars', 'Settings', 'edit_posts', 'divibars-settings', array( 'DiviBars_Admin_Controller', 'admin_settings' ) );
		}
		
		
		public static function admin_settings() {
			
			self::display_configuration_page();
		}
		
		
		public static function display_configuration_page() {
			
			DiviBars_Admin::$options = get_option( 'dib_settings' );
        ?>
        <div class="wrap">
            <h1>Divi Bars</h1>
            <form method="post" action="options.php">
            <?php
                // This prints out all hidden setting fields
                settings_fields( 'divibars_settings' );
                do_settings_sections( 'divibars-settings' );
                submit_button();
            ?>
            </form>
        </div>
	<?php
	
	$license = get_option( 'divilife_edd_divibars_license_key' );
	$status  = get_option( 'divilife_edd_divibars_license_status' );
	$check_license = divilife_edd_divibars_check_license( TRUE );
	
	if ( $license != '' ) {
		
		$license = '*******';
	}
	
	if ( isset( $check_license->expires ) ) {
		
		$license_expires = strtotime( $check_license->expires );
		$now = strtotime('now');
		$timeleft = $license_expires - $now;
		
		$daysleft = round( ( ( $timeleft / 24 ) / 60 ) / 60 );
		if ( $daysleft > 0 ) {
			
			$daysleft = '( ' . ( ( $daysleft > 1 ) ? $daysleft . ' days left' : $daysleft . ' day left' ) . ' )';
			
		} else {
			
			$daysleft = '';
		}
	}
	?>
	<div class="wrap">
		<h2><?php _e('License Settings'); ?></h2>
		<form method="post" action="options.php">

			<?php settings_fields('divilife_edd_divibars_license'); ?>

			<table class="form-table">
				<tbody>
					<tr valign="top">
						<th scope="row" valign="top">
							<?php _e('License Key'); ?>
						</th>
						<td>
							<input id="divilife_edd_divibars_license_key" name="divilife_edd_divibars_license_key" type="text" class="regular-text" value="<?php esc_attr_e( $license ); ?>" />
							<label class="description" for="divilife_edd_divibars_license_key"><?php _e('Enter your license key'); ?></label>
						</td>
					</tr>
					<?php if ( FALSE !== $license ) { ?>
					
						<tr valign="top">
							<th scope="row" valign="top">
								<?php _e('License Status'); ?>
							</th>
							<td>
								<?php if ( $status === false ) {
									wp_nonce_field( 'divilife_edd_divibars_nonce', 'divilife_edd_divibars_nonce' ); ?>
									<input type="submit" class="button-secondary" name="divilife_edd_divibars_license_activate" value="<?php _e('Activate License'); ?>"/>
								<?php } ?>
								
								<h3 class="dl-edm-heading-msg no-margin">
									<?php 
										if ( $status !== false && $check_license->license == 'valid' ) {
											
											print '<span class="dashicons dashicons-yes dashicons-success dashicons-large"></span><br><br>';
										}
										else {
											
											if ( $check_license->license == 'expired' ) {
											
												print '<span class="dashicons dashicons-no-alt dashicons-fail dashicons-large"></span>';
												print '&nbsp;&nbsp;<span class="small-text">( Expired on ' . date( 'F d, Y', strtotime( $check_license->expires ) ) . ' )</span>';
											}
											
											if ( $check_license->license == NULL && $status !== false ) {
												
												print '<span class="dashicons dashicons-no-alt dashicons-fail dashicons-large"></span>';
												print '&nbsp;&nbsp;<span class="small-text">( Cannot retrieve license status from Divi Life server. Please contact Divi Life support. )</span>';
											}
										}
									?>
								</h3>
								<br>
								
								<?php if ( $status !== false && $check_license->license == 'valid' ) { ?>
									<?php wp_nonce_field( 'divilife_edd_divibars_nonce', 'divilife_edd_divibars_nonce' ); ?>
									<input type="submit" class="button-secondary" name="divilife_edd_divibars_license_deactivate" value="<?php _e('Deactivate License'); ?>"/>
								<?php } ?>
							</td>
						</tr>
						
						<?php
						
						if ( $status !== false ) { 
							
							if ( $daysleft != '' && $check_license->license == 'valid' ) { ?>
							<tr valign="top">
								<th scope="row" valign="top">
									<?php _e('License Expires on'); ?>
								</th>
								<td>
									<h4 class="no-margin">
										<?php print date( 'F d, Y', strtotime( $check_license->expires ) ); ?>
										<?php print $daysleft; ?>
									</h4>
								</td>
							<?php
							}
						}
						?>
						
					<?php } ?>
				</tbody>
			</table>
			<?php submit_button(); ?>

		</form>
	<?php
		
		}
		
	} // end DiviBars_Admin_Controller