<?php

	class DiviBars_Controller extends DiviBars {
		
		protected static $_show_errors = FALSE;
		
		/**
		 * @var \WP_Filesystem_Base|null
		 */
		public static $wpfs;
		
		private static $slug = 'DiviBars-divi-custom-styles';
		
		private static $post_id;
		
		private static $filename;
		
		private static $file_extension;
		
		private static $cache_dir;
		
		public function __construct() {
			
		}
		
		public static function _init() {
			
			// Disable "Static CSS File Generation" temporarily in case it's activated
			et_update_option( 'et_pb_static_css_file', 'off' );
			
			if ( null !== self::$post_id ) {
				// Class has already been initialized
				return;
			}
			
			$divi_styles_manager = DiviBars::$helper->getDiviStylesManager();
			
			if ( ! $divi_styles_manager->forced_inline ) {
				
				global $wp_filesystem;
				self::$wpfs = $wp_filesystem;
				
				$custom_divi_css = file_get_contents( $divi_styles_manager->URL );
				
				// Remove #page-container from Divi Cached Inline Styles tag and cloning it to prevent issues
				$custom_divi_css = str_replace( '#page-container ', '', $custom_divi_css );
				
				self::$post_id = $divi_styles_manager->post_id;
				self::$filename = 'et-custom-divibars-' . self::$post_id;
				self::$file_extension = '.min.css';
				self::$cache_dir = ET_Core_PageResource::get_cache_directory();
				
				$files = glob( self::$cache_dir . '/' . self::$post_id . '/' . self::$filename . '-' . '[0-9]*' . self::$file_extension, GLOB_BRACE);
				
				if ( $files ) {
					
					$relative_path = '/' . self::$post_id . '/' . basename( $files[0] );
					
				} else {
					
					$relative_path = self::createResourceFile( $custom_divi_css );
				}
				
				$relative_divi_path  = ET_Core_PageResource::get_cache_directory( 'relative' );
				$relative_divi_path .= $relative_path;

				$start = strpos( $relative_divi_path, 'cache/et' );
				$first_parse = substr( $relative_divi_path, $start );
				
				$start = strpos( $relative_divi_path, 'et-cache' );
				$second_parse = substr( $relative_divi_path, $start );
				
				$url = content_url( $second_parse );
				
				printf(
					'<link rel="stylesheet" id="%1$s" href="%2$s" />', // phpcs:ignore WordPress.WP.EnqueuedResources.NonEnqueuedStylesheet
					esc_attr( self::$slug ),
					esc_url( set_url_scheme( $url ) )
				);
			}
		}
		
		
		private static function createResourceFile( $data ) {
			
			// Static resource file doesn't exist
			$time = (string) microtime( true );
			$time = str_replace( '.', '', $time );
			
			$relative_path = '/' . self::$post_id . '/' . self::$filename . '-' . $time . self::$file_extension;
			
			$file = self::$cache_dir . $relative_path;
			
			if ( is_writable( self::$cache_dir ) ) {
				
				self::$wpfs->put_contents( $file, $data, 0644 );
			}
			
			return $relative_path;
		}
		
		
		public static function showDiviBars( $params ) {
			
			self::$helper = new DiviBars_Helper;
			
			try {
				
				if ( isset( $_GET['et_fb'] ) ) {
					
					$divi_builder_enabled = htmlspecialchars( $_GET['et_fb'] );
					
					// is divi theme builder ?
					if ( $divi_builder_enabled === '1' ) {
						
						return;
					}
				}
				
				print '<div id="sidebar-divibar">';
				
				
				/* Add Global Settings */
				print '<script type="text/javascript">var divibars_settings = {';
				
				$divibars_settings = get_option( 'dib_settings' );
				if ( isset( $divibars_settings['dib_custom_elems'] ) ) {
					
					print '\'dib_custom_elems\': \'' . $divibars_settings['dib_custom_elems'] . '\',';
				}
				
				print '};</script>';
				
				
				/* Search CSS Triggers in all Divi divibars */
				$divibars_with_css_trigger = array();
				
				$dwct_posts = DiviBars::$divibarsList['divibars_with_css_trigger'];
				if ( !empty( $dwct_posts ) ) {
					
					print '<script type="text/javascript">var divibars_with_css_trigger = {';
					
					foreach( $dwct_posts as $post_id => $css_selector ) {
						
						print '\'' . $post_id . '\': \'' . $css_selector . '\',';
					}
					
					print '};</script>';
				}
				
				
				/* Search Divi divibars with Custom Close Buttons */
				$dccb_posts = DiviBars_Model::getDivibars('customizeclosebtn');
				if ( !empty( $dccb_posts ) ) {
					
					print '<style type="text/css">';
					
					foreach( $dccb_posts as $dv_post ) {
						
						$post_id = $dv_post->ID;
						
						$cbc_textcolor = get_post_meta( $post_id, 'post_closebtn_text_color', true );
						$cbc_bgcolor = get_post_meta( $post_id, 'post_closebtn_bg_color', true );
						$cbc_fontsize = get_post_meta( $post_id, 'post_closebtn_fontsize', true );
						$cbc_borderradius = get_post_meta( $post_id, 'post_closebtn_borderradius', true );
						$cbc_padding = get_post_meta( $post_id, 'post_closebtn_padding', true );
						
						$customizeclosebtn = get_post_meta( $post_id, 'post_do_customizeclosebtn' );
						if ( isset( $customizeclosebtn[0] ) ) {
							
							$customizeclosebtn = $customizeclosebtn[0];
							
						} else {
							
							continue;
						}
						
						if ( $customizeclosebtn ) {
							
							print '
							.divibar-customclose-btn-' . $post_id . ' {
								top:5px !important;
								color:' . $cbc_textcolor . ' !important;
								background-color:' . $cbc_bgcolor . ' !important;
								font-size:' . $cbc_fontsize . 'px !important;
								padding:' . $cbc_padding . 'px !important;
								-moz-border-radius:' . $cbc_borderradius . '% !important;
								-webkit-border-radius:' . $cbc_borderradius . '% !important;
								-khtml-border-radius:' . $cbc_borderradius . '% !important;
								border-radius:' . $cbc_borderradius . '% !important;
							}
							';
						}
					}
					
					print '</style>';
				}
				
				
				/* Search Automatic Triggers in all Divi divibars */
				$dwat_posts = DiviBars::$divibarsList['divibars_with_automatic_trigger'];
				if ( !empty( $dwat_posts ) ) {
					
					print '<script type="text/javascript">var divibars_with_automatic_trigger = {';
					
					foreach( $dwat_posts as $post_id => $at_settings ) {
						
						print '\'' . $post_id . '\': \'' . $at_settings . '\',';
					}
					
					print '};</script>';
				}
				
				
				$divibars = DiviBars::$divibarsList['ids'];
				if ( is_array( $divibars ) && count( $divibars ) > 0 ) {
					
					global $post;
					
					$display_in_current = false;
					
					if ( function_exists( 'get_queried_object_id' ) && get_queried_object_id() > 0 ) {
						
						$current_post_id = get_queried_object_id();
					
					} else {
					
						$current_home_post_id = (int) get_option( 'page_on_front' );
						
						$is_home = is_home();
						
						if ( $current_home_post_id == 0 && !$is_home ) {
							
							$current_post_id = get_the_ID();
						}
					}
					
					foreach( $divibars as $divibar_id => $idx ) {
						
						if ( get_post_status ( $divibar_id ) == 'publish' ) {
						
							$at_pages = get_post_meta( $divibar_id, 'at_pages' );
							
							$display_in_posts = ( !isset( $at_pages[0] ) ) ? 'all' : $at_pages[0];
							
							if ( $display_in_posts == 'specific' ) {
								
								$display_in_current = false;
								
								$in_posts = get_post_meta( $divibar_id, 'at_pages_selected' );
								
								if ( isset( $in_posts[0] ) && is_array( $in_posts[0] ) ) {
								
									foreach( $in_posts[0] as $in_post => $the_id ) {
										
										if ( $the_id == $current_post_id ) {
											
											$display_in_current = true;
											
											break;
										}
									}
								}
							}
							
							if ( $display_in_posts == 'all' ) {
								
								$display_in_current = true;
								
								$except_in_posts = get_post_meta( $divibar_id, 'at_pagesexception_selected' );
								
								if ( isset( $except_in_posts[0] ) ) {
									
									foreach( $except_in_posts[0] as $in_post => $the_id ) {
										
										if ( $the_id == $current_post_id ) {
											
											$display_in_current = false;
											
											break;
										}
									}
								}
							}
							
							if ( $display_in_current ) {
							
								print self::divibar_render( $divibar_id );
							}
						}
					}
				}
				
				print '</div>';
				
				?>
				<script type="text/javascript">
				var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
				var diviAjaxUrl = '<?php print plugins_url( 'ajax-handler-wp.php' , __FILE__ ); ?>';
				</script>
				<?php
			
			} catch (Exception $e) {
			
				DiviBars::log( $e );
			}
			
			wp_register_script('exit-intent', DIVI_BARS_PLUGIN_URL . 'assets/js/jquery.exitintent.js', array('jquery') );
			
			wp_enqueue_script('exit-intent');
		}
		
		public static function divibar_render( $divibar_id = NULL ) {
			
			if ( !is_numeric( $divibar_id ) ) {
				
				throw new InvalidArgumentException( 'divibar_render > $divibar_id is not numeric');
			}
			
			$divibar_id = (int) $divibar_id;
			
			$post_data = get_post( $divibar_id );
			
			$post_content = $post_data->post_content;
			
			
			/* Scheduling */
			$enable_scheduling = get_post_meta( $post_data->ID, 'divibars_enable_scheduling' );
			
			if( !isset( $enable_scheduling[0] ) ) {
				
				$enable_scheduling[0] = 0;
			}
			
			$enable_scheduling = (int) $enable_scheduling[0];
			
			if ( $enable_scheduling ) {
				
				$timezone = DIVI_SERVER_TIMEZONE;
				
				$divibars_settings = get_option( 'dib_settings' );
				
				$wp_timezone = DiviBars::$helper->getWPTimeZone();
				
				if ( $wp_timezone !== false ) {
					
					$timezone = $wp_timezone;
				}
				
				$timezone = new DateTimeZone( $timezone );
				
				$date_now = new DateTime( strtotime( time() ), $timezone );
				
				// Start & End Time
				if ( $enable_scheduling == 1 ) {
					
					$date_start = get_post_meta( $post_data->ID, 'divibars_date_start', true );
					$date_end = get_post_meta( $post_data->ID, 'divibars_date_end', true );
					
					$date_start = DiviBars::$helper->convertDateToUserTimezone( $date_start );
					$date_start = new DateTime( $date_start, $timezone );

					if ( $date_start >= $date_now ) {
						
						return;
					}
					
					if ( $date_end != '' ) {
					
						$date_end = DiviBars::$helper->convertDateToUserTimezone( $date_end );
						$date_end = new DateTime( $date_end, $timezone );
						
						if ( $date_end <= $date_now ) {
							
							return;
						}
					}
				}
				
				// Recurring Scheduling
				if ( $enable_scheduling == 2 ) {
					
					$wNum = $date_now->format( 'N' );
					
					$is_today = get_post_meta( $post_data->ID, 'divibars_scheduling_daysofweek_' . $wNum );
					
					if ( isset( $is_today[0] ) && $is_today[0] == 1 ) {
						
						$is_today = $is_today[0];
						
						$time_start = get_post_meta( $post_data->ID, 'divibars_time_start', true );
						$time_end = get_post_meta( $post_data->ID, 'divibars_time_end', true );
						$schedule_start = null;
						$schedule_end = null;
						
						if ( $time_start != '' ) {
							
							$time_start_24 = date( 'H:i', strtotime( $time_start ) );
							$time_start_24 = explode( ':', $time_start_24 );
							$time_start_now = new DateTime( 'now', $timezone );
							$schedule_start = $time_start_now->setTime( $time_start_24[0], $time_start_24[1], 0 );
						}
						
						if ( $time_end != '' ) {
							
							$time_end_24 = date( 'H:i', strtotime( $time_end ) );
							$time_end_24 = explode( ':', $time_end_24 );
							$time_end_now = new DateTime( 'now', $timezone );
							$schedule_end = $time_end_now->setTime( $time_end_24[0], $time_end_24[1], 0 );
						}
						
						if ( ( $time_start != '' && $time_end != '' && $schedule_start >= $date_now && $schedule_end > $date_now )
							|| ( $time_start != '' && $time_end != '' && $schedule_start <= $date_now && $schedule_end < $date_now )
							|| ( $time_start != '' && $time_end == '' && $schedule_start <= $date_now )
							|| ( $time_start == '' && $time_end != '' && $schedule_end < $date_now )
							) {
							
							return;
						}
						
					} else {
				
						return;
					}
				}
			}
			
			
			$divibar_effect = get_post_meta( $post_data->ID,'_et_pb_divibar_effect', true );
			
			if ( $divibar_effect == '' ) {
				
				$divibar_effect = 'divibar-hugeinc';
			}
			
			$output = apply_filters( 'the_content', $post_content );
			
			// Monarch fix: Remove Divi Builder main section class and add it later with JS
			$output = str_replace( 'et_pb_section ', 'divibars_dbars_section ', $output );
			
			// Restore "Static CSS File Generation"
			et_update_option( 'et_pb_static_css_file', 'on' );
			
			$bgcolor = get_post_meta( $post_data->ID, 'post_divibars_bg_color', true );
			$fontcolor = get_post_meta( $post_data->ID, 'post_divibars_font_color', true );
			
			$hideclosebtn = get_post_meta( $post_data->ID, 'post_do_hideclosebtn' );
			if ( isset( $hideclosebtn[0] ) ) {
				
				$hideclosebtn = $hideclosebtn[0];
				
			} else {
				
				$hideclosebtn = 0;
			}
			
			$placement = get_post_meta( $post_data->ID, 'dib_post_placement', true );
			if( !isset( $placement ) ) {
				
				$placement = 'top';
			}
			
			$pushpage = get_post_meta( $post_data->ID, 'post_pushpage' );
			if ( isset( $pushpage[0] ) ) {
				
				$pushpage = $pushpage[0];
				
			} else {
				
				$pushpage = 1;
			}
			
			$screenfixed = 0;
			
			if ( $placement != 'bottom' ) {
				
				$screenfixed = get_post_meta( $post_data->ID, 'post_screenfixed' );
				if ( isset( $screenfixed[0] ) ) {
					
					$screenfixed = $screenfixed[0];
					
				} else {
					
					$screenfixed = 1;
				}
			}
			
			$data_path_to = null;
			$svg = null;
			
			if ( $divibar_effect == 'divibar-cornershape' ) {
				
				$data_path_to = 'data-path-to = "m 0,0 1439.999975,0 0,805.99999 -1439.999975,0 z"';
				$svg = '<svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 1440 806" preserveAspectRatio="none">
						<path class="divibar-path" d="m 0,0 1439.999975,0 0,805.99999 0,-805.99999 z"/>
					</svg>';
			}
			if ( $divibar_effect == 'divibar-boxes' ) {
				
				$svg = '<svg xmlns="http://www.w3.org/2000/svg" width="100%" height="101%" viewBox="0 0 1440 806" preserveAspectRatio="none">
						<path d="m0.005959,200.364029l207.551124,0l0,204.342453l-207.551124,0l0,-204.342453z"/>
						<path d="m0.005959,400.45401l207.551124,0l0,204.342499l-207.551124,0l0,-204.342499z"/>
						<path d="m0.005959,600.544067l207.551124,0l0,204.342468l-207.551124,0l0,-204.342468z"/>
						<path d="m205.752151,-0.36l207.551163,0l0,204.342437l-207.551163,0l0,-204.342437z"/>
						<path d="m204.744629,200.364029l207.551147,0l0,204.342453l-207.551147,0l0,-204.342453z"/>
						<path d="m204.744629,400.45401l207.551147,0l0,204.342499l-207.551147,0l0,-204.342499z"/>
						<path d="m204.744629,600.544067l207.551147,0l0,204.342468l-207.551147,0l0,-204.342468z"/>
						<path d="m410.416046,-0.36l207.551117,0l0,204.342437l-207.551117,0l0,-204.342437z"/>
						<path d="m410.416046,200.364029l207.551117,0l0,204.342453l-207.551117,0l0,-204.342453z"/>
						<path d="m410.416046,400.45401l207.551117,0l0,204.342499l-207.551117,0l0,-204.342499z"/>
						<path d="m410.416046,600.544067l207.551117,0l0,204.342468l-207.551117,0l0,-204.342468z"/>
						<path d="m616.087402,-0.36l207.551086,0l0,204.342437l-207.551086,0l0,-204.342437z"/>
						<path d="m616.087402,200.364029l207.551086,0l0,204.342453l-207.551086,0l0,-204.342453z"/>
						<path d="m616.087402,400.45401l207.551086,0l0,204.342499l-207.551086,0l0,-204.342499z"/>
						<path d="m616.087402,600.544067l207.551086,0l0,204.342468l-207.551086,0l0,-204.342468z"/>
						<path d="m821.748718,-0.36l207.550964,0l0,204.342437l-207.550964,0l0,-204.342437z"/>
						<path d="m821.748718,200.364029l207.550964,0l0,204.342453l-207.550964,0l0,-204.342453z"/>
						<path d="m821.748718,400.45401l207.550964,0l0,204.342499l-207.550964,0l0,-204.342499z"/>
						<path d="m821.748718,600.544067l207.550964,0l0,204.342468l-207.550964,0l0,-204.342468z"/>
						<path d="m1027.203979,-0.36l207.550903,0l0,204.342437l-207.550903,0l0,-204.342437z"/>
						<path d="m1027.203979,200.364029l207.550903,0l0,204.342453l-207.550903,0l0,-204.342453z"/>
						<path d="m1027.203979,400.45401l207.550903,0l0,204.342499l-207.550903,0l0,-204.342499z"/>
						<path d="m1027.203979,600.544067l207.550903,0l0,204.342468l-207.550903,0l0,-204.342468z"/>
						<path d="m1232.659302,-0.36l207.551147,0l0,204.342437l-207.551147,0l0,-204.342437z"/>
						<path d="m1232.659302,200.364029l207.551147,0l0,204.342453l-207.551147,0l0,-204.342453z"/>
						<path d="m1232.659302,400.45401l207.551147,0l0,204.342499l-207.551147,0l0,-204.342499z"/>
						<path d="m1232.659302,600.544067l207.551147,0l0,204.342468l-207.551147,0l0,-204.342468z"/>
						<path d="m-0.791443,-0.360001l207.551163,0l0,204.342438l-207.551163,0l0,-204.342438z"/>
					</svg>';
			}
			
			if ( $divibar_effect == 'divibar-genie' ) {
				
				$data_path_to = 'data-steps = "m 701.56545,809.01175 35.16718,0 0,19.68384 -35.16718,0 z;m 698.9986,728.03569 41.23353,0 -3.41953,77.8735 -34.98557,0 z;m 687.08153,513.78234 53.1506,0 C 738.0505,683.9161 737.86917,503.34193 737.27015,806 l -35.90067,0 c -7.82727,-276.34892 -2.06916,-72.79261 -14.28795,-292.21766 z;m 403.87105,257.94772 566.31246,2.93091 C 923.38284,513.78233 738.73561,372.23931 737.27015,806 l -35.90067,0 C 701.32034,404.49318 455.17312,480.07689 403.87105,257.94772 z;M 51.871052,165.94772 1362.1835,168.87863 C 1171.3828,653.78233 738.73561,372.23931 737.27015,806 l -35.90067,0 C 701.32034,404.49318 31.173122,513.78234 51.871052,165.94772 z;m 52,26 1364,4 c -12.8007,666.9037 -273.2644,483.78234 -322.7299,776 l -633.90062,0 C 359.32034,432.49318 -6.6979288,733.83462 52,26 z;m 0,0 1439.999975,0 0,805.99999 -1439.999975,0 z"';
				$svg = '<svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 1440 806" preserveAspectRatio="none">
						<path class="divibar-path" d="m 701.56545,809.01175 35.16718,0 0,19.68384 -35.16718,0 z"/>
					</svg>';
			}
			
			$customizeclosebtn = get_post_meta( $post_data->ID, 'post_do_customizeclosebtn' );
			if( !isset( $customizeclosebtn[0] ) ) {
				
				$customizeclosebtn[0] = '0';
			}
			
			$close_cookie = get_post_meta( $post_data->ID, 'post_closebtn_cookie', true );
			if( !isset( $close_cookie ) ) {
				
				$close_cookie = 1;
			}
			
			$is_mobile = DiviBars::$isMobileDevice;
			$is_tablet = DiviBars::$isTabletDevice;
			
			if ( !$is_mobile ) {
				
				$is_mobile = 0;
			}
			
			if ( !$is_tablet ) {
				
				$is_tablet = 0;
			}
			
			// Monarch fix: Remove Divi Builder main section class and add it later with JS
			$output = str_replace( 'et_pb_section ', '', $output );
			
			$body = $output;
			
			require( DIVI_BARS_PLUGIN_DIR . '/templates/divibar.php');
		}
		
	} // end DiviBars_Controller