<?php

	$divibuilder = false;
	
	// is divi theme builder ?
	if ( is_singular() && 'on' === get_post_meta( $post_data->ID, '_et_pb_use_builder', true ) ) {
		$divibuilder = true;
	}
?>
<div id="divi-divibars-container-<?php print $divibar_id;?>" class="divibars-container<?php if ( !$divibuilder ) { print ' divibar-nob'; } ?>">
	<div id="divibar-<?php print $post_data->ID;?>" class="divibars <?php print $divibar_effect;?>" <?php print $data_path_to;?> 
	data-bgcolor="<?php print $bgcolor;?>" data-fontcolor="<?php print $fontcolor;?>" data-scrolltop="" 
	data-placement="<?php print $placement ?>" data-pushpage="<?php print $pushpage ?>" data-screenfixed="<?php print $screenfixed ?>"
	data-cookie="<?php print $close_cookie ?>" data-ismobile="<?php print $is_mobile ?>" data-istablet="<?php print $is_tablet ?>">
		<div class="divibars-content-inner">
			<div class="divibars-body">
				<?php print $body; ?>
			</div>
		</div>
		<div class="divibars-close-container">
			<?php if ( $hideclosebtn == 0 ) { ?>
			<button type="button" class="divibars-close divibar-customclose-btn-<?php print $divibar_id ?>">
				<span class="<?php if ( $customizeclosebtn[0] == 1 ) { ?>custom_btn<?php } ?>">&times;</span>
			</button>
			<?php } ?>
		</div>
	</div>
</div>