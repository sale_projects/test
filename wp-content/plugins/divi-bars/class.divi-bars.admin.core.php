<?php

	class DiviBars_Admin {
		
		private static $_show_errors = FALSE;
		private static $initiated = FALSE;
		private static $helper_admin = NULL;
		
		public static $helper = NULL;
		
		/**
		 * Holds the values to be used in the fields callbacks
		 */
		public static $options;
		
		public static function init() {
			
			if ( ! self::$initiated ) {
				
				self::load_resources();
				
				self::init_hooks();
			}
		}
		
		
		private static function init_hooks() {
			
			self::$initiated = true;
			
			self::$helper = new DiviBars_Helper();
			
			self::$helper_admin = new DiviBars_Admin_Helper();
			
			// Admin styles/scripts
			add_action( 'admin_init', array( 'DiviBars_Admin', 'register_assets' ) );
			add_action( 'admin_enqueue_scripts', array( 'DiviBars_Admin', 'include_assets'), '999');
			
			// Add meta boxes
			add_action( 'add_meta_boxes', array( 'DiviBars_Admin_Controller', 'add_meta_boxes') );
			
			// Add Divi Theme Builder
			add_filter( 'et_builder_post_types', array( 'DiviBars_Admin', 'enable_divi_builder') );
			
			// Add Search Filter Post Title
			add_filter( 'posts_where', array( 'DiviBars_Admin', 'post_title_like_where' ), 10, 2 );
			
			// Save post fields
			add_action( 'save_post', array( 'DiviBars_Admin_Controller', 'save_post' ), 10, 2 );
			
			add_action( 'admin_menu', array( 'DiviBars_Admin_Controller', 'add_admin_submenu' ), 5 );
			
			add_action( 'wp_ajax_nopriv_ajax_dib_listposts', array( 'DiviBars_Admin_Controller', 'get_wp_posts' ) );
			add_action( 'wp_ajax_ajax_dib_listposts', array( 'DiviBars_Admin_Controller', 'get_wp_posts' ) );
			
			// Register settings
			add_action( 'admin_init', array( 'DiviBars_Admin', 'register_divibars_settings' ) );
		}
		
		
		protected static function load_resources() {
			
			require_once( DIVI_BARS_PLUGIN_DIR . '/includes/class.divi-bars.admin.controller.php' );
			require_once( DIVI_BARS_PLUGIN_DIR . '/includes/class.divi-bars.admin.helper.php' );
			require_once( DIVI_BARS_PLUGIN_DIR . '/includes/class.divi-bars.admin.model.php' );
			require_once( DIVI_BARS_PLUGIN_DIR . '/includes/class.divi-bars.admin.ajax.php' );
			require_once( DIVI_BARS_PLUGIN_DIR . '/includes/class.divi-bars.helper.php' );
		}
		
		
		public static function register_assets( $hook ) {
			
			wp_register_style( 'divi-bars-wp-color-picker', DIVI_BARS_PLUGIN_URL . 'assets/css/admin/cs-wp-color-picker.min.css', array( 'wp-color-picker' ), '1.0.0', 'all' );
			wp_register_script( 'divi-bars-wp-color-picker', DIVI_BARS_PLUGIN_URL . 'assets/js/admin/cs-wp-color-picker.min.js', array( 'wp-color-picker' ), '1.0.0', true );
			
			wp_register_style( 'divi-bars-select2', DIVI_BARS_PLUGIN_URL . 'assets/css/admin/select2.4.0.9.min.css', array(), '4.0.9', 'all' );
			wp_register_script( 'divi-bars-select2', DIVI_BARS_PLUGIN_URL . 'assets/js/admin/select2.4.0.9.min.js', array('jquery'), '4.0.9', true );
			wp_register_style( 'divi-bars-select2-bootstrap', DIVI_BARS_PLUGIN_URL . 'assets/css/admin/select2-bootstrap.min.css', array('divi-bars-admin-bootstrap'), '1.0.0', 'all' );
			
			/* Scheduling requirements */
			wp_register_script( 'divi-bars-datetime-moment', '//cdn.jsdelivr.net/momentjs/latest/moment.min.js', array('jquery'), '1.0.0', true );
			wp_register_script( 'divi-bars-datetime-moment-timezone', '//cdn.jsdelivr.net/npm/moment-timezone@0.5.13/builds/moment-timezone-with-data.min.js', array('jquery'), '1.0.0', true );
			wp_register_style( 'divi-bars-admin-bootstrap', DIVI_BARS_PLUGIN_URL . 'assets/css/admin/bootstrap.css', array(), '1.0.0', 'all' );
			wp_register_script( 'divi-bars-datetime-bootstrap', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js', array('jquery'), '1.0.0', true );
			wp_register_script( 'divi-bars-datetime-bootstrap-select', '//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/js/bootstrap-select.min.js', array('jquery'), '1.0.0', true );
			wp_register_style( 'divi-bars-admin-bootstrap-select', '//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/css/bootstrap-select.min.css', array(), '1.0.0', 'all' );
			
			/* Include Date Range Picker */
			wp_register_style( 'divi-bars-datetime-corecss', DIVI_BARS_PLUGIN_URL . 'assets/css/admin/bootstrap-datetimepicker.min.css', array( 'divi-bars-admin-bootstrap' ), '1.0.0', 'all' );
			wp_register_script( 'divi-bars-datetime-corejs', DIVI_BARS_PLUGIN_URL . 'assets/js/admin/bootstrap-datetimepicker.min.js', array( 'jquery', 'divi-bars-datetime-bootstrap' ), '1.0.0', true );
			
			wp_register_style( 'divi-bars-admin', DIVI_BARS_PLUGIN_URL . 'assets/css/admin/admin.css', array(), '1.0.0', 'all' );
			wp_register_script( 'divi-bars-admin-functions', DIVI_BARS_PLUGIN_URL . 'assets/js/admin/admin-functions.js', array( 'jquery', 'divi-bars-select2' ), '1.0.0', true );
		}
		
		
		public static function include_assets( $hook ) {
			
			$screen = get_current_screen();
			
			if ( $screen->post_type != 'divi_bars' ) {
				return;
			}
			
			wp_enqueue_style( 'wp-color-picker' );
			wp_enqueue_style( 'divi-bars-wp-color-picker' );
			wp_enqueue_script( 'wp-color-picker' );
			wp_enqueue_script( 'divi-bars-wp-color-picker' );
			
			wp_enqueue_script( 'divi-bars-datetime-moment' );
			wp_enqueue_script( 'divi-bars-datetime-moment-timezone' );
			wp_enqueue_style( 'divi-bars-admin-bootstrap' );
			wp_enqueue_script( 'divi-bars-datetime-bootstrap' );
			wp_enqueue_script( 'divi-bars-datetime-bootstrap-select' );
			wp_enqueue_style( 'divi-bars-admin-bootstrap-select' );
			
			wp_enqueue_style( 'divi-bars-select2' );
			wp_enqueue_style( 'divi-bars-select2-bootstrap' );
			wp_enqueue_script( 'divi-bars-select2' );
			
			wp_enqueue_style( 'divi-bars-datetime-corecss' );
			wp_enqueue_script( 'divi-bars-datetime-corejs' );
			
			wp_enqueue_style( 'divi-bars-admin' );
			wp_enqueue_script( 'divi-bars-admin-functions' );
		}
		
		
		public static function enable_divi_builder( $post_types ) {
			
			$post_types[] = 'divi_bars';
			
			return $post_types;
		}
		
		
		public static function post_title_like_where( $where, $wp_query ) {
			
			global $wpdb;
			
			if ( $post_title_like = $wp_query->get( 'post_title_like' ) ) {
				
				$where .= ' AND ' . $wpdb->posts . '.post_title LIKE \'%' . esc_sql( $wpdb->esc_like( trim( $post_title_like ) ) ) . '%\'';
			}
			
			return $where;
		}
		
		
		public static function register_divibars_settings( $args ) {
			
			register_setting( 
				'divibars_settings', 
				'dib_settings', 
				array( 'DiviBars_Admin', 'sanitize' ) 
			);
			
			add_settings_section(
				'dib_settings_description',
				'Settings',
				array( 'DiviBars_Admin', 'print_description_settings' ),
				'divibars-settings'
			);
			
			
			$options = array( 
				'type' => 'text',
				'name' => 'dib_custom_elems'
			);
			
			add_settings_field(
				'dib_custom_elems', 
				'Custom Elements CSS Selector', 
				array( 'DiviBars_Admin', 'parse_fields_callback' ), 
				'divibars-settings', 
				'dib_settings_description',
				$options
			);
		}
		
		
		public static function print_description_settings() {
			
			print '';
		}
		
		/**
		 * Sanitize each setting field as needed
		 *
		 * @param array $input Contains all settings fields as array keys
		 */
		public static function sanitize( $input ) {
			
			$new_input = array();
			
			if ( isset( $input['dib_custom_elems'] ) ) {
				
				$new_input['dib_custom_elems'] = sanitize_text_field( $input['dib_custom_elems'] );
			}
			
			if ( isset( $input['dib_sharedhostingready'] ) ) {
				
				$new_input['dib_sharedhostingready'] = sanitize_text_field( $input['dib_sharedhostingready'] );
			}
			
			return $new_input;
		}
		
		public static function parse_fields_callback( $options ) {
			
			$field_type = isset( $options['type'] ) ? $options['type'] : '';
			
			$field_name = $optionname = isset( $options['name'] ) ? $options['name'] : '';
			
			$field_default_value = isset( $options['default_value'] ) ? $options['default_value'] : '';
			
			if ( 'text' == $field_type ) {
				
				printf(
					'<input type="text" id="' . $field_name . '" name="dib_settings[' . $field_name . ']" value="%s" />',
					isset( self::$options[ $field_name ] ) ? esc_attr( self::$options[ $field_name ] ) : $field_default_value
				);
			}
			else if ( 'select' == $field_type ) {
				
				$valid_options = array();
				
				$selected = isset( self::$options[ $field_name ] ) ? esc_attr( self::$options[ $field_name ] ) : $field_default_value;
				
				if ( $selected != $field_default_value ) {
					
					$field_default_value = $selected;
				}
				
				?>
				<select name="dib_settings[<?php print $field_name; ?>]" data-defaultvalue="<?php print $field_default_value ?>" class="select-<?php print $options['name'] ?>">
				<?php
				
				if ( isset( $options['options'] ) ) {
				
					foreach ( $options['options'] as $option ) {
						
						?>
						<option <?php selected( $selected, $option['value'] ); ?> value="<?php print $option['value']; ?>"><?php print $option['title']; ?></option>
						<?php
					}
				}
				
				?>
				</select>
				<?php
			}
			else if ( 'checkbox' == $field_type ) {
				
				$value = isset( self::$options[ $field_name ] ) ? esc_attr( self::$options[ $field_name ] ) : $field_default_value;
				
				$checked = checked( $value, 1, false );
				
				printf(
					'<input type="checkbox" id="' . $field_name . '" name="dib_settings[' . $field_name . ']" %s value="1">',
					$checked
				);
			}
		}
		
	} // end DiviBars_Controller