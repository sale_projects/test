function _getAllValuesDiviBarsCustomClose() {
	var options = {};
	options['color'] = jQuery('.closebtn-text-color').val();
	options['backgroundColor'] = jQuery('.closebtn-bg-color').val();
	options['fontsize'] = parseFloat( jQuery('.post_closebtn_fontsize').val() );
	options['borderRadius'] = parseFloat( jQuery('.post_closebtn_borderradius').val() );
	options['padding'] = parseFloat( jQuery('.post_closebtn_padding').val() );
	
	return options;
};

jQuery(document).ready(function( $ ) {
    
	DiviBarsCustomClose = new DiviBarsCustomClose( _getAllValuesDiviBarsCustomClose() );
	DiviBarsCustomClose.update();
	
	
	$('.closebtn-text-color').wpColorPicker({
		clear: function() {

			DiviBarsCustomClose.color = '';
			DiviBarsCustomClose.update();
		},
		change: function(event, ui) {
			
			var hexcolor = jQuery( this ).wpColorPicker( 'color' );
			
			DiviBarsCustomClose.color = hexcolor;
			DiviBarsCustomClose.update();
		}
	});
	
	
	$('.closebtn-bg-color').wpColorPicker({
		clear: function() {

			DiviBarsCustomClose.backgroundColor = '';
			DiviBarsCustomClose.update();
		},
		change: function(event, ui) {
			
			var hexcolor = jQuery( this ).wpColorPicker( 'color' );
			
			DiviBarsCustomClose.backgroundColor = hexcolor;
			DiviBarsCustomClose.update();
		}
	});
	
	
	/* Close Button cookie */
	$('#slider-closebtn-cookie').slider({
		value: 0,
		min: 0,
		max: 99,
		step: 1,
		slide: function(event, ui) {
			var val = dib_getFromField(ui.value, 0, 99);
			
			$('.post_closebtn_cookie').val(val);
		}
	});
	
	
	/* Close Button font size */
	$('#slider-closebtn-fontsize').slider({
		value: 25,
		min: 25,
		max: 250,
		step: 1,
		slide: function(event, ui) {
			var val = dib_getFromField(ui.value, 25, 250);
			DiviBarsCustomClose.fontsize = val;
			DiviBarsCustomClose.update();
			
			$('.post_closebtn_fontsize').val(val);
		}
	});
	
	var default_val = $('.post_closebtn_fontsize').val();
	$('#slider-closebtn-fontsize').slider('value', default_val);
	
	
	/* Close Button border radius */
	$('#slider-closebtn-borderradius').slider({
		value: 0,
		min: 0,
		max: 50,
		step: 1,
		slide: function(event, ui) {
			var val = dib_getFromField(ui.value, 0, 50);
			DiviBarsCustomClose.borderRadius = val;
			DiviBarsCustomClose.update();
			
			$('.post_closebtn_borderradius').val(val);
		}
	});
	
	var default_val = $('.post_closebtn_borderradius').val();
	$('#slider-closebtn-borderradius').slider('value', default_val);
	
	
	/* Close Button padding */
	$('#slider-closebtn-padding').slider({
		value: 0,
		min: 0,
		max: 99,
		step: 1,
		slide: function(event, ui) {
			var val = dib_getFromField(ui.value, 0, 99);
			DiviBarsCustomClose.padding = val;
			DiviBarsCustomClose.update();
			
			$('.post_closebtn_padding').val(val);
		}
	});
	
	default_val = $('.post_closebtn_padding').val();
	$('#slider-closebtn-padding').slider('value', default_val);
});

function formatPostResults ( post ) {
	
	var post_title = formatPostTitle( post );
	
	if ( post.loading ) {
		return post.text;
	}
	
    if ( typeof post_title === 'undefined' ) {
		post_title = 'Post without Title';
    }

	var markup = "<div class='select2-result-post clearfix'>" +
	"<div class='select2-result-post__meta'>" +
	  "<div class='select2-result-post__title'>" + post.id + " : " + post_title + "</div>";

	markup += "</div></div>";
	
	if ( post.id == 0 ) {
		
		markup = post_title;
	}

	return markup;
}

function formatPostTitle (post) {
	return post.post_title || post.text;
}

jQuery( function ( $ ) {
	
	if ( $('#divibars_moresettings_meta_box2').length ) {
	
		$(".chosen").select2({
			width: '100%',
			theme: "classic",
			minimumResultsForSearch: Infinity
		});
		
		
		$(".do-list-pages").select2({
			dropdownParent: $('#divibars_displaylocations_meta_box1'),
			width: '100%',
			theme: "bootstrap",
			ajax: {
				url: ajaxurl,
				dataType: 'json',
				delay: 250,
				method: 'POST',
				data: function (params) {
				  return {
					action: 'ajax_dib_listposts',
					q: params.term,
					page: params.page,
					json: 1
				  };
				},
				processResults: function (data, params) {
				  params.page = params.page || 1;
				  
				  return {
					results: data.items,
					pagination: {
					  more: (params.page * 7) < data.total_count
					}
				  };
				},
				cache: true
			},
			minimumInputLength: 1,
			escapeMarkup: function (markup) { return markup; },
			templateResult: formatPostResults,
			templateSelection: formatPostTitle
		});
		
		
		$('body').on('change','.post_divibar_placement', function(event){
			
			var placement = $(this).val();
			
			if ( placement == 'top' ) {
				
				$('.moresettings_screenfixed').addClass('do-show').removeClass('do-hide');
			}
			else {
				
				$('.moresettings_screenfixed').removeClass('do-show').addClass('do-hide');
			}
		});
		
		
		$('body').on('click','[data-showhideblock]', function(event){
			
			var block_content = $(this).data('showhideblock');
			
			if ( $(this).is(':checked') ) {
			
				$( block_content ).addClass('do-show');
				
			} else {
				
				$( block_content ).removeClass('do-show');
			}
		});
		
		
		$('body').on('change','.post_divibar_automatictrigger', function(event){
			
			var type_at = $(this).val();
			
			if ( type_at == 'divibar-timed' ) {
				
				$('.divi_automatictrigger_timed').addClass('do-show');
			}
			else {
				
				$('.divi_automatictrigger_timed').removeClass('do-show');
			}
			
			
			if ( type_at == 'divibar-scroll' ) {
				
				$('.divi_automatictrigger_scroll').addClass('do-show');
			}
			else {
				
				$('.divi_automatictrigger_scroll').removeClass('do-show');
			}
		});
		
		
		$('body').on('change','.divibars-filter-by-pages', function(event){
			
			var type_pages = $(this).val();
			
			if ( type_pages == 'specific' ) {
				
				$(this).parent().find('.do-list-pages-container').addClass('do-show');
			}
			else {
				
				$(this).parent().find('.do-list-pages-container').removeClass('do-show');
			}
		});
		
		
		
		$('[data-dropdownshowhideblock]').change(function() {
			
			var dropdown_id = $(this).attr('id')
			, showhideblock = $(this).find(':selected').data('showhideblock');
			
			$(this).find('option[data-showhideblock]').each(function() {
				
				var elemRef = $(this).data('showhideblock');
				
				$( elemRef ).removeClass('do-show');
			});
			
			if ( showhideblock !== undefined ) {
				
				$( showhideblock ).addClass('do-show');
			}
		});
	}
	
	// Scheduling	
    $('input[name="dib_date_start"]').datetimepickerr({
        sideBySide: true,
		showTodayButton: true,
		stepping: 30, // Number of minutes the up/down arrow's will move the minutes value in the time picker
		widgetPositioning: {
			horizontal: 'auto',
			vertical: 'top'
		},
		minDate: moment().subtract(1, 'days'),
		format: 'MM/DD/YYYY h:mm A',
		showClear: true,
		toolbarPlacement: 'bottom',
		defaultDate : moment()
    }, 
    function(start, end, label) {
		
		
	});
	
    $('input[name="dib_date_end"]').datetimepickerr({
		sideBySide: true,
		stepping: 30, // Number of minutes the up/down arrow's will move the minutes value in the time picker
		widgetPositioning: {
			horizontal: 'auto',
			vertical: 'top'
		},
		minDate: moment().subtract(1, 'days'),
		format: 'MM/DD/YYYY h:mm A',
		showClear: true,
		toolbarPlacement: 'bottom',
		defaultDate : moment(),
		useCurrent: false
    }, 
    function(start, end, label) {
		
		
	});
	
	$('input[name="dib_time_start"]').datetimepickerr({
		format: 'LT',
		stepping: 30, // Number of minutes the up/down arrow's will move the minutes value in the time picker
		widgetPositioning: {
			horizontal: 'auto',
			vertical: 'top'
		},
		showClear: true,
		format: 'h:mm A',
		toolbarPlacement: 'bottom'
	}, 
    function(start, end, label) {
		
		
	});
	
	$('input[name="dib_time_end"]').datetimepickerr({
		format: 'LT',
		stepping: 30, // Number of minutes the up/down arrow's will move the minutes value in the time picker
		widgetPositioning: {
			horizontal: 'auto',
			vertical: 'top'
		},
		showClear: true,
		format: 'h:mm A',
		toolbarPlacement: 'bottom'
	}, 
    function(start, end, label) {
		
		
	});
	
	$('input[name="dib_date_start"],input[name="dib_date_end"],input[name="dib_time_start"],input[name="dib_time_end"]').on("dp.change", function (e) {
		
		$('.dib-recurring-user-msg').removeClass('do-show');
	});
	
	$('.post-type-divi_bars form#post').submit(function(e) {
		
		var recurring_msg = $('.dib-recurring-user-msg'), recurring_msg_position;
		
		// Check Start & End Time date values
		if ( $('.divibars-enable-scheduling option:selected').val() == 1 ) {
			
			var dib_date_start = $('input[name="dib_date_start"]'),
			dib_date_start_value = dib_date_start.val(),
			dib_date_end = $('input[name="dib_date_end"]')
			dib_date_end_value = dib_date_end.val(),
			date_start = '',
			date_end = '';
			
			if ( dib_date_start_value != '' && dib_date_end_value != '' ) {
				
				date_start = moment( dib_date_start_value, 'MM/DD/YYYY h:mm A');
				date_end = moment( dib_date_end_value, 'MM/DD/YYYY h:mm A');
				
				if ( date_start.isBefore( date_end ) === false ) {
					
					dib_date_end.focus();
					
					recurring_msg.html('<p><strong>"End date"</strong> field should be greater than <strong>"Start date"</strong> field.</p>').addClass('do-show').removeClass('do-hide');
					
					recurring_msg_position = recurring_msg.offset().top - 100;
					$('html,body').animate({ scrollTop: recurring_msg_position }, 500);
					
					return false;
				}
			}
		}
		
		// Check Recurring Scheduling time values
		if ( $('.divibars-enable-scheduling option:selected').val() == 2 ) {
		
			var dib_time_start = $('input[name="dib_time_start"]')
			dib_time_start_value = dib_time_start.val(),
			dib_time_end = $('input[name="dib_time_end"]'),
			dib_time_end_value = dib_time_end.val(),
			time_start = '',
			time_end = '';
			
			if ( dib_time_start_value != '' && dib_time_end_value != '' ) {
				
				time_start = moment( dib_time_start_value, 'h:mm A');
				time_end = moment( dib_time_end_value, 'h:mm A');
				
				if ( time_start.isBefore( time_end ) === false ) {
					
					dib_time_end.focus();
					
					recurring_msg.html('<p><strong>"End Time"</strong> field should be greater than <strong>"Start Time"</strong> field.</p>').addClass('do-show').removeClass('do-hide');
					
					recurring_msg_position = recurring_msg.offset().top - 100;
					$('html,body').animate({ scrollTop: recurring_msg_position  }, 500);
					
					return false;
				}
			}
		}
	});
});

function DiviBarsCustomClose (options) {
    this.htmlElement = jQuery('.divibars-customclose-btn');
    this.color = options['color'] || '#333333';
	this.backgroundColor = options['backgroundColor'] || '';
	this.fontsize = options['fontsize'] || 25;
	this.borderRadius = options['borderRadius'] || 0;
    this.padding = options['padding'] || 0;
    
};

DiviBarsCustomClose.prototype.update = function () {
	this.htmlElement.css('color', this.color, 'important');
	this.htmlElement.css('background-color', this.backgroundColor, 'important');
	this.htmlElement.css('-moz-border-radius', this.borderRadius + '%', 'important');
	this.htmlElement.css('-webkit-border-radius', this.borderRadius + '%', 'important');
	this.htmlElement.css('-khtml-border-radius', this.borderRadius + '%', 'important');
	this.htmlElement.css('font-size', this.fontsize + 'px', 'important');
	this.htmlElement.css('border-radius', this.borderRadius + '%', 'important');
	this.htmlElement.css('padding', this.padding + 'px', 'important');
};

function dib_getFromField(value, min, max, elem) {
	var val = parseFloat(value);
	if (isNaN(val) || val < min) {
		val = 0;
	} else if (val > max) {
		val = max;
	}

	if (elem)
		elem.val(val);

	return val;
}