<?php
$team_members_categories = wp_get_post_terms( get_the_ID() ,'department_category', array("fields" => "ids"));
$cat_filter = '';
if ( count($team_members_categories ) > 0 ){
foreach ( $team_members_categories as $cat_id ){
		$cat_filter .= 'cat-'.$cat_id.' ';		
}
}

$divi_team_display_detail_page_link = !empty(get_option('divi_team_display_detail_page_link')) ? get_option('divi_team_display_detail_page_link') : 'Yes';

?>
<div class="et_pb_column jv_team-scale-anm all et_pb_column_1_3 et_pb_jv_team_members_column <?php echo rtrim($cat_filter);?> <?php //echo $last_child_ele;?>">
	<div class="et_pb_jv_team jv_team_member_equalheight">
		<div class="et_pb_jv_team_image">
			<?php 
				$jv_team_thumb = wp_get_attachment_image_src(get_post_thumbnail_id(), 'jv_team_grid_image');
					if( $jv_team_thumb[0] != ''){ 
								$image_path = $jv_team_thumb[0] ;
							}else{
								$image_path = JVTEAM_PLUGIN_URL . '/assets/images/default.png';
							} 
				?>
			<img src="<?php echo $image_path;?>" alt="">
		</div>		
		<div class="et_pb_jv_team_description"  id="post-<?php the_ID(); ?>">
			
			    <?php if ( $divi_team_display_detail_page_link == "Yes" ){?><a href="<?php echo get_permalink(get_the_ID()); ?>"><?php }?>
			
					<h3 class="et_pb_jv_team_title"><?php echo get_the_title();?></h3>
			
			    <?php if ( $divi_team_display_detail_page_link == "Yes" ){?> </a> <?php }?>
			
				<?php
				$jv_team_designation = get_post_meta( get_the_ID(),'jv_team_designation', true ); 
				if( $jv_team_designation != ''){ ?>
				<span class="et_pb_position"><?php echo $jv_team_designation;?></span>
				<?php } ?>
			<div class="et_pb_jv_team_content ">
			<?php $content_len= strlen(get_the_content());
                           $divi_team_content_length = !empty(get_option('divi_team_content_length')) ? get_option('divi_team_content_length') : '180';
                           if ($content_len > $divi_team_content_length){
                               echo do_shortcode(wpautop(substr(strip_tags(get_the_content()),0,$divi_team_content_length).'...'));
                           }else{
                               echo do_shortcode(wpautop(get_the_content()));
                           }
                    ?>
			</div>
			<?php 
					$jv_team_facebook = get_post_meta( get_the_ID(),'jv_team_facebook', true );
					$jv_team_twitter = get_post_meta( get_the_ID(),'jv_team_twitter', true );
					$jv_team_google = get_post_meta( get_the_ID(),'jv_team_google', true );
					$jv_team_linkdin = get_post_meta( get_the_ID(),'jv_team_linkedin', true );
					$jv_team_instagram = get_post_meta( get_the_ID(),'jv_team_instagram', true );
				?>
			
						<ul class="et_pb_jv_team_social_link">
								<?php if( $jv_team_facebook != '' ) { ?><li><a href="<?php echo $jv_team_facebook;?>" target="_blank"><i class="et-pb-icon jv_team_member_social_font">&#xe0aa;</i></a></li><?php } ?>
								<?php if( $jv_team_twitter != '' ) { ?><li><a href="<?php echo $jv_team_twitter;?>" target="_blank"><i class="jv_team_member_social_font et-pb-icon">&#xe0ab;</i></a></li><?php } ?>
								<?php if( $jv_team_google != '' ) { ?><li><a href="<?php echo $jv_team_google;?>" target="_blank"><i class="jv_team_member_social_font et-pb-icon">&#xe0ad;</i></a></li><?php } ?>
								<?php if( $jv_team_linkdin != '' ) { ?><li><a href="<?php echo $jv_team_linkdin;?>"target="_blank"><i class="jv_team_member_social_font et-pb-icon">&#xe0b4;</i></a></li><?php } ?>
								<?php if( $jv_team_instagram != '' ) { ?><li><a href="<?php echo $jv_team_instagram;?>"target="_blank"><i class="jv_team_member_social_font et-pb-icon">&#xe0b1;</i></a></li><?php } ?>
						</ul>
				
		</div>
	</div>	
</div>