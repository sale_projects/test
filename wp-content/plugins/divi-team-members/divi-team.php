<?php
/*
   Plugin Name: Divi Team Members
   Plugin URI: http://divi-professional.com
   Description: To showcase your staff/employees/people on your website the easy way. You can quickly add members to your team(s), add their picture, position, bios, social links. What you see is what you get.
   Version: 1.4.1
   Author: Divi Professional
   Author URI: http://divi-professional.com
   License: GPL2
   Text Domain: jvteam
*/
require_once( dirname(__FILE__) . '/include/divi-team-members-license.php' );
if (  class_exists( 'DiviTeamMembers_License_Plugin' ) ) {
	/**
	 * @param string $file             Must be __FILE__ from the root plugin file, or theme functions file.
	 * @param string $software_title   Must be exactly the same as the Software Title in the product.
	 * @param string $software_version This product's current software version.
	 * @param string $plugin_or_theme  'plugin' or 'theme'
	 * @param string $api_url          The URL to the site that is running the API Manager. Example: https://www.toddlahman.com/
	 *
	 * @return \AM_License_Submenu|null
	 */
	DiviTeamMembers_License_Plugin::instance( __FILE__, 'Divi Team Members', '1.4.1', 'plugin', 'http://divi-professional.com' );
}
define( 'JVTEAM_PLUGIN_URL', plugin_dir_url( __FILE__) );
define( 'JVTEAM_PLUGIN_PATH',plugin_dir_path( __FILE__ ) );
define( 'JVTEAM_PLUGIN_BASENAME',plugin_basename( __FILE__ ) );
add_action( 'plugins_loaded','jvteam_plugin_load_function' );
function jvteam_plugin_load_function()
{
	require_once( 'jv-team-post-type.php' );
	require_once( 'divi-team-module.php' );
	require_once( 'divi-team-grid-module.php' );
	require_once( 'divi-team-list-module.php' );
	require_once( 'divi-team-horizontal-module.php' );
	require_once( 'team-members-module-slider.php' );
	require_once( 'team-members-module-table.php' );
	
	add_action('et_builder_ready', 'jv_team_module',11);
	add_action('et_builder_ready', 'jv_team_grid_modules',10); 
	add_action('et_builder_ready', 'jv_team_list_modules',9);
	add_action('et_builder_ready', 'jv_team_members_horizontal_module',11);
	add_action('et_builder_ready', 'team_members_sliders_modules',12);
	add_action('et_builder_ready', 'team_members_table_modules',13);
	add_action('wp_enqueue_scripts', 'jv_team_enqueue', 9999);
	add_action('admin_enqueue_scripts', 'jv_team_enqueue_admin', 9999);
}
// Single Page redirect Tempalte hook
add_filter( 'single_template', 'jv_team_single_template' );
function jv_team_single_template($single_template) {
	 global $post;
	 if ($post->post_type == 'jv_team_members') {
		 $single_template = JVTEAM_PLUGIN_PATH. 'single-jv_team_members.php';  
	 }
		return $single_template;
}
// jv_team_single_template
function jv_team_enqueue_admin() {
		wp_enqueue_style('admin_jv_team_module', JVTEAM_PLUGIN_URL .'assets/css/admin_jv_team_module.css', array(), NULL);			
}
function jv_team_enqueue() {
		wp_enqueue_style('jv_team_single', JVTEAM_PLUGIN_URL .'assets/css/single-jv-team.css', array(), NULL);
		if(et_core_is_fb_enabled()) { 
			wp_enqueue_style('jv_team_module_list_members', JVTEAM_PLUGIN_URL .'assets/css/archive_team_members_list_css/archive_team_members_list.css', array(), NULL);
			for ( $i=1;$i<=60;$i++){
				wp_enqueue_style('jv_team_module_member_style'.$i, JVTEAM_PLUGIN_URL .'assets/css/archive_team_members_grid_css/jv_team_module_member_style'.$i.'.css', array(), NULL);
			}
			for ( $i=1;$i<=9;$i++){
				wp_enqueue_style('archive_team_members_list_cssstyle'.$i, JVTEAM_PLUGIN_URL .'assets/css/archive_team_members_list_css/jv_team_module_list_style'.$i.'.css', array(), NULL);
			}
			wp_enqueue_style('content_archive_team_members_slider-css', JVTEAM_PLUGIN_URL . '/assets/css/archive_team_members_sliders_css/swiper.min.css', array(), NULL);
			wp_enqueue_script('content_archive_team_members_slider-js', JVTEAM_PLUGIN_URL . '/assets/js/swiper.min.js', array('jquery'), NULL, TRUE );
			wp_enqueue_style('jv_team_members_slider', JVTEAM_PLUGIN_URL . '/assets/css/archive_team_members_sliders_css/jv_team_members_slider.css', array(), NULL);
			wp_enqueue_style('jv_team_horizontal', JVTEAM_PLUGIN_URL .'assets/css/archive_team_members_horizontal_css/jv_team_horizontal.css', array(), NULL);
			wp_enqueue_style('content-team-table', JVTEAM_PLUGIN_URL . '/assets/css/archive_team_members_table_css/content-team-table.css', array(), NULL);
			wp_enqueue_script('front_swiper_slider-js', JVTEAM_PLUGIN_URL . '/assets/js/front_swiper_slider.js', array('jquery'), NULL, TRUE );
		}
}
register_activation_hook(__FILE__,'jv_team_plugin_enabled');
function jv_team_plugin_enabled() {	}
register_uninstall_hook(__FILE__,'jv_team_plugin_uninstall');
function jv_team_plugin_uninstall() {}	
add_action( 'init', 'jv_team_image_resize' );
function jv_team_image_resize() { 
	add_image_size( 'jv_team_image_250_330',250,330, array( 'top', 'center' ) ); 
    add_image_size( 'jv_team_list_image',300,300, array( 'left', 'top' ) ); 
    add_image_size( 'jv_team_grid_image',300,400, array( 'top', 'center' ) ); 
    add_image_size( 'jv_team_horizontal_image', 600,600, array( 'left', 'top' ) ); 
   // add_image_size( 'jv_team_grid_style60', 300,400, array( 'top', 'center' ) ); 
}
/*Archive Team Memebers*/
add_action( 'admin_menu', 'jvteam_setting_admin_menu');
function jvteam_setting_admin_menu() {
		add_submenu_page( 'edit.php?post_type=jv_team_members', __( 'Team Members Settings', 'jvteam' ), __( 'Team Members Settings', 'jvteam' ), 'manage_options', 'jv-team-members',  'jv_team_members'  );
}
function jv_team_members(){
	if(is_admin()){
		 wp_enqueue_style( 'wp-color-picker' );
		 wp_enqueue_script( 'team_details_page_js', JVTEAM_PLUGIN_URL .'assets/js/team_details_page.js', array( 'jquery', 'wp-color-picker' ), '', true  );
	 }
$is_settings_updated = false;
$nonce   = "divi_team_nonce"; 
if ( isset( $_POST[ $nonce ] ) ) {
	$is_settings_updated         = true;
	$is_settings_updated_success = false;
	// Verify nonce and user permission
	if ( wp_verify_nonce( $_POST[ $nonce ], $nonce ) && current_user_can( 'switch_themes' ) ) {
		$active_tab_new = isset( $_GET[ 'tab' ] ) ? $_GET[ 'tab' ] : 'general_options';
		if( $active_tab_new == 'general_options' ) {
				$divi_team_content_length= $_POST['divi_team_content_length'];
				$divi_team_display_htmlcontent= $_POST['divi_team_display_htmlcontent'];
				update_option( 'divi_team_content_length', $divi_team_content_length );
				update_option( 'divi_team_display_htmlcontent', $divi_team_display_htmlcontent );
		}
		if( $active_tab_new == 'archive_options' ) {
			$divi_team_listing_page_slug	= $_POST['divi_team_listing_page_slug'];
			$divi_team_archive_page_layout  = $_POST['divi_team_archive_page_layout'];			
			$divi_team_per_page= $_POST['divi_team_per_page'];
			$divi_team_display_pagination= $_POST['divi_team_display_pagination'];
			$divi_team_display_filter= $_POST['divi_team_display_filter'];
			$divi_team_display_detail_page_link= $_POST['divi_team_display_detail_page_link'];
			
			/*update*/
			update_option( 'divi_team_listing_page_slug', $divi_team_listing_page_slug );
			update_option( 'divi_team_archive_page_layout', $divi_team_archive_page_layout );			
			update_option( 'divi_team_per_page', $divi_team_per_page );
			update_option( 'divi_team_display_pagination', $divi_team_display_pagination );
			update_option( 'divi_team_display_filter', $divi_team_display_filter );
			update_option( 'divi_team_display_detail_page_link', $divi_team_display_detail_page_link );
			
		}
		if( $active_tab_new == 'team_deatail_options' ) {
			$divi_team_details_page_layout  = $_POST['divi_team_details_page_layout'];
			$divi_team_select_sidebar= $_POST['divi_team_select_sidebar'];
			$divi_team_select_style_layout= $_POST['divi_team_select_style_layout'];
			$divi_team_title_color= $_POST['divi_team_title_color'];
			$divi_team_position_color= $_POST['divi_team_position_color'];
			$divi_team_content_color= $_POST['divi_team_content_color'];
			$divi_team_background_color= $_POST['divi_team_background_color'];
			$divi_team_border_width= $_POST['divi_team_border_width'];
			$divi_team_border_color= $_POST['divi_team_border_color'];
			$divi_team_boxshadow_color= $_POST['divi_team_boxshadow_color'];
			$divi_team_icon_color= $_POST['divi_team_icon_color'];
			$divi_team_horizontal_color= $_POST['divi_team_horizontal_color'];
			$divi_team_information_color= $_POST['divi_team_information_color'];
			$divi_team_contact_us_title= $_POST['divi_team_contact_us_title'];
			
			update_option( 'divi_team_select_sidebar', $divi_team_select_sidebar );
			update_option( 'divi_team_details_page_layout', $divi_team_details_page_layout );
			update_option( 'divi_team_select_style_layout', $divi_team_select_style_layout );
			update_option( 'divi_team_title_color', $divi_team_title_color );
			update_option( 'divi_team_position_color', $divi_team_position_color );
			update_option( 'divi_team_content_color', $divi_team_content_color );
			update_option( 'divi_team_background_color', $divi_team_background_color );
			update_option( 'divi_team_border_width', $divi_team_border_width );
			update_option( 'divi_team_border_color', $divi_team_border_color );
			update_option( 'divi_team_boxshadow_color', $divi_team_boxshadow_color );
			update_option( 'divi_team_icon_color', $divi_team_icon_color );
			update_option( 'divi_team_horizontal_color', $divi_team_horizontal_color );
			update_option( 'divi_team_information_color', $divi_team_information_color );
			update_option( 'divi_team_contact_us_title', $divi_team_contact_us_title );
			
		}
		
		if( $active_tab_new == 'popup_options' ) {
			$divi_popupteam_select_style_layout= $_POST['divi_popupteam_select_style_layout'];
			$divi_popupteam_title_color= $_POST['divi_popupteam_title_color'];
			$divi_popupteam_position_color= $_POST['divi_popupteam_position_color'];
			$divi_popupteam_content_color= $_POST['divi_popupteam_content_color'];
			$divi_popupteam_background_color= $_POST['divi_popupteam_background_color'];
			$divi_popupteam_border_width= $_POST['divi_popupteam_border_width'];
			$divi_popupteam_border_color= $_POST['divi_popupteam_border_color'];
			$divi_popupteam_boxshadow_color= $_POST['divi_popupteam_boxshadow_color'];
			$divi_popupteam_icon_color= $_POST['divi_popupteam_icon_color'];
			$divi_popupteam_horizontal_color= $_POST['divi_popupteam_horizontal_color'];
			$divi_popupteam_information_color= $_POST['divi_popupteam_information_color'];
			$divi_popupteam_contact_us_title= $_POST['divi_popupteam_contact_us_title'];
			$divi_team_close_background_color= $_POST['divi_team_close_background_color'];
			$divi_team_close_color= $_POST['divi_team_close_color'];
			
			
			update_option( 'divi_popupteam_select_style_layout', $divi_popupteam_select_style_layout );
			update_option( 'divi_popupteam_title_color', $divi_popupteam_title_color );
			update_option( 'divi_popupteam_position_color', $divi_popupteam_position_color );
			update_option( 'divi_popupteam_content_color', $divi_popupteam_content_color );
			update_option( 'divi_popupteam_background_color', $divi_popupteam_background_color );
			update_option( 'divi_popupteam_border_width', $divi_popupteam_border_width );
			update_option( 'divi_popupteam_border_color', $divi_popupteam_border_color );
			update_option( 'divi_popupteam_boxshadow_color', $divi_popupteam_boxshadow_color );
			update_option( 'divi_popupteam_icon_color', $divi_popupteam_icon_color );
			update_option( 'divi_popupteam_horizontal_color', $divi_popupteam_horizontal_color );
			update_option( 'divi_popupteam_information_color', $divi_popupteam_information_color );
			update_option( 'divi_popupteam_contact_us_title', $divi_popupteam_contact_us_title );
			update_option( 'divi_team_close_background_color', $divi_team_close_background_color );
			update_option( 'divi_team_close_color', $divi_team_close_color );
			
		}
		
			$is_settings_updated_message = __( 'Options data has been updated.','jvteam' );
			$is_settings_updated_success = true;
	} else {
			$is_settings_updated_message = __( 'Error authenticating request. Please try again.','jvteam' );
	}
}
?>
<div class="wrap">
		<?php if ( $is_settings_updated ) { ?>
			<div id="setting-error-settings_updated" class="updated settings-error notice is-dismissible <?php echo $is_settings_updated_success ? '' : 'error' ?>" style="margin: 0 0 25px 0;">
				<p>
					<strong><?php echo esc_html( $is_settings_updated_message ); ?></strong>
				</p>
				<button type="button" class="notice-dismiss">
					<span class="screen-reader-text"><?php _e( 'Dismiss this notice.' ,'jvteam'); ?></span>
				</button>
			</div>
		<?php }
			$active_tab = isset( $_GET[ 'tab' ] ) ? $_GET[ 'tab' ] : 'general_options';
		?>
		<h2 class="nav-tab-wrapper">
			<a href="?post_type=jv_team_members&page=jv-team-members&tab=general_options" class="nav-tab <?php echo $active_tab == 'general_options' ? 'nav-tab-active' : ''; ?>">General Setting</a>
			<a href="?post_type=jv_team_members&page=jv-team-members&tab=archive_options" class="nav-tab <?php echo $active_tab == 'archive_options' ? 'nav-tab-active' : ''; ?>">Archive Page Setting</a>
			<a href="?post_type=jv_team_members&page=jv-team-members&tab=team_deatail_options" class="nav-tab <?php echo $active_tab == 'team_deatail_options' ? 'nav-tab-active' : ''; ?>">Team Detail Page Setting</a>
			<a href="?post_type=jv_team_members&page=jv-team-members&tab=popup_options" class="nav-tab <?php echo $active_tab == 'popup_options' ? 'nav-tab-active' : ''; ?>">Popup Options</a>
		</h2>
            <form action="" method="POST" class="jvteam-form">
			<?php if( $active_tab == 'general_options' ) {?>
			<table class="form-table">
				<h2><?php echo __('General Setting', 'jvteam'); ?></h2>
			    <hr>
				<tr>
                    <th><?php echo __('Display Content Length', 'jvteam'); ?></th>
                    <td><?php $divi_team_content_length = !empty(get_option('divi_team_content_length')) ? get_option('divi_team_content_length') : '180'; ?>
					<input type="text" name="divi_team_content_length" value="<?php echo $divi_team_content_length;?>"/></td>
                </tr>
				<tr>
                    <th><?php echo __('Allow to access HTML Tag on Content [frontend]', 'jvteam'); ?></th>
                    <td><?php $divi_team_display_htmlcontent = !empty(get_option('divi_team_display_htmlcontent')) ? get_option('divi_team_display_htmlcontent') : 'No'; ?>
					  <select name="divi_team_display_htmlcontent">
							 <option value="No" <?php selected( $divi_team_display_htmlcontent, "No" ); ?>>No</option>
							  <option value="Yes" <?php selected( $divi_team_display_htmlcontent,"Yes" ); ?>>Yes</option>
						</select>
					 </td>
                </tr>
			</table>
		<?php } ?>
		<?php if( $active_tab == 'archive_options' ) {?>	
				<h2><?php echo __('Archive Page Settings', 'jvteam'); ?></h2>
			    <hr>
            <table class="form-table">
			    <tr>
                    <th><?php echo __('Team Members Archive Page Slug', 'jvteam'); ?></th>
                    <td><?php $divi_team_listing_page_slug = !empty(get_option('divi_team_listing_page_slug')) ? get_option('divi_team_listing_page_slug') : 'teams'; ?>
					<input type="text" name="divi_team_listing_page_slug" value="<?php echo $divi_team_listing_page_slug;?>"/></td>
                </tr>
				<tr>
                    <th><?php echo __('Archive Page Layout', 'jvteam'); ?></th>
                    <td><?php $divi_team_archive_page_layout = !empty(get_option('divi_team_archive_page_layout')) ? get_option('divi_team_archive_page_layout') : 'Grid'; ?>
					<select name="divi_team_archive_page_layout">
							 <option value="Grid" <?php selected( $divi_team_archive_page_layout,"Grid" ); ?>>Grid</option>
							 <option value="List" <?php selected( $divi_team_archive_page_layout, "List" ); ?>>List</option>
					</select>
					</td>
                </tr>
				<tr>
                    <th><?php echo __('Team Members Per Page', 'jvteam'); ?></th>
                    <td><?php $divi_team_per_page = !empty(get_option('divi_team_per_page')) ? get_option('divi_team_per_page') : '3'; ?>
					 <input type="text" name="divi_team_per_page" value="<?php echo $divi_team_per_page;?>"/>
					 </td>
                </tr>
				<tr>
                    <th><?php echo __('Display Filter', 'jvteam'); ?></th>
                    <td><?php $divi_team_display_filter = !empty(get_option('divi_team_display_filter')) ? get_option('divi_team_display_filter') : 'Yes'; ?>
					  <select name="divi_team_display_filter">
							 <option value="Yes" <?php selected( $divi_team_display_filter,"Yes" ); ?>>Yes</option>
							 <option value="No" <?php selected( $divi_team_display_filter, "No" ); ?>>No</option>
						</select>
					 </td>
                </tr>
				<tr>
                    <th><?php echo __('Display Pagination', 'jvteam'); ?></th>
                    <td><?php $divi_team_display_pagination = !empty(get_option('divi_team_display_pagination')) ? get_option('divi_team_display_pagination') : 'Yes'; ?>
					<select name="divi_team_display_pagination">
						 <option value="Yes" <?php selected( $divi_team_display_pagination,"Yes" ); ?>>Yes</option>
						 <option value="No" <?php selected( $divi_team_display_pagination, "No" ); ?>>No</option>
					</select>
					 </td>
                </tr>
				<tr>
                    <th><?php echo __('Display Detail Page Link', 'jvteam'); ?></th>
                    <td><?php $divi_team_display_detail_page_link = !empty(get_option('divi_team_display_detail_page_link')) ? get_option('divi_team_display_detail_page_link') : 'Yes'; ?>
					 <select name="divi_team_display_detail_page_link">
							 <option value="Yes" <?php selected( $divi_team_display_detail_page_link,"Yes" ); ?>>Yes</option>
							<option value="No" <?php selected( $divi_team_display_detail_page_link, "No" ); ?>>No</option>
					 </select>
					 </td>
                </tr>
				</table>
		<?php } ?>
		<?php if( $active_tab == 'team_deatail_options' ) {?>
			<table class="form-table">
				<h2><?php echo __('Team Detail Page Setting', 'jvteam'); ?></h2>
			    <hr>				
				<tr>
                    <th><?php echo __('Details Layout', 'jvteam'); ?></th>
                    <td><?php $divi_team_details_page_layout = !empty(get_option('divi_team_details_page_layout')) ? get_option('divi_team_details_page_layout') : 'Fullwidth'; ?>
					<select name="divi_team_details_page_layout">
								 <option value="Right" <?php selected( $divi_team_details_page_layout,"Right" ); ?>>Right Sidebar</option>
								 <option value="Left" <?php selected( $divi_team_details_page_layout, "Left" ); ?>>Left Sidebar</option>
								  <option value="Fullwidth" <?php selected( $divi_team_details_page_layout, "Fullwidth" ); ?>>Fullwidth Sidebar</option>
					</select>
					</td>
                </tr>
				<tr>
                    <th><?php echo __('Select Sidebar', 'jvteam'); ?></th>
                    <td><?php $divi_team_select_sidebar = !empty(get_option('divi_team_select_sidebar')) ? get_option('divi_team_select_sidebar') : 'Sidebar-1'; ?>
					<select name="divi_team_select_sidebar">
					<?php foreach ( $GLOBALS['wp_registered_sidebars'] as $sidebar ) { ?>
						 <option value="<?php echo ucwords( $sidebar['id'] ); ?>" <?php selected( $divi_team_select_sidebar, ucwords( $sidebar['id'] ) ); ?>>
								  <?php echo ucwords( $sidebar['name'] ); ?>
						 </option>
					<?php } ?>
					</select>
					</td>
                </tr>
				<tr>
                    <th><?php echo __('Select Style', 'jvteam'); ?></th>
                    <td><?php $divi_team_select_style_layout = !empty(get_option('divi_team_select_style_layout')) ? get_option('divi_team_select_style_layout') : 'style1'; ?>
					<select name="divi_team_select_style_layout">
						 <option value="style1" <?php selected( $divi_team_select_style_layout,"style1" ); ?>>Style 1</option>
						 <option value="style2" <?php selected( $divi_team_select_style_layout, "style2" ); ?>>Style 2</option>
						 <option value="style3" <?php selected( $divi_team_select_style_layout, "style3" ); ?>>Style 3</option>
						 <option value="style4" <?php selected( $divi_team_select_style_layout,"style4" ); ?>>Style 4</option>
						 <option value="style5" <?php selected( $divi_team_select_style_layout, "style5" ); ?>>Style 5</option>
						 <option value="style6" <?php selected( $divi_team_select_style_layout, "style6" ); ?>>Style 6</option>
					</select>
					</td>
                </tr>
				<tr>
                    <th><?php echo __('Title Color', 'jvteam'); ?></th>
                    <td><?php $divi_team_title_color = !empty(get_option('divi_team_title_color')) ? get_option('divi_team_title_color') : ''; ?>
						<input type="text" name="divi_team_title_color" value="<?php echo $divi_team_title_color;?>" class="jvteam-color-picker"/>
					 </td>
                </tr>
				<tr>
                    <th><?php echo __('Position Color', 'jvteam'); ?></th>
                    <td><?php $divi_team_position_color = !empty(get_option('divi_team_position_color')) ? get_option('divi_team_position_color') : ''; ?>
						<input type="text" name="divi_team_position_color" value="<?php echo $divi_team_position_color;?>" class="jvteam-color-picker"/>
					 </td>
                </tr>
				<tr>
                    <th><?php echo __('Content Color', 'jvteam'); ?></th>
                    <td><?php $divi_team_content_color = !empty(get_option('divi_team_content_color')) ? get_option('divi_team_content_color') : ''; ?>
						<input type="text" name="divi_team_content_color" value="<?php echo $divi_team_content_color;?>" class="jvteam-color-picker"/>
					 </td>
                </tr>
				<tr>
                    <th><?php echo __('Information Color', 'jvteam'); ?></th>
                    <td><?php $divi_team_information_color = !empty(get_option('divi_team_information_color')) ? get_option('divi_team_information_color') : ''; ?>
						<input type="text" name="divi_team_information_color" value="<?php echo $divi_team_information_color;?>" class="jvteam-color-picker"/>
					 </td>
                </tr>
				<tr>
                    <th><?php echo __('Background Color', 'jvteam'); ?></th>
                    <td><?php $divi_team_background_color = !empty(get_option('divi_team_background_color')) ? get_option('divi_team_background_color') : ''; ?>
						<input type="text" name="divi_team_background_color" value="<?php echo $divi_team_background_color;?>" class="jvteam-color-picker"/>
					 </td>
                </tr>
				<tr>
                    <th><?php echo __('Border Width', 'jvteam'); ?></th>
                    <td><?php $divi_team_border_width = !empty(get_option('divi_team_border_width')) ? get_option('divi_team_border_width') : '1'; ?>
					<select name="divi_team_border_width">
					<?php 
						for( $i=1; $i < 11; $i++ ) { ?>  
							 <option value="<?php echo $i; ?>" <?php selected( $divi_team_border_width, $i ); ?>><?php echo $i; ?></option>
							 
				<?php }	 ?>
					</select>
					</td>
                </tr>
				<tr>
                    <th><?php echo __('Border Color', 'jvteam'); ?></th>
                    <td><?php $divi_team_border_color = !empty(get_option('divi_team_border_color')) ? get_option('divi_team_border_color') : ''; ?>
						<input type="text" name="divi_team_border_color" value="<?php echo $divi_team_border_color;?>" class="jvteam-color-picker"/>
					 </td>
                </tr>
				<tr>
                    <th><?php echo __('Box Shadow Color', 'jvteam'); ?></th>
                    <td><?php $divi_team_boxshadow_color = !empty(get_option('divi_team_boxshadow_color')) ? get_option('divi_team_boxshadow_color') : ''; ?>
						<input type="text" name="divi_team_boxshadow_color" value="<?php echo $divi_team_boxshadow_color;?>" class="jvteam-color-picker"/>
					 </td>
                </tr>
				<tr>
                    <th><?php echo __('Icon Color( information,social )', 'jvteam'); ?></th>
                    <td><?php $divi_team_icon_color = !empty(get_option('divi_team_icon_color')) ? get_option('divi_team_icon_color') : ''; ?>
						<input type="text" name="divi_team_icon_color" value="<?php echo $divi_team_icon_color;?>" class="jvteam-color-picker"/>
					 </td>
                </tr>
				<tr>
                    <th><?php echo __('Horizontal Line Color', 'jvteam'); ?></th>
                    <td><?php $divi_team_horizontal_color = !empty(get_option('divi_team_horizontal_color')) ? get_option('divi_team_horizontal_color') : ''; ?>
						<input type="text" name="divi_team_horizontal_color" value="<?php echo $divi_team_horizontal_color;?>" class="jvteam-color-picker"/>
					 </td>
                </tr>
				<tr>
                    <th><?php echo __('Conatct Us Title', 'jvteam'); ?></th>
                    <td><?php $divi_team_contact_us_title = !empty(get_option('divi_team_contact_us_title')) ? get_option('divi_team_contact_us_title') : 'Drop Me a Line'; ?>
					<input type="text" name="divi_team_contact_us_title" value="<?php echo $divi_team_contact_us_title;?>"/></td>
                </tr>
			</table>
		<?php } ?>	
		
		<?php if( $active_tab == 'popup_options' ) {?>
			<table class="form-table">
				<h2><?php echo __('Popup Options', 'jvteam'); ?></h2>
			    <hr>				
				<tr>
                    <th><?php echo __('Select Style', 'jvteam'); ?></th>
                    <td><?php $divi_popupteam_select_style_layout = !empty(get_option('divi_popupteam_select_style_layout')) ? get_option('divi_popupteam_select_style_layout') : 'style1'; ?>
					<select name="divi_popupteam_select_style_layout">
						 <option value="style1" <?php selected( $divi_popupteam_select_style_layout,"style1" ); ?>>Style 1</option>
						 <option value="style2" <?php selected( $divi_popupteam_select_style_layout, "style2" ); ?>>Style 2</option>
					</select>
					</td>
                </tr>
				<tr>
                    <th><?php echo __('Close Icon Background Color', 'jvteam'); ?></th>
                    <td><?php $divi_team_close_background_color = !empty(get_option('divi_team_close_background_color')) ? get_option('divi_team_close_background_color') : ''; ?>
						<input type="text" name="divi_team_close_background_color" value="<?php echo $divi_team_close_background_color;?>" class="jvteam-color-picker"/>
					 </td>
                </tr>
				<tr>
                    <th><?php echo __('Close Icon Color', 'jvteam'); ?></th>
                    <td><?php $divi_team_close_color = !empty(get_option('divi_team_close_color')) ? get_option('divi_team_close_color') : ''; ?>
						<input type="text" name="divi_team_close_color" value="<?php echo $divi_team_close_color;?>" class="jvteam-color-picker"/>
					 </td>
                </tr>
				<tr>
                    <th><?php echo __('Title Color', 'jvteam'); ?></th>
                    <td><?php $divi_popupteam_title_color = !empty(get_option('divi_popupteam_title_color')) ? get_option('divi_popupteam_title_color') : ''; ?>
						<input type="text" name="divi_popupteam_title_color" value="<?php echo $divi_popupteam_title_color;?>" class="jvteam-color-picker"/>
					 </td>
                </tr>
				<tr>
                    <th><?php echo __('Position Color', 'jvteam'); ?></th>
                    <td><?php $divi_popupteam_position_color = !empty(get_option('divi_popupteam_position_color')) ? get_option('divi_popupteam_position_color') : ''; ?>
						<input type="text" name="divi_popupteam_position_color" value="<?php echo $divi_popupteam_position_color;?>" class="jvteam-color-picker"/>
					 </td>
                </tr>
				<tr>
                    <th><?php echo __('Content Color', 'jvteam'); ?></th>
                    <td><?php $divi_popupteam_content_color = !empty(get_option('divi_popupteam_content_color')) ? get_option('divi_popupteam_content_color') : ''; ?>
						<input type="text" name="divi_popupteam_content_color" value="<?php echo $divi_popupteam_content_color;?>" class="jvteam-color-picker"/>
					 </td>
                </tr>
				<tr>
                    <th><?php echo __('Information Color', 'jvteam'); ?></th>
                    <td><?php $divi_popupteam_information_color = !empty(get_option('divi_popupteam_information_color')) ? get_option('divi_popupteam_information_color') : ''; ?>
						<input type="text" name="divi_popupteam_information_color" value="<?php echo $divi_popupteam_information_color;?>" class="jvteam-color-picker"/>
					 </td>
                </tr>
				<tr>
                    <th><?php echo __('Background Color', 'jvteam'); ?></th>
                    <td><?php $divi_popupteam_background_color = !empty(get_option('divi_popupteam_background_color')) ? get_option('divi_popupteam_background_color') : ''; ?>
						<input type="text" name="divi_popupteam_background_color" value="<?php echo $divi_popupteam_background_color;?>" class="jvteam-color-picker"/>
					 </td>
                </tr>
				<tr>
                    <th><?php echo __('Border Width', 'jvteam'); ?></th>
                    <td><?php $divi_popupteam_border_width = !empty(get_option('divi_popupteam_border_width')) ? get_option('divi_popupteam_border_width') : '1'; ?>
					<select name="divi_popupteam_border_width">
					<?php 
						for( $i=1; $i < 11; $i++ ) { ?>  
							 <option value="<?php echo $i; ?>" <?php selected( $divi_popupteam_border_width, $i ); ?>><?php echo $i; ?></option>
							 
				<?php }	 ?>
					</select>
					</td>
                </tr>
				<tr>
                    <th><?php echo __('Border Color', 'jvteam'); ?></th>
                    <td><?php $divi_popupteam_border_color = !empty(get_option('divi_popupteam_border_color')) ? get_option('divi_popupteam_border_color') : ''; ?>
						<input type="text" name="divi_popupteam_border_color" value="<?php echo $divi_popupteam_border_color;?>" class="jvteam-color-picker"/>
					 </td>
                </tr>
				<tr>
                    <th><?php echo __('Box Shadow Color', 'jvteam'); ?></th>
                    <td><?php $divi_popupteam_boxshadow_color = !empty(get_option('divi_popupteam_boxshadow_color')) ? get_option('divi_popupteam_boxshadow_color') : ''; ?>
						<input type="text" name="divi_popupteam_boxshadow_color" value="<?php echo $divi_popupteam_boxshadow_color;?>" class="jvteam-color-picker"/>
					 </td>
                </tr>
				<tr>
                    <th><?php echo __('Icon Color( information,social )', 'jvteam'); ?></th>
                    <td><?php $divi_popupteam_icon_color = !empty(get_option('divi_popupteam_icon_color')) ? get_option('divi_popupteam_icon_color') : ''; ?>
						<input type="text" name="divi_popupteam_icon_color" value="<?php echo $divi_popupteam_icon_color;?>" class="jvteam-color-picker"/>
					 </td>
                </tr>
				<tr>
                    <th><?php echo __('Horizontal Line Color', 'jvteam'); ?></th>
                    <td><?php $divi_popupteam_horizontal_color = !empty(get_option('divi_popupteam_horizontal_color')) ? get_option('divi_popupteam_horizontal_color') : ''; ?>
						<input type="text" name="divi_popupteam_horizontal_color" value="<?php echo $divi_popupteam_horizontal_color;?>" class="jvteam-color-picker"/>
					 </td>
                </tr>
				<tr>
                    <th><?php echo __('Conatct Us Title', 'jvteam'); ?></th>
                    <td><?php $divi_popupteam_contact_us_title = !empty(get_option('divi_popupteam_contact_us_title')) ? get_option('divi_popupteam_contact_us_title') : 'Drop Me a Line'; ?>
					<input type="text" name="divi_popupteam_contact_us_title" value="<?php echo $divi_popupteam_contact_us_title;?>"/></td>
                </tr>
			</table>
		<?php } ?>
		
		<table class="form-table">
		<tr>
                    <td>
                        <button type="submit" class="button-primary"><?php echo __('Save Settings', 'jvteam'); ?></button>
                    </td>
					 <td>&nbsp;</td>
                </tr>
		</table>
                <?php wp_nonce_field( $nonce, $nonce ); ?>
                <input type="text" hidden name="action" value="team_settings">
            </form>
    </div>
<?php
}
/*
CHANGE SLUGS OF CUSTOM POST TYPES
*/
function divi_team_listing_page_slug( $args, $post_type ) {
   $divi_team_listing_page_slug = get_option('divi_team_listing_page_slug','teams');
   //flush_rewrite_rules();
   /*item post type slug*/   
   if ( 'jv_team_members' === $post_type ) {
      $args['rewrite']['slug'] = $divi_team_listing_page_slug;
   }
   return $args;
}
add_filter( 'register_post_type_args', 'divi_team_listing_page_slug', 10, 2 );
function get_jv_team_members_template( $archive_template ) {
     global $post;
     if ( is_post_type_archive ( 'jv_team_members' ) ) {
          $archive_template = plugin_dir_path(__FILE__) . 'archive-jv_team_members.php';
     }
     return $archive_template;
}
add_filter( 'archive_template', 'get_jv_team_members_template' ) ;
function jv_team_members_post_queries( $query ) {
  // do not alter the query on wp-admin pages and only alter it if it's the main query
    if ( $query->is_archive && is_post_type_archive ( 'jv_team_members' )  && (! is_admin() )) {
	  $divi_team_per_page = !empty(get_option('divi_team_per_page')) ? get_option('divi_team_per_page') : '3';
	  $query->set( 'post_type', array('jv_team_members') );
      $query->set('posts_per_page', $divi_team_per_page);
    }
}
add_action( 'pre_get_posts', 'jv_team_members_post_queries',99);
function jv_team_members_class( $classes ) {
	// Maybe add the full width jv_team_members class.
	if ( is_post_type_archive ( 'jv_team_members' ) ){
			$classes[] = 'et_full_width_page';
			unset( $classes[array_search('et_right_sidebar', $classes)] );
			unset( $classes[array_search('et_pb_pagebuilder_layout', $classes)] );
			
	}
	if (  is_singular ( 'jv_team_members' ) ) 
	{
		$divi_team_details_page_layout = !empty(get_option('divi_team_details_page_layout')) ? get_option('divi_team_details_page_layout') : 'Right'; 
		if ( $divi_team_details_page_layout == 'Fullwidth' ) { 
			$classes[] = 'et_full_width_page';
			if (in_array('et_pb_pagebuilder_layout', $classes)) {			
				 unset( $classes[array_search('et_right_sidebar', $classes)] );
				 unset( $classes[array_search('et_pb_pagebuilder_layout', $classes)] );
			}
		}
		if ( $divi_team_details_page_layout == 'Left' ) { 
			$classes[] = ' et_left_sidebar';
			if (in_array('et_pb_pagebuilder_layout', $classes)) {
				 unset( $classes[array_search('et_pb_pagebuilder_layout', $classes)] );
			}
		}
		if ( $divi_team_details_page_layout == 'Right' ) { 
			$classes[] = ' et_right_sidebar';
			if (in_array('et_pb_pagebuilder_layout', $classes)) {
				 unset( $classes[array_search('et_pb_pagebuilder_layout', $classes)] );
			}
		}
		
		$divi_team_select_style_layout = !empty(get_option('divi_team_select_style_layout')) ? get_option('divi_team_select_style_layout') : 'style1';
		if( $divi_team_select_style_layout =='style6'){
			$classes[] = ' body_jv_team_single_style6 ';
		}
	}
	return $classes;
}
add_filter( 'body_class', 'jv_team_members_class' ,99);

function jvteam_custom_css() {
	
	$divi_team_title_color = !empty(get_option('divi_team_title_color')) ? get_option('divi_team_title_color') : ''; 
	$divi_team_position_color = !empty(get_option('divi_team_position_color')) ? get_option('divi_team_position_color') : '';
	$divi_team_content_color = !empty(get_option('divi_team_content_color')) ? get_option('divi_team_content_color') : '';
	$divi_team_background_color = !empty(get_option('divi_team_background_color')) ? get_option('divi_team_background_color') : '';
	$divi_team_border_width = !empty(get_option('divi_team_border_width')) ? get_option('divi_team_border_width') : '1'; 
	$divi_team_border_color = !empty(get_option('divi_team_border_color')) ? get_option('divi_team_border_color') : ''; 
	$divi_team_boxshadow_color = !empty(get_option('divi_team_boxshadow_color')) ? get_option('divi_team_boxshadow_color') : '';
	$divi_team_icon_color = !empty(get_option('divi_team_icon_color')) ? get_option('divi_team_icon_color') : '';
	$divi_team_horizontal_color = !empty(get_option('divi_team_horizontal_color')) ? get_option('divi_team_horizontal_color') : '';
	$divi_team_select_style_layout = !empty(get_option('divi_team_select_style_layout')) ? get_option('divi_team_select_style_layout') : 'style1';
	$divi_team_information_color = !empty(get_option('divi_team_information_color')) ? get_option('divi_team_information_color') : '';
	
	
	 if ( is_singular ( 'jv_team_members' ) )
	 {
		if( $divi_team_select_style_layout == 'style1'){
			  $detail_style1 = '';
			  if ( $divi_team_background_color != '' ){ $detail_style1 .= ".jv_team_single_style1{background-color: ".$divi_team_background_color."!important;}"; }
			  if ( $divi_team_title_color != '' ){  $detail_style1 .= ".jv_team_single_style1 h4.jv_team_list_title,.jv_team_single_style1 .style1_contact_form h2{color: ".$divi_team_title_color." !important;}"; }
			  if ( $divi_team_position_color != '' ){ $detail_style1 .= ".jv_team_single_style1 .jv_team_list_position{color: ".$divi_team_position_color." !important;}"; }
			  if ( $divi_team_content_color != '' ){ $detail_style1 .= ".jv_team_single_style1 .jv_team_list_content,.jv_team_single_style1 .website_url,.jv_team_single_style1 .contect_number,.jv_team_single_style1 .email_address,.jv_team_single_style1 .jv_team_list_address1{color: ".$divi_team_content_color." !important;}"; }
			  if ( $divi_team_border_width != '' || $divi_team_border_color !== ''){ $detail_style1 .= ".jv_team_single_style1{border: ".$divi_team_border_width."px solid ".$divi_team_border_color." !important;}	"; }
			  if ( $divi_team_icon_color != '' ){ $detail_style1 .= ".jv_team_single_style1 .jv_team_member_icon_font.et-pb-icon,.jv_team_single_style1 .jv_team_member_social_font.et-pb-icon{ color: ".$divi_team_icon_color." !important;}"; }
			  if ( $divi_team_information_color != '' ){ $detail_style1 .= ".jv_team_single_style1 .website_url,.jv_team_single_style1 .contect_number,.jv_team_single_style1 .email_address,.jv_team_single_style1 .jv_team_list_address1{color: ".$divi_team_information_color." !important;}"; }
			  if ( $divi_team_boxshadow_color != '' ){ $detail_style1 .= " .jv_team_single_style1{ box-shadow: 0 20px 20px ".$divi_team_boxshadow_color." !important;}"; }
			  if ( $divi_team_horizontal_color != '' ){ $detail_style1 .= " .jv_team_single_style1 .style1_hr_line{ border-color: ".$divi_team_horizontal_color." !important;}"; }
			  
			  echo "<style>".$detail_style1."</style>";
	
		}if( $divi_team_select_style_layout == 'style2'){
			$detail_style2 = '';
			if ( $divi_team_background_color != '' ){ $detail_style2 .= ".jv_team_single_style2{background-color: ".$divi_team_background_color."!important;}"; }
			if ( $divi_team_title_color != '' ){ $detail_style2 .= ".jv_team_single_style2 h2.jv_team_list_title,.jv_team_single_style2 h2.jv_team_information,.jv_team_single_style2 .style2_contact_form h2{color: ".$divi_team_title_color." !important;}"; }
			if ( $divi_team_position_color != '' ){ $detail_style2 .= ".jv_team_single_style2 .jv_team_list_position{color: ".$divi_team_position_color." !important;}"; }
			if ( $divi_team_content_color != '' ){ $detail_style2 .= ".jv_team_single_style2 .jv_team_list_content{color: ".$divi_team_content_color." !important;}"; }
			if ( $divi_team_information_color != '' ){ $detail_style2 .= ".jv_team_single_style2 .jv_team_list_member_info ul li strong,.jv_team_single_style2 .jv_team_list_member_info ul li span,.jv_team_single_style2 .jv_team_list_member_info ul li a{color: ".$divi_team_information_color." !important;}"; }
			if ( $divi_team_icon_color != '' ){ $detail_style2 .= ".jv_team_single_style2 .jv_team_member_social_font.et-pb-icon{color: ".$divi_team_icon_color." !important;}"; }
			if ( $divi_team_horizontal_color != '' ){ $detail_style2 .= ".jv_team_single_style2 h2.jv_team_list_title,.jv_team_single_style2 .jv_team_list_text,.jv_team_single_style2 .jv_team_list_member_info ul li{ border-bottom: 2px solid ".$divi_team_horizontal_color." !important;}"; }
			if ( $divi_team_border_width != '' || $divi_team_border_color != '' ){ $detail_style2 .= ".jv_team_single_style2{border: ".$divi_team_border_width."px solid ".$divi_team_border_color." !important;}"; }
			if ( $divi_team_horizontal_color != '' ){ $detail_style2 .= " .jv_team_single_style2 .style2_hr_line{ border-color: ".$divi_team_horizontal_color." !important;}"; }
			
		   echo "<style>".$detail_style2."</style>";
		
		}if( $divi_team_select_style_layout == 'style3'){
			$detail_style3 = '';
			if ( $divi_team_title_color != '' ){  $detail_style3 .= ".jv_team_single_style3 h4.jv_team_list_title,.jv_team_single_style3 .jv_team_list_text h4{color: ".$divi_team_title_color."!important;}";}
			if ( $divi_team_information_color != '' ){  $detail_style3 .= ".jv_team_single_style3 .contect_number{color: ".$divi_team_information_color."!important;}";}
			if ( $divi_team_position_color != '' ){  $detail_style3 .= ".jv_team_single_style3 .jv_team_list_position{color: ".$divi_team_position_color."!important;}";}
			if ( $divi_team_content_color != '' ){  $detail_style3 .= ".jv_team_single_style3 .jv_team_list_content{color: ".$divi_team_content_color."!important;}";}
			if ( $divi_team_information_color != '' ){  $detail_style3 .= ".jv_team_single_style3 .email_address,.jv_team_single_style3 .website_url,.jv_team_single_style3 .jv_team_list_address1 span{color: ".$divi_team_information_color."!important;}";}
			if ( $divi_team_icon_color != '' ){  $detail_style3 .= ".jv_team_single_style3 .jv_team_member_social_font.et-pb-icon,.jv_team_member_icon_font.et-pb-icon{color: ".$divi_team_icon_color." !important;}";}
			if ( $divi_team_icon_color != '' ){  $detail_style3 .= ".jv_team_single_style3 .jv_team_list_social_link li a{border: 1px solid ".$divi_team_icon_color." !important;}";}
			if ( $divi_team_horizontal_color != '' ){  $detail_style3 .= ".jv_team_single_style3 .jv_team_list_social_link{border-bottom: 1px solid ".$divi_team_horizontal_color." !important;}";}
			if ( $divi_team_horizontal_color != '' ){ $detail_style3 .= " .jv_team_single_style3 .style3_hr_line{ border-color: ".$divi_team_horizontal_color." !important;}"; }
			
			echo "<style>".$detail_style3."</style>";
		
		}
		if( $divi_team_select_style_layout == 'style4'){
			$detail_style4 = ''; 
			if ( $divi_team_title_color != '' ){ $detail_style4 .= ".jv_team_single_style4 h1.jv_team_list_title,.jv_team_single_style4 .style4_contact_form h2{color: ".$divi_team_title_color." !important;}"; }
			if ( $divi_team_position_color != '' ){ $detail_style4 .= ".jv_team_single_style4 h2.jv_team_list_position{color: ".$divi_team_position_color." !important;}"; }
			if ( $divi_team_information_color != '' ){ $detail_style4 .= ".jv_team_single_style4 .email_address{color: ".$divi_team_information_color." !important;}"; }
			if ( $divi_team_icon_color != '' ){ $detail_style4 .= ".jv_team_single_style4 .jv_team_member_social_font.et-pb-icon{color: ".$divi_team_icon_color." !important;}"; }
			if ( $divi_team_horizontal_color != '' ){ $detail_style4 .= ".jv_team_single_style4 .jv_team_list_social_link{border-bottom: 1px solid ".$divi_team_horizontal_color." !important;}"; }
			if ( $divi_team_content_color != '' ){ $detail_style4 .= ".jv_team_single_style4 .jv_team_list_content{color: ".$divi_team_content_color." !important;}"; }
			if ( $divi_team_background_color != '' ){ $detail_style4 .= ".jv_team_single_style4{background-color: ".$divi_team_background_color." !important;}"; }
			if ( $divi_team_horizontal_color != '' ){ $detail_style4 .= " .jv_team_single_style4 .style4_hr_line{ border-color: ".$divi_team_horizontal_color." !important;}"; }
			
			echo "<style>".$detail_style4."</style>"; 
		
		}
		if( $divi_team_select_style_layout == 'style5'){	
		    $detail_style5 = '';
			if ( $divi_team_title_color != '' ){ $detail_style5 .= ".jv_team_single_style5 h2.jv_team_list_title,.jv_team_single_style5 .jv_team_list_text h2,.jv_team_single_style5 .style5_contact_form h2{color: ".$divi_team_title_color."!important;}"; }
			if ( $divi_team_content_color != '' ){ $detail_style5 .= ".jv_team_single_style5 .jv_team_list_content{color: ".$divi_team_content_color." !important;}"; }
			if ( $divi_team_icon_color != '' ){ $detail_style5 .= ".jv_team_single_style5 .jv_team_member_social_font.et-pb-icon,.jv_team_single_style5 .map-icon.et-pb-icon{color: ".$divi_team_icon_color." !important;}"; }
			if ( $divi_team_icon_color != '' ){ $detail_style5 .= ".jv_team_single_style5 .jv_team_list_social_link li a{border: 1px solid ".$divi_team_icon_color." !important;}"; }
			if ( $divi_team_information_color != '' ){ $detail_style5 .= ".jv_team_single_style5 .jv_team_as_designation,.jv_team_single_style5 .jv_team_as_designation ul li a,.jv_team_single_style5 .jv_team_list_map_donate_wrap a{color:".$divi_team_information_color." !important;}"; }
			if ( $divi_team_information_color != '' ){ $detail_style5 .= ".jv_team_single_style5 .map-icon.et-pb-icon{background-color: ".$divi_team_information_color." !important;}"; }
			if ( $divi_team_horizontal_color != '' ){ $detail_style5 .= " .jv_team_single_style5 .style5_hr_line{ border-color: ".$divi_team_horizontal_color." !important;}"; }			
			echo "<style>".$detail_style5."</style>"; 
		
		}
		if( $divi_team_select_style_layout =='style6'){
			$detail_style6 = '';
			if ( $divi_team_background_color != '' ){ $detail_style6 .= ".jv_team_single_style6 .jv_team_list_social_link,.jv_team_single_style6 .jv_team_list_title span{background-color: ".$divi_team_background_color." !important;}" ; }
			if ( $divi_team_title_color != '' ){ $detail_style6 .= ".jv_team_single_style6 .jv_team_list_title h2,.jv_team_single_style6 .jv_team_list_title span,.jv_team_single_style6 .style6_contact_form h2{color: ".$divi_team_title_color." !important;}" ; }
			if ( $divi_team_icon_color != '' ){ $detail_style6 .= ".jv_team_single_style6 .jv_team_feture_box .jv_team_icon span{color: ".$divi_team_icon_color." !important;}" ; }
			if ( $divi_team_information_color != '' ){ $detail_style6 .= ".jv_team_single_style6 .jv_team_feture_box .jv_team_info h4,.jv_team_single_style6 .jv_team_feture_box .jv_team_info span{color: ".$divi_team_information_color." !important;}" ; }
			if ( $divi_team_content_color != '' ){ $detail_style6 .= ".jv_team_single_style6 .jv_team_list_content{color: ".$divi_team_content_color." !important;}" ; }
			if ( $divi_team_horizontal_color != '' ){ $detail_style6 .= " .jv_team_single_style6 .style6_hr_line{ border-color: ".$divi_team_horizontal_color." !important;}"; }
			echo "<style>".$detail_style6."</style>";
		}	
	}
	echo "<style>.et-fb-modal__support-notice{display:none;}</style>";

}
add_action( 'wp_footer', 'jvteam_custom_css' );
add_action('admin_footer', 'jvteam_admin_footer');
function jvteam_admin_footer() {
	echo "<style>.et-fb-modal__support-notice{display:none;}</style>";
}
?>