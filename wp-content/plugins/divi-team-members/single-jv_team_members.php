<?php
get_header();
$divi_team_details_page_layout = !empty(get_option('divi_team_details_page_layout')) ? get_option('divi_team_details_page_layout') : 'Right'; 
$divi_team_select_style_layout = !empty(get_option('divi_team_select_style_layout')) ? get_option('divi_team_select_style_layout') : 'style1';

?>
<div id="main-content">
<div class="container jv_<?php echo $divi_team_details_page_layout;?> ">
   <div id="content-area" class="clearfix">
		<?php  if ( $divi_team_details_page_layout !='Fullwidth' ) {	 ?>
		<div id="left-area">
		<?php } ?>
        <?php while (have_posts()) : the_post(); ?>
                <article id="post-<?php echo $post->ID; ?>" <?php post_class(); ?>>
					<?php $display_contact_form = get_post_meta( $post->ID, 'display_contact_form', true ); ?>
                    <div class="et_pb_row clearfix jv_team_single_<?php echo $divi_team_select_style_layout; ?> <?php if ( $display_contact_form != 'on' ) { echo 'display_contact_form_style'; }?>" >
						<div class="et_pb_column et_pb_column_4_4">
						<?php
							
							$jv_template_path =  get_stylesheet_directory() . '/divi-team-members';
							$jv_css_path = $jv_template_path.'/css/team_detail_css';
							$jv_css_url =  get_stylesheet_directory_uri().'/divi-team-members/css/team_detail_css'; 
							
							$jv_grid_path =  $jv_template_path.'/team-detail';
							
							if ( file_exists( $jv_css_path . '/detail-'.$divi_team_select_style_layout.'.css' ) )
							{
								wp_enqueue_style('team_detail_css_'.$divi_team_select_style_layout, $jv_css_url . 'detail-'.$divi_team_select_style_layout.'.css', array(), NULL);
							}else{
								wp_enqueue_style( 'team_detail_css_'.$divi_team_select_style_layout, JVTEAM_PLUGIN_URL .'assets/css/team_detail_css/detail-'.$divi_team_select_style_layout.'.css');	
							}
			
							 if ( file_exists( $jv_grid_path . '/detail-'.$divi_team_select_style_layout.'.php' ) ){
									 include $jv_grid_path. '/detail-'.$divi_team_select_style_layout.'.php';
							 }else{
									include JVTEAM_PLUGIN_PATH. '/team-detail/detail-'.$divi_team_select_style_layout.'.php';
							 }	
							
				  		?>
						</div>
				</div> <!-- .et_pb_row -->
                </article> <!-- .et_pb_post -->
          
    <?php wp_reset_postdata(); endwhile; ?>
	<?php  if ( $divi_team_details_page_layout !='Fullwidth' ) {	 ?>
		</div> <!-- #left-area -->
		<?php
					if ( ( is_single() || is_page() ) && 'et_full_width_page' === get_post_meta( get_queried_object_id(), '_et_pb_page_layout', true ) )
						return;
					$divi_team_select_sidebar = !empty(get_option('divi_team_select_sidebar')) ? get_option('divi_team_select_sidebar') : 'Sidebar-1';
					if ( is_active_sidebar( $divi_team_select_sidebar ) ) : ?>
						<div id="sidebar">
							<?php dynamic_sidebar( $divi_team_select_sidebar ); ?>
						</div> <!-- end #sidebar -->
			<?php endif; ?>
		<?php } ?>
</div> <!-- #content-area -->
 </div> <!-- .container -->
</div> <!-- #main-content -->
<?php
get_footer();
?>