<?php
$team_members_categories = wp_get_post_terms( get_the_ID() ,'department_category', array("fields" => "ids"));
$cat_filter = '';
if ( count($team_members_categories ) > 0 ){
foreach ( $team_members_categories as $cat_id ){
		$cat_filter .= 'cat-'.$cat_id.' ';		
}
}
$divi_team_display_detail_page_link = !empty(get_option('divi_team_display_detail_page_link')) ? get_option('divi_team_display_detail_page_link') : 'Yes';
?>
<div class="et_pb_row team-scale-anm all et_pb_jv_team_members_list_column <?php echo rtrim($cat_filter);?>">
	<div class="et_pb_column et_pb_column_4_4">
		<div class="jv_team_list_member <?php if ( $divi_team_display_detail_page_link != 'Yes') { ?>jv_team_list_paddding <?php } ?>">
			<div class="jv_team_list_member_image">
				<?php 
					$jv_team_thumb = wp_get_attachment_image_src(get_post_thumbnail_id(), 'jv_team_list_image');
					if( $jv_team_thumb[0] != ''){ 
								$image_path = $jv_team_thumb[0] ;
							}else{
								$image_path = JVTEAM_PLUGIN_URL . '/assets/images/default.png';
							} 
					?>
				<img src="<?php echo $image_path;?>" alt="">	
			</div>
			<?php 
					$jv_team_designation = get_post_meta( get_the_ID(),'jv_team_designation', true );
					$jv_team_facebook = get_post_meta( get_the_ID(),'jv_team_facebook', true );
					$jv_team_twitter = get_post_meta( get_the_ID(),'jv_team_twitter', true );
					$jv_team_google = get_post_meta( get_the_ID(),'jv_team_google', true );
					$jv_team_linkdin = get_post_meta( get_the_ID(),'jv_team_linkedin', true );
					$jv_team_instagram = get_post_meta( get_the_ID(),'jv_team_instagram', true );
					$jv_team_mobile_number = get_post_meta( get_the_ID(),'jv_team_phone_number', true );
					$jv_team_email_address = get_post_meta( get_the_ID(),'jv_team_email_address', true );
					$jv_team_website_url = get_post_meta( get_the_ID(),'jv_team_website_url', true );
					$jv_team_address1 = get_post_meta( get_the_ID(),'jv_team_address1', true );
					$jv_team_address2 = get_post_meta( get_the_ID(),'jv_team_address2', true );
					$city_name_value = get_post_meta( get_the_ID(), 'jv_team_city_name', true );
			        $state_name_value = get_post_meta( get_the_ID(), 'jv_team_state_name', true );
				    $country_name_value = get_post_meta( get_the_ID(), 'jv_team_country_name', true );
				    $zipcode_number_value = get_post_meta( get_the_ID(), 'jv_team_zipcode', true );
					$jv_team_address = '';
					if( $jv_team_address1 != ''){
						$jv_team_address .= $jv_team_address1.',';
					}
					if( $jv_team_address2 != ''){
						$jv_team_address .= $jv_team_address2.',';
					}
					if( $city_name_value != ''){
						$jv_team_address .= $city_name_value.',';
					}
					if( $jv_team_state_name != ''){
						$jv_team_address .= $jv_team_state_name.',';
					}
					if( $country_name_value != ''){
						$jv_team_address .= $country_name_value.',';
					}
					if( $zipcode_number_value != ''){
						$jv_team_address .= $zipcode_number_value.',';
					}
			?>
			<div class="jv_team_list_description">
				<h4 class="jv_team_list_title"><?php echo get_the_title();?></h4>
				<?php if( $jv_team_designation != ''){ ?>
					<div class="jv_team_list_position"><?php echo $jv_team_designation;?></div>
					<?php } ?>
						<div class="jv_team_list_content">
								<?php $content_len= strlen(get_the_content());
									   $divi_team_content_length = !empty(get_option('divi_team_content_length')) ? get_option('divi_team_content_length') : '180';
									   if ($content_len > $divi_team_content_length){
										   echo do_shortcode(wpautop(substr(strip_tags(get_the_content()),0,$divi_team_content_length).'...'));
									   }else{
										   echo do_shortcode(wpautop(get_the_content()));
									   }
								?>
						</div> 
						<?php if( $jv_team_address != ''){ ?> 
						<div class="jv_team_list_address1"><i class="jv_team_member_icon_font et-pb-icon">&#xe01d;</i><?php echo rtrim($jv_team_address,',');?></div>
						<?php } ?>
						<?php if( $jv_team_website_url != ''){ ?> 
							<div class="jv_team_list_web_url"><i class="jv_team_member_icon_font et-pb-icon">&#xe0e3;</i>  <a class="website_url" href="<?php echo $jv_team_website_url;?>" target="_blank" ><?php echo $jv_team_website_url;?></a></div>
						<?php } ?> 
						<?php if( $jv_team_mobile_number != ''){ ?> 
							<div class="jv_team_list_mobile"><i class="jv_team_member_icon_font et-pb-icon">&#xe00b;</i>  <a class="contect_number" href="tel:<?php echo $jv_team_mobile_number;?>"  ><?php echo $jv_team_mobile_number;?></a></div>
						<?php } ?> 
						<?php if( $jv_team_email_address != ''){ ?> 
								<div class="jv_team_email_list_address"><i class="jv_team_member_icon_font et-pb-icon">&#xe076;</i>  <a class="email_address" href="mailto:<?php echo $jv_team_email_address;?>"  ><?php echo $jv_team_email_address;?></a></div>
						<?php } ?> 	
					
						<ul class="jv_team_list_social_link <?php if ( $divi_team_display_detail_page_link != 'Yes') { ?>jv_team_list_view_more_border <?php } ?>">
								<?php if( $jv_team_facebook != '' ) { ?><li><a href="<?php echo $jv_team_facebook;?>" target="_blank"><i class="jv_team_member_social_font et-pb-icon">&#xe0aa;</i></a></li><?php } ?>
								<?php if( $jv_team_twitter != '' ) { ?><li><a href="<?php echo $jv_team_twitter;?>" target="_blank"><i class="jv_team_member_social_font et-pb-icon">&#xe0ab;</i></a></li><?php } ?>
								<?php if( $jv_team_google != '' ) { ?><li><a href="<?php echo $jv_team_google;?>" target="_blank"><i class="jv_team_member_social_font jv_team_member_social_font_gogle et-pb-icon">&#xe0ad;</i></a></li><?php } ?>
								<?php if( $jv_team_linkdin != '' ) { ?><li><a href="<?php echo $jv_team_linkdin;?>"target="_blank"><i class="jv_team_member_social_font et-pb-icon">&#xe0b4;</i></a></li><?php } ?>
								<?php if( $jv_team_instagram != '' ) { ?><li><a href="<?php echo $jv_team_instagram;?>"target="_blank"><i class="jv_team_member_social_font et-pb-icon">&#xe0b1;</i></a></li><?php } ?>
						</ul>
					
			</div>
				<?php if ( $divi_team_display_detail_page_link == 'Yes') { ?>
					<a href="<?php echo get_permalink(get_the_ID()); ?>" class="jv_team_view_more">View More</a>
				<?php } ?> 	
		</div> 
	</div>
</div>