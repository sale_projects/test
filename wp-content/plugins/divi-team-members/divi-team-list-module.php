<?php 
function et_builder_include_jv_team_list_categories(){
		$terms = get_terms( array(
			'taxonomy' 	=> 'department_category',
			'hide_empty' 	=> false,
		) );
		$terms_content = array();
		$count = count($terms);
		if ( $count > 0 ){
			foreach($terms as $department_categoryval){
				$terms_content[$department_categoryval->term_id] = $department_categoryval->name;
			}
		}
		return $terms_content;  
} 
function jv_team_list_modules() {
	class ET_Builder_Module_JV_Team_List_Member extends ET_Builder_Module {
		function init() {
			$this->name 			=  esc_html__( 'Team Members List', 'et_builder' );
			$this->slug 			= 'et_pb_jv_team_list_members';
			$this->vb_support  		= 'partial';
			$this->main_css_element = '%%order_class%%.et_pb_jv_team_members_list';
		}
		function get_settings_modal_toggles() {
			return array(
				'general'	=> 	array(
					'toggles'	=> 	array(
						'jv_team_member_list_general'	=> 	esc_html__( 'General Settings', 'et_builder' ),
						'jv_team_member_list_category'	=> esc_html__( 'Category Settings', 'et_builder' ),
						'jv_team_member_list_color'		=> 	esc_html__( 'Color', 'et_builder' ),
					),
				),
			);
		}
		function get_advanced_fields_config() {
			return  array(
				'fonts' => array(
					'team_name'		=> array(
						'label'		=> esc_html__( 'Team Name', 'et_builder' ),
						'css'      	=> array(
							'main' 		=> "{$this->main_css_element} .jv_team_list_title",
					),
						'font_size' => array(
							'default'	=> '23px',
						),
					),
					'designation_name_fonts'	=> array(
						'label'		=> esc_html__( 'Designation Name', 'et_builder' ),
						'css'		=> array(
							'main' 		=> "{$this->main_css_element} .jv_team_list_position",
						),
						'font_size' => array(
							'default'	=> '20px',
						),
					),
					'descrption_name_fonts'	=> array(
						'label'		=> esc_html__( 'Content Box Color', 'et_builder' ),
						'css'     	=> array(
							'main' 	=> "{$this->main_css_element} .jv_team_list_content,{$this->main_css_element} .website_url,{$this->main_css_element} .email_address,{$this->main_css_element} .contect_number,{$this->main_css_element} .jv_team_list_address1,{$this->main_css_element} .jv_team_list_social_link li,{$this->main_css_element} .jv_team_info span,{$this->main_css_element} .team-expand .team-more",
						),
						'font_size'	=> array(
							'default'	=> '15px',
						),
					),
				),
				'button'	=> array(
					'button' 		=> array(
						'label' 	=> esc_html__( 'Button', 'et_builder' ),
						'css' 		=> array(
							'main' 	=> "{$this->main_css_element} .jv_team_view_more",
						),
					),
				),
				'borders' => array(
					'default' => array(
						'css'      => array(
							'main' => array(
								'border_radii'	=> '%%order_class%%.et_pb_jv_team_members_list .et_pb_jv_team_members_list_column',
								'border_styles'	=> '%%order_class%%.et_pb_jv_team_members_list .et_pb_jv_team_members_list_column',
							),
						),
						'defaults' => array(
							'border_radii' 	=> 'on|0px|0px|0px|0px',
							'border_styles' => array(
									'width'		=> '1px',
									'color'		=> '#d9d9d9',
									'style'		=> 'solid',
							),
						),
					),					
				),
				'box_shadow' => array(
					'default' => array(
						'css' => array(
							'main' => "{$this->main_css_element}.et_pb_jv_team_members_list .et_pb_row",
						),
					),
				),
				'filters'	=> false,
				'max_width'		=> false,
				'background'	=> array(
						'use_background_image'	=>	false,
						'use_background_video'	=>	false,
				),
			); 
		}

		function get_custom_css_fields_config() {
			return array(
				'team_box' => array(
					'label'    => esc_html__( 'Team Members Box', 'et_builder' ),
					'selector' => '.et_pb_jv_team_members_list_column',
				),
				'team_name' => array(
					'label'    => esc_html__( 'Team Name', 'et_builder' ),
					'selector' => '.jv_team_list_title',
				),
				'designation_name_fonts' => array(
					'label'    => esc_html__( 'Designation Name', 'et_builder' ),
					'selector' => '.jv_team_list_position',
				),
				'descrption_name_fonts' => array(
					'label'    => esc_html__( 'Description Name', 'et_builder' ),
					'selector' => '.jv_team_list_content,.website_url,.email_address,.contect_number,.jv_team_list_address1',
				),
				'category_filter' => array(
					'label'    => esc_html__( 'Category Filter', 'et_builder' ),
					'selector' => '.et_pb_jv_team_category_list li a',
				),
				'category_filter_active' => array(
					'label'    => esc_html__( 'Category Filter Active', 'et_builder' ),
					'selector' => '.et_pb_jv_team_category_list li a.active',
				),
				'social_link_text_color' => array(
					'label'    => esc_html__( 'Social link color', 'et_builder' ),
					'selector' => '.jv_team_member_social_font.et-pb-icon',
				),
				'social_link_text_hover_color' => array(
					'label'    => esc_html__( 'Social link hover color', 'et_builder' ),
					'selector' => '.jv_team_member_social_font.et-pb-icon:hover',
				),
			); 
		}

		function get_fields() {
			$fields = array(
				'select_style'	=> array(
					'label'			=> esc_html__( 'Select Style', 'et_builder' ),
					'type'			=> 'select',
					'option_category'	=> 'basic_option',
					'default'		=> 'style1',
					'options'		=> array(
						'style1'		=> esc_html__( 'Style1 Layout', 'et_builder' ),
						'style2'		=> esc_html__( 'Style2 Layout', 'et_builder' ),
						'style3'		=> esc_html__( 'Style3 Layout', 'et_builder' ),
						'style4'		=> esc_html__( 'Style4 Layout', 'et_builder' ),
						'style5'		=> esc_html__( 'Style5 Layout', 'et_builder' ),
						'style6'		=> esc_html__( 'Style6 Layout', 'et_builder' ),
						'style7'		=> esc_html__( 'Style7 Layout', 'et_builder' ),
						'style8'		=> esc_html__( 'Style8 Layout', 'et_builder' ),
						'style9'		=> esc_html__( 'Style9 Layout', 'et_builder' ),
					),
					'description'	=> esc_html__( 'Here you can select style.', 'et_builder' ),
					'toggle_slug'	=> 'jv_team_member_list_general',
				), 
				'display_category_filter'	=> array(
					'label'			=> esc_html__( 'Display Category Filter', 'et_builder' ),
					'type'			=> 'yes_no_button',
					'option_category' 	=> 'configuration',
					'options'		=> array(
						'on'  			=> esc_html__( 'yes', 'et_builder' ),
					    'off'			=> esc_html__( 'No', 'et_builder' ),
					),
					'default'		=> 'on',
					'toggle_slug'	=> 'jv_team_member_list_general',
					'description'	=> esc_html__( 'This setting will turn on and off the Category Filter.', 'et_builder' ),
				),	
				'filter_style' 	=> array(
					'label'			=> esc_html__( 'Button Filter Style', 'et_builder' ),
					'type'			=> 'select',
					'option_category' 	=> 'basic_option',
					'options'		=> array(
						'fstyle1'		=> esc_html__( 'Style 1', 'et_builder' ),
						'fstyle2'		=> esc_html__( 'Style 2', 'et_builder' ),
						'fstyle3'		=> esc_html__( 'Style 3', 'et_builder' ),
						'fstyle4'		=> esc_html__( 'Style 4', 'et_builder' ),
						'fstyle5'		=> esc_html__( 'Style 5', 'et_builder' ),
						'fstyle6'		=> esc_html__( 'Style 6', 'et_builder' ),
						'fstyle7'		=> esc_html__( 'Style 7', 'et_builder' ),
						'fstyle8'		=> esc_html__( 'Style 8', 'et_builder' ),
						'fstyle9'		=> esc_html__( 'Style 9', 'et_builder' ),
						'fstyle10'		=> esc_html__( 'Style 10', 'et_builder' ),
						'fstyle11'		=> esc_html__( 'Style 11', 'et_builder' ),
						'fstyle12'		=> esc_html__( 'Style 12', 'et_builder' ),
						'fstyle13'		=> esc_html__( 'Style 13', 'et_builder' ),
						'fstyle14'		=> esc_html__( 'Style 14', 'et_builder' ),
						'fstyle15'		=> esc_html__( 'Style 15', 'et_builder' ),
						'fstyle16'		=> esc_html__( 'Style 16', 'et_builder' ),
						'fstyle17'		=> esc_html__( 'Style 17', 'et_builder' ),
						'fstyle18'		=> esc_html__( 'Style 18', 'et_builder' ),
						'fstyle19'		=> esc_html__( 'Style 19', 'et_builder' ),
						'fstyle20'		=> esc_html__( 'Style 20', 'et_builder' ),
				),
					'default'		=> 'fstyle1',
 					'show_if'		=> array('display_category_filter' => 'on'),
					'toggle_slug'	=> 'jv_team_member_list_general',
					'description'	=> esc_html__( 'Here you can select filter style.', 'et_builder' ),
				), 
				'display_popup_onteam' 	=> array(
					'label'			=> esc_html__( 'Display Popup On Link', 'et_builder' ),
					'type'			=> 'yes_no_button',			
					'options'		=> array(
						'off' 			=> esc_html__( 'No', 'et_builder' ),
						'on'  			=> esc_html__( 'yes', 'et_builder' ),
					),
					'default'		=> 'off',
					'toggle_slug'	=> 'jv_team_member_list_general',
					'description'	=> esc_html__( 'This setting will turn on and off the display Popup on link.', 'et_builder' ),
				),	
				'display_detail_page' 	=> array(
					'label'			=> esc_html__( 'Display Detail Page Link', 'et_builder' ),
					'type'			=> 'yes_no_button',	
 					'show_if'		=> array('display_popup_onteam' => 'off'),
					'toggle_slug'	=> 'jv_team_member_list_general',	
					'default'		=> 'on',		
					'options'		=> array(
						'on'  			=> esc_html__( 'yes', 'et_builder' ),
						'off' 			=> esc_html__( 'No', 'et_builder' ),
					),
					'description'	=> esc_html__( 'This setting will turn on and off the display social link.', 'et_builder' ),
				),
				'display_detail_page_link_type' 	=> array(
					'label'			=> esc_html__( 'Display Detail Page Link Type', 'et_builder' ),
					'type'			=> 'select',
 					'show_if'		=> array('display_detail_page' => 'on'),
					'toggle_slug'	=> 'jv_team_member_list_general',
					'option_category'	=> 'basic_option',
					'options'		=> array(
						'default'   	=> esc_html__( 'Default', 'et_builder' ),
						'custom'   		=> esc_html__( 'Custom', 'et_builder' ),
					), 
					'default'		=> 'default',
					'description'	=> esc_html__( 'Here you can select the team member Display Detail Page Link Type.', 'et_builder' ),
				),
				'link_open_in_new_tab' 	=> array(
					'label'			=> esc_html__( 'Open In a New Tab', 'et_builder' ),
					'type'			=> 'yes_no_button',	
 					'show_if'		=> array('display_detail_page' => 'on'),
					'toggle_slug'	=> 'jv_team_member_list_general',		
					'options'		=> array(
						'off' 			=> esc_html__( 'No', 'et_builder' ),
						'on'  			=> esc_html__( 'yes', 'et_builder' ),
					),
					'default'		=> 'off',
					'description'	=> esc_html__( 'This setting will turn on and off the Open In a New Tab link.', 'et_builder' ),
				),
				'posts_number' 	=> array(
					'label'			=> esc_html__( 'Number of Team', 'et_builder' ),
					'type'			=> 'text',
					'option_category'	=> 'configuration',
					'toggle_slug'	=> 'jv_team_member_list_general',
					'description'		=> esc_html__( 'Choose how many Team Members you would like to display.', 'et_builder' ),
				),
				'orderby' 	=> array(
					'label'			=> esc_html__( 'Order By', 'et_builder' ),
					'type'			=> 'select',
					'option_category'	=> 'configuration',
					'options'		=> array(
						'date_desc'  	=> esc_html__( 'Date: new to old', 'et_builder' ),
						'date_asc'   	=> esc_html__( 'Date: old to new', 'et_builder' ),
						'title_asc'  	=> esc_html__( 'Title: a-z', 'et_builder' ),
						'title_desc' 	=> esc_html__( 'Title: z-a', 'et_builder' ),
						'menu_order_desc' 	=> esc_html__( 'Menu Order : DESC', 'et_builder' ),
						'menu_order_asc' 	=> esc_html__( 'Menu Order : ASC', 'et_builder' ),
						'rand'       	=> esc_html__( 'Random', 'et_builder' ),
					),
					'default'		=> 'date_desc',
					'toggle_slug'	=> 'jv_team_member_list_general',
					'description'	=> esc_html__( 'Here you can adjust the order in which posts are displayed.', 'et_builder' ),
				),
				'filter_all_label' 	=> array(
					'label'			=> esc_html__( 'ALL Text Filter Label', 'et_builder' ),
					'type'			=> 'text',
					'option_category'	=> 'configuration',
					'show_if'		=> array('display_category_filter' => 'on'),	
					'default'		=> 'All',
					'toggle_slug'	=> 'jv_team_member_list_general',
					'description'	=> esc_html__( 'ALL Text Filter Label', 'et_builder' ),
				),
				'category_orderby' 	=> array(
					'label'			=> esc_html__( 'Category Filter List Order By', 'et_builder' ),
					'type'			=> 'select',
					'option_category'	=> 'configuration',
					'options'		=> array(
						'id_desc'  		=> esc_html__( 'ID: High to Low', 'et_builder' ),
						'id_asc'   		=> esc_html__( 'ID: Low to High', 'et_builder' ),
						'name_asc'  	=> esc_html__( 'Title: a-z', 'et_builder' ),
						'name_desc' 	=> esc_html__( 'Title: z-a', 'et_builder' ),
						'slug_desc' 	=> esc_html__( 'Slug : z-a', 'et_builder' ),
						'slug_asc' 		=> esc_html__( 'Slug : a-z', 'et_builder' ),
						'count'			=> esc_html__( 'Count', 'et_builder' ),
					),
					'default'		=> 'id_desc',
					'show_if'		=> array('display_category_filter' => 'on'),	
					'toggle_slug'	=> 'jv_team_member_list_general',
					'description'	=> esc_html__( 'Here you can adjust Category Filter List Order.', 'et_builder' ),
				),	
				'display_view_more_button' 	=> array(
					'label'			=> esc_html__( 'Display View More Button', 'et_builder' ),
					'type'			=> 'yes_no_button',
					'option_category' 	=> 'configuration',
					'options'		=> array(
						'on'  			=> esc_html__( 'yes', 'et_builder' ),
						'off' 			=> esc_html__( 'No', 'et_builder' ),
					),
					 'default'			=> 'on',
					 'toggle_slug'	=> 'jv_team_member_list_general',
					'description'	=> esc_html__( 'This setting will turn on and off the display view more button.', 'et_builder' ),
				),
				'read_more_text_btn' 	=> array(
					'label'			=> esc_html__( 'Read More Text', 'et_builder' ),
					'type'			=> 'text',
					'option_category'	=> 'configuration',
 					'show_if'		=> array('display_view_more_button' => 'on'),
					'toggle_slug'	=> 'jv_team_member_list_general',
					'default'		=> 'View More',
					'description'   => esc_html__( 'Choose how many Team Members you would like to change read more text.', 'et_builder' ),
				),

				'display_category'	=> array(
					'label'			=> esc_html__( 'Display Category On Filter/Display Team Member By Category', 'et_builder' ),
					'type'			=> 'select',
					'option_category'	=> 'basic_option',
					 'options'		=> array(
						'all'   		=> esc_html__( 'All', 'et_builder' ),
						'specificcategory'	=> esc_html__( 'Specific Category', 'et_builder' ),
					), 
					'default'		=> 'all',
					'toggle_slug'	=> 'jv_team_member_list_category',
					'description'	=> esc_html__( 'Here you can select the team members display category.', 'et_builder' ),
				),
				'include_categories' 	=> array(
					'label'			=> esc_html__( 'Specific Categories', 'et_builder' ),
					'renderer'		=> 'et_builder_include_categories_option',
					'option_category'	=> 'basic_option',
 					'show_if'		=> array('display_category' => 'specificcategory'),
					'toggle_slug'	=> 'jv_team_member_list_category',
					'renderer_options' 	=> array(
						'use_terms' 	=> true,
						'term_name' 	=> 'department_category',
					),
					'description'	=> esc_html__( 'Choose which categories you would like to include in the team members filter.', 'et_builder' ),
				),
				'style1_list_image_shadow_color' => array(
					'label'			=> esc_html__( 'Style 1 Image Shadows Color', 'et_builder' ),
					'type'			=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style1'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a image shadow color color for this module.', 'et_builder' ),
				),
				'style1_list_image_shadow_hover_color' => array(
					'label'        	=> esc_html__( 'Style 1 Image Shadows Hover Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style1'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a image shadow hover color color for this module.', 'et_builder' ),
				),
				'style1_list_box_shadow' 	=> array(
					'label'        	=> esc_html__( 'Style 1 Box Shaodow Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style1'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a box shadow color color for this module.', 'et_builder' ),
				),
				'style1_list_box_shadow_hover' 	=> array(
					'label'        	=> esc_html__( 'Style 1 Box Shaodow Hover Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style1'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a box shadow hover color color for this module.', 'et_builder' ),
				),
				'style1_list_bg_content_color' 	=> array(
					'label'        	=> esc_html__( 'Style 1 Content Background Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style1'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a content backgound color for this module.', 'et_builder' ),
				),
				'style1_list_discription_bottom_border' => array(
					'label'        	=> esc_html__( 'Style 1 Bottom Border Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style1'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a border bottom color color for this module.', 'et_builder' ),
				),
				'style1_button_active_color' 	=> array(
					'label'        	=> esc_html__( 'Style 1 Button Filter Active Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('filter_style' => 'fstyle1'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter active color for this module.', 'et_builder' ),
				),
				'style1_button_color' 	=> array(
					'label'        	=> esc_html__( 'Style 1 Button Filter Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('filter_style' => 'fstyle1'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter color for this module.', 'et_builder' ),
				),
				'style2_list_bg_content_color' 	=> array(
					'label'        	=> esc_html__( 'Style 2 Content Background Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style2'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a content background color for this module.', 'et_builder' ),
				),
				'style2_list_image_border_color' 	=> array(
					'label'        	=> esc_html__( 'Style 2 Border Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style2'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a border color for this module.', 'et_builder' ),
				),
				'style2_list_boxshadow_color' 	=> array(
					'label'        	=> esc_html__( 'Style 2 Box Shadow Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style2'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a box shadow color color for this module.', 'et_builder' ),
				),
				'style2_list_boxshadow_hover_color' 	=> array(
					'label'        	=> esc_html__( 'Style 2 Box Shadow Hover Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style2'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a box shadow hover color for this module.', 'et_builder' ),
				),
				'style2_list_discription_bottom_border' 	=> array(
					'label'        	=> esc_html__( 'Style 2 Bottom Border Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style2'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a bottom border color color for this module.', 'et_builder' ),
				),
				'style2_list_position_bottom_border' 	=> array(
					'label'        	=> esc_html__( 'Style 2 Bottom Border Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style2'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a content color for this module.', 'et_builder' ),
				),
				'style2_button_active_color' => array(
					'label'        	=> esc_html__( 'Style 2 Button Filter Active Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('filter_style' => 'fstyle2'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter active color color for this module.', 'et_builder' ),
				),
				'style2_button_animation_border_color' 	=> array(
					'label'        	=> esc_html__( 'Style 2 Button Filter Animation Border Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('filter_style' => 'fstyle2'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter animation border color color for this module.', 'et_builder' ),
				),
				'style2_button_hover_color' 	=> array(
					'label'        	=> esc_html__( 'Style 2 Button Filter Border Color & Button hover color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('filter_style' => 'fstyle2'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter border color & button filter hover color for this module.', 'et_builder' ),
				),
				'style3_list_bg_content_color' 	=> array(
					'label'        	=> esc_html__( 'Style 3 Content Background Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style3'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a content background color for this module.', 'et_builder' ),
				),
				'style3_list_boxshadow_color' 	=> array(
					'label'       	=> esc_html__( 'Style 3 Box Shadow Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style3'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a box shadow color for this module.', 'et_builder' ),
				),
				'style3_list_boxshadow_hover_color' 	=> array(
					'label'        	=> esc_html__( 'Style 3 Box Shadow Hover Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style3'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a box shadow hover color for this module.', 'et_builder' ),
				),
				'style3_list_border_color' 	=> array(
					'label'        	=> esc_html__( 'Style 3 Border Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style3'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a border color color for this module.', 'et_builder' ),
				),
				'style3_button_border_color' 	=> array(
					'label'        	=> esc_html__( 'Style 3 Button Filter Active Border Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('filter_style' => 'fstyle3'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter active border color for this module.', 'et_builder' ),
				),
				'style3_button_color' 	=> array(
					'label'        	=> esc_html__( 'Style 3 Button Filter Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('filter_style' => 'fstyle3'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter color for this module.', 'et_builder' ),
				),
				'style3_button_hover_color' 	=> array(
					'label'        	=> esc_html__( 'Style 3 Button Filter hover Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('filter_style' => 'fstyle3'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter hover color for this module.', 'et_builder' ),
				),
				'style4_list_bg_content_color' 	=> array(
					'label'        	=> esc_html__( 'Style 4 Content Background Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style4'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a content background color for this module.', 'et_builder' ),
				),
				'style4_list_box_border_color' 	=> array(
					'label'        	=> esc_html__( 'Style 4 Box Border Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style4'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a box border color for this module.', 'et_builder' ),
				),
				'style4_list_box_shadow_color' 	=> array(
					'label'        	=> esc_html__( 'Style 3 Box Shadow Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style4'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a list box shadows color for this module.', 'et_builder' ),
				),
				'style4_list_box_shadow_hover_color' 	=> array(
					'label'        	=> esc_html__( 'Style 3 Box Shadow Hover Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style4'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a list box shadows hover color for this module.', 'et_builder' ),
				),
				'style4_button_border_color' 	=> array(
					'label'        	=> esc_html__( 'Style 4 Button Filter Border Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('filter_style' => 'fstyle4'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter border color for this module.', 'et_builder' ),
				),
				'style4_button_color' 	=> array(
					'label'        	=> esc_html__( 'Style 4 Button Filter Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('filter_style' => 'fstyle4'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button color for this module.', 'et_builder' ),
				),
				'style5_button_border_active_color' 	=> array(
					'label'        	=> esc_html__( 'Style 5 Button Filter Border Active Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('filter_style' => 'fstyle5'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter border active color for this module.', 'et_builder' ),
				),
				'style5_button_border_color' 	=> array(
					'label'        	=> esc_html__( 'Style 5 Button Filter Border Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('filter_style' => 'fstyle5'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter border color for this module.', 'et_builder' ),
				),
				'style6_button_active_color' 	=> array(
					'label'        	=> esc_html__( 'Style 6 Button Filter Active Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('filter_style' => 'fstyle6'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter active color for this module.', 'et_builder' ),
				),
				'style6_button_color' 	=> array(
					'label'        	=> esc_html__( 'Style 6 Button Filter Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('filter_style' => 'fstyle6'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter color for this module.', 'et_builder' ),
				),
				'style7_button_border_active_color' 	=> array(
					'label'        	=> esc_html__( 'Style 7 Button Filter Border Active Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('filter_style' => 'fstyle7'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter border active color for this module.', 'et_builder' ),
				),
				'style7_button_border_color' 	=> array(
					'label'        	=> esc_html__( 'Style 7 Button Filter Border Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('filter_style' => 'fstyle7'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter border color for this module.', 'et_builder' ),
				),
				'style7_discription_bg_color' 	=> array(
					'label'        	=> esc_html__( 'Style 7 Discription Background Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style7'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description' 	=> esc_html__( 'Use the color picker to choose a discription background color for this module.', 'et_builder' ),
				),
				'style7_content_bg_color' 	=> array(
					'label'        	=> esc_html__( 'Style 7 Content Background Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style7'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a content background color for this module.', 'et_builder' ),
				),
				'style7_border_color' 	=> array(
					'label'       	=> esc_html__( 'Style7 Horizontal Border Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style7'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a horizontal border color for this module.', 'et_builder' ),
				),				
				'style8_button_active_color' 	=> array(
					'label'        	=> esc_html__( 'Style 8 Button Filter Active Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('filter_style' => 'fstyle8'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button flter active color for this module.', 'et_builder' ),
				),
				'style8_button_border_color' 	=> array(
					'label'        	=> esc_html__( 'Style 8 Button Filter Border Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('filter_style' => 'fstyle8'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter border color for this module.', 'et_builder' ),
				),
				'style9_button_active_color' 	=> array(
					'label'        	=> esc_html__( 'Style 9 Button Filter Active Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('filter_style' => 'fstyle9'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter active color for this module.', 'et_builder' ),
				),
				'style9_button_color' 	=> array(
					'label'        	=> esc_html__( 'Style 9 Button Filter Color', 'et_builder' ),
					'type'        	=> 'color-alpha',
 					'show_if'		=> array('filter_style' => 'fstyle9'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter color for this module.', 'et_builder' ),
				),
				'style10_button_active_border_color' 	=> array(
					'label'        	=> esc_html__( 'Style 10 Button Filter Active Border Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('filter_style' => 'fstyle10'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter active border color for this module.', 'et_builder' ),
				),
				'style10_button_color' 	=> array(
					'label'        	=> esc_html__( 'Style 10 Button Filter Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('filter_style' => 'fstyle10'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter color for this module.', 'et_builder' ),
				),
				'style11_button_active_color' 	=> array(
					'label'       	=> esc_html__( 'Style 11 Button Filter Active Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('filter_style' => 'fstyle11'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter active color for this module.', 'et_builder' ),
				),
				'style11_button_color' 	=> array(
					'label'        	=> esc_html__( 'Style 11 Button Filter Hover Color & Border Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('filter_style' => 'fstyle11'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter hover color & border color for this module.', 'et_builder' ),
				),
				'style12_button_active_border_color' 	=> array(
					'label'        	=> esc_html__( 'Style 12 Button Filter Active Border Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style1'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter active border color for this module.', 'et_builder' ),
				),
				'style12_button_border_color' 	=> array(
					'label'        	=> esc_html__( 'Style 12 Button Filter Border Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('filter_style' => 'fstyle12'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter border color for this module.', 'et_builder' ),
				),
				'style13_button_active_color' 	=> array(
					'label'        	=> esc_html__( 'Style 13 Button Filter Active Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('filter_style' => 'fstyle13'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter active color for this module.', 'et_builder' ),
				),
				'style13_button_active_hover_color' 	=> array(
					'label'       	=> esc_html__( 'Style 13 Button Filter Active Hover Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('filter_style' => 'fstyle13'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description' 	=> esc_html__( 'Use the color picker to choose a button filter active hover color for this module.', 'et_builder' ),
				),
				'style13_button_hover_color' 	=> array(
					'label'        	=> esc_html__( 'Style 13 button Filter border bolor & button hover color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('filter_style' => 'fstyle13'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button border color & button hover color for this module.', 'et_builder' ),
				),
				'style14_button_active_color' 	=> array(
					'label'        	=> esc_html__( 'Style 14 Button Filter Active Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if' 		=> array('filter_style' => 'fstyle14'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button active color color for this module.', 'et_builder' ),
				),
				'style14_button_color' 	=> array(
					'label'        	=> esc_html__( 'Style 14 Button Filter Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('filter_style' => 'fstyle14'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button color for this module.', 'et_builder' ),
				),
				'style15_button_active_color' 	=> array(
					'label'        	=> esc_html__( 'Style 15 Button Filter Active Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('filter_style' => 'fstyle15'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter active color for this module.', 'et_builder' ),
				),
				'style15_button_active_hover_color' 	=> array(
					'label'        	=> esc_html__( 'Style 15 Button Filter Active Hover Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('filter_style' => 'fstyle15'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter active hover color for this module.', 'et_builder' ),
				),
				'style15_button_hover_color' 	=> array(
					'label'        	=> esc_html__( 'Style 15 Button Filter Border Color & Button Hover Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('filter_style' => 'fstyle15'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter border color & button filter hover color for this module.', 'et_builder' ),
				),
				'style16_button_active_color' 	=> array(
					'label'        	=> esc_html__( 'Style 16 Button Filter Active Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('filter_style' => 'fstyle16'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter active color for this module.', 'et_builder' ),
				),
				'style16_button_hover_color' 	=> array(
					'label'        	=> esc_html__( 'Style 16 Button Filter Hover Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('filter_style' => 'fstyle16'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter hover color for this module.', 'et_builder' ),
				),
				'style16_button_border_color' 	=> array(
					'label'        	=> esc_html__( 'Style 16 Button Filter Border Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('filter_style' => 'fstyle16'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter border color for this module.', 'et_builder' ),
				),
				'style17_button_active_color' 	=> array(
					'label'        	=> esc_html__( 'Style 17 Button Filter Active Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('filter_style' => 'fstyle17'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter active color for this module.', 'et_builder' ),
				),
				'style17_button_border_color' 	=> array(
					'label'        	=> esc_html__( 'Style 17 Button Filter Border Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('filter_style' => 'fstyle17'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter border color for this module.', 'et_builder' ),
				),
				'style17_button_hover_color' 	=> array(
					'label'        	=> esc_html__( 'Style 17 Button Filter Hover Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('filter_style' => 'fstyle17'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter hover color for this module.', 'et_builder' ),
				),
				'style18_button_border_color' 	=> array(
					'label'        	=> esc_html__( 'Style 18 Button Filter Border Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('filter_style' => 'fstyle18'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter border color for this module.', 'et_builder' ),
				),
				'style18_button_color' 	=> array(
					'label'        	=> esc_html__( 'Style 18 Button Filter Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('filter_style' => 'fstyle18'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter border color for this module.', 'et_builder' ),
				),
				'style19_button_gardien1_color' => array(
					'label'        	=> esc_html__( 'Style 19 Button Filter Gardien Color 1', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('filter_style' => 'fstyle19'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter gardien color 1 for this module.', 'et_builder' ),
				),
				'style19_button_gardien2_color' 	=> array(
					'label'        	=> esc_html__( 'Style 19 Button Filter Gardien Color 2', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('filter_style' => 'fstyle19'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter gardien color 2 for this module.', 'et_builder' ),
				),
				'style19_button_gardien3_color' 	=> array(
					'label'        	=> esc_html__( 'Style 19 Button Filter Gardien Color 3', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('filter_style' => 'fstyle19'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter gardien color 3 for this module.', 'et_builder' ),
				),
				'style19_button_gardien4_color' 	=> array(
					'label'        	=> esc_html__( 'Style 19 Button Filter Gardien Color 4', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('filter_style' => 'fstyle19'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter gardien color 4 for this module.', 'et_builder' ),
				),
				'style19_button_active_border_color' => array(
					'label'        	=> esc_html__( 'Style 19 Button Filter Active Border Color', 'et_builder' ),
					'type'        	=> 'color-alpha',
 					'show_if'		=> array('filter_style' => 'fstyle19'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter active border color for this module.', 'et_builder' ),
				),
				'style20_button_border_color' 	=> array(
					'label'        	=> esc_html__( 'Style 20 Button Filter Border Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('filter_style' => 'fstyle20'),
					'toggle_slug'	=> 'jv_team_member_list_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter border color for this module.', 'et_builder' ),
				),
				'buttons_text_color'	=> array(
				    'label'        	=> esc_html__( 'Button Filter Text Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
					'custom_color' 	=> true,
					'toggle_slug'	=> 'jv_team_member_list_color',
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter text color for this module.', 'et_builder' ),
				),
				'button_text_hover_color'=> array(
				    'label'        	=> esc_html__( 'Button Filter Text Hover Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
					'custom_color' 	=> true,
					'toggle_slug'	=> 'jv_team_member_list_color',
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter text hover color for this module.', 'et_builder' ),
				),
				'social_link_text_color' 	=> array(
					'label'        	=> esc_html__( 'Style Social Link Text Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
					'custom_color' 	=> true,
					'toggle_slug'	=> 'jv_team_member_list_color',
					'description'  	=> esc_html__( 'Use the color picker to choose a social link text color content color for this module.', 'et_builder' ),
				),
				'social_link_text_hover_color' 	=> array(
					'label'        	=> esc_html__( 'Style Social Link Text Hover Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
					'custom_color' 	=> true,
					'toggle_slug'	=> 'jv_team_member_list_color',
					'description'  	=> esc_html__( 'Use the color picker to choose a social link text hover color for this module.', 'et_builder' ),
				),
				'icon_color' 	=> array(
					'label'        	=> esc_html__( 'Style Icon Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
					'custom_color' 	=> true,
					'toggle_slug'	=> 'jv_team_member_list_color',
					'description'  	=> esc_html__( 'Use the color picker to choose a icon color for this module.', 'et_builder' ),
				),
				'team_members_display_pagination' => array(
					'label'			=> esc_html__( 'Display Pagination', 'et_builder' ),
					'type'			=> 'yes_no_button',
					'option_category' 	=> 'configuration',
					'options'		=> array(
						'off' 			=> esc_html__( 'No', 'et_builder' ),
						'on'  			=> esc_html__( 'yes', 'et_builder' ),
					),
					'default'		=> 'off',
					'toggle_slug'	=> 'jv_team_member_list_general',
					'description'	=> esc_html__( 'This setting will turn on and off the Pagination.', 'et_builder' ),
				),	
				'display_social_link' 	=> array(
					'label'			=> esc_html__( 'Display Social Link', 'et_builder' ),
					'type'			=> 'yes_no_button',
					'option_category' 	=> 'configuration',
					'options'		=> array(
						'on'  			=> esc_html__( 'yes', 'et_builder' ),
						'off' 			=> esc_html__( 'No', 'et_builder' ),
					),
					'toggle_slug'	=> 'jv_team_member_list_general',					
					'default'		=> 'off',
					'description'	=> esc_html__( 'This setting will turn on and off the Equal Height of Content.', 'et_builder' ),
				),
				
			);
			return $fields;
		}
		
		function render( $attrs, $content = null, $render_slug ) {

			$display_popup_onteam	= $this->props['display_popup_onteam'];
			$divi_popupteam_select_style_layout	= !empty(get_option('divi_popupteam_select_style_layout')) ? get_option('divi_popupteam_select_style_layout') : 'style1';
			if ( !is_admin()){
				wp_enqueue_style('jv_team_module_list_members', JVTEAM_PLUGIN_URL .'assets/css/archive_team_members_list_css/archive_team_members_list.css', array(), NULL);			
				wp_enqueue_script( 'archive_team_members_filter', JVTEAM_PLUGIN_URL . 'assets/js/archive_team_members_list_filter.js', array('jquery'), NULL, TRUE );
				
				if( $display_popup_onteam == 'on') {
					
					$jv_template_path_p =  get_stylesheet_directory() . '/divi-team-members';
					$jv_css_path_p = $jv_template_path_p.'/css/popup';
					$jv_css_url_p =  get_stylesheet_directory_uri().'/divi-team-members/css/popup'; 
					
					$jv_grid_path_p =  $jv_template_path_p.'/content-popup';
				
					if ( file_exists( $jv_css_path_p . '/popup-'.$divi_popupteam_select_style_layout.'.css' ) )
					{
						wp_enqueue_style('jv_team_module_members_popup_'.$divi_popupteam_select_style_layout, $jv_css_url_p . '/popup-'.$divi_popupteam_select_style_layout.'.css', array(), NULL);
					}else{
						wp_enqueue_style('jv_team_module_members_popup_'.$divi_popupteam_select_style_layout, JVTEAM_PLUGIN_URL .'assets/css/popup/popup-'.$divi_popupteam_select_style_layout.'.css', array(), NULL);	
					}
					
					wp_enqueue_style('jv_team_module_members_popup_custom', JVTEAM_PLUGIN_URL .'assets/css/popup/popup-custom.css', array(), NULL);		
					$divi_popupteam_select_style_layout = !empty(get_option('divi_popupteam_select_style_layout')) ? get_option('divi_popupteam_select_style_layout') : 'style1';
					$divi_team_close_background_color = !empty(get_option('divi_team_close_background_color')) ? get_option('divi_team_close_background_color') : '';
					$divi_team_close_color = !empty(get_option('divi_team_close_color')) ? get_option('divi_team_close_color') : ''; 
					$divi_popupteam_title_color = !empty(get_option('divi_popupteam_title_color')) ? get_option('divi_popupteam_title_color') : '';
					$divi_popupteam_position_color = !empty(get_option('divi_popupteam_position_color')) ? get_option('divi_popupteam_position_color') : '';
					$divi_popupteam_content_color = !empty(get_option('divi_popupteam_content_color')) ? get_option('divi_popupteam_content_color') : ''; 
					$divi_popupteam_information_color = !empty(get_option('divi_popupteam_information_color')) ? get_option('divi_popupteam_information_color') : '';
					$divi_popupteam_background_color = !empty(get_option('divi_popupteam_background_color')) ? get_option('divi_popupteam_background_color') : '';
					$divi_popupteam_border_width = !empty(get_option('divi_popupteam_border_width')) ? get_option('divi_popupteam_border_width') : '1'; 
					$divi_popupteam_border_color = !empty(get_option('divi_popupteam_border_color')) ? get_option('divi_popupteam_border_color') : '';
					$divi_popupteam_boxshadow_color = !empty(get_option('divi_popupteam_boxshadow_color')) ? get_option('divi_popupteam_boxshadow_color') : '';
					$divi_popupteam_icon_color = !empty(get_option('divi_popupteam_icon_color')) ? get_option('divi_popupteam_icon_color') : '';
					$divi_popupteam_horizontal_color = !empty(get_option('divi_popupteam_horizontal_color')) ? get_option('divi_popupteam_horizontal_color') : ''; 
					$divi_popupteam_contact_us_title = !empty(get_option('divi_popupteam_contact_us_title')) ? get_option('divi_popupteam_contact_us_title') : 'Drop Me a Line'; 
					
						if( $divi_popupteam_select_style_layout == 'style1'){
							  $popup_style1 = '';
							  if ( $divi_team_close_background_color != '' ){ $popup_style1 .= ".mfp-close-btn-in .jv_popup_style1 .mfp-close,.mfp-wrap .mfp-container .jv_popup_style1  button:hover{background-color: ".$divi_team_close_background_color." !important;}"; }
							  if ( $divi_team_close_color != '' ){ $popup_style1 .= ".mfp-close-btn-in .jv_popup_style1 .mfp-close,.mfp-wrap .mfp-container .jv_popup_style1  button:hover{color: ".$divi_team_close_color." !important;}"; }
							  if ( $divi_popupteam_background_color != '' ){ $popup_style1 .= ".jv_team_popup_style1{background-color: ".$divi_popupteam_background_color." !important;}"; }
							  if ( $divi_popupteam_title_color != '' ){  $popup_style1 .= ".jv_team_popup_style1 h4.jv_team_list_title{color: ".$divi_popupteam_title_color." !important;}"; }
							  if ( $divi_popupteam_position_color != '' ){ $popup_style1 .= ".jv_team_popup_style1 .jv_team_list_position{color: ".$divi_popupteam_position_color."  !important;}"; }
							  if ( $divi_popupteam_content_color != '' ){ $popup_style1 .= ".jv_team_popup_style1 .jv_team_list_content,.jv_team_popup_style1 .website_url,.jv_team_popup_style1 .contect_number,.jv_team_popup_style1 .email_address,.jv_team_popup_style1 .jv_team_list_address1{color: ".$divi_popupteam_content_color."  !important;}"; }
							  if ( $divi_popupteam_border_width != '' || $divi_popupteam_border_color !== ''){ $popup_style1 .= ".jv_team_popup_style1{border: ".$divi_popupteam_border_width."px solid ".$divi_popupteam_border_color." !important;}	"; }
							  if ( $divi_popupteam_icon_color != '' ){ $popup_style1 .= ".jv_team_popup_style1 .jv_team_member_icon_font.et-pb-icon,.jv_team_popup_style1 .jv_team_member_social_font.et-pb-icon{ color: ".$divi_popupteam_icon_color." !important;}"; }
							  if ( $divi_popupteam_information_color != '' ){ $popup_style1 .= ".jv_team_popup_style1 .website_url,.jv_team_popup_style1 .contect_number,.jv_team_popup_style1 .email_address,.jv_team_popup_style1 .jv_team_list_address1{color: ".$divi_popupteam_information_color." !important;}"; }
							  if ( $divi_popupteam_boxshadow_color != '' ){ $popup_style1 .= " .jv_team_popup_style1{ box-shadow: 0 20px 20px ".$divi_popupteam_boxshadow_color."  !important;}"; }
							  
							  wp_add_inline_style( 'jv_team_module_members_popup_custom', $popup_style1 );
						}
						
						if( $divi_popupteam_select_style_layout == 'style2'){
							  $popup_style2 = '';
							  if ( $divi_team_close_background_color != '' ){ $popup_style2 .= ".mfp-close-btn-in .jv_popup_style2 .mfp-close,.mfp-wrap .mfp-container .jv_popup_style2  button:hover{background-color: ".$divi_team_close_background_color." !important;}"; }
							  if ( $divi_team_close_color != '' ){ $popup_style2 .= ".mfp-close-btn-in .jv_popup_style2 .mfp-close,.mfp-wrap .mfp-container .jv_popup_style2  button:hover{color: ".$divi_team_close_color." !important;}"; }
							  if ( $divi_popupteam_background_color != '' ){ $popup_style2 .= ".jv_popup_style2 .et_pb_row{background-color: ".$divi_popupteam_background_color." !important;}"; }
							  if ( $divi_popupteam_title_color != '' ){  $popup_style2 .= ".jv_popup_style2 .jv_team_list_title h2{color: ".$divi_popupteam_title_color." !important;}"; }
							  if ( $divi_popupteam_position_color != '' ){ $popup_style2 .= ".jv_popup_style2 .jv_team_feture_box .jv_team_info h4{color: ".$divi_popupteam_position_color." !important;}"; }
							  if ( $divi_popupteam_content_color != '' ){ $popup_style2 .= ".jv_popup_style2 .jv_team_list_content{color: ".$divi_popupteam_content_color." !important;}"; }
							  if ( $divi_popupteam_icon_color != '' ){ $popup_style2 .= ".jv_popup_style2 .jv_team_member_info.et-pb-icon{ color: ".$divi_popupteam_icon_color."  !important;}.jv_popup_style2 .jv_team_list_title span,.jv_popup_style2 .jv_team_list_social_link{ background-color: ".$divi_popupteam_icon_color." !important;}"; }
							  if ( $divi_popupteam_information_color != '' ){ $popup_style2 .= ".jv_popup_style2 .jv_team_feture_box .jv_team_info span{color: ".$divi_popupteam_information_color." !important;}"; }
							  
							  wp_add_inline_style( 'jv_team_module_members_popup_custom', $popup_style2 );
						}
						
				}
			}
				$select_style   = $this->props['select_style'];
				$filter_style   = $this->props['filter_style'];
				$posts_number             = $this->props['posts_number'];
				$filter_all_label      = $this->props['filter_all_label'];
				$orderby                  = $this->props['orderby'];
				$category_orderby                  = $this->props['category_orderby'];
				$display_view_more_button  = $this->props['display_view_more_button'];
				$include_team_categories  = $this->props['include_categories'];
				$display_category_filter  = $this->props['display_category_filter'];
		 		$include_categories_array = explode(',',$include_team_categories);
				$link_open_in_new_tab      = $this->props['link_open_in_new_tab'];
				$style1_list_image_shadow_color  = $this->props['style1_list_image_shadow_color'];	
				$style1_list_image_shadow_hover_color  = $this->props['style1_list_image_shadow_hover_color'];	
				$style1_list_box_shadow  = $this->props['style1_list_box_shadow'];	
				$style1_list_box_shadow_hover  = $this->props['style1_list_box_shadow_hover'];	
				$style1_list_bg_content_color  = $this->props['style1_list_bg_content_color'];
				$style1_list_discription_bottom_border  = $this->props['style1_list_discription_bottom_border'];
				$style1_button_active_color  = $this->props['style1_button_active_color'];
				$style1_button_color  = $this->props['style1_button_color'];
				$style2_list_bg_content_color  = $this->props['style2_list_bg_content_color'];
				$style2_list_image_border_color  = $this->props['style2_list_image_border_color'];
				$style2_list_boxshadow_color  = $this->props['style2_list_boxshadow_color'];
				$style2_list_boxshadow_hover_color  = $this->props['style2_list_boxshadow_hover_color'];
				$style2_list_discription_bottom_border  = $this->props['style2_list_discription_bottom_border'];
				$style2_list_position_bottom_border  = $this->props['style2_list_position_bottom_border'];
				$style2_button_active_color = $this->props['style2_button_active_color'];
				$style2_button_animation_border_color = $this->props['style2_button_animation_border_color'];
				$style2_button_hover_color = $this->props['style2_button_hover_color'];
				$style3_list_bg_content_color  = $this->props['style3_list_bg_content_color'];
				$style3_list_border_color  = $this->props['style3_list_border_color'];
				$style3_list_boxshadow_color  = $this->props['style3_list_boxshadow_color'];
				$style3_list_boxshadow_hover_color  = $this->props['style3_list_boxshadow_hover_color'];
				$style3_button_border_color  = $this->props['style3_button_border_color'];
				$style3_button_color  = $this->props['style3_button_color'];
				$style3_button_hover_color  = $this->props['style3_button_hover_color'];
				$style4_list_bg_content_color  = $this->props['style4_list_bg_content_color'];
				$style4_list_box_border_color  = $this->props['style4_list_box_border_color'];
				$style4_list_box_shadow_color  = $this->props['style4_list_box_shadow_color'];
				$style4_list_box_shadow_hover_color  = $this->props['style4_list_box_shadow_hover_color'];
				$style4_button_border_color  = $this->props['style4_button_border_color'];
				$style4_button_color  = $this->props['style4_button_color'];
				$style5_button_border_active_color  = $this->props['style5_button_border_active_color'];
				$style5_button_border_color  = $this->props['style5_button_border_color'];
				$style6_button_active_color  = $this->props['style6_button_active_color'];
				$style6_button_color  = $this->props['style6_button_color'];
				$style7_button_border_active_color  = $this->props['style7_button_border_active_color'];
				$style7_button_border_color  = $this->props['style7_button_border_color'];
				$style7_discription_bg_color  = $this->props['style7_discription_bg_color'];
				$style7_content_bg_color  = $this->props['style7_content_bg_color'];
				$style7_border_color  = $this->props['style7_border_color'];		
				$style8_button_active_color  = $this->props['style8_button_active_color'];
				$style8_button_border_color  = $this->props['style8_button_border_color'];
				$style9_button_active_color  = $this->props['style9_button_active_color'];
				$style9_button_color  = $this->props['style9_button_color'];
				$style10_button_active_border_color  = $this->props['style10_button_active_border_color'];
				$style10_button_color  = $this->props['style10_button_color'];
				$style11_button_active_color  = $this->props['style11_button_active_color'];
				$style11_button_color  = $this->props['style11_button_color'];
				$style12_button_active_border_color  = $this->props['style12_button_active_border_color'];
				$style12_button_border_color  = $this->props['style12_button_border_color'];
				$style13_button_active_color = $this->props['style13_button_active_color'];
				$style13_button_active_hover_color = $this->props['style13_button_active_hover_color'];
				$style13_button_hover_color = $this->props['style13_button_hover_color'];
				$style14_button_active_color = $this->props['style14_button_active_color'];
				$style14_button_color = $this->props['style14_button_color'];
				$style15_button_active_color = $this->props['style15_button_active_color'];
				$style15_button_active_hover_color = $this->props['style15_button_active_hover_color'];
				$style15_button_hover_color = $this->props['style15_button_hover_color'];
				$style16_button_active_color = $this->props['style16_button_active_color'];
				$style16_button_hover_color = $this->props['style16_button_hover_color'];
				$style16_button_border_color = $this->props['style16_button_border_color'];
				$style17_button_active_color = $this->props['style17_button_active_color'];
				$style17_button_hover_color = $this->props['style17_button_hover_color'];
				$style17_button_border_color = $this->props['style17_button_border_color'];
				$style18_button_border_color = $this->props['style18_button_border_color'];
				$style18_button_color = $this->props['style18_button_color'];
				$style19_button_gardien1_color = $this->props['style19_button_gardien1_color'];
				$style19_button_gardien2_color = $this->props['style19_button_gardien2_color'];
				$style19_button_gardien3_color = $this->props['style19_button_gardien3_color'];
				$style19_button_gardien4_color = $this->props['style19_button_gardien4_color'];
				$style19_button_active_border_color = $this->props['style19_button_active_border_color'];
				$style20_button_border_color = $this->props['style20_button_border_color'];
				$social_link_text_color  = $this->props['social_link_text_color'];
				$social_link_text_hover_color  = $this->props['social_link_text_hover_color'];
				$display_detail_page_link_type  = $this->props['display_detail_page_link_type'];
				$display_detail_page  = $this->props['display_detail_page'];
				$icon_color  = $this->props['icon_color'];
				$display_category  		  = $this->props['display_category'];
				$display_social_link      = $this->props['display_social_link'];
 				$team_members_display_pagination = $this->props['team_members_display_pagination'];
				$buttons_text_color = $this->props['buttons_text_color'];
				$button_text_hover_color = $this->props['button_text_hover_color'];
				$read_more_text_btn = $this->props['read_more_text_btn'];
				$module_id = $this->props['module_id'];
				$module_class = $this->props['module_class'];
				$module_class = ET_Builder_Element::add_module_order_class( $module_class, $render_slug );
				$module_id = ( '' !== $module_id ? sprintf( ' id="%1$s"', esc_attr( $module_id ) ) : '' );
				$module_class = ( '' !== $module_class ? sprintf( ' %1$s', esc_attr( $module_class ) ) : '' );
				/*<-------------------------- Button text color comman ------------------------->*/
				if( $buttons_text_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.jv_span li a span',
						'declaration' => sprintf('color: %1$s !important;',esc_html( $buttons_text_color )),
					) );
				}
				if( $button_text_hover_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.jv_span li a:hover span',
						'declaration' => sprintf('color: %1$s !important;',esc_html( $button_text_hover_color )),
					) );
				}
				/*<------------------------ Button filter ftyle 1 to 20 ----------------------->*/
				if ( $filter_style == 'fstyle1' ){
						if( $style1_button_active_color !='' ){
							ET_Builder_Element::set_style( $render_slug, array(
								'selector'    => '.et_pb_jv_team_members_category_list.fstyle1 li a:hover,.et_pb_jv_team_members_category_list.fstyle1 li a.active',
								'declaration' => sprintf('background-color: %1$s !important;',esc_html( $style1_button_active_color )),
							) );
						}
						if( $style1_button_color !='' ){
							ET_Builder_Element::set_style( $render_slug, array(
								'selector'    => '.et_pb_jv_team_members_category_list.fstyle1 li a',
								'declaration' => sprintf('background-color: %1$s !important;',esc_html( $style1_button_color )),
							) );
						}
				} 
				if( $filter_style == 'fstyle2' ) {
					if( $style2_button_active_color !='' ){
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle2 li a.active',
							'declaration' => sprintf('background-color: %1$s !important;border: 1px solid %1$s !important;',esc_html( $style2_button_active_color )),
						) );
					}
					if( $style2_button_animation_border_color !='' ) {
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle2 li a:before, .et_pb_jv_team_members_category_list.fstyle2 li a:after',
							'declaration' => sprintf('background: %1$s !important;',esc_html( $style2_button_animation_border_color )),
						) );
					}
					if( $style2_button_hover_color !='' ) {
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle2 li a:hover',
							'declaration' => sprintf('border: 1px solid %1$s !important;background: %1$s !important;',esc_html( $style2_button_hover_color )),
						) );
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle2 li a',
							'declaration' => sprintf('border: 1px solid %1$s !important;',esc_html( $style2_button_hover_color )),
						) );
					} 
				}	
				if( $filter_style == 'fstyle3' ) {
					if( $style3_button_border_color !='' ) {
						ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle3 li a.active',
						'declaration' => sprintf('border-color: %1$s !important;background-color: #fff  !important;',esc_html( $style3_button_border_color )),
						) );
					}
					if( $style3_button_color !='' ) {
						ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle3 li a',
						'declaration' => sprintf('border: 1px solid %1$s !important;background: %1$s !important;',esc_html( $style3_button_color )),
						) );
					}
					if( $style3_button_hover_color !='' ) {
						ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle3 li a:hover',
						'declaration' => sprintf('background-color: %1$s !important;',esc_html( $style3_button_hover_color )),
						) );
					}
				} 
				if( $filter_style == 'fstyle4' ) {	
					if( $style4_button_color !='' ) {
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle4 li a:before',
							'declaration' => sprintf('background: %1$s !important;',esc_html( $style4_button_color )),
						) );
					}
					if( $style4_button_border_color !='' ) {
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle4 li a.active, .et_pb_jv_team_members_category_list.fstyle4 li a',
							'declaration' => sprintf('border: 2px solid %1$s !important;',esc_html( $style4_button_border_color )),
						) );
					}	
					ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle4 li a.active:before',
							'declaration' => sprintf('background-color: %1$s !important;','#fff'),
					) );
				} 
				if( $filter_style == 'fstyle5' ) {
					if( $style5_button_border_active_color !='' ){
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle5 li a.active:after, .et_pb_jv_team_members_category_list.fstyle5 li a.active:before',
							'declaration' => sprintf('background-color: %1$s !important;',esc_html( $style5_button_border_active_color )),
						) );	
					}
					if( $style5_button_border_color !='' ){
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle5 li a:before, .et_pb_jv_team_members_category_list.fstyle5 li a:after',
							'declaration' => sprintf('background-color: %1$s !important;',esc_html( $style5_button_border_color )),
						) );
					}
				} 
				if( $filter_style == 'fstyle6' ) {
					if( $style6_button_active_color !='' ){
						ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle6 li a.active',
						'declaration' => sprintf('background: %1$s !important;',esc_html( $style6_button_active_color )),
						) );
						ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle6 li a.active:after',
						'declaration' => sprintf('border: 2px solid %1$s !important;',esc_html( $style6_button_active_color )),
						) );
					}
					if( $style6_button_color !='' ){
						ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle6 li a',
						'declaration' => sprintf('background-color: %1$s !important;',esc_html( $style6_button_color )),
						) );
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle6 li a:after',
							'declaration' => sprintf('border: 2px solid %1$s !important;',esc_html( $style6_button_color )),
						) );
					}
				} 
				if( $filter_style == 'fstyle7' ) {
					if( $style7_button_border_active_color !='' ){
						ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle7 li a.active',
						'declaration' => sprintf('border-left: 3px solid %1$s !important;border-right: 3px solid %1$s !important;',esc_html( $style7_button_border_active_color )),
						) );
						ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle7 li a.active:before,.et_pb_jv_team_members_category_list.fstyle7 li a.active:after',
						'declaration' => sprintf('background-color: %1$s !important;',esc_html( $style7_button_border_active_color )),
						) );
					}
					if( $style7_button_border_color !='' ){
						ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle7 li a',
						'declaration' => sprintf('border-left: 3px solid %1$s !important;border-right: 3px solid %1$s !important;',esc_html( $style7_button_border_color )),
						) );
						ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle7 li a:before,.et_pb_jv_team_members_category_list.fstyle7 li a:after',
						'declaration' => sprintf('background: %1$s !important;',esc_html( $style7_button_border_color )),
						) );
					}
				}  
				if( $filter_style == 'fstyle8' ) {
					if( $style8_button_active_color !='' ){
						ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle8 li a:after',
						'declaration' => sprintf('background:  %1$s !important;',esc_html( $style8_button_active_color )),
						) );
						ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle8 li a.active',
						'declaration' => sprintf('background:  %1$s !important;border: 2px solid %1$s !important;',esc_html( $style8_button_active_color )),
						) );
					}
					if( $style8_button_border_color !='' ) {
						ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle8 li a',
						'declaration' => sprintf('border: 2px solid %1$s !important;',esc_html( $style8_button_border_color )),
						) );
					}
				} 
				if( $filter_style == 'fstyle9' ) {
					if( $style9_button_active_color !='' ) {
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle9 li a.active',
							'declaration' => sprintf('background: %1$s !important;',esc_html( $style9_button_active_color )),
						) );
					}
					if( $style9_button_color !='' ) {
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle9 li a',
							'declaration' => sprintf('background: %1$s !important;',esc_html( $style9_button_color )),
						) );
					}
				}  
				if( $filter_style == 'fstyle10' ) {
					if( $style10_button_active_border_color !='') {
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle10 li a.active:before, .et_pb_jv_team_members_category_list.fstyle10 li a.active:after,.et_pb_jv_team_members_category_list.fstyle10 li a.active:hover:before,.et_pb_jv_team_members_category_list.fstyle10 li a.active :hover:after',
							'declaration' => sprintf('border-color:%1$s !important;',esc_html( $style10_button_active_border_color )),
						) );
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle10 li a.active,.et_pb_jv_team_members_category_list.fstyle10 li a.active:hover',
							'declaration' => sprintf('border: 2px solid %1$s !important;',esc_html( $style10_button_active_border_color )),
						) );
					}
					if( $style10_button_color !='') {
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle10 li a:before,.et_pb_jv_team_members_category_list.fstyle10 li a:after,.et_pb_jv_team_members_category_list.fstyle10 li a:hover:before,.et_pb_jv_team_members_category_list.fstyle10 li a :hover:after',
							'declaration' => sprintf('border-color:%1$s !important;',esc_html( $style10_button_color )),
						) );
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle10 li a, .et_pb_jv_team_members_category_list.fstyle10 li a:hover',
							'declaration' => sprintf('border: 2px solid %1$s !important;',esc_html( $style10_button_color )
							),
						) );
					}
				} 
				if( $filter_style == 'fstyle11' ) {
					if( $style11_button_active_color !='') {
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle11 li a.active',
							'declaration' => sprintf('background: %1$s !important;border: 2px solid %1$s !important;',esc_html( $style11_button_active_color )),
							) );
					} 
					if( $style11_button_color !='') {
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle11 li a:hover,.et_pb_jv_team_members_category_list.fstyle11 li a:before,.et_pb_jv_team_members_category_list.fstyle11 li a:after',
							'declaration' => sprintf('background: %1$s !important;',esc_html( $style11_button_color )),
							) );
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle11 li a',
							'declaration' => sprintf('border: 2px solid %1$s !important;',esc_html( $style11_button_color )),
						) );
					}
				}
				if( $filter_style == 'fstyle12' ) {
					if( $style12_button_active_border_color !='' ){
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle12 li a.active:before,.et_pb_jv_team_members_category_list.fstyle12 li a.active:after,.et_pb_jv_team_members_category_list.fstyle12 li a.active span:before,.et_pb_jv_team_members_category_list.fstyle12 li a.active span:after',
							'declaration' => sprintf('background: %1$s !important;',esc_html( $style12_button_active_border_color )),
						) );
					}
					if( $style12_button_border_color !='' ){
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle12 li a:before,.et_pb_jv_team_members_category_list.fstyle12 li a:after,.et_pb_jv_team_members_category_list.fstyle12 li a span:before,.et_pb_jv_team_members_category_list.fstyle12 li a span:after',
							'declaration' => sprintf('background: %1$s !important;',esc_html( $style12_button_border_color )),
						) );
					}
				} 
				if( $filter_style == 'fstyle13' ) {
					if( $style13_button_active_color !='' ) {
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle13 li a.active',
							'declaration' => sprintf('background: %1$s !important;border: 1px solid %1$s !important;',esc_html( $style13_button_active_color )),
						) );
					}
					if( $style13_button_active_hover_color !='' ) {
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle13 li a.active:before, .et_pb_jv_team_members_category_list.fstyle13 li a.active:after, .et_pb_jv_team_members_category_list.fstyle13 li a.active span:before, .et_pb_jv_team_members_category_list.fstyle13 li a.active span:after',
							'declaration' => sprintf('background: %1$s !important;',esc_html( $style13_button_active_hover_color )),
						) );
					}
					if( $style13_button_hover_color !='' ) {
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle13 li a:before, .et_pb_jv_team_members_category_list.fstyle13 li a:after, .et_pb_jv_team_members_category_list.fstyle13 li a span:before, .et_pb_jv_team_members_category_list.fstyle13 li a span:after',
							'declaration' => sprintf('background: %1$s !important;',esc_html( $style13_button_hover_color )),
						) );
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle13 li a',
							'declaration' => sprintf('border: 1px solid %1$s !important;',esc_html( $style13_button_hover_color )),
						) );
					}
				} 
				if( $filter_style == 'fstyle14' ) {
					if( $style14_button_active_color !='' ) {
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle14 li a.active:after',
							'declaration' => sprintf('box-shadow: 0 0 0 2px %1$s !important;',esc_html( $style14_button_active_color )),
						) );
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle14 li a.active',
							'declaration' => sprintf('background: %1$s !important;',esc_html( $style14_button_active_color )),
						) );
					}	
					if( $style14_button_color !='' ) {
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle14 li a',
							'declaration' => sprintf('background: %1$s !important;',esc_html( $style14_button_color )),
						) );
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle14 li a:after',
							'declaration' => sprintf('box-shadow: 0 0 0 2px %1$s !important;',esc_html( $style14_button_color )),
						) );
					}
				} 
				if( $filter_style == 'fstyle15' ) {
					if( $style15_button_active_color !='' ) {
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle15 li a.active',
							'declaration' => sprintf('background: %1$s !important;',esc_html( $style15_button_active_color )),
						) );
					}
					if( $style15_button_active_hover_color !='' ) {
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle15 li a.active:before, .et_pb_jv_team_members_category_list.fstyle15 li a.active:after, .et_pb_jv_team_members_category_list.fstyle15 li a.active span:before, .et_pb_jv_team_members_category_list.fstyle15 li a.active span:after',
								'declaration' => sprintf('background: %1$s !important;',esc_html( $style15_button_active_hover_color )),
						) );
					}
					if( $style15_button_hover_color !='' ) {
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle15 li a',
							'declaration' => sprintf('border: 1px solid %1$s !important;',esc_html( $style15_button_hover_color )),
						) );
						ET_Builder_Element::set_style( $render_slug, array(
								'selector'    => '.et_pb_jv_team_members_category_list.fstyle15 li a:before, .et_pb_jv_team_members_category_list.fstyle15 li a:after, .et_pb_jv_team_members_category_list.fstyle15 li a span:before, .et_pb_jv_team_members_category_list.fstyle15 li a span:after',
								'declaration' => sprintf('background:  %1$s !important;',esc_html( $style15_button_hover_color )),
						) );
					}
				} 
				if( $filter_style == 'fstyle16' ) {
					if( $style16_button_active_color !=''){
						ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle16 li a.active, .et_pb_jv_team_members_category_list.fstyle16 li a.active:before, .et_pb_jv_team_members_category_list.fstyle16 li a.active:after, .et_pb_jv_team_members_category_list.fstyle16 li a.active span:before, .et_pb_jv_team_members_category_list.fstyle16 li a.active span:after',
						'declaration' => sprintf('background: %1$s !important;',esc_html( $style16_button_active_color )),
						) );
					}
					if( $style16_button_hover_color !='') {
						ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle16 li a:before, .et_pb_jv_team_members_category_list.fstyle16 li a:after, .et_pb_jv_team_members_category_list.fstyle16 li a span:before, .et_pb_jv_team_members_category_list.fstyle16 li a span:after',
						'declaration' => sprintf('background: %1$s !important;',esc_html( $style16_button_hover_color )),
						) );
					}
					if( $style16_button_border_color !='') {
						ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle16 li a',
						'declaration' => sprintf('border: 1px solid %1$s !important;',esc_html( $style16_button_border_color )),
						) );
					}
				} 
				if( $filter_style == 'fstyle17' ) {
					if( $style17_button_active_color !='' ) {
							ET_Builder_Element::set_style( $render_slug, array(
								'selector'    => '.et_pb_jv_team_members_category_list.fstyle17 li a.active',
								'declaration' => sprintf('background: %1$s !important;',esc_html( $style17_button_active_color )),
							) );
						}
					if( $style17_button_hover_color !='' ){
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle17 li a:hover',
							'declaration' => sprintf('background: %1$s !important;',esc_html( $style17_button_hover_color )),
						) );
					}
					if( $style17_button_border_color !='' ){
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle17 li a',
							'declaration' => sprintf('border: 2px solid %1$s !important;',esc_html( $style17_button_border_color )),
						) );
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle17 li a:before',
							'declaration' => sprintf('border-top: 2px solid %1$s !important;border-left: 2px solid %1$s !important;',esc_html( $style17_button_border_color )),
						) );
						ET_Builder_Element::set_style( $render_slug, array(
								'selector'    => '.et_pb_jv_team_members_category_list.fstyle17 li a:after',
								'declaration' => sprintf('border-bottom: 2px solid %1$s !important;border-right: 2px solid %1$s !important;',esc_html( $style17_button_border_color )),
						) );
					}
				} 
				if( $filter_style == 'fstyle18' ) {
					if( $style18_button_border_color !=''){
						ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle18 li a.active',
						'declaration' => sprintf('border: 2px solid %1$s !important;',esc_html( $style18_button_border_color )),
						) );	
					}
					if( $style18_button_color !=''){
						ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle18 li a:before, .et_pb_jv_team_members_category_list.fstyle18 li a:after',
						'declaration' => sprintf('background: %1$s !important;',esc_html( $style18_button_color )),
						) );	
					}
				} 
				if( $filter_style == 'fstyle19' ) {
					if(( $style19_button_gardien1_color !='') && ( $style19_button_gardien2_color !='') && ( $style19_button_gardien3_color !='') &&  ( $style19_button_gardien4_color !='') ){
						ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle19 li a',
						'declaration' => sprintf('background-image: linear-gradient(to top, %1$s 0px, %2$s 10px, %3$s 10px, %4$s 100px) !important;',esc_html( $style19_button_gardien1_color ),esc_html( $style19_button_gardien2_color ),esc_html( $style19_button_gardien3_color ),esc_html( $style19_button_gardien4_color )),
						) );	
					}
					if( $style19_button_active_border_color !=''){
						ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle19 li a:after',
						'declaration' => sprintf('background: %1$s !important;',esc_html( $style19_button_active_border_color )),
						) );	
					}
				}	
				if( $filter_style == 'fstyle20' ) {
					if( $style20_button_border_color !=''){
						ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle20 li a, .et_pb_jv_team_members_category_list.fstyle20 li a:before, .et_pb_jv_team_members_category_list.fstyle20 li a:after',
						'declaration' => sprintf('border-color: %1$s !important;',esc_html( $style20_button_border_color )),
						) );	
						ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle20 li a:hover:after',
						'declaration' => sprintf('border-color: %1$s !important;',esc_html( $style20_button_border_color )),
						) );
						ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle20 li a:hover:before',
						'declaration' => sprintf('border-color: %1$s !important;',esc_html( $style20_button_border_color )),
						) );
					}
				}
			/********************************************* Style 1 to 20 Css Class***********************************************/
			if( $social_link_text_color !='' ){
				ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '%%order_class%% .jv_team_member_social_font.et-pb-icon',
							'declaration' => sprintf('color: %1$s;',esc_html( $social_link_text_color )),
						) );
			} 
			if ( $social_link_text_hover_color !='' ) {
				ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '%%order_class%% .jv_team_member_social_font.et-pb-icon:hover',
							'declaration' => sprintf('color: %1$s;',esc_html( $social_link_text_hover_color )),
				) );
			} 
			if( $icon_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '%%order_class%% .jv_team_member_icon_font.et-pb-icon',
							'declaration' => sprintf('color: %1$s',esc_html( $icon_color )),
					) );
			}
			if ( $select_style == 'style1' ){
					if( $style1_list_image_shadow_color !='' ){
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '%%order_class%%.et_pb_jv_team_members_list_style1 .jv_team_list_member_image img',
							'declaration' => sprintf('box-shadow: 0 0px 20px %1$s',esc_html( $style1_list_image_shadow_color )),
						) );
					}
					if( $style1_list_image_shadow_hover_color !='' ){
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '%%order_class%%.et_pb_jv_team_members_list_style1 .jv_team_list_member_image img:hover',
							'declaration' => sprintf('box-shadow: 0 3px 30px %1$s;',esc_html( $style1_list_image_shadow_hover_color )),
						) );
					}
					if( $style1_list_bg_content_color !='' ){
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '%%order_class%%.et_pb_jv_team_members_list_style1 .jv_team_list_description',
							'declaration' => sprintf('background-color: %1$s;',esc_html( $style1_list_bg_content_color )),
						) );
					}
					if( $style1_list_discription_bottom_border !='' ){
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '%%order_class%%.et_pb_jv_team_members_list_style1 .jv_team_list_description_border',
							'declaration' => sprintf('border-bottom: 10px solid %1$s;',esc_html( $style1_list_discription_bottom_border )),
						) );
					}
					if( $style1_list_box_shadow !='' ){
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '%%order_class%%.et_pb_jv_team_members_list_style1 .jv_team_list_description',
							'declaration' => sprintf('box-shadow: 0 5px 20px %1$s;',esc_html( $style1_list_box_shadow )),
						) );
					}
					if( $style1_list_box_shadow_hover !='' ){
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '%%order_class%%.et_pb_jv_team_members_list_style1 .jv_team_list_description:hover',
							'declaration' => sprintf('box-shadow: 0 5px 30px %1$s;',esc_html( $style1_list_box_shadow_hover )),
						) );
					}
			}  
			if ( $select_style == 'style2' ) {
					if( $style2_list_bg_content_color !='' ){
							ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '%%order_class%%.et_pb_jv_team_members_list_style2 .jv_team_list_member',
							'declaration' => sprintf('background-color: %1$s;',esc_html( $style2_list_bg_content_color )),
							) );
					}
					if( $style2_list_image_border_color !='' ){
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '%%order_class%%.et_pb_jv_team_members_list_style2 .jv_team_list_member_image',
							'declaration' => sprintf('border: 10px solid %1$s;',esc_html( $style2_list_image_border_color )),
						) );
					}	
					if( $style2_list_image_border_color !='' ){
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '%%order_class%%.et_pb_jv_team_members_list_style2 .jv_team_list_description',
							'declaration' => sprintf('border-bottom: 10px solid %1$s;',esc_html( $style2_list_image_border_color )),
						) );
					}
					if( $style2_list_image_border_color !='' ){
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '%%order_class%%.et_pb_jv_team_members_list_style2 .jv_team_list_member .jv_team_list_position',
							'declaration' => sprintf('border-bottom: 1px solid %1$s;',esc_html( $style2_list_image_border_color )),
						) );
					}
					if( $style2_list_boxshadow_color !='' ){
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '%%order_class%%.et_pb_jv_team_members_list_style2 .jv_team_list_member',
							'declaration' => sprintf('box-shadow: 0 4px 20px %1$s;',esc_html( $style2_list_boxshadow_color )),
						) );
					}
					if( $style2_list_boxshadow_hover_color !='' ){
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '%%order_class%%.et_pb_jv_team_members_list_style2 .jv_team_list_member:hover',
							'declaration' => sprintf('box-shadow: 0 4px 30px %1$s;',esc_html( $style2_list_boxshadow_hover_color )),
						) );
					}
			} 
			if ( $select_style == 'style3' ) {
					if(( $style3_list_bg_content_color !='' ) || ( $style3_list_boxshadow_color !='' )) {
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '%%order_class%%.et_pb_jv_team_members_list_style3 .jv_team_list_member',
							'declaration' => sprintf('background-color: %1$s;box-shadow: 0 5px 20px %2$s;',esc_html( $style3_list_bg_content_color ),esc_html( $style3_list_boxshadow_color )),
						) );	
					}
					if( $style3_list_border_color !='' ){
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '%%order_class%%.et_pb_jv_team_members_list_style3 .jv_team_list_member .jv_team_list_member_image img',
							'declaration' => sprintf('border: 1px solid %1$s',esc_html( $style3_list_border_color )),
						) );
					}
					if( $style3_list_border_color !='' ){
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '%%order_class%%.et_pb_jv_team_members_list_style3 .jv_team_list_member .jv_team_list_description_border',
							'declaration' => sprintf('border-left: 1px solid %1$s;',esc_html( $style3_list_border_color )),
						) );
					}
					if( $style3_list_boxshadow_hover_color !='' ){	
						 ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '%%order_class%%.et_pb_jv_team_members_list_style3 .jv_team_list_member:hover',
							'declaration' => sprintf('box-shadow: 0 5px 30px %1$s;',esc_html( $style3_list_boxshadow_hover_color )),
						 ) );	
					} 
				} 
				if ( $select_style == 'style4' ) {
					if(( $style4_list_bg_content_color !='' ) || ( $style4_list_box_border_color !='' ) || ( $style4_list_box_shadow_color !='' )){	
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '%%order_class%%.et_pb_jv_team_members_list_style4 .jv_team_list_member',
							'declaration' => sprintf('background-color: %1$s;border: 5px solid %2$s;box-shadow: 0 4px 20px %3$s;',esc_html( $style4_list_bg_content_color ),esc_html( $style4_list_box_border_color ),esc_html( $style4_list_box_shadow_color )),
						) );
					}
					if( $style4_list_box_border_color !='') {
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '%%order_class%%.et_pb_jv_team_members_list_style4 .jv_team_list_social_link',
							'declaration' => sprintf('border-bottom: 1px solid %1$s;',esc_html( $style4_list_box_border_color )),
						) );
					}
					if( $style4_list_box_shadow_hover_color !='') {
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '%%order_class%%.et_pb_jv_team_members_list_style4 .jv_team_list_member:hover',
							'declaration' => sprintf('box-shadow: 0 5px 30px %1$s;',esc_html( $style4_list_box_shadow_hover_color )),
						) ); 
					}
				}
				if ( $select_style == 'style7' ) {	
					if( $style7_discription_bg_color !='') {
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '%%order_class%%.et_pb_jv_team_members_list_style7 .et_pb_jv_team_members_list_column',
							'declaration' => sprintf('background: %1$s;',esc_html( $style7_discription_bg_color )),
						) );
					}
					if( $style7_content_bg_color !='') {
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '%%order_class%%.et_pb_jv_team_members_list_style7 .et_pb_jv_team_members_list_column .second_row',
							'declaration' => sprintf('background: %1$s;',esc_html( $style7_content_bg_color )),
						) );
					}
					if( $style7_border_color !='') {
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '%%order_class%%.et_pb_jv_team_members_list_style7 h3.jv_team_list_title',
							'declaration' => sprintf('border-bottom: 2px solid %1$s;',esc_html( $style7_border_color )),
						) );
					}
				}
				
			
				/********************************************Order By ASC And DESC*********************************************/
			    if ( $posts_number == ''){
					$args = array( 'posts_per_page' => -1 );
				}else{
					$args = array( 'posts_per_page' => (int) $posts_number );
				}
				$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
				if ( 'date_desc' !== $orderby ) {
					switch( $orderby ) {
						case 'date_asc' :
							$args['orderby'] = 'date';
							$args['order'] = 'ASC';
							break;
						case 'title_asc' :
							$args['orderby'] = 'title';
							$args['order'] = 'ASC';
							break;
						case 'title_desc' :
							$args['orderby'] = 'title';
							$args['order'] = 'DESC';
							break;
						case 'rand' :
							$args['orderby'] = 'rand';
							break;
						case 'menu_order_desc' :
							$args['orderby'] = 'menu_order';
							$args['order'] = 'DESC';
							break;
						case 'menu_order_asc' :
							$args['orderby'] = 'menu_order';
							$args['order'] = 'ASC';
							break;
					}
				}else{
					$args['orderby'] = 'date';
					$args['order'] = 'DESC';
				}
				$args['post_type'] = 'jv_team_members';
				$args['paged'] = $paged;
				$archive_jv_team_output = '';
				$archive_jv_team_list_class = "archive_jv_team_view_list";
				/*Filter Option*/
				if ( 'id_desc' !== $category_orderby ) {
					switch( $category_orderby ) {
						case 'id_asc' :
							$orderby = 'id';
							$order = 'ASC';
							break;
						case 'name_asc' :
							$orderby = 'name';
							$order = 'ASC';
							break;
						case 'name_desc' :
							$orderby = 'name';
							$order = 'DESC';
							break;
						case 'count' :
							$orderby = 'count';
							break;
						case 'slug_desc' :
							$orderby = 'slug';
							$order = 'DESC';
							break;
						case 'slug_asc' :
							$orderby = 'slug';
							$order = 'ASC';
							break;
					}
				}else{
					$orderby = 'id';
					$order = 'DESC';
				}
				$archive_department_category_terms = get_terms( array(
					'taxonomy' => 'department_category',
					'hide_empty' => false,
					'orderby'           => $orderby, 
		    		'order'             => $order,
				) );
				if($display_category_filter == 'off' ){		
					if ( $display_category != 'all' ){
						$args['tax_query'] = array(
							array(
								'taxonomy' => 'department_category',
								'field' => 'term_id',
								'terms' => explode(",", $include_team_categories),
								'operator' => 'IN'
							)
						);
					}
				}
				if($display_category_filter == 'on' ){
					 if ( $display_category == 'all'){
						$archive_jv_team_output .= '<ul class="et_pb_jv_team_members_category_list jv_span '.$filter_style.' "><li>
											<a href="javascript:void(0)" class="jv_filter active" data-filter=".all" ><span>'.$filter_all_label.'</span></a></li>';
						$archive_department_category_count = count($archive_department_category_terms);
						if ( $archive_department_category_count > 0 ){
						foreach($archive_department_category_terms as $department_categoryval){
								$archive_jv_team_output .= '<li><a href="javascript:void(0)" class="jv_filter" data-filter=".cat-'.$department_categoryval->term_id.'" ><span>'.$department_categoryval->name.'</span></a></li>';
							}
						}
						$archive_jv_team_output .= '</ul>';
					}else{
						if ( $include_team_categories != '' ){
							$archive_jv_team_output .= '<ul class="et_pb_jv_team_members_category_list '.$filter_style.' "><li>
												<a href="javascript:void(0)" class="jv_filter active" data-filter=".all" ><span>'.$filter_all_label.'</span></a></li>';
							$archive_department_category_count = count($archive_department_category_terms);
						if ( $archive_department_category_count > 0 ){
							foreach($archive_department_category_terms as $department_categoryval){
									if  ( in_array($department_categoryval->term_id,$include_categories_array)){
										$archive_jv_team_output .= '<li><a href="javascript:void(0)" class="jv_filter" data-filter=".cat-'.$department_categoryval->term_id.'" ><span>'.$department_categoryval->name.'</span></a></li>';
									}
								}
							}
							$archive_jv_team_output .= '</ul>';
						}
					}
				}
				/*Filter Option*/
				$archive_jv_team_output .= '<div class="et_pb_module et_pb_jv_team_members_list et_pb_jv_team_members_list_'.$select_style.' '.$module_class.' '.$archive_jv_team_list_class.'" '.$module_id.'>';
				$archive_jv_team_query = new WP_Query( $args );
				if ( $archive_jv_team_query->have_posts() ) { 
					$jv_template_path =  get_stylesheet_directory() . '/divi-team-members';
					$jv_css_path = $jv_template_path.'/css/archive_team_members_list_css';
					$jv_css_url =  get_stylesheet_directory_uri().'/divi-team-members/css/archive_team_members_list_css'; 
					
					$jv_grid_path =  $jv_template_path.'/content-archive-team-list';
					
					if ( file_exists( $jv_css_path . '/jv_team_module_list_'.$select_style.'.css' ) )
					{
						wp_enqueue_style('archive_team_members_list_css'.$select_style, $jv_css_url . '/jv_team_module_list_'.$select_style.'.css', array(), NULL);
					}else{
						wp_enqueue_style('archive_team_members_list_css'.$select_style, JVTEAM_PLUGIN_URL .'assets/css/archive_team_members_list_css/jv_team_module_list_'.$select_style.'.css', array(), NULL);
					}
						while ( $archive_jv_team_query->have_posts() ) { $archive_jv_team_query->the_post();
						   ob_start();
						    $jv_team_designation = get_post_meta( get_the_ID(),'jv_team_designation', true );
							$jv_team_facebook = get_post_meta( get_the_ID(),'jv_team_facebook', true );
							$jv_team_twitter = get_post_meta( get_the_ID(),'jv_team_twitter', true );
							$jv_team_google = get_post_meta( get_the_ID(),'jv_team_google', true );
							$jv_team_linkdin = get_post_meta( get_the_ID(),'jv_team_linkedin', true );
							$jv_team_instagram = get_post_meta( get_the_ID(),'jv_team_instagram', true );
							$jv_team_mobile_number = get_post_meta( get_the_ID(),'jv_team_phone_number', true );
							$jv_team_email_address = get_post_meta( get_the_ID(),'jv_team_email_address', true );
							$jv_team_website_url = get_post_meta( get_the_ID(),'jv_team_website_url', true );
							$jv_team_address1 = get_post_meta( get_the_ID(),'jv_team_address1', true );
							$jv_team_address2 = get_post_meta( get_the_ID(),'jv_team_address2', true );
							$city_name_value = get_post_meta( get_the_ID(), 'jv_team_city_name', true );
							$state_name_value = get_post_meta( get_the_ID(), 'jv_team_state_name', true );
							$country_name_value = get_post_meta( get_the_ID(), 'jv_team_country_name', true );
							$zipcode_number_value = get_post_meta( get_the_ID(), 'jv_team_zipcode', true );
							$jv_team_address = '';
							if( $jv_team_address1 != ''){
								$jv_team_address .= $jv_team_address1.',';
							}
							if( $jv_team_address2 != ''){
								$jv_team_address .= $jv_team_address2.',';
							}
							if( $city_name_value != ''){
								$jv_team_address .= $city_name_value.',';
							}
							if( $jv_team_state_name != ''){
								$jv_team_address .= $jv_team_state_name.',';
							}
							if( $country_name_value != ''){
								$jv_team_address .= $country_name_value.',';
							}
							if( $zipcode_number_value != ''){
								$jv_team_address .= $zipcode_number_value.',';
							}	
						   
						   if ( file_exists( $jv_grid_path . '/content-archive-team-list-'.$select_style.'.php' ) ){
						   		 include $jv_grid_path. '/content-archive-team-list-'.$select_style.'.php';
						   }else{
						   		 include JVTEAM_PLUGIN_PATH. '/content-archive-team-list/content-archive-team-list-'.$select_style.'.php';
						   }
						 
						   $archive_jv_team_output .= ob_get_contents();
						   ob_end_clean();
						}
						wp_reset_query();
				}
				wp_reset_postdata();
				$archive_jv_team_output .= '</div>';
				if($team_members_display_pagination == 'on'){
					$archive_jv_team_output .= '<div class="et_pb_row jv_team_archive_pagination">';
					$big = 999999999; // need an unlikely integer
					$archive_jv_team_output .= paginate_links(array(
						'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
						'format' => '?paged=%#%',
						'current' => max( 1, get_query_var('paged') ),
						'total' => $archive_jv_team_query->max_num_pages, 
						'prev_text' => __( '<span class="et-pb-icon">&#x34;</span>', 'textdomain' ),
						'next_text' => __( '<span class="et-pb-icon">&#x35;</span>', 'textdomain' ),
					) );
					$archive_jv_team_output .= '</div>';
				}
				if( $display_popup_onteam =='on') {
						$archive_jv_team_output .= "<script>
						 jQuery(function ($) {
						 	$('a[href^=\"#teammodal\"]').addClass('et_smooth_scroll_disabled');
							$('.popup-modal').magnificPopup({
								type: 'inline',
								midClick: true,
								closeBtnInside: true
							});
						});
						 </script>";
				}
				return $archive_jv_team_output;
			}
	}
	new ET_Builder_Module_JV_Team_List_Member();
}
?>