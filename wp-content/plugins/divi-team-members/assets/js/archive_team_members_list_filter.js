jQuery(function() {
		var selectedClass = "";
			jQuery(".jv_filter").click(function(){  
				jQuery('.et_pb_jv_team_members_category_list li a').removeClass('active');
				jQuery(this).addClass('active');
				selectedClass = jQuery(this).attr("data-filter");  
				jQuery(".et_pb_jv_team_members_list").fadeTo(100, 0.1);
				jQuery(".et_pb_jv_team_members_list > div.et_pb_jv_team_members_list_column").not(selectedClass).fadeOut().removeClass('jv_team-scale-anm');
				setTimeout(function() {
					  jQuery(selectedClass).fadeIn().addClass('jv_team-scale-anm');
					  jQuery(".et_pb_jv_team_members_list").fadeTo(300, 1);

				}, 100); 
		});
});	