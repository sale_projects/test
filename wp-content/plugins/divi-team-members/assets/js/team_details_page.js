(function( $ ) {
    $(function() {
        // Add Color Picker to all inputs that have 'color-field' class
        $( '.jvteam-color-picker' ).wpColorPicker();
    });
})( jQuery );