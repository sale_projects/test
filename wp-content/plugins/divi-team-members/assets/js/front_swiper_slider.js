jQuery(window).on('et_fb_module_did_update_et_pb_team_members_slider', function(props, prevProps) {	
     if (! props.loading) {
       init_swiper_slider();
     }
});

																 
function init_swiper_slider(){
	function init_swiper_slider_script(){
					jQuery('.tms_container').each(function(index, value) {
								
								
								var tm1921 = jQuery(this).data('tm1921');
								var tm1501 	= jQuery(this).data('tm1501');
								var tm1201 = jQuery(this).data('tm1201');
								var tm981 	= jQuery(this).data('tm981');
								var tm768  = jQuery(this).data('tm768');
								var tm767  = jQuery(this).data('tm767');
								var tmrand 	= jQuery(this).data('tmrand');
								var tmautomaticslide 	= jQuery(this).data('tmautomaticslide');
								var tmslideduration = jQuery(this).data('tmslideduration');
								var tmspeed 	= jQuery(this).data('tmspeed');
								
									
										if ( tmautomaticslide == 'on' ){
											var swiper = new Swiper(this, {
												pagination: {el: '.tms_'+tmrand,clickable: true,},loop:true,slidesPerView: 4,spaceBetween: 50,roundLengths:true,autoplay : { delay : tmslideduration,},speed : tmspeed,
												breakpoints: {
													3500: {slidesPerView: tm1921,spaceBetween: 40},
													1920: {slidesPerView: tm1501,spaceBetween: 40},
													1500: {slidesPerView: tm1201,spaceBetween: 30},
													1200: {slidesPerView: tm981,spaceBetween: 20},
													980: {slidesPerView: tm768,spaceBetween: 20},
													767: {slidesPerView: tm767,spaceBetween: 10}
												}
											});
										
										}else{
											//  Autoplay OFF
											var swiper = new Swiper(this, {
											pagination: {el: '.tms_'+tmrand,clickable: true,},loop:true,slidesPerView: 4,spaceBetween: 50,roundLengths:true,speed : tmspeed,
											breakpoints: {
													3500: {slidesPerView: tm1921,spaceBetween: 40},
													1920: {slidesPerView: tm1501,spaceBetween: 40},
													1500: {slidesPerView: tm1201,spaceBetween: 30},
													1200: {slidesPerView: tm981,spaceBetween: 20},
													980: {slidesPerView: tm768,spaceBetween: 20},
													767: {slidesPerView: tm767,spaceBetween: 10}
												}
											});

										}
							
			  
				 });
	}
 	init_swiper_slider_script();
}