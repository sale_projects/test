<?php
$team_members_categories = wp_get_post_terms( get_the_ID() ,'department_category', array("fields" => "ids"));
$cat_filter = '';
if ( count($team_members_categories ) > 0 ){
	foreach ( $team_members_categories as $cat_id ){
			$cat_filter .= 'cat-'.$cat_id.' ';		
	}
}
$link = "#";
if( $team_members_display_detail_page_link_type =='default' ) { 
		$link = get_permalink(get_the_ID());
}else if ( $team_members_display_detail_page_link_type =='custom' ){
		$url = get_post_meta( get_the_ID(),'custom_url_detail_page', true );
		$link = $url!= '' ? $url : '#';
}else{
		$link = "#";
}
	$jv_team_designation = get_post_meta( get_the_ID(),'jv_team_designation', true );
	$jv_team_facebook = get_post_meta( get_the_ID(),'jv_team_facebook', true );
	$jv_team_twitter = get_post_meta( get_the_ID(),'jv_team_twitter', true );
	$jv_team_google = get_post_meta( get_the_ID(),'jv_team_google', true );
	$jv_team_linkdin = get_post_meta( get_the_ID(),'jv_team_linkedin', true );
	$jv_team_instagram = get_post_meta( get_the_ID(),'jv_team_instagram', true );
	$jv_team_mobile_number = get_post_meta( get_the_ID(),'jv_team_phone_number', true );
	$jv_team_email_address = get_post_meta( get_the_ID(),'jv_team_email_address', true );
	$jv_team_website_url = get_post_meta( get_the_ID(),'jv_team_website_url', true );

$class_1 = '';$link_no_new_tab = '';$link_new_tab_open = '';$img_stlink = '';$target_link = '';$img_endlink = '';

// Image ST
if ($link_open_in_new_tab == 'on'){ $target_link = 'target="_blank"';}
if( $team_members_display_detail_page == 'on' && $display_popup_onteam == 'off' ) { 
	$img_stlink = '<a href="'.$link.'" '.$target_link.'>'; 
} else if(  $display_popup_onteam == 'on' ){ 
	$img_stlink = '<a class="popup-modal" href="#teammodal'.get_the_ID().'">';
}else{ $img_stlink = '';}
// Image End
if( ( $team_members_display_detail_page == 'on' && $display_popup_onteam == 'off' ) || ( $display_popup_onteam == 'on' ) ) { $img_endlink = '</a>';}
?>
<div class="et_pb_column et_pb_column_1_2 jv_team-scale-anm all jv_hr_members jv_horizontal_tm jvtmhr jvtmhr_member jv_team_member_equalheight <?php echo rtrim($cat_filter);?> "><!--et_pb_column_1_2-->
	<div class="et_pb_jv_team ">
		<div class="et_pb_column et_pb_column_1_3">
			<div class="jv_hr_members hr_image jvtmhr">
			
				<?php 
					$jv_team_thumb = wp_get_attachment_image_src(get_post_thumbnail_id(), 'full');
						if( $jv_team_thumb[0] != ''){ 
									$image_path = $jv_team_thumb[0] ;
								}else{
									$image_path = JVTEAM_PLUGIN_URL . '/assets/images/default.png';
								} 
				?>
				<?php  echo $img_stlink; ?>
					<img src="<?php echo $image_path;?>" alt="<?php echo get_the_title();?>" >
				<?php  echo $img_endlink; ?>
			</div>
		</div>
		<div class="et_pb_column et_pb_column_2_3">
			<div class="jv_hr_members hr_container jvtmhr"  id="post-<?php the_ID(); ?>">
				<?php  echo $img_stlink;  ?>
				<h2 class="jv_hr_members hr_name jvtmhr"><?php echo get_the_title();?></h2>
				<?php   echo $img_endlink; ?>
			
				<?php if( $jv_team_designation != ''){ ?><h3 class="jv_hr_members hr_designation jvtmhr"><?php echo $jv_team_designation;?></h3><?php } ?>
				
				
					<?php if( $jv_team_facebook != '' || $jv_team_twitter != '' || $jv_team_google != '' || $jv_team_linkdin != '' || $jv_team_instagram != ''  ) { ?>
						<ul class="jv_hr_members hr_links jvtmhr">
						<?php if( $jv_team_facebook != '' ) { ?><li><a href="<?php echo $jv_team_facebook;?>" target="_blank" class="jv_hr_members  jvtmhr"><i class="jv_team_member_social_font et-pb-icon">&#xe0aa;</i></a></li><?php } ?>
						<?php if( $jv_team_twitter != '' ) { ?><li><a href="<?php echo $jv_team_twitter;?>" target="_blank" class="jv_hr_members  jvtmhr"><i class="jv_team_member_social_font et-pb-icon">&#xe0ab;</i></a></li><?php } ?>
						<?php if( $jv_team_google != '' ) { ?><li><a href="<?php echo $jv_team_google;?>" target="_blank" class="jv_hr_members  jvtmhr"><i class="jv_team_member_social_font jv_team_member_social_font_gogle et-pb-icon">&#xe0ad;</i></a></li><?php } ?>
						<?php if( $jv_team_linkdin != '' ) { ?><li><a href="<?php echo $jv_team_linkdin;?>" target="_blank" class="jv_hr_members  jvtmhr"><i class="jv_team_member_social_font et-pb-icon">&#xe0b4;</i></a></li><?php } ?>
						<?php if( $jv_team_instagram != '' ) { ?><li><a href="<?php echo $jv_team_instagram;?>" target="_blank" class="jv_hr_members  jvtmhr"><i class="jv_team_member_social_font et-pb-icon">&#xe0b1;</i></a></li><?php } ?>
					</ul>
					<?php }
					if( $jv_team_email_address != '' || $jv_team_mobile_number != '' ||  $jv_team_website_url != ''  ) { ?>
						<div class="jv_hr_members hr_infos jvtmhr">	
							<?php if( $jv_team_email_address != ''){ ?>
								<div class="jv_hr_members  jvtmhr  hr_info email">
									<span><i class="et-pb-icon">&#xe076;</i></span>
									<span><a href="mailto:<?php echo $jv_team_email_address;?>"><?php echo $jv_team_email_address;?></a></span>
								</div>
							<?php } ?>
							<?php if( $jv_team_mobile_number != ''){ ?>	
								<div class="jv_hr_members  jvtmhr  hr_info telphone">
									<span><i class="et-pb-icon">&#xe090;</i></span>
									<span><?php echo $jv_team_mobile_number;?></span>
								</div>
							<?php } ?>	
							<?php if( $jv_team_website_url != ''){ ?> 
								<div class="jv_hr_members  jvtmhr  hr_info website">
									<span><i class="et-pb-icon">&#xe0e3;</i></span>
									<span><a  href="<?php echo $jv_team_website_url;?>" target="_blank" ><?php echo $jv_team_website_url;?></a></span>
								</div>
							<?php } ?> 
						</div>
					<?php }?>
				
			</div>
		</div>
	</div>	
</div>	
<?php 
if( $display_popup_onteam =='on') {
	$divi_popupteam_select_style_layout = !empty(get_option('divi_popupteam_select_style_layout')) ? get_option('divi_popupteam_select_style_layout') : 'style1';
	if ( file_exists( $jv_grid_path_p . '/popup-'.$divi_popupteam_select_style_layout.'.php' ) ){
		 include $jv_grid_path_p. '/popup-'.$divi_popupteam_select_style_layout.'.php';
    }else{
		include JVTEAM_PLUGIN_PATH. '/content-popup/popup-'.$divi_popupteam_select_style_layout.'.php';
    }
    
} 
?>