<?php 
	$jv_team_designation = get_post_meta( get_the_ID(),'jv_team_designation', true );
	$jv_team_facebook = get_post_meta( get_the_ID(),'jv_team_facebook', true );
	$jv_team_twitter = get_post_meta( get_the_ID(),'jv_team_twitter', true );
	$jv_team_google = get_post_meta( get_the_ID(),'jv_team_google', true );
	$jv_team_linkdin = get_post_meta( get_the_ID(),'jv_team_linkedin', true );
	$jv_team_instagram = get_post_meta( get_the_ID(),'jv_team_instagram', true );
	$jv_team_mobile_number = get_post_meta( get_the_ID(),'jv_team_phone_number', true );
	$jv_team_email_address = get_post_meta( get_the_ID(),'jv_team_email_address', true );
	$jv_team_website_url = get_post_meta( get_the_ID(),'jv_team_website_url', true );
	$jv_team_address1 = get_post_meta( get_the_ID(),'jv_team_address1', true );
	$jv_team_address2 = get_post_meta( get_the_ID(),'jv_team_address2', true );
	$city_name_value = get_post_meta( get_the_ID(), 'jv_team_city_name', true );
	$state_name_value = get_post_meta( get_the_ID(), 'jv_team_state_name', true );
	$country_name_value = get_post_meta( get_the_ID(), 'jv_team_country_name', true );
	$zipcode_number_value = get_post_meta( get_the_ID(), 'jv_team_zipcode', true );
	$jv_team_contact_form = get_post_meta( $post->ID, 'jv_team_contact_form', true );
	$display_contact_form = get_post_meta( $post->ID, 'display_contact_form', true );
	$divi_team_contact_us_title = !empty(get_option('divi_team_contact_us_title')) ? get_option('divi_team_contact_us_title') : 'Drop Me a Line';
	
	$jv_team_address = '';
	if( $jv_team_address1 != ''){
		$jv_team_address .= $jv_team_address1.',<br/>';
	}
	if( $jv_team_address2 != ''){
		$jv_team_address .= $jv_team_address2.',<br/>';
	}
	if( $city_name_value != ''){
		$jv_team_address .= $city_name_value.',';
	}
	if( $state_name_value != ''){
		$jv_team_address .= $state_name_value.',';
	}
	if( $country_name_value != ''){
		$jv_team_address .= $country_name_value.',';
	}
	if( $zipcode_number_value != ''){
		$jv_team_address .= $zipcode_number_value.',';
	}

?>
<div class="jv_team_list_member jv_team_list_member_<?php echo $divi_team_select_style_layout; ?>">
	<div class="et_pb_row style5_responsive_info style5_first_row"> 
		<div class="et_pb_column et_pb_column_1_3">
			<div class="jv_team_list_member_image">
				<?php 
					$jv_team_thumb = wp_get_attachment_image_src(get_post_thumbnail_id(), 'jv_team_list_image');
					if( $jv_team_thumb[0] != ''){ 
								$image_path = $jv_team_thumb[0] ;
							}else{
								$image_path = JVTEAM_PLUGIN_URL . '/assets/images/default.png';
							} 
					?>
			<img src="<?php echo $image_path;?>" alt="<?php echo get_the_title();?>">
			</div>
		</div>
		<div class="et_pb_column et_pb_column_2_3">
		
				<h2 class="jv_team_list_title"><?php echo get_the_title();?></h2>
				<div class="jv_team_list_content">
					<?php echo do_shortcode(wpautop(get_the_content())); ?>
				</div>
				<?php 
					if(( $jv_team_facebook != '') || ( $jv_team_twitter != '') || ( $jv_team_google != '') || ( $jv_team_linkdin != '') || ( $jv_team_instagram != '')) { ?>
					<ul class="jv_team_list_social_link">
						<?php if( $jv_team_facebook != '' ) { ?><li><a href="<?php echo $jv_team_facebook;?>" target="_blank"><i class="jv_team_member_social_font et-pb-icon">&#xe093;</i></a></li><?php } ?>
						<?php if( $jv_team_twitter != '' ) { ?><li><a href="<?php echo $jv_team_twitter;?>" target="_blank"><i class="jv_team_member_social_font et-pb-icon">&#xe094;</i></a></li><?php } ?>
						<?php if( $jv_team_google != '' ) { ?><li><a href="<?php echo $jv_team_google;?>" target="_blank"><i class="jv_team_member_social_font jv_team_member_social_font_gogle et-pb-icon">&#xe096;</i></a></li><?php } ?>
						<?php if( $jv_team_linkdin != '' ) { ?><li><a href="<?php echo $jv_team_linkdin;?>"target="_blank"><i class="jv_team_member_social_font et-pb-icon">&#xe09d;</i></a></li><?php } ?>
						<?php if( $jv_team_instagram != '' ) { ?><li><a href="<?php echo $jv_team_instagram;?>"target="_blank"><i class="jv_team_member_social_font et-pb-icon">&#xe09a;</i></a></li><?php } ?>
					</ul>
				<?php 	} ?>	

		</div>
	</div>
   <div class="et_pb_row style5_second_row"> 
			<div class="jv_team_list_text">
				<h2><?php echo get_the_title();?> Information</h2>
			</div>
		<div class="et_pb_column et_pb_column_2_3">
			<div class="jv_team_list_designation">
				<div class="jv_team_as_designation">
					<ul>
						<?php if( $jv_team_address != '' ) {?>
							<li>Addresss:</li>
							<li><?php echo rtrim($jv_team_address,',');?></li><?php }?>
					</ul>
				</div>
				<div class="jv_team_as_designation">
					<ul>				
						<?php if( $jv_team_website_url != ''){ ?> 
							<li>Website URL:</li>  
							<li><a class="website_url" href="<?php echo $jv_team_website_url;?>" target="_blank" ><?php echo $jv_team_website_url;?></a></li>
						<?php } ?>
					</ul>
				</div>
				<div class="jv_team_as_designation">
					<ul>
						<?php if( $jv_team_mobile_number != ''){ ?>
							<li>Phone Number:</li> 				
							<li><a class="contect_number" href="tel:<?php echo $jv_team_mobile_number;?>"><?php echo $jv_team_mobile_number;?></a></li>
						<?php } ?>
					</ul>	
				</div>
				<div class="jv_team_as_designation">
					<ul>
						<?php if( $jv_team_email_address != ''){ ?>
							<li>Email Address:</li> 					
							<li><a class="email_address" href="mailto:<?php echo $jv_team_email_address;?>"  ><?php echo $jv_team_email_address;?></a></li>
						<?php } ?> 
					</ul>
				</div>	
				<div class="jv_team_as_designation">
					<ul>				
						<?php if( $jv_team_designation != ''){ ?>
							<li>Position:</li> 	
							<li><span class="jv_team_list_position"><?php echo $jv_team_designation;?></li>
						<?php } ?>
					</ul>
				</div>
			</div>
		</div>
		<div class="et_pb_column et_pb_column_1_3">
			<div class="jv_team_member_list_map">
				<img src="<?php echo esc_url( JVTEAM_PLUGIN_URL.'/assets/images/map.jpg' ); ?>" height="100%" width="100%" alt="map">
			</div>
			<div class="jv_team_list_map_donate">
				<div class="jv_team_list_map_donate_wrap">
					<i class="map-icon et-pb-icon">&#xe081;</i>
					<a href="https://maps.google.it/maps?q=<?php echo rtrim($jv_team_address,','); ?>">Get a <span>direction</span></a>
				</div>
			</div>
		</div>
	</div>
	<?php
		if( $display_contact_form =='on') {
			if ( is_plugin_active( 'caldera-forms/caldera-core.php' ) ) {
			
		?> <div class="et_pb_row">
				<div class="et_pb_column et_pb_column_4_4">
					<div class="jv_team_list_text style5_contact_form">
						<h2><?php echo $divi_team_contact_us_title; ?></h2>
						<div class="style5_hr_line"></div>
					</div>
						<?php echo do_shortcode('[caldera_form id="'.$jv_team_contact_form.'"]') ?>
				</div>
			</div>
		<?php 	}	
		}
		?>
</div>	