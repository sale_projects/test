<?php 
	$jv_team_designation = get_post_meta( get_the_ID(),'jv_team_designation', true );
	$jv_team_facebook = get_post_meta( get_the_ID(),'jv_team_facebook', true );
	$jv_team_twitter = get_post_meta( get_the_ID(),'jv_team_twitter', true );
	$jv_team_google = get_post_meta( get_the_ID(),'jv_team_google', true );
	$jv_team_linkdin = get_post_meta( get_the_ID(),'jv_team_linkedin', true );
	$jv_team_instagram = get_post_meta( get_the_ID(),'jv_team_instagram', true );
	$jv_team_mobile_number = get_post_meta( get_the_ID(),'jv_team_phone_number', true );
	$jv_team_email_address = get_post_meta( get_the_ID(),'jv_team_email_address', true );
	$jv_team_website_url = get_post_meta( get_the_ID(),'jv_team_website_url', true );
	$jv_team_address1 = get_post_meta( get_the_ID(),'jv_team_address1', true );
	$jv_team_address2 = get_post_meta( get_the_ID(),'jv_team_address2', true );
	$city_name_value = get_post_meta( get_the_ID(), 'jv_team_city_name', true );
	$state_name_value = get_post_meta( get_the_ID(), 'jv_team_state_name', true );
	$country_name_value = get_post_meta( get_the_ID(), 'jv_team_country_name', true );
	$zipcode_number_value = get_post_meta( get_the_ID(), 'jv_team_zipcode', true );
	$jv_team_contact_form = get_post_meta( $post->ID, 'jv_team_contact_form', true );
	$display_contact_form = get_post_meta( $post->ID, 'display_contact_form', true );
	$divi_team_contact_us_title = !empty(get_option('divi_team_contact_us_title')) ? get_option('divi_team_contact_us_title') : 'Drop Me a Line';


	$jv_team_address = '';
	if( $jv_team_address1 != ''){
		$jv_team_address .= $jv_team_address1.',';
	}
	if( $jv_team_address2 != ''){
		$jv_team_address .= $jv_team_address2.',';
	}
	if( $city_name_value != ''){
		$jv_team_address .= $city_name_value.',';
	}
	if( $state_name_value != ''){
		$jv_team_address .= $state_name_value.',';
	}
	if( $country_name_value != ''){
		$jv_team_address .= $country_name_value.',';
	}
	if( $zipcode_number_value != ''){
		$jv_team_address .= $zipcode_number_value.',';
	}
?>
<div class="jv_team_list_member jv_team_list_member_<?php echo $divi_team_select_style_layout; ?>">
	<div class="et_pb_row style3_responsive style3_first_row"> 
		<div class="et_pb_column et_pb_column_1_2 style3_column_1">
			<div class="jv_team_list_member_image">
				<?php 
					$jv_team_thumb = wp_get_attachment_image_src(get_post_thumbnail_id(), 'jv_team_list_image');
					if( $jv_team_thumb[0] != ''){ 
							$image_path = $jv_team_thumb[0] ;
						}else{
							$image_path = JVTEAM_PLUGIN_URL . '/assets/images/default.png';
						} 
				?>
					<img src="<?php echo $image_path;?>" alt="<?php echo get_the_title();?>">	
			</div>
		</div>
		<div class="et_pb_column et_pb_column_1_2 style3_column_2">			
			<div class="jv_team_list_description">
				<h4 class="jv_team_list_title"><?php echo get_the_title();?>
					<?php if( $jv_team_mobile_number != ''){ ?>
						<a class="contect_number" href="tel:<?php echo $jv_team_mobile_number;?>"><?php echo $jv_team_mobile_number;?></a>
					<?php } ?>
					<?php if( $jv_team_designation != ''){ ?>
					<small class="jv_team_list_position"><?php echo $jv_team_designation;?></small>
					<?php } ?>
				</h4>
				<?php 
				if(( $jv_team_facebook != '') || ( $jv_team_twitter != '') || ( $jv_team_google != '') || ( $jv_team_linkdin != '') || ( $jv_team_instagram != '')) { ?>
				<ul class="jv_team_list_social_link">
					<?php if( $jv_team_facebook != '' ) { ?><li><a href="<?php echo $jv_team_facebook;?>" target="_blank"><i class="jv_team_member_social_font et-pb-icon">&#xe093;</i></a></li><?php } ?>
					<?php if( $jv_team_twitter != '' ) { ?><li><a href="<?php echo $jv_team_twitter;?>" target="_blank"><i class="jv_team_member_social_font et-pb-icon">&#xe094;</i></a></li><?php } ?>
					<?php if( $jv_team_google != '' ) { ?><li><a href="<?php echo $jv_team_google;?>" target="_blank"><i class="jv_team_member_social_font jv_team_member_social_font_gogle et-pb-icon">&#xe096;</i></a></li><?php } ?>
					<?php if( $jv_team_linkdin != '' ) { ?><li><a href="<?php echo $jv_team_linkdin;?>"target="_blank"><i class="jv_team_member_social_font et-pb-icon">&#xe09d;</i></a></li><?php } ?>
					<?php if( $jv_team_instagram != '' ) { ?><li><a href="<?php echo $jv_team_instagram;?>"target="_blank"><i class="jv_team_member_social_font et-pb-icon">&#xe09a;</i></a></li><?php } ?>
				</ul>
				<?php 	} ?>					
				<div class="jv_team_list_content">
					<?php echo do_shortcode(wpautop(get_the_content())); ?>
				</div> 	
			</div>
		</div>
	</div>
	<div class="style3_hr_line"></div>
 <?php 
 	if( $display_contact_form =='on') {
 include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
  if ( is_plugin_active( 'caldera-forms/caldera-core.php' ) ) { 
	   if ($jv_team_contact_form != '' ) { $colum_class = "et_pb_column_1_3"; }else{  $colum_class = "et_pb_column_4_4"; }
  }else{
	  $colum_class = "et_pb_column_4_4";
   }
	}
  ?>
	<div class="et_pb_row style3_responsive_contact_info style3_second_row"> 
		<div class="et_pb_column <?php echo $colum_class?>">
			<div class="jv_team_list_text">
				<h4>Contact Information</h4>
			</div>
			<?php if( $jv_team_address != ''){ ?> 
			<div class="jv_team_list_address1"><i class="jv_team_member_icon_font et-pb-icon">&#xe01d;</i><span><?php echo rtrim($jv_team_address,',');?></span></div>
			<?php } ?>
			<?php if( $jv_team_website_url != ''){ ?> 
				<div class="jv_team_list_web_url"><i class="jv_team_member_icon_font et-pb-icon">&#xe0e3;</i>  <a class="website_url" href="<?php echo $jv_team_website_url;?>" target="_blank" ><?php echo $jv_team_website_url;?></a></div>
			<?php } ?> 
			
			<?php if( $jv_team_email_address != ''){ ?> 
					<div class="jv_team_email_list_address"><i class="jv_team_member_icon_font et-pb-icon">&#xe076;</i>  <a class="email_address" href="mailto:<?php echo $jv_team_email_address;?>"  ><?php echo $jv_team_email_address;?></a></div>
			<?php } ?> 	
		</div>
		<?php 	
		if( $display_contact_form =='on') {
			if ( is_plugin_active( 'caldera-forms/caldera-core.php' ) ) {
				if ($jv_team_contact_form != '' ) {
		?>
		<div class="et_pb_column et_pb_column_2_3 style3_responsive_contact_form">	
			<div class="jv_team_list_text style3_contact_form">
				<h4><?php echo $divi_team_contact_us_title; ?></h4>
				<div class="style3_hr_line"></div>
			</div>
			<?php echo do_shortcode('[caldera_form id="'.$jv_team_contact_form.'"]') ?>		
		</div>
		<?php 	}
			}	
		}
		?>
	</div>	
</div>	