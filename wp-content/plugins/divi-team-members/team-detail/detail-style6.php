<?php 
$team_members_categories = wp_get_post_terms( get_the_ID() ,'department_category', array("fields" => "names"));
$cat_name = '';
if ( count($team_members_categories ) > 0 ){
	foreach ( $team_members_categories as $cat_names ){
		$cat_name .= $cat_names.',';		
	}
}
	$jv_team_designation = get_post_meta( get_the_ID(),'jv_team_designation', true );
	$jv_team_facebook = get_post_meta( get_the_ID(),'jv_team_facebook', true );
	$jv_team_twitter = get_post_meta( get_the_ID(),'jv_team_twitter', true );
	$jv_team_google = get_post_meta( get_the_ID(),'jv_team_google', true );
	$jv_team_linkdin = get_post_meta( get_the_ID(),'jv_team_linkedin', true );
	$jv_team_instagram = get_post_meta( get_the_ID(),'jv_team_instagram', true );
	$jv_team_mobile_number = get_post_meta( get_the_ID(),'jv_team_phone_number', true );
	$jv_team_email_address = get_post_meta( get_the_ID(),'jv_team_email_address', true );
	$jv_team_website_url = get_post_meta( get_the_ID(),'jv_team_website_url', true );
	$jv_team_address1 = get_post_meta( get_the_ID(),'jv_team_address1', true );
	$jv_team_address2 = get_post_meta( get_the_ID(),'jv_team_address2', true );
	$city_name_value = get_post_meta( get_the_ID(), 'jv_team_city_name', true );
	$state_name_value = get_post_meta( get_the_ID(), 'jv_team_state_name', true );
	$country_name_value = get_post_meta( get_the_ID(), 'jv_team_country_name', true );
	$zipcode_number_value = get_post_meta( get_the_ID(), 'jv_team_zipcode', true );
	$jv_team_contact_form = get_post_meta( $post->ID, 'jv_team_contact_form', true );
	$display_contact_form = get_post_meta( $post->ID, 'display_contact_form', true );
	$divi_team_contact_us_title = !empty(get_option('divi_team_contact_us_title')) ? get_option('divi_team_contact_us_title') : 'Drop Me a Line';
	
	
	$jv_team_address = '';
	if( $jv_team_address1 != ''){
		$jv_team_address .= $jv_team_address1.',';
	}
	if( $jv_team_address2 != ''){
		$jv_team_address .= $jv_team_address2.',';
	}
	if( $city_name_value != ''){
		$jv_team_address .= $city_name_value.',';
	}
	if( $state_name_value != ''){
		$jv_team_address .= $state_name_value.',';
	}
	if( $country_name_value != ''){
		$jv_team_address .= $country_name_value.',';
	}
	if( $zipcode_number_value != ''){
		$jv_team_address .= $zipcode_number_value.',';
	}
?>
<div class="jv_team_list_member jv_team_list_member_<?php echo $divi_team_select_style_layout; ?>">
	<div class="et_pb_row style6_first_row"> 
		<div class="et_pb_column et_pb_column_1_3 style6_responsive_social">
			<div class="jv_team_list_member_image">
				<?php 
					$jv_team_thumb = wp_get_attachment_image_src(get_post_thumbnail_id(), 'jv_team_list_image');
					$jv_team_thumb_mobile = wp_get_attachment_image_src(get_post_thumbnail_id(), 'large');
					if( $jv_team_thumb[0] != '' || $jv_team_thumb_mobile[0] != ''){ 
								$image_path = $jv_team_thumb[0] ;
								$image_path_mobile = $jv_team_thumb_mobile[0] ;
					}else{
								$image_path = JVTEAM_PLUGIN_URL . '/assets/images/default.png';
					} 
					?>
			<img src="<?php echo $image_path;?>" alt="<?php echo get_the_title();?>" class="img_desk">
			<img src="<?php echo $image_path_mobile;?>" alt="<?php echo get_the_title();?>" class="img_mobile">			
			</div>
			<?php 
				if(( $jv_team_facebook != '') || ( $jv_team_twitter != '') || ( $jv_team_google != '') || ( $jv_team_linkdin != '') || ( $jv_team_instagram != '')) { ?>
				<div class="jv_team_list_social_link">
					<ul>
						<?php if( $jv_team_facebook != '' ) { ?><li><a href="<?php echo $jv_team_facebook;?>" target="_blank"><i class="jv_team_member_social_font et-pb-icon">&#xe093;</i></a></li><?php } ?>
						<?php if( $jv_team_twitter != '' ) { ?><li><a href="<?php echo $jv_team_twitter;?>" target="_blank"><i class="jv_team_member_social_font et-pb-icon">&#xe094;</i></a></li><?php } ?>
						<?php if( $jv_team_google != '' ) { ?><li><a href="<?php echo $jv_team_google;?>" target="_blank"><i class="jv_team_member_social_font jv_team_member_social_font_gogle et-pb-icon">&#xe096;</i></a></li><?php } ?>
						<?php if( $jv_team_linkdin != '' ) { ?><li><a href="<?php echo $jv_team_linkdin;?>"target="_blank"><i class="jv_team_member_social_font et-pb-icon">&#xe09d;</i></a></li><?php } ?>
						<?php if( $jv_team_instagram != '' ) { ?><li><a href="<?php echo $jv_team_instagram;?>"target="_blank"><i class="jv_team_member_social_font et-pb-icon">&#xe09a;</i></a></li><?php } ?>
					</ul>
				</div>
		    <?php } ?>	
		</div>
		<div class="et_pb_column et_pb_column_2_3 right_info_columm">
			<div class="jv_team_list_title">
				<span>Know about me <?php echo get_the_title();?></span>
				<h2><?php echo get_the_title();?></h2>
			</div>
			<?php if( $jv_team_designation !='') { ?>
			<div class="et_pb_column <?php if( $divi_team_details_page_layout == "Fullwidth" ){ ?>et_pb_column_1_2<?php }else{ ?> style6_feture_info <?php }?>">
				<div class="jv_team_feture_box">
					<div class="jv_team_icon">
						<span><i class="jv_team_member_info et-pb-icon">&#xe085;</i></span>
					</div>
					<div class="jv_team_info">
						<h4>Designation:</h4>
						<span><?php echo $jv_team_designation;?></span>
					</div>
				</div>
			</div>
			<?php } ?>
			<?php if( $jv_team_mobile_number !='') { ?>
			<div class="et_pb_column <?php if( $divi_team_details_page_layout == "Fullwidth" ){ ?>et_pb_column_1_2<?php }else{ ?> style6_feture_info <?php }?>">
				<div class="jv_team_feture_box">
					<div class="jv_team_icon">
						<span><i class="jv_team_member_info et-pb-icon">&#xe00b;</i></span>
					</div>
					<div class="jv_team_info">
						<h4>Phone:</h4>
						<span><?php echo $jv_team_mobile_number;?></span>
					</div>
				</div>
			</div>
			<?php } ?>
			<?php if( $jv_team_email_address !='' ) { ?>
			<div class="et_pb_column <?php if( $divi_team_details_page_layout == "Fullwidth" ){ ?>et_pb_column_1_2<?php }else{ ?> style6_feture_info <?php }?>">
				<div class="jv_team_feture_box">
					<div class="jv_team_icon">
						<span><i class="jv_team_member_info et-pb-icon">&#xe076;</i></span>
					</div>
					<div class="jv_team_info">
						<h4>Email:</h4>
						<span><?php echo $jv_team_email_address;?></span>
					</div>
				</div>
			</div>
			<?php } ?>
			<?php if( $jv_team_website_url !='') { ?>
			<div class="et_pb_column <?php if( $divi_team_details_page_layout == "Fullwidth" ){ ?>et_pb_column_1_2<?php }else{ ?> style6_feture_info <?php }?>">
				<div class="jv_team_feture_box">
					<div class="jv_team_icon">
						<span><i class="jv_team_member_info et-pb-icon">&#xe0e3;</i></span>
					</div>
					<div class="jv_team_info">
						<h4>Website URL:</h4>
						<span><?php echo $jv_team_website_url;?></span>
					</div>
				</div>
			</div>
			<?php } ?>
			<?php if( $jv_team_address !='') { ?>
			<div class="et_pb_column <?php if( $divi_team_details_page_layout == "Fullwidth" ){ ?>et_pb_column_1_2<?php }else{ ?> style6_feture_info <?php }?>">
				<div class="jv_team_feture_box">
					<div class="jv_team_icon">
						<span><i class="jv_team_member_info et-pb-icon">&#xe081;</i></span>
					</div>
					<div class="jv_team_info">
						<h4>Address:</h4>
						<span><?php echo rtrim($jv_team_address,',');?></span>
					</div>
				</div>
			</div>
			<?php } ?>
			<?php if( $cat_name !='' ) { ?>
			<div class="et_pb_column <?php if( $divi_team_details_page_layout == "Fullwidth" ){ ?>et_pb_column_1_2<?php }else{ ?> style6_feture_info <?php }?>">
				<div class="jv_team_feture_box">
					<div class="jv_team_icon">
						<span><i class="jv_team_member_info et-pb-icon">&#xe018;</i></span>
					</div>
					<div class="jv_team_info">
						<h4>Department:</h4>
						<span><?php echo rtrim( $cat_name,',' ); ?></span>
					</div>
				</div>
			</div>
			<?php } ?>
		</div>		
	</div>
	<div class="et_pb_row style6_second_row"> 
	<div class="jv_team_list_content">
		<?php echo do_shortcode(wpautop(get_the_content())); ?>
	</div>	
	</div>	
	<?php
		if( $display_contact_form =='on') {
			if ( is_plugin_active( 'caldera-forms/caldera-core.php' ) ) {
			
		?> <div class="et_pb_row style6_third_row">
				<div class="et_pb_column et_pb_column_4_4">
					<div class="jv_team_list_text style6_contact_form">
						<h2><?php echo $divi_team_contact_us_title; ?></h2>
						<div class="style6_hr_line"></div>
					</div>
						<?php echo do_shortcode('[caldera_form id="'.$jv_team_contact_form.'"]') ?>
				</div>
			</div>
		<?php 	}	
		}
		?>
</div>		