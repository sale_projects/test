<?php
$link = "#";
if( $team_table_display_detail_page_link_type =='default' ) { 
		$link = get_permalink(get_the_ID());
}else if ( $team_table_display_detail_page_link_type =='custom' ){
		$url = get_post_meta( get_the_ID(),'custom_url_detail_page', true );
		$link = $url!= '' ? $url : '#';
}else{
		$link = "#";
}

$class_1 = '';$link_no_new_tab = '';$link_new_tab_open = '';$img_stlink = '';$target_link = '';$img_endlink = '';

// Image ST
if ($link_open_in_new_tab == 'on'){ $target_link = 'target="_blank"';}
if( $team_table_display_detail_page == 'on' && $display_popup_onteam == 'off' ) { 
	$img_stlink = '<a href="'.$link.'" '.$target_link.'>'; 
} else if(  $display_popup_onteam == 'on' ){ 
	$img_stlink = '<a class="popup-modal" href="#teammodal'.get_the_ID().'">';
}else{ $img_stlink = '';}
// Image End
if( ( $team_table_display_detail_page == 'on' && $display_popup_onteam == 'off' ) || ( $display_popup_onteam == 'on' ) ) { $img_endlink = '</a>';}

?>
<div class="jvtmt_table jvtmt_table_ele jvtmt_member">
	<div class="jvtmt_image_centering jvtmt_table_ele">
		<?php 
			$jv_team_thumb = wp_get_attachment_image_src(get_post_thumbnail_id(), 'medium');
				if( $jv_team_thumb[0] != ''){ 
							$image_path = $jv_team_thumb[0] ;
						}else{
							$image_path = JVTEAM_PLUGIN_URL . '/assets/images/default.png';
						} 
		?>
		<?php echo $img_stlink;?><img src="<?php echo $image_path;?>" alt="<?php echo get_the_title();?>" ><?php echo $img_endlink;?>
	</div>
		<?php 
			$jv_team_designation = get_post_meta( get_the_ID(),'jv_team_designation', true );
			$jv_team_facebook = get_post_meta( get_the_ID(),'jv_team_facebook', true );
			$jv_team_twitter = get_post_meta( get_the_ID(),'jv_team_twitter', true );
			$jv_team_google = get_post_meta( get_the_ID(),'jv_team_google', true );
			$jv_team_linkdin = get_post_meta( get_the_ID(),'jv_team_linkedin', true );
			$jv_team_instagram = get_post_meta( get_the_ID(),'jv_team_instagram', true );
			$jv_team_mobile_number = get_post_meta( get_the_ID(),'jv_team_phone_number', true );
			$jv_team_email_address = get_post_meta( get_the_ID(),'jv_team_email_address', true );
			$jv_team_website_url = get_post_meta( get_the_ID(),'jv_team_website_url', true ); 
		?>
		
			<h2 class="jvtmt_name jvtmt_table_ele"><?php if( $team_table_display_detail_page == 'on' ) { echo $img_stlink; }?>	<?php echo get_the_title();?><i class="jvtmt_icon_color et-pb-icon">&#xe03e;</i><?php if( $team_table_display_detail_page =='on' ) {  echo $img_endlink; }?></h2>		
		
	
	<?php if( $jv_team_designation != ''){ ?>
	<h3 class="jvtmt_designation jvtmt_table_ele"><?php echo $jv_team_designation;?></h3>
	<?php } ?>
	
	<?php if( $jv_team_email_address != ''){ ?>
		<div class="jvtmt_email jvtmt_table_ele jvtmt_info">
			<span><i class="jvtmt_icon_color et-pb-icon">&#xe010;</i></span>
			<span><a href="mailto:<?php echo $jv_team_email_address;?>"><?php echo $jv_team_email_address;?></a></span>
		</div>
	<?php } ?>
	
	<?php if( $jv_team_mobile_number != ''){ ?>
		<div class="jvtmt_mobile jvtmt_table_ele jvtmt_info">
			<span><i class="jvtmt_icon_color et-pb-icon">&#xe090;</i></span>
			<span><?php echo $jv_team_mobile_number;?></span>			
		</div>
	<?php } ?>	
	<div class="jvtmt_links jvtmt_table_ele">
		<?php if( $jv_team_facebook != '' ) { ?><a href="<?php echo $jv_team_facebook;?>" target="_blank" class="jvtmt_table_ele"><i class="jvtmt_social_font et-pb-icon">&#xe0aa;</i></a><?php } ?>
		<?php if( $jv_team_twitter != '' ) { ?><a href="<?php echo $jv_team_twitter;?>" target="_blank" class="jvtmt_table_ele"><i class="jvtmt_social_font et-pb-icon">&#xe0ab;</i></a><?php } ?>
		<?php if( $jv_team_google != '' ) { ?><a href="<?php echo $jv_team_google;?>" target="_blank" class="jvtmt_table_ele"><i class="jvtmt_social_font et-pb-icon">&#xe0ad;</i></a><?php } ?>
		<?php if( $jv_team_linkdin != '' ) { ?><a href="<?php echo $jv_team_linkdin;?>"target="_blank" class="jvtmt_table_ele"><i class="jvtmt_social_font et-pb-icon">&#xe0b4;</i></a><?php } ?>
		<?php if( $jv_team_instagram != '' ) { ?><a href="<?php echo $jv_team_instagram;?>"target="_blank" class="jvtmt_table_ele"><i class="jvtmt_social_font et-pb-icon">&#xe0b1;</i></a><?php } ?>		
	</div>		
</div>
<?php 
if( $display_popup_onteam =='on') {
	$divi_popupteam_select_style_layout = !empty(get_option('divi_popupteam_select_style_layout')) ? get_option('divi_popupteam_select_style_layout') : 'style1';
	if ( file_exists( $jv_grid_path_p . '/popup-'.$divi_popupteam_select_style_layout.'.php' ) ){
		 include $jv_grid_path_p. '/popup-'.$divi_popupteam_select_style_layout.'.php';
    }else{
		include JVTEAM_PLUGIN_PATH. '/content-popup/popup-'.$divi_popupteam_select_style_layout.'.php';
    }
    
} 
?>