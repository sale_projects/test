<?php
$team_members_categories = wp_get_post_terms( get_the_ID() ,'department_category', array("fields" => "ids"));
$cat_filter = '';
if ( count($team_members_categories ) > 0 ){
	foreach ( $team_members_categories as $cat_id ){
			$cat_filter .= 'cat-'.$cat_id.' ';		
	}
}

$link = "#";
if( $display_detail_page_link_type =='default' ) { 
		$link = get_permalink(get_the_ID());
}else if ( $display_detail_page_link_type =='custom' ){
		$url = get_post_meta( get_the_ID(),'custom_url_detail_page', true );
		$link = $url!= '' ? $url : '#';
}else{
		$link = "#";
}
	
$class_1 = '';$link_no_new_tab = '';$link_new_tab_open = '';$img_stlink = '';$target_link = '';$img_endlink = '';$btn_stlink = '';$btn_endlink = '';

// Popup ST
if( $display_popup_onteam == 'on' ) { $popup_st = '<a class="popup-modal" href="#teammodal'.get_the_ID().'">'; }
// Popup END
if( $display_popup_onteam == 'on' ) { $popup_end = '</a>'; }

// Image ST
if ($link_open_in_new_tab == 'on'){ $target_link = 'target="_blank"';}
if( $display_detail_page == 'on' && $display_popup_onteam == 'off'  ) { 
	$img_stlink = '<a href="'.$link.'" '.$target_link.'>'; 
	$btn_stlink = '<a href="'.$link.'" '.$target_link.' class="jv_team_view_more" >'; 
} else if(  $display_popup_onteam == 'on' ){ 
	$img_stlink = '<a class="popup-modal" href="#teammodal'.get_the_ID().'">';
	$btn_stlink = '<a class="popup-modal jv_team_view_more" href="#teammodal'.get_the_ID().'">';
}else{ $img_stlink = '';$btn_stlink = '';}
// Image End
if( ( $display_detail_page == 'on' && $display_popup_onteam == 'off' ) || ( $display_popup_onteam == 'on' ) ) { $img_endlink = '</a>';$btn_endlink = '</a>';}
			
?>
<div class="et_pb_row team-scale-anm all et_pb_jv_team_members_list_column <?php echo rtrim($cat_filter);?>">
	<div class="et_pb_column et_pb_column_4_4">
		<div class="jv_team_list_member <?php if ( $display_view_more_button != 'on') { ?>jv_team_list_paddding <?php } ?> <?php if( $jv_team_facebook == '' || $jv_team_twitter == '' || $jv_team_google == '' || $jv_team_linkdin == '' || $jv_team_instagram == ''  ) { ?> jv_team_list_bottom_padding <?php } ?>">
			<div class="jv_team_list_member_image">
				<?php 
					$jv_team_thumb = wp_get_attachment_image_src(get_post_thumbnail_id(), 'jv_team_list_image');
					if( $jv_team_thumb[0] != ''){ 
								$image_path = $jv_team_thumb[0] ;
							}else{
								$image_path = JVTEAM_PLUGIN_URL . '/assets/images/default_list.png';
							} 
					?>
				<?php echo $img_stlink;?><img src="<?php echo $image_path;?>" alt="<?php echo get_the_title();?>"><?php echo $img_endlink;?>	
			</div>
			
			<div class="jv_team_list_description">
			<?php if( $display_detail_page == 'on' ) {  echo $img_stlink; }?>
				<h4 class="jv_team_list_title"><?php echo get_the_title();?></h4>
			<?php if( $display_detail_page =='on' ) { echo $img_endlink;}
				  if( $jv_team_designation != ''){ ?>
					<div class="jv_team_list_position"><?php echo $jv_team_designation;?></div>
					<?php } ?>
						<div class="jv_team_list_content">
								<?php $content_len= strlen(get_the_content());
									   $divi_team_content_length = !empty(get_option('divi_team_content_length')) ? get_option('divi_team_content_length') : '180';
									   if ($content_len > $divi_team_content_length){
										   echo do_shortcode(wpautop(substr(strip_tags(get_the_content()),0,$divi_team_content_length).'...'));
									   }else{
										   echo do_shortcode(wpautop(get_the_content()));
									   }
								?>
						</div> 
						<?php if( $jv_team_address != ''){ ?> 
						<div class="jv_team_list_address1"><i class="jv_team_member_icon_font et-pb-icon">&#xe01d;</i><?php echo rtrim($jv_team_address,',');?></div>
						<?php } ?>
						<?php if( $jv_team_website_url != ''){ ?> 
							<div class="jv_team_list_web_url"><i class="jv_team_member_icon_font et-pb-icon">&#xe0e3;</i>  <a class="website_url" href="<?php echo $jv_team_website_url;?>" target="_blank" ><?php echo $jv_team_website_url;?></a></div>
						<?php } ?> 
						<?php if( $jv_team_mobile_number != ''){ ?> 
							<div class="jv_team_list_mobile"><i class="jv_team_member_icon_font et-pb-icon">&#xe00b;</i>  <a class="contect_number" href="tel:<?php echo $jv_team_mobile_number;?>"  ><?php echo $jv_team_mobile_number;?></a></div>
						<?php } ?> 
						<?php if( $jv_team_email_address != ''){ ?> 
								<div class="jv_team_email_list_address"><i class="jv_team_member_icon_font et-pb-icon">&#xe076;</i>  <a class="email_address" href="mailto:<?php echo $jv_team_email_address;?>"  ><?php echo $jv_team_email_address;?></a></div>
						<?php } ?> 	
					<?php if ( $display_social_link == 'on'){ ?>
						<ul class="jv_team_list_social_link <?php if ( $display_view_more_button != 'on') { ?>jv_team_list_view_more_border <?php } ?>">
								<?php if( $jv_team_facebook != '' ) { ?><li><a href="<?php echo $jv_team_facebook;?>" target="_blank"><i class="jv_team_member_social_font et-pb-icon">&#xe0aa;</i></a></li><?php } ?>
								<?php if( $jv_team_twitter != '' ) { ?><li><a href="<?php echo $jv_team_twitter;?>" target="_blank"><i class="jv_team_member_social_font et-pb-icon">&#xe0ab;</i></a></li><?php } ?>
								<?php if( $jv_team_google != '' ) { ?><li><a href="<?php echo $jv_team_google;?>" target="_blank"><i class="jv_team_member_social_font jv_team_member_social_font_gogle et-pb-icon">&#xe0ad;</i></a></li><?php } ?>
								<?php if( $jv_team_linkdin != '' ) { ?><li><a href="<?php echo $jv_team_linkdin;?>"target="_blank"><i class="jv_team_member_social_font et-pb-icon">&#xe0b4;</i></a></li><?php } ?>
								<?php if( $jv_team_instagram != '' ) { ?><li><a href="<?php echo $jv_team_instagram;?>"target="_blank"><i class="jv_team_member_social_font et-pb-icon">&#xe0b1;</i></a></li><?php } ?>
					</ul>
					<?php  }
					?>	
			</div>
				<?php if(( $display_view_more_button == 'on') && ( $read_more_text_btn != '')) {  echo $btn_stlink; echo $read_more_text_btn; echo $btn_endlink; } ?>
		</div> 
	</div>
</div>
<?php 
if( $display_popup_onteam =='on') {
	$divi_popupteam_select_style_layout = !empty(get_option('divi_popupteam_select_style_layout')) ? get_option('divi_popupteam_select_style_layout') : 'style1';
	if ( file_exists( $jv_grid_path_p . '/popup-'.$divi_popupteam_select_style_layout.'.php' ) ){
		 include $jv_grid_path_p. '/popup-'.$divi_popupteam_select_style_layout.'.php';
    }else{
		include JVTEAM_PLUGIN_PATH. '/content-popup/popup-'.$divi_popupteam_select_style_layout.'.php';
    }
    
} 
?>