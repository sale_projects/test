<?php
$team_members_categories = wp_get_post_terms( get_the_ID() ,'department_category', array("fields" => "ids"));
$cat_filter = '';
if ( count($team_members_categories ) > 0 ){
foreach ( $team_members_categories as $cat_id ){
		$cat_filter .= 'cat-'.$cat_id.' ';		
}
}

$link = "#";
if( $display_detail_page_link_type =='default' ) { 
		$link = get_permalink(get_the_ID());
}else if ( $display_detail_page_link_type =='custom' ){
		$url = get_post_meta( get_the_ID(),'custom_url_detail_page', true );
		$link = $url!= '' ? $url : '#';
}else{
		$link = "#";
}
				
$class_1 = '';$link_no_new_tab = '';$link_new_tab_open = '';$img_stlink = '';$target_link = '';$img_endlink = '';$btn_stlink = '';$btn_endlink = '';

// Popup ST
if( $display_popup_onteam == 'on' ) { $popup_st = '<a class="popup-modal" href="#teammodal'.get_the_ID().'">'; }
// Popup END
if( $display_popup_onteam == 'on' ) { $popup_end = '</a>'; }

// Image ST
if ($link_open_in_new_tab == 'on'){ $target_link = 'target="_blank"';}
if( $display_detail_page == 'on' && $display_popup_onteam == 'off'  ) { 
	$img_stlink = '<a href="'.$link.'" '.$target_link.'>'; 
	$btn_stlink = '<a href="'.$link.'" '.$target_link.' class="jv_team_view_more" >'; 
} else if(  $display_popup_onteam == 'on' ){ 
	$img_stlink = '<a class="popup-modal" href="#teammodal'.get_the_ID().'">';
	$btn_stlink = '<a class="popup-modal jv_team_view_more" href="#teammodal'.get_the_ID().'">';
}else{ $img_stlink = '';$btn_stlink = '';}
// Image End
if( ( $display_detail_page == 'on' && $display_popup_onteam == 'off' ) || ( $display_popup_onteam == 'on' ) ) { $img_endlink = '</a>';$btn_endlink = '</a>';}
	
?>
<div class=" team-scale-anm all et_pb_jv_team_members_list_column <?php echo rtrim($cat_filter);?> ">
	<div class="et_pb_row first_row">
		<div class="et_pb_column et_pb_column_1_3 style7_margin">
			<div class="jv_team_list_member">
				<?php 
					$jv_team_thumb = wp_get_attachment_image_src(get_post_thumbnail_id(), 'jv_team_list_image');
					$jv_team_thumb_mobile = wp_get_attachment_image_src(get_post_thumbnail_id(), 'large');
					if( $jv_team_thumb[0] != ''){ 
						$image_path = $jv_team_thumb[0] ;
						$image_path_mobile = $jv_team_thumb_mobile[0] ;
					}else{
						$image_path = JVTEAM_PLUGIN_URL . '/assets/images/default_list.png';
						$image_path_mobile = JVTEAM_PLUGIN_URL . '/assets/images/default_list.png';
					} 
				?>
				<?php echo $img_stlink;?><img src="<?php echo $image_path;?>" alt="<?php echo get_the_title();?>" class="style7_desk">	<?php echo $img_endlink;?>
				<?php echo $img_stlink;?><img src="<?php echo $image_path_mobile;?>" alt="<?php echo get_the_title();?>" class="style7_mobile" >	<?php echo $img_endlink;?>
			</div>
		</div>
		<div class="et_pb_column et_pb_column_2_3 style7_bg_color">	
			<?php echo $img_stlink;?><h3 class="jv_team_list_title"><?php echo get_the_title();?></h3>	<?php echo $img_endlink;?>	
			<div class="jv_team_list_position">
				<?php if( $jv_team_designation != ''){ ?>
					<strong><?php echo $jv_team_designation;?></strong>
				<?php } ?>
			</div>
			<div class="jv_team_info">
				<?php if( $jv_team_address != ''){ ?> 
						<div class="jv_team_list_address1"><strong>Address:&nbsp;</strong><span><?php echo rtrim($jv_team_address,',');?></span></div>
				<?php } ?>
				<?php if( $jv_team_website_url != ''){ ?> 
						<div class="website_url"><strong>Website URL:&nbsp;</strong><span><a class="website_url" href="<?php echo $jv_team_website_url;?>" target="_blank" ><?php echo $jv_team_website_url;?></a></span></div>
				<?php } ?> 				
				<?php if( $jv_team_mobile_number != ''){ ?> 
					<div class="contect_number"><strong>Phone:&nbsp;</strong><span><?php echo $jv_team_mobile_number;?></span></div>
				<?php } ?> 
				<?php if( $jv_team_email_address != ''){ ?> 
					<div class="email_address"><strong>Email:&nbsp;</strong><span><?php echo $jv_team_email_address;?></span></div>
				<?php } ?>
			</div>
			<?php 
				if(( $jv_team_facebook != '') || ( $jv_team_twitter != '') || ( $jv_team_google != '') || ( $jv_team_linkdin != '') || ( $jv_team_instagram != '')) { ?>
					<ul class="jv_team_list_social_link">
						<?php if( $jv_team_facebook != '' ) { ?><li><a href="<?php echo $jv_team_facebook;?>" target="_blank"><i class="jv_team_member_social_font et-pb-icon">&#xe093;</i></a></li><?php } ?>
						<?php if( $jv_team_twitter != '' ) { ?><li><a href="<?php echo $jv_team_twitter;?>" target="_blank"><i class="jv_team_member_social_font et-pb-icon">&#xe094;</i></a></li><?php } ?>
						<?php if( $jv_team_google != '' ) { ?><li><a href="<?php echo $jv_team_google;?>" target="_blank"><i class="jv_team_member_social_font jv_team_member_social_font_gogle et-pb-icon">&#xe096;</i></a></li><?php } ?>
						<?php if( $jv_team_linkdin != '' ) { ?><li><a href="<?php echo $jv_team_linkdin;?>"target="_blank"><i class="jv_team_member_social_font et-pb-icon">&#xe09d;</i></a></li><?php } ?>
						<?php if( $jv_team_instagram != '' ) { ?><li><a href="<?php echo $jv_team_instagram;?>"target="_blank"><i class="jv_team_member_social_font et-pb-icon">&#xe09a;</i></a></li><?php } ?>
					</ul>
				<?php 	} 
				?>
				
		</div>
	</div>
	<div class="et_pb_row second_row">
		<div class="team-expand">
				<a href="javascript:void(0);" class="jv_team_view_more team-<?php echo get_the_ID();?>"><?php echo $read_more_text_btn; ?></a>
				<div class="team-more teamcontent-<?php echo get_the_ID();?>" style="display:none;">
					<?php  echo wpautop(get_the_content());  ?>
				</div>		
		</div>
	</div>	
</div>
<?php 
if( $display_popup_onteam =='on') {
	$divi_popupteam_select_style_layout = !empty(get_option('divi_popupteam_select_style_layout')) ? get_option('divi_popupteam_select_style_layout') : 'style1';
	if ( file_exists( $jv_grid_path_p . '/popup-'.$divi_popupteam_select_style_layout.'.php' ) ){
		 include $jv_grid_path_p. '/popup-'.$divi_popupteam_select_style_layout.'.php';
    }else{
		include JVTEAM_PLUGIN_PATH. '/content-popup/popup-'.$divi_popupteam_select_style_layout.'.php';
    }
    
} 
?>
<script>
jQuery(document).ready(function() {
   jQuery(".team-<?php echo get_the_ID();?>").click(function(){		
		if ( jQuery(this).hasClass('teamopen') ){
			jQuery('.team-<?php echo get_the_ID();?>').removeClass('teamopen');
			jQuery('.team-<?php echo get_the_ID();?>').html('<?php echo $read_more_text_btn; ?>');
		} 
	    else{
			jQuery('.team-<?php echo get_the_ID();?>').html('Close');	
			jQuery('.team-<?php echo get_the_ID();?>').addClass('teamopen');		
		}
        jQuery('.teamcontent-<?php echo get_the_ID();?>').slideToggle('slow');
    });
});
</script>
	