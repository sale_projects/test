<?php
get_header();
$is_page_builder_used = et_pb_is_pagebuilder_used( get_the_ID() );
$divi_team_archive_page_layout = !empty(get_option('divi_team_archive_page_layout')) ? get_option('divi_team_archive_page_layout') : 'Grid';
$divi_team_per_page = !empty(get_option('divi_team_per_page')) ? get_option('divi_team_per_page') : '3';
$divi_team_display_pagination = !empty(get_option('divi_team_display_pagination')) ? get_option('divi_team_display_pagination') : 'Yes';
$divi_team_display_filter = !empty(get_option('divi_team_display_filter')) ? get_option('divi_team_display_filter') : 'Yes';
 ?>
<div id="main-content">
	<div class="container">
		<div id="content-area" class="clearfix">
			<div id="left-area">
		<?php	
		$ranclass = "jvteam_".rand(10,100);
		$args = array( 'posts_per_page' => (int) $posts_number );
		$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
		$args['post_type'] = 'jv_team_members';
		$args['paged'] = $paged;		
		$archive_jv_team_output = '';
	   /*Filter Option*/
		$archive_department_category_terms = get_terms( array(
			'taxonomy' => 'department_category',
			'hide_empty' => false,
		) );
		if($divi_team_display_filter == 'Yes' ){
				$archive_jv_team_output .= '<ul class="et_pb_jv_team_members_category_list "><li>
									<a href="javascript:void(0)" class="jv_filter active" data-filter=".all" ><span>All</span></a></li>';
				$archive_department_category_count = count($archive_department_category_terms);
				if ( $archive_department_category_count > 0 ){
				foreach($archive_department_category_terms as $department_categoryval){
						$archive_jv_team_output .= '<li><a href="javascript:void(0)" class="jv_filter" data-filter=".cat-'.$department_categoryval->term_id.'" ><span>'.$department_categoryval->name.'</span></a></li>';
					}
				}
				$archive_jv_team_output .= '</ul>';
		}
		$class_pagination = "";
		if( $divi_team_archive_page_layout == "Grid" ){
			//Grid
			$select_style='style37';
			$archive_jv_team_grid_class = "archive_jv_team_view_grid";
			/*Filter Option*/
			$archive_jv_team_output .= '<div class=" et_pb_jv_team_members '.$ranclass.' et_pb_jv_team_members_'.$select_style.' '.$archive_jv_team_grid_class.'" >';
			$archive_jv_team_query = new WP_Query( $args );
			//$index = 1;
			if ( $archive_jv_team_query->have_posts() ) { 
			
					  wp_enqueue_style('archive_grid', JVTEAM_PLUGIN_URL .'assets/css/archive_grid.css', array(), NULL);
					  wp_enqueue_script( 'archive_team_members_filter', JVTEAM_PLUGIN_URL . 'assets/js/archive_grid_filter.js', array('jquery'), NULL, TRUE );
					  wp_enqueue_script('jv_team_member_equalheight', JVTEAM_PLUGIN_URL .'/assets/js/jv_team_member_equalheight.js', array(), NULL);	
					  while ( $archive_jv_team_query->have_posts() ) { $archive_jv_team_query->the_post();
					 
					  // if($index % 3 == 0 && $index != 0){  $last_child_ele = ' et-last-child ';}else{$last_child_ele = '';}	
					   ob_start();
					   include JVTEAM_PLUGIN_PATH. '/content-archive-team-grid.php';
					   $archive_jv_team_output .= ob_get_contents();
					   ob_end_clean();
					   //$index++;
					}
					wp_reset_query();	
			   }
			   wp_reset_postdata();
			$archive_jv_team_output .= '</div>';
			$class_pagination = "";
		}else{
			// LIST
			$select_style='style4';
			$archive_jv_team_list_class = "archive_jv_team_view_list";
			$archive_jv_team_output .= '<div class="et_pb_module et_pb_jv_team_members_list '.$ranclass.' et_pb_jv_team_members_list_'.$select_style.' '.$archive_jv_team_list_class.'">';
			$archive_jv_team_query = new WP_Query( $args );
			if ( $archive_jv_team_query->have_posts() ) { 
				wp_enqueue_style('archive_list', JVTEAM_PLUGIN_URL .'assets/css/archive_list.css', array(), NULL);
				wp_enqueue_script( 'archive_team_members_filter', JVTEAM_PLUGIN_URL . 'assets/js/archive_team_members_list_filter.js', array('jquery'), NULL, TRUE );
					while ( $archive_jv_team_query->have_posts() ) { $archive_jv_team_query->the_post();
					   ob_start();
					   include JVTEAM_PLUGIN_PATH. '/content-archive-team-list.php';
					   $archive_jv_team_output .= ob_get_contents();
					   ob_end_clean();
					}
					wp_reset_query();
			}	
			wp_reset_postdata();		
			$archive_jv_team_output .= '</div>';
			$class_pagination = "et_pb_row";
		}
		if($divi_team_display_pagination == 'Yes'){
			$archive_jv_team_output .= '<div class="jv_team_archive_pagination '.$class_pagination.' ">';
			$big = 999999999; // need an unlikely integer
			$archive_jv_team_output .= paginate_links(array(
			'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
			'format' => '?paged=%#%',
			'current' => max( 1, get_query_var('paged') ),
			'total' => $archive_jv_team_query->max_num_pages, 
			'prev_text' => __( '<span class="et-pb-icon">&#x34;</span>', 'jvteam' ),
			'next_text' => __( '<span class="et-pb-icon">&#x35;</span>', 'jvteam' ),
			) );
			$archive_jv_team_output .= '</div>';
		}
		$archive_jv_team_output .= '<script>
		if ( jQuery(window).width() > 768 ){
				jQuery(window).load(function() {
				if( jQuery("body").find(".'.$ranclass.' .jv_team_member_equalheight").length >0 ){
					equalheight(".'.$ranclass.' .jv_team_member_equalheight");
				 }
		});
		}</script>';
		echo $archive_jv_team_output;
?>
			</div> <!-- #left-area -->
		</div> <!-- #content-area -->
	</div> <!-- .container -->
</div> <!-- #main-content -->
<?php get_footer(); ?>