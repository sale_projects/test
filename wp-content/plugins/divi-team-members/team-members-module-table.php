<?php 
function team_members_table_modules() {
	class ET_Team_Members_Table  extends ET_Builder_Module {
		function init() {
			$this->name 			=  esc_html__( 'Team Members Table', 'jvteam' );
			$this->slug 			= 'et_pb_team_members_table';
			$this->vb_support  		= 'partial';
			$this->main_css_element = '%%order_class%%.team_members_table_main';
		}
		function get_settings_modal_toggles() {
			return array(
				'general'	=> 	array(
					'toggles'	=> 	array(
						'main_content'	=> 	esc_html__( 'Settings', 'et_builder' ),
						'display_onoff'	=> 	esc_html__( 'Display ON/OFF', 'et_builder' ),
						'color_setting'	=> 	esc_html__( 'Color', 'et_builder' ),
					),
				),
			);
		}
		function get_advanced_fields_config() {
			return array(
				'fonts'	=> array(
					'team_name'	=> array(
						'label'	=> esc_html__( 'Team Name', 'et_builder' ),
						'css'	=> array(
							'main'	=> "{$this->main_css_element} .jvtmt_table > .jvtmt_name",
							'important'	=> 'all',
						),
					),
					'designation_name_fonts'	=> array(
						'label'	=> esc_html__( 'Designation Name', 'et_builder' ),
						'css'	=> array(
							'main'	=> "{$this->main_css_element} .jvtmt_table > h3.jvtmt_designation",
						),
						'font_size'	=> array(
							'default'	=> '20px',
						)
					),
					'email_fonts'	=> array(
						'label'	=> esc_html__( 'Email Name', 'et_builder' ),
						'css'	=> array(
							'main'	=> "{$this->main_css_element} .jvtmt_table > .jvtmt_email span a",
						),
						'font_size'	=> array(
							'default'	=> '15px',
						)
					),
					'phone_fonts'	=> array(
						'label'	=> esc_html__( 'Phone Name', 'et_builder' ),
						'css'	=> array(
							'main'	=> "{$this->main_css_element} .jvtmt_table > .jvtmt_mobile",
						),
						'font_size'	=> array(
							'default'	=> '15px',
						)
					),
				),
				'filters'	=> false,
				'background'	=> array(
					'use_background_image'	=>	false,
					'use_background_video'	=>	false,
				),
			);
		}
		function get_custom_css_fields_config() {
			return array(
				'team_members_table_name'	=> array(
					'label'		=> esc_html__( 'Team Members Name', 'et_builder' ),
					'selector'	=> '.jvtmt_table > .jvtmt_name',
				),
				'team_members_table_designation'	=> array(
					'Label'		=> esc_html__( 'Team Members Designation', 'et_builder' ),
					'selector'	=> '.jvtmt_table > h3.jvtmt_designation',
				),
				'team_members_table_email'	=> array(
					'label'		=> esc_html__( 'Team Members Email', 'et_builder' ),
					'selector'	=> '.jvtmt_table > .jvtmt_email span a',
				),
				'team_members_table_phone_number'	=> array(
					'label'		=> esc_html__( 'Team Members Phone Number', 'et_builder' ),
					'selector'	=> '.jvtmt_table > .jvtmt_mobile',
				),
				
			);
		}
		function get_fields() {
			$fields = array(
				'team_table_display_category'	=> array(
					'label'				=> esc_html__( 'Display Team Members By Category', 'et_builder' ),
					'type'				=> 'select',
					'option_category'	=> 'basic_option',
					 'options'			=> array(
						'all'				=> esc_html__( 'All', 'et_builder' ),
						'specificcategory'	=> esc_html__( 'Specific Category', 'et_builder' ),
					), 
					'default'  			=> 'all',
					'description'       => esc_html__( 'Here you can select the Team Members Slider display category.', 'et_builder' ),
					'toggle_slug'     	=> 'main_content',
				), 
				'include_categories'	=> array(
					'label'				=> esc_html__( 'Specific Categories', 'et_builder' ),
					'renderer'         	=> 'et_builder_include_categories_option',
					'option_category'  	=> 'basic_option',
					'show_if'			=> array('team_table_display_category' => 'specificcategory'),
					'renderer_options' 	=> array(
						'use_terms' 		=> true,
						'term_name' 		=> 'department_category',
					),
					'description'      	=> esc_html__( 'Choose which categories you would like to display team members.', 'et_builder' ),
					'toggle_slug'     	=> 'main_content',
				),
			 	'team_table_count' => array(
					'label'           	=> esc_html__( 'Display Number of Team Members(Count)', 'et_builder' ),
					'type'            	=> 'text',
					'option_category' 	=> 'basic_option',
					'default'			=> '5',
					'description'     	=> esc_html__( 'Count of Team Members to be shown.', 'et_builder' ),
					'toggle_slug'     	=> 'main_content',
				), 
				'orderby' => array(
					'label'             => esc_html__( 'Order By', 'et_builder' ),
					'type'              => 'select',
					'option_category'   => 'configuration',
					'options'           => array(
						'date_desc' 		=> esc_html__( 'Date: new to old', 'et_builder' ),
						'date_asc'  		=> esc_html__( 'Date: old to new', 'et_builder' ),
						'title_asc' 		=> esc_html__( 'Title: a-z', 'et_builder' ),
						'title_desc' 		=> esc_html__( 'Title: z-a', 'et_builder' ),
						'menu_order_desc' 	=> esc_html__( 'Menu Order : DESC', 'et_builder' ),
						'menu_order_asc' 	=> esc_html__( 'Menu Order : ASC', 'et_builder' ),
						'rand'       		=> esc_html__( 'Random', 'et_builder' ),
					),
					'default'  			=> 'date_desc',
					'description'       => esc_html__( 'Here you can adjust the order in which posts are displayed.', 'et_builder' ),
					'toggle_slug'     	=> 'main_content',
				),
				'display_popup_onteam'	=> array(
					'label'				=> esc_html__( 'Display Popup On Link', 'et_builder' ),
					'type'            	=> 'yes_no_button',			
					'options' 			=> array(
						'off' 				=> esc_html__( 'No', 'et_builder' ),
						'on'  				=> esc_html__( 'yes', 'et_builder' ),
					),
					'default'  			=> 'off',
					'description'     	=> esc_html__( 'This setting will turn on and off the display Popup on link.', 'et_builder' ),
					'toggle_slug'     => 'display_onoff',
				),
				'team_table_display_detail_page'	=> array(
					'label'           	=> esc_html__( 'Display Detail Page Link', 'et_builder' ),
					'type'            	=> 'yes_no_button',	
					'show_if'			=> array('display_popup_onteam' => 'off'),
					'options'         	=> array(
							'on'			=> esc_html__( 'yes', 'et_builder' ),
							'off'			=> esc_html__( 'No', 'et_builder' ),
					),
					'default'  			=> 'on',
					'description'     	=> esc_html__( 'This setting will turn on and off the display social link.', 'et_builder' ),
					'toggle_slug'     	=> 'display_onoff',
				),	
				'team_table_display_detail_page_link_type'	=> array(
					'label'       		=> esc_html__( 'Display Detail Page Link Type', 'et_builder' ),
					'type'           	=> 'select',
					'show_if'			=> array('team_table_display_detail_page' => 'on'),
					'option_category'	=> 'basic_option',
					'options'			=> array(
							'default'		=> esc_html__( 'Default', 'et_builder' ),
							'custom'   		=> esc_html__( 'Custom', 'et_builder' ),
						), 
					'default'  			=> 'default',
					'description'       => esc_html__( 'Here you can select the team member Display Detail Page Link Type.', 'et_builder' ),
					'toggle_slug'     	=> 'main_content',
				),
				
				
				'link_open_in_new_tab' 	=> array(
					'label'				=> esc_html__( 'Open In a New Tab', 'et_builder' ),
					'type'            	=> 'yes_no_button',	
 					'show_if'			=> array('team_table_display_detail_page' => 'on'),		
					'options'         	=> array(
						'off' 				=> esc_html__( 'No', 'et_builder' ),
						'on'  				=> esc_html__( 'yes', 'et_builder' ),
					),
					'default'  			=> 'off',
					'toggle_slug'     	=> 'display_onoff',	
					'description'     	=> esc_html__( 'This setting will turn on and off the Open In a New Tab link.', 'et_builder' ),
				),
				'team_table_display_pagination'	=> array(
					'label'           	=> esc_html__( 'Display Pagination', 'et_builder' ),
					'type'            	=> 'yes_no_button',
					'option_category' 	=> 'configuration',
					'options'         	=> array(
						'off' 				=> esc_html__( 'No', 'et_builder' ),
						'on'  				=> esc_html__( 'yes', 'et_builder' ),
					),
					'default'  			=> 'off',
					'description'     	=> esc_html__( 'This setting will turn on and off the Pagination.', 'et_builder' ),
					'toggle_slug'     	=> 'display_onoff',
				),
				'table_bg_strip1_color' => array(
					'label'        		=> esc_html__( 'Table Row Background Color(ODD)', 'et_builder' ),
					'type'         		=> 'color-alpha',
					'custom_color' 		=> true,
					'description'  		=> esc_html__( 'Use the color picker to choose a table background color odd for this module.', 'et_builder' ),
					'toggle_slug'     	=> 'color_setting',
				),
				'table_bg_strip2_color' => array(
					'label'        		=> esc_html__( 'Table Row Background Color(EVEN)', 'et_builder' ),
					'type'         		=> 'color-alpha',
					'custom_color' 		=> true,
					'description'  		=> esc_html__( 'Use the color picker to choose a table background color even for this module.', 'et_builder' ),
					'toggle_slug'     	=> 'color_setting',
				),				
				'table_bg_hover_color' 	=> array(
					'label'        		=> esc_html__( 'Table Row Background Hover Color', 'et_builder' ),
					'type'         		=> 'color-alpha',
					'custom_color' 		=> true,
					'description'  		=> esc_html__( 'Use the color picker to choose a table background hover color for this module.', 'et_builder' ),
					'toggle_slug'     	=> 'color_setting',
				),
				'table_icon_color' 		=> array(
					'label'        		=> esc_html__( 'Icon Color (Detail,Telephone,Email,Social)', 'et_builder' ),
					'type'         		=> 'color-alpha',
					'custom_color' 		=> true,
					'description'  		=> esc_html__( 'Use the color picker to choose a icon color detail,telephone,email,social for this module.', 'et_builder' ),
					'toggle_slug'     	=> 'color_setting',
				),				
				'pagination_border_color' => array(
					'label'        		=> esc_html__( 'Pagination Border Color', 'et_builder' ),
					'type'         		=> 'color-alpha',
 					'show_if'			=> array('team_table_display_pagination' => 'on'),
					'custom_color' 		=> true,
					'description'  		=> esc_html__( 'Use the color picker to choose a content pagination border color for this module.', 'et_builder' ),
					'toggle_slug'     	=> 'color_setting',
				),
				'pagination_bg_color' => array(
					'label'        		=> esc_html__( 'Pagination Background Color', 'et_builder' ),
					'type'         		=> 'color-alpha',
 					'show_if'			=> array('team_table_display_pagination' => 'on'),
					'custom_color' 		=> true,
					'description'  		=> esc_html__( 'Use the color picker to choose a pagination background color for this module.', 'et_builder' ),
					'toggle_slug'     	=> 'color_setting',
				),
				'pagination_bg_hover_color'	=> array(
					'label'        		=> esc_html__( 'Pagination Background Hover Color', 'et_builder' ),
					'type'         		=> 'color-alpha',
 					'show_if'			=> array('team_table_display_pagination' => 'on'),
					'custom_color' 		=> true,
					'description' 	 	=> esc_html__( 'Use the color picker to choose a Pagination Background Hover Color for this module.', 'et_builder' ),
					'toggle_slug'     	=> 'color_setting',
				),						
			);
			return $fields;
		}
		function render($attrs, $content = null, $render_slug ) {
			$display_popup_onteam		= $this->props['display_popup_onteam'];
			$divi_popupteam_select_style_layout	= !empty(get_option('divi_popupteam_select_style_layout')) ? get_option('divi_popupteam_select_style_layout') : 'style1';

			if ( !is_admin()){
			
				if( $display_popup_onteam == 'on') {
					
					
					$jv_template_path_p	=  get_stylesheet_directory() . '/divi-team-members';
					$jv_css_path_p		= $jv_template_path_p.'/css/popup';
					$jv_css_url_p		=  get_stylesheet_directory_uri().'/divi-team-members/css/popup'; 
					$jv_grid_path_p		=  $jv_template_path_p.'/content-popup';
				
					if ( file_exists( $jv_css_path_p . '/popup-'.$divi_popupteam_select_style_layout.'.css' ) )
					{
						wp_enqueue_style('jv_team_module_members_popup_'.$divi_popupteam_select_style_layout, $jv_css_url_p . '/popup-'.$divi_popupteam_select_style_layout.'.css', array(), NULL);
					}else{
						wp_enqueue_style('jv_team_module_members_popup_'.$divi_popupteam_select_style_layout, JVTEAM_PLUGIN_URL .'assets/css/popup/popup-'.$divi_popupteam_select_style_layout.'.css', array(), NULL);	
					}
					
					wp_enqueue_style('jv_team_module_members_popup_custom', JVTEAM_PLUGIN_URL .'assets/css/popup/popup-custom.css', array(), NULL);		
					$divi_popupteam_select_style_layout = !empty(get_option('divi_popupteam_select_style_layout')) ? get_option('divi_popupteam_select_style_layout') : 'style1';
					$divi_team_close_background_color = !empty(get_option('divi_team_close_background_color')) ? get_option('divi_team_close_background_color') : '';
					$divi_team_close_color = !empty(get_option('divi_team_close_color')) ? get_option('divi_team_close_color') : ''; 
					$divi_popupteam_title_color = !empty(get_option('divi_popupteam_title_color')) ? get_option('divi_popupteam_title_color') : '';
					$divi_popupteam_position_color = !empty(get_option('divi_popupteam_position_color')) ? get_option('divi_popupteam_position_color') : '';
					$divi_popupteam_content_color = !empty(get_option('divi_popupteam_content_color')) ? get_option('divi_popupteam_content_color') : ''; 
					$divi_popupteam_information_color = !empty(get_option('divi_popupteam_information_color')) ? get_option('divi_popupteam_information_color') : '';
					$divi_popupteam_background_color = !empty(get_option('divi_popupteam_background_color')) ? get_option('divi_popupteam_background_color') : '';
					$divi_popupteam_border_width = !empty(get_option('divi_popupteam_border_width')) ? get_option('divi_popupteam_border_width') : '1'; 
					$divi_popupteam_border_color = !empty(get_option('divi_popupteam_border_color')) ? get_option('divi_popupteam_border_color') : '';
					$divi_popupteam_boxshadow_color = !empty(get_option('divi_popupteam_boxshadow_color')) ? get_option('divi_popupteam_boxshadow_color') : '';
					$divi_popupteam_icon_color = !empty(get_option('divi_popupteam_icon_color')) ? get_option('divi_popupteam_icon_color') : '';
					$divi_popupteam_horizontal_color = !empty(get_option('divi_popupteam_horizontal_color')) ? get_option('divi_popupteam_horizontal_color') : ''; 
					$divi_popupteam_contact_us_title = !empty(get_option('divi_popupteam_contact_us_title')) ? get_option('divi_popupteam_contact_us_title') : 'Drop Me a Line'; 
					
						if( $divi_popupteam_select_style_layout == 'style1'){
							  $popup_style1 = '';
							  if ( $divi_team_close_background_color != '' ){ $popup_style1 .= ".mfp-close-btn-in .jv_popup_style1 .mfp-close,.mfp-wrap .mfp-container .jv_popup_style1  button:hover{background-color: ".$divi_team_close_background_color."!important;}"; }
							  if ( $divi_team_close_color != '' ){ $popup_style1 .= ".mfp-close-btn-in .jv_popup_style1 .mfp-close,.mfp-wrap .mfp-container .jv_popup_style1  button:hover{color: ".$divi_team_close_color."!important;}"; }
							  if ( $divi_popupteam_background_color != '' ){ $popup_style1 .= ".jv_team_popup_style1{background-color: ".$divi_popupteam_background_color."!important;}"; }
							  if ( $divi_popupteam_title_color != '' ){  $popup_style1 .= ".jv_team_popup_style1 h4.jv_team_list_title{color: ".$divi_popupteam_title_color." !important;}"; }
							  if ( $divi_popupteam_position_color != '' ){ $popup_style1 .= ".jv_team_popup_style1 .jv_team_list_position{color: ".$divi_popupteam_position_color." !important;}"; }
							  if ( $divi_popupteam_content_color != '' ){ $popup_style1 .= ".jv_team_popup_style1 .jv_team_list_content,.jv_team_popup_style1 .website_url,.jv_team_popup_style1 .contect_number,.jv_team_popup_style1 .email_address,.jv_team_popup_style1 .jv_team_list_address1{color: ".$divi_popupteam_content_color." !important;}"; }
							  if ( $divi_popupteam_border_width != '' || $divi_popupteam_border_color !== ''){ $popup_style1 .= ".jv_team_popup_style1{border: ".$divi_popupteam_border_width."px solid ".$divi_popupteam_border_color." !important;}	"; }
							  if ( $divi_popupteam_icon_color != '' ){ $popup_style1 .= ".jv_team_popup_style1 .jv_team_member_icon_font.et-pb-icon,.jv_team_popup_style1 .jv_team_member_social_font.et-pb-icon{ color: ".$divi_popupteam_icon_color." !important;}"; }
							  if ( $divi_popupteam_information_color != '' ){ $popup_style1 .= ".jv_team_popup_style1 .website_url,.jv_team_popup_style1 .contect_number,.jv_team_popup_style1 .email_address,.jv_team_popup_style1 .jv_team_list_address1{color: ".$divi_popupteam_information_color." !important;}"; }
							  if ( $divi_popupteam_boxshadow_color != '' ){ $popup_style1 .= " .jv_team_popup_style1{ box-shadow: 0 20px 20px ".$divi_popupteam_boxshadow_color." !important;}"; }
							  
							  wp_add_inline_style( 'jv_team_module_members_popup_custom', $popup_style1 );
						}
						
						if( $divi_popupteam_select_style_layout == 'style2'){
							  $popup_style2 = '';
							  if ( $divi_team_close_background_color != '' ){ $popup_style2 .= ".mfp-close-btn-in .jv_popup_style2 .mfp-close,.mfp-wrap .mfp-container .jv_popup_style2  button:hover{background-color: ".$divi_team_close_background_color." !important;}"; }
							  if ( $divi_team_close_color != '' ){ $popup_style2 .= ".mfp-close-btn-in .jv_popup_style2 .mfp-close,.mfp-wrap .mfp-container .jv_popup_style2  button:hover{color: ".$divi_team_close_color." !important;}"; }
							  if ( $divi_popupteam_background_color != '' ){ $popup_style2 .= ".jv_popup_style2 .et_pb_row{background-color: ".$divi_popupteam_background_color." !important;}"; }
							  if ( $divi_popupteam_title_color != '' ){  $popup_style2 .= ".jv_popup_style2 .jv_team_list_title h2{color: ".$divi_popupteam_title_color." !important;}"; }
							  if ( $divi_popupteam_position_color != '' ){ $popup_style2 .= ".jv_popup_style2 .jv_team_feture_box .jv_team_info h4{color: ".$divi_popupteam_position_color." !important;}"; }
							  if ( $divi_popupteam_content_color != '' ){ $popup_style2 .= ".jv_popup_style2 .jv_team_list_content{color: ".$divi_popupteam_content_color." !important;}"; }
							  if ( $divi_popupteam_icon_color != '' ){ $popup_style2 .= ".jv_popup_style2 .jv_team_member_info.et-pb-icon{ color: ".$divi_popupteam_icon_color."  !important;}.jv_popup_style2 .jv_team_list_title span,.jv_popup_style2 .jv_team_list_social_link{ background-color: ".$divi_popupteam_icon_color." !important;}"; }
							  if ( $divi_popupteam_information_color != '' ){ $popup_style2 .= ".jv_popup_style2 .jv_team_feture_box .jv_team_info span{color: ".$divi_popupteam_information_color." !important;}"; }
							  
							  wp_add_inline_style( 'jv_team_module_members_popup_custom', $popup_style2 );
						}
						
				}
			}
			$select_style    		= $this->props['select_style'];
			$module_id        		= $this->props['module_id'];
			$module_class    		= $this->props['module_class'];
			$posts_per_page 		= $this->props['team_table_count'];
			$team_table_display_category	= $this->props['team_table_display_category'];
			$include_categories 	= $this->props['include_categories'];
			$team_table_display_detail_page = $this->props['team_table_display_detail_page'];
			$team_table_display_detail_page_link_type = $this->props['team_table_display_detail_page_link_type'];
			$team_table_display_pagination 	= $this->props['team_table_display_pagination'];	
			$table_bg_strip1_color 	= $this->props['table_bg_strip1_color'];
			$table_bg_strip2_color 	= $this->props['table_bg_strip2_color'];
			$table_bg_hover_color 	= $this->props['table_bg_hover_color'];
			$table_icon_color 		= $this->props['table_icon_color'];
			$pagination_border_color 	= $this->props['pagination_border_color'];
			$pagination_bg_color 	= $this->props['pagination_bg_color'];
			$pagination_bg_hover_color 	= $this->props['pagination_bg_hover_color'];
			$orderby       			= $this->props['orderby'];		
			$link_open_in_new_tab 	= $this->props['link_open_in_new_tab'];
			$this->content	= et_builder_replace_code_content_entities( $this->content );
			$module_class = ET_Builder_Element::add_module_order_class( $module_class, $render_slug );
			$module_id = ( '' !== $module_id ? sprintf( ' id="%1$s"', esc_attr( $module_id ) ) : '' );
			$module_class = ( '' !== $module_class ? sprintf( ' %1$s', esc_attr( $module_class ) ) : '' );
			
			if( $table_bg_strip1_color !='' ){
				ET_Builder_Element::set_style( $render_slug, array(
					'selector'		=> '%%order_class%% .jvtmt_table',
					'declaration'	=> sprintf('background: %1$s;',esc_html( $table_bg_strip1_color )),
				) );
			}
			if( $table_bg_strip2_color !='' ){
				ET_Builder_Element::set_style( $render_slug, array(
					'selector'		=> '%%order_class%% .jvtmt_table:nth-of-type(even)',
					'declaration'	=> sprintf('background: %1$s;',esc_html( $table_bg_strip2_color )),
				) );
			}
			if( $table_bg_hover_color !='' ){
				ET_Builder_Element::set_style( $render_slug, array(
					'selector'		=> '%%order_class%% .jvtmt_table:hover',
					'declaration' 	=> sprintf('background: %1$s;',esc_html( $table_bg_hover_color )),
				) );	
			}
			if( $table_icon_color !='' ){
				ET_Builder_Element::set_style( $render_slug, array(
					'selector'    	=> '%%order_class%% .jvtmt_name .jvtmt_icon_color.et-pb-icon,%%order_class%% .jvtmt_email .jvtmt_icon_color.et-pb-icon,%%order_class%% .jvtmt_mobile .jvtmt_icon_color.et-pb-icon,%%order_class%% .jvtmt_links .jvtmt_social_font.et-pb-icon ',
					'declaration' 	=> sprintf('color: %1$s;',esc_html( $table_icon_color )),
				) );
			}
			if( $pagination_border_color !='' ){
				ET_Builder_Element::set_style( $render_slug, array(
					'selector'		=> '%%order_class%%.jv_team_table_archive_pagination .page-numbers',
					'declaration' 	=> sprintf('border: 2px solid %1$s;',esc_html( $pagination_border_color )),
				) );
			}
			if( $pagination_bg_color !='' ){
				ET_Builder_Element::set_style( $render_slug, array(
					'selector'    	=> '%%order_class%%.jv_team_table_archive_pagination .page-numbers',
					'declaration' 	=> sprintf('background-color: %1$s;',esc_html( $pagination_bg_color )),
				) );
			}
			if( $pagination_bg_hover_color !='' ){
				ET_Builder_Element::set_style( $render_slug, array(
					'selector'    	=> '%%order_class%%.jv_team_table_archive_pagination .page-numbers:hover',
					'declaration' 	=> sprintf('background-color: %1$s;',esc_html( $pagination_bg_hover_color )),
				) );	
				ET_Builder_Element::set_style( $render_slug, array(
					'selector'    	=> '%%order_class%%.jv_team_table_archive_pagination .page-numbers.current',
					'declaration' 	=> sprintf('background-color: %1$s;',esc_html( $pagination_bg_hover_color )),
				) );
			}
			$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
			$args['paged'] = $paged;
			$args = array(
				'post_type' => 'jv_team_members',
				'posts_per_page' => $posts_per_page,	
			);
			if ( 'date_desc' !== $orderby ) {
				switch( $orderby ) {
					case 'date_asc' :
						$args['orderby'] = 'date';
						$args['order'] = 'ASC';
						break;
					case 'title_asc' :
						$args['orderby'] = 'title';
						$args['order'] = 'ASC';
						break;
					case 'title_desc' :
						$args['orderby'] = 'title';
						$args['order'] = 'DESC';
						break;
					case 'rand' :
						$args['orderby'] = 'rand';
						break;
					case 'menu_order_desc' :
						$args['orderby'] = 'menu_order';
						$args['order'] = 'DESC';
						break;
					case 'menu_order_asc' :
						$args['orderby'] = 'menu_order';
						$args['order'] = 'ASC';
						break;
				}
			}else{
				$args['orderby'] = 'date';
				$args['order'] = 'DESC';
			}
			$include_categories_array = explode(',',$include_categories);
			if ( $team_table_display_category != 'all' ){
					$args['tax_query'] = array(
						array(
							'taxonomy' => 'department_category',
							'field' => 'term_id',
							'terms' => explode(",", $include_categories),
							'operator' => 'IN'
						)
					);
			}
			$output = '';
			$team_members_table = new WP_Query( $args );			
				if ( $team_members_table->have_posts() ) {
						$jv_template_path =  get_stylesheet_directory() . '/divi-team-members';
						$jv_css_path = $jv_template_path.'/css/archive_team_members_table_css';
						$jv_css_url =  get_stylesheet_directory_uri().'/divi-team-members/css/archive_team_members_table_css'; 
						
						$jv_grid_path =  $jv_template_path.'/content-archive-team-table';
		
		
					if(! is_admin()){
						if ( file_exists( $jv_css_path . '/content-team-table.css' ) )
						{
							wp_enqueue_style('content-team-table', $jv_css_url . '/content-team-table.css', array(), NULL);
						}else{
							wp_enqueue_style('content-team-table', JVTEAM_PLUGIN_URL . '/assets/css/archive_team_members_table_css/content-team-table.css', array(), NULL);
						}
					}	
					$output .=  '<div '.$module_id.' class="'.$module_class.' team_members_table_main">';
							while ( $team_members_table->have_posts() ) { $team_members_table->the_post();
							ob_start();
							 if ( file_exists( $jv_grid_path . '/content-team-table.php' ) ){
					   				 include $jv_grid_path. '/content-team-table.php';
							 }else{
									 include JVTEAM_PLUGIN_PATH. '/content-archive-team-table/content-team-table.php';
							 }
					   
							$output .= ob_get_contents();
							ob_end_clean();
					}
					wp_reset_query();
					$output .= '</div>';
			}
			wp_reset_postdata();
			if($team_table_display_pagination == 'on'){
				$output .= '<div class="jv_team_table_archive_pagination '.$module_class.'">';
				$big = 999999999; // need an unlikely integer
				$output .= paginate_links(array(
					'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
					'format' => '?paged=%#%',
					'current' => max( 1, get_query_var('paged') ),
					'total' => $team_members_table->max_num_pages, 
					'prev_text' => __( '<span class="et-pb-icon">&#x34;</span>', 'textdomain' ),
					'next_text' => __( '<span class="et-pb-icon">&#x35;</span>', 'textdomain' ),
				) );
				$output .= '</div>';
			}
			if( $display_popup_onteam =='on') {
					$output .= "<script>
					 jQuery(function ($) {
					 	$('a[href^=\"#teammodal\"]').addClass('et_smooth_scroll_disabled');
						$('.popup-modal').magnificPopup({
							type: 'inline',
							midClick: true,
							closeBtnInside: true
						});
					});
					 </script>";
			}
			return $output;
		}
	}
	new ET_Team_Members_Table();
}
?>