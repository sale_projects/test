<?php 
function jv_team_members_horizontal_module() {
	class ET_Team_Members_Horizontal  extends ET_Builder_Module {
		function init() {
			$this->name 			=  esc_html__( 'Team Members Horizontal View', 'et_builder' );
			$this->slug 			= 'et_pb_team_members_horizontal';
			$this->vb_support  		= 'partial';
			$this->main_css_element = '%%order_class%%.horizontal_team_members';
		}
		function get_settings_modal_toggles() {
			return array(
				'general'	=> 	array(
					'toggles'	=> 	array(
						'main_content'	=>  esc_html__( 'Settings', 'et_builder' ),
						'display_onoff'	=>	esc_html__( 'Display ON/OFF', 'et_builder' ),
						'color_setting'	=> 	esc_html__( 'Color', 'et_builder' ),
					),
				),
			);
		}	
		function get_advanced_fields_config() {
			return  array(
				'fonts' => array(
					'member_name_fonts'	=> array(
						'label'		=> esc_html__( 'Name', 'et_builder' ),
						'css'		=> array(
							'main' 		=> "{$this->main_css_element} .jv_hr_members.hr_name.jvtmhr",
						),
						'font_size' => array(
							'default'	=> '20px',
						)
					),
					'designation_name_fonts'	=> array(
						'label'		=> esc_html__( 'Designation', 'et_builder' ),
						'css'		=> array(
							'main' 		=> "{$this->main_css_element} .jv_hr_members.hr_designation.jvtmhr",
						),
						'font_size' => array(
							'default'	=> '15px',
						)
					),
					'email_fonts'	=> array(
						'label'		=> esc_html__( 'Email', 'et_builder' ),
						'css'		=> array(
							'main' 		=> "{$this->main_css_element} .jv_hr_members.jvtmhr.hr_info.email a, {$this->main_css_element} .jv_hr_members.jvtmhr.hr_info.email .et-pb-icon",
						),
						'font_size' => array(
							'default'	=> '15px',
						)
					),
					'telphone_fonts'	=> array(
						'label'		=> esc_html__( 'Telephone', 'et_builder' ),
						'css'		=> array(
							'main' 		=> "{$this->main_css_element} .jv_hr_members.jvtmhr.hr_info.telphone,{$this->main_css_element} .jv_hr_members.jvtmhr.hr_info.telphone .et-pb-icon",
						),
						'font_size' => array(
							'default'	=> '15px',
						)
					),
					'website_fonts'	=> array(
						'label'		=> esc_html__( 'Website', 'et_builder' ),
						'css'		=> array(
							'main' 		=> "{$this->main_css_element} .jv_hr_members.jvtmhr.hr_info.website a, {$this->main_css_element} .jv_hr_members.jvtmhr.hr_info.website .et-pb-icon",
						),
						'font_size' => array(
							'default'	=> '15px',
						)
					),
				),
				'custom_margin_padding' => array(
					'css' 		=> array(
						'main' 		=> "{$this->main_css_element}",
						'important' => 'all',
					),
				),
				'borders' => array(
					'default' => array(
						'css'      => array(
							'main' => array(
								'border_radii'	=> '%%order_class%%.horizontal_team_members .et_pb_jv_team',
								'border_styles'	=> '%%order_class%%.horizontal_team_members .et_pb_jv_team',
							),
						),
						'defaults' => array(
							'border_radii' 	=> 'on|0px|0px|0px|0px',
							'border_styles' => array(
									'width'		=> '1px',
									'color'		=> '#d9d9d9',
									'style'		=> 'solid',
							),
						),
					),					
				),
				'box_shadow'	=> false,
				'filters'		=> false,
				'max_width'		=> false,
				'background'	=> false,
			);
		}
		function get_custom_css_fields_config() {
			return array(
				'team_members_box' => array(
					'label'    => esc_html__( 'Team Member Box', 'et_builder' ),
					'selector' => '.jvtmhr_member',
				),
				'team_members_name' => array(
					'label'    => esc_html__( 'Name', 'et_builder' ),
					'selector' => '.jv_hr_members.hr_name.jvtmhr',
				),
				'team_member_designation' => array(
					'label'    => esc_html__( 'Designation', 'et_builder' ),
					'selector' => '.jv_hr_members.hr_designation.jvtmhr',
				),
				'team_member_social_link' => array(
					'label'    => esc_html__( 'Social Link', 'et_builder' ),
					'selector' => '.hr_links .jv_team_member_social_font.et-pb-icon',
				),
				'team_member_email' => array(
					'label'    => esc_html__( 'Email', 'et_builder' ),
					'selector' => '.jv_hr_members.jvtmhr.hr_info.email a',
				),
				'team_member_telephone' => array(
					'label'    => esc_html__( 'Telephone', 'et_builder' ),
					'selector' => '.jv_hr_members.jvtmhr.hr_info.telphone',
				),
				'team_member_website' => array(
					'label'    => esc_html__( 'website', 'et_builder' ),
					'selector' => '.jv_hr_members.jvtmhr.hr_info.website a',
				),
				'team_member_image' => array(
					'label'    => esc_html__( 'Image', 'et_builder' ),
					'selector' => '.hr_image img',
				),
			);
		}
			   
		function get_fields() {
			$fields = array(
				'team_members_display_category' => array(
					'label'				=> esc_html__( 'Display Team Members By Category', 'et_builder' ),
					'type'          	=> 'select',
					'option_category'	=> 'basic_option',
					'options'			=> array(
						'all'   			=> esc_html__( 'All', 'et_builder' ),
						'specificcategory'	=> esc_html__( 'Specific Category', 'et_builder' ),
					), 
					'default'			=> 'all',
					'description'       => esc_html__( 'Here you can select the Team Members display category.', 'et_builder' ),
					'toggle_slug'     	=> 'main_content',
				), 
				'include_categories' 	=> array(
					'label'				=> esc_html__( 'Specific Categories', 'et_builder' ),
					'renderer'			=> 'et_builder_include_categories_option',
					'option_category'	=> 'basic_option',
 					'show_if'			=> array('team_members_display_category' => 'specificcategory'),
					'renderer_options' 	=> array(
						'use_terms'			=> true,
						'term_name'			=> 'department_category',
					),
					'description'		=> esc_html__( 'Choose which categories you would like to display team members.', 'et_builder' ),
					'toggle_slug'		=> 'main_content',
				),
				'team_members_display_category_filter'   => array(
					'label'				=> esc_html__( 'Display Category Filter', 'et_builder' ),
					'type'				=> 'yes_no_button',
					'option_category'	=> 'configuration',
					'options'         	=> array(
						'on'  				=> esc_html__( 'yes', 'et_builder' ),
						'off' 				=> esc_html__( 'No', 'et_builder' ),
					),
					'default'			=> 'on',
					'description'		=> esc_html__( 'This setting will turn on and off the Category Filter.', 'et_builder' ),
					'toggle_slug'		=> 'display_onoff',
				),	
				'filter_all_label' 	=> array(
					'label'			=> esc_html__( 'ALL Text Filter Label', 'et_builder' ),
					'type'			=> 'text',
					'option_category'	=> 'configuration',
					'default'		=> 'All',
					'show_if'		=> array('team_members_display_category_filter' => 'on'),	
					'toggle_slug'	=> 'display_onoff',
					'description'	=> esc_html__( 'ALL Text Filter Label.', 'et_builder' ),
				),

				'team_members_filter_style'	=> array(
					'label'				=> esc_html__( 'Button Filter Style', 'et_builder' ),
					'type'				=> 'select',
					'option_category' 	=> 'basic_option',
 					'show_if'			=> array('team_members_display_category_filter' => 'on'),
					'options'			=> array(
						'fstyle1'			=> esc_html__( 'Style 1', 'et_builder' ),
						'fstyle2'			=> esc_html__( 'Style 2', 'et_builder' ),
						'fstyle3'			=> esc_html__( 'Style 3', 'et_builder' ),
						'fstyle4'			=> esc_html__( 'Style 4', 'et_builder' ),
						'fstyle5'			=> esc_html__( 'Style 5', 'et_builder' ),
						'fstyle6'			=> esc_html__( 'Style 6', 'et_builder' ),
						'fstyle7'			=> esc_html__( 'Style 7', 'et_builder' ),
						'fstyle8'			=> esc_html__( 'Style 8', 'et_builder' ),
						'fstyle9'			=> esc_html__( 'Style 9', 'et_builder' ),
						'fstyle10'			=> esc_html__( 'Style 10', 'et_builder' ),
						'fstyle11'			=> esc_html__( 'Style 11', 'et_builder' ),
						'fstyle12'			=> esc_html__( 'Style 12', 'et_builder' ),
						'fstyle13'			=> esc_html__( 'Style 13', 'et_builder' ),
						'fstyle14'			=> esc_html__( 'Style 14', 'et_builder' ),
						'fstyle15'			=> esc_html__( 'Style 15', 'et_builder' ),
						'fstyle16'			=> esc_html__( 'Style 16', 'et_builder' ),
						'fstyle17'			=> esc_html__( 'Style 17', 'et_builder' ),
						'fstyle18'			=> esc_html__( 'Style 18', 'et_builder' ),
						'fstyle19'			=> esc_html__( 'Style 19', 'et_builder' ),
						'fstyle20'			=> esc_html__( 'Style 20', 'et_builder' ),
					),
					'description'		=> esc_html__( 'Here you can select filter style.', 'et_builder' ),
					'toggle_slug'		=> 'display_onoff',
				), 
				'orderby'	=> array(
					'label'				=> esc_html__( 'Order By', 'et_builder' ),
					'type'              => 'select',
					'option_category'   => 'configuration',
					'options'           => array(
						'date_desc'  		=> esc_html__( 'Date: new to old', 'et_builder' ),
						'date_asc'   		=> esc_html__( 'Date: old to new', 'et_builder' ),
						'title_asc'  		=> esc_html__( 'Title: a-z', 'et_builder' ),
						'title_desc' 		=> esc_html__( 'Title: z-a', 'et_builder' ),
						'menu_order_desc' 	=> esc_html__( 'Menu Order : DESC', 'et_builder' ),
						'menu_order_asc' 	=> esc_html__( 'Menu Order : ASC', 'et_builder' ),
						'rand'       		=> esc_html__( 'Random', 'et_builder' ),
					),
					'default'			=> 'date_desc',
					'description'		=> esc_html__( 'Here you can adjust the order in which posts are displayed.', 'et_builder' ),
					'toggle_slug'     	=> 'main_content',
				 ),
				'category_orderby' => array(
					'label'				=> esc_html__( 'Category Filter List Order By', 'et_builder' ),
					'type'              => 'select',
					'option_category'   => 'configuration',
					'options'           => array(
						'id_desc'  			=> esc_html__( 'ID: High to Low', 'et_builder' ),
						'id_asc'  		 	=> esc_html__( 'ID: Low to High', 'et_builder' ),
						'name_asc'  		=> esc_html__( 'Title: a-z', 'et_builder' ),
						'name_desc' 		=> esc_html__( 'Title: z-a', 'et_builder' ),
						'slug_desc' 		=> esc_html__( 'Slug : z-a', 'et_builder' ),
						'slug_asc' 			=> esc_html__( 'Slug : a-z', 'et_builder' ),
						'count'      		=> esc_html__( 'Count', 'et_builder' ),
					),
					'default'			=> 'id_desc',
					'show_if'		=> array('team_members_display_category_filter' => 'on'),	
					'toggle_slug'	=> 'display_onoff',
					'description'       => esc_html__( 'Here you can adjust Category Filter List Order.', 'et_builder' ),
				),	
				'display_popup_onteam' => array(
					'label'           	=> esc_html__( 'Display Popup On Link', 'et_builder' ),
					'type'            	=> 'yes_no_button',			
						'options'		=> array(
						'off' 				=> esc_html__( 'No', 'et_builder' ),
						'on'  				=> esc_html__( 'yes', 'et_builder' ),
					),
					'default'			=> 'off',
					'toggle_slug'     	=> 'display_onoff',
					'description'     	=> esc_html__( 'This setting will turn on and off the display Popup on link.', 'et_builder' ),
				),
				'team_members_display_detail_page' => array(
					'label'           	=> esc_html__( 'Display Detail Page Link', 'et_builder' ),
					'type'            	=> 'yes_no_button',
 					'show_if'			=> array('display_popup_onteam' => 'off'),		
					'options'         	=> array(
						'on'  				=> esc_html__( 'yes', 'et_builder' ),
						'off' 				=> esc_html__( 'No', 'et_builder' ),
					),
					'toggle_slug'     	=> 'display_onoff',
					'description'    	=> esc_html__( 'This setting will turn on and off the display social link.', 'et_builder' ),
				  ),
				'team_members_display_detail_page_link_type' => array(
					'label'				=> esc_html__( 'Display Detail Page Link Type', 'et_builder' ),
					'type'				=> 'select',
						'show_if'		=> array('team_members_display_detail_page' => 'on'),
					'option_category'	=> 'basic_option',
					 'options'        	=> array(
						'default'   		=> esc_html__( 'Default', 'et_builder' ),
						'custom'   			=> esc_html__( 'Custom', 'et_builder' ),
					), 
					 'toggle_slug'     	=> 'display_onoff',
					'description'       => esc_html__( 'Here you can select the team member Display Detail Page Link Type.', 'et_builder' ),
				 ),
				'link_open_in_new_tab' => array(
					'label'           	=> esc_html__( 'Open In a New Tab', 'et_builder' ),
					'type'            	=> 'yes_no_button',	
 					'show_if'			=> array('team_members_display_detail_page' => 'on'),		
					'options'    	    => array(
						'off' 				=> esc_html__( 'No', 'et_builder' ),
						'on'  				=> esc_html__( 'yes', 'et_builder' ),
					),
					'toggle_slug'     	=> 'display_onoff',
					'default'			=> 'off',
					'description'     	=> esc_html__( 'This setting will turn on and off the Open In a New Tab link.', 'et_builder' ),
				),
				'team_members_count' 	=> array(
					'label'           	=> esc_html__( 'Display Number of Team Members(Count)', 'et_builder' ),
					'type'            	=> 'text',
					'option_category' 	=> 'basic_option',
					'default'			=> '-1',
					'description'     	=> esc_html__( 'Count of Team Members to be shown.', 'et_builder' ),
					'toggle_slug'     	=> 'main_content',
				), 
				'content_bg_color' 		=> array(
					'label'				=> esc_html__( 'Content Background Color', 'et_builder' ),
					'type'				=> 'color-alpha',
					'custom_color' 		=> true,
					'description'  		=> esc_html__( 'Use the color picker to choose a content background color for this module.', 'et_builder' ),
					'toggle_slug'     	=> 'color_setting',
				),
				'social_icon_color' 	=> array(
					'label'        		=> esc_html__( 'Social Icon Color', 'et_builder' ),
					'type'         		=> 'color-alpha',
					'custom_color'		=> true,
					'description' 		=> esc_html__( 'Use the color picker to choose a social icon color for this module.', 'et_builder' ),
					'toggle_slug'     	=> 'color_setting',
				),
				'style1_button_active_color' 	=> array(
					'label'        		=> esc_html__( 'Style 1 Button Filter Active Color', 'et_builder' ),
					'type'         		=> 'color-alpha',
 					'show_if'			=> array('team_members_filter_style' => 'fstyle1'),
					'toggle_slug'		=> 'color_setting',
					'custom_color'		=> true,
					'description'  		=> esc_html__( 'Use the color picker to choose a button filter active color for this module.', 'et_builder' ),
				),
				'style1_button_color' => array(
					'label'        		=> esc_html__( 'Style 1 Button Filter Color', 'et_builder' ),
					'type'        		=> 'color-alpha',
 					'show_if'			=> array('team_members_filter_style' => 'fstyle1'),
					'toggle_slug'		=> 'color_setting',
					'custom_color' 		=> true,
					'description'  		=> esc_html__( 'Use the color picker to choose a button filter color for this module.', 'et_builder' ),
				),
				'style2_button_active_color' 	=> array(
					'label'        		=> esc_html__( 'Style 2 Button Filter Active Color', 'et_builder' ),
					'type'         		=> 'color-alpha',
 					'show_if'			=> array('team_members_filter_style' => 'fstyle2'),
					'toggle_slug'		=> 'color_setting',
					'custom_color' 		=> true,
					'description'  		=> esc_html__( 'Use the color picker to choose a button filter active color color for this module.', 'et_builder' ),
				),
				'style2_button_animation_border_color' => array(
					'label'        		=> esc_html__( 'Style 2 Button Filter Animation Border Color', 'et_builder' ),
					'type'         		=> 'color-alpha',
 					'show_if'			=> array('team_members_filter_style' => 'fstyle2'),
					'toggle_slug'		=> 'color_setting',
					'custom_color' 		=> true,
					'description' 		=> esc_html__( 'Use the color picker to choose a button filter animation border color color for this module.', 'et_builder' ),
				),
				'style2_button_hover_color' 	=> array(
					'label'        		=> esc_html__( 'Style 2 Button Filter Border Color & Button Filter hover color', 'et_builder' ),
					'type'         		=> 'color-alpha',
 					'show_if'			=> array('team_members_filter_style' => 'fstyle2'),
					'toggle_slug'		=> 'color_setting',
					'custom_color' 		=> true,
					'description'  		=> esc_html__( 'Use the color picker to choose a button filter border color & button filter hover color for this module.', 'et_builder' ),
				),
				'style3_button_border_color' 	=> array(
					'label'        		=> esc_html__( 'Style 3 Button Filter Border Color', 'et_builder' ),
					'type'         		=> 'color-alpha',
 					'show_if'			=> array('team_members_filter_style' => 'fstyle3'),
					'toggle_slug'		=> 'color_setting',
					'custom_color' 		=> true,
					'description'  		=> esc_html__( 'Use the color picker to choose a read more button filter border color for this module.', 'et_builder' ),
				),
				'style3_button_color' 	=> array(
					'label'        		=> esc_html__( 'Style 3 Button Filter Color', 'et_builder' ),
					'type'         		=> 'color-alpha',
 					'show_if'			=> array('team_members_filter_style' => 'fstyle3'),
					'toggle_slug'		=> 'color_setting',
					'custom_color'		=> true,
					'description'  		=> esc_html__( 'Use the color picker to choose a button filter color for this module.', 'et_builder' ),
				),
				'style3_button_hover_color'		=> array(
					'label'        		=> esc_html__( 'Style 3 Button Filter hover Color', 'et_builder' ),
					'type'         		=> 'color-alpha',
 					'show_if'			=> array('team_members_filter_style' => 'fstyle3'),
					'toggle_slug'		=> 'color_setting',
					'custom_color' 		=> true,
					'description'  		=> esc_html__( 'Use the color picker to choose a button filter hover color for this module.', 'et_builder' ),
				),
				'style4_button_border_color' => array(
					'label'        		=> esc_html__( 'Style 4 Button Filter Border Color', 'et_builder' ),
					'type'         		=> 'color-alpha',
 					'show_if'			=> array('team_members_filter_style' => 'fstyle4'),
					'toggle_slug'		=> 'color_setting',
					'custom_color' 		=> true,
					'description'  		=> esc_html__( 'Use the color picker to choose a button filter border color for this module.', 'et_builder' ),
				),
				'style4_button_color' 	=> array(
					'label'        		=> esc_html__( 'Style 4 Button Filter Color', 'et_builder' ),
					'type'         		=> 'color-alpha',
 					'show_if'			=> array('team_members_filter_style' => 'fstyle4'),
					'toggle_slug'		=> 'color_setting',
					'custom_color' 		=> true,
					'description'  		=> esc_html__( 'Use the color picker to choose a button filter color for this module.', 'et_builder' ),
				),
				'style5_button_border_active_color' => array(
					'label'        		=> esc_html__( 'Style 5 Button Filter Border Active Color', 'et_builder' ),
					'type'         		=> 'color-alpha',
 					'show_if'			=> array('team_members_filter_style' => 'fstyle5'),
					'toggle_slug'		=> 'color_setting',
					'custom_color' 		=> true,
					'description'  		=> esc_html__( 'Use the color picker to choose a button filter border active color for this module.', 'et_builder' ),
				),
				'style5_button_border_color' 	=> array(
					'label'        		=> esc_html__( 'Style 5 Button Filter Border Color', 'et_builder' ),
					'type'         		=> 'color-alpha',
 					'show_if'			=> array('team_members_filter_style' => 'fstyle5'),
					'toggle_slug'		=> 'color_setting',
					'custom_color' 		=> true,
					'description'  		=> esc_html__( 'Use the color picker to choose a button filter border color for this module.', 'et_builder' ),
				),
				'style6_button_active_color' 	=> array(
					'label'        		=> esc_html__( 'Style 6 Button Filter Active Color', 'et_builder' ),
					'type'         		=> 'color-alpha',
 					'show_if'			=> array('team_members_filter_style' => 'fstyle6'),
					'toggle_slug'		=> 'color_setting',
					'custom_color' 		=> true,
					'description'  		=> esc_html__( 'Use the color picker to choose a button filter active color for this module.', 'et_builder' ),
				),
				'style6_button_color' 	=> array(
					'label'        		=> esc_html__( 'Style 6 Button Filter Color', 'et_builder' ),
					'type'         		=> 'color-alpha',
 					'show_if'			=> array('team_members_filter_style' => 'fstyle6'),
					'toggle_slug'		=> 'color_setting',
					'custom_color' 		=> true,
					'description'  		=> esc_html__( 'Use the color picker to choose a button filter color for this module.', 'et_builder' ),
				),
				'style7_button_border_active_color' 	=> array(
					'label'        		=> esc_html__( 'Style 7 Button Filter Border Active Color', 'et_builder' ),
					'type'         		=> 'color-alpha',
 					'show_if'			=> array('team_members_filter_style' => 'fstyle7'),
					'toggle_slug'		=> 'color_setting',
					'custom_color' 		=> true,
					'description'  		=> esc_html__( 'Use the color picker to choose a button filter border active color for this module.', 'et_builder' ),
				),
				'style7_button_border_color' 	=> array(
					'label'        		=> esc_html__( 'Style 7 Button Filter Border Color', 'et_builder' ),
					'type'         		=> 'color-alpha',
 					'show_if'			=> array('team_members_filter_style' => 'fstyle7'),
					'toggle_slug'		=> 'color_setting',
					'custom_color' 		=> true,
					'description'  		=> esc_html__( 'Use the color picker to choose a button filter border color for this module.', 'et_builder' ),
				),
				'style8_button_active_color' 	=> array(
					'label'        		=> esc_html__( 'Style 8 Button Filter Active Color', 'et_builder' ),
					'type'         		=> 'color-alpha',
 					'show_if'			=> array('team_members_filter_style' => 'fstyle8'),
					'toggle_slug'		=> 'color_setting',
					'custom_color' 		=> true,
					'description'  		=> esc_html__( 'Use the color picker to choose a button filter active color for this module.', 'et_builder' ),
				),
				'style8_button_border_color' 	=> array(
					'label'       		=> esc_html__( 'Style 8 Button Filter Border Color', 'et_builder' ),
					'type'        		=> 'color-alpha',
 					'show_if'			=> array('team_members_filter_style' => 'fstyle8'),
					'toggle_slug'		=> 'color_setting',
					'custom_color' 		=> true,
					'description'  		=> esc_html__( 'Use the color picker to choose a button filter border color for this module.', 'et_builder' ),
				),
				'style9_button_active_color' 	=> array(
					'label'        		=> esc_html__( 'Style 9 Button Filter Active Color', 'et_builder' ),
					'type'        		=> 'color-alpha',
 					'show_if'			=> array('team_members_filter_style' => 'fstyle9'),
					'toggle_slug'		=> 'color_setting',
					'custom_color' 		=> true,
					'description'  		=> esc_html__( 'Use the color picker to choose a button filter active color for this module.', 'et_builder' ),
				),
				'style9_button_color' 	=> array(
					'label'       		=> esc_html__( 'Style 9 Button Filter Color', 'et_builder' ),
					'type'         		=> 'color-alpha',
 					'show_if'			=> array('team_members_filter_style' => 'fstyle9'),
					'toggle_slug'		=> 'color_setting',
					'custom_color' 		=> true,
					'description'  		=> esc_html__( 'Use the color picker to choose a button filter color for this module.', 'et_builder' ),
				),
				'style10_button_active_border_color' 	=> array(
					'label'        		=> esc_html__( 'Style 10 Button Filter Active Border Color', 'et_builder' ),
					'type'         		=> 'color-alpha',
 					'show_if'			=> array('team_members_filter_style' => 'fstyle10'),
					'toggle_slug'		=> 'color_setting',
					'custom_color' 		=> true,
					'description'  		=> esc_html__( 'Use the color picker to choose a button filter active border color for this module.', 'et_builder' ),
				),
				'style10_button_color' 	=> array(
					'label'        		=> esc_html__( 'Style 10 Button Filter Color', 'et_builder' ),
					'type'         		=> 'color-alpha',
 					'show_if'			=> array('team_members_filter_style' => 'fstyle10'),
					'toggle_slug'		=> 'color_setting',
					'custom_color' 		=> true,
					'description'  		=> esc_html__( 'Use the color picker to choose a button filter color for this module.', 'et_builder' ),
				),
				'style11_button_active_color' 	=> array(
					'label'        		=> esc_html__( 'Style 11 Button Filter Active Color', 'et_builder' ),
					'type'         		=> 'color-alpha',
 					'show_if'			=> array('team_members_filter_style' => 'fstyle11'),
					'toggle_slug'		=> 'color_setting',
					'custom_color' 		=> true,
					'description'  		=> esc_html__( 'Use the color picker to choose a button filter active color for this module.', 'et_builder' ),
				),
				'style11_button_color' 	=> array(
					'label'        		=> esc_html__( 'Style 11 Button Filter Hover Color & Border Color', 'et_builder' ),
					'type'         		=> 'color-alpha',
 					'show_if'			=> array('team_members_filter_style' => 'fstyle11'),
					'toggle_slug'		=> 'color_setting',
					'custom_color' 		=> true,
					'description'  		=> esc_html__( 'Use the color picker to choose a button filter color & border color for this module.', 'et_builder' ),
				),
				'style12_button_active_border_color' 	=> array(
					'label'        		=> esc_html__( 'Style 12 Button Filter Active Border Color', 'et_builder' ),
					'type'         		=> 'color-alpha',
 					'show_if'			=> array('team_members_filter_style' => 'fstyle12'),
					'toggle_slug'		=> 'color_setting',
					'custom_color' 		=> true,
					'description'  		=> esc_html__( 'Use the color picker to choose a button filter active border color for this module.', 'et_builder' ),
				),
				'style12_button_border_color' 	=> array(
					'label'        		=> esc_html__( 'Style 12 Button Filter Border Color', 'et_builder' ),
					'type'         		=> 'color-alpha',
 					'show_if'			=> array('team_members_filter_style' => 'fstyle12'),
					'toggle_slug'		=> 'color_setting',
					'custom_color' 		=> true,
					'description'  		=> esc_html__( 'Use the color picker to choose a button filter border color for this module.', 'et_builder' ),
				),
				'style13_button_active_color' 	=> array(
					'label'        		=> esc_html__( 'Style 13 Button Filter Active Color', 'et_builder' ),
					'type'         		=> 'color-alpha',
 					'custom_color' 		=> true,
					'show_if'			=> array('team_members_filter_style' => 'fstyle13'),
					'toggle_slug'		=> 'color_setting',
					'description'  		=> esc_html__( 'Use the color picker to choose a button filter active color for this module.', 'et_builder' ),
				),
				'style13_button_active_hover_color' 	=> array(
					'label'        		=> esc_html__( 'Style 13 Button Filter Active Hover Color', 'et_builder' ),
					'type'         		=> 'color-alpha',
 					'show_if'			=> array('team_members_filter_style' => 'fstyle13'),
					'toggle_slug'		=> 'color_setting',
					'custom_color' 		=> true,
					'description'  		=> esc_html__( 'Use the color picker to choose a button filter active hover color for this module.', 'et_builder' ),
				),
				'style13_button_hover_color' 	=> array(
					'label'       		=> esc_html__( 'Style 13 Button Filter Border Color & Button Filter Hover Color', 'et_builder' ),
					'type'         		=> 'color-alpha',
 					'show_if'			=> array('team_members_filter_style' => 'fstyle13'),
					'toggle_slug'		=> 'color_setting',
					'custom_color' 		=> true,
					'description'  		=> esc_html__( 'Use the color picker to choose a button filter border color & button filter hover color for this module.', 'et_builder' ),
				),
				'style14_button_active_color' 	=> array(
					'label'        		=> esc_html__( 'Style 14 Button Filter Active Color', 'et_builder' ),
					'type'         		=> 'color-alpha',
 					'show_if'			=> array('team_members_filter_style' => 'fstyle14'),
					'toggle_slug'		=> 'color_setting',
					'custom_color' 		=> true,
					'description'  		=> esc_html__( 'Use the color picker to choose a button filter active color for this module.', 'et_builder' ),
				),
				'style14_button_color' 	=> array(
					'label'        		=> esc_html__( 'Style 14 Button Filter Color', 'et_builder' ),
					'type'         		=> 'color-alpha',
 					'show_if'			=> array('team_members_filter_style' => 'fstyle14'),
					'toggle_slug'		=> 'color_setting',
					'custom_color' 		=> true,
					'description'  		=> esc_html__( 'Use the color picker to choose a button filter active color for this module.', 'et_builder' ),
				),
				'style15_button_active_color' => array(
					'label'        		=> esc_html__( 'Style 15 Button Filter Active Color', 'et_builder' ),
					'type'         		=> 'color-alpha',
 					'show_if'			=> array('team_members_filter_style' => 'fstyle15'),
					'toggle_slug'		=> 'color_setting',
					'custom_color' 		=> true,
					'description'  		=> esc_html__( 'Use the color picker to choose a button filter active color for this module.', 'et_builder' ),
				),
				'style15_button_active_hover_color' => array(
					'label'        		=> esc_html__( 'Style 15 Button Filter Active Hover Color', 'et_builder' ),
					'type'         		=> 'color-alpha',
 					'show_if'			=> array('team_members_filter_style' => 'fstyle15'),
					'toggle_slug'		=> 'color_setting',
					'custom_color' 		=> true,
					'description'  		=> esc_html__( 'Use the color picker to choose a button filter active hover color for this module.', 'et_builder' ),
				),
				'style15_button_hover_color' => array(
					'label'        		=> esc_html__( 'Style 15 Button Filter Border Color & Button Filter Hover Color', 'et_builder' ),
					'type'         		=> 'color-alpha',
 					'show_if'			=> array('team_members_filter_style' => 'fstyle15'),
					'toggle_slug'		=> 'color_setting',
					'custom_color' 		=> true,
					'description'  		=> esc_html__( 'Use the color picker to choose a button filter border color & button filter hover color for this module.', 'et_builder' ),
				),
				'style16_button_active_color' 	=> array(
					'label'        		=> esc_html__( 'Style 16 Button Filter Active Color', 'et_builder' ),
					'type'         		=> 'color-alpha',
 					'show_if'			=> array('team_members_filter_style' => 'fstyle16'),
					'toggle_slug'		=> 'color_setting',
					'custom_color' 		=> true,
					'description'  		=> esc_html__( 'Use the color picker to choose a button filter active color for this module.', 'et_builder' ),
				),
				'style16_button_hover_color' 	=> array(
					'label'        		=> esc_html__( 'Style 16 Button Filter Hover Color', 'et_builder' ),
					'type'         		=> 'color-alpha',
 					'custom_color' 		=> true,
					'show_if'			=> array('team_members_filter_style' => 'fstyle16'),
					'toggle_slug'		=> 'color_setting',
					'description'  		=> esc_html__( 'Use the color picker to choose a button filter hover color for this module.', 'et_builder' ),
				),
				'style16_button_border_color' => array(
					'label'        		=> esc_html__( 'Style 16 Button Filter Border Color', 'et_builder' ),
					'type'         		=> 'color-alpha',
 					'show_if'			=> array('team_members_filter_style' => 'fstyle16'),
					'toggle_slug'		=> 'color_setting',
					'custom_color' 		=> true,
					'description'  		=> esc_html__( 'Use the color picker to choose a button filter border color for this module.', 'et_builder' ),
				),
				'style17_button_active_color' 	=> array(
					'label'        		=> esc_html__( 'Style 17 Button Filter Active Color', 'et_builder' ),
					'type'         		=> 'color-alpha',
 					'show_if'			=> array('team_members_filter_style' => 'fstyle17'),
					'toggle_slug'		=> 'color_setting',
					'custom_color' 		=> true,
					'description'  		=> esc_html__( 'Use the color picker to choose a button filter active color for this module.', 'et_builder' ),
				),
				'style17_button_border_color' 	=> array(
					'label'        		=> esc_html__( 'Style 17 Button Filter Border Color', 'et_builder' ),
					'type'         		=> 'color-alpha',
 					'show_if'			=> array('team_members_filter_style' => 'fstyle17'),
					'toggle_slug'		=> 'color_setting',
					'custom_color' 		=> true,
					'description'  		=> esc_html__( 'Use the color picker to choose a button filter border color for this module.', 'et_builder' ),
				),
				'style17_button_hover_color' 	=> array(
					'label'        		=> esc_html__( 'Style 17 Button Filter Hover Color', 'et_builder' ),
					'type'         		=> 'color-alpha',
 					'show_if'			=> array('team_members_filter_style' => 'fstyle17'),
					'toggle_slug'		=> 'color_setting',
					'custom_color' 		=> true,
					'description'  		=> esc_html__( 'Use the color picker to choose a button filter hover color for this module.', 'et_builder' ),
				),
				'style18_button_border_color' 	=> array(
					'label'        		=> esc_html__( 'Style 18 Button Filter Border Color', 'et_builder' ),
					'type'         		=> 'color-alpha',
 					'show_if'			=> array('team_members_filter_style' => 'fstyle18'),
					'toggle_slug'		=> 'color_setting',
					'custom_color' 		=> true,
					'description'  		=> esc_html__( 'Use the color picker to choose a button filter border color for this module.', 'et_builder' ),
				),
				'style18_button_color' 	=> array(
					'label'        		=> esc_html__( 'Style 18 Button Filter Color', 'et_builder' ),
					'type'         		=> 'color-alpha',
 					'show_if'			=> array('team_members_filter_style' => 'fstyle18'),
					'toggle_slug'		=> 'color_setting',
					'custom_color' 		=> true,
					'description'  		=> esc_html__( 'Use the color picker to choose a button filter color for this module.', 'et_builder' ),
				),
				'style19_button_gardien1_color' 	=> array(
					'label'        		=> esc_html__( 'Style 19 Button Filter Gardien Color 1', 'et_builder' ),
					'type'         		=> 'color-alpha',
 					'show_if'			=> array('team_members_filter_style' => 'fstyle19'),
					'toggle_slug'		=> 'color_setting',
					'custom_color' 		=> true,
					'description'  		=> esc_html__( 'Use the color picker to choose a button filter gardien color 1 for this module.', 'et_builder' ),
				),
				'style19_button_gardien2_color' 	=> array(
					'label'        		=> esc_html__( 'Style 19 Button Filter Gardien Color 2', 'et_builder' ),
					'type'         		=> 'color-alpha',
 					'show_if'			=> array('team_members_filter_style' => 'fstyle19'),
					'toggle_slug'		=> 'color_setting',
					'custom_color' 		=> true,
					'description'  		=> esc_html__( 'Use the color picker to choose a button filter gardien color 2 for this module.', 'et_builder' ),
				),
				'style19_button_gardien3_color' 	=> array(
					'label'        		=> esc_html__( 'Style 19 Button Filter Gardien Color 3', 'et_builder' ),
					'type'         		=> 'color-alpha',
 					'show_if'			=> array('team_members_filter_style' => 'fstyle19'),
					'toggle_slug'		=> 'color_setting',
					'custom_color' 		=> true,
					'description'  		=> esc_html__( 'Use the color picker to choose a button filter gardien color 3 for this module.', 'et_builder' ),
				),
				'style19_button_gardien4_color' 	=> array(
					'label'        		=> esc_html__( 'Style 19 Button Filter Gardien Color 4', 'et_builder' ),
					'type'         		=> 'color-alpha',
 					'show_if'			=> array('team_members_filter_style' => 'fstyle19'),
					'toggle_slug'		=> 'color_setting',
					'custom_color' 		=> true,
					'description'  		=> esc_html__( 'Use the color picker to choose a button filter gardien color 4 for this module.', 'et_builder' ),
				),
				'style19_button_active_border_color' 	=> array(
					'label'        		=> esc_html__( 'Style 19 Button Filter active border color', 'et_builder' ),
					'type'         		=> 'color-alpha',
 					'show_if'			=> array('team_members_filter_style' => 'fstyle19'),
					'toggle_slug'		=> 'color_setting',
					'custom_color' 		=> true,
					'description'  		=> esc_html__( 'Use the color picker to choose a button filter active border color  for this module.', 'et_builder' ),
				),
				'style20_button_border_color' 	=> array(
					'label'        		=> esc_html__( 'Style 20 Button Filter Border Color', 'et_builder' ),
					'type'         		=> 'color-alpha',
 					'show_if'			=> array('team_members_filter_style' => 'fstyle20'),
					'toggle_slug'		=> 'color_setting',
					'custom_color' 		=> true,
					'description'  		=> esc_html__( 'Use the color picker to choose a button filter border color for this module.', 'et_builder' ),
				),
				'button_name_text_color'	=> array(
					'label'        		=> esc_html__( 'Button Filter Text Color', 'et_builder' ),
					'type'         		=> 'color-alpha',
					'custom_color' 		=> true,
					'toggle_slug'		=> 'color_setting',
					'description'  		=> esc_html__( 'Use the color picker to choose a button filter text color for this module.', 'et_builder' ),
				),
				'button_text_hover_color'	=> array(
					'label'        		=> esc_html__( 'Button Text Filter Hover Color', 'et_builder' ),
					'type'         		=> 'color-alpha',
					'custom_color' 		=> true,
					'toggle_slug'		=> 'color_setting',
					'description'  		=> esc_html__( 'Use the color picker to choose a button filter text hover color for this module.', 'et_builder' ),
				),
			
			);
			return $fields;
		}
		function render( $attrs, $content = null, $render_slug ) {

			$display_popup_onteam      = $this->props['display_popup_onteam'];
			$divi_popupteam_select_style_layout = !empty(get_option('divi_popupteam_select_style_layout')) ? get_option('divi_popupteam_select_style_layout') : 'style1';

			if ( !is_admin()){
				if( $display_popup_onteam == 'on') {
					
					$jv_template_path_p =  get_stylesheet_directory() . '/divi-team-members';
					$jv_css_path_p = $jv_template_path_p.'/css/popup';
					$jv_css_url_p =  get_stylesheet_directory_uri().'/divi-team-members/css/popup'; 
					
					$jv_grid_path_p =  $jv_template_path_p.'/content-popup';
				
					if ( file_exists( $jv_css_path_p . '/popup-'.$divi_popupteam_select_style_layout.'.css' ) )
					{
						wp_enqueue_style('jv_team_module_members_popup_'.$divi_popupteam_select_style_layout, $jv_css_url_p . '/popup-'.$divi_popupteam_select_style_layout.'.css', array(), NULL);
					}else{
						wp_enqueue_style('jv_team_module_members_popup_'.$divi_popupteam_select_style_layout, JVTEAM_PLUGIN_URL .'assets/css/popup/popup-'.$divi_popupteam_select_style_layout.'.css', array(), NULL);	
					}
					
					wp_enqueue_style('jv_team_module_members_popup_custom', JVTEAM_PLUGIN_URL .'assets/css/popup/popup-custom.css', array(), NULL);		
					$divi_popupteam_select_style_layout = !empty(get_option('divi_popupteam_select_style_layout')) ? get_option('divi_popupteam_select_style_layout') : 'style1';
					$divi_team_close_background_color = !empty(get_option('divi_team_close_background_color')) ? get_option('divi_team_close_background_color') : '';
					$divi_team_close_color = !empty(get_option('divi_team_close_color')) ? get_option('divi_team_close_color') : ''; 
					$divi_popupteam_title_color = !empty(get_option('divi_popupteam_title_color')) ? get_option('divi_popupteam_title_color') : '';
					$divi_popupteam_position_color = !empty(get_option('divi_popupteam_position_color')) ? get_option('divi_popupteam_position_color') : '';
					$divi_popupteam_content_color = !empty(get_option('divi_popupteam_content_color')) ? get_option('divi_popupteam_content_color') : ''; 
					$divi_popupteam_information_color = !empty(get_option('divi_popupteam_information_color')) ? get_option('divi_popupteam_information_color') : '';
					$divi_popupteam_background_color = !empty(get_option('divi_popupteam_background_color')) ? get_option('divi_popupteam_background_color') : '';
					$divi_popupteam_border_width = !empty(get_option('divi_popupteam_border_width')) ? get_option('divi_popupteam_border_width') : '1'; 
					$divi_popupteam_border_color = !empty(get_option('divi_popupteam_border_color')) ? get_option('divi_popupteam_border_color') : '';
					$divi_popupteam_boxshadow_color = !empty(get_option('divi_popupteam_boxshadow_color')) ? get_option('divi_popupteam_boxshadow_color') : '';
					$divi_popupteam_icon_color = !empty(get_option('divi_popupteam_icon_color')) ? get_option('divi_popupteam_icon_color') : '';
					$divi_popupteam_horizontal_color = !empty(get_option('divi_popupteam_horizontal_color')) ? get_option('divi_popupteam_horizontal_color') : ''; 
					$divi_popupteam_contact_us_title = !empty(get_option('divi_popupteam_contact_us_title')) ? get_option('divi_popupteam_contact_us_title') : 'Drop Me a Line'; 
					
						if( $divi_popupteam_select_style_layout == 'style1'){
							  $popup_style1 = '';
							  if ( $divi_team_close_background_color != '' ){ $popup_style1 .= ".mfp-close-btn-in .jv_popup_style1 .mfp-close,.mfp-wrap .mfp-container .jv_popup_style1  button:hover{background-color: ".$divi_team_close_background_color." !important;}"; }
							  if ( $divi_team_close_color != '' ){ $popup_style1 .= ".mfp-close-btn-in .jv_popup_style1 .mfp-close,.mfp-wrap .mfp-container .jv_popup_style1  button:hover{color: ".$divi_team_close_color." !important;}"; }
							  if ( $divi_popupteam_background_color != '' ){ $popup_style1 .= ".jv_team_popup_style1{background-color: ".$divi_popupteam_background_color." !important;}"; }
							  if ( $divi_popupteam_title_color != '' ){  $popup_style1 .= ".jv_team_popup_style1 h4.jv_team_list_title{color: ".$divi_popupteam_title_color." !important;}"; }
							  if ( $divi_popupteam_position_color != '' ){ $popup_style1 .= ".jv_team_popup_style1 .jv_team_list_position{color: ".$divi_popupteam_position_color."  !important;}"; }
							  if ( $divi_popupteam_content_color != '' ){ $popup_style1 .= ".jv_team_popup_style1 .jv_team_list_content,.jv_team_popup_style1 .website_url,.jv_team_popup_style1 .contect_number,.jv_team_popup_style1 .email_address,.jv_team_popup_style1 .jv_team_list_address1{color: ".$divi_popupteam_content_color."  !important;}"; }
							  if ( $divi_popupteam_border_width != '' || $divi_popupteam_border_color !== ''){ $popup_style1 .= ".jv_team_popup_style1{border: ".$divi_popupteam_border_width."px solid ".$divi_popupteam_border_color." !important;}	"; }
							  if ( $divi_popupteam_icon_color != '' ){ $popup_style1 .= ".jv_team_popup_style1 .jv_team_member_icon_font.et-pb-icon,.jv_team_popup_style1 .jv_team_member_social_font.et-pb-icon{ color: ".$divi_popupteam_icon_color." !important;}"; }
							  if ( $divi_popupteam_information_color != '' ){ $popup_style1 .= ".jv_team_popup_style1 .website_url,.jv_team_popup_style1 .contect_number,.jv_team_popup_style1 .email_address,.jv_team_popup_style1 .jv_team_list_address1{color: ".$divi_popupteam_information_color." !important;}"; }
							  if ( $divi_popupteam_boxshadow_color != '' ){ $popup_style1 .= " .jv_team_popup_style1{ box-shadow: 0 20px 20px ".$divi_popupteam_boxshadow_color."  !important;}"; }
							  
							  wp_add_inline_style( 'jv_team_module_members_popup_custom', $popup_style1 );
						}
						
						if( $divi_popupteam_select_style_layout == 'style2'){
							  $popup_style2 = '';
							  if ( $divi_team_close_background_color != '' ){ $popup_style2 .= ".mfp-close-btn-in .jv_popup_style2 .mfp-close,.mfp-wrap .mfp-container .jv_popup_style2  button:hover{background-color: ".$divi_team_close_background_color." !important;}"; }
							  if ( $divi_team_close_color != '' ){ $popup_style2 .= ".mfp-close-btn-in .jv_popup_style2 .mfp-close,.mfp-wrap .mfp-container .jv_popup_style2  button:hover{color: ".$divi_team_close_color." !important;}"; }
							  if ( $divi_popupteam_background_color != '' ){ $popup_style2 .= ".jv_popup_style2 .et_pb_row{background-color: ".$divi_popupteam_background_color."!important;}"; }
							  if ( $divi_popupteam_title_color != '' ){  $popup_style2 .= ".jv_popup_style2 .jv_team_list_title h2{color: ".$divi_popupteam_title_color." !important;}"; }
							  if ( $divi_popupteam_position_color != '' ){ $popup_style2 .= ".jv_popup_style2 .jv_team_feture_box .jv_team_info h4{color: ".$divi_popupteam_position_color." !important;}"; }
							  if ( $divi_popupteam_content_color != '' ){ $popup_style2 .= ".jv_popup_style2 .jv_team_list_content{color: ".$divi_popupteam_content_color." !important;}"; }
							  if ( $divi_popupteam_icon_color != '' ){ $popup_style2 .= ".jv_popup_style2 .jv_team_member_info.et-pb-icon{ color: ".$divi_popupteam_icon_color."  !important;}.jv_popup_style2 .jv_team_list_title span,.jv_popup_style2 .jv_team_list_social_link{ background-color: ".$divi_popupteam_icon_color." !important;}"; }
							  if ( $divi_popupteam_information_color != '' ){ $popup_style2 .= ".jv_popup_style2 .jv_team_feture_box .jv_team_info span{color: ".$divi_popupteam_information_color." !important;}"; }
							  
							  wp_add_inline_style( 'jv_team_module_members_popup_custom', $popup_style2 );
						}
						
				}
			}
				$filter_all_label      = $this->props['filter_all_label'];
				$filter_style   						  = $this->props['team_members_filter_style'];
				$team_orderby                 				  = $this->props['orderby'];	
				$category_orderby                  = $this->props['category_orderby'];
				$team_members_display_category_filter     = $this->props['team_members_display_category_filter'];	
			    $posts_per_page                           = $this->props['team_members_count'];
				$team_members_display_detail_page     	  = $this->props['team_members_display_detail_page'];
				$team_members_display_detail_page_link_type   = $this->props['team_members_display_detail_page_link_type'];
				$team_members_display_category            = $this->props['team_members_display_category'];
				$include_categories 					  = $this->props['include_categories'];
				$content_bg_color          		 		  = $this->props['content_bg_color'];
				$social_icon_color          		      = $this->props['social_icon_color'];
				$style1_button_active_color  = $this->props['style1_button_active_color'];
				$style1_button_color  = $this->props['style1_button_color'];
				$style2_button_active_color = $this->props['style2_button_active_color'];
				$style2_button_animation_border_color = $this->props['style2_button_animation_border_color'];
				$style2_button_hover_color = $this->props['style2_button_hover_color'];
				$style3_button_border_color  = $this->props['style3_button_border_color'];
				$style3_button_color  = $this->props['style3_button_color'];
				$style3_button_hover_color  = $this->props['style3_button_hover_color'];
				$style4_button_border_color  = $this->props['style4_button_border_color'];
				$style4_button_color  = $this->props['style4_button_color'];
				$style5_button_border_active_color  = $this->props['style5_button_border_active_color'];
				$style5_button_border_color  = $this->props['style5_button_border_color'];
				$style6_button_active_color  = $this->props['style6_button_active_color'];
				$style6_button_color  = $this->props['style6_button_color'];
				$style7_button_border_active_color  = $this->props['style7_button_border_active_color'];
				$style7_button_border_color  = $this->props['style7_button_border_color'];
				$style8_button_active_color  = $this->props['style8_button_active_color'];
				$style8_button_border_color  = $this->props['style8_button_border_color'];
				$style9_button_active_color  = $this->props['style9_button_active_color'];
				$style9_button_color  = $this->props['style9_button_color'];
				$style10_button_active_border_color  = $this->props['style10_button_active_border_color'];
				$style10_button_color  = $this->props['style10_button_color'];
				$style11_button_active_color  = $this->props['style11_button_active_color'];
				$style11_button_color  = $this->props['style11_button_color'];
				$style12_button_active_border_color  = $this->props['style12_button_active_border_color'];
				$style12_button_border_color  = $this->props['style12_button_border_color'];
				$style13_button_active_color = $this->props['style13_button_active_color'];
				$style13_button_active_hover_color = $this->props['style13_button_active_hover_color'];
				$style13_button_hover_color = $this->props['style13_button_hover_color'];
				$style14_button_active_color = $this->props['style14_button_active_color'];
				$style14_button_color = $this->props['style14_button_color'];
				$style15_button_active_color = $this->props['style15_button_active_color'];
				$style15_button_active_hover_color = $this->props['style15_button_active_hover_color'];
				$style15_button_hover_color = $this->props['style15_button_hover_color'];
				$style16_button_active_color = $this->props['style16_button_active_color'];
				$style16_button_hover_color = $this->props['style16_button_hover_color'];
				$style16_button_border_color = $this->props['style16_button_border_color'];
				$style17_button_active_color = $this->props['style17_button_active_color'];
				$style17_button_hover_color = $this->props['style17_button_hover_color'];
				$style17_button_border_color = $this->props['style17_button_border_color'];
				$style18_button_border_color = $this->props['style18_button_border_color'];
				$style18_button_color = $this->props['style18_button_color'];
				$style19_button_gardien1_color = $this->props['style19_button_gardien1_color'];
				$style19_button_gardien2_color = $this->props['style19_button_gardien2_color'];
				$style19_button_gardien3_color = $this->props['style19_button_gardien3_color'];
				$style19_button_gardien4_color = $this->props['style19_button_gardien4_color'];
				$style19_button_active_border_color = $this->props['style19_button_active_border_color'];
				$style20_button_border_color = $this->props['style20_button_border_color'];
				$button_name_text_color = $this->props['button_name_text_color'];
				$button_text_hover_color = $this->props['button_text_hover_color'];
				$module_id              				  = $this->props['module_id'];
				$module_class           				  = $this->props['module_class'];
				$link_open_in_new_tab      = $this->props['link_open_in_new_tab'];
				$this->content = et_builder_replace_code_content_entities( $this->content );
				$module_class = ET_Builder_Element::add_module_order_class( $module_class, $render_slug );
				/*<-------------------------Css & Js---------------->*/
				if ( !is_admin()){
					if($team_members_display_category_filter == 'on' ){			
						wp_enqueue_script( 'archive_team_members_horizontal_filter', JVTEAM_PLUGIN_URL . 'assets/js/archive_team_members_horizontal_filter.js', array('jquery'), NULL, TRUE );
					}	
					wp_enqueue_script('jv_team_member_equalheight', JVTEAM_PLUGIN_URL .'/assets/js/jv_team_member_equalheight.js', array(), NULL);	
				} 
				/*<------------------------ Button text color comman ----------------------->*/
				if( $content_bg_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.jvtmhr_member',
						'declaration' => sprintf('background: %1$s !important;',esc_html( $content_bg_color )),
					) );
				}
				if( $social_icon_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.hr_links .jv_team_member_social_font.et-pb-icon',
						'declaration' => sprintf('color: %1$s !important;',esc_html( $social_icon_color )),
					) );
				}
				if( $button_name_text_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list li a span',
						'declaration' => sprintf('color: %1$s !important;',esc_html( $button_name_text_color )),
					) );
				}
				if( $button_text_hover_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list li a:hover span',
						'declaration' => sprintf('color: %1$s !important;',esc_html( $button_text_hover_color )),
					) );
				}
			/*---End----*/
			/*<------------------------ Button filter ftyle 1 to 20 ----------------------->*/
				if ( $filter_style == 'fstyle1' ){
						if( $style1_button_active_color !='' ){
							ET_Builder_Element::set_style( $render_slug, array(
								'selector'    => '.et_pb_jv_team_members_category_list.fstyle1 li a:hover,.et_pb_jv_team_members_category_list.fstyle1 li a.active',
								'declaration' => sprintf('background-color: %1$s !important;',esc_html( $style1_button_active_color )),
							) );
						}
						if( $style1_button_color !='' ){
							ET_Builder_Element::set_style( $render_slug, array(
								'selector'    => '.et_pb_jv_team_members_category_list.fstyle1 li a',
								'declaration' => sprintf('background-color: %1$s !important;',esc_html( $style1_button_color )),
							) );
						}
				} 
				if( $filter_style == 'fstyle2' ) {
					if( $style2_button_active_color !='' ){
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle2 li a.active',
							'declaration' => sprintf('background-color: %1$s !important;border: 1px solid %1$s !important;',esc_html( $style2_button_active_color )),
						) );
					}
					if( $style2_button_animation_border_color !='' ) {
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle2 li a:before, .et_pb_jv_team_members_category_list.fstyle2 li a:after',
							'declaration' => sprintf('background: %1$s !important;',esc_html( $style2_button_animation_border_color )),
						) );
					}
					if( $style2_button_hover_color !='' ) {
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle2 li a:hover',
							'declaration' => sprintf('border: 1px solid %1$s !important;background: %1$s !important;',esc_html( $style2_button_hover_color )),
						) );
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle2 li a',
							'declaration' => sprintf('border: 1px solid %1$s !important;',esc_html( $style2_button_hover_color )),
						) );
					} 
				}	
				if( $filter_style == 'fstyle3' ) {
						if( $style3_button_border_color !='' ) {
							ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle3 li a.active',
							'declaration' => sprintf('border-color: %1$s !important;background-color: #fff  !important;',esc_html( $style3_button_border_color )),
							) );
						}
						if( $style3_button_color !='' ) {
							ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle3 li a',
							'declaration' => sprintf('border: 1px solid %1$s !important;;background: %1$s !important;',esc_html( $style3_button_color )),
							) );
						}
						if( $style3_button_hover_color !='' ) {
							ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle3 li a:hover',
							'declaration' => sprintf('background-color: %1$s !important;',esc_html( $style3_button_hover_color )),
							) );
						}
				} 
				if( $filter_style == 'fstyle4' ) {	
					if( $style4_button_border_color !='' ) {
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle4 li a.active, .et_pb_jv_team_members_category_list.fstyle4 li a',
							'declaration' => sprintf('border: 2px solid %1$s !important;',esc_html( $style4_button_border_color )),
						) );
					}	
					ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle4 li a.active:before',
							'declaration' => sprintf('background-color: %1$s !important;','#fff'),
						) );
					if( $style4_button_color !='' ) {
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle4 li a:before',
							'declaration' => sprintf('background: %1$s !important;',esc_html( $style4_button_color )),
						) );
					}
				} 
				if( $filter_style == 'fstyle5' ) {
					if( $style5_button_border_active_color !='' ){
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle5 li a.active:after, .et_pb_jv_team_members_category_list.fstyle5 li a.active:before',
							'declaration' => sprintf('background-color: %1$s !important;',esc_html( $style5_button_border_active_color )),
						) );	
					}
					if( $style5_button_border_color !='' ){
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle5 li a:before, .et_pb_jv_team_members_category_list.fstyle5 li a:after',
							'declaration' => sprintf('background-color: %1$s !important;',esc_html( $style5_button_border_color )),
						) );
					}
				}
				 if( $filter_style == 'fstyle6' ) {
					if( $style6_button_active_color !='' ){
						ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle6 li a.active',
						'declaration' => sprintf('background: %1$s !important;',esc_html( $style6_button_active_color )),
						) );
						ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle6 li a.active:after',
						'declaration' => sprintf('border: 2px solid %1$s !important;',esc_html( $style6_button_active_color )),
						) );
					}
					if( $style6_button_color !='' ){
						ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle6 li a',
						'declaration' => sprintf('background-color: %1$s !important;',esc_html( $style6_button_color )),
						) );
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle6 li a:after',
							'declaration' => sprintf('border: 2px solid %1$s !important;',esc_html( $style6_button_color )),
						) );
					}
				}
				 if( $filter_style == 'fstyle7' ) {
					if( $style7_button_border_active_color !='' ){
						ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle7 li a.active',
						'declaration' => sprintf('border-left: 3px solid %1$s !important;border-right: 3px solid %1$s !important;',esc_html( $style7_button_border_active_color )),
						) );
						ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle7 li a.active:before,.et_pb_jv_team_members_category_list.fstyle7 li a.active:after',
						'declaration' => sprintf('background-color: %1$s !important;',esc_html( $style7_button_border_active_color )),
						) );
					}
					if( $style7_button_border_color !='' ){
						ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle7 li a',
						'declaration' => sprintf('border-left: 3px solid %1$s !important;border-right: 3px solid %1$s !important;',esc_html( $style7_button_border_color )),
						) );
						ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle7 li a:before,.et_pb_jv_team_members_category_list.fstyle7 li a:after',
						'declaration' => sprintf('background: %1$s !important;',esc_html( $style7_button_border_color )),
						) );
					}
				}  
				if( $filter_style == 'fstyle8' ) {
					if( $style8_button_active_color !='' ){
						ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle8 li a:after',
						'declaration' => sprintf('background:  %1$s !important;',esc_html( $style8_button_active_color )),
						) );
						ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle8 li a.active',
						'declaration' => sprintf('background:  %1$s !important;border: 2px solid %1$s !important;',esc_html( $style8_button_active_color )),
						) );
					}
					if( $style8_button_border_color !='' ) {
						ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle8 li a',
						'declaration' => sprintf('border: 2px solid %1$s !important;',esc_html( $style8_button_border_color )),
						) );
					}
				}  
				if( $filter_style == 'fstyle9' ) {
					if( $style9_button_active_color !='' ) {
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle9 li a.active',
							'declaration' => sprintf('background: %1$s !important;',esc_html( $style9_button_active_color )),
						) );
					}
					if( $style9_button_color !='' ) {
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle9 li a',
							'declaration' => sprintf('background: %1$s !important;',esc_html( $style9_button_color )),
						) );
					}
				}  
				if( $filter_style == 'fstyle10' ) {
					if( $style10_button_active_border_color !='') {
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle10 li a.active:before, .et_pb_jv_team_members_category_list.fstyle10 li a.active:after,.et_pb_jv_team_members_category_list.fstyle10 li a.active:hover:before,.et_pb_jv_team_members_category_list.fstyle10 li a.active :hover:after',
							'declaration' => sprintf('border-color:%1$s !important;',esc_html( $style10_button_active_border_color )),
						) );
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle10 li a.active,.et_pb_jv_team_members_category_list.fstyle10 li a.active:hover',
							'declaration' => sprintf('border: 2px solid %1$s !important;',esc_html( $style10_button_active_border_color )),
						) );
					}
					if( $style10_button_color !='') {
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle10 li a:before,.et_pb_jv_team_members_category_list.fstyle10 li a:after,.et_pb_jv_team_members_category_list.fstyle10 li a:hover:before,.et_pb_jv_team_members_category_list.fstyle10 li a :hover:after',
							'declaration' => sprintf('border-color:%1$s !important;',esc_html( $style10_button_color )),
						) );
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle10 li a, .et_pb_jv_team_members_category_list.fstyle10 li a:hover',
							'declaration' => sprintf('border: 2px solid %1$s !important;',esc_html( $style10_button_color )),
						) );
					}
				} 
				if( $filter_style == 'fstyle11' ) {
					if( $style11_button_active_color !='') {
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle11 li a.active',
							'declaration' => sprintf('background: %1$s !important;border: 2px solid %1$s !important;',esc_html( $style11_button_active_color )),
							) );
					}
					 if( $style11_button_color !='') {
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle11 li a:hover,.et_pb_jv_team_members_category_list.fstyle11 li a:before,.et_pb_jv_team_members_category_list.fstyle11 li a:after',
							'declaration' => sprintf('background: %1$s !important;',esc_html( $style11_button_color )),
							) );
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle11 li a',
							'declaration' => sprintf('border: 2px solid %1$s !important;',esc_html( $style11_button_color )),
						) );
					}
				} 
				if( $filter_style == 'fstyle12' ) {
					if( $style12_button_active_border_color !='' ){
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle12 li a.active:before,.et_pb_jv_team_members_category_list.fstyle12 li a.active:after,.et_pb_jv_team_members_category_list.fstyle12 li a.active span:before,.et_pb_jv_team_members_category_list.fstyle12 li a.active span:after',
							'declaration' => sprintf('background: %1$s !important;',esc_html( $style12_button_active_border_color )),
						) );
					}
					if( $style12_button_border_color !='' ){
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle12 li a:before,.et_pb_jv_team_members_category_list.fstyle12 li a:after,.et_pb_jv_team_members_category_list.fstyle12 li a span:before,.et_pb_jv_team_members_category_list.fstyle12 li a span:after',
							'declaration' => sprintf('background: %1$s !important;',esc_html( $style12_button_border_color )),
						) );
					}
				} 
				if( $filter_style == 'fstyle13' ) {
					if( $style13_button_active_color !='' ) {
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle13 li a.active',
							'declaration' => sprintf('background: %1$s !important;border: 1px solid %1$s !important;',esc_html( $style13_button_active_color )),
						) );
					}
					if( $style13_button_active_hover_color !='' ) {
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle13 li a.active:before, .et_pb_jv_team_members_category_list.fstyle13 li a.active:after, .et_pb_jv_team_members_category_list.fstyle13 li a.active span:before, .et_pb_jv_team_members_category_list.fstyle13 li a.active span:after',
							'declaration' => sprintf('background: %1$s !important;',esc_html( $style13_button_active_hover_color )),
						) );
					}
					if( $style13_button_hover_color !='' ) {
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle13 li a:before, .et_pb_jv_team_members_category_list.fstyle13 li a:after, .et_pb_jv_team_members_category_list.fstyle13 li a span:before, .et_pb_jv_team_members_category_list.fstyle13 li a span:after',
							'declaration' => sprintf('background: %1$s !important;',esc_html( $style13_button_hover_color )),
						) );
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle13 li a',
							'declaration' => sprintf('border: 1px solid %1$s !important;',esc_html( $style13_button_hover_color )),
						) );
					}
				} 
				if( $filter_style == 'fstyle14' ) {
					if( $style14_button_active_color !='' ) {
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle14 li a.active:after',
							'declaration' => sprintf('box-shadow: 0 0 0 2px %1$s !important;',esc_html( $style14_button_active_color )),
						) );
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle14 li a.active',
							'declaration' => sprintf('background: %1$s !important;',esc_html( $style14_button_active_color )),
						) );
					}	
					if( $style14_button_color !='' ) {
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle14 li a',
							'declaration' => sprintf('background: %1$s !important;',esc_html( $style14_button_color )),
						) );
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle14 li a:after',
							'declaration' => sprintf('box-shadow: 0 0 0 2px %1$s !important;',esc_html( $style14_button_color )),
						) );
					}
				}
				 if( $filter_style == 'fstyle15' ) {
					if( $style15_button_active_color !='' ) {
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle15 li a.active',
							'declaration' => sprintf('background: %1$s !important;',esc_html( $style15_button_active_color )),
						) );
					}
					if( $style15_button_active_hover_color !='' ) {
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle15 li a.active:before, .et_pb_jv_team_members_category_list.fstyle15 li a.active:after, .et_pb_jv_team_members_category_list.fstyle15 li a.active span:before, .et_pb_jv_team_members_category_list.fstyle15 li a.active span:after',
								'declaration' => sprintf('background: %1$s !important;',esc_html( $style15_button_active_hover_color )),
						) );
					}
					if( $style15_button_hover_color !='' ) {
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle15 li a',
							'declaration' => sprintf('border: 1px solid %1$s !important;',esc_html( $style15_button_hover_color )),
						) );
						ET_Builder_Element::set_style( $render_slug, array(
								'selector'    => '.et_pb_jv_team_members_category_list.fstyle15 li a:before, .et_pb_jv_team_members_category_list.fstyle15 li a:after, .et_pb_jv_team_members_category_list.fstyle15 li a span:before, .et_pb_jv_team_members_category_list.fstyle15 li a span:after',
								'declaration' => sprintf('background:  %1$s !important;',esc_html( $style15_button_hover_color )),
						) );
					}
				} 
				if( $filter_style == 'fstyle16' ) {
					if( $style16_button_active_color !=''){
						ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle16 li a.active, .et_pb_jv_team_members_category_list.fstyle16 li a.active:before, .et_pb_jv_team_members_category_list.fstyle16 li a.active:after, .et_pb_jv_team_members_category_list.fstyle16 li a.active span:before, .et_pb_jv_team_members_category_list.fstyle16 li a.active span:after',
						'declaration' => sprintf('background: %1$s !important;',esc_html( $style16_button_active_color )),
						) );
					}
					if( $style16_button_hover_color !='') {
						ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle16 li a:before, .et_pb_jv_team_members_category_list.fstyle16 li a:after, .et_pb_jv_team_members_category_list.fstyle16 li a span:before, .et_pb_jv_team_members_category_list.fstyle16 li a span:after',
						'declaration' => sprintf('background: %1$s !important;',esc_html( $style16_button_hover_color )),
						) );
					}
					if( $style16_button_border_color !='') {
						ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle16 li a',
						'declaration' => sprintf('border: 1px solid %1$s !important;',esc_html( $style16_button_border_color )),
						) );
					}
				} 
				if( $filter_style == 'fstyle17' ) {
					if( $style17_button_active_color !='' ) {
							ET_Builder_Element::set_style( $render_slug, array(
								'selector'    => '.et_pb_jv_team_members_category_list.fstyle17 li a.active',
								'declaration' => sprintf('background: %1$s !important;',esc_html( $style17_button_active_color )),
							) );
					}
					if( $style17_button_hover_color !='' ){
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle17 li a:hover',
							'declaration' => sprintf('background: %1$s !important;',esc_html( $style17_button_hover_color )),
						) );
					}
					if( $style17_button_border_color !='' ){
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle17 li a',
							'declaration' => sprintf('border: 2px solid %1$s !important;',esc_html( $style17_button_border_color )),
						) );
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle17 li a:before',
							'declaration' => sprintf('border-top: 2px solid %1$s !important;border-left: 2px solid %1$s !important;',esc_html( $style17_button_border_color )),
						) );
						ET_Builder_Element::set_style( $render_slug, array(
								'selector'    => '.et_pb_jv_team_members_category_list.fstyle17 li a:after',
								'declaration' => sprintf('border-bottom: 2px solid %1$s !important;border-right: 2px solid %1$s !important;',esc_html( $style17_button_border_color )),
						) );
					}
				} 
				if( $filter_style == 'fstyle18' ) {
					if( $style18_button_border_color !=''){
						ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle18 li a.active',
						'declaration' => sprintf('border: 2px solid %1$s !important;',esc_html( $style18_button_border_color )),
						) );	
					}
					if( $style18_button_color !=''){
						ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle18 li a:before, .et_pb_jv_team_members_category_list.fstyle18 li a:after',
						'declaration' => sprintf('background: %1$s !important;',esc_html( $style18_button_color )),
						) );	
					}
				} 
				if( $filter_style == 'fstyle19' ) {
					if(( $style19_button_gardien1_color !='') && ( $style19_button_gardien2_color !='') && ( $style19_button_gardien3_color !='') &&  ( $style19_button_gardien4_color !='') ){
						ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle19 li a',
						'declaration' => sprintf('background-image: linear-gradient(to top, %1$s 0px, %2$s 10px, %3$s 10px, %4$s 100px) !important;',esc_html( $style19_button_gardien1_color ),esc_html( $style19_button_gardien2_color ),esc_html( $style19_button_gardien3_color ),esc_html( $style19_button_gardien4_color )),
						) );	
					}
					if( $style19_button_active_border_color !=''){
						ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle19 li a:after',
						'declaration' => sprintf('background: %1$s !important;',esc_html( $style19_button_active_border_color )),
						) );	
					}
				}	
				if( $filter_style == 'fstyle20' ) {
						if( $style20_button_border_color !=''){
							ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle20 li a, .et_pb_jv_team_members_category_list.fstyle20 li a:before, .et_pb_jv_team_members_category_list.fstyle20 li a:after',
							'declaration' => sprintf('border-color: %1$s !important;',esc_html( $style20_button_border_color )),
							) );	
							ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle20 li a:hover:after',
							'declaration' => sprintf('border-color: %1$s !important;',esc_html( $style20_button_border_color )),
							) );
							ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle20 li a:hover:before',
							'declaration' => sprintf('border-color: %1$s !important;',esc_html( $style20_button_border_color )),
							) );
						}
				}
				/*--end---*/
				$args = array(
					'post_type' => 'jv_team_members',
					'posts_per_page' => $posts_per_page 
				);
				
				 if ( 'id_desc' !== $category_orderby ) {
					switch( $category_orderby ) {
						case 'id_asc' :
							$orderby = 'id';
							$order = 'ASC';
							break;
						case 'name_asc' :
							$orderby = 'name';
							$order = 'ASC';
							break;
						case 'name_desc' :
							$orderby = 'name';
							$order = 'DESC';
							break;
						case 'count' :
							$orderby = 'count';
							break;
						case 'slug_desc' :
							$orderby = 'slug';
							$order = 'DESC';
							break;
						case 'slug_asc' :
							$orderby = 'slug';
							$order = 'ASC';
							break;
					}
				}else{
					$orderby = 'id';
					$order = 'DESC';
				}
				
				$archive_department_category_terms = get_terms( array(
					'taxonomy' => 'department_category',
					'hide_empty' => false,
					'orderby'           => $orderby, 
		    		'order'             => $order,
				) );
				$include_categories_array = explode(',',$include_categories);
				if ( $team_members_display_category != 'all' ){
						$args['tax_query'] = array(
							array(
								'taxonomy' => 'department_category',
								'field' => 'term_id',
								'terms' => explode(",", $include_categories),
								'operator' => 'IN'
							)
						);
				}

				if ( 'date_desc' !== $team_orderby ) {
					switch( $team_orderby ) {
						case 'date_asc' :
							$args['orderby'] = 'date';
							$args['order'] = 'ASC';
							break;
						case 'title_asc' :
							$args['orderby'] = 'title';
							$args['order'] = 'ASC';
							break;
						case 'title_desc' :
							$args['orderby'] = 'title';
							$args['order'] = 'DESC';
							break;
						case 'rand' :
							$args['orderby'] = 'rand';
							break;
						case 'menu_order_desc' :
							$args['orderby'] = 'menu_order';
							$args['order'] = 'DESC';
							break;
						case 'menu_order_asc' :
							$args['orderby'] = 'menu_order';
							$args['order'] = 'ASC';
							break;
					}
				}else{
					$args['orderby'] = 'date';
					$args['order'] = 'DESC';
				}
				$output = '';
				if($team_members_display_category_filter == 'on' ){
					 if ( $team_members_display_category == 'all'){
						$output .= '<ul class="et_pb_jv_team_members_category_list '.$filter_style.' "><li>
											<a href="javascript:void(0)" class="jv_filter active" data-filter=".all" ><span>'.$filter_all_label.'</span></a></li>';
						$archive_department_category_count = count($archive_department_category_terms);
						if ( $archive_department_category_count > 0 ){
						foreach($archive_department_category_terms as $department_categoryval){
								$output .= '<li><a href="javascript:void(0)" class="jv_filter" data-filter=".cat-'.$department_categoryval->term_id.'" ><span>'.$department_categoryval->name.'</span></a></li>';
							}
						}
						$output .= '</ul>';
				}else{
						if ( $include_categories != '' ){
							$output .= '<ul class="et_pb_jv_team_members_category_list '.$filter_style.' "><li>
												<a href="javascript:void(0)" class="jv_filter active" data-filter=".all" ><span>'.$filter_all_label.'</span></a></li>';
							$archive_department_category_count = count($archive_department_category_terms);
							if ( $archive_department_category_count > 0 ){
								foreach($archive_department_category_terms as $department_categoryval){
									if  ( in_array($department_categoryval->term_id,$include_categories_array)){
										$output .= '<li><a href="javascript:void(0)" class="jv_filter" data-filter=".cat-'.$department_categoryval->term_id.'" ><span>'.$department_categoryval->name.'</span></a></li>';
									}
								}
							}
								$output .= '</ul>';
						}
					}
				}
				$team_members_horizontal = new WP_Query( $args );
					$rand =  "tms_horizontal_".rand(10,500);
					// The Loop
					if ( $team_members_horizontal->have_posts() ) {
					$jv_template_path =  get_stylesheet_directory() . '/divi-team-members';
					$jv_css_path = $jv_template_path.'/css/archive_team_members_horizontal_css';
					$jv_css_url =  get_stylesheet_directory_uri().'/divi-team-members/css/archive_team_members_horizontal_css'; 
					
					$jv_grid_path =  $jv_template_path.'/content-archive-team-horizontal';
					
							if ( file_exists( $jv_css_path . '/jv_team_horizontal.css' ) )
							{
								wp_enqueue_style('jv_team_horizontal', $jv_css_url . '/jv_team_horizontal.css', array(), NULL);
							}else{
								wp_enqueue_style('jv_team_horizontal', JVTEAM_PLUGIN_URL .'assets/css/archive_team_members_horizontal_css/jv_team_horizontal.css', array(), NULL);
							}
							$output .=  '<div '.$module_id.' class="'.$module_class.' horizontal_team_members '.$rand.' jv_hr_members">';
								while ( $team_members_horizontal->have_posts() ) { $team_members_horizontal->the_post();
									ob_start();
									if ( file_exists( $jv_grid_path . '/content-team-horizontal.php' ) ){
											 include $jv_grid_path. '/content-team-horizontal.php';
									}else{
											include JVTEAM_PLUGIN_PATH. '/content-archive-team-horizontal/content-team-horizontal.php';
									}
									$output .= ob_get_contents();
									ob_end_clean();
								}
							 $output .= '</div>';
							wp_reset_postdata();
						}
					$output .= '<script>
						jQuery(window).load(function() {
						if( jQuery("body").find(".'.$rand.' .jv_team_member_equalheight").length >0 ){
							equalheight(".'.$rand.' .jv_team_member_equalheight");
						 }
						});
						jQuery(window).resize(function(){
						 if( jQuery("body").find(".'.$rand.' .jv_team_member_equalheight").length >0 ){
									equalheight(".'.$rand.' .jv_team_member_equalheight");
								 }
						});
						</script>';
					if( $display_popup_onteam =='on') {
						$output .= "<script>
						 jQuery(function ($) {
						 	$('a[href^=\"#teammodal\"]').addClass('et_smooth_scroll_disabled');
							$('.popup-modal').magnificPopup({
								type: 'inline',
								midClick: true,
								closeBtnInside: true
							});
						});
						 </script>";
					}		
				return $output;
			}
			/* public function process_box_shadow( $render_slug ) {
				$boxShadow = ET_Builder_Module_Fields_Factory::get( 'BoxShadow' );
				self::set_style( $render_slug, $boxShadow->get_style(
					sprintf( '.%1$s .jvtmhr_member', self::get_module_order_class( $render_slug ) ),
					$this->props
				) );
			} */
			
			// protected function _add_additional_shadow_fields() {
			// 	return false;
			// }

			// function process_box_shadow( $render_slug ) {
			// 	return false;
			// }
	}
	new ET_Team_Members_Horizontal();
}
?>