<!-- POPUP -->
<div id="teammodal<?php echo get_the_ID(); ?>" class="white-popup mfp-hide jv_popup_style1">
		<div class="et_pb_row clearfix jv_team_popup_style1">
			<div class="et_pb_column et_pb_column_4_4">
					<div class="jv_team_list_member jv_team_list_member_style1">
						<div class="jv_team_list_member_image">
							<?php 
								$jv_team_thumb_p = wp_get_attachment_image_src(get_post_thumbnail_id(), 'jv_team_list_image');
								if( $jv_team_thumb_p[0] != ''){ $image_path_p = $jv_team_thumb_p[0] ;}else{$image_path_p = JVTEAM_PLUGIN_URL . '/assets/images/default.png';} 
							?>
							<img src="<?php echo $image_path_p;?>" alt="<?php echo get_the_title();?>">	
						</div>
						<div class="jv_team_list_description">
							<h4 class="jv_team_list_title"><?php echo get_the_title();?></h4>
							
							<?php if( $jv_team_designation != ''){ ?><div class="jv_team_list_position"><?php echo $jv_team_designation;?></div><?php } ?>
							
							<div class="jv_team_list_content"><?php echo do_shortcode(wpautop(get_the_content())); ?></div> 
							
							<?php if( $jv_team_address != '' ) {?><div class="jv_team_list_address1"><i class="et-pb-icon jv_team_member_icon_font">&#xe01d;</i><?php echo rtrim($jv_team_address,',');?></div><?php } 
							
							 if( $jv_team_website_url != ''){ ?> <div class="jv_team_list_web_url"><i class="et-pb-icon jv_team_member_icon_font">&#xe0e3;</i><a class="website_url" href="<?php echo $jv_team_website_url;?>" target="_blank" ><?php echo $jv_team_website_url;?></a></div><?php } 
							
							 if( $jv_team_mobile_number != ''){ ?> <div class="jv_team_list_mobile"><i class="et-pb-icon jv_team_member_icon_font">&#xe00b;</i><a class="contect_number" href="tel:<?php echo $jv_team_mobile_number;?>"><?php echo $jv_team_mobile_number;?></a></div><?php } 
							
							 if( $jv_team_email_address != ''){ ?> <div class="et_pb_jv_team_email_address"><i class="et-pb-icon jv_team_member_icon_font">&#xe076;</i><a class="email_address" href="mailto:<?php echo $jv_team_email_address;?>"  ><?php echo $jv_team_email_address;?></a></div><?php } 	
							
							 if(( $jv_team_facebook != '') || ( $jv_team_twitter != '') || ( $jv_team_google != '') || ( $jv_team_linkdin != '') || ( $jv_team_instagram != '')) { ?>
							 <ul class="jv_team_list_social_link">
							  <?php if( $jv_team_facebook != '' ) { ?><li><a href="<?php echo $jv_team_facebook;?>" target="_blank"><i class="jv_team_member_social_font et-pb-icon">&#xe0aa;</i></a></li><?php } 
							   if( $jv_team_twitter != '' ) { ?><li><a href="<?php echo $jv_team_twitter;?>" target="_blank"><i class="jv_team_member_social_font et-pb-icon">&#xe0ab;</i></a></li><?php } 
							   if( $jv_team_google != '' ) { ?><li><a href="<?php echo $jv_team_google;?>" target="_blank"><i class="jv_team_member_social_font et-pb-icon">&#xe0ad;</i></a></li><?php } 
							   if( $jv_team_linkdin != '' ) { ?><li><a href="<?php echo $jv_team_linkdin;?>"target="_blank"><i class="jv_team_member_social_font et-pb-icon">&#xe0b4;</i></a></li><?php } 
							   if( $jv_team_instagram != '' ) { ?><li><a href="<?php echo $jv_team_instagram;?>"target="_blank"><i class="jv_team_member_social_font et-pb-icon">&#xe0b1;</i></a></li><?php } ?>
							</ul>
							<?php } ?>	
						</div>
						<?php 
							if( $display_contact_form =='on') {
								if ( is_plugin_active( 'caldera-forms/caldera-core.php' ) ) {
							?> 
									<div class="et_pb_column et_pb_column_4_4 style1_contact_form_column_4">
										<div class="jv_team_list_text style1_contact_form">
											<h2><?php echo $divi_popupteam_contact_us_title; ?></h2>
											<div class="style1_hr_line"></div>
										</div>
										<?php echo do_shortcode('[caldera_form id="'.$jv_team_contact_form.'"]') ?>
									</div>
								
							<?php 	}	
							}
							?>
				</div> 
			</div>
		</div>
</div>