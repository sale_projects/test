<!-- POPUP -->
<?php
$team_members_categories = wp_get_post_terms( get_the_ID() ,'department_category', array("fields" => "names"));
$cat_name = '';
if ( count($team_members_categories ) > 0 ){
	foreach ( $team_members_categories as $cat_names ){
		$cat_name .= $cat_names.',';		
	}
}
?>
<div id="teammodal<?php echo get_the_ID(); ?>" class="white-popup mfp-hide jv_popup_style2">
<div class="jv_team_list_member jv_team_list_member_popup_style2">
	<div class="et_pb_row style6_first_row"> 
		<div class="et_pb_column et_pb_column_1_3 style6_responsive_social">
			<div class="jv_team_list_member_image">
				<?php 
					$jv_team_thumb_p = wp_get_attachment_image_src(get_post_thumbnail_id(), 'large');
					$jv_team_thumb_mobile_p = wp_get_attachment_image_src(get_post_thumbnail_id(), 'large');
					if( $jv_team_thumb_p[0] != '' || $jv_team_thumb_mobile_p[0] != ''){ 
								$image_path_p = $jv_team_thumb_p[0] ;
								$image_path_mobile_p = $jv_team_thumb_mobile_p[0] ;
					}else{
								$image_path_p = JVTEAM_PLUGIN_URL . '/assets/images/default.png'; 
					} 
					?>
			<img src="<?php echo $image_path_p;?>" alt="<?php echo get_the_title();?>" class="img_desk">
			<img src="<?php echo $image_path_mobile_p;?>" alt="<?php echo get_the_title();?>" class="img_mobile">			
			</div>
			<?php 
				if(( $jv_team_facebook != '') || ( $jv_team_twitter != '') || ( $jv_team_google != '') || ( $jv_team_linkdin != '') || ( $jv_team_instagram != '')) { ?>
				<div class="jv_team_list_social_link">
					<ul>
						<?php if( $jv_team_facebook != '' ) { ?><li><a href="<?php echo $jv_team_facebook;?>" target="_blank"><i class="jv_team_member_social_font et-pb-icon">&#xe093;</i></a></li><?php } ?>
						<?php if( $jv_team_twitter != '' ) { ?><li><a href="<?php echo $jv_team_twitter;?>" target="_blank"><i class="jv_team_member_social_font et-pb-icon">&#xe094;</i></a></li><?php } ?>
						<?php if( $jv_team_google != '' ) { ?><li><a href="<?php echo $jv_team_google;?>" target="_blank"><i class="jv_team_member_social_font jv_team_member_social_font_gogle et-pb-icon">&#xe096;</i></a></li><?php } ?>
						<?php if( $jv_team_linkdin != '' ) { ?><li><a href="<?php echo $jv_team_linkdin;?>"target="_blank"><i class="jv_team_member_social_font et-pb-icon">&#xe09d;</i></a></li><?php } ?>
						<?php if( $jv_team_instagram != '' ) { ?><li><a href="<?php echo $jv_team_instagram;?>"target="_blank"><i class="jv_team_member_social_font et-pb-icon">&#xe09a;</i></a></li><?php } ?>
					</ul>
				</div>
		    <?php } ?>	
		</div>
		<div class="et_pb_column et_pb_column_2_3 right_info_columm">
			<div class="jv_team_list_title">
				<span>Know about me <?php echo get_the_title(get_the_ID());?></span>
				<h2><?php echo get_the_title(get_the_ID());?></h2>
			</div>
			<?php if( $jv_team_designation !='') { ?>
			<div class="et_pb_column et_pb_column_1_2">
				<div class="jv_team_feture_box">
					<div class="jv_team_icon">
						<span><i class="jv_team_member_info et-pb-icon">&#xe085;</i></span>
					</div>
					<div class="jv_team_info">
						<h4>Designation:</h4>
						<span><?php echo $jv_team_designation;?></span>
					</div>
				</div>
			</div>
			<?php } ?>
			<?php if( $jv_team_mobile_number !='') { ?>
			<div class="et_pb_column et_pb_column_1_2">
				<div class="jv_team_feture_box">
					<div class="jv_team_icon">
						<span><i class="jv_team_member_info et-pb-icon">&#xe00b;</i></span>
					</div>
					<div class="jv_team_info">
						<h4>Phone:</h4>
						<span><?php echo $jv_team_mobile_number;?></span>
					</div>
				</div>
			</div>
			<?php } ?>
			<?php if( $jv_team_email_address !='' ) { ?>
			<div class="et_pb_column et_pb_column_1_2">
				<div class="jv_team_feture_box">
					<div class="jv_team_icon">
						<span><i class="jv_team_member_info et-pb-icon">&#xe076;</i></span>
					</div>
					<div class="jv_team_info">
						<h4>Email:</h4>
						<span><?php echo $jv_team_email_address;?></span>
					</div>
				</div>
			</div>
			<?php } ?>
			<?php if( $jv_team_website_url !='') { ?>
			<div class="et_pb_column et_pb_column_1_2">
				<div class="jv_team_feture_box">
					<div class="jv_team_icon">
						<span><i class="jv_team_member_info et-pb-icon">&#xe0e3;</i></span>
					</div>
					<div class="jv_team_info">
						<h4>Website URL:</h4>
						<span><?php echo $jv_team_website_url;?></span>
					</div>
				</div>
			</div>
			<?php } ?>
			<?php if( $jv_team_address !='') { ?>
			<div class="et_pb_column et_pb_column_1_2">
				<div class="jv_team_feture_box">
					<div class="jv_team_icon">
						<span><i class="jv_team_member_info et-pb-icon">&#xe081;</i></span>
					</div>
					<div class="jv_team_info">
						<h4>Address:</h4>
						<span><?php echo rtrim($jv_team_address,',');?></span>
					</div>
				</div>
			</div>
			<?php } ?>
			<?php if( $cat_name !='' ) { ?>
			<div class="et_pb_column et_pb_column_1_2">
				<div class="jv_team_feture_box">
					<div class="jv_team_icon">
						<span><i class="jv_team_member_info et-pb-icon">&#xe018;</i></span>
					</div>
					<div class="jv_team_info">
						<h4>Department:</h4>
						<span><?php echo rtrim( $cat_name,',' ); ?></span>
					</div>
				</div>
			</div>
			<?php } ?>
		</div>		
	</div>
	<div class="et_pb_row style6_second_row"> 
	<div class="jv_team_list_content">
		<?php echo do_shortcode(wpautop(get_the_content())); ?>
	</div>	
	</div>	
	<?php
		if( $display_contact_form =='on') {
			if ( is_plugin_active( 'caldera-forms/caldera-core.php' ) ) {
			
		?> <div class="et_pb_row style6_third_row">
				<div class="et_pb_column et_pb_column_4_4">
					<div class="jv_team_list_text style6_contact_form">
						<h2><?php echo $divi_popupteam_contact_us_title; ?></h2>
						<div class="style6_hr_line"></div>
					</div>
						<?php echo do_shortcode('[caldera_form id="'.$jv_team_contact_form.'"]') ?>
				</div>
			</div>
		<?php 	}	
		}
		?>
</div>		
</div>