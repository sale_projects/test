<?php 
if ( ! class_exists( 'Divi_Team_Post_Type' ) )
{
	class Divi_Team_Post_Type
	{
		function __construct()
		{
			add_action( 'init', array( &$this, 'jv_team_init' ) );
			add_action( 'add_meta_boxes', array( &$this, 'jv_team_meta_box_add' ) ); 
			add_action( 'save_post',  array( &$this,'jv_team_save_meta_box_data' ) );
			add_action( 'post_updated_messages',  array( &$this,'jv_team_updated_messages' ) );
			add_action( 'manage_edit-jv_team_members_columns',  array( &$this,'jv_team_thumbnail_column' ),10,1);
			add_action( 'manage_jv_team_members_posts_custom_column',  array( &$this,'jv_team_display_thumbnail' ),10,1);
			add_filter( 'enter_title_here',array( &$this, 'jv_team_default_title' ));
			add_action( 'do_meta_boxes', array( &$this,'jv_team_change_image_box' ) );
		}
		function jv_team_init()
		{
			$jvteam_labels = array(
								'name' 					=> __('Team Members','jvteam'),
								'singular_name' 		=> __('Team Members','jvteam'),
								'add_new' 				=> __('Add Team Member','jvteam'),
								'add_new_item' 			=> __('Add New Team Member ','jvteam'),
								'edit_item' 			=> __('Edit Team Member ','jvteam'),
								'new_item' 				=> __('New Team Member','jvteam'),
								'all_items' 			=> __('All Team Members','jvteam'),
								'search_items' 			=> __('Serach Team Member','jvteam'),
								'not_found' 			=> __('No Team Member found','jvteam'),
								'not_found_in_trash' 	=> __('No Team Member found in Trash','jvteam'), 
								'parent_item_colon'		=> '',
								'menu_name' 			=> __('Team Members','jvteam')
							);
			$jvteam_args = array(		
								'labels'             => $jvteam_labels,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'jv-team-members' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'menu_position'      => null,
								'menu_icon'			 => 'dashicons-groups',
								'supports'           => array( 'title', 'editor','thumbnail')
							);
							register_post_type( 'jv_team_members', $jvteam_args );
			$jvteam_categary_labels = array(
							'name'              => __( 'Department Categories', 'jvteam' ),
							'singular_name'     => __( 'Department Categories',  'jvteam' ),
							'search_items'      => __( 'Search Department Category', 'jvteam' ),
							'all_items'         => __( 'All Department Categories', 'jvteam' ),
							'parent_item'       => __( 'Parent Category', 'jvteam' ),
							'parent_item_colon' => __( 'Parent Category:', 'jvteam' ),
							'edit_item'         => __( 'Edit Department Category', 'jvteam' ),
							'update_item'       => __( 'Update Department Category', 'jvteam' ),
							'add_new_item'      => __( 'Add New Department Category', 'jvteam' ),
							'new_item_name'     => __( 'New Department Category', 'jvteam' ),
							'menu_name'         => __( 'Department Categories', 'jvteam' ),
						);
			$jvteam_categary_args = array(
								'hierarchical'      => true,
								'labels'            => $jvteam_categary_labels,
								'show_ui'           => true,
								'show_admin_column' => true,
								'query_var'         => true,
								'rewrite'           => array( 'slug' => 'department-category' ),
							);
					register_taxonomy( 'department_category', array( 'jv_team_members' ), $jvteam_categary_args );
        }
		function jv_team_meta_box_add() 
		{
			add_meta_box('jvteam_metabox',__( 'Team Member Information' ),array( &$this,'jv_team_custom_meta_box' ), 'jv_team_members', 'normal', 'low' );
		}
		function jv_team_custom_meta_box( $post ) 
		{
			wp_nonce_field( 'jv_team_meta_box', 'jv_team_meta_box_nonce' );
			$designation_name_value = get_post_meta( $post->ID, 'jv_team_designation', true );
			$facebook_name_value = get_post_meta( $post->ID, 'jv_team_facebook', true );
			$linkedin_name_value = get_post_meta( $post->ID, 'jv_team_linkedin', true );
			$instagram_name_value = get_post_meta( $post->ID, 'jv_team_instagram', true );
			$google_name_value = get_post_meta( $post->ID, 'jv_team_google', true );
			$twitter_name_value = get_post_meta( $post->ID, 'jv_team_twitter', true );
			$phone_number_value = get_post_meta( $post->ID, 'jv_team_phone_number', true );
			$email_address_value = get_post_meta( $post->ID, 'jv_team_email_address', true );
			$website_url_name_value = get_post_meta( $post->ID, 'jv_team_website_url', true );
			$address1_name_value = get_post_meta( $post->ID, 'jv_team_address1', true );
			$address2_name_value = get_post_meta( $post->ID, 'jv_team_address2', true );
			$city_name_value = get_post_meta( $post->ID, 'jv_team_city_name', true );
			$state_name_value = get_post_meta( $post->ID, 'jv_team_state_name', true );
			$country_name_value = get_post_meta( $post->ID, 'jv_team_country_name', true );
			$zipcode_number_value = get_post_meta( $post->ID, 'jv_team_zipcode', true );
			$custom_url_detail_page = get_post_meta( $post->ID, 'custom_url_detail_page', true );
			$jv_team_contact_form = get_post_meta( $post->ID, 'jv_team_contact_form', true );
			$display_contact_form = get_post_meta( $post->ID, 'display_contact_form', true );
		?>
		<table cellpadding="10">
			<tr>
				<td>
					<label for="designation_name">
						<?php _e( 'Designation/Position ', 'jvteam' ); ?>
					</label>
				</td>
				<td><input type="text" id="designation_name" name="jv_team_designation" value="<?php echo esc_attr( $designation_name_value ) ?>" size="40" /></td>			
			</tr>
			<tr>
				<td>
					<label for="phone_Number">
						<?php _e( 'Phone Number ', 'jvteam' ); ?>
					</label>
				</td>
				<td><input type="text" id="phone_number" name="jv_team_phone_number" value="<?php echo esc_attr( $phone_number_value ) ?>" size="40" /></td>			
			</tr>
			<tr>	
				<td>
					<label for="email_address">
						<?php _e( 'Email Address ', 'jvteam' ); ?>
					</label>
				</td>
				<td><input type="text" id="email_address" name="jv_team_email_address" value="<?php echo esc_attr( $email_address_value ) ?>" size="40" /></td>			
			</tr>
			<tr>		
				<td>
					<label for="website_url">
						<?php _e( 'Web site Url ', 'jvteam' ); ?>
					</label>
				</td>
				<td><input type="text" id="website_url" name="jv_team_website_url" value="<?php echo esc_attr( $website_url_name_value ) ?>" size="40" /><br />Please add http/https on link</td>			
			</tr>
			<tr>	
				<td>
					<label for="address1">
						<?php _e( 'Address 1 ', 'jvteam' ); ?>
					</label>
				</td>
				<td><input type="text" id="address1" name="jv_team_address1" value="<?php echo esc_attr( $address1_name_value ) ?>" size="40" /></td>			
			</tr>
			<tr>
				<td>
					<label for="address2">
						<?php _e( 'Address 2 ', 'jvteam' ); ?>
					</label>
				</td>
				<td><input type="text" id="address2" name="jv_team_address2" value="<?php echo esc_attr( $address2_name_value ) ?>" size="40" /></td>			
			</tr>
			<tr>
				<td>
					<label for="city">
						<?php _e( 'City ', 'jvteam' ); ?>
					</label>
				</td>
				<td><input type="text" id="city_name" name="jv_team_city_name" value="<?php echo esc_attr( $city_name_value ) ?>" size="40" /></td>			
			</tr>
			<tr>			
				<td>
					<label for="state">
						<?php _e( 'State ', 'jvteam' ); ?>
					</label>
				</td>
				<td><input type="text" id="state_name" name="jv_team_state_name" value="<?php echo esc_attr( $state_name_value ) ?>" size="40" /></td>			
			</tr>
			<tr>		
				<td>
					<label for="countey">
						<?php _e( 'Country ', 'jvteam' ); ?>
					</label>
				</td>
				<td><input type="text" id="country_name" name="jv_team_country_name" value="<?php echo esc_attr( $country_name_value ) ?>" size="40" /></td>			
			</tr>
			<tr>			
				<td>
					<label for="zipcode">
						<?php _e( 'Zipcode', 'jvteam' ); ?>
					</label>
				</td>
				<td><input type="text" id="zipcode_number" name="jv_team_zipcode" value="<?php echo esc_attr( $zipcode_number_value ) ?>" size="40" /></td>			
			</tr>
			<tr>
				<td>
					<label for="facebook">
						<?php _e( 'Facebook URL ', 'jvteam' ); ?>
					</label>
				</td>
				<td><input type="text" id="facebook_url" name="jv_team_facebook" value="<?php echo esc_attr( $facebook_name_value ) ?>" size="40" /><br />Please add http/https on link</td>			
			</tr>
			<tr>
				<td>
					<label for="linkedin">
						<?php _e( 'Linkedin URL ', 'jvteam' ); ?>
					</label>
				</td>
				<td><input type="text" id="linkedin_url" name="jv_team_linkedin" value="<?php echo esc_attr( $linkedin_name_value ) ?>" size="40" /><br />Please add http/https on link</td>			
			</tr>	
			<tr>
				<td>
					<label for="Instagram">
						<?php _e( 'Instagram URL ', 'jvteam' ); ?>
					</label>
				</td>
				<td><input type="text" id="instagram_url" name="jv_team_instagram" value="<?php echo esc_attr( $instagram_name_value ) ?>" size="40" /><br />Please add http/https on link</td>			
			</tr>
			<tr>		
				<td>
					<label for="google">
						<?php _e( 'Google+ URL ', 'jvteam' ); ?>
					</label>
				</td>
				<td><input type="text" id="google_url" name="jv_team_google" value="<?php echo esc_attr( $google_name_value ) ?>" size="40" /><br />Please add http/https on link</td>			
			</tr>	
			<tr>
				<td>
					<label for="twitter">
						<?php _e( 'Twitter URL ', 'jvteam' ); ?>
					</label>
				</td>
				<td><input type="text" id="twitter_url" name="jv_team_twitter" value="<?php echo esc_attr( $twitter_name_value ) ?>" size="40" /><br />Please add http/https on link</td>			
			</tr>	
			<tr>
				<td>
					<label for="customurl">
						<?php _e( 'Custom URL of Detail Page', 'jvteam' ); ?>
					</label>
				</td>
				<td><input type="text" id="custom_url_detail_page" name="custom_url_detail_page" value="<?php echo esc_attr( $custom_url_detail_page ) ?>" size="40" /><br />Please add http/https on link</td>			
			</tr>
			<tr>
				<td>
					<label for="Display Contact Form">
						<?php _e( 'Display Contact Form', 'jvteam' ); ?>
					</label>
				</td>
				<td><input type="checkbox" id="jv_team_display_contact_form" name="display_contact_form" <?php checked($display_contact_form,"on" ) ?> /></td>			
			</tr>
			<tr>
				<td><?php 
					$jv_team_get_caldera_forms = $this->jv_team_get_caldera_forms();
					?>
					<label for="customurl">
						<?php _e( 'Select Contact Form', 'jvteam' ); ?>
					</label>
				</td>
				<td>
				<select name="jv_team_contact_form">
				
				<?php
					foreach ( $jv_team_get_caldera_forms as $key=>$value ){
						?>
						<option value="<?php echo $key; ?>" <?php selected( $jv_team_contact_form, $key ); ?>><?php echo $value; ?></option>
				<?php	}
				?>
				</select>
				<br/>
				Please Install <a href="https://wordpress.org/plugins/caldera-forms/" target="_blank">Caldera Forms More Than Contact Forms</a> Plugin to display contact form and import json file as per provide in plugin.
				</td>			
			</tr>
		</table>
		<?php 	
		}
		function jv_team_save_meta_box_data($post_id)
			{
				if ( ! isset( $_POST['jv_team_meta_box_nonce'] ) ) {
					return;
				}
				if ( ! wp_verify_nonce( $_POST['jv_team_meta_box_nonce'], 'jv_team_meta_box' ) ) {
					return;
				}
				if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
					return;
				}
				if ( isset( $_POST['post_type'] ) && 'jv_team_members' == $_POST['post_type'] )
				{
					if ( ! current_user_can( 'edit_page', $post_id ) ) {
						return;
					}
				} 
				else 
				{
					if ( ! current_user_can( 'edit_post', $post_id ) ) {
						return;
					}
				}
					$designation_name = sanitize_text_field( $_POST['jv_team_designation'] );
					$facebook_url = sanitize_text_field( $_POST['jv_team_facebook'] );
					$linkedin_url = sanitize_text_field( $_POST['jv_team_linkedin'] );
					$instagram_url = sanitize_text_field( $_POST['jv_team_instagram'] );
					$google_url = sanitize_text_field( $_POST['jv_team_google'] );
					$twitter_url = sanitize_text_field( $_POST['jv_team_twitter'] );	
					$phone_number = sanitize_text_field( $_POST['jv_team_phone_number'] );
					$email_address = sanitize_text_field( $_POST['jv_team_email_address'] );
					$website_url = sanitize_text_field( $_POST['jv_team_website_url'] );
					$address1 = sanitize_text_field( $_POST['jv_team_address1'] );
					$address2 = sanitize_text_field( $_POST['jv_team_address2'] );
					$city_name = sanitize_text_field( $_POST['jv_team_city_name'] );
					$state_name = sanitize_text_field( $_POST['jv_team_state_name'] );
					$country_name = sanitize_text_field( $_POST['jv_team_country_name'] );
					$zipcode_name = sanitize_text_field( $_POST['jv_team_zipcode'] );
					$custom_url_detail_page = sanitize_text_field( $_POST['custom_url_detail_page'] );
					$jv_team_contact_form = sanitize_text_field( $_POST['jv_team_contact_form'] );
					$display_contact_form = sanitize_text_field( $_POST['display_contact_form'] );
					
					// Update the meta field in the database.
					update_post_meta( $post_id, 'jv_team_designation', $designation_name );
					update_post_meta( $post_id, 'jv_team_facebook', $facebook_url );
					update_post_meta( $post_id, 'jv_team_linkedin', $linkedin_url );
					update_post_meta( $post_id, 'jv_team_instagram', $instagram_url );
					update_post_meta( $post_id, 'jv_team_google', $google_url );
					update_post_meta( $post_id, 'jv_team_twitter', $twitter_url );
					update_post_meta( $post_id, 'jv_team_phone_number', $phone_number );
					update_post_meta( $post_id, 'jv_team_email_address', $email_address );
					update_post_meta( $post_id, 'jv_team_website_url', $website_url );
					update_post_meta( $post_id, 'jv_team_address1', $address1 );
					update_post_meta( $post_id, 'jv_team_address2', $address2 );
					update_post_meta( $post_id, 'jv_team_city_name', $city_name );
					update_post_meta( $post_id, 'jv_team_state_name', $state_name );
					update_post_meta( $post_id, 'jv_team_country_name', $country_name );
					update_post_meta( $post_id, 'jv_team_zipcode', $zipcode_name );
					update_post_meta( $post_id, 'custom_url_detail_page', $custom_url_detail_page );
					update_post_meta( $post_id, 'jv_team_contact_form', $jv_team_contact_form );
					update_post_meta( $post_id, 'display_contact_form', $display_contact_form );
			}
	function jv_team_updated_messages( $messages ) 
		{
			$post             = get_post();
			$post_type        = get_post_type( $post );
			$post_type_object = get_post_type_object( $post_type );
			$messages['jv_team_members'] = array(
				0  => '', 
				1  => __( 'Team updated.', 'jvteam' ),
				2  => __( 'Custom field updated.', 'jvteam' ),
				3  => __( 'Custom field deleted.', 'jvteam' ),
				4  => __( 'Team updated.', 'jvteam' ),
				5  => isset( $_GET['revision'] ) ? sprintf( __( 'Team restored to revision from %s', 'jvteam' ), wp_post_revision_title( (int) $_GET['revision'], false ) ) : false,
				6  => __( 'Team published.', 'jvteam' ),
				7  => __( 'Team saved.', 'jvteam' ),
				8  => __( 'Team submitted.', 'jvteam' ),
				9  => sprintf(
					__( 'Team scheduled for: <strong>%1$s</strong>.', 'jvteam' ),
					date_i18n( __( 'M j, Y @ G:i', 'jvteam' ), strtotime( $post->post_date ) )
				),
				10 => __( 'Team draft updated.', 'jvteam' )
			);
			if ( $post_type_object->publicly_queryable && 'jv_team_members' == $post_type ) 
			{
				$permalink = get_permalink( $post->ID );
				$view_link = sprintf( ' <a href="%s">%s</a>', esc_url( $permalink ),'View '. $post_type );
				$messages[ $post_type ][1] .= $view_link;
				$messages[ $post_type ][6] .= $view_link;
				$messages[ $post_type ][9] .= $view_link;
				$preview_permalink = add_query_arg( 'preview', 'true', $permalink );
				$preview_link = sprintf( ' <a target="_blank" href="%s">%s</a>', esc_url( $preview_permalink ),'Preview '. $post_type );
				$messages[ $post_type ][8]  .= $preview_link;
				$messages[ $post_type ][10] .= $preview_link;
			}
			return $messages;
		}
	function jv_team_thumbnail_column( $columns )
	{
		$column_thumbnail = array	(  'thumbnail' => __('Image', 'jvteam' ),
									   'title'  => __('Name','jvteam'),
									   'email' => __('Email', 'jvteam' ),
									   'designation' => __('Designation', 'jvteam' ),
									   'taxonomy-department_category' =>__('Department Category','jvteam')
									);
		$columns = array_slice( $columns, 0, 1, true ) + $column_thumbnail + array_slice( $columns, 1, 1, true ) + array_slice( $columns, 1, NULL, true );
			return $columns;
	}
	function jv_team_display_thumbnail( $column)
	{
		global $post;
			switch ( $column )
			{
				case 'thumbnail' :
					$default_image = esc_url( JVTEAM_PLUGIN_URL.'/assets/images/default.png' );
					$get_feture_image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ) ); 	
				    if( !empty( $get_feture_image[0] ) ) {
				    	echo '<img src="'. esc_url( $get_feture_image[0] ).'" style="max-height: 50px" name="'.esc_attr( get_the_title() ).'" />';
				    } else {
				    	echo '<img src="'.$default_image.'" style="max-height: 50px" name="'.esc_attr( get_the_title() ).'" />';  
				    }
		        	break;
					case 'email':
					echo get_post_meta($post->ID, 'jv_team_email_address' , true );
					break;
					case 'designation':
					echo get_post_meta($post->ID, 'jv_team_designation' , true );
					break;
			}
	}
	function jv_team_default_title( $title )
		{
			$screen = get_current_screen();
			if ( 'jv_team_members' == $screen->post_type ){
				$title = 'Enter the team member';
			}
			return $title;
		}
	function jv_team_change_image_box()
		{
			remove_meta_box( 'postimagediv', 'jv_team_members', 'side' );
    		add_meta_box( 'postimagediv', __('Team Member Image ( Best view size: 400px * 400px)'), 'post_thumbnail_meta_box', 'jv_team_members', 'side', 'low' );
		}
		function jv_team_get_caldera_forms() {

			include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

			if ( is_plugin_active( 'caldera-forms/caldera-core.php' ) ) {
			
				$forms = Caldera_Forms::get_forms();
				$got_forms = array();
				if(!empty($forms)){
				foreach($forms as $form_id=>$form){
					$got_forms += [$form_id => esc_html__( $form['name'], 'et_builder' )];
				}
				}else{
					$got_forms += [null => 'Sorry No Forms Make Some'];
				}
				return $got_forms;
			}
			else{
				$got_forms = array();
				$got_forms = [null => 'Please activate Caldera Forms. It is required'];
				return $got_forms;
			}

		}

	}
	new Divi_Team_Post_Type;
}
?>