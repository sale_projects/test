<?php
$team_members_categories = wp_get_post_terms( get_the_ID() ,'department_category', array("fields" => "ids"));
$cat_filter = '';
if ( count($team_members_categories ) > 0 ){
foreach ( $team_members_categories as $cat_id ){
		$cat_filter .= 'cat-'.$cat_id.' ';		
}
}
$link = "#";
if( $display_detail_page_link_type =='default' ) { 
		$link = get_permalink(get_the_ID());
}else if ( $display_detail_page_link_type =='custom' ){
		$url = get_post_meta( get_the_ID(),'custom_url_detail_page', true );
		$link = $url!= '' ? $url : '#';
}else{
		$link = "#";
}

$class_1 = '';$link_no_new_tab = '';$link_new_tab_open = '';$popup_st = '';$popup_end = '';$img_stlink = '';$target_link = '';$img_endlink = '';
// Link
if( $display_detail_page == 'on' && $display_popup_onteam == 'off' && $display_social_link == 'off' ) { $class_1 = ' et_pb_jv_team_members_link ';}
// Link without new tab
if( $display_detail_page == 'on' && $display_popup_onteam == 'off' && $link_open_in_new_tab == 'off' && $display_social_link == 'off' ) { $link_no_new_tab =  'onclick="location.href=\''.$link.'\';"'; } 	
// Link new tab
if( $display_detail_page == 'on' && $display_popup_onteam == 'off' && $link_open_in_new_tab == 'on' && $display_social_link == 'off' ){ 
	$link_new_tab_open = 'onclick="window.open(\''.$link.'\', \'_blank\')"'; 
}
// Popup ST
if( $display_popup_onteam == 'on' ) { $popup_st = '<a class="popup-modal" href="#teammodal'.get_the_ID().'">'; }
// Popup END
if( $display_popup_onteam == 'on' ) { $popup_end = '</a>'; }

// Image ST
if ($link_open_in_new_tab == 'on'){ $target_link = 'target="_blank"';}
if( $display_detail_page == 'on' && $display_popup_onteam == 'off' && $display_social_link == 'on'  ) { 
	$img_stlink = '<a href="'.$link.'" '.$target_link.'>'; 
} else if(  $display_popup_onteam == 'on' ){ 
	$img_stlink = '<a class="popup-modal" href="#teammodal'.get_the_ID().'">';
}else{ $img_stlink = '';}
// Image End
if( ( $display_detail_page == 'on' && $display_popup_onteam == 'off' ) || ( $display_popup_onteam == 'on' ) ) { $img_endlink = '</a>';}

?>
<div class="et_pb_column jv_team-scale-anm all et_pb_column_1_3 et_pb_jv_team_members_column <?php echo rtrim($cat_filter);?> <?php echo $class_1;?>"  <?php echo $link_no_new_tab; echo $link_new_tab_open;?>  >
	<div class="et_pb_jv_team jv_team_member_equalheight">
		<?php 
					$jv_team_thumb = wp_get_attachment_image_src(get_post_thumbnail_id(), 'jv_team_grid_image');
					if( $jv_team_thumb[0] != ''){ 
								$image_path = $jv_team_thumb[0] ;
							}else{
								$image_path = JVTEAM_PLUGIN_URL . '/assets/images/default.png';
							} 
					?>
			<?php echo $img_stlink;?><img src="<?php echo $image_path;?>" alt="<?php echo get_the_title();?>"><?php echo $img_endlink;?>
			<?php 
			if ( $display_social_link == 'on'){
					if (($jv_team_facebook || $jv_team_twitter || $jv_team_instagram || $jv_team_google ||$jv_team_linkdin) != '' ) {?>
						<ul class="et_pb_jv_team_social_link <?php if( $display_popup_onteam == 'on' ) { ?>style7_social<?php } ?>">
								<?php if( $jv_team_facebook != '' ) { ?><li><a href="<?php echo $jv_team_facebook;?>" target="_blank"><i class="et-pb-icon jv_team_member_social_font">&#xe0aa;</i></a></li><?php } ?>
								<?php if( $jv_team_twitter != '' ) { ?><li><a href="<?php echo $jv_team_twitter;?>" target="_blank"><i class="jv_team_member_social_font et-pb-icon">&#xe0ab;</i></a></li><?php } ?>
								<?php if( $jv_team_google != '' ) { ?><li><a href="<?php echo $jv_team_google;?>" target="_blank"><i class="jv_team_member_social_font et-pb-icon">&#xe0ad;</i></a></li><?php } ?>
								<?php if( $jv_team_linkdin != '' ) { ?><li><a href="<?php echo $jv_team_linkdin;?>"target="_blank"><i class="jv_team_member_social_font et-pb-icon">&#xe0b4;</i></a></li><?php } ?>
								<?php if( $jv_team_instagram != '' ) { ?><li><a href="<?php echo $jv_team_instagram;?>"target="_blank"><i class="jv_team_member_social_font et-pb-icon">&#xe0b1;</i></a></li><?php } ?>
						</ul>
			<?php 
				}
			}
			?>
			<div class="et_pb_jv_team_description" id="post-<?php the_ID(); ?>">
			<?php echo $img_stlink;?><h3 class="et_pb_jv_team_title"><?php echo get_the_title();?></h3><?php echo $img_endlink;
				if( $jv_team_designation != ''){ ?>
					<span class="et_pb_position"><?php echo $jv_team_designation;?></span>
					<?php } 
					if( $display_popup_onteam == 'on' ) { ?><a class="popup-modal" href="#teammodal<?php echo get_the_ID(); ?>"><?php echo $link_read_more_text;?></a><?php } ?>
			</div>
			<div class="layer"></div>
	</div>	
<?php 
if( $display_popup_onteam =='on') {
	$divi_popupteam_select_style_layout = !empty(get_option('divi_popupteam_select_style_layout')) ? get_option('divi_popupteam_select_style_layout') : 'style1';
	if ( file_exists( $jv_grid_path_p . '/popup-'.$divi_popupteam_select_style_layout.'.php' ) ){
		 include $jv_grid_path_p. '/popup-'.$divi_popupteam_select_style_layout.'.php';
    }else{
		include JVTEAM_PLUGIN_PATH. '/content-popup/popup-'.$divi_popupteam_select_style_layout.'.php';
    }
    
} 
?>
</div>