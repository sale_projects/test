<?php 
function jv_team_grid_modules() {
	class ET_Builder_Module_JV_Team_Grid_Member extends ET_Builder_Module {
		function init() {
			$this->name 			=  esc_html__( 'Team Members Grid', 'et_builder' );
			$this->slug 			= 'et_pb_jv_team_grid_members';
			$this->vb_support  		= 'partial';
			$this->main_css_element = '%%order_class%%.et_pb_jv_team_members';
		}
		function get_settings_modal_toggles() {
			return array(
				'general'	=> 	array(
					'toggles'	=> 	array(
						'jv_team_grid_member_general'	=> 	esc_html__( 'General Settings', 'et_builder' ),
						'jv_team_grid_member_category'	=> esc_html__( 'Category Settings', 'et_builder' ),
						'jv_team_grid_member_color'	=> 	esc_html__( 'Color', 'et_builder' ),
					),
				),
			);
		}
		function get_advanced_fields_config() {
			return  array(
				'fonts'	=> array(
					'team_name'	=> array(
					'label'		=> esc_html__( 'Team Name', 'et_builder' ),
					'css'		=> array(
						'main'		=> "{$this->main_css_element} .et_pb_jv_team_title,{$this->main_css_element} .et_pb_jv_team_description_hover a",
						)
					),
					'designation_name_fonts'	=> array(
						'label'		=> esc_html__( 'Designation ', 'et_builder' ),
						'css'		=> array(
							'main' 		=> "{$this->main_css_element} .et_pb_position",
						)
					),
					'descrption_name_fonts'	=> array(
						'label'		=> esc_html__( 'Descrption ', 'et_builder' ),
						'css'		=> array(
							'main' 		=> "{$this->main_css_element} .et_pb_jv_team_content p",
						),
						'font_size' => array(
							'default'	=> '15px',
						)
					),
				),
				'borders' => array(
					'default' => array(
						'css'      => array(
							'main' => array(
								'border_radii'	=> '%%order_class%%.et_pb_jv_team_members .et_pb_jv_team',
								'border_styles'	=> '%%order_class%%.et_pb_jv_team_members .et_pb_jv_team',
							),
						),
						'defaults' => array(
							'border_radii' 	=> 'on|0px|0px|0px|0px',
							'border_styles' => array(
									'width'		=> '1px',
									'color'		=> '#d9d9d9',
									'style'		=> 'solid',
							),
						),
					),					
				),
				'filters'		=> false,
				'max_width'		=> false,
				'background'	=> array(
						'use_background_image'	=>	false,
						'use_background_video'	=>	false,
				),
			); 
		}
		function get_custom_css_fields_config() {
			return array(
				'team_box_color' 	=> array(
					'label'		=> esc_html__( 'Team Members Box', 'et_builder' ),
					'selector' 	=> '.et_pb_jv_team_members_column',
				),
				'category_filter' 	=> array(
					'label'		=> esc_html__( 'Team Name', 'et_builder' ),
					'selector' 	=> '.et_pb_jv_team_category_list li a',
				),
				'category_filter_active'	=> array(
					'label'		=> esc_html__( 'Team Descrption', 'et_builder' ),
					'selector' 	=> '.et_pb_jv_team_category_list li a.active',
				),
				'team_image' 	=> array(
					'label'		=> esc_html__( 'Team Image', 'et_builder' ),
					'selector' 	=> '.et_pb_jv_team_image',
				),
				'social_link_text_color'	=> array(
					'label'		=> esc_html__( 'Social Link Color', 'et_builder' ),
					'selector' 	=> '.jv_team_member_social_font.et-pb-icon',
				),
				'social_link'	=> array(
					'label'		=> esc_html__( 'Social Link Hover Color', 'et_builder' ),
					'selector' 	=> '.jv_team_member_social_font.et-pb-icon:hover',
				),
			); 
		}

			
		function get_fields() {
			$fields = array(
				'select_style' => array(
					'label'			=> esc_html__( 'Select Style', 'et_builder' ),
					'type'			=> 'select',
					'option_category'	=> 'basic_option',
					'options'		=> array(
						'style1'		=> esc_html__( 'Style1 Layout', 'et_builder' ),
						'style2'		=> esc_html__( 'Style2 Layout', 'et_builder' ),
						'style3'        => esc_html__( 'Style3 Layout', 'et_builder' ),
						'style4'		=> esc_html__( 'Style4 Layout', 'et_builder' ),
						'style5'		=> esc_html__( 'Style5 Layout', 'et_builder' ),
						'style6'		=> esc_html__( 'Style6 Layout', 'et_builder' ),
						'style7'		=> esc_html__( 'Style7 Layout', 'et_builder' ),
						'style8'		=> esc_html__( 'Style8 Layout', 'et_builder' ),
						'style9'		=> esc_html__( 'Style9 Layout', 'et_builder' ),
						'style10'		=> esc_html__( 'Style10 Layout', 'et_builder' ),
						'style11'		=> esc_html__( 'Style11 Layout', 'et_builder' ),
						'style12'		=> esc_html__( 'Style12 Layout', 'et_builder' ),
						'style13'		=> esc_html__( 'Style13 Layout', 'et_builder' ),
						'style14'		=> esc_html__( 'Style14 Layout', 'et_builder' ),
						'style15'		=> esc_html__( 'Style15 Layout', 'et_builder' ),
						'style16'		=> esc_html__( 'Style16 Layout', 'et_builder' ),
						'style17'		=> esc_html__( 'Style17 Layout', 'et_builder' ),
						'style18'		=> esc_html__( 'Style18 Layout', 'et_builder' ),
						'style19'		=> esc_html__( 'Style19 Layout', 'et_builder' ),
						'style20'		=> esc_html__( 'Style20 Layout', 'et_builder' ),
						'style21'		=> esc_html__( 'Style21 Layout', 'et_builder' ),
						'style22'		=> esc_html__( 'Style22 Layout', 'et_builder' ),
						'style23'		=> esc_html__( 'Style23 Layout', 'et_builder' ),
						'style24'		=> esc_html__( 'Style24 Layout', 'et_builder' ),
						'style25'		=> esc_html__( 'Style25 Layout', 'et_builder' ),
						'style26'		=> esc_html__( 'Style26 Layout', 'et_builder' ),
						'style27'		=> esc_html__( 'Style27 Layout', 'et_builder' ),
						'style28'		=> esc_html__( 'Style28 Layout', 'et_builder' ),
						'style29'		=> esc_html__( 'Style29 Layout', 'et_builder' ),
						'style30'		=> esc_html__( 'Style30 Layout', 'et_builder' ),
						'style31'		=> esc_html__( 'Style31 Layout', 'et_builder' ),
						'style32'		=> esc_html__( 'Style32 Layout', 'et_builder' ),
						'style33'		=> esc_html__( 'Style33 Layout', 'et_builder' ),
						'style34'		=> esc_html__( 'Style34 Layout', 'et_builder' ),
						'style35'		=> esc_html__( 'Style35 Layout', 'et_builder' ),
						'style36'		=> esc_html__( 'Style36 Layout', 'et_builder' ),
						'style37'		=> esc_html__( 'Style37 Layout', 'et_builder' ),
						'style38'		=> esc_html__( 'Style38 Layout', 'et_builder' ),
						'style39'		=> esc_html__( 'Style39 Layout', 'et_builder' ),
						'style40'		=> esc_html__( 'Style40 Layout', 'et_builder' ),
						'style41'		=> esc_html__( 'Style41 Layout', 'et_builder' ),
						'style42'		=> esc_html__( 'Style42 Layout', 'et_builder' ),
						'style43'		=> esc_html__( 'Style43 Layout', 'et_builder' ),
						'style44'		=> esc_html__( 'Style44 Layout', 'et_builder' ),
						'style45'		=> esc_html__( 'Style45 Layout', 'et_builder' ),
						'style46'		=> esc_html__( 'Style46 Layout', 'et_builder' ),
						'style47'		=> esc_html__( 'Style47 Layout', 'et_builder' ),
						'style48'		=> esc_html__( 'Style48 Layout', 'et_builder' ),
						'style49'		=> esc_html__( 'Style49 Layout', 'et_builder' ),
						'style50'		=> esc_html__( 'Style50 Layout', 'et_builder' ),
						'style51'		=> esc_html__( 'Style51 Layout', 'et_builder' ),
						'style52'		=> esc_html__( 'Style52 Layout', 'et_builder' ),
						'style53'		=> esc_html__( 'Style53 Layout', 'et_builder' ),
						'style54'		=> esc_html__( 'Style54 Layout', 'et_builder' ),
						'style55'		=> esc_html__( 'Style55 Layout', 'et_builder' ),
						'style56'		=> esc_html__( 'Style56 Layout', 'et_builder' ),
						'style57'		=> esc_html__( 'Style57 Layout', 'et_builder' ),
						'style58'		=> esc_html__( 'Style58 Layout', 'et_builder' ),
						'style59'		=> esc_html__( 'Style59 Layout', 'et_builder' ),
						'style60'		=> esc_html__( 'Style60 Layout', 'et_builder' ),
					),
					
					'default'		=> 'style1',
					'toggle_slug'	=> 'jv_team_grid_member_general',
					'description'	=> esc_html__( 'Here you can select style.', 'et_builder' ),
				), 
				'filter_style'	=> array(
					'label'			=> esc_html__( 'Button Filter Style', 'et_builder' ),
					'type'			=> 'select',
					'option_category'	=> 'basic_option',
					'options'		=> array(
						'fstyle1'		=> esc_html__( 'Style 1', 'et_builder' ),
						'fstyle2'		=> esc_html__( 'Style 2', 'et_builder' ),
						'fstyle3'		=> esc_html__( 'Style 3', 'et_builder' ),
						'fstyle4'		=> esc_html__( 'Style 4', 'et_builder' ),
						'fstyle5'		=> esc_html__( 'Style 5', 'et_builder' ),
						'fstyle6'		=> esc_html__( 'Style 6', 'et_builder' ),
						'fstyle7'		=> esc_html__( 'Style 7', 'et_builder' ),
						'fstyle8'		=> esc_html__( 'Style 8', 'et_builder' ),
						'fstyle9'		=> esc_html__( 'Style 9', 'et_builder' ),
						'fstyle10'		=> esc_html__( 'Style 10', 'et_builder' ),
						'fstyle11'		=> esc_html__( 'Style 11', 'et_builder' ),
						'fstyle12'		=> esc_html__( 'Style 12', 'et_builder' ),
						'fstyle13'		=> esc_html__( 'Style 13', 'et_builder' ),
						'fstyle14'		=> esc_html__( 'Style 14', 'et_builder' ),
						'fstyle15'		=> esc_html__( 'Style 15', 'et_builder' ),
						'fstyle16'		=> esc_html__( 'Style 16', 'et_builder' ),
						'fstyle17'		=> esc_html__( 'Style 17', 'et_builder' ),
						'fstyle18'		=> esc_html__( 'Style 18', 'et_builder' ),
						'fstyle19'		=> esc_html__( 'Style 19', 'et_builder' ),
						'fstyle20'		=> esc_html__( 'Style 20', 'et_builder' ),
					),
					'default'			=> 'fstyle1',
					'toggle_slug'		=> 'jv_team_grid_member_general',
					'description'       => esc_html__( 'Here you can select filter style.', 'et_builder' ),
				), 
				'display_category_filter'   => array(
					'label'			=> esc_html__( 'Display Category Filter', 'et_builder' ),
					'type'			=> 'yes_no_button',
					'option_category'	=> 'configuration',
					'options'		=> array(
						'on'  			=> esc_html__( 'yes', 'et_builder' ),
					    'off' 			=> esc_html__( 'No', 'et_builder' ),
					),
					'default'		=> 'on',
					'toggle_slug'	=> 'jv_team_grid_member_category',
					'description'	=> esc_html__( 'This setting will turn on and off the Category Filter.', 'et_builder' ),
				),	
				'display_category'	=> array(
					'label'			=> esc_html__( 'Display Category On Filter/Display Team Member By Category', 'et_builder' ),
					'type'			=> 'select',
					'option_category'	=> 'basic_option',
					'options'		=> array(
						'all'			=> esc_html__( 'All', 'et_builder' ),
						'specificcategory'	=> esc_html__( 'Specific Category', 'et_builder' ),
					), 
					'toggle_slug'	=> 'jv_team_grid_member_category',
					'default'		=> 'all',
					'description'	=> esc_html__( 'Here you can select the team member display category.', 'et_builder' ),
				),
				'include_categories' 	=> array(
					'label'			=> esc_html__( 'Specific Categories', 'et_builder' ),
					'renderer'		=> 'et_builder_include_categories_option',
					'option_category'  => 'basic_option',
					'toggle_slug'	=> 'jv_team_grid_member_category',
					'show_if'		=> array('display_category' => 'specificcategory'),
					'renderer_options' 	=> array(
						'use_terms' 	=> true,
						'term_name'	 	=> 'department_category',
					),
					'description'	=> esc_html__( 'Choose which categories you would like to include in the team filter.', 'et_builder' ),
				),
				'posts_number' 	=> array(
					'label'			=> esc_html__( 'Display Number of Team Member', 'et_builder' ),
					'type'			=> 'text',
					'option_category'	=> 'configuration',
					'toggle_slug'	=> 'jv_team_grid_member_general',
					'description'	=> esc_html__( 'Choose how many team members you would like to display.', 'et_builder' ),
				),
				'orderby' => array(
					'label'			=> esc_html__( 'Order By', 'et_builder' ),
					'type'			=> 'select',
					'option_category'	=> 'configuration',
					'options'		=> array(
						'date_desc'		=> esc_html__( 'Date: new to old', 'et_builder' ),
						'date_asc'		=> esc_html__( 'Date: old to new', 'et_builder' ),
						'title_asc'  	=> esc_html__( 'Title: a-z', 'et_builder' ),
						'title_desc' 	=> esc_html__( 'Title: z-a', 'et_builder' ),
						'menu_order_desc'	=> esc_html__( 'Menu Order : DESC', 'et_builder' ),
						'menu_order_asc' 	=> esc_html__( 'Menu Order : ASC', 'et_builder' ),
						'rand'			=> esc_html__( 'Random', 'et_builder' ),
					),
					'default'		=> 'date_desc',
					'toggle_slug'	=> 'jv_team_grid_member_general',
					'description'	=> esc_html__( 'Here you can adjust the order in which posts are displayed.', 'et_builder' ),
				),
				'category_orderby' 	=> array(
					'label'			=> esc_html__( 'Category Filter List Order By', 'et_builder' ),
					'type'			=> 'select',
					'option_category'	=> 'configuration',
					'options'			=> array(
						'id_desc'  		=> esc_html__( 'ID: High to Low', 'et_builder' ),
						'id_asc'   		=> esc_html__( 'ID: Low to High', 'et_builder' ),
						'name_asc'  	=> esc_html__( 'Title: a-z', 'et_builder' ),
						'name_desc' 	=> esc_html__( 'Title: z-a', 'et_builder' ),
						'slug_desc' 	=> esc_html__( 'Slug : z-a', 'et_builder' ),
						'slug_asc' 		=> esc_html__( 'Slug : a-z', 'et_builder' ),
						'count'       	=> esc_html__( 'Count', 'et_builder' ),
					),
					'default'		=> 'id_desc',
					'show_if'		=> array('display_category_filter' => 'on'),	
					'toggle_slug'	=> 'jv_team_grid_member_category',
					'description'	=> esc_html__( 'Here you can adjust Category Filter List Order.', 'et_builder' ),
				),	
				'filter_all_label' 	=> array(
					'label'			=> esc_html__( 'ALL Text Filter Label', 'et_builder' ),
					'type'			=> 'text',
					'option_category'	=> 'configuration',
					'show_if'		=> array('display_category_filter' => 'on'),	
					'default'		=> 'All',
					'toggle_slug'	=> 'jv_team_grid_member_category',
					'description'	=> esc_html__( 'ALL Text Filter Label', 'et_builder' ),
				),
				'team_members_display_pagination'	=> array(
					'label'			=> esc_html__( 'Display Pagination', 'et_builder' ),
					'type'			=> 'yes_no_button',
					'option_category'	=> 'configuration',
					'options'		=> array(
						'off' 			=> esc_html__( 'No', 'et_builder' ),
						'on'  			=> esc_html__( 'yes', 'et_builder' ),
					),
					'default'		=> 'off',
					'toggle_slug'	=> 'jv_team_grid_member_general',
					'description'	=> esc_html__( 'This setting will turn on and off the Pagination.', 'et_builder' ),
				),	
				'display_social_link' 	=> array(
					'label'			=> esc_html__( 'Display Social Link', 'et_builder' ),
					'type'			=> 'yes_no_button',
					'option_category'	=> 'configuration',
					'options'		=> array(
						'on' 		 	=> esc_html__( 'yes', 'et_builder' ),
						'off' 			=> esc_html__( 'No', 'et_builder' ),
					),
					'toggle_slug'	=> 'jv_team_grid_member_general',
					'default'		=> 'off',
					'description'	=> esc_html__( 'This setting will turn on and off the display social link.', 'et_builder' ),
					),
				'display_popup_onteam' => array(
					'label'			=> esc_html__( 'Display Popup On Link', 'et_builder' ),
					'type'			=> 'yes_no_button',			
					'options'		=> array(
						'off' 			=> esc_html__( 'No', 'et_builder' ),
						'on'  			=> esc_html__( 'yes', 'et_builder' ),
					),
					'default'		=> 'off',
					'toggle_slug'	=> 'jv_team_grid_member_general',
					'description'	=> esc_html__( 'This setting will turn on and off the display Popup on link.', 'et_builder' ),
				),
				'display_detail_page' 	=> array(
					'label'			=> esc_html__( 'Display Detail Page Link', 'et_builder' ),
					'type'			=> 'yes_no_button',	
 					'show_if'		=> array('display_popup_onteam' => 'off'),	
					'options'		=> array(
						'on'  			=> esc_html__( 'yes', 'et_builder' ),
						'off' 			=> esc_html__( 'No', 'et_builder' ),
					),
					'toggle_slug'	=> 'jv_team_grid_member_general',
					'default'		=> 'on',
					'description'	=> esc_html__( 'This setting will turn on and off the display social link.', 'et_builder' ),
				),
				'display_detail_page_link_type' 	=> array(
					'label'			=> esc_html__( 'Display Detail Page Link Type', 'et_builder' ),
					'type'			=> 'select',
					'show_if'		=> array('display_detail_page' => 'on'),
					'option_category'	=> 'basic_option',
						'options'		=> array(
						'default'   	=> esc_html__( 'Default', 'et_builder' ),
						'custom'   		=> esc_html__( 'Custom', 'et_builder' ),
					), 
					'default'		=> 'default',
					'toggle_slug'	=> 'jv_team_grid_member_general',
					'description'	=> esc_html__( 'Here you can select the team member Display Detail Page Link Type.', 'et_builder' ),
				),
				'link_open_in_new_tab' 	=> array(
					'label'			=> esc_html__( 'Open In a New Tab', 'et_builder' ),
					'type'			=> 'yes_no_button',	
					'show_if'		=> array('display_detail_page' => 'on'),		
					'options'		=> array(
						'off' 			=> esc_html__( 'No', 'et_builder' ),
						'on'  			=> esc_html__( 'yes', 'et_builder' ),
					),
					'default'		=> 'off',
					'toggle_slug'	=> 'jv_team_grid_member_general',
					'description'	=> esc_html__( 'This setting will turn on and off the Open In a New Tab link.', 'et_builder' ),
				),
				'link_read_more_text' 	=> array(
					'label'			=> esc_html__( 'Read More Text', 'et_builder' ),
					'type'			=> 'text',
					'option_category'	=> 'configuration',
					'default'		=> 'Read More',
					'toggle_slug'	=> 'jv_team_grid_member_general',
					'description'	=> esc_html__( 'Read More Text of team members you would like to display.', 'et_builder' ),
				),
				'style1_bg_content_color'	=> array(
					'label'			=> esc_html__( 'Style 1 Content Background Color', 'et_builder' ),
					'type'			=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style1'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a background content color for this module.', 'et_builder' ),
				),
				'style1_button_active_color' => array(
					'label'			=> esc_html__( 'Style 1 Button Filter Active Color', 'et_builder' ),
					'type'			=> 'color-alpha',
 					'show_if'		=> array('filter_style' => 'fstyle1'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter active color for this module.', 'et_builder' ),
				),
				'style1_button_color' => array(
					'label'			=> esc_html__( 'Style 1 Button Filter Color', 'et_builder' ),
					'type'			=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('filter_style' => 'fstyle1'),
					'custom_color'	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter color for this module.', 'et_builder' ),
				),
				'style2_bg_content_color' 	=> array(
					'label'			=> esc_html__( 'Style 2 Content Background Color', 'et_builder' ),
					'type'			=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('select_style' => 'style2'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a background content color for this module.', 'et_builder' ),
				),
				'style2_bg_content_hover_color' 	=> array(
					'label'			=> esc_html__( 'Style 2 Content Background hover Color', 'et_builder' ),
					'type'			=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('select_style' => 'style2'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a background content hover color for this module.', 'et_builder' ),
				),
				'style2_bg_email_text_color'	=> array(
					'label'			=> esc_html__( 'Style 2 email text Color', 'et_builder' ),
					'type'			=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('select_style' => 'style2'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a email text color for this module.', 'et_builder' ),
				),
				'style2_bg_image_hover_color' 	=> array(
					'label'			=> esc_html__( 'Style 2 Image hover color', 'et_builder' ),
					'type'			=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('select_style' => 'style2'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a image background hover color for this module.', 'et_builder' ),
				),
				'style2_social_media_bg_color' => array(
					'label'			=> esc_html__( 'Style 2 Social Media Background color', 'et_builder' ),
					'type'			=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('select_style' => 'style2'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a social media background color for this module.', 'et_builder' ),
				),
				'style2_social_media_bg_hover_color' => array(
					'label'			=> esc_html__( 'Style 2 Social Media Background Hover color', 'et_builder' ),
					'type'			=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('select_style' => 'style2'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a social media background hover color for this module.', 'et_builder' ),
				),
				'style2_button_active_color' 	=> array(
					'label'			=> esc_html__( 'Style 2 Button Filter Active Color', 'et_builder' ),
					'type'			=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('filter_style' => 'fstyle2'),
					'custom_color' 	=> true,
					'description' 	=> esc_html__( 'Use the color picker to choose a button filter active color color for this module.', 'et_builder' ),
				),
				'style2_button_animation_border_color' 	=> array(
					'label'			=> esc_html__( 'Style 2 Button Filter Animation Border Color', 'et_builder' ),
					'type'			=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('filter_style' => 'fstyle2'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter animation border color color for this module.', 'et_builder' ),
				),
				'style2_button_hover_color' 	=> array(
					'label'			=> esc_html__( 'Style 2 Button Filter Border Color & Button hover color', 'et_builder' ),
					'type'			=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('filter_style' => 'fstyle2'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter border color & button hover color for this module.', 'et_builder' ),
				),
				'style3_bg_content_color' 	=> array(
					'label'			=> esc_html__( 'Style 3 Content Background Color', 'et_builder' ),
					'type'			=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('select_style' => 'style3'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a background content color for this module.', 'et_builder' ),
				),
				'style3_bg_image_hover_color' 	=> array(
					'label'        	=> esc_html__( 'Style 3 Image hover Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('select_style' => 'style3'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a image background hover color for this module.', 'et_builder' ),
				),
				'style3_bg_read_more_color' => array(
					'label'        	=> esc_html__( 'Style 3 Read more hover Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('select_style' => 'style3'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a read more background color for this module.', 'et_builder' ),
				),
				'style3_button_border_color' 	=> array(
					'label'        	=> esc_html__( 'Style 3 Button Filter Border Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('filter_style' => 'fstyle3'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter border color for this module.', 'et_builder' ),
				),
				'style3_button_color' 	=> array(
					'label'        	=> esc_html__( 'Style 3 Button Filter Active Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('filter_style' => 'fstyle3'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter active color for this module.', 'et_builder' ),
				),
				'style3_button_hover_color' 	=> array(
					'label'        	=> esc_html__( 'Style 3 Button Filter hover Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('filter_style' => 'fstyle3'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter hover color for this module.', 'et_builder' ),
				),		
				'style4_bg_content_color' 	=> array(
					'label'        	=> esc_html__( 'Style 4 Content Background Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('select_style' => 'style4'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a background content color for this module.', 'et_builder' ),
				),
				'style4_bg_image_gardian1_color' => array(
					'label'			=> esc_html__( 'Style 4 Image  Gardian Color 1', 'et_builder' ),
					'type'			=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('select_style' => 'style4'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a image gardian color 1 for this module.', 'et_builder' ),
					),
				'style4_bg_image_gardian2_color' => array(
					'label'			=> esc_html__( 'Style 4 Image Gardian Color 2', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('select_style' => 'style4'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a image gardian color 2 for this module.', 'et_builder' ),
				),		
				'style4_button_border_color' 	=> array(
					'label'			=> esc_html__( 'Style 4 Button Filter Border Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('filter_style' => 'fstyle4'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter border color for this module.', 'et_builder' ),
				),
				'style4_button_color' => array(
					'label'        	=> esc_html__( 'Style 4 Button Filter Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('filter_style' => 'fstyle4'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter color for this module.', 'et_builder' ),
				),
				'style5_bg_content_color' 	=> array(
					'label'        	=> esc_html__( 'Style 5 Content Background Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('select_style' => 'style5'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a background content color for this module.', 'et_builder' ),
				),
				'style5_bg_border_color' => array(
					'label'        	=> esc_html__( 'Style 5 Content border Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('select_style' => 'style5'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a background border color for this module.', 'et_builder' ),
				),
				'style5_button_border_active_color' 	=> array(
					'label'        	=> esc_html__( 'Style 5 Button Filter Border Active Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('filter_style' => 'fstyle5'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter border active color for this module.', 'et_builder' ),
				),
				'style5_button_border_color' => array(
					'label'        	=> esc_html__( 'Style 5 Button Filter Border Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('filter_style' => 'fstyle5'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter border color for this module.', 'et_builder' ),
				),
				'style6_bg_content_color' 	=> array(
					'label'        	=> esc_html__( 'Style 6 Content Background Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('select_style' => 'style6'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a background content color for this module.', 'et_builder' ),
				),
				'style6_button_active_color' 	=> array(
					'label'        	=> esc_html__( 'Style 6 Button Filter Active Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('filter_style' => 'fstyle6'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter active color for this module.', 'et_builder' ),
				),
				'style6_button_color' 	=> array(
					'label'        	=> esc_html__( 'Style 6 Button Filter Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('filter_style' => 'fstyle6'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter color for this module.', 'et_builder' ),
				),
				'style7_bg_content_color' 	=> array(
					'label'        	=> esc_html__( 'Style 7 Content Background Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('select_style' => 'style7'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a background content color for this module.', 'et_builder' ),
				),
				'style7_bg_overlayer_color'	 => array(
					'label'        	=> esc_html__( 'Style 7 Overlayer Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('select_style' => 'style7'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a overlayer background color for this module.', 'et_builder' ),
				),
				'style7_button_border_active_color' => array(
					'label'        	=> esc_html__( 'Style 7 Button Filter Border Active Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('filter_style' => 'fstyle7'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter border active color for this module.', 'et_builder' ),
				),
				'style7_button_border_color' => array(
					'label'        	=> esc_html__( 'Style 7 Button Filter Border Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('filter_style' => 'fstyle7'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter border color for this module.', 'et_builder' ),
				),
				'style8_button_active_color' 	=> array(
					'label'        	=> esc_html__( 'Style 8 Button Filter Active Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('filter_style' => 'fstyle8'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter active color for this module.', 'et_builder' ),
				),
				'style8_button_border_color' 	=> array(
					'label'        	=> esc_html__( 'Style 8 Button Filter Border Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('filter_style' => 'fstyle8'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter border color for this module.', 'et_builder' ),
				),
				'style9_bg_content_color' 	=> array(
					'label'        	=> esc_html__( 'Style 9 Content Background Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('select_style' => 'style9'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a background content color for this module.', 'et_builder' ),
				),
				'style9_bg_content_hover_color' => array(
					'label'        	=> esc_html__( 'Style 9 Content hover Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('select_style' => 'style9'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a background content hover color for this module.', 'et_builder' ),
				),
				'style9_bg_content_border_color' 	=> array(
					'label'        	=> esc_html__( 'Style 9 Content border Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('select_style' => 'style9'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a background content border color for this module.', 'et_builder' ),
				),
				'style9_button_active_color' 	=> array(
					'label'        	=> esc_html__( 'Style 9 Button Filter Active Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('filter_style' => 'fstyle9'),
					'custom_color'	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter active color for this module.', 'et_builder' ),
				),
				'style9_button_color'	=> array(
					'label'			=> esc_html__( 'Style 9 Button Filter Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('filter_style' => 'fstyle9'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter color for this module.', 'et_builder' ),
				),
				'style10_bg_content_color' 	=> array(
					'label'			=> esc_html__( 'Style 10 Content Background Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('select_style' => 'style10'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a background content color for this module.', 'et_builder' ),
				),
				'style10_bg_border_color' 	=> array(
					'label'			=> esc_html__( 'Style 10 border  Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('select_style' => 'style10'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a background border color for this module.', 'et_builder' ),
				),
				'style10_social_media_bg_color' 	=> array(
					'label'        	=> esc_html__( 'Style 10 Social media Background Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('select_style' => 'style10'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a social media background color for this module.', 'et_builder' ),
				),
				'style10_button_active_border_color' 	=> array(
					'label'        	=> esc_html__( 'Style 10 Button Filter Active Border Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('filter_style' => 'fstyle10'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter active border color for this module.', 'et_builder' ),
				),
				'style10_button_color' 	=> array(
					'label'        	=> esc_html__( 'Style 10 Button Filter Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('filter_style' => 'fstyle10'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter color for this module.', 'et_builder' ),
				),
				'style11_bg_content_color' 	=> array(
					'label'        	=> esc_html__( 'Style 11 Content Background Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('select_style' => 'style11'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a background content color for this module.', 'et_builder' ),
				),
				'style11_bg_content_hover_color' => array(
					'label'        	=> esc_html__( 'Style 11 Content hover Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('select_style' => 'style11'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a background content hover color for this module.', 'et_builder' ),
				),
				'style11_bg_hover_border_color' => array(
					'label'        	=> esc_html__( 'Style 11  hover border Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('select_style' => 'style11'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a background hover border color for this module.', 'et_builder' ),
				),
				'style11_social_media_bg_color' => array(
					'label'        	=> esc_html__( 'Style 11 Social Media Background Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('select_style' => 'style11'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a social media background color color for this module.', 'et_builder' ),
				),
				'style11_social_media_bg_hover_color' => array(
					'label'        	=> esc_html__( 'Style 11 Social Media Background Hover Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('select_style' => 'style11'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a social media background hover color for this module.', 'et_builder' ),
				),
				'style11_button_active_color' 	=> array(
					'label'        	=> esc_html__( 'Style 11 Button Filter Active Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('filter_style' => 'fstyle11'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter active color for this module.', 'et_builder' ),
				),
				'style11_button_color' => array(
					'label'        	=> esc_html__( 'Style 11 Button Filter Hover Color & Border Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('filter_style' => 'fstyle11'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter color & border color for this module.', 'et_builder' ),
				),
				'style12_bg_content_color' 	=> array(
					'label'        	=> esc_html__( 'Style 12 Content Background Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('select_style' => 'style12'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a background content color for this module.', 'et_builder' ),
				),
				'style12_bg_image_hover_color' 	=> array(
					'label'        	=> esc_html__( 'Style 12 Image Background Hover Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('select_style' => 'style12'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a image background hover color color for this module.', 'et_builder' ),
				),
				'style12_border_color' 	=> array(
					'label'        	=> esc_html__( 'Style 12 Border Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('select_style' => 'style12'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a border color for this module.', 'et_builder' ),
				),
				'style12_social_media_bg_color' 	=> array(
					'label'        	=> esc_html__( 'Style 12 Social Media Background Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('select_style' => 'style12'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a social media background color for this module.', 'et_builder' ),
				),
				'style12_button_active_border_color' 	=> array(
					'label'        	=> esc_html__( 'Style 12 Button Filter Active Border Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('filter_style' => 'fstyle12'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter active border color for this module.', 'et_builder' ),
				),
				'style12_button_border_color' 	=> array(
					'label'        	=> esc_html__( 'Style 12 Button Filter Border Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('filter_style' => 'fstyle12'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter border color for this module.', 'et_builder' ),
				),
				'style13_bg_content_color' => array(
					'label'        	=> esc_html__( 'Style 13 Content Background Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('select_style' => 'style13'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a content color for this module.', 'et_builder' ),
				),
				'style13_bg_content_hover_color' 	=> array(
					'label'        	=> esc_html__( 'Style 13 Content hover Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('select_style' => 'style13'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a background content hover color for this module.', 'et_builder' ),
				),
				'style13_bg_image_hover_color' => array(
					'label'        	=> esc_html__( 'Style 13 Image hover Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('select_style' => 'style13'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a background image hover color for this module.', 'et_builder' ),
				),
				'style13_bg_image_hover_border_color' => array(
					'label'        	=> esc_html__( 'Style 13 Image hover border Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('select_style' => 'style13'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a image hover border color for this module.', 'et_builder' ),
				),
				'style13_bg_content_border_color' => array(
					'label'        	=> esc_html__( 'Style 13 Content border Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('select_style' => 'style13'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a background content border color for this module.', 'et_builder' ),
				),
				'style13_button_active_color' 	=> array(
					'label'			=> esc_html__( 'Style 13 Button Filter Active Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('filter_style' => 'fstyle13'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter active color for this module.', 'et_builder' ),
				),
				'style13_button_active_hover_color' => array(
					'label'        	=> esc_html__( 'Style 13 Button Filter Active Hover Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('filter_style' => 'fstyle13'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter active hover color for this module.', 'et_builder' ),
				),
				'style13_button_hover_color' 	=> array(
					'label'        	=> esc_html__( 'Style 13 Button Filter Border Color & Button Filter Hover Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('filter_style' => 'fstyle13'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter border color & button filter hover color for this module.', 'et_builder' ),
				),
				'style14_bg_content_color' 	=> array(
					'label'        	=> esc_html__( 'Style 14 Content Background Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('select_style' => 'style14'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a background content color for this module.', 'et_builder' ),
				),
				'style14_social_media_bg_color' => array(
					'label'        	=> esc_html__( 'Style 14 Social Media Background Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('select_style' => 'style14'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a social media background color color for this module.', 'et_builder' ),
				),
				'style14_button_active_color' => array(
					'label'        	=> esc_html__( 'Style 14 Button Filter Active Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('filter_style' => 'fstyle14'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter active color for this module.', 'et_builder' ),
				),
				'style14_button_color' 	=> array(
					'label'        	=> esc_html__( 'Style 14 Button Filter Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('filter_style' => 'fstyle14'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter active color for this module.', 'et_builder' ),
				),
				'style15_bg_image_color' 	=> array(
					'label'        	=> esc_html__( 'Style 15 Image Background Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('select_style' => 'style15'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a background image color for this module.', 'et_builder' ),
				),
				'style15_bg_image_over_color' 	=> array(
					'label'			=> esc_html__( 'Style 15 Image Over Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('select_style' => 'style15'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a background image hover color for this module.', 'et_builder' ),
				),
				'style15_button_active_color' 	=> array(
					'label'        	=> esc_html__( 'Style 15 Button Filter Active Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('filter_style' => 'fstyle15'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter active color for this module.', 'et_builder' ),
				),
				'style15_button_active_hover_color' 	=> array(
					'label'        	=> esc_html__( 'Style 15 Button Filter Active Hover Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('filter_style' => 'fstyle15'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter active hover color for this module.', 'et_builder' ),
				),
				'style15_button_hover_color' 	=> array(
					'label'        	=> esc_html__( 'Style 15 Button Filter Border Color & Button Filter Hover Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('filter_style' => 'fstyle15'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter border color & button filter hover color for this module.', 'et_builder' ),
				),
				'style16_bg_content_color' 	=> array(
					'label'        	=> esc_html__( 'Style 16 Content Background Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('select_style' => 'style16'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a background content color for this module.', 'et_builder' ),
				),
				'style16_bg_content_hover_color' => array(
					'label'        	=> esc_html__( 'Style 16 Content hover Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('select_style' => 'style16'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a background content hover color for this module.', 'et_builder' ),
				),
				'style16_bg_image_border_color' 	=> array(
					'label'        	=> esc_html__( 'Style 16 Image border Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('select_style' => 'style16'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a background image hover color for this module.', 'et_builder' ),
				),
				'style16_bg_image_hover_color' 	=> array(
					'label'        	=> esc_html__( 'Style 16 Image hover Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('select_style' => 'style16'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a bakground image hover color for this module.', 'et_builder' ),
				),
				'style16_button_active_color' 	=> array(
					'label'			=> esc_html__( 'Style 16 Button Filter Active Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('filter_style' => 'fstyle16'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter active color for this module.', 'et_builder' ),
				),
				'style16_button_hover_color' => array(
					'label'        	=> esc_html__( 'Style 16 Button Filter Hover Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('filter_style' => 'fstyle16'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter hover color for this module.', 'et_builder' ),
				),
				'style16_button_border_color' 	=> array(
					'label'			=> esc_html__( 'Style 16 Button Filter Border Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('filter_style' => 'fstyle16'),
					'custom_color' 	=> true,
					'description' 	=> esc_html__( 'Use the color picker to choose a button border filter color for this module.', 'et_builder' ),
				),
				'style17_bg_content_color' 	=> array(
					'label'			=> esc_html__( 'Style 17 Content Background Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('select_style' => 'style17'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a background content color for this module.', 'et_builder' ),
				),
				'style17_bg_image_hover_color' 	=> array(
					'label'        	=> esc_html__( 'Style 17 Image hover Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('select_style' => 'style17'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a image background hover color for this module.', 'et_builder' ),
				),
				'style17_border_color' 	=> array(
					'label'        	=> esc_html__( 'Style 17 Border Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('select_style' => 'style17'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a border color for this module.', 'et_builder' ),
				),
				'style17_mobile_content_bg_color' 	=> array(
					'label'        	=> esc_html__( 'Style 17 Mobile Content Bacground Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('select_style' => 'style17'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a mobile no text background color for this module.', 'et_builder' ),
				),
				'style17_mobile_content_bg_hover_color' => array(
					'label'        	=> esc_html__( 'Style 17 Mobile Content Bacground Hover Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('select_style' => 'style17'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a mobile no text background hover color for this module.', 'et_builder' ),
				),
				'style17_mobile_number_txt_color' => array(
					'label'        	=> esc_html__( 'Style 17 Mobile Number Text Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('select_style' => 'style17'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a mobile no text color for this module.', 'et_builder' ),
				),
				'style17_mobile_number_txt_hover_color' 	=> array(
					'label'        	=> esc_html__( 'Style 17 Mobile Number Text Hover Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('select_style' => 'style17'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a mobile no text hover color for this module.', 'et_builder' ),
				),
				'style17_button_active_color' => array(
					'label'        	=> esc_html__( 'Style 17 Button Filter Active Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('filter_style' => 'fstyle17'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter active color for this module.', 'et_builder' ),
				),
				'style17_button_border_color' 	=> array(
					'label'        	=> esc_html__( 'Style 17 Button Filter Border Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('filter_style' => 'fstyle17'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter border color for this module.', 'et_builder' ),
				),
				'style17_button_hover_color' 	=> array(
					'label'        	=> esc_html__( 'Style 17 Button Filter Hover Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('filter_style' => 'fstyle17'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter hover color for this module.', 'et_builder' ),
				),
				'style18_bg_content_color' 	=> array(
					'label'        	=> esc_html__( 'Style 18 Content Background Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('select_style' => 'style18'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a background content color for this module.', 'et_builder' ),
				),
				'style18_bg_image_hover_color' 	=> array(
					'label'        	=> esc_html__( 'Style 18 Image hover Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('select_style' => 'style18'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a background image hover color for this module.', 'et_builder' ),
				),
				'style18_button_border_color' 	=> array(
					'label'       	=> esc_html__( 'Style 18 Button Filter Border Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('filter_style' => 'fstyle18'),
					'custom_color' 	=> true,
					'description' 	=> esc_html__( 'Use the color picker to choose a button filter border color for this module.', 'et_builder' ),
				),
				'style18_button_color' 	=> array(
					'label'        	=> esc_html__( 'Style 18 Button Filter Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('filter_style' => 'fstyle18'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter color for this module.', 'et_builder' ),
				),
				'style19_bg_image_hover_color' 	=> array(
					'label'        	=> esc_html__( 'Style 19 Image hover Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('select_style' => 'style19'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a  background image hover color for this module.', 'et_builder' ),
				),
				'style19_button_gardien1_color' 	=> array(
					'label'        	=> esc_html__( 'Style 19 Button Filter Gardien Color 1', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('filter_style' => 'fstyle19'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter gardien color 1 for this module.', 'et_builder' ),
				),
				'style19_button_gardien2_color' 	=> array(
					'label'        	=> esc_html__( 'Style 19 Button Filter Gardien Color 2', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('filter_style' => 'fstyle19'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter gardien color 2 for this module.', 'et_builder' ),
				),
				'style19_button_gardien3_color' 	=> array(
					'label'        	=> esc_html__( 'Style 19 Button Filter Gardien Color 3', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('filter_style' => 'fstyle19'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter gardien color 3 for this module.', 'et_builder' ),
				),
				'style19_button_gardien4_color' 	=> array(
					'label'        	=> esc_html__( 'Style 19 Button Filter Gardien Color 4', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('filter_style' => 'fstyle19'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter gardien color 4 for this module.', 'et_builder' ),
				),
				'style19_button_active_border_color' 	=> array(
					'label'        	=> esc_html__( 'Style 19 Button Filter Active Border Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('filter_style' => 'fstyle19'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter active border color  for this module.', 'et_builder' ),
				),
				'style20_bg_content_color' 	=> array(
					'label'        	=> esc_html__( 'Style 20 Content Background Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('select_style' => 'style20'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a background content color for this module.', 'et_builder' ),
				),
				'style20_bg_image_border_color' 	=> array(
					'label'        	=> esc_html__( 'Style 20 Image Border Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('select_style' => 'style20'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a background image border color for this module.', 'et_builder' ),
				),
				'style20_bg_image_hover_border_color' 	=> array(
					'label'        	=> esc_html__( 'Style 20 Image Border Hover Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('select_style' => 'style20'),
					'custom_color' 	=> true,
					'description' 	=> esc_html__( 'Use the color picker to choose a image hover border color for this module.', 'et_builder' ),
				),
				'style20_button_border_color' 	=> array(
					'label'        	=> esc_html__( 'Style 20 Button Filter Border Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('filter_style' => 'fstyle20'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a button filter border color for this module.', 'et_builder' ),
				),
				'style21_bg_image_hover_color' 	=> array(
					'label'        	=> esc_html__( 'Style 21 Image hover Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('select_style' => 'style21'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a image hover color for this module.', 'et_builder' ),
				),
				'style22_social_media_bg_color' 	=> array(
					'label'        	=> esc_html__( 'Style 22 Social Media Background Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'toggle_slug'	=> 'jv_team_grid_member_color',
					'show_if'		=> array('select_style' => 'style22'),
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a social media background color for this module.', 'et_builder' ),
				),
				'style23_bg_image_border_color' 	=> array(
					'label'			=> esc_html__( 'Style 23 Image border Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style23'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a background image border color for this module.', 'et_builder' ),
				),
				'style23_bg_image_border_hover_color' 	=> array(
					'label'        	=> esc_html__( 'Style 23 Image border hover Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style23'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a background image border hover color for this module.', 'et_builder' ),
				),
				'style24_bg_image_hover_color' => array(
					'label'			=> esc_html__( 'Style 24 Image hover color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style24'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a background image hover color for this module.', 'et_builder' ),
				),
				'style24_title_color' 	=> array(
					'label'        	=> esc_html__( 'Style 24  Title color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style24'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a title color for this module.', 'et_builder' ),
				),
				'style24_title_hover_color' 	=> array(
					'label'        	=> esc_html__( 'Style 24  Title hover color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style24'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a title hover color for this module.', 'et_builder' ),
				),
				'style25_content_border_color' 	=> array(
					'label'        	=> esc_html__( 'Style 25 Content border color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style25'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a content border color for this module.', 'et_builder' ),
				),
				'style25_image_border_color' => array(
					'label'        	=> esc_html__( 'Style 25 Image border color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'custom_color' 	=> true,
					'show_if'		=> array('select_style' => 'style25'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'description'  	=> esc_html__( 'Use the color picker to choose a image border color for this module.', 'et_builder' ),
				),
				'style25_title_color' 	=> array(
					'label'        	=> esc_html__( 'Style 25 Title color', 'et_builder' ),
					'type'         	=> 'color-alpha',
					'show_if'		=> array('select_style' => 'style25'),					
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a title color for this module.', 'et_builder' ),
				),
				'style25_title_hover_color' => array(
					'label'        	=> esc_html__( 'Style 25 Title hover color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style25'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a title hover color for this module.', 'et_builder' ),
				),
				'style25_read_more_bg_color' 	=> array(
					'label'        	=> esc_html__( 'Style 25 Read More Background color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style25'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a read more background color for this module.', 'et_builder' ),
				),
				'style25_icon_color' 	=> array(
					'label'        	=> esc_html__( 'Style 25 Icon Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style25'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a icon color for this module.', 'et_builder' ),
				),
				'style26_bg_image_hover_color' 	=> array(
					'label'        	=> esc_html__( 'Style 26 Image hover color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style26'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a background image hover color for this module.', 'et_builder' ),
				),
				'style26_image_box_shadow_color' 	=> array(
					'label'        	=> esc_html__( 'Style 26 Image Box shadow color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style26'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a image box shadow color color for this module.', 'et_builder' ),
				),
				'style26_title_color' 	=> array(
					'label'        	=> esc_html__( 'Style 26 title color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style26'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a title color for this module.', 'et_builder' ),
				),
				'style26_title_hover_color' 	=> array(
					'label'        	=> esc_html__( 'Style 26 title hover color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style26'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color'	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a title hover color for this module.', 'et_builder' ),
				),
				'style27_bg_image_hover_color' 	=> array(
					'label'        	=> esc_html__( 'Style 27 Image hover color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style27'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a background image hover color for this module.', 'et_builder' ),
				),
				'style27_position_color' 	=> array(
					'label'        	=> esc_html__( 'Style 27 Position title color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style27'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a position color for this module.', 'et_builder' ),
				),
				'style28_bg_content_color' 	=> array(
					'label'        	=> esc_html__( 'Style 28 Content Background Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style28'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a background content color for this module.', 'et_builder' ),
				),
				'style28_bg_image_hover_color' 	=> array(
					'label'        	=> esc_html__( 'Style 28 Image hover Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style28'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a background image hover color for this module.', 'et_builder' ),
				),
				'style28_display_content_on_hover' 	=> array(
					'label'         => esc_html__( ' Style 28 Display Content On Hover', 'et_builder' ),
 					'show_if'		=> array('select_style' => 'style28'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'type'          => 'yes_no_button',			
					'options'		=> array(
						'on'			=> esc_html__( 'yes', 'et_builder' ),
						'off'			=> esc_html__( 'No', 'et_builder' ),
					),
					'description'     	=> esc_html__( 'This setting will turn on and off the display content on hover.', 'et_builder' ),
				),
				'style28_display_phone' 	=> array(
					'label'			=> esc_html__( 'Style 28 Display Phone Number', 'et_builder' ),
 					'show_if'		=> array('select_style' => 'style28'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'type'			=> 'yes_no_button',			
					'options'		=> array(
						'on'			=> esc_html__( 'yes', 'et_builder' ),
						'off'			=> esc_html__( 'No', 'et_builder' ),
					),
					'description'	=> esc_html__( 'This setting will turn on and off the display phone number.', 'et_builder' ),
				),
				'style28_display_email'	=> array(
					'label'			=> esc_html__( 'Style28 Display Email', 'et_builder' ),
 					'show_if'		=> array('select_style' => 'style28'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'type'			=> 'yes_no_button',			
					'options'		=> array(
						'on'  			=> esc_html__( 'yes', 'et_builder' ),
						'off' 			=> esc_html__( 'No', 'et_builder' ),
					),
					'description'	=> esc_html__( 'This setting will turn on and off the display email.', 'et_builder' ),
				),
				'style28_display_category' 	=> array(
					'label'			=> esc_html__( 'Style28 Display Category', 'et_builder' ),
 					'show_if'		=> array('select_style' => 'style28'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'type'			=> 'yes_no_button',			
					'options'		=> array(
						'on'  			=> esc_html__( 'yes', 'et_builder' ),
						'off' 			=> esc_html__( 'No', 'et_builder' ),
					),
					'description'	=> esc_html__( 'This setting will turn on and off the display category.', 'et_builder' ),
				),
				'style29_bg_content_color' 	=> array(
					'label'        	=> esc_html__( 'Style 29 Content Background Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style29'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a background content color for this module.', 'et_builder' ),
				),
				'style29_postion_title_color' 	=> array(
					'label'        	=> esc_html__( 'Style 29 Position Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style29'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a postion title color for this module.', 'et_builder' ),
				),
				'style30_social_media_bg_color' => array(
					'label'			=> esc_html__( 'Style 30 Social Media Background Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style30'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a social media background color for this module.', 'et_builder' ),
				),
				'style32_image_content_hover_color' 	=> array(
					'label'        	=> esc_html__( 'Style 32 Image content hover color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style32'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a image content hover color for this module.', 'et_builder' ),
				),
				'style32_background_color' 	=> array(
					'label'        	=> esc_html__( 'Style 32 Background Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style32'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a background color for this module.', 'et_builder' ),
				),
				'style33_bg_content_color' 	=> array(
					'label'        	=> esc_html__( 'Style 33 Content Background Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style33'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a background content color for this module.', 'et_builder' ),
				),
				'style34_bg_content_color' 	=> array(
					'label'        	=> esc_html__( 'Style 34 Content Background Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style34'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a background content color for this module.', 'et_builder' ),
				),
				'style34_content_border_color' 	=> array(
					'label'        	=> esc_html__( 'Style 34 Content Border Color', 'et_builder' ),
					'type'        	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style34'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a content border color for this module.', 'et_builder' ),
				),
				'style34_title_color' 	=> array(
					'label'        	=> esc_html__( 'Style 34 Title Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style34'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a title color for this module.', 'et_builder' ),
				),
				'style34_title_hover_color' 	=> array(
					'label'        	=> esc_html__( 'Style 34 Title hover Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style34'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a title hover color for this module.', 'et_builder' ),
				),
				'style35_bg_content_color' 	=> array(
					'label'        	=> esc_html__( 'Style 35 Content Background Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style35'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a background content color for this module.', 'et_builder' ),
				),
				'style35_image_hover_border_color' 	=> array(
					'label'			=> esc_html__( 'Style 35 Image hover border Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style35'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a image hover border color for this module.', 'et_builder' ),
				),
				'style35_bg_border_color' => array(
					'label'       	=> esc_html__( 'Style 35 Background border Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style35'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a background border color for this module.', 'et_builder' ),
				),
				'style35_image_border_color' 	=> array(
					'label'        	=> esc_html__( 'Style 35 Image border Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style35'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a image border color for this module.', 'et_builder' ),
				),
				'style36_bg_image_hover_color' 	=> array(
					'label'        	=> esc_html__( 'Style 36 Image hover Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style36'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a background image hover color for this module.', 'et_builder' ),
				),
				'style37_bg_content_color' 	=> array(
					'label'        	=> esc_html__( 'Style 37 Content Background Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style37'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a background content color for this module.', 'et_builder' ),
				),
				'style37_bg_content_hover_color' 	=> array(
					'label'        	=> esc_html__( 'Style 37 Content Background Hover Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style37'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a background content hover color for this module.', 'et_builder' ),
				),
				'style37_border_color' 	=> array(
					'label'        	=> esc_html__( 'Style 37 Top Border Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style37'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a border color for this module.', 'et_builder' ),
				),
				'style38_bg_content_color' 	=> array(
					'label'        	=> esc_html__( 'Style 38 Content Background Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style38'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a background content color for this module.', 'et_builder' ),
				),
				'style38_title_color' 	=> array(
					'label'        	=> esc_html__( 'Style 38 Title Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style38'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a title color for this module.', 'et_builder' ),
				),
				'style38_bg_color' 	=> array(
					'label'        	=> esc_html__( 'Style 38 Background Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style38'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a background color for this module.', 'et_builder' ),
				),
				'style39_bg_image_hover_color' 	=> array(
					'label'			=> esc_html__( 'Style 39 Image Hover Color', 'et_builder' ),
					'type'        	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style39'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a background image hover color for this module.', 'et_builder' ),
				),
				'style39_image_border_color' 	=> array(
					'label'        	=> esc_html__( 'Style 39 Image Border Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style39'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a image boder color for this module.', 'et_builder' ),
				),
				'style39_image_hover_border_color' 	=> array(
					'label'        	=> esc_html__( 'Style 39 Image Border Hover Color', 'et_builder' ),
					'type'        	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style39'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a image hover border color for this module.', 'et_builder' ),
				),
				'style39_frame_border_color' 	=> array(
					'label'        	=> esc_html__( 'Style 39 Frame Border Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style39'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color'	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a frame border color for this module.', 'et_builder' ),
				),
				'style40_image_box_shadow_color' 	=> array(
					'label'        	=> esc_html__( 'Style 40 Image Box Shadow Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style40'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a image box shadow color for this module.', 'et_builder' ),
				),
				'style40_icon_color' 	=> array(
					'label'        	=> esc_html__( 'Style 40 Icon Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style40'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a icon color for this module.', 'et_builder' ),
				),
				'style40_social_media_bg_color' 	=> array(
					'label'        	=> esc_html__( 'Style 40 Social Media Background Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style40'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a social media background color for this module.', 'et_builder' ),
				),
				'style41_content_bg_color' 	=> array(
					'label'			=> esc_html__( 'Style 41 Content Background Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style41'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a content background color for this module.', 'et_builder' ),
				),
				'style41_content_bg_overlayer_color' 	=> array(
					'label'        	=> esc_html__( 'Style 41 Content Background Overlayer Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style41'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a content background overlayer color for this module.', 'et_builder' ),
				),
				'style41_image_hover_border_color' 	=> array(
					'label'        	=> esc_html__( 'Style 41 Image Hover Border Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style41'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a image hover border color for this module.', 'et_builder' ),
				),
				'style41_image_hover_box_shadow_color' 	=> array(
					'label'        	=> esc_html__( 'Style 41 Image Hover Box Shadows Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style41'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a image hover box shadow color for this module.', 'et_builder' ),
				),
				'style41_social_media_bg_color' 	=> array(
					'label'        	=> esc_html__( 'Style 41 Social Media Background Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style41'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a social media background color for this module.', 'et_builder' ),
				),
				'style41_social_media_bg_hover_color' 	=> array(
					'label'        	=> esc_html__( 'Style 41 Social Media Background Hover Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style41'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description' 	=> esc_html__( 'Use the color picker to choose a social media background hover color for this module.', 'et_builder' ),
				),
				'style42_content_bg_color' 	=> array(
					'label'        	=> esc_html__( 'Style 42 Content Background Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style42'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a content background color for this module.', 'et_builder' ),
				),
				'style42_social_media_bg_color' 	=> array(
					'label'        	=> esc_html__( 'Style 42 Social Media Background Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style42'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a social media background color for this module.', 'et_builder' ),
				),
				'style42_social_media_bg_hover_color' 	=> array(
					'label'        	=> esc_html__( 'Style 42 Social Media Background Hover Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style43'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a social media background hover color for this module.', 'et_builder' ),
				),
				'style43_content_bg_color' 	=> array(
					'label'        	=> esc_html__( 'Style 43 Content Background Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style43'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a content background color for this module.', 'et_builder' ),
				),
				'style43_image_hover_border_color' => array(
					'label'        	=> esc_html__( 'Style 43 Image Hover Border Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style43'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a image hover border color for this module.', 'et_builder' ),
				),
				'style43_social_media_border_color' 	=> array(
					'label'        	=> esc_html__( 'Style 43 Social Media Border Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style43'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a social media border color for this module.', 'et_builder' ),
				),
				'style43_social_media_bg_hover_color' => array(
					'label'        	=> esc_html__( 'Style 43 Social Media Background Hover Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style43'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a social media background hover color for this module.', 'et_builder' ),
				),
				'style44_content_bg_color' 	=> array(
					'label'        	=> esc_html__( 'Style 43 Content Background Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style44'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a content background color for this module.', 'et_builder' ),
				),
				'style44_content_bg_hover_color' => array(
					'label'        	=> esc_html__( 'Style 44 Content Background Hover Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style44'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a content background hover color for this module.', 'et_builder' ),
				),
				'style44_bg_image_hover_color' 	=> array(
					'label'        	=> esc_html__( 'Style 44 Image Background Hover Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style44'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a content background hover color for this module.', 'et_builder' ),
				),
				'style44_social_media_bg_hover_color' => array(
					'label'        	=> esc_html__( 'Style 44 Social Media Background Hover Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style44'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a social media background hover color for this module.', 'et_builder' ),
				),
				'style45_content_bg_color' 	=> array(
					'label'        	=> esc_html__( 'Style 45 Content Background Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style45'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a content background color for this module.', 'et_builder' ),
				),
				'style45_bg_image_hover_color' 	=> array(
					'label'        	=> esc_html__( 'Style 45 Image Background Hover Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style45'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a content background hover color for this module.', 'et_builder' ),
				),
				'style46_bg_content_color' 	=> array(
					'label'        	=> esc_html__( 'Style 46 Content Background Color', 'et_builder' ),
					'type'        	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style46'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a content background color for this module.', 'et_builder' ),
				),
				'style47_image_curve_border_color' 	=> array(
					'label'        	=> esc_html__( 'Style 47 Image Curve Border Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style47'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a image curve border color for this module.', 'et_builder' ),
				),
				'style47_image_curve_border_hover_color' 	=> array(
					'label'        	=> esc_html__( 'Style 47 Image Curve Border Hover Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style47'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a image curve border hover color for this module.', 'et_builder' ),
				),
				'style47_social_media_border_color' => array(
					'label'        	=> esc_html__( 'Style 47 Social Media Background Border Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style47'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a social media border color for this module.', 'et_builder' ),
				),
				'style47_social_media_bg_hover_color' 	=> array(
					'label'        	=> esc_html__( 'Style 47 Social Media Background Hover Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style47'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description' 	=> esc_html__( 'Use the color picker to choose a social media background hover color for this module.', 'et_builder' ),
				),
				'style48_bg_content_color' 	=> array(
					'label'        	=> esc_html__( 'Style 48 Content Background Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style48'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a content background color for this module.', 'et_builder' ),
				),
				'style48_bg_content_hover_color' 	=> array(
					'label'        	=> esc_html__( 'Style 48 Content Background Hover Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style48'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a content background hover color for this module.', 'et_builder' ),
				),
				'style48_image_bg_shadow_gardin1_hover_color' => array(
					'label'        	=> esc_html__( 'Style 48 Image Background Gardian 1 Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style48'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color'	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a image background gardian 1 color for this module.', 'et_builder' ),
				),
				'style48_image_bg_shadow_gardin2_hover_color' 	=> array(
					'label'        	=> esc_html__( 'Style 48 Image Background Gardian 2 Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style48'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a image background gardian 1 color for this module.', 'et_builder' ),
				),
				'style48_social_media_bg_hover_color' 	=> array(
					'label'       	=> esc_html__( 'Style 48 Social Media Background Hover Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style48'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a social media background hover color for this module.', 'et_builder' ),
				),
				'style49_image_layer1_color' 	=> array(
					'label'        	=> esc_html__( 'Style 49 Image Background Layer 1', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style49'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a image background layer 1 for this module.', 'et_builder' ),
				),
				'style49_image_layer2_color' 	=> array(
					'label'        	=> esc_html__( 'Style 49 Image Background Layer 2', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style49'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a image background layer 2 for this module.', 'et_builder' ),
				),
				'style49_image_layer3_color' 	=> array(
					'label'        	=> esc_html__( 'Style 49 Image Background Layer 3', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style49'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a image background layer 2 for this module.', 'et_builder' ),
				),
				'style49_image_layer4_color' 	=> array(
					'label'        	=> esc_html__( 'Style 49 Image Background Layer 4', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style49'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a image background layer 2 for this module.', 'et_builder' ),
				),
				'style49_social_media_bg_color' 	=> array(
					'label'        	=> esc_html__( 'Style 49 Social Media Background Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style49'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a social media background color for this module.', 'et_builder' ),
				),
				'style49_social_media_bg_hover_color' 	=> array(
					'label'        	=> esc_html__( 'Style 49 Social Media Background Hover Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style49'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a social media background hover color for this module.', 'et_builder' ),
				),
				'style50_image_bg_hover_color' 	=> array(
					'label'        	=> esc_html__( 'Style 50 Image Background Hover Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style50'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a image background hover color for this module.', 'et_builder' ),
				),
				'style50_image_bg_hover_border_color' 	=> array(
					'label'        	=> esc_html__( 'Style 50 Image Hover Border Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style50'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a image hover border color for this module.', 'et_builder' ),
				),
				'style50_image_box_shadows_color' 	=> array(
					'label'        	=> esc_html__( 'Style 50 Image Box Shadows Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style50'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a image box shadows color for this module.', 'et_builder' ),
				),
				'style51_image_animation_border_color1' 	=> array(
					'label'        	=> esc_html__( 'Style 51 Image Animation Border Color 1', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style51'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a image animation border color 1 for this module.', 'et_builder' ),
				),
				'style51_image_animation_border_color2' 	=> array(
					'label'        	=> esc_html__( 'Style 51 Image Animation Border Color 2', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style51'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a image animation border color 2 for this module.', 'et_builder' ),
				),
				'style51_social_media_background_color' 	=> array(
					'label'        	=> esc_html__( 'Style 51 Social Media Background Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style51'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a social media background color for this module.', 'et_builder' ),
				),
				'style51_social_media_background_hover_color' 	=> array(
					'label'        	=> esc_html__( 'Style 51 Social Media Background Hover Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style51'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a social media background hover color for this module.', 'et_builder' ),
				),		
				'style52_image_bg_overlay_color' 	=> array(
					'label'        	=> esc_html__( 'Style 52 Image Background Overlayer Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style52'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a image background overlayer color for this module.', 'et_builder' ),
				),		
				'style52_image_border_color' 	=> array(
					'label'        	=> esc_html__( 'Style 52 Image Border Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style52'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a image border color for this module.', 'et_builder' ),
				),		
				'style52_image_border_hover_color' 	=> array(
					'label'        	=> esc_html__( 'Style 52 Image Border Hover Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style52'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a image border hover color for this module.', 'et_builder' ),
				),
				'style52_social_media_border_color' 	=> array(
					'label'        	=> esc_html__( 'Style 52 Social Media Border Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style52'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a social media border color for this module.', 'et_builder' ),
				),
				'style52_social_media_bg_hover_color' 	=> array(
					'label'        	=> esc_html__( 'Style 52 Social Media Background Hover Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style52'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a social media background hover color for this module.', 'et_builder' ),
				),
				'style53_image_overlay_bg_color' 	=> array(
					'label'        	=> esc_html__( 'Style 53 Image Background Overlayer Color', 'et_builder' ),
					'type'        	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style53'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description' 	=> esc_html__( 'Use the color picker to choose a image background overlayer color for this module.', 'et_builder' ),
				),
				'style53_border_color' 	=> array(
					'label'        	=> esc_html__( 'Style 53 Border Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style53'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a border color for this module.', 'et_builder' ),
				),
				'style53_social_media_bg_color' 	=> array(
					'label'        	=> esc_html__( 'Style 53 Social Media Background Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style53'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color'	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a social media background color this module.', 'et_builder' ),
				),
				'style54_bg_image_gardian1_color' 	=> array(
					'label'        	=> esc_html__( 'Style 54 Image  Gardian Color 1', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style54'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a image gardian color 1 for this module.', 'et_builder' ),
					),
				'style54_bg_image_gardian2_color' 	=> array(
					'label'        	=> esc_html__( 'Style 54 Image Gardian Color 2', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style54'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a image gardian color 2 for this module.', 'et_builder' ),
				),
				'style55_image_bg_overlayer_color' 	=> array(
					'label'        	=> esc_html__( 'Style 55 Image Backgroud Overlayer color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style55'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color'	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a image background overlayer color for this module.', 'et_builder' ),
				),
				'style55_border_hover_color' 	=> array(
					'label'        	=> esc_html__( 'Style 55 Image Border Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style55'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a image border color for this module.', 'et_builder' ),
				),
				'style56_content_bg_color' 	=> array(
					'label'        	=> esc_html__( 'Style 56 Content Background Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style56'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a content background color for this module.', 'et_builder' ),
				),
				'style56_social_media_bg_color' 	=> array(
					'label'        	=> esc_html__( 'Style 56 Social Media Background Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style56'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a social media background color for this module.', 'et_builder' ),
				),
				'style57_content_hover_bg_color' 	=> array(
					'label'        	=> esc_html__( 'Style 57 Content Hover Background Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style57'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a content background color for this module.', 'et_builder' ),
				),
				'style57_social_media_hover_bg_color' 	=> array(
					'label'        	=> esc_html__( 'Style 57 Social Media Hover Background Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style57'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a content background color for this module.', 'et_builder' ),
				),
				'style58_overlay_hover_color1' 	=> array(
					'label'        	=> esc_html__( 'Style 58 Overlay Hover Background Color 1', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style58'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a content background color for this module.', 'et_builder' ),
				),
				'style58_overlay_hover_color2' 	=> array(
					'label'        	=> esc_html__( 'Style 58 Overlay Hover Background Color 2', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style58'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a content background color for this module.', 'et_builder' ),
				),
				'style58_social_media_bg_color' 	=> array(
					'label'        	=> esc_html__( 'Style 58 Social Media Background Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style58'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a content background color for this module.', 'et_builder' ),
				),
				'style58_content_title_color' 	=> array(
					'label'        	=> esc_html__( 'Style 58 Content Title Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style58'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a content background color for this module.', 'et_builder' ),
				),
				'style59_content_bg_img_border_hover_color' 	=> array(
					'label'        	=> esc_html__( 'Style 59 Content Hover Background Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style59'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a content background color for this module.', 'et_builder' ),
				),
				'style59_social_media_hover_bg_color' 	=> array(
					'label'        	=> esc_html__( 'Style 59 Social Media Hover Background Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style59'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a content background color for this module.', 'et_builder' ),
				),
				'style60_content_bg_social_media_color' 	=> array(
					'label'        	=> esc_html__( 'Style 60 Content Hover Background Color Social Icon Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style60'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a content background color for this module.', 'et_builder' ),
				),
				'style60_img_border_hover_color' 	=> array(
					'label'        	=> esc_html__( 'Style 60 image Border Social Icon Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
 					'show_if'		=> array('select_style' => 'style60'),
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a content background color for this module.', 'et_builder' ),
				),
				'button_text_color'	=> array(
					'label'			=> esc_html__( 'Button Filter Text Color', 'et_builder' ),
					'type'			=> 'color-alpha',
					'custom_color' 	=> true,
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'description'  	=> esc_html__( 'Use the color picker to choose a button text color for this module.', 'et_builder' ),
					),
				'button_text_hover_color'	=> array(
				    'label'        	=> esc_html__( 'Button Filter Text Hover Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
					'custom_color' 	=> true,
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'description'  	=> esc_html__( 'Use the color picker to choose a button text hover color for this module.', 'et_builder' ),
				),
				'social_link_media_text_color' => array(
					'label'        	=> esc_html__( 'Social Media Link Background Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
					'custom_color' 	=> true,
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'description' 	=> esc_html__( 'Use the color picker to choose a social media text color for this module.', 'et_builder' ),
				),
				'social_link_text_hover_color'	=> array(
				    'label'        	=> esc_html__( 'Social Media Link Hover Background Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
					'custom_color' 	=> true,
					'toggle_slug'	=> 'jv_team_grid_member_color',
					'description'  	=> esc_html__( 'Use the color picker to choose a social media text hover color color for this module.', 'et_builder' ),
				),
			);
			return $fields;
		}
		/***********************************************Shortcode_Callback**************************************************************/
	function render( $attrs, $content = null, $render_slug ) {
	
		$display_popup_onteam      = $this->props['display_popup_onteam'];
		$divi_popupteam_select_style_layout = !empty(get_option('divi_popupteam_select_style_layout')) ? get_option('divi_popupteam_select_style_layout') : 'style1';

		if ( !is_admin()){
			wp_enqueue_style('jv_team_module_members', JVTEAM_PLUGIN_URL .'assets/css/archive_team_members_grid_css/archive_team_member_grid.css', array(), NULL);			
			wp_enqueue_script( 'archive_team_members_filter', JVTEAM_PLUGIN_URL . 'assets/js/archive_team_members_filter.js', array('jquery'), NULL, TRUE );
			wp_enqueue_script('jv_team_member_equalheight', JVTEAM_PLUGIN_URL .'/assets/js/jv_team_member_equalheight.js', array(), NULL);	
			
			if( $display_popup_onteam == 'on') {
			
				$jv_template_path_p =  get_stylesheet_directory() . '/divi-team-members';
				$jv_css_path_p = $jv_template_path_p.'/css/popup';
				$jv_css_url_p =  get_stylesheet_directory_uri().'/divi-team-members/css/popup'; 
				
				$jv_grid_path_p =  $jv_template_path_p.'/content-popup';
			
				if ( file_exists( $jv_css_path_p . '/popup-'.$divi_popupteam_select_style_layout.'.css' ) )
				{
					wp_enqueue_style('jv_team_module_members_popup_'.$divi_popupteam_select_style_layout, $jv_css_url_p . '/popup-'.$divi_popupteam_select_style_layout.'.css', array(), NULL);
				}else{
					wp_enqueue_style('jv_team_module_members_popup_'.$divi_popupteam_select_style_layout, JVTEAM_PLUGIN_URL .'assets/css/popup/popup-'.$divi_popupteam_select_style_layout.'.css', array(), NULL);	
				}
				
				wp_enqueue_style('jv_team_module_members_popup_custom', JVTEAM_PLUGIN_URL .'assets/css/popup/popup-custom.css', array(), NULL);		
				$divi_popupteam_select_style_layout = !empty(get_option('divi_popupteam_select_style_layout')) ? get_option('divi_popupteam_select_style_layout') : 'style1';
				$divi_team_close_background_color = !empty(get_option('divi_team_close_background_color')) ? get_option('divi_team_close_background_color') : '';
				$divi_team_close_color = !empty(get_option('divi_team_close_color')) ? get_option('divi_team_close_color') : ''; 
				$divi_popupteam_title_color = !empty(get_option('divi_popupteam_title_color')) ? get_option('divi_popupteam_title_color') : '';
				$divi_popupteam_position_color = !empty(get_option('divi_popupteam_position_color')) ? get_option('divi_popupteam_position_color') : '';
				$divi_popupteam_content_color = !empty(get_option('divi_popupteam_content_color')) ? get_option('divi_popupteam_content_color') : ''; 
				$divi_popupteam_information_color = !empty(get_option('divi_popupteam_information_color')) ? get_option('divi_popupteam_information_color') : '';
				$divi_popupteam_background_color = !empty(get_option('divi_popupteam_background_color')) ? get_option('divi_popupteam_background_color') : '';
				$divi_popupteam_border_width = !empty(get_option('divi_popupteam_border_width')) ? get_option('divi_popupteam_border_width') : '1'; 
				$divi_popupteam_border_color = !empty(get_option('divi_popupteam_border_color')) ? get_option('divi_popupteam_border_color') : '';
				$divi_popupteam_boxshadow_color = !empty(get_option('divi_popupteam_boxshadow_color')) ? get_option('divi_popupteam_boxshadow_color') : '';
				$divi_popupteam_icon_color = !empty(get_option('divi_popupteam_icon_color')) ? get_option('divi_popupteam_icon_color') : '';
				$divi_popupteam_horizontal_color = !empty(get_option('divi_popupteam_horizontal_color')) ? get_option('divi_popupteam_horizontal_color') : ''; 
				$divi_popupteam_contact_us_title = !empty(get_option('divi_popupteam_contact_us_title')) ? get_option('divi_popupteam_contact_us_title') : 'Drop Me a Line'; 
				
					if( $divi_popupteam_select_style_layout == 'style1'){
						  $popup_style1 = '';
						  if ( $divi_team_close_background_color != '' ){ $popup_style1 .= ".mfp-close-btn-in .jv_popup_style1 .mfp-close,.mfp-wrap .mfp-container .jv_popup_style1  button:hover{background-color: ".$divi_team_close_background_color." !important;}"; }
						  if ( $divi_team_close_color != '' ){ $popup_style1 .= ".mfp-close-btn-in .jv_popup_style1 .mfp-close,.mfp-wrap .mfp-container .jv_popup_style1  button:hover{color: ".$divi_team_close_color." !important;}"; }
						  if ( $divi_popupteam_background_color != '' ){ $popup_style1 .= ".jv_team_popup_style1{background-color: ".$divi_popupteam_background_color." !important;}"; }
						  if ( $divi_popupteam_title_color != '' ){  $popup_style1 .= ".jv_team_popup_style1 h4.jv_team_list_title{color: ".$divi_popupteam_title_color." !important;}"; }
						  if ( $divi_popupteam_position_color != '' ){ $popup_style1 .= ".jv_team_popup_style1 .jv_team_list_position{color: ".$divi_popupteam_position_color."   !important;}"; }
						  if ( $divi_popupteam_content_color != '' ){ $popup_style1 .= ".jv_team_popup_style1 .jv_team_list_content,.jv_team_popup_style1 .website_url,.jv_team_popup_style1 .contect_number,.jv_team_popup_style1 .email_address,.jv_team_popup_style1 .jv_team_list_address1{color: ".$divi_popupteam_content_color."    !important;}"; }
						  if ( $divi_popupteam_border_width != '' || $divi_popupteam_border_color !== ''){ $popup_style1 .= ".jv_team_popup_style1{border: ".$divi_popupteam_border_width."px solid ".$divi_popupteam_border_color." !important;}	"; }
						  if ( $divi_popupteam_icon_color != '' ){ $popup_style1 .= ".jv_team_popup_style1 .jv_team_member_icon_font.et-pb-icon,.jv_team_popup_style1 .jv_team_member_social_font.et-pb-icon{ color: ".$divi_popupteam_icon_color." !important;}"; }
						  if ( $divi_popupteam_information_color != '' ){ $popup_style1 .= ".jv_team_popup_style1 .website_url,.jv_team_popup_style1 .contect_number,.jv_team_popup_style1 .email_address,.jv_team_popup_style1 .jv_team_list_address1{color: ".$divi_popupteam_information_color." !important;}"; }
						  if ( $divi_popupteam_boxshadow_color != '' ){ $popup_style1 .= " .jv_team_popup_style1{ box-shadow: 0 20px 20px ".$divi_popupteam_boxshadow_color."  !important;}"; }
						  
						  wp_add_inline_style( 'jv_team_module_members_popup_custom', $popup_style1 );
					}
					
					if( $divi_popupteam_select_style_layout == 'style2'){
						  $popup_style2 = '';
						  if ( $divi_team_close_background_color != '' ){ $popup_style2 .= ".mfp-close-btn-in .jv_popup_style2 .mfp-close,.mfp-wrap .mfp-container .jv_popup_style2  button:hover{background-color: ".$divi_team_close_background_color." !important;}"; }
						  if ( $divi_team_close_color != '' ){ $popup_style2 .= ".mfp-close-btn-in .jv_popup_style2 .mfp-close,.mfp-wrap .mfp-container .jv_popup_style2  button:hover{color: ".$divi_team_close_color." !important;}"; }
						  if ( $divi_popupteam_background_color != '' ){ $popup_style2 .= ".jv_popup_style2 .et_pb_row{background-color: ".$divi_popupteam_background_color." !important;}"; }
						  if ( $divi_popupteam_title_color != '' ){  $popup_style2 .= ".jv_popup_style2 .jv_team_list_title h2{color: ".$divi_popupteam_title_color." !important;}"; }
						  if ( $divi_popupteam_position_color != '' ){ $popup_style2 .= ".jv_popup_style2 .jv_team_feture_box .jv_team_info h4{color: ".$divi_popupteam_position_color." !important;}"; }
						  if ( $divi_popupteam_content_color != '' ){ $popup_style2 .= ".jv_popup_style2 .jv_team_list_content{color: ".$divi_popupteam_content_color." !important;}"; }
						  if ( $divi_popupteam_icon_color != '' ){ $popup_style2 .= ".jv_popup_style2 .jv_team_member_info.et-pb-icon{ color: ".$divi_popupteam_icon_color."  !important;}.jv_popup_style2 .jv_team_list_title span,.jv_popup_style2 .jv_team_list_social_link{ background-color: ".$divi_popupteam_icon_color." !important;}"; }
						  if ( $divi_popupteam_information_color != '' ){ $popup_style2 .= ".jv_popup_style2 .jv_team_feture_box .jv_team_info span{color: ".$divi_popupteam_information_color." !important;}"; }
						  
						  wp_add_inline_style( 'jv_team_module_members_popup_custom', $popup_style2 );
					}
					
			}
		}
			$select_style   = $this->props['select_style'];
		    $filter_style   = $this->props['filter_style'];
			$posts_number             = $this->props['posts_number'];
			$filter_all_label      = $this->props['filter_all_label'];
			$orderby                  = $this->props['orderby'];
			$category_orderby                  = $this->props['category_orderby'];
			$include_team_categories  = $this->props['include_categories'];
	 		$include_categories_array = explode(',',$include_team_categories); 
			$display_category_filter  = $this->props['display_category_filter'];
			$display_category  		  = $this->props['display_category'];
			$display_social_link      = $this->props['display_social_link'];
			$display_detail_page      = $this->props['display_detail_page'];
			$link_open_in_new_tab      = $this->props['link_open_in_new_tab'];
			$link_read_more_text      = $this->props['link_read_more_text'];
			$display_detail_page_link_type      = $this->props['display_detail_page_link_type'];
			$team_members_display_pagination = $this->props['team_members_display_pagination'];
			$style1_bg_content_color  = $this->props['style1_bg_content_color'];
			$style1_button_active_color  = $this->props['style1_button_active_color'];
			$style1_button_color  = $this->props['style1_button_color'];
			$style2_bg_content_color  = $this->props['style2_bg_content_color'];
			$style2_bg_content_hover_color  = $this->props['style2_bg_content_hover_color'];
			$style2_bg_email_text_color = $this->props['style2_bg_email_text_color'];
			$style2_bg_image_hover_color = $this->props['style2_bg_image_hover_color'];
			$style2_social_media_bg_color = $this->props['style2_social_media_bg_color'];
			$style2_social_media_bg_hover_color = $this->props['style2_social_media_bg_hover_color'];
			$style2_button_active_color = $this->props['style2_button_active_color'];
			$style2_button_animation_border_color = $this->props['style2_button_animation_border_color'];
			$style2_button_hover_color = $this->props['style2_button_hover_color'];
			$style3_bg_content_color  = $this->props['style3_bg_content_color'];
			$style3_bg_image_hover_color  = $this->props['style3_bg_image_hover_color'];
			$style3_bg_read_more_color  = $this->props['style3_bg_read_more_color'];
			$style3_button_border_color  = $this->props['style3_button_border_color'];
			$style3_button_color  = $this->props['style3_button_color'];
			$style3_button_hover_color  = $this->props['style3_button_hover_color'];
			$style4_bg_content_color  = $this->props['style4_bg_content_color'];
			$style4_bg_image_gardian1_color  = $this->props['style4_bg_image_gardian1_color'];
			$style4_bg_image_gardian2_color  = $this->props['style4_bg_image_gardian2_color'];
			$style4_button_border_color  = $this->props['style4_button_border_color'];
			$style4_button_color  = $this->props['style4_button_color'];
			$style5_bg_content_color  = $this->props['style5_bg_content_color'];
			$style5_bg_border_color  = $this->props['style5_bg_border_color'];
			$style5_button_border_active_color  = $this->props['style5_button_border_active_color'];
			$style5_button_border_color  = $this->props['style5_button_border_color'];
			$style6_bg_content_color  = $this->props['style6_bg_content_color'];
			$style6_button_active_color  = $this->props['style6_button_active_color'];
			$style6_button_color  = $this->props['style6_button_color'];
			$style7_bg_content_color  = $this->props['style7_bg_content_color'];
			$style7_bg_overlayer_color  = $this->props['style7_bg_overlayer_color'];
			$style7_button_border_active_color  = $this->props['style7_button_border_active_color'];
			$style7_button_border_color  = $this->props['style7_button_border_color'];
			$style8_button_active_color  = $this->props['style8_button_active_color'];
			$style8_button_border_color  = $this->props['style8_button_border_color'];
			$style9_bg_content_color  = $this->props['style9_bg_content_color'];
			$style9_bg_content_hover_color  = $this->props['style9_bg_content_hover_color'];
			$style9_bg_content_border_color  = $this->props['style9_bg_content_border_color'];
			$style9_button_active_color  = $this->props['style9_button_active_color'];
			$style9_button_color  = $this->props['style9_button_color'];
			$style10_bg_content_color = $this->props['style10_bg_content_color'];
			$style10_bg_border_color = $this->props['style10_bg_border_color'];
			$style10_social_media_bg_color = $this->props['style10_social_media_bg_color'];
			$style10_button_active_border_color  = $this->props['style10_button_active_border_color'];
			$style10_button_color  = $this->props['style10_button_color'];
			$style11_bg_content_color = $this->props['style11_bg_content_color'];
			$style11_bg_content_hover_color = $this->props['style11_bg_content_hover_color'];
			$style11_bg_hover_border_color = $this->props['style11_bg_hover_border_color'];
			$style11_social_media_bg_color = $this->props['style11_social_media_bg_color'];
			$style11_social_media_bg_hover_color = $this->props['style11_social_media_bg_hover_color'];
			$style11_button_active_color  = $this->props['style11_button_active_color'];
			$style11_button_color  = $this->props['style11_button_color'];
			$style12_bg_content_color = $this->props['style12_bg_content_color'];
			$style12_bg_image_hover_color = $this->props['style12_bg_image_hover_color'];
			$style12_border_color = $this->props['style12_border_color'];
			$style12_social_media_bg_color = $this->props['style12_social_media_bg_color'];
			$style12_button_active_border_color  = $this->props['style12_button_active_border_color'];
			$style12_button_border_color  = $this->props['style12_button_border_color'];
			$style13_bg_content_color = $this->props['style13_bg_content_color'];
			$style13_bg_content_hover_color = $this->props['style13_bg_content_hover_color'];
			$style13_bg_image_hover_color = $this->props['style13_bg_image_hover_color'];
			$style13_bg_image_hover_border_color = $this->props['style13_bg_image_hover_border_color'];
			$style13_bg_content_border_color = $this->props['style13_bg_content_border_color'];
			$style13_button_active_color = $this->props['style13_button_active_color'];
			$style13_button_active_hover_color = $this->props['style13_button_active_hover_color'];
			$style13_button_hover_color = $this->props['style13_button_hover_color'];
			$style14_bg_content_color = $this->props['style14_bg_content_color'];
			$style14_social_media_bg_color = $this->props['style14_social_media_bg_color'];
			$style14_button_active_color = $this->props['style14_button_active_color'];
			$style14_button_color = $this->props['style14_button_color'];
			$style15_bg_image_color = $this->props['style15_bg_image_color'];
			$style15_bg_image_over_color = $this->props['style15_bg_image_over_color'];
			$style15_button_active_color = $this->props['style15_button_active_color'];
			$style15_button_active_hover_color = $this->props['style15_button_active_hover_color'];
			$style15_button_hover_color = $this->props['style15_button_hover_color'];
			$style16_bg_content_color = $this->props['style16_bg_content_color'];
			$style16_bg_content_hover_color = $this->props['style16_bg_content_hover_color'];
			$style16_bg_image_border_color = $this->props['style16_bg_image_border_color'];
			$style16_bg_image_hover_color = $this->props['style16_bg_image_hover_color'];
			$style16_button_active_color = $this->props['style16_button_active_color'];
			$style16_button_hover_color = $this->props['style16_button_hover_color'];
			$style16_button_border_color = $this->props['style16_button_border_color'];
			$style17_bg_content_color = $this->props['style17_bg_content_color'];
			$style17_bg_image_hover_color = $this->props['style17_bg_image_hover_color'];
			$style17_border_color = $this->props['style17_border_color'];
			$style17_mobile_content_bg_color = $this->props['style17_mobile_content_bg_color'];
			$style17_mobile_content_bg_hover_color = $this->props['style17_mobile_content_bg_hover_color'];
			$style17_mobile_number_txt_color = $this->props['style17_mobile_number_txt_color'];
			$style17_mobile_number_txt_hover_color = $this->props['style17_mobile_number_txt_hover_color'];
			$style17_button_active_color = $this->props['style17_button_active_color'];
			$style17_button_hover_color = $this->props['style17_button_hover_color'];
			$style17_button_border_color = $this->props['style17_button_border_color'];
			$style18_bg_content_color = $this->props['style18_bg_content_color'];
			$style18_bg_image_hover_color = $this->props['style18_bg_image_hover_color'];
			$style18_button_border_color = $this->props['style18_button_border_color'];
			$style18_button_color = $this->props['style18_button_color'];
			$style19_bg_image_hover_color = $this->props['style19_bg_image_hover_color'];
			$style19_button_gardien1_color = $this->props['style19_button_gardien1_color'];
			$style19_button_gardien2_color = $this->props['style19_button_gardien2_color'];
			$style19_button_gardien3_color = $this->props['style19_button_gardien3_color'];
			$style19_button_gardien4_color = $this->props['style19_button_gardien4_color'];
			$style19_button_active_border_color = $this->props['style19_button_active_border_color'];
			$style20_bg_content_color = $this->props['style20_bg_content_color'];
			$style20_bg_image_border_color = $this->props['style20_bg_image_border_color'];
			$style20_bg_image_hover_border_color = $this->props['style20_bg_image_hover_border_color'];
			$style20_button_border_color = $this->props['style20_button_border_color'];
			$style21_bg_image_hover_color = $this->props['style21_bg_image_hover_color'];
			$style22_social_media_bg_color = $this->props['style22_social_media_bg_color'];
			$style23_bg_image_border_color = $this->props['style23_bg_image_border_color'];
			$style23_bg_image_border_hover_color = $this->props['style23_bg_image_border_hover_color'];
			$style24_bg_image_hover_color = $this->props['style24_bg_image_hover_color'];
			$style24_title_color = $this->props['style24_title_color'];
			$style24_title_hover_color = $this->props['style24_title_hover_color'];
			$style25_content_border_color = $this->props['style25_content_border_color'];
			$style25_image_border_color = $this->props['style25_image_border_color'];
			$style25_title_color = $this->props['style25_title_color'];
			$style25_title_hover_color = $this->props['style25_title_hover_color'];
			$style25_read_more_bg_color = $this->props['style25_read_more_bg_color'];
			$style25_icon_color = $this->props['style25_icon_color'];
			$style26_bg_image_hover_color = $this->props['style26_bg_image_hover_color'];
			$style26_image_box_shadow_color = $this->props['style26_image_box_shadow_color'];
			$style26_title_color = $this->props['style26_title_color'];
			$style26_title_hover_color = $this->props['style26_title_hover_color'];
			$style27_bg_image_hover_color = $this->props['style27_bg_image_hover_color'];
			$style27_position_color = $this->props['style27_position_color'];
			$style28_bg_content_color = $this->props['style28_bg_content_color'];
			$style28_bg_image_hover_color = $this->props['style28_bg_image_hover_color'];
			$style28_display_content_on_hover = $this->props['style28_display_content_on_hover'];
			$style28_display_phone = $this->props['style28_display_phone'];
			$style28_display_email = $this->props['style28_display_email'];
			$style28_display_category = $this->props['style28_display_category'];
			$style29_bg_content_color = $this->props['style29_bg_content_color'];
			$style29_postion_title_color = $this->props['style29_postion_title_color'];
			$style30_social_media_bg_color = $this->props['style30_social_media_bg_color'];
			$style32_image_content_hover_color = $this->props['style32_image_content_hover_color'];
			$style32_background_color = $this->props['style32_background_color'];
			$style33_bg_content_color = $this->props['style33_bg_content_color'];
			$style34_bg_content_color = $this->props['style34_bg_content_color'];
			$style34_content_border_color = $this->props['style34_content_border_color'];
			$style34_title_color = $this->props['style34_title_color'];
			$style34_title_hover_color = $this->props['style34_title_hover_color'];
			$style35_bg_content_color = $this->props['style35_bg_content_color'];
			$style35_image_hover_border_color = $this->props['style35_image_hover_border_color'];
			$style35_bg_border_color = $this->props['style35_bg_border_color'];
			$style35_image_border_color = $this->props['style35_image_border_color'];
			$style36_bg_image_hover_color = $this->props['style36_bg_image_hover_color'];
			$style37_bg_content_color = $this->props['style37_bg_content_color'];
			$style37_bg_content_hover_color = $this->props['style37_bg_content_hover_color'];
			$style37_border_color = $this->props['style37_border_color'];
			$style38_bg_content_color = $this->props['style38_bg_content_color'];
			$style38_title_color = $this->props['style38_title_color'];
			$style38_bg_color = $this->props['style38_bg_color'];
			$style39_bg_image_hover_color = $this->props['style39_bg_image_hover_color'];
			$style39_image_border_color = $this->props['style39_image_border_color'];
			$style39_image_hover_border_color = $this->props['style39_image_hover_border_color'];
			$style39_frame_border_color = $this->props['style39_frame_border_color'];
			$style40_image_box_shadow_color = $this->props['style40_image_box_shadow_color'];
			$style40_icon_color = $this->props['style40_icon_color'];
			$style40_social_media_bg_color = $this->props['style40_social_media_bg_color'];
			$style41_content_bg_color = $this->props['style41_content_bg_color'];
			$style41_content_bg_overlayer_color = $this->props['style41_content_bg_overlayer_color'];
			$style41_image_hover_border_color = $this->props['style41_image_hover_border_color'];
			$style41_image_hover_box_shadow_color = $this->props['style41_image_hover_box_shadow_color'];
			$style41_social_media_bg_color = $this->props['style41_social_media_bg_color'];
			$style41_social_media_bg_hover_color = $this->props['style41_social_media_bg_hover_color'];
			$style42_content_bg_color = $this->props['style42_content_bg_color'];
			$style42_social_media_bg_color = $this->props['style42_social_media_bg_color'];
			$style42_social_media_bg_hover_color = $this->props['style42_social_media_bg_hover_color'];
			$style43_content_bg_color = $this->props['style43_content_bg_color'];
			$style43_image_hover_border_color = $this->props['style43_image_hover_border_color'];
			$style43_social_media_border_color = $this->props['style43_social_media_border_color'];
			$style43_social_media_bg_hover_color = $this->props['style43_social_media_bg_hover_color'];
			$style44_content_bg_color = $this->props['style44_content_bg_color'];
			$style44_content_bg_hover_color = $this->props['style44_content_bg_hover_color'];
			$style44_bg_image_hover_color = $this->props['style44_bg_image_hover_color'];
			$style44_social_media_bg_hover_color = $this->props['style44_social_media_bg_hover_color'];
			$style45_content_bg_color = $this->props['style45_content_bg_color'];
			$style45_bg_image_hover_color = $this->props['style45_bg_image_hover_color'];
			$style46_bg_content_color = $this->props['style46_bg_content_color'];
			$style47_image_curve_border_color = $this->props['style47_image_curve_border_color'];
			$style47_image_curve_border_hover_color = $this->props['style47_image_curve_border_hover_color'];
			$style47_social_media_border_color = $this->props['style47_social_media_border_color'];
			$style47_social_media_bg_hover_color = $this->props['style47_social_media_bg_hover_color'];
			$style48_bg_content_color = $this->props['style48_bg_content_color'];
			$style48_bg_content_hover_color = $this->props['style48_bg_content_hover_color'];
			$style48_image_bg_shadow_gardin1_hover_color = $this->props['style48_image_bg_shadow_gardin1_hover_color'];
			$style48_image_bg_shadow_gardin2_hover_color = $this->props['style48_image_bg_shadow_gardin2_hover_color'];
			$style48_social_media_bg_hover_color = $this->props['style48_social_media_bg_hover_color'];
			$style49_image_layer1_color = $this->props['style49_image_layer1_color'];
			$style49_image_layer2_color = $this->props['style49_image_layer2_color'];
			$style49_image_layer3_color = $this->props['style49_image_layer3_color'];
			$style49_image_layer4_color = $this->props['style49_image_layer4_color'];
			$style49_social_media_bg_color = $this->props['style49_social_media_bg_color'];
			$style49_social_media_bg_hover_color = $this->props['style49_social_media_bg_hover_color'];
			$style50_image_bg_hover_color = $this->props['style50_image_bg_hover_color'];
			$style50_image_bg_hover_border_color = $this->props['style50_image_bg_hover_border_color'];
			$style50_image_box_shadows_color = $this->props['style50_image_box_shadows_color'];
			$style51_image_animation_border_color1 = $this->props['style51_image_animation_border_color1'];
			$style51_image_animation_border_color2 = $this->props['style51_image_animation_border_color2'];	
			$style51_social_media_background_color = $this->props['style51_social_media_background_color'];
			$style51_social_media_background_hover_color = $this->props['style51_social_media_background_hover_color'];
			$style52_image_bg_overlay_color = $this->props['style52_image_bg_overlay_color'];
			$style52_image_border_color = $this->props['style52_image_border_color'];
			$style52_image_border_hover_color = $this->props['style52_image_border_hover_color'];
			$style52_social_media_border_color = $this->props['style52_social_media_border_color'];
			$style52_social_media_bg_hover_color = $this->props['style52_social_media_bg_hover_color'];
			$style53_image_overlay_bg_color = $this->props['style53_image_overlay_bg_color'];
			$style53_border_color = $this->props['style53_border_color'];
			$style53_social_media_bg_color = $this->props['style53_social_media_bg_color'];
			$style54_bg_image_gardian1_color = $this->props['style54_bg_image_gardian1_color'];
			$style54_bg_image_gardian2_color = $this->props['style54_bg_image_gardian2_color'];
			$style55_image_bg_overlayer_color = $this->props['style55_image_bg_overlayer_color'];
			$style55_border_hover_color = $this->props['style55_border_hover_color'];
			$style56_content_bg_color = $this->props['style56_content_bg_color'];
			$style56_social_media_bg_color = $this->props['style56_social_media_bg_color'];
			$style57_content_hover_bg_color = $this->props['style57_content_hover_bg_color'];
			$style57_social_media_hover_bg_color = $this->props['style57_social_media_hover_bg_color'];
			$style58_overlay_hover_color1 = $this->props['style58_overlay_hover_color1'];
			$style58_overlay_hover_color2 = $this->props['style58_overlay_hover_color2'];
			$style58_social_media_bg_color = $this->props['style58_social_media_bg_color'];
			$style58_content_title_color = $this->props['style58_content_title_color'];
			$style59_social_media_hover_bg_color = $this->props['style59_social_media_hover_bg_color'];
			$style59_content_bg_img_border_hover_color = $this->props['style59_content_bg_img_border_hover_color'];
			$style60_content_bg_social_media_color = $this->props['style60_content_bg_social_media_color'];
			$style60_img_border_hover_color = $this->props['style60_img_border_hover_color'];

			$social_link_media_text_color = $this->props['social_link_media_text_color'];
			$social_link_text_hover_color = $this->props['social_link_text_hover_color'];
			$button_text_color = $this->props['button_text_color'];
			$button_text_hover_color = $this->props['button_text_hover_color'];
			$module_id = $this->props['module_id'];
			$module_class = $this->props['module_class'];
			$module_class = ET_Builder_Element::add_module_order_class( $module_class, $render_slug );
			$module_id = ( '' !== $module_id ? sprintf( ' id="%1$s"', esc_attr( $module_id ) ) : '' );
			$module_class = ( '' !== $module_class ? sprintf( ' %1$s', esc_attr( $module_class ) ) : '' );
		/********************************************* Social Link color Comman ***********************************************/
		if( $social_link_media_text_color != '' ){
			ET_Builder_Element::set_style( $render_slug, array(
				'selector'    => '%%order_class%% .jv_team_member_social_font.et-pb-icon',
				'declaration' => sprintf('color: %1$s;',esc_html( $social_link_media_text_color )),
			) );
		}
		if( $social_link_text_hover_color != '' ){
			ET_Builder_Element::set_style( $render_slug, array(
				'selector'    => '%%order_class%% .jv_team_member_social_font.et-pb-icon:hover',
				'declaration' => sprintf('color: %1$s;',esc_html( $social_link_text_hover_color )),
			) );
		}
		/*<-------------------------- Button text color comman ------------------------->*/
		if( $button_text_color != '' ){
			ET_Builder_Element::set_style( $render_slug, array(
				'selector'    => '.et_pb_jv_team_members_category_list li a span',
				'declaration' => sprintf('color: %1$s;',esc_html( $button_text_color )),
			) );
		}
		if( $button_text_hover_color != '' ){
			ET_Builder_Element::set_style( $render_slug, array(
				'selector'    => '.et_pb_jv_team_members_category_list li a:hover span',
				'declaration' => sprintf('color: %1$s;',esc_html( $button_text_hover_color )),
			) );
		}
		/*<------------------------ Button filter ftyle 1 to 20 ----------------------->*/
			if ( $filter_style == 'fstyle1' ){
					if( $style1_button_active_color != '' ){
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle1 li a:hover,.et_pb_jv_team_members_category_list.fstyle1 li a.active',
							'declaration' => sprintf('background-color: %1$s !important;',esc_html( $style1_button_active_color )),
						) );
					}
					if( $style1_button_color != '' ){
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle1 li a',
							'declaration' => sprintf('background-color: %1$s !important;',esc_html( $style1_button_color )),
						) );
					}
			}
			if( $filter_style == 'fstyle2' ) {
				if( $style2_button_active_color != '' ){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle2 li a.active',
						'declaration' => sprintf('background-color: %1$s !important;border: 1px solid %1$s !important;',esc_html( $style2_button_active_color )),
					) );
				}
				if( $style2_button_animation_border_color != '' ) {
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle2 li a:before, .et_pb_jv_team_members_category_list.fstyle2 li a:after',
						'declaration' => sprintf('background: %1$s !important;',esc_html( $style2_button_animation_border_color )),
					) );
				}
				if( $style2_button_hover_color != '' ) {
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle2 li a:hover',
						'declaration' => sprintf('border: 1px solid %1$s !important;background: %1$s !important;',esc_html( $style2_button_hover_color )),
					) );
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle2 li a',
						'declaration' => sprintf('border: 1px solid %1$s !important;',esc_html( $style2_button_hover_color )),
					) );
				} 
			}	
			if( $filter_style == 'fstyle3' ) {
				if( $style3_button_border_color != '' ) {
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle3 li a.active',
						'declaration' => sprintf('border-color: %1$s !important;background-color: #fff  !important;',esc_html( $style3_button_border_color )),
					) );
				}
				if( $style3_button_color != '' ) {
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle3 li a',
						'declaration' => sprintf('border: 1px solid %1$s !important;;background: %1$s !important;',esc_html( $style3_button_color )),
					) );
				}
				if( $style3_button_hover_color != '' ) {
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle3 li a:hover',
						'declaration' => sprintf('background-color: %1$s !important;',esc_html( $style3_button_hover_color )),
					) );
				}
			} 
			if( $filter_style == 'fstyle4' ) {	
				if( $style4_button_border_color != '' ) {
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle4 li a.active, .et_pb_jv_team_members_category_list.fstyle4 li a',
						'declaration' => sprintf('border: 2px solid %1$s !important;',esc_html( $style4_button_border_color )),
					) );
				}	
				ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '.et_pb_jv_team_members_category_list.fstyle4 li a.active:before',
					'declaration' => sprintf('background-color: %1$s !important;','#fff'),
				) );
				if( $style4_button_color != '' ) {
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle4 li a:before',
						'declaration' => sprintf('background: %1$s !important;',esc_html( $style4_button_color )),
					) );
				}
			} 
			if( $filter_style == 'fstyle5' ) {
				if( $style5_button_border_active_color != '' ){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle5 li a.active:after, .et_pb_jv_team_members_category_list.fstyle5 li a.active:before',
						'declaration' => sprintf('background-color: %1$s !important;',esc_html( $style5_button_border_active_color )),
					) );	
				}
				if( $style5_button_border_color != '' ){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle5 li a:before, .et_pb_jv_team_members_category_list.fstyle5 li a:after',
						'declaration' => sprintf('background-color: %1$s !important;',esc_html( $style5_button_border_color )),
					) );
				}
			} 
			if( $filter_style == 'fstyle6' ) {
				if( $style6_button_active_color != '' ){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle6 li a.active',
						'declaration' => sprintf('background: %1$s !important;',esc_html( $style6_button_active_color )),
					) );
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle6 li a.active:after',
						'declaration' => sprintf('border: 2px solid %1$s !important;',esc_html( $style6_button_active_color )),
					) );
				}
				if( $style6_button_color !='' ){
						ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle6 li a',
						'declaration' => sprintf('background-color: %1$s !important;',esc_html( $style6_button_color )),
						) );
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle6 li a:after',
							'declaration' => sprintf('border: 2px solid %1$s !important;',esc_html( $style6_button_color )),
						) );
				}
			} 
			if( $filter_style == 'fstyle7' ) {
				if( $style7_button_border_active_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '.et_pb_jv_team_members_category_list.fstyle7 li a.active',
					'declaration' => sprintf('border-left: 3px solid %1$s !important;border-right: 3px solid %1$s !important;',esc_html( $style7_button_border_active_color )),
					) );
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '.et_pb_jv_team_members_category_list.fstyle7 li a.active:before,.et_pb_jv_team_members_category_list.fstyle7 li a.active:after',
					'declaration' => sprintf('background-color: %1$s !important;',esc_html( $style7_button_border_active_color )),
					) );
				}
				if( $style7_button_border_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '.et_pb_jv_team_members_category_list.fstyle7 li a',
					'declaration' => sprintf('border-left: 3px solid %1$s !important;border-right: 3px solid %1$s !important;',esc_html( $style7_button_border_color )),
					) );
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '.et_pb_jv_team_members_category_list.fstyle7 li a:before,.et_pb_jv_team_members_category_list.fstyle7 li a:after',
					'declaration' => sprintf('background: %1$s !important;',esc_html( $style7_button_border_color )),
					) );
				}
			}  
			if( $filter_style == 'fstyle8' ) {
				if( $style8_button_active_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '.et_pb_jv_team_members_category_list.fstyle8 li a:after',
					'declaration' => sprintf('background:  %1$s !important;',esc_html( $style8_button_active_color )),
					) );
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '.et_pb_jv_team_members_category_list.fstyle8 li a.active',
					'declaration' => sprintf('background:  %1$s !important;border: 2px solid %1$s !important;',esc_html( $style8_button_active_color )),
					) );
				}
				if( $style8_button_border_color !='' ) {
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '.et_pb_jv_team_members_category_list.fstyle8 li a',
					'declaration' => sprintf('border: 2px solid %1$s !important;',esc_html( $style8_button_border_color )),
					) );
				}
			}  
			if( $filter_style == 'fstyle9' ) {
				if( $style9_button_active_color !='' ) {
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle9 li a.active',
						'declaration' => sprintf('background: %1$s !important;',esc_html( $style9_button_active_color )),
					) );
				}
				if( $style9_button_color !='' ) {
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle9 li a',
						'declaration' => sprintf('background: %1$s !important;',esc_html( $style9_button_color )),
					) );
				}
			}  
			if( $filter_style == 'fstyle10' ) {
				if( $style10_button_active_border_color !='') {
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle10 li a.active:before, .et_pb_jv_team_members_category_list.fstyle10 li a.active:after,.et_pb_jv_team_members_category_list.fstyle10 li a.active:hover:before,.et_pb_jv_team_members_category_list.fstyle10 li a.active :hover:after',
						'declaration' => sprintf('border-color:%1$s !important;',esc_html( $style10_button_active_border_color )),
					) );
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle10 li a.active,.et_pb_jv_team_members_category_list.fstyle10 li a.active:hover',
						'declaration' => sprintf('border: 2px solid %1$s !important;',esc_html( $style10_button_active_border_color )),
					) );
				}
				if( $style10_button_color !='') {
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle10 li a:before,.et_pb_jv_team_members_category_list.fstyle10 li a:after,.et_pb_jv_team_members_category_list.fstyle10 li a:hover:before,.et_pb_jv_team_members_category_list.fstyle10 li a :hover:after',
						'declaration' => sprintf('border-color:%1$s !important;',esc_html( $style10_button_color )),
					) );
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle10 li a, .et_pb_jv_team_members_category_list.fstyle10 li a:hover',
						'declaration' => sprintf('border: 2px solid %1$s !important;',esc_html( $style10_button_color )),
					) );
				}
			} 
			if( $filter_style == 'fstyle11' ) {
				if( $style11_button_active_color !='') {
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle11 li a.active',
						'declaration' => sprintf('background: %1$s !important;border: 2px solid %1$s !important;',esc_html( $style11_button_active_color )),
						) );
				} 
				if( $style11_button_color !='') {
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle11 li a:hover,.et_pb_jv_team_members_category_list.fstyle11 li a:before,.et_pb_jv_team_members_category_list.fstyle11 li a:after',
						'declaration' => sprintf('background: %1$s !important;',esc_html( $style11_button_color )),
						) );
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle11 li a',
						'declaration' => sprintf('border: 2px solid %1$s !important;',esc_html( $style11_button_color )),
					) );
				}
			} 
			if( $filter_style == 'fstyle12' ) {
				if( $style12_button_active_border_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle12 li a.active:before,.et_pb_jv_team_members_category_list.fstyle12 li a.active:after,.et_pb_jv_team_members_category_list.fstyle12 li a.active span:before,.et_pb_jv_team_members_category_list.fstyle12 li a.active span:after',
						'declaration' => sprintf('background: %1$s !important;',esc_html( $style12_button_active_border_color )),
						) );
				}
				if( $style12_button_border_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle12 li a:before,.et_pb_jv_team_members_category_list.fstyle12 li a:after,.et_pb_jv_team_members_category_list.fstyle12 li a span:before,.et_pb_jv_team_members_category_list.fstyle12 li a span:after',
						'declaration' => sprintf('background: %1$s !important;',esc_html( $style12_button_border_color )),
					) );
				}
			} 
			if( $filter_style == 'fstyle13' ) {
				if( $style13_button_active_color !='' ) {
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle13 li a.active',
						'declaration' => sprintf('background: %1$s !important;border: 1px solid %1$s !important;',esc_html( $style13_button_active_color )),
					) );
				}
				if( $style13_button_active_hover_color !='' ) {
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle13 li a.active:before, .et_pb_jv_team_members_category_list.fstyle13 li a.active:after, .et_pb_jv_team_members_category_list.fstyle13 li a.active span:before, .et_pb_jv_team_members_category_list.fstyle13 li a.active span:after',
						'declaration' => sprintf('background: %1$s !important;',esc_html( $style13_button_active_hover_color )),
					) );
				}
				if( $style13_button_hover_color !='' ) {
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle13 li a:before, .et_pb_jv_team_members_category_list.fstyle13 li a:after, .et_pb_jv_team_members_category_list.fstyle13 li a span:before, .et_pb_jv_team_members_category_list.fstyle13 li a span:after',
						'declaration' => sprintf('background: %1$s !important;',esc_html( $style13_button_hover_color )),
					) );
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle13 li a',
						'declaration' => sprintf('border: 1px solid %1$s !important;',esc_html( $style13_button_hover_color )),
					) );
				}
			} 
			if( $filter_style == 'fstyle14' ) {
				if( $style14_button_active_color !='' ) {
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle14 li a.active:after',
						'declaration' => sprintf('box-shadow: 0 0 0 2px %1$s !important;',esc_html( $style14_button_active_color )),
					) );
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle14 li a.active',
						'declaration' => sprintf('background: %1$s !important;',esc_html( $style14_button_active_color )),
					) );
				}	
				if( $style14_button_color !='' ) {
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle14 li a',
						'declaration' => sprintf('background: %1$s !important;',esc_html( $style14_button_color )),
					) );
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle14 li a:after',
						'declaration' => sprintf('box-shadow: 0 0 0 2px %1$s !important;',esc_html( $style14_button_color )),
					) );
				}
			} 
			if( $filter_style == 'fstyle15' ) {
				if( $style15_button_active_color !='' ) {
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle15 li a.active',
						'declaration' => sprintf('background: %1$s !important;',esc_html( $style15_button_active_color )),
					) );
				}
				if( $style15_button_active_hover_color !='' ) {
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle15 li a.active:before, .et_pb_jv_team_members_category_list.fstyle15 li a.active:after, .et_pb_jv_team_members_category_list.fstyle15 li a.active span:before, .et_pb_jv_team_members_category_list.fstyle15 li a.active span:after',
							'declaration' => sprintf('background: %1$s !important;',esc_html( $style15_button_active_hover_color )),
					) );
				}
				if( $style15_button_hover_color !='' ) {
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle15 li a',
						'declaration' => sprintf('border: 1px solid %1$s !important;',esc_html( $style15_button_hover_color )),
					) );
					ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle15 li a:before, .et_pb_jv_team_members_category_list.fstyle15 li a:after, .et_pb_jv_team_members_category_list.fstyle15 li a span:before, .et_pb_jv_team_members_category_list.fstyle15 li a span:after',
							'declaration' => sprintf('background:  %1$s !important;',esc_html( $style15_button_hover_color )),
					) );
				}
			} 
			if( $filter_style == 'fstyle16' ) {
				if( $style16_button_active_color !=''){
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '.et_pb_jv_team_members_category_list.fstyle16 li a.active, .et_pb_jv_team_members_category_list.fstyle16 li a.active:before, .et_pb_jv_team_members_category_list.fstyle16 li a.active:after, .et_pb_jv_team_members_category_list.fstyle16 li a.active span:before, .et_pb_jv_team_members_category_list.fstyle16 li a.active span:after',
					'declaration' => sprintf('background: %1$s !important;',esc_html( $style16_button_active_color )),
					) );
				}
				if( $style16_button_hover_color !='') {
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '.et_pb_jv_team_members_category_list.fstyle16 li a:before, .et_pb_jv_team_members_category_list.fstyle16 li a:after, .et_pb_jv_team_members_category_list.fstyle16 li a span:before, .et_pb_jv_team_members_category_list.fstyle16 li a span:after',
					'declaration' => sprintf('background: %1$s !important;',esc_html( $style16_button_hover_color )),
					) );
				}
				if( $style16_button_border_color !='') {
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '.et_pb_jv_team_members_category_list.fstyle16 li a',
					'declaration' => sprintf('border: 1px solid %1$s !important;',esc_html( $style16_button_border_color )),
					) );
				}
			}
			if( $filter_style == 'fstyle17' ) {
				if( $style17_button_active_color !='' ) {
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle17 li a.active',
							'declaration' => sprintf('background: %1$s !important;',esc_html( $style17_button_active_color )),
						) );
				}
				if( $style17_button_hover_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle17 li a:hover',
						'declaration' => sprintf('background: %1$s !important;',esc_html( $style17_button_hover_color )),
					) );
				}
				if( $style17_button_border_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle17 li a',
						'declaration' => sprintf('border: 2px solid %1$s !important;',esc_html( $style17_button_border_color )),
					) );
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '.et_pb_jv_team_members_category_list.fstyle17 li a:before',
						'declaration' => sprintf('border-top: 2px solid %1$s !important;border-left: 2px solid %1$s !important;',esc_html( $style17_button_border_color )),
					) );
					ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '.et_pb_jv_team_members_category_list.fstyle17 li a:after',
							'declaration' => sprintf('border-bottom: 2px solid %1$s !important;border-right: 2px solid %1$s !important;',esc_html( $style17_button_border_color )),
					) );
				}
			} 
			if( $filter_style == 'fstyle18' ) {
				if( $style18_button_border_color !=''){
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '.et_pb_jv_team_members_category_list.fstyle18 li a.active',
					'declaration' => sprintf('border: 2px solid %1$s !important;',esc_html( $style18_button_border_color )),
					) );	
				}
				if( $style18_button_color !=''){
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '.et_pb_jv_team_members_category_list.fstyle18 li a:before, .et_pb_jv_team_members_category_list.fstyle18 li a:after',
					'declaration' => sprintf('background: %1$s !important;',esc_html( $style18_button_color )),
					) );	
				}
			} 
			if( $filter_style == 'fstyle19' ) {
				if(( $style19_button_gardien1_color !='') && ( $style19_button_gardien2_color !='') && ( $style19_button_gardien3_color !='') &&  ( $style19_button_gardien4_color !='') ){
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '.et_pb_jv_team_members_category_list.fstyle19 li a',
					'declaration' => sprintf('background-image: linear-gradient(to top, %1$s 0px, %2$s 10px, %3$s 10px, %4$s 100px) !important;',esc_html( $style19_button_gardien1_color ),esc_html( $style19_button_gardien2_color ),esc_html( $style19_button_gardien3_color ),esc_html( $style19_button_gardien4_color )),
					) );	
				}
				if( $style19_button_active_border_color !=''){
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '.et_pb_jv_team_members_category_list.fstyle19 li a:after',
					'declaration' => sprintf('background: %1$s !important;',esc_html( $style19_button_active_border_color )),
					) );	
				}
			}	
			if( $filter_style == 'fstyle20' ) {
				if( $style20_button_border_color !=''){
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '.et_pb_jv_team_members_category_list.fstyle20 li a, .et_pb_jv_team_members_category_list.fstyle20 li a:before, .et_pb_jv_team_members_category_list.fstyle20 li a:after',
					'declaration' => sprintf('border-color: %1$s !important;',esc_html( $style20_button_border_color )),
					) );	
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '.et_pb_jv_team_members_category_list.fstyle20 li a:hover:after',
					'declaration' => sprintf('border-color: %1$s !important;',esc_html( $style20_button_border_color )),
					) );
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '.et_pb_jv_team_members_category_list.fstyle20 li a:hover:before',
					'declaration' => sprintf('border-color: %1$s !important;',esc_html( $style20_button_border_color )),
					) );
				}
			}
		/*<--------------------------Select Style 1 to 50 ------------------------->*/
			if ( $select_style == 'style1' ){
				if( $style1_bg_content_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style1 .et_pb_jv_team .et_pb_jv_team_description',
						'declaration' => sprintf('background-color: %1$s;',esc_html( $style1_bg_content_color )),
					) );
				}
			} 
			if( $select_style == 'style2' ) {
				if( $style2_bg_content_color != '' ) {
					ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '%%order_class%%.et_pb_jv_team_members_style2 .et_pb_jv_team .et_pb_jv_team_description',
							'declaration' => sprintf('background: %1$s;',esc_html( $style2_bg_content_color )),
					) );
				}
				if( $style2_bg_content_hover_color != '' ) {
					ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '%%order_class%%.et_pb_jv_team_members_style2 .et_pb_jv_team .et_pb_jv_team_description_hover',
							'declaration' => sprintf('background: %1$s;',esc_html( $style2_bg_content_hover_color )),
					) );
				}
				if( $style2_bg_email_text_color != '' ) {
					ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '%%order_class%%.et_pb_jv_team_members_style2 .et_pb_jv_team small.et_pb_jv_team_email',
							'declaration' => sprintf('color: %1$s;',esc_html( $style2_bg_email_text_color )),
					) );
				}
				if( $style2_bg_image_hover_color != '' ){
					ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '%%order_class%%.et_pb_jv_team_members_style2 .et_pb_jv_team .et_pb_jv_team_image:before, %%order_class%%.et_pb_jv_team_members_style2 .et_pb_jv_team .et_pb_jv_team_description_hover',
							'declaration' => sprintf('background: %1$s;border-bottom: 2px solid %1$s',esc_html( $style2_bg_image_hover_color )),
					) );
				}	
				if( $style2_social_media_bg_color != '' ){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style2 .et_pb_jv_team .et_pb_jv_team_social_link li a',
						'declaration' => sprintf('background-color: %1$s;',esc_html( $style2_social_media_bg_color )),
					) );
				}
				if( $style2_social_media_bg_hover_color != '' ){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style2 .et_pb_jv_team .et_pb_jv_team_social_link li a:hover',
						'declaration' => sprintf('background-color: %1$s;',esc_html( $style2_social_media_bg_hover_color )),
					) );
				}
			} 
			if( $select_style == 'style3' ){
				if( $style3_bg_content_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style3 .et_pb_jv_team_description',
						'declaration' => sprintf('background-color: %1$s;',esc_html( $style3_bg_content_color )),
					) );
				}		
				if ( $style3_bg_image_hover_color != '' ) {
				    ET_Builder_Element::set_style( $render_slug, array(
					   'selector'    => '%%order_class%%.et_pb_jv_team_members_style3 .et_pb_jv_team .et_pb_jv_team_image:after',
					   'declaration' => sprintf('box-shadow:0 0 0 900px %1$s !important;',esc_html( $style3_bg_image_hover_color )),
				    ) );
				}
				if( $style3_bg_read_more_color != '' ){
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%%.et_pb_jv_team_members_style3 .et_pb_jv_team .et_pb_jv_team_image:after',
					'declaration' => sprintf('background-color: %1$s;',esc_html( $style3_bg_read_more_color )),
					) );
				}
			} 
			if( $select_style == 'style4' ){
				if( $style4_bg_content_color != '' ){
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '%%order_class%%.et_pb_jv_team_members_style4 .et_pb_jv_team .et_pb_jv_team_description',
							'declaration' => sprintf('background-color: %1$s;',esc_html( $style4_bg_content_color )),
						) );
				}	
				if( $style4_bg_image_gardian1_color != '' && $style4_bg_image_gardian2_color!= ''){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style4 .et_pb_jv_team:after',
						'declaration' => sprintf('background: linear-gradient(135deg, %1$s, %2$s);',esc_html( $style4_bg_image_gardian1_color ),esc_html( $style4_bg_image_gardian2_color )),
					) );
				}
			} 
			if( $select_style == 'style5' ){
				if( $style5_bg_content_color !='' ) {
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%%.et_pb_jv_team_members_style5 .et_pb_jv_team .et_pb_jv_team_description:after, %%order_class%%.et_pb_jv_team_members_style5 .et_pb_jv_team .et_pb_jv_team_description:before, %%order_class%%.et_pb_jv_team_members_style5 .et_pb_jv_team .et_pb_jv_team_description',
					'declaration' => sprintf('background-color: %1$s;',esc_html( $style5_bg_content_color )),
					) );
				}	
				if( $style5_bg_border_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style5 .et_pb_jv_team .et_pb_jv_team_title:before, %%order_class%%.et_pb_jv_team_members_style5 .et_pb_jv_team .et_pb_jv_team_title:after',
						'declaration' => sprintf('background-color: %1$s;',esc_html( $style5_bg_border_color )),
						) );	
				}
			} 
			if( $select_style == 'style6' ){
				if( $style6_bg_content_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%%.et_pb_jv_team_members_style6 .et_pb_jv_team .et_pb_jv_team_description',
					'declaration' => sprintf('background-color: %1$s;',esc_html( $style6_bg_content_color )),
					) );
				}	
			} 
			if( $select_style == 'style7' ){
				if( $style7_bg_content_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%%.et_pb_jv_team_members_style7 .et_pb_jv_team:hover .et_pb_jv_team_social_link, %%order_class%%.et_pb_jv_team_members_style7 .et_pb_jv_team .et_pb_jv_team_description',
					'declaration' => sprintf('background-color: %1$s;',esc_html( $style7_bg_content_color )),
					) );
				}	
				if( $style7_bg_overlayer_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%%.et_pb_jv_team_members_style7 .et_pb_jv_team .layer:after',
					'declaration' => sprintf('background-color: %1$s;',esc_html( $style7_bg_overlayer_color )),
					) );
				}
			} 
			if( $select_style == 'style9' ){
				if($style9_bg_content_border_color !='') {
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%%.et_pb_jv_team_members_style9 .et_pb_jv_team .et_pb_jv_team_description',
					'declaration' => sprintf('background: %1$s;border-left: 5px solid %1$s;',esc_html( $style9_bg_content_color ),esc_html( $style9_bg_content_border_color )),
					) );
				}
				if($style9_bg_content_hover_color !='') {
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style9 .et_pb_jv_team:hover .et_pb_jv_team_description',
						'declaration' => sprintf('background: %1$s;',esc_html( $style9_bg_content_hover_color )),
					) );	
				}
			}	
			if( $select_style == 'style10' ){
					if( $style10_bg_content_color !='' ) {
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '%%order_class%%.et_pb_jv_team_members_style10 .et_pb_jv_team .et_pb_jv_team_content',
							'declaration' => sprintf('background-color: %1$s;',esc_html( $style10_bg_content_color )),
						) );
					} 
					if( $style10_bg_border_color !='') {			
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '%%order_class%%.et_pb_jv_team_members_style10 .et_pb_jv_team',
							'declaration' => sprintf('border-left: 8px solid %1$s;border-bottom: 8px solid %1$s;',esc_html( $style10_bg_border_color )),
						) );
					}	
					if( $style10_social_media_bg_color !='' ){
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '%%order_class%%.et_pb_jv_team_members_style10 .et_pb_jv_team .et_pb_jv_team_social_link',
							'declaration' => sprintf('background-color: %1$s;',esc_html( $style10_social_media_bg_color )),
						) );
					}
			}	
			if( $select_style == 'style11' ){
					if( $style11_bg_content_color !='') {
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '%%order_class%%.et_pb_jv_team_members_style11 .et_pb_jv_team .et_pb_jv_team_description:before, %%order_class%%.et_pb_jv_team_members_style11 .et_pb_jv_team .et_pb_jv_team_description',
							'declaration' => sprintf('background-color: %1$s;',esc_html( $style11_bg_content_color )),
							) );
					}
					if( $style11_bg_content_hover_color !='') {
						ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '%%order_class%%.et_pb_jv_team_members_style11 .et_pb_jv_team:hover .et_pb_jv_team_description, %%order_class%%.et_pb_jv_team_members_style11 .et_pb_jv_team:hover .et_pb_jv_team_description:before',
							'declaration' => sprintf('background-color: %1$s;',esc_html( $style11_bg_content_hover_color )),
						) );
					}
					if( $style11_bg_hover_border_color !='' ){
							ET_Builder_Element::set_style( $render_slug, array(
								'selector'    => '%%order_class%%.et_pb_jv_team_members_style11 .et_pb_jv_team:hover .et_pb_jv_team_image:before, %%order_class%%.et_pb_jv_team_members_style11 .et_pb_jv_team:hover .et_pb_jv_team_image:after',
								'declaration' => sprintf('border-color: %1$s;',esc_html( $style11_bg_hover_border_color )),
								) );
					}
					if( $style11_social_media_bg_color !='' ){
						ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style11 .et_pb_jv_team .et_pb_jv_team_social_link li:hover a:before',
						'declaration' => sprintf('background-color: %1$s;',esc_html( $style11_social_media_bg_color )),
						) );
					}
					if( $style11_social_media_bg_hover_color !='' ){
						ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style11 .et_pb_jv_team .et_pb_jv_team_social_link li a:before',
						'declaration' => sprintf('background-color: %1$s;',esc_html( $style11_social_media_bg_hover_color )),
						) );
					}
			} 
			if( $select_style == 'style12' ){
				if( $style12_bg_content_color !='' ) {
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%%.et_pb_jv_team_members_style12 .et_pb_jv_team .et_pb_jv_team_description',
					'declaration' => sprintf('background-color: %1$s;',esc_html( $style12_bg_content_color )),
					) );	
				}
				if( $style12_bg_image_hover_color !='' ) {
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%%.et_pb_jv_team_members_style12 .et_pb_jv_team .et_pb_jv_team_image:after',
					'declaration' => sprintf('background-color: %1$s;',esc_html( $style12_bg_image_hover_color )),
					) );	
				}
				if( $style12_border_color !='' ) {
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style12 .et_pb_jv_team .et_pb_position:before, %%order_class%%.et_pb_jv_team_members_style12 .et_pb_jv_team .et_pb_jv_team_title:after, %%order_class%%.et_pb_jv_team_members_style12 .et_pb_jv_team .et_pb_jv_team_title::before',
						'declaration' => sprintf('background-color: %1$s;',esc_html( $style12_border_color )),
					) );
				}
				if( $style12_social_media_bg_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style12 .et_pb_jv_team .et_pb_jv_team_social_link',
						'declaration' => sprintf('background-color: %1$s;',esc_html( $style12_social_media_bg_color )),
						) );
				}
			} 
			if( $select_style == 'style13' ){
				if( $style13_bg_content_color !='') {
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%%.et_pb_jv_team_members_style13 .et_pb_jv_team .et_pb_jv_team_description',
					'declaration' => sprintf('background-color: %1$s;',esc_html( $style13_bg_content_color )),
					) );
				}
				if( $style13_bg_content_hover_color !='') {
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%%.et_pb_jv_team_members_style13 .et_pb_jv_team .et_pb_jv_team_description:before',
					'declaration' => sprintf('background-color: %1$s;',esc_html( $style13_bg_content_hover_color )),
					) );
				}
				if( $style13_bg_image_hover_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%%.et_pb_jv_team_members_style13 .et_pb_jv_team .et_pb_jv_team_image:before',
					'declaration' => sprintf('background-color: %1$s;',esc_html( $style13_bg_image_hover_color )),
					) );
				}
				if( $style13_bg_image_hover_border_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%%.et_pb_jv_team_members_style13 .et_pb_jv_team .et_pb_jv_team_image:after',
					'declaration' => sprintf('border-color: %1$s;',esc_html( $style13_bg_image_hover_border_color )),
					) );
				}
				if( $style13_bg_content_border_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style13 .et_pb_jv_team .et_pb_jv_team_description',
						'declaration' => sprintf('border-top: 2px solid %1$s;',esc_html( $style13_bg_content_border_color )),
					) );
				}
			} 
			if( $select_style == 'style14' ) {
				if( $style14_bg_content_color !='' ) {
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style14 .et_pb_jv_team .jv_team_over_layer',
						'declaration' => sprintf('background-color: %1$s;',esc_html( $style14_bg_content_color )),
					) );
				}		
				if( $style14_social_media_bg_color !=''){	
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style14 .et_pb_jv_team .et_pb_jv_team_social_link',
						'declaration' => sprintf('background-color: %1$s;',esc_html( $style14_social_media_bg_color )),
					) );
				}
			}	
			if( $select_style == 'style15' ){
					if( $style15_bg_image_color !='' ){
						ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style15 .et_pb_jv_team:hover .et_pb_jv_team_image:before',
						'declaration' => sprintf('box-shadow: 0 0 0 400px %1$s inset;',esc_html( $style15_bg_image_color )),
						) );
					}
					if( $style15_bg_image_over_color !=''){
						ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style15 .et_pb_jv_team .et_pb_jv_team_image:before',
						'declaration' => sprintf('box-shadow: 0 0 0 0px %1$s inset;',esc_html( $style15_bg_image_over_color )),
						) );
					}
			} 
			if( $select_style == 'style16' ) {
				if( $style16_bg_content_color !=''){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style16 .et_pb_jv_team .et_pb_jv_team_description',
						'declaration' => sprintf('background-color: %1$s;',esc_html( $style16_bg_content_color )),
					) );	
				}
				if( $style16_bg_content_hover_color !=''){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style16 .et_pb_jv_team .et_pb_jv_team_description:before',
						'declaration' => sprintf('background-color: %1$s;',esc_html( $style16_bg_content_hover_color )),
					) );	
				}
				if( $style16_bg_image_border_color !=''){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style16 .et_pb_jv_team .jv_team:before',
						'declaration' => sprintf('background-color: %1$s;',esc_html( $style16_bg_image_border_color )),
						) );	
				}
				if( $style16_bg_image_hover_color !=''){
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%%.et_pb_jv_team_members_style16 .et_pb_jv_team .et_pb_jv_team_image:after',
					'declaration' => sprintf('background-color: %1$s;',esc_html( $style16_bg_image_hover_color )),
					) );
				}
			} 
			if( $select_style == 'style17' ){
				if( $style17_bg_content_color !='') {
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%%.et_pb_jv_team_members_style17 .et_pb_jv_team .et_pb_jv_team_description',
					'declaration' => sprintf('background-color: %1$s;',esc_html( $style17_bg_content_color )),
					) );		
				}	
				if( $style17_bg_image_hover_color !='') {
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style17 .et_pb_jv_team .jv_team_over_layer',
						'declaration' => sprintf('background-color: %1$s;',esc_html( $style17_bg_image_hover_color )),
					) );
				}
				if( $style17_border_color !=''){
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%%.et_pb_jv_team_members_style17 .et_pb_jv_team .jv_team_over_layer:before',
					'declaration' => sprintf('border-top: 5px solid %1$s;border-left: 5px solid %1$s;',esc_html( $style17_border_color )),
					) );
				}
				if( $style17_mobile_content_bg_color !=''){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style17 .et_pb_jv_team .read:after, %%order_class%%.et_pb_jv_team_members_style17 .et_pb_jv_team .read, %%order_class%%.et_pb_jv_team_members_style17 .et_pb_jv_team .et_pb_position:after',
						'declaration' => sprintf('background-color: %1$s;',esc_html( $style17_mobile_content_bg_color )),
					) );
				}
				if( $style17_mobile_content_bg_hover_color !=''){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style17 .et_pb_jv_team .read:hover:after, %%order_class%%.et_pb_jv_team_members_style17 .et_pb_jv_team .read:hover',
						'declaration' => sprintf('background-color: %1$s;',esc_html( $style17_mobile_content_bg_hover_color )),
					) );
				}
				if( $style17_mobile_number_txt_color !=''){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style17 .et_pb_jv_team .read',
						'declaration' => sprintf('color: %1$s;',esc_html( $style17_mobile_number_txt_color )),
					) );
				}
				if( $style17_mobile_number_txt_hover_color !=''){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style17 .et_pb_jv_team .read:hover',
						'declaration' => sprintf('color: %1$s;',esc_html( $style17_mobile_number_txt_hover_color )),
					) );
				}
			} 
			if( $select_style == 'style18' ) {
				if( $style18_bg_content_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%%.et_pb_jv_team_members_style18 .et_pb_jv_team .jv_team_prof',
					'declaration' => sprintf('background-color: %1$s;',esc_html( $style18_bg_content_color )),
					) );
				}
				if( $style18_bg_image_hover_color !=''){
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%%.et_pb_jv_team_members_style18 .et_pb_jv_team .et_pb_jv_team_description',
					'declaration' => sprintf('background-color: %1$s;',esc_html( $style18_bg_image_hover_color )),
					) );
				}
			}
			if( $select_style == 'style19' ) {
				if( $style19_bg_image_hover_color == 'style19' ){
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%%.et_pb_jv_team_members_style19 .et_pb_jv_team:hover:before',
					'declaration' => sprintf('background-color: %1$s;',esc_html( $style19_bg_image_hover_color )),
					) );	
				}
			}	
			if( $select_style == 'style20' ){
				if( $style20_bg_content_color !=''){
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%%.et_pb_jv_team_members_style20 .et_pb_jv_team',
					'declaration' => sprintf('background: %1$s;',esc_html( $style20_bg_content_color )),
					) );	
				}
				if( $style20_bg_image_border_color !=''){
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%%.et_pb_jv_team_members_style20 .et_pb_jv_team .et_pb_jv_team_image',
					'declaration' => sprintf('border: 3px solid %1$s;',esc_html( $style20_bg_image_border_color )),
					) );
				}
				if( $style20_bg_image_hover_border_color !=''){
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%%.et_pb_jv_team_members_style20 .et_pb_jv_team:hover .et_pb_jv_team_image',
					'declaration' => sprintf('border: 3px solid %1$s;',esc_html( $style20_bg_image_hover_border_color )),
					) );
				}
		    } 
			if( $select_style == 'style21' ){
				if( $style21_bg_image_hover_color !=''){
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%%.et_pb_jv_team_members_style21 .et_pb_jv_team .et_pb_jv_team_description',
					'declaration' => sprintf('background-color: %1$s;',esc_html( $style21_bg_image_hover_color )),
					) );	
				}
			} 
			if( $select_style == 'style22' ){
				if( $style22_social_media_bg_color !=''){
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%%.et_pb_jv_team_members_style22 .et_pb_jv_team .et_pb_jv_team_social_link li a',
					'declaration' => sprintf('background-color: %1$s;',esc_html( $style22_social_media_bg_color )),
					) );
				}
			} 
			if( $select_style == 'style23' ){
				if( $style23_bg_image_border_color !=''){
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%%.et_pb_jv_team_members_style23 .et_pb_jv_team img',
					'declaration' => sprintf('border-color: %1$s;',esc_html( $style23_bg_image_border_color )),
					) );	
				}
				if( $style23_bg_image_border_hover_color !=''){
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%%.et_pb_jv_team_members_style23 .et_pb_jv_team:hover img, %%order_class%%.et_pb_jv_team_members_style23 .et_pb_jv_team:hover .et_pb_jv_team_social_link:after',
					'declaration' => sprintf('border-color: %1$s;',esc_html( $style23_bg_image_border_hover_color )),
					) );
				}
			} 
			if( $select_style == 'style24' ){
				if( $style24_bg_image_hover_color !=''){
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%%.et_pb_jv_team_members_style24 .et_pb_jv_team .et_pb_jv_team_description',
					'declaration' => sprintf('background-color: %1$s;',esc_html( $style24_bg_image_hover_color )),
					) );	
				}
				if( $style24_title_color !=''){
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%%.et_pb_jv_team_members_style24 .et_pb_jv_team .et_pb_jv_team_title a',
					'declaration' => sprintf('color: %1$s;',esc_html( $style24_title_color )),
					) );
				}
				if( $style24_title_hover_color !=''){
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%%.et_pb_jv_team_members_style24 .et_pb_jv_team .et_pb_jv_team_title a:hover',
					'declaration' => sprintf('color: %1$s;',esc_html( $style24_title_hover_color )),
					) );
				}
		    } 
			if( $select_style == 'style25' ){
				if( $style25_content_border_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style25 .et_pb_jv_team',
						'declaration' => sprintf('border:1px solid %1$s;',esc_html( $style25_content_border_color )),
					) );
				}	
				if( $style25_image_border_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style25 .et_pb_jv_team img',
						'declaration' => sprintf('border-color: %1$s;',esc_html( $style25_image_border_color )),
						) );
				}
				if( $style25_title_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%%.et_pb_jv_team_members_style25 .et_pb_jv_team .et_pb_jv_team_title a',
					'declaration' => sprintf('color: %1$s;',esc_html( $style25_title_color )),
					) );
				}
				if( $style25_title_hover_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%%.et_pb_jv_team_members_style25 .et_pb_jv_team:hover .et_pb_jv_team_title a',
					'declaration' => sprintf('color: %1$s;',esc_html( $style25_title_hover_color )),
					) );
				}
				if( $style25_read_more_bg_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%%.et_pb_jv_team_members_style25 .et_pb_jv_team:hover .read',
					'declaration' => sprintf('background: %1$s;',esc_html( $style25_read_more_bg_color )),
					) );
	            }
				if( $style25_icon_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%% .jv_team_member_read_more_icon.et-pb-icon',
						'declaration' => sprintf('color: %1$s;',esc_html( $style25_icon_color )),
					) );
				}
			} 
		if( $select_style == 'style26' ){
				if( $style26_bg_image_hover_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%%.et_pb_jv_team_members_style26 .et_pb_jv_team .et_pb_jv_team_description',
					'declaration' => sprintf('background-color: %1$s;',esc_html( $style26_bg_image_hover_color )),
					) );
				}
				if( $style26_image_box_shadow_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%%.et_pb_jv_team_members_style26 .et_pb_jv_team',
					'declaration' => sprintf('box-shadow: 0 0 5px %1$s;',esc_html( $style26_image_box_shadow_color )),
					) );
				}
				if( $style26_title_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%%.et_pb_jv_team_members_style26 .et_pb_jv_team .et_pb_jv_team_title a',
					'declaration' => sprintf('color: %1$s;',esc_html( $style26_title_color )),
					) );
				}
				if( $style26_title_hover_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%%.et_pb_jv_team_members_style26 .et_pb_jv_team .et_pb_jv_team_title a:hover',
					'declaration' => sprintf('color: %1$s;',esc_html( $style26_title_hover_color )),
					) );
				}
		} 
		if( $select_style == 'style27' ){
				if( $style27_bg_image_hover_color !=''){
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%%.et_pb_jv_team_members_style27 .et_pb_jv_team .et_pb_jv_team_description',
					'declaration' => sprintf('background-color: %1$s;',esc_html( $style27_bg_image_hover_color )),
					) );
				}
				if( $style27_position_color !=''){
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%%.et_pb_jv_team_members_style27 .et_pb_jv_team .et_pb_jv_team_title small',
					'declaration' => sprintf('color: %1$s;',esc_html( $style27_position_color )),
					) );
				}
		} 
		if( $select_style == 'style28' ){
				if( $style28_bg_content_color !=''){
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%%.et_pb_jv_team_members_style28 .et_pb_jv_team .et_pb_jv_team_description',
					'declaration' => sprintf('background-color: %1$s;',esc_html( $style28_bg_content_color )),
					) );	
				}
				if( $style28_bg_image_hover_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%%.et_pb_jv_team_members_style28 .et_pb_jv_team:hover .et_pb_jv_team_image:before, %%order_class%%.et_pb_jv_team_members_style28 .et_pb_jv_team .et_pb_jv_team_title:after',
					'declaration' => sprintf('background: %1$s;border-bottom: 2px solid  %1$s',esc_html( $style28_bg_image_hover_color )),
					) );
				}	
		} 
		if( $select_style == 'style29' ){
				if( $style29_bg_content_color !=''){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style29 .et_pb_jv_team .et_pb_jv_team_description',
						'declaration' => sprintf('background-color: %1$s;',esc_html( $style29_bg_content_color )),
						) );	
				}
				if( $style29_postion_title_color !=''){
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%%.et_pb_jv_team_members_style29 .et_pb_jv_team .et_pb_jv_team_title small',
					'declaration' => sprintf('color: %1$s;',esc_html( $style29_postion_title_color )),
					) );	
				}
		} 
		if( $select_style == 'style30' ){
			if( $style30_social_media_bg_color !=''){	
				ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%%.et_pb_jv_team_members_style30 .et_pb_jv_team .social_media_team',
					'declaration' => sprintf('background: %1$s;',esc_html( $style30_social_media_bg_color )),
				) );
			}
		} 
		if( $select_style == 'style32' ){
				if( $style32_image_content_hover_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%%.et_pb_jv_team_members_style32 .et_pb_jv_team .et_pb_jv_team_description',
					'declaration' => sprintf('background-color: %1$s;',esc_html( $style32_image_content_hover_color )),
					) );
				}	
				if( $style32_background_color !='' ){		
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%%.et_pb_jv_team_members_style32 .et_pb_jv_team .et_pb_jv_team_social_link',
					'declaration' => sprintf('background-color: %1$s;',esc_html( $style32_background_color )),
					) );
				}
		} 
		if( $select_style == 'style33' ) {
			if( $style33_bg_content_color !='' ){
				ET_Builder_Element::set_style( $render_slug, array(
				'selector'    => '%%order_class%%.et_pb_jv_team_members_style33 .et_pb_jv_team .et_pb_jv_team_description',
				'declaration' => sprintf('background-color: %1$s;',esc_html( $style33_bg_content_color )),
				) );	
		    }	
	    } 
		if( $select_style == 'style34' ){
				if( $style34_bg_content_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%%.et_pb_jv_team_members_style34 .et_pb_jv_team .et_pb_jv_team_description',
					'declaration' => sprintf('background-color: %1$s;',esc_html( $style34_bg_content_color )),
					) );
				}		
				if( $style34_content_border_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%%.et_pb_jv_team_members_style34 .et_pb_jv_team .et_pb_jv_team_description:before',
					'declaration' => sprintf('border-bottom: 30px solid %1$s;',esc_html( $style34_content_border_color )),
					) ); 
				}
				if( $style34_title_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%%.et_pb_jv_team_members_style34 .et_pb_jv_team .et_pb_jv_team_title a',
					'declaration' => sprintf('color: %1$s;',esc_html( $style34_title_color )),
					) ); 
				}
				if( $style34_title_hover_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style34 .et_pb_jv_team .et_pb_jv_team_title a:hover',
						'declaration' => sprintf('color: %1$s;',esc_html( $style34_title_hover_color )),
					) ); 
				}
		} 
		if( $select_style == 'style35' ){
				if( $style35_bg_content_color !=''){
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%%.et_pb_jv_team_members_style35 .et_pb_jv_team:hover',
					'declaration' => sprintf('background-color: %1$s;',esc_html( $style35_bg_content_color )),
					) );	
				}
				if( $style35_image_hover_border_color !=''){
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%%.et_pb_jv_team_members_style35 .et_pb_jv_team:hover .et_pb_jv_team_image img',
					'declaration' => sprintf('border-color: %1$s;',esc_html( $style35_image_hover_border_color )),
					) );
				}
				if( $style35_bg_border_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%%.et_pb_jv_team_members_style35 .et_pb_jv_team',
					'declaration' => sprintf('border-color: %1$s;',esc_html( $style35_bg_border_color )),
					) );
				}
				if( $style35_image_border_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%%.et_pb_jv_team_members_style35 .et_pb_jv_team_image img',
					'declaration' => sprintf('border-color: %1$s;',esc_html( $style35_image_border_color )),
					) );
				}
		} 
		if( $select_style == 'style36' ){
				if( $style36_bg_image_hover_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style36 .jv_team_social_media_team',
						'declaration' => sprintf('background-color: %1$s;',esc_html( $style36_bg_image_hover_color )),
					) );
				}	
		} 
		if ( $select_style == 'style37' ) {
			if( $style37_bg_content_color !='' ){
				ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%%.et_pb_jv_team_members_style37 .et_pb_jv_team',
					'declaration' => sprintf('background-color: %1$s;',esc_html( $style37_bg_content_color )),
				) );
			}
			if( $style37_bg_content_hover_color !='' ){
				ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%%.et_pb_jv_team_members_style37 .et_pb_jv_team:hover',
					'declaration' => sprintf('background-color: %1$s;',esc_html( $style37_bg_content_hover_color )),
				) );
			}
			if( $style37_border_color !='' ){
				ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%%.et_pb_jv_team_members_style37 .et_pb_jv_team_social_link',
					'declaration' => sprintf('border-top: 1px solid %1$s;',esc_html( $style37_border_color )),
				) );
			}
		} 
		if( $select_style == 'style38' ){
				if( $style38_bg_content_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style38 .et_pb_jv_team',
						'declaration' => sprintf('background-color: %1$s;',esc_html( $style38_bg_content_color )),
					) );
				}
				if( $style38_title_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style38 .et_pb_jv_team_description .et_pb_jv_team_title a',
						'declaration' => sprintf('color: %1$s;',esc_html( $style38_title_color )),
					) );
				}
				if( $style38_bg_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style38 .et_pb_jv_team_social_link',
						'declaration' => sprintf('background: %1$s;',esc_html( $style38_bg_color )),
					) );	
				}
		} 
		if( $select_style == 'style39' ){
				if( $style39_bg_image_hover_color != '' ){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style39 .et_pb_jv_team .et_pb_jv_team_image:after',
						'declaration' => sprintf('background-color: %1$s;',esc_html( $style39_bg_image_hover_color )),
					) );
				}	
				if( $style39_image_border_color != '' ){
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%%.et_pb_jv_team_members_style39 .et_pb_jv_team .et_pb_jv_team_image',
					'declaration' => sprintf('border: 3px solid %1$s;',esc_html( $style39_image_border_color )),
					) );
				}
				if( $style39_image_hover_border_color != '' ){
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%%.et_pb_jv_team_members_style39 .et_pb_jv_team:hover .et_pb_jv_team_image',
					'declaration' => sprintf('border: 3px solid %1$s;',esc_html( $style39_image_hover_border_color )),
					) );
				}
				if( $style39_frame_border_color != '' ){
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%%.et_pb_jv_team_members_style39 .et_pb_jv_team:before, %%order_class%%.et_pb_jv_team_members_style39 .et_pb_jv_team:after',
					'declaration' => sprintf('background-color: %1$s;',esc_html( $style39_frame_border_color )),
					) );
				}
		 } 
		 if ( $select_style == 'style40' ) {
		 	 	if( $style40_image_box_shadow_color !=''){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style40 .et_pb_jv_team:hover',
						'declaration' => sprintf('box-shadow: 0 0 10px %1$s;', esc_html( $style40_image_box_shadow_color )),
					) );	
				}
				if( $style40_icon_color !=''){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style40 .et_pb_jv_team .icon',
						'declaration' => sprintf('background-color: %1$s;',esc_html( $style40_icon_color )),
					) );
				}
				if( $style40_social_media_bg_color !=''){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style40 .et_pb_jv_team .et_pb_jv_team_social_link li a',
						'declaration' => sprintf('background: %1$s;',esc_html( $style40_social_media_bg_color )),
					) );
				}
		  } 
		  if ( $select_style == 'style41' ) {
				if( $style41_content_bg_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style41 .et_pb_jv_team',
						'declaration' => sprintf('background: %1$s;',esc_html( $style41_content_bg_color )),
					) );
				}
				if( $style41_content_bg_overlayer_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style41 .et_pb_jv_team .et_pb_jv_team_image:before',
						'declaration' => sprintf('background: %1$s;',esc_html( $style41_content_bg_overlayer_color )),
					) );
				}
				if( $style41_image_hover_border_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style41 .et_pb_jv_team .et_pb_jv_team_image:after',
						'declaration' => sprintf('background: %1$s;',esc_html( $style41_image_hover_border_color )),
					) );
				}
				if( $style41_image_hover_box_shadow_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style41 .et_pb_jv_team:hover .et_pb_jv_team_image img',
						'declaration' => sprintf('box-shadow: 0 0 0 14px %1$s;',esc_html( $style41_image_hover_box_shadow_color )),
					) );
				}
				if( $style41_social_media_bg_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style41 .et_pb_jv_team .et_pb_jv_team_social_link',
						'declaration' => sprintf('background: %1$s;',esc_html( $style41_social_media_bg_color )),
					) );
				}
				if( $style41_social_media_bg_hover_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style41 .et_pb_jv_team .et_pb_jv_team_social_link li a:hover',
						'declaration' => sprintf('background: %1$s;',esc_html( $style41_social_media_bg_hover_color )),
					) );
				}
			} 
			if ( $select_style == 'style42' ) {
				if( $style42_content_bg_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style42 .et_pb_jv_team .et_pb_jv_team_image',
						'declaration' => sprintf('background: %1$s;',esc_html( $style42_content_bg_color )),
					) );
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style42 .et_pb_jv_team .et_pb_position:after',
						'declaration' => sprintf('background: %1$s;',esc_html( $style42_content_bg_color )),
					) );
				}
				if( $style42_social_media_bg_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style42 .et_pb_jv_team .et_pb_jv_team_social_link li a',
						'declaration' => sprintf('background: %1$s;',esc_html( $style42_social_media_bg_color )),
					) );
				}
				if( $style42_social_media_bg_hover_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style42 .et_pb_jv_team .et_pb_jv_team_social_link li a:hover',
						'declaration' => sprintf('background: %1$s;',esc_html( $style42_social_media_bg_hover_color )),
					) );
				}
			} 
			if ( $select_style == 'style43' ) {
				if( $style43_content_bg_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style43 .et_pb_jv_team',
						'declaration' => sprintf('background: %1$s;',esc_html( $style43_content_bg_color )),
					) );
				}
				if( $style43_image_hover_border_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style43 .et_pb_jv_team:hover .et_pb_jv_team_image',
						'declaration' => sprintf('background: %1$s;',esc_html( $style43_image_hover_border_color )),
					) );
				}
				if( $style43_social_media_border_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style43 .et_pb_jv_team .et_pb_jv_team_social_link li a',
						'declaration' => sprintf('border: 1px solid %1$s;',esc_html( $style43_social_media_border_color )),
					) );
				}
				if( $style43_social_media_bg_hover_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style43 .et_pb_jv_team:hover .et_pb_jv_team_social_link li a',
						'declaration' => sprintf('background: %1$s;',esc_html( $style43_social_media_bg_hover_color )),
					) );
				}
			} 
			if ( $select_style == 'style44' ) {
				if( $style44_content_bg_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style44 .et_pb_jv_team',
						'declaration' => sprintf('background: %1$s;',esc_html( $style44_content_bg_color )),
					) );
				}
				if( $style44_content_bg_hover_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style44 .et_pb_jv_team:hover',
						'declaration' => sprintf('background: %1$s;',esc_html( $style44_content_bg_hover_color )),
					) );
				}
				if( $style44_bg_image_hover_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style44 .et_pb_jv_team .et_pb_jv_team_image:before',
						'declaration' => sprintf('background: %1$s;',esc_html( $style44_bg_image_hover_color )),
					) );
				}
				if( $style44_social_media_bg_hover_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style44 .et_pb_jv_team .et_pb_jv_team_social_link a:hover',
						'declaration' => sprintf('background: %1$s;',esc_html( $style44_social_media_bg_hover_color )),
					) );
				}
			} 
			if ( $select_style == 'style45' ) {
				if( $style45_content_bg_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style45 .et_pb_jv_team .et_pb_jv_team_description',
						'declaration' => sprintf('background: %1$s;',esc_html( $style45_content_bg_color )),
					) );
				}
				if( $style45_bg_image_hover_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style45 .et_pb_jv_team .et_pb_jv_team_social_link',
						'declaration' => sprintf('background: %1$s;',esc_html( $style45_bg_image_hover_color )),
					) );
				}
			} 
			if ( $select_style == 'style46' ) {
				if( $style46_bg_content_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style46 .et_pb_jv_team .et_pb_jv_team_description',
						'declaration' => sprintf('background: %1$s;',esc_html( $style46_bg_content_color )),
					) );
				}
			} 
			if ( $select_style == 'style47' ) {	
				if( $style47_image_curve_border_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style47 .et_pb_jv_team .et_pb_jv_team_image',
						'declaration' => sprintf('border-top: 5px solid %1$s;border-bottom: 5px solid %1$s;',esc_html( $style47_image_curve_border_color )),
					) );
				}
				if( $style47_image_curve_border_hover_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style47 .et_pb_jv_team:hover .et_pb_jv_team_image',
						'declaration' => sprintf('border-top-color: %1$s;border-bottom-color: %1$s;',esc_html( $style47_image_curve_border_hover_color )),
					) );
				}
				if( $style47_social_media_border_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style47 .et_pb_jv_team .et_pb_jv_team_social_link li a',
						'declaration' => sprintf('border: 1px solid %1$s;',esc_html( $style47_social_media_border_color )),
					) );
				}
				if( $style47_social_media_bg_hover_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style47 .et_pb_jv_team:hover .et_pb_jv_team_social_link li a',
						'declaration' => sprintf('background: %1$s;',esc_html( $style47_social_media_bg_hover_color )),
					) );
				}
			} 
			if ( $select_style == 'style48' ) {
				if( $style48_bg_content_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style48 .et_pb_jv_team .et_pb_jv_team_description',
						'declaration' => sprintf('background: %1$s;',esc_html( $style48_bg_content_color )),
					) );
				}
				if( $style48_bg_content_hover_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style48 .et_pb_jv_team:hover .et_pb_jv_team_description',
						'declaration' => sprintf('background: %1$s;',esc_html( $style48_bg_content_hover_color )),
					) );
				}
				if( $style48_image_bg_shadow_gardin1_hover_color !='' && $style48_image_bg_shadow_gardin2_hover_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
							'selector'    => '%%order_class%%.et_pb_jv_team_members_style48 .et_pb_jv_team .et_pb_jv_team_social',
							'declaration' => sprintf('background: linear-gradient( to right, %1$s, %2$s );',esc_html( $style48_image_bg_shadow_gardin1_hover_color ),esc_html( $style48_image_bg_shadow_gardin2_hover_color )),
					) );
				}
				if( $style48_social_media_bg_hover_color !='' ) {
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style48 .et_pb_jv_team .et_pb_jv_team_social_link li a',
						'declaration' => sprintf('background: %1$s;',esc_html( $style48_social_media_bg_hover_color )),
					) );
				}
			} 
			if ( $select_style == 'style49' ){	
				if( ( $style49_image_layer1_color !='' )  && ( $style49_image_layer2_color !='' ) && ( $style49_image_layer3_color !='' ) && ( $style49_image_layer4_color !='') ){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style49 .et_pb_jv_team:hover .et_pb_jv_team_image:after, %%order_class%%.et_pb_jv_team_members_style49 .et_pb_jv_team:hover .et_pb_jv_team_image:before',
						'declaration' => sprintf('box-shadow: 5px 0 0 %1$s inset,0 5px 0 %2$s inset,-5px 0 0 %3$s inset,-5px 0 0 %4$s inset;',
							esc_html( $style49_image_layer1_color ),esc_html( $style49_image_layer2_color ),esc_html( $style49_image_layer3_color ),esc_html( $style49_image_layer4_color )),
					) );
				}  
				if( $style49_social_media_bg_color !='' ) {
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style49 .et_pb_jv_team .et_pb_jv_team_social_link li a',
						'declaration' => sprintf('background: %1$s;',esc_html( $style49_social_media_bg_color )),
					) );
				}
				if( $style49_social_media_bg_hover_color !='' ) {
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style49 .et_pb_jv_team:hover .et_pb_jv_team_social_link li a',
						'declaration' => sprintf('background: %1$s;',esc_html( $style49_social_media_bg_hover_color )),
					) );
				}
			} 
			if ( $select_style == 'style50' ){	
				if( $style50_image_bg_hover_color !='' ) {
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style50 .et_pb_jv_team',
						'declaration' => sprintf('background: %1$s;',esc_html( $style50_image_bg_hover_color )),
					) );
				}
				if( $style50_image_bg_hover_border_color !='' ) {
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style50 .et_pb_jv_team .et_pb_position',
						'declaration' => sprintf('background: %1$s;',esc_html( $style50_image_bg_hover_border_color )),
					) );
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style50 .et_pb_jv_team .et_pb_jv_team_title:before, %%order_class%%.et_pb_jv_team_members_style50 .et_pb_jv_team .et_pb_jv_team_title:after',
						'declaration' => sprintf('background: %1$s;',esc_html( $style50_image_bg_hover_border_color )),
					) );
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style50 .et_pb_jv_team .et_pb_jv_team_content:before, %%order_class%%.et_pb_jv_team_members_style50 .et_pb_jv_team .et_pb_jv_team_content:after',
						'declaration' => sprintf('background: %1$s;',esc_html( $style50_image_bg_hover_border_color )),
					) );
				}
				if( $style50_image_box_shadows_color !='' ) {
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style50 .et_pb_jv_team',
						'declaration' => sprintf('box-shadow:  0 0 10px %1$s;',esc_html( $style50_image_box_shadows_color )),
					) );
				}
			} 
			if ( $select_style == 'style51' ){
				if( ( $style51_image_animation_border_color1 !='' ) && ( $style51_image_animation_border_color2 !='' ) ){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style51 .et_pb_jv_team .et_pb_jv_team_image:after',
						'declaration' => sprintf('border-color: %1$s %1$s %2$s %2$s;',esc_html( $style51_image_animation_border_color1 ),( $style51_image_animation_border_color2 )),
					) );
				}
				if( $style51_social_media_background_color !='' ) {
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style51 .et_pb_jv_team .et_pb_jv_team_social_link li a',
						'declaration' => sprintf('background: %1$s;',esc_html( $style51_social_media_background_color )),
					) );
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style51 .et_pb_jv_team .et_pb_jv_team_title:after',
						'declaration' => sprintf('background: %1$s;',esc_html( $style51_social_media_background_color )),
					) );
				}
				if( $style51_social_media_background_hover_color !='' ) {
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style51 .et_pb_jv_team .et_pb_jv_team_social_link li a:hover',
						'declaration' => sprintf('background: %1$s;',esc_html( $style51_social_media_background_hover_color )),
					) );
				}
			} 
			if ( $select_style == 'style52' ){
				if( $style52_image_bg_overlay_color !='' ) {
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style52 .et_pb_jv_team .et_pb_jv_team_image:after',
						'declaration' => sprintf('background: %1$s;',esc_html( $style52_image_bg_overlay_color )),
					) );
				}	
				if( $style52_image_border_color !='' ) {
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style52 .et_pb_jv_team .jv_team_content',
						'declaration' => sprintf('border: 2px dotted %1$s;',esc_html( $style52_image_border_color )),
					) );
				}	
				if( $style52_image_border_hover_color !='' ) {
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style52 .et_pb_jv_team:hover .jv_team_content',
						'declaration' => sprintf('border: 2px dotted %1$s;',esc_html( $style52_image_border_hover_color )),
					) );
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style52 .et_pb_jv_team:hover .et_pb_jv_team_title',
						'declaration' => sprintf('color: %1$s !important;',esc_html( $style52_image_border_hover_color )),
					) );
				}	
				if( $style52_social_media_border_color !='' ) {
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style52 .et_pb_jv_team .et_pb_jv_team_social_link li a',
						'declaration' => sprintf('border: 1px solid %1$s;',esc_html( $style52_social_media_border_color )),
					) );
				}
				if( $style52_social_media_bg_hover_color !='' ) {
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style52 .et_pb_jv_team .et_pb_jv_team_social_link li a:hover',
						'declaration' => sprintf('background: %1$s;',esc_html( $style52_social_media_bg_hover_color )),
					) );
				}
			} 
			if ( $select_style == 'style53' ){
				if( $style53_image_overlay_bg_color !='' ) {
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style53 .et_pb_jv_team:after',
						'declaration' => sprintf('background: %1$s;',esc_html( $style53_image_overlay_bg_color )),
					) );
				}
				if( $style53_border_color !='' ) {
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style53 .et_pb_jv_team .jv_team_content:after',
						'declaration' => sprintf('background: %1$s;',esc_html( $style53_border_color )),
					) );
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style53 .et_pb_jv_team .jv_team_content',
						'declaration' => sprintf('border-bottom: 3px solid %1$s;',esc_html( $style53_border_color )),
					) );
				}
				if( $style53_social_media_bg_color !='' ) {
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style53 .et_pb_jv_team .et_pb_jv_team_social_link li a',
						'declaration' => sprintf('background: %1$s;',esc_html( $style53_social_media_bg_color )),
					) );
				}
			} 
			if ( $select_style == 'style54' ){
				if(( $style54_bg_image_gardian1_color !='' ) && ( $style54_bg_image_gardian2_color !='' )){
					ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%%.et_pb_jv_team_members_style54 .et_pb_jv_team .jv_team_content, %%order_class%%.et_pb_jv_team_members_style54 .et_pb_jv_team .et_pb_jv_team_social_link li a:hover',
					'declaration' => sprintf('background: linear-gradient(to right, %1$s, %2$s);',esc_html( $style54_bg_image_gardian1_color ),esc_html( $style54_bg_image_gardian2_color )),
					) );
				}
			} 
			if ( $select_style == 'style55' ){	
				if( $style55_image_bg_overlayer_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style55 .et_pb_jv_team .et_pb_jv_team_image:before',
						'declaration' => sprintf('background: %1$s;',esc_html( $style55_image_bg_overlayer_color )),
					) );
				}
				if( $style55_border_hover_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style55 .et_pb_jv_team:before',
						'declaration' => sprintf('border: 4px solid %1$s;',esc_html( $style55_border_hover_color )),
					) );
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style55 .et_pb_jv_team .et_pb_jv_team_social_link li a',
						'declaration' => sprintf('border-right: 1px solid %1$s;',esc_html( $style55_border_hover_color )),
					) );
				}	
			} 
			if ( $select_style == 'style56' ){
				if( $style56_social_media_bg_color !='' ){
						ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style56 .et_pb_jv_team .et_pb_jv_team_social_link',
						'declaration' => sprintf('background: %1$s;',esc_html( $style56_social_media_bg_color )),
					) );
				}
				if( $style56_content_bg_color !='' ){	
					   ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style56 .et_pb_jv_team .jv_team_content',
						'declaration' => sprintf('background: %1$s;',esc_html( $style56_content_bg_color )),
					   ) );
				}
			}
			if ( $select_style == 'style57' ){
				if( $style57_content_hover_bg_color !='' ){	
					   ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style57 .et_pb_jv_team:hover ,%%order_class%%.et_pb_jv_team_members_style57 .et_pb_jv_team .et_pb_jv_team_social_link',
						'declaration' => sprintf('background: %1$s;',esc_html( $style57_content_hover_bg_color )),
					   ) );
					    ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style57 .et_pb_jv_team',
						'declaration' => sprintf('color: %1$s;',esc_html( $style57_content_hover_bg_color )),
					   ) );
					    ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style57 .et_pb_jv_team:hover',
						'declaration' => sprintf('color: %1$s;','#fff'),
					   ) );
				}
			}
			if ( $select_style == 'style57' ){
				if( $style57_social_media_hover_bg_color !='' ){	
					    ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style57 .et_pb_jv_team .et_pb_jv_team_social_link li a i:hover',
						'declaration' => sprintf('color: %1$s;',esc_html( $style57_social_media_hover_bg_color )),
					   ) );
				}
			}
			if ( $select_style == 'style58' ){
				if( $style58_overlay_hover_color1 !='' ){	
					   ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style58 .et_pb_jv_team .et_pb_jv_team_pic:before',
						'declaration' => sprintf('background: %1$s;',esc_html( $style58_overlay_hover_color1 )),
					   ) );
				}
				if( $style58_overlay_hover_color2 !='' ){	
					   ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style58 .et_pb_jv_team .et_pb_jv_team_pic:after',
						'declaration' => sprintf('background: %1$s;',esc_html( $style58_overlay_hover_color2 )),
					   ) );
				}
				if( $style58_social_media_bg_color !='' ){	
					   ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style58 .et_pb_jv_team .et_pb_jv_team_social_link li a i:after',
						'declaration' => sprintf('background: %1$s;',esc_html( $style58_social_media_bg_color )),
					   ) );
				}
				if( $style58_content_title_color !='' ){	
					   ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style58 .et_pb_jv_team .et_pb_jv_team_title',
						'declaration' => sprintf('color: %1$s;',esc_html( $style58_content_title_color )),
					   ) );
				}
			}
			if ( $select_style == 'style59' ){
				if( $style59_content_bg_img_border_hover_color !='' ){	
					   ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style59 .et_pb_jv_team .et_pb_jv_team_pic, %%order_class%%.et_pb_jv_team_members_style59 .et_pb_jv_team .jv_team_content',
						'declaration' => sprintf('background: %1$s;',esc_html( $style59_content_bg_img_border_hover_color )),
					   ) );
				}
				if( $style59_social_media_hover_bg_color !='' ){	
					   ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style59 .et_pb_jv_team .et_pb_jv_team_social_link li a i:before',
						'declaration' => sprintf('background: %1$s;',esc_html( $style59_social_media_hover_bg_color )),
					   ) );
				}
			}			
			if ( $select_style == 'style60' ){
				if( $style60_img_border_hover_color !='' ){	
					   ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style60 .et_pb_jv_team',
						'declaration' => sprintf('border:2px solid %1$s;',esc_html( $style60_img_border_hover_color )),
					   ) );
					   ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style60 .et_pb_jv_team:after, %%order_class%%.et_pb_jv_team_members_style60 .et_pb_jv_team:before',
						'declaration' => sprintf('background: %1$s;',esc_html( $style60_img_border_hover_color )),
					   ) );
					   ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style60 .et_pb_jv_team .et_pb_jv_team_social_link li a i',
						'declaration' => sprintf('color: %1$s;',esc_html( $style60_img_border_hover_color )),
					   ) );
						 ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style60 .et_pb_jv_team .et_pb_jv_team_social_link li a i:hover',
						'declaration' => sprintf('background: linear-gradient(to bottom,%1$s, darkred);',esc_html( $style60_img_border_hover_color )),
					   ) );
						  ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style60 .et_pb_jv_team .et_pb_jv_team_social_link li a i:hover',
						'declaration' => sprintf('color: %1$s;','#fff'),
					   ) );
				}
				if( $style60_content_bg_social_media_color !='' ){	
					   ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%%.et_pb_jv_team_members_style60 .et_pb_jv_team .jv_team_content',
						'declaration' => sprintf('background: %1$s;',esc_html( $style60_content_bg_social_media_color )),
					   ) );
				}
			}						
			$ranclass = "jvteam_".rand(10,100);
			/********************************************Order By ASC And DESC*********************************************/
		    if ( $posts_number == ''){
				$args = array( 'posts_per_page' => -1 );
			}else{
				$args = array( 'posts_per_page' => (int) $posts_number );
			}
			$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
			if ( 'date_desc' !== $orderby ) {
				switch( $orderby ) {
					case 'date_asc' :
						$args['orderby'] = 'date';
						$args['order'] = 'ASC';
						break;
					case 'title_asc' :
						$args['orderby'] = 'title';
						$args['order'] = 'ASC';
						break;
					case 'title_desc' :
						$args['orderby'] = 'title';
						$args['order'] = 'DESC';
						break;
					case 'rand' :
						$args['orderby'] = 'rand';
						break;
					case 'menu_order_desc' :
						$args['orderby'] = 'menu_order';
						$args['order'] = 'DESC';
						break;
					case 'menu_order_asc' :
						$args['orderby'] = 'menu_order';
						$args['order'] = 'ASC';
						break;
				}
			}else{
				$args['orderby'] = 'date';
				$args['order'] = 'DESC';
			}
			$args['post_type'] = 'jv_team_members';
			$args['paged'] = $paged;
			$archive_jv_team_output = '';
			$archive_jv_team_grid_class = "archive_jv_team_view_grid";
		   /*Filter Option*/
		   if ( 'id_desc' !== $category_orderby ) {
				switch( $category_orderby ) {
					case 'id_asc' :
						$orderby = 'id';
						$order = 'ASC';
						break;
					case 'name_asc' :
						$orderby = 'name';
						$order = 'ASC';
						break;
					case 'name_desc' :
						$orderby = 'name';
						$order = 'DESC';
						break;
					case 'count' :
						$orderby = 'count';
						break;
					case 'slug_desc' :
						$orderby = 'slug';
						$order = 'DESC';
						break;
					case 'slug_asc' :
						$orderby = 'slug';
						$order = 'ASC';
						break;
				}
			}else{
				$orderby = 'id';
				$order = 'DESC';
			}
			
			$archive_department_category_terms = get_terms( array(
				'taxonomy' => 'department_category',
				'hide_empty' => false,
				'orderby'           => $orderby, 
	    		'order'             => $order,
			) );
		
			//if($display_category_filter == 'off' ){
				if ( $display_category != 'all' ){
					$args['tax_query'] = array(
						array(
							'taxonomy' => 'department_category',
							'field' => 'term_id',
							'terms' => $include_categories_array,
							'operator' => 'IN'
						)
					);
				}
			//}
			if($display_category_filter == 'on' ){
				 if ( $display_category == 'all'){
					$archive_jv_team_output .= '<ul class="et_pb_jv_team_members_category_list '.$filter_style.' "><li>
										<a href="javascript:void(0)" class="jv_filter active" data-filter=".all" ><span>'.$filter_all_label.'</span></a></li>';
					$archive_department_category_count = count($archive_department_category_terms);
					if ( $archive_department_category_count > 0 ){
					foreach($archive_department_category_terms as $department_categoryval){
							$archive_jv_team_output .= '<li><a href="javascript:void(0)" class="jv_filter" data-filter=".cat-'.$department_categoryval->term_id.'" ><span>'.$department_categoryval->name.'</span></a></li>';
						}
					}
					$archive_jv_team_output .= '</ul>';
				 }else{
					if ( $include_team_categories != '' ){
						$archive_jv_team_output .= '<ul class="et_pb_jv_team_members_category_list '.$filter_style.' "><li>
											<a href="javascript:void(0)" class="jv_filter active" data-filter=".all" ><span>'.$filter_all_label.'</span></a></li>';
						$archive_department_category_count = count($archive_department_category_terms);
						if ( $archive_department_category_count > 0 ){
							foreach($archive_department_category_terms as $department_categoryval){
								if  ( in_array($department_categoryval->term_id,$include_categories_array)){
									$archive_jv_team_output .= '<li><a href="javascript:void(0)" class="jv_filter" data-filter=".cat-'.$department_categoryval->term_id.'" ><span>'.$department_categoryval->name.'</span></a></li>';
								}
							}
						}
						$archive_jv_team_output .= '</ul>';
					}
				}
			}
			/*Filter Option*/
			$archive_jv_team_output .= '<div class="et_pb_module et_pb_jv_team_members '.$ranclass.' et_pb_jv_team_members_'.$select_style.' '.$module_class.' '.$archive_jv_team_grid_class.'" '.$module_id.'>';
			$archive_jv_team_query = new WP_Query( $args );
			if ( $archive_jv_team_query->have_posts() ) { 
			$jv_template_path =  get_stylesheet_directory() . '/divi-team-members';
			$jv_css_path = $jv_template_path.'/css/archive_team_members_grid_css';
			$jv_css_url =  get_stylesheet_directory_uri().'/divi-team-members/css/archive_team_members_grid_css'; 
			
			$jv_grid_path =  $jv_template_path.'/content-archive-team-grid';
			
			if ( file_exists( $jv_css_path . '/jv_team_module_member_'.$select_style.'.css' ) )
			{
				wp_enqueue_style('jv_team_module_member_'.$select_style, $jv_css_url . '/jv_team_module_member_'.$select_style.'.css', array(), NULL);
			}else{
				wp_enqueue_style('jv_team_module_member_'.$select_style, JVTEAM_PLUGIN_URL .'assets/css/archive_team_members_grid_css/jv_team_module_member_'.$select_style.'.css', array(), NULL);
			}
					  while ( $archive_jv_team_query->have_posts() ) { $archive_jv_team_query->the_post();
					   ob_start();
					   
					    $jv_team_designation = get_post_meta( get_the_ID(),'jv_team_designation', true );
						$jv_team_facebook = get_post_meta( get_the_ID(),'jv_team_facebook', true );
						$jv_team_twitter = get_post_meta( get_the_ID(),'jv_team_twitter', true );
						$jv_team_google = get_post_meta( get_the_ID(),'jv_team_google', true );
						$jv_team_linkdin = get_post_meta( get_the_ID(),'jv_team_linkedin', true );
						$jv_team_instagram = get_post_meta( get_the_ID(),'jv_team_instagram', true );
						
						$jv_team_email_address = get_post_meta( get_the_ID(),'jv_team_email_address', true );
						$jv_team_website_url = get_post_meta( get_the_ID(),'jv_team_website_url', true );
						$jv_team_address1 = get_post_meta( get_the_ID(),'jv_team_address1', true );
						$jv_team_address2 = get_post_meta( get_the_ID(),'jv_team_address2', true );
						$city_name_value = get_post_meta( get_the_ID(), 'jv_team_city_name', true );
						$state_name_value = get_post_meta( get_the_ID(), 'jv_team_state_name', true );
						$country_name_value = get_post_meta( get_the_ID(), 'jv_team_country_name', true );
						$zipcode_number_value = get_post_meta( get_the_ID(), 'jv_team_zipcode', true );
						$jv_team_contact_form = get_post_meta( get_the_ID(), 'jv_team_contact_form', true );
						$display_contact_form = get_post_meta( get_the_ID(), 'display_contact_form', true );
						$divi_team_contact_us_title = !empty(get_option('divi_team_contact_us_title')) ? get_option('divi_team_contact_us_title') : 'Drop Me a Line';
						$jv_team_mobile_number = get_post_meta( get_the_ID(),'jv_team_phone_number', true );
							
						$jv_team_address = '';
						if( $jv_team_address1 != ''){
							$jv_team_address .= $jv_team_address1.',';
						}
						if( $jv_team_address2 != ''){
							$jv_team_address .= $jv_team_address2.',';
						}
						if( $city_name_value != ''){
							$jv_team_address .= $city_name_value.',';
						}
						if( $state_name_value != ''){
							$jv_team_address .= $state_name_value.',';
						}
						if( $country_name_value != ''){
							$jv_team_address .= $country_name_value.',';
						}
						if( $zipcode_number_value != ''){
							$jv_team_address .= $zipcode_number_value.',';
						}

					   if ( file_exists( $jv_grid_path . '/content-archive-team-'.$select_style.'.php' ) ){
					   		 include $jv_grid_path. '/content-archive-team-'.$select_style.'.php';
					   }else{
					   		 include JVTEAM_PLUGIN_PATH. '/content-archive-team-grid/content-archive-team-'.$select_style.'.php';
					   }
					   $archive_jv_team_output .= ob_get_contents();
					   ob_end_clean();
				    }
					wp_reset_query();
			}
			wp_reset_postdata();
			$archive_jv_team_output .= '</div>';
			if($team_members_display_pagination == 'on'){
				$archive_jv_team_output .= '<div class="et_pb_row jv_team_archive_pagination">';
				$big = 999999999; // need an unlikely integer
				$archive_jv_team_output .= paginate_links(array(
					'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
					'format' => '?paged=%#%',
					'current' => max( 1, get_query_var('paged') ),
					'total' => $archive_jv_team_query->max_num_pages, 
					'prev_text' => __( '<span class="et-pb-icon">&#x34;</span>', 'textdomain' ),
					'next_text' => __( '<span class="et-pb-icon">&#x35;</span>', 'textdomain' ),
				) );
				$archive_jv_team_output .= '</div>';
			}
			$archive_jv_team_output .= '<script>
				jQuery(window).load(function() {
					if( jQuery("body").find(".'.$ranclass.' .jv_team_member_equalheight").length >0 ){
						equalheight(".'.$ranclass.' .jv_team_member_equalheight");
					 }
				});
				jQuery(window).resize(function(){
			  		if( jQuery("body").find(".'.$ranclass.' .jv_team_member_equalheight").length >0 ){
						equalheight(".'.$ranclass.' .jv_team_member_equalheight");
					 }
				});	 	
	 </script>';
			if( $display_popup_onteam =='on') {
					$archive_jv_team_output .= "<script>
					 jQuery(function ($) {
					 	$('a[href^=\"#teammodal\"]').addClass('et_smooth_scroll_disabled');
						$('.popup-modal').magnificPopup({
							type: 'inline',
							midClick: true,
							closeBtnInside: true
						});
					});
					 </script>";
			}
			return $archive_jv_team_output;
		}
		public function process_box_shadow( $render_slug ) {
			$boxShadow = ET_Builder_Module_Fields_Factory::get( 'BoxShadow' );
			self::set_style( $render_slug, $boxShadow->get_style(
				sprintf( '.%1$s .et_pb_jv_team_members_column', self::get_module_order_class( $render_slug ) ),
				$this->props
			) );
		}
	}
	new ET_Builder_Module_JV_Team_Grid_Member();
}
?>