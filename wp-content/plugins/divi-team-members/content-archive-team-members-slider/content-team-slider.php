<?php
$link = "#";
if( $team_members_slider_display_detail_page_link_type =='default' ) { 
		$link = get_permalink(get_the_ID());
}else if ( $team_members_slider_display_detail_page_link_type =='custom' ){
		$url = get_post_meta( get_the_ID(),'custom_url_detail_page', true );
		$link = $url!= '' ? $url : '#';
}else{
		$link = "#";
}
					
$class_1 = '';$link_no_new_tab = '';$link_new_tab_open = '';$img_stlink = '';$target_link = '';$img_endlink = '';

// Image ST
if ($link_open_in_new_tab == 'on'){ $target_link = 'target="_blank"';}
if( $team_members_slider_display_detail_page == 'on' && $display_popup_onteam == 'off' ) { 
	$img_stlink = '<a href="'.$link.'" '.$target_link.'>'; 
} else if(  $display_popup_onteam == 'on' ){ 
	$img_stlink = '<a class="popup-modal" href="#teammodal'.get_the_ID().'">';
}else{ $img_stlink = '';}
// Image End
if( ( $team_members_slider_display_detail_page == 'on' && $display_popup_onteam == 'off' ) || ( $display_popup_onteam == 'on' ) ) { $img_endlink = '</a>';}

 $jv_team_designation = get_post_meta( get_the_ID(),'jv_team_designation', true );
$jv_team_facebook = get_post_meta( get_the_ID(),'jv_team_facebook', true );
$jv_team_twitter = get_post_meta( get_the_ID(),'jv_team_twitter', true );
$jv_team_google = get_post_meta( get_the_ID(),'jv_team_google', true );
$jv_team_linkdin = get_post_meta( get_the_ID(),'jv_team_linkedin', true );
$jv_team_instagram = get_post_meta( get_the_ID(),'jv_team_instagram', true );

$jv_team_email_address = get_post_meta( get_the_ID(),'jv_team_email_address', true );
$jv_team_website_url = get_post_meta( get_the_ID(),'jv_team_website_url', true );
$jv_team_address1 = get_post_meta( get_the_ID(),'jv_team_address1', true );
$jv_team_address2 = get_post_meta( get_the_ID(),'jv_team_address2', true );
$city_name_value = get_post_meta( get_the_ID(), 'jv_team_city_name', true );
$state_name_value = get_post_meta( get_the_ID(), 'jv_team_state_name', true );
$country_name_value = get_post_meta( get_the_ID(), 'jv_team_country_name', true );
$zipcode_number_value = get_post_meta( get_the_ID(), 'jv_team_zipcode', true );
$jv_team_contact_form = get_post_meta( get_the_ID(), 'jv_team_contact_form', true );
$display_contact_form = get_post_meta( get_the_ID(), 'display_contact_form', true );
$divi_team_contact_us_title = !empty(get_option('divi_team_contact_us_title')) ? get_option('divi_team_contact_us_title') : 'Drop Me a Line';
$jv_team_mobile_number = get_post_meta( get_the_ID(),'jv_team_phone_number', true );
	
$jv_team_address = '';
if( $jv_team_address1 != ''){
	$jv_team_address .= $jv_team_address1.',';
}
if( $jv_team_address2 != ''){
	$jv_team_address .= $jv_team_address2.',';
}
if( $city_name_value != ''){
	$jv_team_address .= $city_name_value.',';
}
if( $state_name_value != ''){
	$jv_team_address .= $state_name_value.',';
}
if( $country_name_value != ''){
	$jv_team_address .= $country_name_value.',';
}
if( $zipcode_number_value != ''){
	$jv_team_address .= $zipcode_number_value.',';
}
?>
<div class="swiper-slide team_member_swiperslider">
    <div class="team_member_slider">
		<div class="tms_img">
			<?php $tms_thumb = wp_get_attachment_image_src(get_post_thumbnail_id(), 'jv_team_grid_image');
					if( $tms_thumb[0] != ''){ 
								$image_path = $tms_thumb[0] ;
							}else{
								$image_path = JVTEAM_PLUGIN_URL . '/assets/images/default.png';
							} 
			?>
			<img src="<?php echo $image_path;?>" alt="<?php echo get_the_title();?>">
		</div>
		<div class="tms_professional">
			<?php if( $team_members_slider_display_detail_page == 'on' ) { echo $img_stlink; }?>
			<h3 class="tms_title"><?php echo get_the_title();?></h3>
			<?php if( $team_members_slider_display_detail_page =='on' ) { echo $img_endlink;}
			if( $jv_team_designation != ''){ ?>
			<span class="team_position"><?php echo $jv_team_designation;?></span>
			<?php } ?>
			<div class="team_description <?php if ( $team_members_slider_display_social_link == 'off'){?>tms_content_bottom<?php }?>">
			<?php $content_len= strlen(get_the_content());
                           $divi_team_content_length = !empty(get_option('divi_team_content_length')) ? get_option('divi_team_content_length') : '180';
                           if ($content_len > $divi_team_content_length){
                               //echo do_shortcode(wpautop(substr(strip_tags(get_the_content()),0,$divi_team_content_length).'...'));
							   $et_fb_processing_shortcode_object = false;
								echo et_core_intentionally_unescaped( wpautop( et_delete_post_first_video( strip_shortcodes( truncate_post( $divi_team_content_length, false, '', true ) ) ) ), 'html' );
								$et_fb_processing_shortcode_object = $global_processing_original_value;
                           }else{
                               $post_content = et_strip_shortcodes( et_delete_post_first_video( get_the_content() ), true );
								echo et_core_intentionally_unescaped( apply_filters( 'the_content', $post_content ), 'html' ) ;	
                           }
                    ?></div>
			<?php if ( $team_members_slider_display_social_link == 'on'){?>
			<ul class="tms_social_media">
				<?php if( $jv_team_facebook != '' ) { ?><li><a href="<?php echo $jv_team_facebook;?>" target="_blank"><i class="tms_social_font et-pb-icon">&#xe0aa;</i></a></li><?php } ?>
				<?php if( $jv_team_twitter != '' ) { ?><li><a href="<?php echo $jv_team_twitter;?>" target="_blank"><i class="tms_social_font et-pb-icon">&#xe0ab;</i></a></li><?php } ?>
				<?php if( $jv_team_google != '' ) { ?><li><a href="<?php echo $jv_team_google;?>" target="_blank"><i class="tms_social_font jv_team_member_social_font_gogle et-pb-icon">&#xe0ad;</i></a></li><?php } ?>
				<?php if( $jv_team_linkdin != '' ) { ?><li><a href="<?php echo $jv_team_linkdin;?>"target="_blank"><i class="tms_social_font et-pb-icon">&#xe0b4;</i></a></li><?php } ?>
				<?php if( $jv_team_instagram != '' ) { ?><li><a href="<?php echo $jv_team_instagram;?>"target="_blank"><i class="tms_social_font et-pb-icon">&#xe0b1;</i></a></li><?php } ?>
			</ul>
			<?php } ?>
		</div>
    </div>
</div>
<?php 
if( $display_popup_onteam =='on') {
	$divi_popupteam_select_style_layout = !empty(get_option('divi_popupteam_select_style_layout')) ? get_option('divi_popupteam_select_style_layout') : 'style1';
	if ( file_exists( $jv_grid_path_p . '/popup-'.$divi_popupteam_select_style_layout.'.php' ) ){
		 include $jv_grid_path_p. '/popup-'.$divi_popupteam_select_style_layout.'.php';
    }else{
		include JVTEAM_PLUGIN_PATH. '/content-popup/popup-'.$divi_popupteam_select_style_layout.'.php';
    }
    
} 
?>