<?php 
function team_members_sliders_modules() {
	class ET_Team_Members_Slider  extends ET_Builder_Module {
		function init() {
			$this->name 			=  esc_html__( 'Team Members Slider', 'jvteam' );
			$this->slug 			= 'et_pb_team_members_slider';
			$this->vb_support  		= 'partial';
			$this->main_css_element = '%%order_class%%.team_members_slider_main';
		}
		function get_settings_modal_toggles() {
			return array(
				'general'	=> 	array(
					'toggles'	=> 	array(
						'main_content'	=> 	esc_html__( 'Settings', 'et_builder' ),
						'display_onoff'	=> 	esc_html__( 'Display ON/OFF', 'et_builder' ),
						'color_setting'	=> 	esc_html__( 'Color', 'et_builder' ),
					),
				),
			);
		}
		function get_advanced_fields_config() {
			return  array(
				'fonts'	=> array(
					'team_name'	=> array(
						'label'		=> esc_html__( 'Name', 'et_builder' ),
						'css'		=> array(
							'line_height'	=> "{$this->main_css_element} .tms_title",
							'main'			=> "{$this->main_css_element} .tms_title",
							'important'		=> 'all',
						),
					),
					'designation_name_fonts'	=> array(
						'label'	=> esc_html__( 'Designation', 'et_builder' ),
						'css'	=> array(
							'main'	=> "{$this->main_css_element} .team_position",
						),
						'font_size'	=> array(
							'default'	=> '20px',
						)
					),
					'description_name_fonts'	=> array(
						'label'    	=> esc_html__( 'Description', 'et_builder' ),
						'css'     	=> array(
							'main' 	=> "{$this->main_css_element} .team_member_slider .team_description",
						),
						'font_size'	=> array(
							'default'		=> '15px',
						)
					),
				),
				'borders' => array(
					'default' => array(
						'css'      => array(
							'main' => array(
								'border_radii'	=> '%%order_class%%.team_members_slider_main .team_member_slider',
								'border_styles'	=> '%%order_class%%.team_members_slider_main .team_member_slider',
							),
						),
						'defaults' => array(
							'border_radii' 	=> 'on|0px|0px|0px|0px',
							'border_styles' => array(
									'width'		=> '1px',
									'color'		=> '#d9d9d9',
									'style'		=> 'solid',
							),
						),
					),					
				),
				'max_width'		=> false,
				'filters'	=> false,
				'background'	=> array(
					'use_background_image'	=>	false,
					'use_background_video'	=>	false,
				),
				'box_shadow' => array(
				'default' => array(
					'css' => array(
						'main' => "%%order_class%%.team_members_slider_main .team_member_slider",
					),
				),
			),
			);
		}
		function get_custom_css_fields_config() {
			return array(
				'teammembersslider_team_members_name' => array(
					'label'    => esc_html__( 'Name', 'et_builder' ),
					'selector' => '.tms_title',
				),
				'teammemberslider_team_members_designation' => array(
					'label'    => esc_html__( 'Designation', 'et_builder' ),
					'selector' => '.team_position',
				),
				'teammemberslider_team_members_description' => array(
					'label'    => esc_html__( 'Description', 'et_builder' ),
					'selector' => '.team_member_slider .team_description',
				),
				'teammemberslider_team_members_img' => array(
					'label'    => esc_html__( 'Image', 'et_builder' ),
					'selector' => '.team_member_slider .tms_img img',
				),
				'teammembersslider_team_members_bullet_color' => array(
					'label'    => esc_html__( 'Bullet Color', 'et_builder' ),
					'selector' => '.swiper-pagination-bullet',
				),
				'teammembersslider_team_members_bullet_active_color' => array(
					'label'    => esc_html__( 'Bullet Active Color ', 'et_builder' ),
					'selector' => '.swiper-pagination-bullet-active.swiper-pagination-bullet',
				),
				'teammembersslider_social_media_color' => array(
					'label'    => esc_html__( 'Social Media Color', 'et_builder' ),
					'selector' => '.tms_social_font.et-pb-icon',
				),
				'teammembersslider_social_media_hover_color' => array(
					'label'    => esc_html__( 'Social Media Hover Color ', 'et_builder' ),
					'selector' => '.tms_social_font.et-pb-icon:hover',
				),
			);
		}
			
		function get_fields() {
			$fields = array(
			/* 'select_style' => array(
				'label'           => esc_html__( 'Select Style', 'et_builder' ),
				'type'            => 'select',
				'option_category' => 'basic_option',
				'options'                => array(
				'style1'                 => esc_html__( 'Slider 1 Layout', 'et_builder' ),
				'style2'                 => esc_html__( 'Slider 2 Layout', 'et_builder' ),
				'style3'                 => esc_html__( 'Slider 3 Layout', 'et_builder' ),
				'style4'                 => esc_html__( 'Slider 4 Layout', 'et_builder' ),
				'style5'                 => esc_html__( 'Slider 5 Layout', 'et_builder' ),
				'style6'                 => esc_html__( 'Slider 6 Layout', 'et_builder' ),
				'style7'                 => esc_html__( 'Slider 7 Layout', 'et_builder' ),
				'style8'                 => esc_html__( 'Slider 8 Layout', 'et_builder' ),
				'style9'                 => esc_html__( 'Slider 9 Layout', 'et_builder' ),
				'style10'                => esc_html__( 'Slider 10 Layout', 'et_builder' ),
					), 
				'description'       => esc_html__( 'Here you can select style.', 'et_builder' ),
				'toggle_slug'     => 'main_content',
			), */
			'team_members_slider_display_category'	=> array(
					'label'			=> esc_html__( 'Display Team Members By Category', 'et_builder' ),
					'type'			=> 'select',
					'option_category'	=> 'basic_option',
					 'options'		=> array(
						'all'			=> esc_html__( 'All', 'et_builder' ),
						'specificcategory'	=> esc_html__( 'Specific Category', 'et_builder' ),
					), 
					'default'		=> 'all',
					'description'	=> esc_html__( 'Here you can select the category to display Team Members. ', 'et_builder' ),
					'toggle_slug'	=> 'main_content',
				), 
				'include_categories'	=> array(
					'label'			=> esc_html__( 'Specific Categories', 'et_builder' ),
					'renderer'		=> 'et_builder_include_categories_option',
					'option_category'	=> 'basic_option',
 					'show_if'			=> array('team_members_slider_display_category' => 'specificcategory'),
					'renderer_options'	=> array(
						'use_terms'		=> true,
						'term_name'		=> 'department_category',
					),
					'description'	=> esc_html__( 'Choose which categories you would like to display Team Members.', 'et_builder' ),
					'toggle_slug'	=> 'main_content',
				),
			 	'team_members_slider_count' => array(
					'label'			=> esc_html__( 'Display Number of Team Members(Count)', 'et_builder' ),
					'type'			=> 'text',
					'option_category'	=> 'basic_option',
					'default'		=> '5',
					'description'	=> esc_html__( 'Number of Team Members to be shown.', 'et_builder' ),
					'toggle_slug'	=> 'main_content',
				), 
				'orderby'	=> array(
					'label'			=> esc_html__( 'Order By', 'et_builder' ),
					'type'			=> 'select',
					'option_category'	=> 'configuration',
					'options'			=> array(
						'date_desc'		=> esc_html__( 'Date: new to old', 'et_builder' ),
						'date_asc'   	=> esc_html__( 'Date: old to new', 'et_builder' ),
						'title_asc' 	=> esc_html__( 'Title: a-z', 'et_builder' ),
						'title_desc' 	=> esc_html__( 'Title: z-a', 'et_builder' ),
						'menu_order_desc'	=> esc_html__( 'Menu Order : DESC', 'et_builder' ),
						'menu_order_asc'	=> esc_html__( 'Menu Order : ASC', 'et_builder' ),
						'rand'			=> esc_html__( 'Random', 'et_builder' ),
					),
					'default'		=> 'date_desc',
					'description'	=> esc_html__( 'Here you can adjust the order in which posts are displayed.', 'et_builder' ),
					'toggle_slug'	=> 'main_content',
				),
				'team_members_slider_speed'	=> array(
					'label'			=> esc_html__( 'Slide Speed (in ms)', 'et_builder' ),
					'type'			=> 'text',
					'option_category'	=> 'basic_option',
					'default'		=> '4000',
					'description'	=> esc_html__( 'Slide Speed (in ms).Default is 4000.', 'et_builder' ),
					'toggle_slug'	=> 'main_content',
				), 
				'display_popup_onteam' => array(
					'label'			=> esc_html__( 'Display Popup On Link', 'et_builder' ),
					'type'			=> 'yes_no_button',			
					'options'		=> array(
						'off'			=> esc_html__( 'No', 'et_builder' ),
						'on'  			=> esc_html__( 'yes', 'et_builder' ),
					),
					'default'  			=> 'off',
					'description'	=> esc_html__( 'This setting will turn on and off the display Popup on link.', 'et_builder' ),
					'toggle_slug'	=> 'display_onoff',
				),
				'team_members_slider_display_detail_page' => array(
					'label'			=> esc_html__( 'Display Detail Page Link', 'et_builder' ),
					'type'			=> 'yes_no_button',
					'show_if'		=> array('display_popup_onteam' => 'off'),
					'options'		=> array(
							'on'		=> esc_html__( 'yes', 'et_builder' ),
							'off' 	 	=> esc_html__( 'No', 'et_builder' ),
					),
 					'default'		=> 'on',
					'description'	=> esc_html__( 'This setting will turn on and off the display Detail Page Link.', 'et_builder' ),
					'toggle_slug'	=> 'display_onoff',
				),
				'team_members_slider_display_detail_page_link_type'	=> array(
					'label'			=> esc_html__( 'Display Detail Page Link Type', 'et_builder' ),
					'type'			=> 'select',
 					'show_if'		=> array('team_members_slider_display_detail_page' => 'on'),
					'option_category'	=> 'basic_option',
					'options'		=> array(
						'default'		=> esc_html__( 'Default', 'et_builder' ),
						'custom'		=> esc_html__( 'Custom', 'et_builder' ),
						), 
					'default'  			=> 'default',
					'description'	=> esc_html__( 'Here you can select the team member Display Detail Page Link Type.', 'et_builder' ),
					'toggle_slug'	=> 'display_onoff',
				),
				'link_open_in_new_tab'	=> array(
					'label'			=> esc_html__( 'Open In a New Tab', 'et_builder' ),
					'type'			=> 'yes_no_button',	
 					'show_if'		=> array('team_members_slider_display_detail_page' => 'on'),		
					'options' 		=> array(
						'off' 			=> esc_html__( 'No', 'et_builder' ),
						'on'  			=> esc_html__( 'yes', 'et_builder' ),
					),
					'default'		=> 'off',
					'description'	=> esc_html__( 'This setting will turn on and off the Open In a New Tab link.', 'et_builder' ),
					'toggle_slug'	=> 'display_onoff',
				),
				'team_members_slider_display_social_link' => array(
					'label'			=> esc_html__( 'Display Social Link', 'et_builder' ),
					'type'			=> 'yes_no_button',
					'option_category'	=> 'configuration',
					'options'		=> array(
						'on'  			=> esc_html__( 'yes', 'et_builder' ),
						'off' 			=> esc_html__( 'No', 'et_builder' ),
					),
					'default'		=> 'off',
					'description'	=> esc_html__( 'This setting will turn on and off the display social link.', 'et_builder' ),
					'toggle_slug'	=> 'display_onoff',
				),
				'display_items_slider_1921_above'	=> array(
					'label'			=> esc_html__( 'Display number of team members between 1921 to above device width per slide', 'et_builder' ),
					'type'			=> 'select',
					'option_category'	=> 'basic_option',
					'options'		=> array(
							'1'  		=> esc_html__( '1', 'et_builder' ),
							'2' 		=> esc_html__( '2', 'et_builder' ),
							'3' 		=> esc_html__( '3', 'et_builder' ),
							'4' 		=> esc_html__( '4', 'et_builder' ),
							'5'			=> esc_html__( '5', 'et_builder' ),
					),
					'default'		=> '4',
					'description'	=> esc_html__( 'Display number of team members between 1921 to above device width per slide', 'et_builder' ),
					'toggle_slug'	=> 'display_onoff',
				),
				'display_items_slider_1501to1920' => array(
					'label'			=> esc_html__( 'Display number of team members between 1501 to 1920 device width per slide', 'et_builder' ),
					'type'			=> 'select',
					'option_category'	=> 'basic_option',
					'options'		=> array(
							'1'  		=> esc_html__( '1', 'et_builder' ),
							'2'		 	=> esc_html__( '2', 'et_builder' ),
							'3' 		=> esc_html__( '3', 'et_builder' ),
							'4' 		=> esc_html__( '4', 'et_builder' ),
							'5' 		=> esc_html__( '5', 'et_builder' ),
					),
					'default'		=> '3',
					'description' 	=> esc_html__( 'Display number of team members between 1501 to 1920 device width per slide', 'et_builder' ),
					'toggle_slug'	=> 'display_onoff',
				),
				'display_items_slider_1201to1500'	 => array(
					'label'			=> esc_html__( 'Display number of team members between 1201 to 1500 device width per slide', 'et_builder' ),
					'type'			=> 'select',
					'option_category'	=> 'basic_option',
					'options'		=> array(
							'1'			=> esc_html__( '1', 'et_builder' ),
							'2' 		=> esc_html__( '2', 'et_builder' ),
							'3' 		=> esc_html__( '3', 'et_builder' ),
							'4' 		=> esc_html__( '4', 'et_builder' ),
					),
					'default'		=> '3',
					'description'	=> esc_html__( 'Display number of team members between 1201 to 1500 device width per slide', 'et_builder' ),
					'toggle_slug'	=> 'display_onoff',
				),
				'display_items_slider_981to1200'	 => array(
					'label'			=> esc_html__( 'Display number of team members between 981 to 1200 device width per slide', 'et_builder' ),
					'type'			=> 'select',
					'option_category'	=> 'basic_option',
					'options'		=> array(
							'1'			=> esc_html__( '1', 'et_builder' ),
							'2'			=> esc_html__( '2', 'et_builder' ),
							'3' 		=> esc_html__( '3', 'et_builder' ),
							'4' 		=> esc_html__( '4', 'et_builder' ),
					),
					'default'		=> '2',
					'description' 	=> esc_html__( 'Display number of team members between 981 to 1200 device width per slide', 'et_builder' ),
					'toggle_slug'	=> 'display_onoff',
				),
				'display_items_slider_768to980'	 => array(
					'label'			=> esc_html__( 'Display number of team members between 798 to 980 device width per slide', 'et_builder' ),
					'type'			=> 'select',
					'option_category'	=> 'basic_option',
					'options'		=> array(
							'1'  		=> esc_html__( '1', 'et_builder' ),
							'2' 		=> esc_html__( '2', 'et_builder' ),
							'3' 		=> esc_html__( '3', 'et_builder' ),
					),
					'default'		=> '2',
					'description' 	=> esc_html__( 'Display number of team members between 798 to 980 device width per slide', 'et_builder' ),
					'toggle_slug'	=> 'display_onoff',
				),
				'display_items_slider_767_below'	 => array(
					'label'			=> esc_html__( 'Display number of team members between 767 to below device width per slide', 'et_builder' ),
					'type'			=> 'select',
					'option_category'	=> 'basic_option',
					'options'		=> array(
							'1' 	 	=> esc_html__( '1', 'et_builder' ),
							'2' 		=> esc_html__( '2', 'et_builder' ),
					),
					'default'		=> '1',
					'description' 	=> esc_html__( 'Display number of team members between 767 to below device width per slide', 'et_builder' ),
					'toggle_slug'	=> 'display_onoff',
				),
				'content_bg_color' 	=> array(
					'label'        	=> esc_html__( 'Content Background Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
					'custom_color' 	=> true,
					'description' 	=> esc_html__( 'Use the color picker to choose a content background color for this module.', 'et_builder' ),
				),
				'content_bg_hover_color' => array(
					'label'        	=> esc_html__( 'Content Background Hover Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a content background hover color for this module.', 'et_builder' ),
				),
				'content_border_color' 	=> array(
					'label'        	=> esc_html__( 'Content Border Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a content border color for this module.', 'et_builder' ),
				),
				'social_media_color' 	=> array(
					'label'        	=> esc_html__( 'Social Media Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a social media color for this module.', 'et_builder' ),
				),
				'social_media_bg_hover_color'	 => array(
					'label'        	=> esc_html__( 'Social Media Background Hover Color', 'et_builder' ),
					'type'         	=> 'color-alpha',
					'custom_color' 	=> true,
					'description'  	=> esc_html__( 'Use the color picker to choose a social media background hover color for this module.', 'et_builder' ),
				),
				'team_members_slider_display_automatic_slide'         => array(
					'label'			=> esc_html__( 'Automatic Slide', 'et_builder' ),
					'type'			=> 'yes_no_button',
					'option_category' 	=> 'configuration',
					'options'		=> array(
						'on' 		 	=> esc_html__( 'yes', 'et_builder' ),
					    'off' 			=> esc_html__( 'No', 'et_builder' ),
					),
					'default'		=> 'on',
					'description'	=> esc_html__( 'This setting will turn on and off the Automatic Slide.', 'et_builder' ),
					'toggle_slug'	=> 'display_onoff',
				),
				'team_members_slider_display_automatic_slide_duration' => array(
					'label'			=> esc_html__( 'Slide Duration (in ms)', 'et_builder' ),
					'type'			=> 'text',
					'option_category'	=> 'basic_option',
 					'show_if'		=> array('team_members_slider_display_automatic_slide' => 'on'),
					'default'		=> '5000',
					'description'	=> esc_html__( 'Slide Duration (in ms).Default is 5000(hold slide for 2 seconds).', 'et_builder' ),
					'toggle_slug'	=> 'display_onoff',
				),
				'team_members_slider_display_dot_navigation' => array(
					'label'			=> esc_html__( 'Bullet Color', 'et_builder' ),
					'type'			=> 'color-alpha',
					'custom_color' 	=> true,
					'default'		=> 'rgba(230,126,34,0.68)',
					'toggle_slug'	=> 'color_setting',
				),
				'team_members_slider_display_active_dot_navigation' => array(
					'label'			=> esc_html__( ' Bullet Active Color', 'et_builder' ),
					'type'			=> 'color-alpha',
					'custom_color' 	=> true,
					'default'		=> '#e67e22',
					'toggle_slug'	=> 'color_setting',
				),
			);
			return $fields;
		}
		function render($attrs, $content = null, $render_slug ) {
		
		$display_popup_onteam      = $this->props['display_popup_onteam'];
		$divi_popupteam_select_style_layout = !empty(get_option('divi_popupteam_select_style_layout')) ? get_option('divi_popupteam_select_style_layout') : 'style1';

		if ( !is_admin()){
				wp_enqueue_style('content_archive_team_members_slider-css', JVTEAM_PLUGIN_URL . '/assets/css/archive_team_members_sliders_css/swiper.min.css', array(), NULL);
				wp_enqueue_script('content_archive_team_members_slider-js', JVTEAM_PLUGIN_URL . '/assets/js/swiper.min.js', array('jquery'), NULL, TRUE );
				
			if( $display_popup_onteam == 'on') {
				
				$jv_template_path_p =  get_stylesheet_directory() . '/divi-team-members';
				$jv_css_path_p = $jv_template_path_p.'/css/popup';
				$jv_css_url_p =  get_stylesheet_directory_uri().'/divi-team-members/css/popup'; 
				
				$jv_grid_path_p =  $jv_template_path_p.'/content-popup';
			
				if ( file_exists( $jv_css_path_p . '/popup-'.$divi_popupteam_select_style_layout.'.css' ) )
				{
					wp_enqueue_style('jv_team_module_members_popup_'.$divi_popupteam_select_style_layout, $jv_css_url_p . '/popup-'.$divi_popupteam_select_style_layout.'.css', array(), NULL);
				}else{
					wp_enqueue_style('jv_team_module_members_popup_'.$divi_popupteam_select_style_layout, JVTEAM_PLUGIN_URL .'assets/css/popup/popup-'.$divi_popupteam_select_style_layout.'.css', array(), NULL);	
				}
				
				wp_enqueue_style('jv_team_module_members_popup_custom', JVTEAM_PLUGIN_URL .'assets/css/popup/popup_custom.css', array(), NULL);		
				$divi_popupteam_select_style_layout = !empty(get_option('divi_popupteam_select_style_layout')) ? get_option('divi_popupteam_select_style_layout') : 'style1';
				$divi_team_close_background_color = !empty(get_option('divi_team_close_background_color')) ? get_option('divi_team_close_background_color') : '';
				$divi_team_close_color = !empty(get_option('divi_team_close_color')) ? get_option('divi_team_close_color') : ''; 
				$divi_popupteam_title_color = !empty(get_option('divi_popupteam_title_color')) ? get_option('divi_popupteam_title_color') : '';
				$divi_popupteam_position_color = !empty(get_option('divi_popupteam_position_color')) ? get_option('divi_popupteam_position_color') : '';
				$divi_popupteam_content_color = !empty(get_option('divi_popupteam_content_color')) ? get_option('divi_popupteam_content_color') : ''; 
				$divi_popupteam_information_color = !empty(get_option('divi_popupteam_information_color')) ? get_option('divi_popupteam_information_color') : '';
				$divi_popupteam_background_color = !empty(get_option('divi_popupteam_background_color')) ? get_option('divi_popupteam_background_color') : '';
				$divi_popupteam_border_width = !empty(get_option('divi_popupteam_border_width')) ? get_option('divi_popupteam_border_width') : '1'; 
				$divi_popupteam_border_color = !empty(get_option('divi_popupteam_border_color')) ? get_option('divi_popupteam_border_color') : '';
				$divi_popupteam_boxshadow_color = !empty(get_option('divi_popupteam_boxshadow_color')) ? get_option('divi_popupteam_boxshadow_color') : '';
				$divi_popupteam_icon_color = !empty(get_option('divi_popupteam_icon_color')) ? get_option('divi_popupteam_icon_color') : '';
				$divi_popupteam_horizontal_color = !empty(get_option('divi_popupteam_horizontal_color')) ? get_option('divi_popupteam_horizontal_color') : ''; 
				$divi_popupteam_contact_us_title = !empty(get_option('divi_popupteam_contact_us_title')) ? get_option('divi_popupteam_contact_us_title') : 'Drop Me a Line'; 
				
					if( $divi_popupteam_select_style_layout == 'style1'){
						  $popup_style1 = '';
						  if ( $divi_team_close_background_color != '' ){ $popup_style1 .= ".mfp-close-btn-in .jv_popup_style1 .mfp-close,.mfp-wrap .mfp-container .jv_popup_style1  button:hover{background-color: ".$divi_team_close_background_color." !important;}"; }
						  if ( $divi_team_close_color != '' ){ $popup_style1 .= ".mfp-close-btn-in .jv_popup_style1 .mfp-close,.mfp-wrap .mfp-container .jv_popup_style1  button:hover{color: ".$divi_team_close_color." !important;}"; }
						  if ( $divi_popupteam_background_color != '' ){ $popup_style1 .= ".jv_team_popup_style1{background-color: ".$divi_popupteam_background_color." !important;}"; }
						  if ( $divi_popupteam_title_color != '' ){  $popup_style1 .= ".jv_team_popup_style1 h4.jv_team_list_title{color: ".$divi_popupteam_title_color." !important;}"; }
						  if ( $divi_popupteam_position_color != '' ){ $popup_style1 .= ".jv_team_popup_style1 .jv_team_list_position{color: ".$divi_popupteam_position_color."  !important;}"; }
						  if ( $divi_popupteam_content_color != '' ){ $popup_style1 .= ".jv_team_popup_style1 .jv_team_list_content,.jv_team_popup_style1 .website_url,.jv_team_popup_style1 .contect_number,.jv_team_popup_style1 .email_address,.jv_team_popup_style1 .jv_team_list_address1{color: ".$divi_popupteam_content_color."  !important;}"; }
						  if ( $divi_popupteam_border_width != '' || $divi_popupteam_border_color !== ''){ $popup_style1 .= ".jv_team_popup_style1{border: ".$divi_popupteam_border_width."px solid ".$divi_popupteam_border_color." !important;}	"; }
						  if ( $divi_popupteam_icon_color != '' ){ $popup_style1 .= ".jv_team_popup_style1 .jv_team_member_icon_font.et-pb-icon,.jv_team_popup_style1 .jv_team_member_social_font.et-pb-icon{ color: ".$divi_popupteam_icon_color." !important;}"; }
						  if ( $divi_popupteam_information_color != '' ){ $popup_style1 .= ".jv_team_popup_style1 .website_url,.jv_team_popup_style1 .contect_number,.jv_team_popup_style1 .email_address,.jv_team_popup_style1 .jv_team_list_address1{color: ".$divi_popupteam_information_color." !important;}"; }
						  if ( $divi_popupteam_boxshadow_color != '' ){ $popup_style1 .= " .jv_team_popup_style1{ box-shadow: 0 20px 20px ".$divi_popupteam_boxshadow_color."  !important;}"; }
						  
						  wp_add_inline_style( 'jv_team_module_members_popup_custom', $popup_style1 );
					}
					
					if( $divi_popupteam_select_style_layout == 'style2'){
						  $popup_style2 = '';
						  if ( $divi_team_close_background_color != '' ){ $popup_style2 .= ".mfp-close-btn-in .jv_popup_style2 .mfp-close,.mfp-wrap .mfp-container .jv_popup_style2  button:hover{background-color: ".$divi_team_close_background_color." !important;}"; }
						  if ( $divi_team_close_color != '' ){ $popup_style2 .= ".mfp-close-btn-in .jv_popup_style2 .mfp-close,.mfp-wrap .mfp-container .jv_popup_style2  button:hover{color: ".$divi_team_close_color." !important;}"; }
						  if ( $divi_popupteam_background_color != '' ){ $popup_style2 .= ".jv_popup_style2 .et_pb_row{background-color: ".$divi_popupteam_background_color." !important;}"; }
						  if ( $divi_popupteam_title_color != '' ){  $popup_style2 .= ".jv_popup_style2 .jv_team_list_title h2{color: ".$divi_popupteam_title_color." !important;}"; }
						  if ( $divi_popupteam_position_color != '' ){ $popup_style2 .= ".jv_popup_style2 .jv_team_feture_box .jv_team_info h4{color: ".$divi_popupteam_position_color." !important;}"; }
						  if ( $divi_popupteam_content_color != '' ){ $popup_style2 .= ".jv_popup_style2 .jv_team_list_content{color: ".$divi_popupteam_content_color." !important;}"; }
						  if ( $divi_popupteam_icon_color != '' ){ $popup_style2 .= ".jv_popup_style2 .jv_team_member_info.et-pb-icon{ color: ".$divi_popupteam_icon_color."  !important;}.jv_popup_style2 .jv_team_list_title span,.jv_popup_style2 .jv_team_list_social_link{ background-color: ".$divi_popupteam_icon_color." !important;}"; }
						  if ( $divi_popupteam_information_color != '' ){ $popup_style2 .= ".jv_popup_style2 .jv_team_feture_box .jv_team_info span{color: ".$divi_popupteam_information_color." !important;}"; }
						  
						  wp_add_inline_style( 'jv_team_module_members_popup_custom', $popup_style2 );
					}
					
			}
		}
		    $select_style            = $this->props['select_style'];
			$module_id               = $this->props['module_id'];
			$module_class            = $this->props['module_class'];
			$posts_per_page = $this->props['team_members_slider_count'];
			$team_members_slider_display_category = $this->props['team_members_slider_display_category'];
			$include_categories = $this->props['include_categories'];
			$team_members_slider_display_detail_page = $this->props['team_members_slider_display_detail_page'];
			$team_members_slider_display_detail_page_link_type = $this->props['team_members_slider_display_detail_page_link_type'];
			$team_members_slider_display_social_link = $this->props['team_members_slider_display_social_link'];
			$team_members_slider_display_dot_navigation = $this->props['team_members_slider_display_dot_navigation'];	
			$content_bg_color = $this->props['content_bg_color'];
			$content_bg_hover_color = $this->props['content_bg_hover_color'];
			$content_border_color = $this->props['content_border_color'];
			$social_media_bg_hover_color = $this->props['social_media_bg_hover_color'];
			$social_media_color = $this->props['social_media_color'];
			$orderby                  = $this->props['orderby'];
			$link_open_in_new_tab      = $this->props['link_open_in_new_tab'];
			$team_members_slider_display_active_dot_navigation = $this->props['team_members_slider_display_active_dot_navigation'];
			$team_members_slider_speed = $this->props['team_members_slider_speed'];
			$team_members_slider_display_automatic_slide = $this->props['team_members_slider_display_automatic_slide'];
			$team_members_slider_display_automatic_slide_duration = $this->props['team_members_slider_display_automatic_slide_duration'];
			$this->content = et_builder_replace_code_content_entities( $this->content );
			$module_class = ET_Builder_Element::add_module_order_class( $module_class, $render_slug );
			$team_members_slider_speed_f  = ( '' !== $team_members_slider_speed ? $team_members_slider_speed : '300' );
			$display_automatic_slide_duration_f = '' !== $team_members_slider_display_automatic_slide_duration ? $team_members_slider_display_automatic_slide_duration : '2000';
			
			$display_items_slider_1921_above = $this->props['display_items_slider_1921_above'];	
			$display_items_slider_1501to1920 = $this->props['display_items_slider_1501to1920'];	
			$display_items_slider_1201to1500 = $this->props['display_items_slider_1201to1500'];	
			$display_items_slider_981to1200 = $this->props['display_items_slider_981to1200'];	
			$display_items_slider_768to980 = $this->props['display_items_slider_768to980'];	
			$display_items_slider_767_below = $this->props['display_items_slider_767_below'];	
			
			
			$module_id = ( '' !== $module_id ? sprintf( ' id="%1$s"', esc_attr( $module_id ) ) : '' );
			$module_class = ( '' !== $module_class ? sprintf( ' %1$s', esc_attr( $module_class ) ) : '' );
			
			if (  '' !== $team_members_slider_display_active_dot_navigation ) {
				ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%% .swiper-pagination-bullet-active.swiper-pagination-bullet',
					'declaration' => sprintf('background-color: %1$s;',esc_html( $team_members_slider_display_active_dot_navigation )),
				) );
			}
			if (  '' !== $team_members_slider_display_dot_navigation ) {
				ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%% .swiper-pagination-bullet',
					'declaration' => sprintf('background-color: %1$s;',esc_html( $team_members_slider_display_dot_navigation )),
				) );
			}
			$display_automatic_slide_duration = "";
			if ( $team_members_slider_display_automatic_slide == 'on' ){
				$display_automatic_slide_duration = 'autoplay : "'.$display_automatic_slide_duration_f.'",';
			} 
			
			if ( $display_items_slider_1921_above != '' ){ $items_slider_1921_above = $display_items_slider_1921_above; }else{$items_slider_1921_above = '4';} 
			if ( $display_items_slider_1501to1920 != '' ){ $items_slider_1501to1920 = $display_items_slider_1501to1920; }else{$items_slider_1501to1920 = '3';} 
			if ( $display_items_slider_1201to1500 != '' ){ $items_slider_1201to1500 = $display_items_slider_1201to1500; }else{$items_slider_1201to1500 = '3';} 
			if ( $display_items_slider_981to1200 != '' ){ $items_slider_981to1200 = $display_items_slider_981to1200; }else{$items_slider_981to1200 = '2';} 
			if ( $display_items_slider_768to980 != '' ){ $items_slider_768to980 = $display_items_slider_768to980; }else{$items_slider_768to980 = '1';} 
			if ( $display_items_slider_767_below != '' ){ $items_slider_767_below = $display_items_slider_767_below; }else{$items_slider_767_below = '1';} 
			
			
			if( $content_bg_color !='' ){
				ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%% .team_member_slider',
					'declaration' => sprintf('background: %1$s;',esc_html( $content_bg_color )),
				) );
			}
			if( $content_bg_hover_color !='' ){
				ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%% .team_member_slider:hover',
					'declaration' => sprintf('background: %1$s;',esc_html( $content_bg_hover_color )),
				) );
			}
			if( $content_border_color !='' ){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%% .tms_social_media',
						'declaration' => sprintf('border-top: 1px solid %1$s;',esc_html( $content_border_color )),
					) );
			}
			if( $team_members_slider_display_social_link =='off' ){
					ET_Builder_Element::set_style( $render_slug, array(
						'selector'    => '%%order_class%% .team_member_slider .team_discription.tms_content_bottom',
						'declaration' => sprintf('border-bottom: 4px solid %1$s;',esc_html( $content_border_color )),
					) );
			}
			if( $social_media_bg_hover_color !='' ){
				ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%% .tms_social_media li a:hover',
					'declaration' => sprintf('background: %1$s;border: 1px solid %1$s;',esc_html( $social_media_bg_hover_color )),
				) );
				ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%% .tms_social_media li a',
					'declaration' => sprintf('border: 1px solid %1$s;',esc_html( $social_media_bg_hover_color )),
				) );
			}
			if( $social_media_color !='' ){
				ET_Builder_Element::set_style( $render_slug, array(
					'selector'    => '%%order_class%% .tms_social_font.et-pb-icon',
					'declaration' => sprintf('color: %1$s;',esc_html( $social_media_color )),
				) );
			}
			
			
			$args = array(
				'post_type' => 'jv_team_members',
				'posts_per_page' => $posts_per_page 
			);
			
			if ( 'date_desc' !== $orderby ) {
				switch( $orderby ) {
					case 'date_asc' :
						$args['orderby'] = 'date';
						$args['order'] = 'ASC';
						break;
					case 'title_asc' :
						$args['orderby'] = 'title';
						$args['order'] = 'ASC';
						break;
					case 'title_desc' :
						$args['orderby'] = 'title';
						$args['order'] = 'DESC';
						break;
					case 'rand' :
						$args['orderby'] = 'rand';
						break;
					case 'menu_order_desc' :
						$args['orderby'] = 'menu_order';
						$args['order'] = 'DESC';
						break;
					case 'menu_order_asc' :
						$args['orderby'] = 'menu_order';
						$args['order'] = 'ASC';
						break;
				}
			}else{
				$args['orderby'] = 'date';
				$args['order'] = 'DESC';
			}
			
			$include_categories_array = explode(',',$include_categories);
			if ( $team_members_slider_display_category != 'all' ){
					$args['tax_query'] = array(
						array(
							'taxonomy' => 'department_category',
							'field' => 'term_id',
							'terms' => explode(",", $include_categories),
							'operator' => 'IN'
						)
					);
			}
			$output = '';
			$team_members_sliders = new WP_Query( $args );
				$rand =  "tms_swiper_".rand(10,500);
				// The Loop
				if ( $team_members_sliders->have_posts() ) {
				$jv_template_path =  get_stylesheet_directory() . '/divi-team-members';
				$jv_css_path = $jv_template_path.'/css/archive_team_members_sliders_css';
				$jv_css_url =  get_stylesheet_directory_uri().'/divi-team-members/css/archive_team_members_sliders_css'; 
				
				$jv_grid_path =  $jv_template_path.'/content-archive-team-members-slider';
				if ( file_exists( $jv_css_path . '/jv_team_module_member_'.$select_style.'.css' ) )
				{
					wp_enqueue_style('jv_team_members_slider', $jv_css_url . '/jv_team_members_slider.css', array(), NULL);
				}else{
					wp_enqueue_style('jv_team_members_slider', JVTEAM_PLUGIN_URL . '/assets/css/archive_team_members_sliders_css/jv_team_members_slider.css', array(), NULL);

				}
			
			$output .=  '<div '.$module_id.' class="'.$module_class.' team_members_slider_main">
			               <div class="swiper-container tms_container '.$rand.'" " data-tm1921="'.$items_slider_1921_above.'" data-tm1501="'.$items_slider_1501to1920.'" data-tm1201="'.$items_slider_1201to1500.'" data-tm981="'.$items_slider_981to1200.'" data-tm768="'.$items_slider_768to980.'"  data-tm767="'.$items_slider_767_below.'"  data-tmrand="'.$rand.'"  data-tmautomaticslide="'.$team_members_slider_display_automatic_slide.'" data-tmspeed="'.$team_members_slider_speed_f.'"  data-tmslideduration="'.$display_automatic_slide_duration_f.'" >
						   <div class="swiper-wrapper">';
					while ( $team_members_sliders->have_posts() ) { $team_members_sliders->the_post();
						ob_start();
					    
					   if ( file_exists( $jv_grid_path . '/content-team-slider.php' ) ){
					   		 include $jv_grid_path. '/content-team-slider.php';
					   }else{
					   		include JVTEAM_PLUGIN_PATH. '/content-archive-team-members-slider/content-team-slider.php';
					   }
						$output .= ob_get_contents();
						ob_end_clean();
					}
					$output .= '</div></div><div class="swiper-pagination tms_'.$rand.'  tms_pagination"></div></div>';
					$output .= " <script>
						jQuery(document).ready(function () {
								var swiper = new Swiper('.".$rand."', {
								pagination: '.tms_".$rand."',
								paginationClickable: true,
								slidesPerView: 4,
								spaceBetween: 50,
								roundLengths:true,
								".$display_automatic_slide_duration."
								speed : ".$team_members_slider_speed_f.",
								breakpoints: {
								    3500: {
										slidesPerView: ".$items_slider_1921_above.",
										spaceBetween: 40
									},
									1920: {
										slidesPerView: ".$items_slider_1501to1920.",
										spaceBetween: 40
									},
									1500: {
										slidesPerView: ".$items_slider_1201to1500.",
										spaceBetween: 30
									},
									1200: {
										slidesPerView: ".$items_slider_981to1200.",
										spaceBetween: 20
									},
									980: {
										slidesPerView:  ".$items_slider_768to980.",
										spaceBetween: 20
									},
									767: {
										slidesPerView: ".$items_slider_767_below.",
										spaceBetween: 10
									}
								}
							});
						});
						</script>";
					if( $display_popup_onteam =='on') {
							$output .= "<script>
							 jQuery(function ($) {
								$('a[href^=\"#teammodal\"]').addClass('et_smooth_scroll_disabled');
								$('.popup-modal').magnificPopup({
									type: 'inline',
									midClick: true,
									closeBtnInside: true
								});
							});
							 </script>";
					}
					wp_reset_postdata();
				}
				
			return $output;
		}
		protected function _add_additional_shadow_fields() {
			return false;
		}

		/*function process_box_shadow( $render_slug ) {
			return false;
		}		*/

	}
	new ET_Team_Members_Slider();
}
?>