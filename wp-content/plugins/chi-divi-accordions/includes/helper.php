<?php

// Die if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	die( 'Access denied.' );
}

/**
 * Modules helper methods.
 * 
 * Includes methodes used by both parent and child modules.
 *
 * @since   1.2.0 
 * 
 */
class Chiac_Divi_Modules_Helper {

    /**
     * Props.
     * 
     * @since   1.2.0
     * 
     */
    public $props = array();

    /**
     * Returns instance of the class.
     * 
     * @since   1.2.0
     * 
     */
	public static function instance(  ) {

		static $instance;
        return $instance ? $instance : $instance = new self(  );
        
	}

    /**
     * Constructor.
     */ 
	private function __construct(  ) {

    }
    
    /**
	 * Generates the responsive css.
	 *
	 * @uses	et_pb_responsive_options()->generate_responsive_css() 
	 * 			which is a copy of et_pb_generate_responsive_css() 
	 * 			with some modifications to improve.
	 *
	 * @since	1.2.0
	 * 
     * @param   array   $props                  Module props.
	 * @param 	string 	$render_slug    		Module slug.
	 * @param 	array  	$args					All arguments.
	 * 			string 	$args['setting']		Setting name.
	 * 			string  $args['selector']   	CSS selector.
	 * 			string 	$args['property']   	CSS property.
	 * 			string 	$args['additional_css']	Additional CSS.
	 * 			string 	$args['field_type']     Value type to determine need filter or not. Previously, it only
	 *                                			accept value from range control and run a function to process
	 *                                			range value.
	 * 			string 	$args['priority']       CSS style declaration priority.
	 */
	public function generate_responsive_css( $props, $render_slug, $args = [] ){

		$setting 		= isset ( $args['setting'] ) ? $args['setting'] : '';
		$css_selector 	= isset ( $args['selector'] ) ? $args['selector'] : '';
		$css_property 	= isset ( $args['property'] ) ? $args['property'] : '';
		$additional_css = isset ( $args['additional_css'] ) ? $args['additional_css'] . ';' : '';
		$type 			= isset ( $args['field_type'] ) ? $args['field_type'] : '';
		$priority 		= isset ( $args['priority'] ) ? $args['priority'] : '';

		// All device values
		$values_array = et_pb_responsive_options()->get_property_values( $props, $setting );

		// Generate responsive CSS
		et_pb_responsive_options()->generate_responsive_css( $values_array, $css_selector, $css_property, $render_slug, $additional_css, $type, $priority );

	}
    
    /**
	 * Generate responsive styles for margin and padding settings.
	 * 
	 * @todo	Merge this method with the generate_responsive_css().
	 * 
	 * @since	1.2.0
	 * 
     * @param   array   $props                  Module props.
	 * @param 	string 	$render_slug    		Module slug.
	 * @param 	array  	$args					All arguments.
	 * 			string 	$args['setting']		Setting name.
	 * 			string  $args['selector']   	CSS selector.
	 * 			string 	$args['property']   	CSS property.
	 * 			string 	$args['additional_css']	Additional CSS.
	 * 			string 	$args['field_type']     Value type to determine need filter or not. Previously, it only
	 *                                			accept value from range control and run a function to process
	 *                                			range value.
	 * 			string 	$args['priority']       CSS style declaration priority.
     *          string  $args['default']        Default value of the setting. Must be equal to the default value 
     *                                          specified for the setting. Needed to prevent the CSS from being
     *                                          generated with default values which normally should not happen.
     *                                          (Temporary fix for the possible bug in Divi API).
	 */
	public function generate_responsive_css_SPACING( $props, $render_slug, $args = [] ){

		$setting 		= isset ( $args['setting'] ) ? $args['setting'] : '';
		$css_selector 	= isset ( $args['selector'] ) ? $args['selector'] : '';
		$property 	    = isset ( $args['property'] ) ? $args['property'] : '';
		$additional_css = isset ( $args['additional_css'] ) ? $args['additional_css'] : '';
		$type 			= isset ( $args['field_type'] ) ? $args['field_type'] : '';
        $priority 		= isset ( $args['priority'] ) ? $args['priority'] : '';
        $default        = isset ( $args['default'] ) ? $args['default'] : '';
        
        // Explode the default value into array.
        $default    = explode( "|", $default );

		// Explode the string value into array for each device.
		$desktop    = explode( "|", $props[$setting] );
		$tablet     = explode( "|", $props["{$setting}_tablet"] );
		$phone	    = explode( "|", $props["{$setting}_phone"] );

		// Get the last edited device.
        $last_edited	= explode( "|", $props["{$setting}_last_edited"] );

		// Loop 4 times to generate CSS for each side (top, right, bottom, left).
		for( $i=0; $i<=3; $i++ ){

			// Get values for the current side for all devices.
			$_default	= isset( $default[$i] ) ? $default[$i] : '';
			$_desktop 	= isset( $desktop[$i] ) && $desktop[$i] !== $_default ? $desktop[$i] : '';
			$_tablet 	= isset( $tablet[$i] ) ? $tablet[$i] : '';
			$_phone 	= isset( $phone[$i] ) ? $phone[$i] : '';

			// Generate the CSS property for each side.
			switch($i){
				case 0:
					$side = 'top';
					break;
				case 1:
					$side = 'right';
					break;
				case 2:
					$side = 'bottom';
					break;
				case 3:
					$side = 'left';
					break;
			}
			$css_property = "{$property}-{$side}";

			if ( '' !== $_tablet || '' !== $_phone || '' !== $_desktop ) {

				// Check if responsive CSS is enabled.
				$responsive_active = et_pb_responsive_options()->get_responsive_status( $last_edited[0] );

				$values_array = array(
					'desktop' => $_desktop,
					'tablet'  => $responsive_active ? $_tablet : '',
					'phone'   => $responsive_active ? $_phone : '',
				);

				// Generate the responsive CSS for the current side.
				et_pb_responsive_options()->generate_responsive_css( $values_array, $css_selector, $css_property, $render_slug, $additional_css, $type, $priority );
			}

        }
        
	}

	/**
	 * Generate border radius responsive css.
	 * 
	 * Used by the child module as a TEMPORARY fix for the Divi API possible bug 
	 * where the borders option adds '!important' to the parent CSS but not the child CSS.
	 * This function is only needed to add '!important' to the child module CSS 
	 * to be able to override the parent CSS.
	 * 
	 * @link	https://github.com/elegantthemes/create-divi-extension/issues/277
	 * 
	 * @todo	Merge this method with the generate_responsive_css().
	 * 
	 * @since	1.2.0
	 * 
     * @param   array   $props                  Module props.
	 * @param 	string 	$render_slug    		Module slug.
	 * @param 	array  	$args					All arguments.
	 * 			string 	$args['setting']		Setting name.
	 * 			string  $args['selector']   	CSS selector.
	 * 			string 	$args['property']   	CSS property.
	 * 			string 	$args['additional_css']	Additional CSS.
	 * 			string 	$args['field_type']     Value type to determine need filter or not. Previously, it only
	 *                                			accept value from range control and run a function to process
	 *                                			range value.
	 * 			string 	$args['priority']       CSS style declaration priority.
	 * 
	 */
	public function generate_border_radius_responsive_css( $props, $render_slug, $args ){

        // Arguments
		$setting 		= isset ( $args['setting'] ) ? $args['setting'] : '';
		$css_selector 	= isset ( $args['selector'] ) ? $args['selector'] : '';
		$property 	    = isset ( $args['property'] ) ? $args['property'] : '';
		$additional_css = isset ( $args['additional_css'] ) ? $args['additional_css'] . ';' : '';
		$type 			= isset ( $args['field_type'] ) ? $args['field_type'] : '';
        $priority 		= isset ( $args['priority'] ) ? $args['priority'] : '';
        $default        = isset ( $args['default'] ) ? $args['default'] : '';

        // Prop prefix
		$prefix = 'border_radii';

		// Explode the BORDER RADIUS string value into array for each device.
		$desktop 	= explode( "|", $props["{$prefix}_{$setting}"] );
		$tablet 	= explode( "|", $props["{$prefix}_{$setting}_tablet"] );
		$phone		= explode( "|", $props["{$prefix}_{$setting}_phone"] );

		// Get the BORDER RADIUS last edited values(example: on|phone).
		$last_edited	= explode( "|", $props["{$prefix}_{$setting}_last_edited"] );

		// Loop 4 times(starting with 1) to generate CSS for each BORDER RADIUS corner (top-left, top-right, bottom-right, bottom-left).
		for( $i=1; $i<=4; $i++ ){

			// Get values for the current corner for all devices.
			$_desktop 	= isset( $desktop[$i] ) ? $desktop[$i] : '';
			$_tablet 	= isset( $tablet[$i] ) ? $tablet[$i] : '';
			$_phone 	= isset( $phone[$i] ) ? $phone[$i] : '';

			// Generate the CSS property for each side.
			switch($i){
				case 1:
					$css_property = 'border-top-left-radius';
					break;
				case 2:
					$css_property = 'border-top-right-radius';
					break;
				case 3:
					$css_property = 'border-bottom-right-radius';
					break;
				case 4:
					$css_property = 'border-bottom-left-radius';
					break;
			}

			if ( '' !== $_tablet || '' !== $_phone || '' !== $_desktop ) {

				// Check if responsive CSS is enabled.
				$responsive_active = et_pb_responsive_options()->get_responsive_status( $last_edited[0] );

				$values_array = array(
					'desktop' => $_desktop,
					'tablet'  => $responsive_active ? $_tablet : '',
					'phone'   => $responsive_active ? $_phone : '',
				);

				// Generate the responsive CSS for the current side.
				et_pb_responsive_options()->generate_responsive_css( $values_array, $css_selector, $css_property, $render_slug, $additional_css, $type, $priority );

			}

		}
	}
	
	/**
	 * Generate border styles responsive css.
     * 
     * Generates responsive CSS for the border-width, border-style and border-color properties.
	 * 
	 * Used by the child module as a TEMPORARY fix for the Divi API possible bug 
	 * where the borders option adds '!important' to the parent CSS but not the child CSS.
	 * This function is only needed to add '!important' to the child module CSS 
	 * to be able to override the parent CSS.
	 * 
	 * @link	https://github.com/elegantthemes/create-divi-extension/issues/277
	 * 
	 * @todo	Merge this method with the generate_responsive_css().
	 * 
	 * @since	1.2.0
	 * 
	 * @param   array   $props                  Module props.
	 * @param 	string 	$render_slug    		Module slug.
	 * @param 	array  	$args					All arguments.
	 * 			string 	$args['setting']		Setting name.
	 * 			string  $args['selector']   	CSS selector.
	 * 			string 	$args['property']   	CSS property.
	 * 			string 	$args['additional_css']	Additional CSS.
	 * 			string 	$args['field_type']     Value type to determine need filter or not. Previously, it only
	 *                                			accept value from range control and run a function to process
	 *                                			range value.
	 * 			string 	$args['priority']       CSS style declaration priority.
	 */
	public function generate_border_styles_responsive_css( $props, $render_slug, $args ){

        // Arguments.
        $setting    = isset( $args['setting'] ) ? $args['setting'] : '';
        $css_selector   = isset( $args['selector'] ) ? $args['selector'] : '';
        $property   = isset( $args['property'] ) ? $args['property'] : '';
        $additional_css  = isset( $args['additional_css'] ) ? $args['additional_css'] . ';' : '';
		$type 		= isset ( $args['field_type'] ) ? $args['field_type'] : '';
        $priority 	= isset ( $args['priority'] ) ? $args['priority'] : '';
        $default    = isset ( $args['default'] ) ? $args['default'] : '';

        // CSS property sides.
        $sides = array(
            'all'       => 'all',
            'top'       => 'top',
            'right'     => 'right',
            'bottom'    => 'bottom',
            'left'      => 'left'
        );

        // Border styles array.
        $styles = array(
            'border-width' => 'width',
            'border-color' => 'color',
            'border-style' => 'style'
        );

        foreach ( $styles as $style => $style_type ){
            
            // Generate the top + right + bottom + left CSS rules for current CSS property.
            foreach ($sides as $key => $side) {

                // Empty the values for each new side.
                $_desktop = '';
                $_tablet = '';
                $_phone = '';

                /*
                * Assemble the prop name to get its value.
                * 
                * Example of assembled prop name: border_width_top_chiac_header_tablet,
                * where 'width' is the $style_type, 'top' is the $side and 'chiac_header' is the $setting.
                */
                $desktop    = $props["border_{$style_type}_{$side}_{$setting}"];
                $tablet     = $props["border_{$style_type}_{$side}_{$setting}_tablet"];
                $phone      = $props["border_{$style_type}_{$side}_{$setting}_phone"];

                // Get the last edited values(example: on|tablet).
                $last_edited = explode( "|", $props["border_{$style_type}_{$side}_{$setting}_last_edited"] );

                // Get values for the current corner for all devices.
                $_desktop   = isset( $desktop ) ? "{$desktop}" : "";
                $_tablet    = isset( $tablet ) ? "{$tablet}" : "";
                $_phone     = isset( $phone ) ? "{$phone}" : "";

                // CSS property name (example: border-width or border-top-width).
                $css_property = 'all' === $side ? "border-{$style_type}" : "border-{$side}-{$style_type}";

                // Generate the responsive CSS for the current CSS property
                if ( '' !== $_tablet || '' !== $_phone || '' !== $_desktop ) {

                    // Check if responsive CSS is enabled.
                    $responsive_active = et_pb_responsive_options()->get_responsive_status( $last_edited[0] );

                    $values_array = array(
                        'desktop' => $_desktop,
                        'tablet'  => $responsive_active ? $_tablet : '',
                        'phone'   => $responsive_active ? $_phone : '',
                    );

                    // Generate the responsive CSS for the current side.
                    et_pb_responsive_options()->generate_responsive_css( $values_array, $css_selector, $css_property, $render_slug, $additional_css, $type, $priority );

                }

            }

        }
    
	}

	/**
	 * Generate border responsive CSS.
	 * 
	 * Used by the child module as a TEMPORARY fix for the Divi API possible bug 
	 * where the borders option adds '!important' to the parent CSS but not the child CSS.
	 * This function is only needed to add '!important' to the child module CSS 
	 * to be able to override the parent CSS.
	 * 
	 * @link	https://github.com/elegantthemes/create-divi-extension/issues/277
	 * 
	 * @todo	Merge this method with the generate_responsive_css().
	 * 
	 * @since	1.2.0
	 * 
	 * @param   array   $props                  Module props.
	 * @param 	string 	$render_slug    		Module slug.
	 * @param 	array  	$args					All arguments.
	 * 			string 	$args['setting']		Setting name.
	 * 			string  $args['selector']   	CSS selector.
	 * 			string 	$args['property']   	CSS property.
	 * 			string 	$args['additional_css']	Additional CSS.
	 * 			string 	$args['field_type']     Value type to determine need filter or not. Previously, it only
	 *                                			accept value from range control and run a function to process
	 *                                			range value.
	 * 			string 	$args['priority']       CSS style declaration priority.
	 */
	public function generate_border_responsive_css( $props, $render_slug, $args ){

		// Generate border radius responsive css.
		$this->generate_border_radius_responsive_css( $props, $render_slug, $args );

		// Generate border styles responsive css.
		$this->generate_border_styles_responsive_css( $props, $render_slug, $args );

	}

}

/**
 * Intstantiates the Chiac_Divi_Modules_Helper class.
 * 
 * @since   1.2.0
 * 
 * @param   array   Props array.
 * @return  Instance of the Chiac_Divi_Modules_Helper class.
 * 
 */
function chiac_modules_helper_methods() {
	return Chiac_Divi_Modules_Helper::instance();
}

