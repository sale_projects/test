<?php

class CHIAC_DiviAccordions extends DiviExtension {

	/**
	 * The gettext domain for the extension's translations.
	 *
	 * @since 1.0.0
	 *
	 * @var string
	 */
	public $gettext_domain = 'chiac-divi-accordions';

	/**
	 * The extension's WP Plugin name.
	 *
	 * @since 1.0.0
	 *
	 * @var string
	 */
	public $name = 'divi-accordions';

	/**
	 * The extension's version
	 *
	 * @since 1.0.0
	 *
	 * @var string
	 */
	public $version = '1.4.1';

	/**
	 * CHIAC_DiviAccordions constructor.
	 *
	 * @param string $name
	 * @param array  $args
	 */
	public function __construct( $name = 'divi-accordions', $args = array() ) {
		$this->plugin_dir     = plugin_dir_path( __FILE__ );
		$this->plugin_dir_url = plugin_dir_url( $this->plugin_dir );
        
        $this->_frontend_js_data = array( 
            'chiac_empty' => '',
        );

		parent::__construct( $name, $args ); 
        
        /**
         * Enqueue admin styles
         */
        add_action( 'admin_enqueue_scripts', array( $this, 'chiac_hook_enqueue_admin_styles' ), 15 );
        
	}
    
    /**
	 * Enqueues the extension's admin styles.
	 * {@see 'admin_enqueue_scripts'}
	 *
	 * @since 1.1.0
	 */
	public function chiac_hook_enqueue_admin_styles() {
        
        $styles     = et_is_builder_plugin_active() ? 'style-dbp' : 'style';
        $styles_url = "{$this->plugin_dir_url}styles/{$styles}.min.css";
        
        wp_enqueue_style( "{$this->name}-admin-styles", $styles_url, array(), CHIAC_VERSION );
        
    }
    
}

new CHIAC_DiviAccordions;
