<?php
/**
 * Child module / module item with FULL builder support
 *
 * @since 1.0.0
 */
class CHIAC_Divi_Accordions_Child extends ET_Builder_Module {
	// Module slug (also used as shortcode tag)
	public $slug                     = 'chiac_divi_accordions_item';
	// Module item has to use `child` as its type property
	public $type                     = 'child';
	// Module item's attribute that will be used for module item label on modal
	public $child_title_var          = 'title';
	// If the attribute defined on $this->child_title_var is empty, this attribute will be used instead
	public $child_title_fallback_var = 'subtitle';
	// Full Visual Builder support
	public $vb_support = 'on';
    // Left icon placeholder image
    public $left_image_icon_placeholder;
    // Right icon placeholder image
    public $right_image_icon_placeholder;
    
	/**
	 * Module properties initialization
	 *
	 * @since 1.0.0
	 */
	function init() {
		// Module name
		$this->name             = esc_html__( 'Divi Accordions Plus Item', 'chiac-divi-accordions' );
		// Default label for module item. Basically if $this->child_title_var and $this->child_title_fallback_var
		// attributes are empty, this default text will be used instead as item label
		$this->advanced_setting_title_text = esc_html__( 'Divi Accordions Plus Item', 'chiac-divi-accordions' );
		// Module item's modal title
		$this->settings_text = esc_html__( 'Accordions Item Settings', 'chiac-divi-accordions' );
        // left icon placeholder image url
        $this->left_image_icon_placeholder = plugins_url( '/chi-divi-accordions/images/left-image-placeholder.png' );
        // left icon placeholder image url
        $this->right_image_icon_placeholder = plugins_url( '/chi-divi-accordions/images/right-image-placeholder.png' );
        
		// Toggle settings
		$this->settings_modal_toggles  = array(
            // Content Tab
			'general'  => array(
				'toggles' => array(
					'main_content' => esc_html__( 'Content', 'chiac-divi-accordions' ),
                    'chiac_item_state' => esc_html__( 'Item State', 'chiac-divi-accordions' )
				),
			),
            // Design Tab
            'advanced'  => array(
				'toggles' => array(
					'chiac_header_toggle'   => esc_html__( 'Header', 'chiac-divi-accordions' ),
                    'chiac_left_icon'       => esc_html__( 'Left Icon', 'chiac-divi-accordions' ),
                    'chiac_right_icon'      => esc_html__( 'Right Icon', 'chiac-divi-accordions' ),
                    'chiac_title_toggle'    => esc_html__( 'Title', 'chiac-divi-accordions' ),
                    'chiac_body'            => esc_html__( 'Body', 'chiac-divi-accordions' ),
                    'chiac_header_text' => array(
						'title'    => esc_html__( 'Header Text', 'chiac-divi-accordions' ),
						'tabbed_subtoggles' => true,
						'sub_toggles' => array(
							'title' => array(
								'name' => 'Title',
							),
							'subtitle' => array(
								'name' => 'Subtitle',
							),
						),
					),
                    'chiac_content_heading' => array(
						'title'    => esc_html__( 'Body Headings', 'chiac-divi-accordions' ),
						//'priority' => 49,
						'tabbed_subtoggles' => true,
						'sub_toggles' => array(
							'h1' => array(
								'name' => 'H1',
								'icon' => 'text-h1',
							),
							'h2' => array(
								'name' => 'H2',
								'icon' => 'text-h2',
							),
							'h3' => array(
								'name' => 'H3',
								'icon' => 'text-h3',
							),
							'h4' => array(
								'name' => 'H4',
								'icon' => 'text-h4',
							),
							'h5' => array(
								'name' => 'H5',
								'icon' => 'text-h5',
							),
							'h6' => array(
								'name' => 'H6',
								'icon' => 'text-h6',
							),
						),
					),
                    'chiac_content_text'   => array(
						'title' => esc_html__( 'Body Text', 'chiac-divi-accordions' ),
						//'priority' => 50,
						// Groups can be organized into tab
						'tabbed_subtoggles' => true,
						// Subtoggle tab configuration. Add `sub_toggle` attribute on field to put them here
						'sub_toggles' => array(
							'p'     => array(
								'name' => 'P',
								'icon' => 'text-left',
							),
							'a'     => array(
								'name' => 'A',
								'icon' => 'text-link',
							),
							'ul'    => array(
								'name' => 'UL',
								'icon' => 'list',
							),
							'ol'    => array(
								'name' => 'OL',
								'icon' => 'numbered-list',
							),
							'quote' => array(
								'name' => 'QUOTE',
								'icon' => 'text-quote',
							),
						),
					),
                    'item_borders'          => esc_html__( 'Borders', 'chiac-divi-accordions' ),
                    'item_box_shadow'       => esc_html__( 'Box Shadow', 'chiac-divi-accordions' ),
				),
			),
		); 
	}
    
    // accordion item default title text
    public $title_default_text = 'Your Title Goes Here';
    // accordion item default subtitle text
    public $subtitle_default_text = 'Your Subtitle Goes Here';
    
	/**
	 * Module's specific fields
	 *
	 * @since 1.0.0
	 *
	 * @return array
	 */
	function get_fields() {
		return array(
			'title' => array(
				'label'           => esc_html__( 'Title', 'chiac-divi-accordions' ),
				'type'            => 'text',
				'option_category' => 'basic_option',
				'description'     => esc_html__( 'Text entered here will appear as accordion item title', 'chiac-divi-accordions' ),
                'default'         => esc_html__( $this->title_default_text , 'chiac-divi-accordions' ),
				'toggle_slug'     => 'main_content',
			),
			'chiac_subtitle_state' => array(
				'label'             => esc_html__( 'Use Subtitle', 'chiac-divi-accordions' ),
                'description'       => esc_html__( 'Enable or disable subtitle', 'chiac-divi-accordions' ),
				'type'              => 'yes_no_button',
				'options'           => array(
					'on'  => esc_html__( 'On', 'chiac-divi-accordions' ),
					'off' => esc_html__( 'Off', 'chiac-divi-accordions' ),
				),
                'default'         => 'on',
				'tab_slug'        => 'general',
				'toggle_slug'     => 'main_content',
			),
			'chiac_subtitle_visibility' => array(
				'label'           => esc_html__( 'Hide Subtitle on', 'chiac-divi-accordions' ),
				'type'            => 'multiple_checkboxes',
				'options'         => array(
					'desktop'      => esc_html__( 'Desktop', 'chiac-divi-accordions' ),
					'tablet'       => esc_html__( 'Tablet', 'chiac-divi-accordions' ),
					'phone'        => esc_html__( 'Phone', 'chiac-divi-accordions' ),
				),
                'description'     => esc_html__('Choose which devices you would like to hide the subtitle on', 'chiac-divi-accordions'),
                'show_if'         => array(
                    'chiac_subtitle_state' => 'on',
                ),
				'tab_slug'        => 'custom_css',
				'toggle_slug'     => 'visibility',
			), 
			'subtitle' => array(
				'label'           => esc_html__( 'Subtitle', 'chiac-divi-accordions' ),
				'type'            => 'text',
				'option_category' => 'basic_option',
				'description'     => esc_html__( 'Text entered here will appear as accordion item subtitle', 'chiac-divi-accordions' ),
                'default'         => esc_html__( $this->subtitle_default_text , 'chiac-divi-accordions' ),
                'show_if'         => array(
                    'chiac_subtitle_state'    => 'on'
                ),
				'tab_slug'        => 'general',
				'toggle_slug'     => 'main_content',
			),
			'content' => array(
				'label'           => esc_html__( 'Body Content', 'chiac-divi-accordions' ),
				'type'            => 'tiny_mce',
				'option_category' => 'basic_option',
				'description'     => esc_html__( 'Content entered here will appear inside the accordion item body', 'chiac-divi-accordions' ),
				'toggle_slug'     => 'main_content',
			),
            'chiac_header_bg_color' => array(
				'label'           => esc_html__( 'Background Color', 'chiac-divi-accordions' ),
				'type'            => 'color-alpha',
				'option_category' => 'basic_option',
                'mobile_options'  => true,
                'responsive'      => true,
                'custom_color'    => true,
                'description'     => esc_html__('Set background color for the accordion item header', 'chiac-divi-accordions'),
                'default'         => '',
				'tab_slug'        => 'advanced',
				'toggle_slug'     => 'chiac_header_toggle',
			),
            'chiac_header_bg_color_o' => array(
				'label'           => esc_html__( 'Background Color(Open Toggle)', 'chiac-divi-accordions' ),
				'type'            => 'color-alpha',
				'option_category' => 'basic_option',
                'mobile_options'  => true,
                'responsive'      => true,
                'custom_color'    => true,
                'description'     => esc_html__('Set header background color for open toggle', 'chiac-divi-accordions'),
                'default'         => '',
				'tab_slug'        => 'advanced',
				'toggle_slug'     => 'chiac_header_toggle',
			),
            'chiac_header_margin' => array(
                'label'             => esc_html__('Header Margin', 'chiac-divi-accordions'),
                'type'              => 'custom_margin',
                'tab_slug'          => 'advanced',
                'toggle_slug'       => 'chiac_header_toggle',
                'mobile_options'    => true,
                'responsive'        => true,
//                'default'           => '0px',
                'default_unit'      => 'px',
            ),
            'chiac_header_padding' => array(
                'label'             => esc_html__('Header Padding', 'chiac-divi-accordions'),
                'type'              => 'custom_padding',
                'tab_slug'          => 'advanced',
                'toggle_slug'       => 'chiac_header_toggle',
                'mobile_options'    => true,
                'responsive'        => true,
//                'default'           => '0px',
                'default_unit'      => 'px',
            ),
            'chiac_title_bg_color' => array(
				'label'           => esc_html__( 'Background Color', 'chiac-divi-accordions' ),
				'type'            => 'color-alpha',
				'option_category' => 'basic_option',
                'mobile_options'  => true,
                'responsive'      => true,
                'custom_color'    => true,
                'description'     => esc_html__('Set background color for the accordion item header title', 'chiac-divi-accordions'),
                'default'         => '',
				'tab_slug'        => 'advanced',
				'toggle_slug'     => 'chiac_title_toggle',
			),
            'chiac_title_bg_color_o' => array(
				'label'           => esc_html__( 'Background Color(Open Toggle)', 'chiac-divi-accordions' ),
				'type'            => 'color-alpha',
				'option_category' => 'basic_option',
                'mobile_options'  => true,
                'responsive'      => true,
                'custom_color'    => true,
                'description'     => esc_html__('Set header title background color for open toggle', 'chiac-divi-accordions'),
                'default'         => '',
				'tab_slug'        => 'advanced',
				'toggle_slug'     => 'chiac_title_toggle',
			),
            'chiac_title_margin' => array(
                'label'             => esc_html__('Title Margin', 'chiac-divi-accordions'),
				'description'     	=> esc_html__( 'Margin adds extra space to the outside of the element, increasing the distance between the element and other items on the page.', 'chiac-divi-accordions' ),
                'type'              => 'custom_margin',
				'option_category' 	=> 'layout',
                'mobile_options'    => true,
                'responsive'        => true,
//                'default'           => '0px|0px|0px|0px|false|false',
                'default_unit'      => 'px',
                'tab_slug'          => 'advanced',
                'toggle_slug'       => 'chiac_title_toggle',
            ),
            'chiac_title_padding' => array(
                'label'             => esc_html__('Title Padding', 'chiac-divi-accordions'),
				'description'     	=> esc_html__( 'Padding adds extra space to the inside of the element, increasing the distance between the edge of the element and its inner contents.', 'chiac-divi-accordions' ),
                'type'              => 'custom_padding',
				'option_category' 	=> 'layout',
                'mobile_options'    => true,
                'responsive'        => true,
				'default'           => '8px|20px|8px|20px|false|false',
                'default_unit'      => 'px',
                'tab_slug'          => 'advanced',
                'toggle_slug'       => 'chiac_title_toggle',
            ),
            'chiac_body_bg_color' => array(
				'label'           => esc_html__( 'Background Color', 'chiac-divi-accordions' ),
				'type'            => 'color-alpha',
				'option_category' => 'basic_option',
                'mobile_options'  => true,
                'responsive'      => true,
                'custom_color'    => true,
                'description'     => esc_html__('Set background color for the accordion item body(content)', 'chiac-divi-accordions'),
                'default'         => '',
				'tab_slug'        => 'advanced',
				'toggle_slug'     => 'chiac_body',
			),
            'chiac_body_margin' => array(
                'label'             => esc_html__('Body Margin', 'chiac-divi-accordions'),
				'description'     	=> esc_html__( 'Margin adds extra space to the outside of the element, increasing the distance between the element and other items on the page.', 'chiac-divi-accordions' ),
                'type'              => 'custom_margin',
				'option_category' 	=> 'layout',
                'mobile_options'    => true,
                'responsive'        => true,
//                'default'           => '0px|0px|0px|0px|false|false',
                'default_unit'      => 'px',
                'tab_slug'          => 'advanced',
                'toggle_slug'       => 'chiac_body',
            ),
            'chiac_body_padding' => array(
                'label'             => esc_html__('Body Padding', 'chiac-divi-accordions'),
				'description'     	=> esc_html__( 'Padding adds extra space to the inside of the element, increasing the distance between the edge of the element and its inner contents.', 'chiac-divi-accordions' ),
                'type'              => 'custom_padding',
				'option_category' 	=> 'layout',
                'mobile_options'    => true,
                'responsive'        => true,
                'default'           => '20px|20px|20px|20px|false|false',
                'default_unit'      => 'px',
                'tab_slug'          => 'advanced',
                'toggle_slug'       => 'chiac_body',
            ),
			'chiac_ul_type' => array(
				'label'             => esc_html__( 'Unordered List Style Type', 'chiac-divi-accordions' ),
                'description'      => esc_html__( 'This setting adjusts the shape of the bullet point that begins each list item.', 'chiac-divi-accordions' ),
				'type'              => 'select',
				'option_category'   => 'configuration',
				'options'           => array(
					'disc'    => esc_html__( 'Disc', 'chiac-divi-accordions' ),
					'circle'  => esc_html__( 'Circle', 'chiac-divi-accordions' ),
					'square'  => esc_html__( 'Square', 'chiac-divi-accordions' ),
					'none'    => esc_html__( 'None', 'chiac-divi-accordions' ),
				),
				'priority'          => 80,
				'default'           => 'disc',
				'default_on_front'  => '',
				'tab_slug'          => 'advanced',
				'toggle_slug'       => 'chiac_content_text',
                'sub_toggle'        => 'ul',
                'mobile_options'    => true,
			),
			'chiac_ul_position' => array(
				'label'             => esc_html__( 'Unordered List Style Position', 'chiac-divi-accordions' ),
                'description'      => esc_html__( 'The bullet point that begins each list item can be placed either inside or outside the parent list wrapper. Placing list items inside will indent them further within the list.', 'chiac-divi-accordions' ),
				'type'              => 'select',
				'option_category'   => 'configuration',
				'options'           => array(
					'outside' => esc_html__( 'Outside', 'chiac-divi-accordions' ),
					'inside'  => esc_html__( 'Inside', 'chiac-divi-accordions' ),
				),
				'priority'          => 85,
				'default'           => 'outside',
				'default_on_front'  => '',
				'tab_slug'          => 'advanced',
				'toggle_slug'       => 'chiac_content_text',
				'sub_toggle'        => 'ul',
                'mobile_options'    => true,
			),
			'chiac_ul_item_indent' => array(
				'label'           => esc_html__( 'Unordered List Item Indent', 'chiac-divi-accordions' ),
                'description'     => esc_html__( 'Increasing indentation will push list items further towards the center of the text content, giving the list more visible separation from the the rest of the text.', 'et_builder' ),
				'type'            => 'range',
				'option_category' => 'configuration',
				'tab_slug'        => 'advanced',
				'toggle_slug'     => 'chiac_content_text',
				'sub_toggle'      => 'ul',
				'priority'        => 90,
				'default'         => '0px',
				'default_unit'    => 'px',
				'default_on_front' => '',
				'range_settings'  => array(
					'min'  => '0',
					'max'  => '100',
					'step' => '1',
				),
                'mobile_options'   => true,
            ),
			'chiac_ol_type' => array(
				'label'             => esc_html__( 'Ordered List Style Type', 'chiac-divi-accordions' ),
                'description'      => esc_html__( 'Here you can choose which types of characters are used to distinguish between each item in the ordered list.', 'chiac-divi-accordions' ),
				'type'              => 'select',
				'option_category'   => 'configuration',
				'options'           => array(
					'decimal'              => 'decimal',
					'armenian'             => 'armenian',
					'cjk-ideographic'      => 'cjk-ideographic',
					'decimal-leading-zero' => 'decimal-leading-zero',
					'georgian'             => 'georgian',
					'hebrew'               => 'hebrew',
					'hiragana'             => 'hiragana',
					'hiragana-iroha'       => 'hiragana-iroha',
					'katakana'             => 'katakana',
					'katakana-iroha'       => 'katakana-iroha',
					'lower-alpha'          => 'lower-alpha',
					'lower-greek'          => 'lower-greek',
					'lower-latin'          => 'lower-latin',
					'lower-roman'          => 'lower-roman',
					'upper-alpha'          => 'upper-alpha',
					'upper-greek'          => 'upper-greek',
					'upper-latin'          => 'upper-latin',
					'upper-roman'          => 'upper-roman',
					'none'                 => 'none',
				),
				'priority'          => 80,
				'default'           => 'decimal',
				'default_on_front' => '',
				'tab_slug'          => 'advanced',
				'toggle_slug'       => 'chiac_content_text',
				'sub_toggle'        => 'ol',
                'mobile_options'   => true,
			),
			'chiac_ol_position' => array(
				'label'             => esc_html__( 'Ordered List Style Position', 'chiac-divi-accordions' ),
                'description'      => esc_html__( 'The characters that begins each list item can be placed either inside or outside the parent list wrapper. Placing list items inside will indent them further within the list.', 'chiac-divi-accordions' ),
				'type'              => 'select',
				'option_category'   => 'configuration',
				'options'           => array(
					'inside'  => esc_html__( 'Inside', 'chiac-divi-accordions' ),
					'outside' => esc_html__( 'Outside', 'chiac-divi-accordions' ),
				),
				'priority'          => 85,
				'default'           => 'inside',
				'default_on_front' => '',
				'tab_slug'          => 'advanced',
				'toggle_slug'       => 'chiac_content_text',
				'sub_toggle'        => 'ol',
                'mobile_options'   => true,
			),
			'chiac_ol_item_indent' => array(
				'label'           => esc_html__( 'Ordered List Item Indent', 'chiac-divi-accordions' ),
                'description'      => esc_html__( 'Increasing indentation will push list items further towards the center of the text content, giving the list more visible separation from the the rest of the text.', 'chiac-divi-accordions' ),
				'type'            => 'range',
				'option_category' => 'configuration',
				'tab_slug'        => 'advanced',
				'toggle_slug'     => 'chiac_content_text',
				'sub_toggle'      => 'ol',
				'priority'        => 90,
				'default'         => '0px',
				'default_unit'    => 'px',
				'default_on_front' => '',
				'range_settings'  => array(
					'min'  => '0',
					'max'  => '100',
					'step' => '1',
				),
                'mobile_options'   => true,
            ),
			'chiac_quote_border_width' => array(
				'label'           => esc_html__( 'Blockquote Border Weight', 'chiac-divi-accordions' ),
                'description'      => esc_html__( 'Block quotes are given a border to separate them from normal text. You can increase or decrease the size of that border using this setting.', 'chiac-divi-accordions' ),
				'type'            => 'range',
				'option_category' => 'configuration',
				'tab_slug'        => 'advanced',
				'toggle_slug'     => 'chiac_content_text',
				'sub_toggle'      => 'quote',
				'priority'        => 85,
				'default'         => '5px',
				'default_unit'    => 'px',
				'default_on_front' => '',
				'range_settings'  => array(
					'min'  => '0',
					'max'  => '100',
					'step' => '1',
				),
                'mobile_options'   => true,
                // 'hover'            => 'tabs',
			),
			'chiac_quote_border_color' => array(
				'label'           => esc_html__( 'Blockquote Border Color', 'chiac-divi-accordions' ),
                'description'     => esc_html__( 'Block quotes are given a border to separate them from normal text. Pick a color to use for that border.', 'chiac-divi-accordions' ),
				'type'            => 'color-alpha',
				'option_category' => 'configuration',
				'custom_color'    => true,
				'tab_slug'        => 'advanced',
				'toggle_slug'     => 'chiac_content_text',
				'sub_toggle'      => 'quote',
				'field_template'  => 'color',
				'priority'        => 90,
                'mobile_options'  => true,
                // 'hover'           => 'tabs',
            ),
			'chiac_l_icon_state' => array(
				'label'             => esc_html__( 'Use Left Icon', 'chiac-divi-accordions' ),
                'description'       => esc_html__( 'Enable or disable left icon', 'chiac-divi-accordions' ),
				'type'              => 'yes_no_button',
				'options'           => array(
					'on'  => esc_html__( 'On', 'chiac-divi-accordions' ),
					'off' => esc_html__( 'Off', 'chiac-divi-accordions' ),
				),
                'default'         => 'on',
				'tab_slug'        => 'advanced',
				'toggle_slug'     => 'chiac_left_icon',
			),
			'chiac_l_icon_rotate' => array(
				'label'             => esc_html__( 'Rotate Left Icon(Open Toggle)', 'chiac-divi-accordions' ),
                'description'       => esc_html__( 'Enable rotation of left icon when the toggle is open. Useful for "arrow" icons', 'chiac-divi-accordions' ),
				'type'              => 'yes_no_button',
				'options'           => array(
					'on'  => esc_html__( 'On', 'chiac-divi-accordions' ),
					'off' => esc_html__( 'Off', 'chiac-divi-accordions' ),
				),
                'default'         => 'off',
                'show_if'         => array(
                    'chiac_l_icon_state' => 'on',
                    ),
				'tab_slug'        => 'advanced',
				'toggle_slug'     => 'chiac_left_icon',
			),
			'chiac_l_icon_rotate_degree' => array(
				'label'           => esc_html__( 'Left Icon Rotation Degree', 'chiac-divi-accordions' ),
				'type'            => 'range',
				'option_category' => 'configuration',
				'tab_slug'        => 'advanced',
				'toggle_slug'     => 'chiac_left_icon',
				'default'         => '180deg',
				'default_unit'    => 'deg',
				'default_on_front' => '',
                'description'     => esc_html__('Set left icon rotation degree', 'chiac-divi-accordions'),
				'range_settings'  => array(
					'min'  => '-360',
					'max'  => '360',
					'step' => '15',
				),
                'show_if'         => array(
                    'chiac_l_icon_state'    => 'on',
                    'chiac_l_icon_rotate'   => 'on'
                ),
			),
            'chiac_icon_or_image' => array(
                'label'             => esc_html__( 'Use Font Icon or Image', 'chiac-divi-accordions' ),
                'type'              => 'select',
                'option_category'   => 'configuration',
                'options'           => array(
                    'fonticon'    => esc_html__( 'Font Icon', 'chiac-divi-accordions' ),
                    'image'       => esc_html__( 'Image', 'chiac-divi-accordions' ),
                ),
                'default'           => 'fonticon',
                'default_on_front'  => 'fonticon',
                'show_if'             => array(
                        'chiac_l_icon_state' => 'on',
                        ),
                'tab_slug'          => 'advanced',
                'toggle_slug'       => 'chiac_left_icon',
            ),
			'chiac_select_fonticon' => array(
				'label'               => esc_html__( 'Select Icon', 'chiac-divi-accordions' ),
				'type'                => 'select_icon',
				'renderer'            => 'select_icon',
				'renderer_with_field' => true,
                'default'             => '%%99%%',
                'show_if'             => array(
                        'chiac_l_icon_state'    => 'on',
                        'chiac_icon_or_image'   => 'fonticon',
                        ),
                'description'         => esc_html__('Select left icon', 'chiac-divi-accordions'),
				'tab_slug'        => 'advanced',
				'toggle_slug'     => 'chiac_left_icon',
			),
			'chiac_l_icon_size' => array(
				'label'           => esc_html__( 'Left Icon Size', 'chiac-divi-accordions' ),
				'type'            => 'range',
				'option_category' => 'configuration',
                'mobile_options'    => true,
                'responsive'        => true,
				'default'         => '20px',
				'default_unit'    => 'px',
				'default_on_front' => '',
                'description'     => esc_html__('Set left icon font size', 'chiac-divi-accordions'),
				'range_settings'  => array(
					'min'  => '0',
					'max'  => '100',
					'step' => '1',
				),
                'show_if'         => array(
                    'chiac_l_icon_state' => 'on',
                    'chiac_icon_or_image'   => 'fonticon',
                ),
				'tab_slug'        => 'advanced',
				'toggle_slug'     => 'chiac_left_icon',
			),
            'chiac_left_image' => array(
                'label'              => esc_html__( 'Upload Image', 'chiac-divi-accordions' ),
                'type'               => 'upload',
                'upload_button_text' => esc_attr__( 'Upload an image', 'chiac-divi-accordions' ),
                'choose_text'        => esc_attr__( 'Choose an Image', 'chiac-divi-accordions' ),
                'update_text'        => esc_attr__( 'Set As Image', 'chiac-divi-accordions' ),
                'description'        => esc_html__('Upload the left icon image(png or jpg)', 'chiac-divi-accordions'),
                'default'            => $this->left_image_icon_placeholder,
                'show_if'            => array(
                        'chiac_l_icon_state' => 'on',
                        'chiac_icon_or_image' => 'image',
                        ),
                'tab_slug'           => 'advanced',
                'toggle_slug'        => 'chiac_left_icon',
            ),
            'chiac_l_image_width' => array(
                'label'             => esc_html__( 'Image Width', 'chiac-divi-accordions' ),
                'type'              => 'range',
                'option_category'   => 'configuration',
                'mobile_options'    => true,
                'responsive'        => true,
                'default'           => '',
                'default_unit'      => 'px',
                'default_on_front'  => '',
                'description'       => esc_html__('Set width of the left icon image', 'chiac-divi-accordions'),
                'range_settings'    => array(
                    'min'  => '0',
                    'max'  => '300',
                    'step' => '1',
                ),
                'show_if'         => array(
                    'chiac_l_icon_state' => 'on',
                    'chiac_icon_or_image'   => 'image',
                ),
                'tab_slug'        => 'advanced',
                'toggle_slug'     => 'chiac_left_icon',
            ),
            'chiac_l_icon_color'  => array(
				'label'           => esc_html__( 'Left Icon Color', 'chiac-divi-accordions' ),
				'type'            => 'color-alpha',
				'option_category' => 'basic_option',
                'mobile_options'  => true,
                'responsive'      => true,
				'custom_color'    => true,
                'description'     => esc_html__('Set left icon color.', 'chiac-divi-accordions'),
                'default'         => '',
                'show_if'         => array(
                        'chiac_l_icon_state' => 'on',
                        'chiac_icon_or_image'   => 'fonticon',
                        ),
				'tab_slug'        => 'advanced',
				'toggle_slug'     => 'chiac_left_icon',
			),
            'chiac_l_icon_color_o'  => array(
				'label'           => esc_html__( 'Left Icon Color(Open Toggle)', 'chiac-divi-accordions' ),
				'type'            => 'color-alpha',
				'option_category' => 'basic_option',
                'mobile_options'  => true,
                'responsive'      => true,
				'custom_color'    => true,
                'description'     => esc_html__('Set left icon color for open toggle.', 'chiac-divi-accordions'),
                'default'         => '',
                'show_if'         => array(
                        'chiac_l_icon_state' => 'on',
                        'chiac_icon_or_image'   => 'fonticon',
                        ),
				'tab_slug'        => 'advanced',
				'toggle_slug'     => 'chiac_left_icon',
			),
            'chiac_l_icon_bg_color'  => array(
				'label'           => esc_html__( 'Left Icon Background Color', 'chiac-divi-accordions' ),
				'type'            => 'color-alpha',
				'option_category' => 'basic_option',
                'mobile_options'  => true,
                'responsive'      => true,
				'custom_color'    => true,
                'description'     => esc_html__('Set left icon background color', 'chiac-divi-accordions'),
                'default'         => '',
                'show_if'         => array(
                        'chiac_l_icon_state' => 'on',
                        ),
				'tab_slug'        => 'advanced',
				'toggle_slug'     => 'chiac_left_icon',
			),
            'chiac_l_icon_bg_color_o'  => array(
				'label'           => esc_html__( 'Left Icon Background Color(Open Toggle)', 'chiac-divi-accordions' ),
				'type'            => 'color-alpha',
				'option_category' => 'basic_option',
                'mobile_options'  => true,
                'responsive'      => true,
				'custom_color'    => true,
                'description'     => esc_html__('Set left icon background color for open toggle', 'chiac-divi-accordions'),
                'default'         => '',
                'show_if'         => array(
                        'chiac_l_icon_state' => 'on',
                        ),
				'tab_slug'        => 'advanced',
				'toggle_slug'     => 'chiac_left_icon',
			),
            'chiac_l_icon_margin' => array(
                'label'             => esc_html__('Left Icon Margin', 'chiac-divi-accordions'),
				'description'     	=> esc_html__( 'Margin adds extra space to the outside of the element, increasing the distance between the element and other items on the page.', 'chiac-divi-accordions' ),
                'type'              => 'custom_margin',
				'option_category' 	=> 'layout',
                'mobile_options'    => true,
                'responsive'        => true,
//              'default'           => '0px|0px|0px|0px|false|false',
                'default_unit'      => 'px',
                'show_if'         => array(
                    'chiac_l_icon_state' => 'on',
                    ),
                'tab_slug'          => 'advanced',
                'toggle_slug'       => 'chiac_left_icon',
            ),
            'chiac_l_icon_padding' => array(
                'label'             => esc_html__('Left Icon Padding', 'chiac-divi-accordions'),
				'description'     	=> esc_html__( 'Padding adds extra space to the inside of the element, increasing the distance between the edge of the element and its inner contents.', 'chiac-divi-accordions' ),
                'type'              => 'custom_padding',
				'option_category' 	=> 'layout',
                'mobile_options'    => true,
                'responsive'        => true,
                'default'           => '20px|20px|20px|20px|false|false',
                'default_unit'      => 'px',
                'show_if'         => array(
                    'chiac_l_icon_state' => 'on',
                    ),
                'tab_slug'          => 'advanced',
                'toggle_slug'       => 'chiac_left_icon',
            ),
			'chiac_l_icon_visibility' => array(
				'label'           => esc_html__( 'Hide Left Icon on', 'chiac-divi-accordions' ),
				'type'            => 'multiple_checkboxes',
				'options'         => array(
					'desktop'      => esc_html__( 'Desktop', 'chiac-divi-accordions' ),
					'tablet'       => esc_html__( 'Tablet', 'chiac-divi-accordions' ),
					'phone'        => esc_html__( 'Phone', 'chiac-divi-accordions' ),
				),
                'description'     => esc_html__('Choose which devices you would like to hide the left icon on', 'chiac-divi-accordions'),
                'show_if'         => array(
                    'chiac_l_icon_state' => 'on',
                ),
				'tab_slug'        => 'custom_css',
				'toggle_slug'     => 'visibility',
			),
			'chiac_state' => array(
				'label'             => esc_html__( 'Active', 'chiac-divi-accordions' ),
                'description'       => esc_html__( 'Set the state (opened or closed) of the accordion item on initial page load', 'chiac-divi-accordions' ),
				'type'              => 'yes_no_button',
				'options'           => array(
					'on'  => esc_html__( 'On', 'chiac-divi-accordions' ),
					'off' => esc_html__( 'Off', 'chiac-divi-accordions' ),
				),
				'tab_slug'        => 'general',
				'toggle_slug'     => 'chiac_item_state',
			),
			'chiac_r_icon_state' => array(
				'label'             => esc_html__( 'Use Right Icon', 'chiac-divi-accordions' ),
                'description'       => esc_html__( 'Enable or disable right icon', 'chiac-divi-accordions' ),
				'type'              => 'yes_no_button',
				'options'           => array(
					'on'  => esc_html__( 'On', 'chiac-divi-accordions' ),
					'off' => esc_html__( 'Off', 'chiac-divi-accordions' ),
				),
                'default'         => 'on',
				'tab_slug'        => 'advanced',
				'toggle_slug'     => 'chiac_right_icon',
			),
			'chiac_r_icon_rotate' => array(
				'label'             => esc_html__( 'Rotate Right Icon(Open Toggle)', 'chiac-divi-accordions' ),
                'description'       => esc_html__( 'Enable rotation of right icon when the toggle is open. Useful for "arrow" icons', 'chiac-divi-accordions' ),
				'type'              => 'yes_no_button',
				'options'           => array(
					'on'  => esc_html__( 'On', 'chiac-divi-accordions' ),
					'off' => esc_html__( 'Off', 'chiac-divi-accordions' ),
				),
                'default'         => 'off',
                'show_if'         => array(
                    'chiac_r_icon_state' => 'on',
                    ),
				'tab_slug'        => 'advanced',
				'toggle_slug'     => 'chiac_right_icon',
			),
			'chiac_r_icon_rotate_degree' => array(
				'label'           => esc_html__( 'Right Icon Rotation Degree', 'chiac-divi-accordions' ),
				'type'            => 'range',
				'option_category' => 'configuration',
				'tab_slug'        => 'advanced',
				'toggle_slug'     => 'chiac_right_icon',
				'default'         => '180deg',
				'default_unit'    => 'deg',
				'default_on_front' => '',
                'description'     => esc_html__('Set right icon rotation degree', 'chiac-divi-accordions'),
				'range_settings'  => array(
					'min'  => '-360',
					'max'  => '360',
					'step' => '15',
				),
                'show_if'         => array(
                    'chiac_r_icon_state'    => 'on',
                    'chiac_r_icon_rotate'   => 'on'
                ),
			),
            'chiac_r_icon_or_image' => array(
                'label'             => esc_html__( 'Use Font Icon or Image', 'chiac-divi-accordions' ),
                'type'              => 'select',
                'option_category'   => 'configuration',
                'options'           => array(
                    'fonticon'    => esc_html__( 'Font Icon', 'chiac-divi-accordions' ),
                    'image'       => esc_html__( 'Image', 'chiac-divi-accordions' ),
                ),
                'default'           => 'fonticon',
                'default_on_front'  => 'fonticon',
                'show_if'             => array(
                        'chiac_r_icon_state' => 'on',
                        ),
                'tab_slug'          => 'advanced',
                'toggle_slug'       => 'chiac_right_icon',
            ),
			'chiac_select_r_fonticon' => array(
				'label'               => esc_html__( 'Select Icon', 'chiac-divi-accordions' ),
				'type'                => 'select_icon',
				'renderer'            => 'select_icon',
				'renderer_with_field' => true,
                'default'             => '%%18%%',
                'show_if'             => array(
                        'chiac_r_icon_state' => 'on',
                        'chiac_r_icon_or_image' => 'fonticon',
                        ),
                'description'         => esc_html__('Select right icon', 'chiac-divi-accordions'),
				'tab_slug'        => 'advanced',
				'toggle_slug'     => 'chiac_right_icon',
			),
			'chiac_r_icon_size' => array(
				'label'           => esc_html__( 'Right Icon Size', 'chiac-divi-accordions' ),
				'type'            => 'range',
				'option_category' => 'configuration',
                'mobile_options'    => true,
                'responsive'        => true,
				'default'         => '20px',
				'default_unit'    => 'px',
				'default_on_front' => '',
                'description'     => esc_html__('Set right icon font size', 'chiac-divi-accordions'),
				'range_settings'  => array(
					'min'  => '0',
					'max'  => '100',
					'step' => '1',
				),
                'show_if'         => array(
                    'chiac_r_icon_state' => 'on',
                    'chiac_r_icon_or_image'   => 'fonticon',
                ),
				'tab_slug'        => 'advanced',
				'toggle_slug'     => 'chiac_right_icon',
			),
            'chiac_right_image' => array(
                'label'              => esc_html__( 'Upload Image', 'chiac-divi-accordions' ),
                'type'               => 'upload',
                'upload_button_text' => esc_attr__( 'Upload an image', 'chiac-divi-accordions' ),
                'choose_text'        => esc_attr__( 'Choose an Image', 'chiac-divi-accordions' ),
                'update_text'        => esc_attr__( 'Set As Image', 'chiac-divi-accordions' ),
                'description'        => esc_html__('Upload the right icon image(png or jpg)', 'chiac-divi-accordions'),
                'default'            => $this->right_image_icon_placeholder,
                'show_if'            => array(
                        'chiac_r_icon_state' => 'on',
                        'chiac_r_icon_or_image' => 'image',
                        ),
                'tab_slug'           => 'advanced',
                'toggle_slug'        => 'chiac_right_icon',
            ),
            'chiac_r_image_width' => array(
                'label'             => esc_html__( 'Image Width', 'chiac-divi-accordions' ),
                'type'              => 'range',
                'option_category'   => 'configuration',
                'mobile_options'    => true,
                'responsive'        => true,
                'default'           => '',
                'default_unit'      => 'px',
                'default_on_front'  => '',
                'description'       => esc_html__('Set width of the right icon image', 'chiac-divi-accordions'),
                'range_settings'    => array(
                    'min'  => '0',
                    'max'  => '300',
                    'step' => '1',
                ),
                'show_if'         => array(
                    'chiac_r_icon_state' => 'on',
                    'chiac_r_icon_or_image'   => 'image',
                ),
                'tab_slug'        => 'advanced',
                'toggle_slug'     => 'chiac_right_icon',
            ),
            'chiac_r_icon_color'  => array(
				'label'           => esc_html__( 'Right Icon Color', 'chiac-divi-accordions' ),
				'type'            => 'color-alpha',
				'option_category' => 'basic_option',
                'mobile_options'  => true,
                'responsive'      => true,
				'custom_color'    => true,
                'description'     => esc_html__('Set right icon color', 'chiac-divi-accordions'),
                'default'         => '',
                'show_if'         => array(
                        'chiac_r_icon_state' => 'on',
                        'chiac_r_icon_or_image'   => 'fonticon',
                        ),
				'tab_slug'        => 'advanced',
				'toggle_slug'     => 'chiac_right_icon',
			),
            'chiac_r_icon_color_o'  => array(
				'label'           => esc_html__( 'Right Icon Color(Open Toggle)', 'chiac-divi-accordions' ),
				'type'            => 'color-alpha',
				'option_category' => 'basic_option',
                'mobile_options'  => true,
                'responsive'      => true,
				'custom_color'    => true,
                'description'     => esc_html__('Set right icon color for open toggle', 'chiac-divi-accordions'),
                'default'         => '',
                'show_if'         => array(
                        'chiac_r_icon_state' => 'on',
                        'chiac_r_icon_or_image'   => 'fonticon',
                        ),
				'tab_slug'        => 'advanced',
				'toggle_slug'     => 'chiac_right_icon',
			),
            'chiac_r_icon_bg_color'  => array(
				'label'           => esc_html__( 'Right Icon Background Color', 'chiac-divi-accordions' ),
				'type'            => 'color-alpha',
				'option_category' => 'basic_option',
                'mobile_options'  => true,
                'responsive'      => true,
				'custom_color'    => true,
                'description'     => esc_html__('Set right icon background color', 'chiac-divi-accordions'),
                'default'         => '',
                'show_if'         => array(
                        'chiac_r_icon_state' => 'on',
                        ),
				'tab_slug'        => 'advanced',
				'toggle_slug'     => 'chiac_right_icon',
			),
            'chiac_r_icon_bg_color_o'  => array(
				'label'           => esc_html__( 'Right Icon Background Color(Open Toggle)', 'chiac-divi-accordions' ),
				'type'            => 'color-alpha',
				'option_category' => 'basic_option',
                'mobile_options'  => true,
                'responsive'      => true,
				'custom_color'    => true,
                'description'     => esc_html__('Set right icon background color for open toggle', 'chiac-divi-accordions'),
                'default'         => '',
                'show_if'         => array(
                        'chiac_r_icon_state' => 'on',
                        ),
				'tab_slug'        => 'advanced',
				'toggle_slug'     => 'chiac_right_icon',
			),
            'chiac_r_icon_margin' => array(
                'label'             => esc_html__('Right Icon Margin', 'chiac-divi-accordions'),
				'description'     	=> esc_html__( 'Margin adds extra space to the outside of the element, increasing the distance between the element and other items on the page.', 'chiac-divi-accordions' ),
                'type'              => 'custom_margin',
				'option_category' 	=> 'layout',
                'mobile_options'    => true,
                'responsive'        => true,
//                'default'           => '0px|0px|0px|0px|false|false',
                'default_unit'      => 'px',
                'show_if'         => array(
                    'chiac_r_icon_state' => 'on',
                ),
                'tab_slug'          => 'advanced',
                'toggle_slug'       => 'chiac_right_icon',
            ),
            'chiac_r_icon_padding' => array(
                'label'             => esc_html__('Right Icon Padding', 'chiac-divi-accordions'),
				'description'     	=> esc_html__( 'Padding adds extra space to the inside of the element, increasing the distance between the edge of the element and its inner contents.', 'chiac-divi-accordions' ),
                'type'              => 'custom_padding',
				'option_category' 	=> 'layout',
                'mobile_options'    => true,
                'responsive'        => true,
                'default'           => '20px|20px|20px|20px|false|false',
                'default_unit'      => 'px',
                'show_if'         => array(
                    'chiac_r_icon_state' => 'on',
                ),
                'tab_slug'          => 'advanced',
                'toggle_slug'       => 'chiac_right_icon',
            ),
			'chiac_r_icon_visibility' => array(
				'label'           => esc_html__( 'Hide Right Icon on', 'chiac-divi-accordions' ),
				'type'            => 'multiple_checkboxes',
				'options'         => array(
					'desktop'      => esc_html__( 'Desktop', 'chiac-divi-accordions' ),
					'tablet'       => esc_html__( 'Tablet', 'chiac-divi-accordions' ),
					'phone'        => esc_html__( 'Phone', 'chiac-divi-accordions' ),
				),
                'description'     => esc_html__('Choose which devices you would like to hide the right icon on', 'chiac-divi-accordions'),
                'show_if'         => array(
                    'chiac_r_icon_state' => 'on',
                ),
				'tab_slug'        => 'custom_css',
				'toggle_slug'     => 'visibility',
			),    
		);
	}
	/**
	 * Module's advanced fields configuration
	 *
	 * @since 1.0.0
	 *
	 * @return array
	 */
	function get_advanced_fields_config() {
        
        // DB plugin main class
        $this->main_css_element = '.chiac_divi_accordions %%order_class%%';
        
        $advanced_fields = array();
        
        // hide item text align and shadow options added by default
        $advanced_fields['text'] = false;
        
        // hide item sizing option added by default
        $advanced_fields['max_width'] = false;
        
        // hide accordion item borders added by default
        $advanced_fields['borders'] = false;
        
        // Item Main spacing (default option)
        $advanced_fields['margin_padding'] = array(
            'css' => array(
                'main' => "{$this->main_css_element}",
				'important' => 'all',
			),
		);
        
        // Background
        $advanced_fields['background'] = array(
			'has_background_color_toggle'   => false, // default. Warning: to be deprecated
			'use_background_color'          => true, // default
			'use_background_color_gradient' => true, // default
			'use_background_image'          => true, // default
			'use_background_video'          => false, // default
		);
        
        // Header border options
		$advanced_fields['borders']['chiac_header'] = array(
			'css' => array(
				'main' => array(
                    'border_radii' => "{$this->main_css_element} .chiac-header",
					'border_styles' => "{$this->main_css_element} .chiac-header",
				),
				'important' => 'all',
			),
            'defaults' => array(
                'border_radii' => 'on||||',
                'border_styles' => array(
                    'width' => '0px',
                    'color' => '#f4f4f4',
                    'style' => 'solid',
                ),
            ),
			'label_prefix'    => esc_html__( 'Header', 'chiac-divi-accordions' ),
			'tab_slug'        => 'advanced',
			'toggle_slug'     => 'chiac_header_toggle',
		);
        
        // Left Icon border options
		$advanced_fields['borders']['chiac_l_icon'] = array(
			'css' => array(
				'main' => array(
                    'border_radii' => "{$this->main_css_element} .chiac-header .chiac_left_icon",
					'border_styles' => "{$this->main_css_element} .chiac-header .chiac_left_icon",
				),
			),
            'defaults' => array(
                'border_radii' => 'on||||',
                'border_styles' => array(
                    'width' => '0px',
                    'color' => '#f4f4f4',
                    'style' => 'solid',
                ),
            ),
            'priority'      => 10,
			'label_prefix'    => esc_html__( 'Left Icon', 'chiac-divi-accordions' ),
            'depends_on'      => array( 'chiac_l_icon_state' ),
            'depends_show_if' => 'on',
			'tab_slug'        => 'advanced',
			'toggle_slug'     => 'chiac_left_icon',
		);
        
        // Right Icon border options
		$advanced_fields['borders']['chiac_r_icon'] = array(
			'css' => array(
				'main' => array(
                    'border_radii' => "{$this->main_css_element} .chiac-header .chiac_right_icon",
					'border_styles' => "{$this->main_css_element} .chiac-header .chiac_right_icon",
				)
			),
            'defaults' => array(
                'border_radii' => 'on||||',
                'border_styles' => array(
                    'width' => '0px',
                    'color' => '#f4f4f4',
                    'style' => 'solid',
                ),
            ),
			'label_prefix'    => esc_html__( 'Right Icon', 'chiac-divi-accordions' ),
            'depends_on'      => array( 'chiac_r_icon_state' ),
            'depends_show_if' => 'on',
			'tab_slug'        => 'advanced',
			'toggle_slug'     => 'chiac_right_icon',
		);
        
        // Title border options
		$advanced_fields['borders']['chiac_title'] = array(
			'css' => array(
				'main' => array(
                    'border_radii' => "{$this->main_css_element} .chiac-header .chiac_heading_container",
					'border_styles' => "{$this->main_css_element} .chiac-header .chiac_heading_container",
				)
			),
            'defaults' => array(
                'border_radii' => 'on||||',
                'border_styles' => array(
                    'width' => '0px',
                    'color' => '#f4f4f4',
                    'style' => 'solid',
                ),
            ),
			'label_prefix'    => esc_html__( 'Title', 'chiac-divi-accordions' ),
			'tab_slug'        => 'advanced',
			'toggle_slug'     => 'chiac_title_toggle',
		);
        
        // Body border options
		$advanced_fields['borders']['chiac_content'] = array(
			'css' => array(
				'main' => array(
                    'border_radii'  => "{$this->main_css_element} .chiac-content",
					'border_styles' => "{$this->main_css_element} .chiac-content",
				)
			),
            'defaults' => array(
                'border_radii' => 'on||||',
                'border_styles' => array(
                    'width' => '0px',
                    'color' => '#f4f4f4',
                    'style' => 'solid',
                ),
            ),
			'label_prefix'    => esc_html__( 'Body', 'chiac-divi-accordions' ),
			'tab_slug'        => 'advanced',
			'toggle_slug'     => 'chiac_body',
		);
        
        // Item Main border options
		$advanced_fields['borders']['chiac_item'] = array(
			'css' => array(
				'main' => array(
                    'border_radii' => "{$this->main_css_element}.chiac_divi_accordions_item",
					'border_styles' => "{$this->main_css_element}.chiac_divi_accordions_item",
				)
			),
            'defaults' => array(
                'border_radii' => 'on||||',
                'border_styles' => array(
                    'width' => '1px',
                    'color' => '',
                    'style' => 'solid',
                ),
            ),
			'label_prefix'    => esc_html__( 'Main', 'chiac-divi-accordions' ),
			'tab_slug'        => 'advanced',
			'toggle_slug'     => 'item_borders',
		);
        
        /**
         * Box Shadow Options
         */
        // Header box-shadow options
		$advanced_fields['box_shadow']['chiac_header'] = array(
			'css' => array(
				'main' => "{$this->main_css_element} .chiac-header",
			),
			'label_prefix'    => esc_html__( 'Header', 'chiac-divi-accordions' ),
			'tab_slug'        => 'advanced',
			'toggle_slug'     => 'chiac_header_toggle',
		);
        // Left Icon box-shadow options
		$advanced_fields['box_shadow']['chiac_l_icon'] = array(
			'css' => array(
				'main' => "{$this->main_css_element} .chiac-header .chiac_left_icon",
			),
			'label_prefix'    => esc_html__( 'Left Icon', 'chiac-divi-accordions' ),
            'depends_on'      => array( 'chiac_l_icon_state' ),
            'depends_show_if' => 'on',
			'tab_slug'        => 'advanced',
			'toggle_slug'     => 'chiac_left_icon',
		);
        // Right Icon box-shadow options
		$advanced_fields['box_shadow']['chiac_r_icon'] = array(
			'css' => array(
				'main' => "{$this->main_css_element} .chiac-header .chiac_right_icon",
			),
			'label_prefix'    => esc_html__( 'Right Icon', 'chiac-divi-accordions' ),
            'depends_on'      => array( 'chiac_r_icon_state' ),
            'depends_show_if' => 'on',
			'tab_slug'        => 'advanced',
			'toggle_slug'     => 'chiac_right_icon',
		);
        // Title box-shadow options
		$advanced_fields['box_shadow']['chiac_title'] = array(
			'css' => array(
				'main' => "{$this->main_css_element} .chiac-header .chiac_heading_container",
			),
			'label_prefix'    => esc_html__( 'Title', 'chiac-divi-accordions' ),
			'tab_slug'        => 'advanced',
			'toggle_slug'     => 'chiac_title_toggle',
		);
        
        // Body box-shadow options
		$advanced_fields['box_shadow']['chiac_content'] = array(
			'css' => array(
				'main' => "{$this->main_css_element} .chiac-content",
			),
			'label_prefix'    => esc_html__( 'Body', 'chiac-divi-accordions' ),
			'tab_slug'        => 'advanced',
			'toggle_slug'     => 'chiac_body',
		);
        
        // Item Main box-shadow options
		$advanced_fields['box_shadow']['chiac_item'] = array(
			'css' => array(
                'main' => "{$this->main_css_element}",
                'important' => 'all'
			),
			'label_prefix'    => esc_html__( 'Item Main', 'chiac-divi-accordions' ),
			'tab_slug'        => 'advanced',
			'toggle_slug'     => 'item_box_shadow',
        );

		/**
         * Header Text
		 * 
		 * @since	v1.0.0
         */
		// Item Header Title
        $advanced_fields['fonts']['chiac_title'] = array(
			'label'    => esc_html__( 'Heading', 'chiac-divi-accordions' ),
			'css'      => array(
				'main' => "{$this->main_css_element} .chiac-title",
                // 'important' => 'all'
			),
            'header_level' => array(
                'default' => 'h4',
            ),
            'label'        => esc_html__( 'Title', 'chiac-divi-accordions' ),
			'tab_slug'    => 'advanced',
            'toggle_slug' => 'chiac_header_text',
			'sub_toggle'  => 'title',
		);
		// Item Header Subtitle
        $advanced_fields['fonts']['chiac_subtitle'] = array(
			'label'    => esc_html__( 'Subheading', 'chiac-divi-accordions' ),
			'css'      => array(
				'main' => "{$this->main_css_element} .chiac-subtitle",
                // 'important' => 'all'
			),
            'header_level' => array(
                'default' => 'h6',
            ),
            'label'        => esc_html__( 'Subtitle', 'chiac-divi-accordions' ),
			'tab_slug'    => 'advanced',
            'toggle_slug' => 'chiac_header_text',
			'sub_toggle'  => 'subtitle',
        );
        /**
         * Body Text
         * 
         */ 
        // Body Text: all
        $advanced_fields['fonts']['chiac_text'] = array(
			'label'    => esc_html__( 'Text', 'chiac-divi-accordions' ),
			'css'      => array(
				'main' => "{$this->main_css_element} .chiac-content",
			),
            'font_size' => array(
                'default' => absint( et_get_option( 'body_font_size', '14' ) ) . 'px',
                'default_on_child' => true,
            ),
            'letter_spacing' => array(
                'default' => '0px',
                'default_on_child' => true,
            ),
            'line_height' => array(
                'default' => floatval( et_get_option( 'body_font_height', '1.7' ) ) . 'em',
                'default_on_child' => true,
            ),
			'tab_slug'    => 'advanced',
            'toggle_slug' => 'chiac_content_text',
			'sub_toggle'  => 'p',
		);
        // Body Text: a
        $advanced_fields['fonts']['chiac_link'] = array(
			'label'    => esc_html__( 'Link', 'chiac-divi-accordions' ),
			'css'      => array(
				'main' => "{$this->main_css_element} .chiac-content a",
				// 'limited_main' => "{$this->main_css_element} .chiac-content a",
			),
            'font_size' => array(
                'default' => absint( et_get_option( 'body_font_size', '14' ) ) . 'px',
                'default_on_child' => true,
            ),
            'letter_spacing' => array(
                'default' => '0px',
                'default_on_child' => true,
            ),
            'line_height' => array(
                'default' => '1em',
                'default_on_child' => true,
            ),
			'tab_slug'    => 'advanced',
            'toggle_slug' => 'chiac_content_text',
			'sub_toggle'  => 'a',
        );
        // Body Text: ul
        $advanced_fields['fonts']['chiac_ul'] = array(
			'label'    => esc_html__( 'Unordered List', 'chiac-divi-accordions' ),
			'css'      => array(
                'main' => "{$this->main_css_element} .chiac-content ul",
                'line_height' => "{$this->main_css_element} .chiac-content ul li",
                'important' => 'line_height',
			),
            'font_size' => array(
                'default' => absint( et_get_option( 'body_font_size', '14' ) ) . 'px',
                'default_on_child' => true,
            ),
            'letter_spacing' => array(
                'default' => '0px',
                'default_on_child' => true,
            ),
            'line_height' => array(
                'default' => '1em',
                'default_on_child' => true,
            ),
			'tab_slug'    => 'advanced',
            'toggle_slug' => 'chiac_content_text',
			'sub_toggle'  => 'ul',
		);
        // Body Text: ol
        $advanced_fields['fonts']['chiac_ol'] = array(
			'label'    => esc_html__( 'Ordered List', 'chiac-divi-accordions' ),
			'css'      => array(
                'main' => "{$this->main_css_element} .chiac-content ol",
                'line_height' => "{$this->main_css_element} .chiac-content ol li",
                'important' => 'line_height',
			),
            'font_size' => array(
                'default' => absint( et_get_option( 'body_font_size', '14' ) ) . 'px',
                'default_on_child' => true,
            ),
            'letter_spacing' => array(
                'default' => '0px',
                'default_on_child' => true,
            ),
            'line_height' => array(
                'default' => '1em',
                'default_on_child' => true,
            ),
			'tab_slug'    => 'advanced',
            'toggle_slug' => 'chiac_content_text',
			'sub_toggle'  => 'ol',
		);
        // Body Text: blockquote
        $advanced_fields['fonts']['chiac_quote'] = array(
			'label'    => esc_html__( 'Blockquote', 'chiac-divi-accordions' ),
			'css'      => array(
				'main' => "{$this->main_css_element} .chiac-content blockquote, {$this->main_css_element} .chiac-content blockquote p",
			),
            'font_size' => array(
                'default' => absint( et_get_option( 'body_font_size', '14' ) ) . 'px',
                'default_on_child' => true,
            ),
            'letter_spacing' => array(
                'default' => '0px',
                'default_on_child' => true,
            ),
            'line_height' => array(
                'default' => '1em',
                'default_on_child' => true,
            ),
			'tab_slug'    => 'advanced',
			'toggle_slug' => 'chiac_content_text',
			'sub_toggle'  => 'quote',
        );

        /**
		 * Body Headings
		 * 
		 * @since	v1.0.0
		 */
		// Body heading: H1
        $advanced_fields['fonts']['chiac_h1'] = array(
            'label'    => esc_html__( 'Heading 1', 'chiac-divi-accordions' ),
            'css'      => array(
                'main' => "{$this->main_css_element} .chiac-content h1",
            ),
            'font_size' => array(
                'default' => absint( et_get_option( 'body_header_size', '30' ) ) . 'px',    
                'default_on_child' => true,
            ),
            'letter_spacing' => array(
                'default' => '0px',
                'default_on_child' => true,
            ),
            'line_height' => array(
                'default' => '1em',
                'default_on_child' => true,
            ),
			'tab_slug'    => 'advanced',
            'toggle_slug' => 'chiac_content_heading',
            'sub_toggle'  => 'h1',
        );
        // Body heading: H2
        $advanced_fields['fonts']['chiac_h2'] = array(
            'label'    => esc_html__( 'Heading 2', 'chiac-divi-accordions' ),
            'css'      => array(
                'main' => "{$this->main_css_element} .chiac-content h2",
            ),
            'font_size' => array(
                'default' => '26px',                
                'default_on_child' => true,
            ),
            'letter_spacing' => array(
                'default' => '0px',
                'default_on_child' => true,
            ),
            'line_height' => array(
                'default' => '1em',
                'default_on_child' => true,
            ),
			'tab_slug'    => 'advanced',
            'toggle_slug' => 'chiac_content_heading',
            'sub_toggle'  => 'h2',
        );
        // Body heading: H3
        $advanced_fields['fonts']['chiac_h3'] = array(
            'label'    => esc_html__( 'Heading 3', 'chiac-divi-accordions' ),
            'css'      => array(
                'main' => "{$this->main_css_element} .chiac-content h3",
            ),
            'font_size' => array(
                'default' => '22px',                
                'default_on_child' => true,
            ),
            'letter_spacing' => array(
                'default' => '0px',
                'default_on_child' => true,
            ),
            'line_height' => array(
                'default' => '1em',
                'default_on_child' => true,
            ),
			'tab_slug'    => 'advanced',
            'toggle_slug' => 'chiac_content_heading',
            'sub_toggle'  => 'h3',
        );
        // Body heading: H4
        $advanced_fields['fonts']['chiac_h4'] = array(
            'label'    => esc_html__( 'Heading 4', 'chiac-divi-accordions' ),
            'css'      => array(
                'main' => "{$this->main_css_element} .chiac-content h4",
            ),
            'font_size' => array(
                'default' => '18px',                
                'default_on_child' => true,
            ),
            'letter_spacing' => array(
                'default' => '0px',
                'default_on_child' => true,
            ),
            'line_height' => array(
                'default' => '1em',
                'default_on_child' => true,
            ),
			'tab_slug'    => 'advanced',
            'toggle_slug' => 'chiac_content_heading',
            'sub_toggle'  => 'h4',
        );
        // Body heading: H5
        $advanced_fields['fonts']['chiac_h5'] = array(
            'label'    => esc_html__( 'Heading 5', 'chiac-divi-accordions' ),
            'css'      => array(
                'main' => "{$this->main_css_element} .chiac-content h5",
            ),
            'font_size' => array(
                'default' => '16px',                
                'default_on_child' => true,
            ),
            'letter_spacing' => array(
                'default' => '0px',
                'default_on_child' => true,
            ),
            'line_height' => array(
                'default' => '1em',
                'default_on_child' => true,
            ),
			'tab_slug'    => 'advanced',
            'toggle_slug' => 'chiac_content_heading',
            'sub_toggle'  => 'h5',
        );
        // Body heading: H6
        $advanced_fields['fonts']['chiac_h6'] = array(
            'label'    => esc_html__( 'Heading 6', 'chiac-divi-accordions' ),
            'css'      => array(
                'main' => "{$this->main_css_element} .chiac-content h6",
            ),
            'font_size' => array(
                'default' => '14px',                
                'default_on_child' => true,
            ),
            'letter_spacing' => array(
                'default' => '0px',
                'default_on_child' => true,
            ),
            'line_height' => array(
                'default' => '1em',
                'default_on_child' => true,
            ),
			'tab_slug'    => 'advanced',
            'toggle_slug' => 'chiac_content_heading',
            'sub_toggle'  => 'h6',
        );
        
        return $advanced_fields;
	}
    
    /**
	 * Module's custom CSS fields configuration
	 *
	 * @since 1.0.0
	 *
	 * @return array
	 */
    public function get_custom_css_fields_config() {
		return array(
			'chiac_item_header_css' => array(
				'label'    => esc_html__( 'Header', 'chiac_divi_accordions_item' ),
				'selector' => '%%order_class%% .chiac-header',
			),
			'chiac_item_header_o_css' => array(
				'label'    => esc_html__( 'Header(Open)', 'chiac_divi_accordions_item' ),
				'selector' => '%%order_class%% .chiac-header.chiac_opened',
			),
			'chiac_item_l_icon_css' => array(
				'label'    => esc_html__( 'Left Icon', 'chiac_divi_accordions_item' ),
				'selector' => '%%order_class%% .chiac_left_icon',
			),
			'chiac_item_l_icon_o_css' => array(
				'label'    => esc_html__( 'Left Icon(Open)', 'chiac_divi_accordions_item' ),
				'selector' => '%%order_class%% .chiac-header.chiac_opened .chiac_left_icon',
			),
			'chiac_item_r_icon_css' => array(
				'label'    => esc_html__( 'Right Icon', 'chiac_divi_accordions_item' ),
				'selector' => '%%order_class%% .chiac_right_icon',
			),
			'chiac_item_r_icon_o_css' => array(
				'label'    => esc_html__( 'Right Icon(Open)', 'chiac_divi_accordions_item' ),
				'selector' => '%%order_class%% .chiac-header.chiac_opened .chiac_right_icon',
			),
			'chiac_item_title_main_css' => array(
				'label'    => esc_html__( 'Title Main', 'chiac_divi_accordions_item' ),
				'selector' => '%%order_class%% .chiac_heading_container',
			),
			'chiac_item_title_main_o_css' => array(
				'label'    => esc_html__( 'Title Main(Open)', 'chiac_divi_accordions_item' ),
				'selector' => '%%order_class%% .chiac-header.chiac_opened .chiac_heading_container',
			),
			'chiac_item_title_css' => array(
				'label'    => esc_html__( 'Title', 'chiac_divi_accordions_item' ),
				'selector' => '%%order_class%% .chiac_divi_accordions_item .chiac-title',
			),
			'chiac_item_title_o_css' => array(
				'label'    => esc_html__( 'Title(Open)', 'chiac_divi_accordions_item' ),
				'selector' => '%%order_class%% .chiac_divi_accordions_item .chiac_opened .chiac-title',
			),
			'chiac_item_subtitle_css' => array(
				'label'    => esc_html__( 'Subtitle', 'chiac_divi_accordions_item' ),
				'selector' => '%%order_class%% .chiac_divi_accordions_item .chiac-subtitle',
			),
			'chiac_item_subtitle_o_css' => array(
				'label'    => esc_html__( 'Subtitle(Open)', 'chiac_divi_accordions_item' ),
				'selector' => '%%order_class%% .chiac_divi_accordions_item .chiac_opened .chiac-subtitle',
			),
			'chiac_item_body_css' => array(
				'label'    => esc_html__( 'Body', 'chiac_divi_accordions_item' ),
				'selector' => '%%order_class%% .chiac-content',
			),
            
		);
    }

    /**
	 * Returns the generate_responsive_css() helper method.
	 * 
	 * @see		Chiac_Divi_Modules_Helper class.
	 * 
	 * @since	1.2.0
	 * 
	 * @param 	string 	$render_slug    		Module slug.
	 * @param 	array  	$args					Arguments.
	 *
	 */
	public function generate_responsive_css( $render_slug, $args ){

		return chiac_modules_helper_methods()->generate_responsive_css( $this->props, $render_slug, $args );

	}

    /**
	 * Returns the generate_responsive_css_SPACING() helper method.
	 * 
	 * @see		Chiac_Divi_Modules_Helper class.
	 * 
	 * @since	1.2.0
	 * 
	 * @param 	string 	$render_slug    		Module slug.
	 * @param 	array  	$args					Arguments.
	 *
	 */
	public function generate_responsive_css_SPACING( $render_slug, $args ){

		return chiac_modules_helper_methods()->generate_responsive_css_SPACING( $this->props, $render_slug, $args );

    }
    
    /**
	 * Returns the generate_border_responsive_css() helper method.
	 * 
	 * @see		Chiac_Divi_Modules_Helper class.
	 * 
	 * @since	1.2.0
	 * 
	 * @param 	string 	$render_slug    		Module slug.
	 * @param 	array  	$args					Arguments.
	 *
	 */
	public function generate_border_responsive_css( $render_slug, $args ){

		return chiac_modules_helper_methods()->generate_border_responsive_css( $this->props, $render_slug, $args );

	}
    
	/**
	 * Render module output
	 *
	 * @since 1.0.0
	 *
	 * @param array  $attrs       List of unprocessed attributes
	 * @param string $content     Content being processed
	 * @param string $render_slug Slug of module that is used for rendering output
	 *
	 * @return string module's rendered output
	 */
	function render( $attrs, $content = null, $render_slug ) {

        // print_r($this->props);

        // Parent module global props array
        global $chiac_parent_module_global_props;

        // Item title heading level set by parent module
        $chiac_i_title_level = $chiac_parent_module_global_props['chiac_i_title_level'];

        // Item subtitle heading level set by parent module
        $chiac_i_subtitle_level = $chiac_parent_module_global_props['chiac_i_subtitle_level'];

		// Module specific props added on $this->get_fields()
		$title                    = $this->props['title'];
		$subtitle                 = $this->props['subtitle'];
        $item_content             = $this->content ? $this->content : '';
        
        $chiac_subtitle_visibility    = explode("|", $this->props['chiac_subtitle_visibility']);
            $chiac_subtitle_visibility_1 = isset( $chiac_subtitle_visibility[1] ) ? $chiac_subtitle_visibility[1] : '';
            $chiac_subtitle_visibility_2 = isset( $chiac_subtitle_visibility[2] ) ? $chiac_subtitle_visibility[2] : '';
        
        $chiac_title_level        = $this->props['chiac_title_level'] ? $this->props['chiac_title_level'] : $chiac_i_title_level;
        $chiac_subtitle_level     = $this->props['chiac_subtitle_level'] ? $this->props['chiac_subtitle_level'] : $chiac_i_subtitle_level;
        
        $chiac_header_bg_color    = $this->props['chiac_header_bg_color'];
        $chiac_header_bg_color_o    = $this->props['chiac_header_bg_color_o'];
        
        $chiac_state              = $this->props['chiac_state'];
        $chiac_l_icon_state       = $this->props['chiac_l_icon_state'];
        
        $chiac_l_icon_rotate        = $this->props['chiac_l_icon_rotate'];
        $chiac_l_icon_rotate_degree = $this->props['chiac_l_icon_rotate_degree'];

        $chiac_l_icon_visibility    = explode("|", $this->props['chiac_l_icon_visibility']);
            $chiac_l_icon_visibility_1 = isset( $chiac_l_icon_visibility[1] ) ? $chiac_l_icon_visibility[1] : '';
            $chiac_l_icon_visibility_2 = isset( $chiac_l_icon_visibility[2] ) ? $chiac_l_icon_visibility[2] : '';
        
        $chiac_l_icon_color       = $this->props['chiac_l_icon_color'];
        $chiac_l_icon_color_o     = $this->props['chiac_l_icon_color_o'];
        $chiac_l_icon_bg_color    = $this->props['chiac_l_icon_bg_color'];
        $chiac_l_icon_bg_color_o  = $this->props['chiac_l_icon_bg_color_o'];
        
        $chiac_l_icon_size              = $this->props['chiac_l_icon_size'];
        $chiac_l_icon_size_tablet       = $this->props['chiac_l_icon_size_tablet'];
        $chiac_l_icon_size_phone        = $this->props['chiac_l_icon_size_phone'];
        $chiac_l_icon_size_last_edited  = explode("|", $this->props['chiac_l_icon_size_last_edited'] );
        
        $chiac_l_image_width              = $this->props['chiac_l_image_width'];
        $chiac_l_image_width_tablet       = $this->props['chiac_l_image_width_tablet'];
        $chiac_l_image_width_phone        = $this->props['chiac_l_image_width_phone'];
        $chiac_l_image_width_last_edited  = explode("|", $this->props['chiac_l_image_width_last_edited'] );
        
        $chiac_r_icon_state       = $this->props['chiac_r_icon_state'];
        
        $chiac_r_icon_rotate        = $this->props['chiac_r_icon_rotate'];
        $chiac_r_icon_rotate_degree = $this->props['chiac_r_icon_rotate_degree'];

        $chiac_r_icon_visibility    = explode("|", $this->props['chiac_r_icon_visibility']);
            $chiac_r_icon_visibility_1 = isset( $chiac_r_icon_visibility[1] ) ? $chiac_r_icon_visibility[1] : '';
            $chiac_r_icon_visibility_2 = isset( $chiac_r_icon_visibility[2] ) ? $chiac_r_icon_visibility[2] : '';
        
        $chiac_r_icon_color       = $this->props['chiac_r_icon_color'];
        $chiac_r_icon_color_o     = $this->props['chiac_r_icon_color_o'];
        $chiac_r_icon_bg_color    = $this->props['chiac_r_icon_bg_color'];
        $chiac_r_icon_bg_color_o  = $this->props['chiac_r_icon_bg_color_o'];
        
        $chiac_r_icon_size              = $this->props['chiac_r_icon_size'];
        $chiac_r_icon_size_tablet       = $this->props['chiac_r_icon_size_tablet'];
        $chiac_r_icon_size_phone        = $this->props['chiac_r_icon_size_phone'];
        $chiac_r_icon_size_last_edited  = explode("|", $this->props['chiac_r_icon_size_last_edited'] );
        
        $chiac_r_image_width              = $this->props['chiac_r_image_width'];
        $chiac_r_image_width_tablet       = $this->props['chiac_r_image_width_tablet'];
        $chiac_r_image_width_phone        = $this->props['chiac_r_image_width_phone'];
        $chiac_r_image_width_last_edited  = explode("|", $this->props['chiac_r_image_width_last_edited'] );
        
        $chiac_subtitle_state   = $this->props['chiac_subtitle_state'];
        $chiac_body_bg_color    = $this->props['chiac_body_bg_color'];
        $chiac_title_bg_color   = $this->props['chiac_title_bg_color'];
        $chiac_title_bg_color_o = $this->props['chiac_title_bg_color_o'];

        // Process header bg color into style
        $this->generate_responsive_css( $render_slug, 
        array( 
            'setting' 			=> 'chiac_header_bg_color',
            'selector' 			=> "{$this->main_css_element} .chiac-header",
            'property' 			=> 'background-color',
            'additional_css'	=> '',
            'field_type'		=> 'color',
            'priority'			=> ''
            )
        );

        // Process open toggle header bg color into style
        if ( $chiac_header_bg_color !== $chiac_header_bg_color_o ) {
            $this->generate_responsive_css( $render_slug, 
                array( 
                    'setting' 			=> 'chiac_header_bg_color_o',
                    'selector' 			=> "{$this->main_css_element} .chiac-header.chiac_opened",
                    'property' 			=> 'background-color',
                    'additional_css'	=> '',
                    'field_type'		=> 'color',
                    'priority'			=> ''
                )
            );
        }

        // Process header title bg color into style
        $this->generate_responsive_css( $render_slug, 
        array( 
            'setting' 			=> 'chiac_title_bg_color',
            'selector' 			=> "{$this->main_css_element} .chiac_heading_container",
            'property' 			=> 'background-color',
            'additional_css'	=> '!important',
            'field_type'		=> 'color',
            'priority'			=> ''
            )
        );

        // Process open toggle header title bg color into style
        if ( $chiac_title_bg_color !== $chiac_title_bg_color_o ) {
            $this->generate_responsive_css( $render_slug, 
                array( 
                    'setting' 			=> 'chiac_title_bg_color_o',
                    'selector' 			=> "{$this->main_css_element} .chiac-header.chiac_opened .chiac_heading_container",
                    'property' 			=> 'background-color',
                    'additional_css'	=> '!important',
                    'field_type'		=> 'color',
                    'priority'			=> ''
                )
            );
        }   

        // Process body bg color into style
        $this->generate_responsive_css( $render_slug, 
        array( 
            'setting' 			=> 'chiac_body_bg_color',
            'selector' 			=> "{$this->main_css_element} .chiac-content",
            'property' 			=> 'background-color',
            'additional_css'	=> '!important',
            'field_type'		=> 'color',
            'priority'			=> ''
            )
        );

        // Process left icon rotation degree into style
		if ( '' !== $chiac_l_icon_rotate_degree ) {
			ET_Builder_Element::set_style( $render_slug, array(
				'selector'    => '%%order_class%% .chiac-header.chiac_l_icon_rotate.chiac_opened .chiac_left_icon > span',
				'declaration' => sprintf(
					'-webkit-transform: rotate(%1$s);
                        -ms-transform: rotate(%1$s);
                            transform: rotate(%1$s);',
					esc_html( $chiac_l_icon_rotate_degree )
				),
			) );
        }     
        
        // Process left icon color into style
        $this->generate_responsive_css( $render_slug, 
        array( 
            'setting' 			=> 'chiac_l_icon_color',
            'selector' 			=> "{$this->main_css_element} .chiac-header .chiac_left_icon",
            'property' 			=> 'color',
            'additional_css'	=> '!important',
            'field_type'		=> 'color',
            'priority'			=> ''
            )
        );
        // Process open toggle left icon color into style
        if ( $chiac_l_icon_color !== $chiac_l_icon_color_o ) {
            $this->generate_responsive_css( $render_slug, 
            array( 
                'setting' 			=> 'chiac_l_icon_color_o',
                'selector' 			=> "{$this->main_css_element} .chiac-header.chiac_opened .chiac_left_icon",
                'property' 			=> 'color',
                'additional_css'	=> '!important',
                'field_type'		=> 'color',
                'priority'			=> ''
                )
            );
        }

        // Process left icon background color into style
        $this->generate_responsive_css( $render_slug, 
        array( 
            'setting' 			=> 'chiac_l_icon_bg_color',
            'selector' 			=> "{$this->main_css_element} .chiac-header .chiac_left_icon",
            'property' 			=> 'background-color',
            'additional_css'	=> '!important',
            'field_type'		=> 'color',
            'priority'			=> ''
            )
        );
        // Process open toggle left icon background color into style
        if ( $chiac_l_icon_bg_color !== $chiac_l_icon_bg_color_o ) {
            $this->generate_responsive_css( $render_slug, 
            array( 
                'setting' 			=> 'chiac_l_icon_bg_color_o',
                'selector' 			=> "{$this->main_css_element} .chiac-header.chiac_opened .chiac_left_icon",
                'property' 			=> 'background-color',
                'additional_css'	=> '!important',
                'field_type'		=> 'color',
                'priority'			=> ''
                )
            );
        }
        
        // Process right icon rotation degree into style
		if ( '' !== $chiac_r_icon_rotate_degree ) {
			ET_Builder_Element::set_style( $render_slug, array(
				'selector'    => '%%order_class%% .chiac-header.chiac_r_icon_rotate.chiac_opened .chiac_right_icon > span',
				'declaration' => sprintf(
					'-webkit-transform: rotate(%1$s);
                        -ms-transform: rotate(%1$s);
                            transform: rotate(%1$s);',
					esc_html( $chiac_r_icon_rotate_degree )
				),
			) );
        }       

        // Process right icon color into style
        $this->generate_responsive_css( $render_slug, 
        array( 
            'setting' 			=> 'chiac_r_icon_color',
            'selector' 			=> "{$this->main_css_element} .chiac-header .chiac_right_icon",
            'property' 			=> 'color',
            'additional_css'	=> '!important',
            'field_type'		=> 'color',
            'priority'			=> ''
            )
        );
        // Process open toggle right icon color into style
        if ( $chiac_r_icon_color !== $chiac_r_icon_color_o ) {
            $this->generate_responsive_css( $render_slug, 
            array( 
                'setting' 			=> 'chiac_r_icon_color_o',
                'selector' 			=> "{$this->main_css_element} .chiac-header.chiac_opened .chiac_right_icon",
                'property' 			=> 'color',
                'additional_css'	=> '!important',
                'field_type'		=> 'color',
                'priority'			=> ''
                )
            );
        }
        // Process right icon background color into style
        $this->generate_responsive_css( $render_slug, 
        array( 
            'setting' 			=> 'chiac_r_icon_bg_color',
            'selector' 			=> "{$this->main_css_element} .chiac-header .chiac_right_icon",
            'property' 			=> 'background-color',
            'additional_css'	=> '!important',
            'field_type'		=> 'color',
            'priority'			=> ''
            )
        );
        // Process open toggle right icon background color into style
        if ( $chiac_r_icon_bg_color !== $chiac_r_icon_bg_color_o ) {
            $this->generate_responsive_css( $render_slug, 
            array( 
                'setting' 			=> 'chiac_r_icon_bg_color_o',
                'selector' 			=> "{$this->main_css_element} .chiac-header.chiac_opened .chiac_right_icon",
                'property' 			=> 'background-color',
                'additional_css'	=> '!important',
                'field_type'		=> 'color',
                'priority'			=> ''
                )
            );
        }
        // Process blockquote border width into style
        $this->generate_responsive_css( $render_slug, 
        array( 
            'setting' 			=> 'chiac_quote_border_width',
            'selector' 			=> "{$this->main_css_element} .chiac-content blockquote",
            'property' 			=> 'border-width',
            'additional_css'	=> '!important',
            'field_type'		=> 'range',
            'priority'			=> ''
            )
        );
        // Process ordered list style type into style
        $this->generate_responsive_css( $render_slug, 
        array( 
            'setting' 			=> 'chiac_ol_type',
            'selector' 			=> "{$this->main_css_element} .chiac-content ol",
            'property' 			=> 'list-style-type',
            'additional_css'	=> '!important',
            'field_type'		=> 'select',
            'priority'			=> ''
            )
        );
        // Process ordered list style position into style
        $this->generate_responsive_css( $render_slug, 
        array( 
            'setting' 			=> 'chiac_ol_position',
            'selector' 			=> "{$this->main_css_element} .chiac-content ol",
            'property' 			=> 'list-style-position',
            'additional_css'	=> '!important',
            'field_type'		=> 'select',
            'priority'			=> ''
            )
        );

        // Process ordered list item indent into style
		// no need for 'chiac_ol_item_indent', it is generated by default
        
        // Process unordered list style type into style
        $this->generate_responsive_css( $render_slug, 
        array( 
            'setting' 			=> 'chiac_ul_type',
            'selector' 			=> "{$this->main_css_element} .chiac-content ul",
            'property' 			=> 'list-style-type',
            'additional_css'	=> '!important',
            'field_type'		=> 'select',
            'priority'			=> ''
            )
        );
        // Process unordered list style position into style
        $this->generate_responsive_css( $render_slug, 
        array( 
            'setting' 			=> 'chiac_ul_position',
            'selector' 			=> "{$this->main_css_element} .chiac-content ul",
            'property' 			=> 'list-style-position',
            'additional_css'	=> '!important',
            'field_type'		=> 'select',
            'priority'			=> ''
            )
        );
        /**
         * DO NOT DELETE THIS YET!!!
         * 
         * Process unordered list item indent into style 
         * 
         * (looks like range option responsive CSS does not need to be generated manually for FE, 
         * just commenting it out for now).
         */ 
        // $this->generate_responsive_css( $render_slug, 
        // array( 
        //     'setting' 			=> 'chiac_ul_item_indent',
        //     'selector' 			=> ".chiac_divi_accordions %%order_class%% .chiac-content ul, {$this->main_css_element} .chiac-content ul",
        //     'property' 			=> 'padding-left',
        //     'additional_css'	=> '!important',
        //     'field_type'		=> 'range',
        //     'priority'			=> ''
        //     )
        // );

        // Process left icon size into style
        if ( '' !== $chiac_l_icon_size_tablet || '' !== $chiac_l_icon_size_phone || '' !== $chiac_l_icon_size ) {
            $chiac_l_icon_size_responsive_active = et_pb_get_responsive_status( $chiac_l_icon_size_last_edited[0] );

            $chiac_l_icon_size_values = array(
                'desktop' => $chiac_l_icon_size,
                'tablet'  => $chiac_l_icon_size_responsive_active ? $chiac_l_icon_size_tablet : '',
                'phone'   => $chiac_l_icon_size_responsive_active ? $chiac_l_icon_size_phone : '',
            );

            et_pb_generate_responsive_css( $chiac_l_icon_size_values, '.chiac_divi_accordions_item%%order_class%% .chiac_left_icon, .et-db #et-boc .chiac_divi_accordions_item%%order_class%% .chiac_left_icon', 'font-size', $render_slug );
		}
        
        // Process left icon image width into style
        if ( '' !== $chiac_l_image_width_tablet || '' !== $chiac_l_image_width_phone || '' !== $chiac_l_image_width ) {
            $chiac_l_image_width_responsive_active = et_pb_get_responsive_status( $chiac_l_image_width_last_edited[0] );

            $chiac_l_image_width_values = array(
                'desktop' => $chiac_l_image_width,
                'tablet'  => $chiac_l_image_width_responsive_active ? $chiac_l_image_width_tablet : '',
                'phone'   => $chiac_l_image_width_responsive_active ? $chiac_l_image_width_phone : '',
            );

            et_pb_generate_responsive_css( $chiac_l_image_width_values, '.chiac_divi_accordions_item%%order_class%% .chiac_left_icon span img', 'width', $render_slug );
		}
        
        // Process left icon image min-width into style
        if ( '' !== $chiac_l_image_width_tablet || '' !== $chiac_l_image_width_phone || '' !== $chiac_l_image_width ) {
            $chiac_l_image_width_responsive_active = et_pb_get_responsive_status( $chiac_l_image_width_last_edited[0] );

            $chiac_l_image_width_values = array(
                'desktop' => $chiac_l_image_width,
                'tablet'  => $chiac_l_image_width_responsive_active ? $chiac_l_image_width_tablet : '',
                'phone'   => $chiac_l_image_width_responsive_active ? $chiac_l_image_width_phone : '',
            );

            et_pb_generate_responsive_css( $chiac_l_image_width_values, '.chiac_divi_accordions_item%%order_class%% .chiac_left_icon span img', 'min-width', $render_slug );
		}
        
        // Process right icon size into style
        if ( '' !== $chiac_r_icon_size_tablet || '' !== $chiac_r_icon_size_phone || '' !== $chiac_r_icon_size ) {
            $chiac_r_icon_size_responsive_active = et_pb_get_responsive_status( $chiac_r_icon_size_last_edited[0] );

            $chiac_r_icon_size_values = array(
                'desktop' => $chiac_r_icon_size,
                'tablet'  => $chiac_r_icon_size_responsive_active ? $chiac_r_icon_size_tablet : '',
                'phone'   => $chiac_r_icon_size_responsive_active ? $chiac_r_icon_size_phone : '',
            );

            et_pb_generate_responsive_css( $chiac_r_icon_size_values, '.chiac_divi_accordions_item%%order_class%% .chiac_right_icon, .et-db #et-boc .chiac_divi_accordions_item%%order_class%% .chiac_right_icon', 'font-size', $render_slug );
		}
        
        // Process right icon image width into style
        if ( '' !== $chiac_r_image_width_tablet || '' !== $chiac_r_image_width_phone || '' !== $chiac_r_image_width ) {
            $chiac_r_image_width_responsive_active = et_pb_get_responsive_status( $chiac_r_image_width_last_edited[0] );

            $chiac_r_image_width_values = array(
                'desktop' => $chiac_r_image_width,
                'tablet'  => $chiac_r_image_width_responsive_active ? $chiac_r_image_width_tablet : '',
                'phone'   => $chiac_r_image_width_responsive_active ? $chiac_r_image_width_phone : '',
            );

            et_pb_generate_responsive_css( $chiac_r_image_width_values, '.chiac_divi_accordions_item%%order_class%% .chiac_right_icon span img', 'width', $render_slug );
		}
        
        // Process right icon image min-width into style
        if ( '' !== $chiac_r_image_width_tablet || '' !== $chiac_r_image_width_phone || '' !== $chiac_r_image_width ) {
            $chiac_r_image_width_responsive_active = et_pb_get_responsive_status( $chiac_r_image_width_last_edited[0] );

            $chiac_r_image_width_values = array(
                'desktop' => $chiac_r_image_width,
                'tablet'  => $chiac_r_image_width_responsive_active ? $chiac_r_image_width_tablet : '',
                'phone'   => $chiac_r_image_width_responsive_active ? $chiac_r_image_width_phone : '',
            );

            et_pb_generate_responsive_css( $chiac_r_image_width_values, '.chiac_divi_accordions_item%%order_class%% .chiac_right_icon span img', 'min-width', $render_slug );
        }

        // Process header border styles.
		$this->generate_border_responsive_css( $render_slug, 
            array( 
                'setting' 		 => 'chiac_header', // $advanced_fields['borders']['chiac_header']
                'selector' 		 => "{$this->main_css_element} .chiac-header",
                'property' 		 => 'border',
                'additional_css' => '!important',
                'field_type'	 => 'border',
                'priority'		 => ''
            )
        );

        // Process left icon border styles.
		$this->generate_border_responsive_css( $render_slug, 
            array( 
                'setting' 		 => 'chiac_l_icon', // $advanced_fields['borders']['chiac_l_icon']
                'selector' 		 => "{$this->main_css_element} .chiac-header .chiac_left_icon",
                'property' 		 => 'border',
                'additional_css' => '!important',
                'field_type'	 => 'border',
                'priority'		 => ''
            )
        );

        // Process right icon border styles.
		$this->generate_border_responsive_css( $render_slug, 
            array( 
                'setting' 		 => 'chiac_r_icon', // $advanced_fields['borders']['chiac_r_icon']
                'selector' 		 => "{$this->main_css_element} .chiac-header .chiac_right_icon",
                'property' 		 => 'border',
                'additional_css' => '!important',
                'field_type'	 => 'border',
                'priority'		 => ''
            )
        );

        // Process title border styles.
		$this->generate_border_responsive_css( $render_slug, 
            array( 
                'setting' 		 => 'chiac_title', // $advanced_fields['borders']['chiac_title']
                'selector' 		 => "{$this->main_css_element} .chiac-header .chiac_heading_container",
                'property' 		 => 'border',
                'additional_css' => '!important',
                'field_type'	 => 'border',
                'priority'		 => ''
            )
        );

        // Process body(content) border styles.
		$this->generate_border_responsive_css( $render_slug, 
            array( 
                'setting' 		 => 'chiac_content', // $advanced_fields['borders']['chiac_content']
                'selector' 		 => "{$this->main_css_element} .chiac-content",
                'property' 		 => 'border',
                'additional_css' => '!important',
                'field_type'	 => 'border',
                'priority'		 => ''
            )
        );

        // Process Item(main) border styles.
		$this->generate_border_responsive_css( $render_slug, 
            array( 
                'setting' 		 => 'chiac_item', // $advanced_fields['borders']['chiac_item']
                'selector' 		 => "{$this->main_css_element}.chiac_divi_accordions_item",
                'property' 		 => 'border',
                'additional_css' => '!important',
                'field_type'	 => 'border',
                'priority'		 => ''
            )
        );
        
        // Process header margin into style
		$this->generate_responsive_css_SPACING( $render_slug, 
            array( 
                'setting' 			=> 'chiac_header_margin',
                'selector' 			=> "{$this->main_css_element} .chiac-header",
                'property' 			=> 'margin',
                'additional_css'	=> '',
                'field_type'		=> 'custom_margin',
                'priority'			=> ''
            )
        );

        // Process header padding into style
        $this->generate_responsive_css_SPACING( $render_slug, 
            array( 
                'setting' 			=> 'chiac_header_padding',
                'selector' 			=> "{$this->main_css_element} .chiac-header",
                'property' 			=> 'padding',
                'additional_css'	=> '',
                'field_type'		=> 'custom_padding',
                'priority'			=> ''
            )
        );

        // Process LEFT ICON margin into style
		$this->generate_responsive_css_SPACING( $render_slug, 
            array( 
                'setting' 			=> 'chiac_l_icon_margin',
                'selector' 			=> "{$this->main_css_element} .chiac-header .chiac_left_icon",
                'property' 			=> 'margin',
                'additional_css'	=> '',
                'field_type'		=> 'custom_margin',
                'priority'			=> ''
            )
        );

        // Process LEFT ICON padding into style
        $this->generate_responsive_css_SPACING( $render_slug, 
            array( 
                'setting' 			=> 'chiac_l_icon_padding',
                'selector' 			=> "{$this->main_css_element} .chiac-header .chiac_left_icon",
                'property' 			=> 'padding',
                'additional_css'	=> '',
                'field_type'		=> 'custom_padding',
                'priority'			=> '',
                'default'           => '20px|20px|20px|20px|false|false'
            )
        );

        // Process RIGHT ICON margin into style
		$this->generate_responsive_css_SPACING( $render_slug, 
            array( 
                'setting' 			=> 'chiac_r_icon_margin',
                'selector' 			=> "{$this->main_css_element} .chiac-header .chiac_right_icon",
                'property' 			=> 'margin',
                'additional_css'	=> '',
                'field_type'		=> 'custom_margin',
                'priority'			=> ''
            )
        );

        // Process RIGHT ICON padding into style
        $this->generate_responsive_css_SPACING( $render_slug, 
            array( 
                'setting' 			=> 'chiac_r_icon_padding',
                'selector' 			=> "{$this->main_css_element} .chiac-header .chiac_right_icon",
                'property' 			=> 'padding',
                'additional_css'	=> '',
                'field_type'		=> 'custom_padding',
                'priority'			=> '',
                'default'           => '20px|20px|20px|20px|false|false'
            )
        );

        // Process TITLE margin into style
		$this->generate_responsive_css_SPACING( $render_slug, 
            array( 
                'setting' 			=> 'chiac_title_margin',
                'selector' 			=> "{$this->main_css_element} .chiac-header .chiac_heading_container",
                'property' 			=> 'margin',
                'additional_css'	=> '',
                'field_type'		=> 'custom_margin',
                'priority'			=> ''
            )
        );

        // Process TITLE padding into style
        $this->generate_responsive_css_SPACING( $render_slug, 
            array( 
                'setting' 			=> 'chiac_title_padding',
                'selector' 			=> "{$this->main_css_element} .chiac-header .chiac_heading_container",
                'property' 			=> 'padding',
                'additional_css'	=> '',
                'field_type'		=> 'custom_padding',
                'priority'			=> '',
                'default'           => '8px|20px|8px|20px|false|false'
            )
        );

        // Process BODY margin into style
		$this->generate_responsive_css_SPACING( $render_slug, 
            array( 
                'setting' 			=> 'chiac_body_margin',
                'selector' 			=> "{$this->main_css_element} .chiac-content",
                'property' 			=> 'margin',
                'additional_css'	=> '',
                'field_type'		=> 'custom_margin',
                'priority'			=> ''
            )
        );

        // Process BODY padding into style
        $this->generate_responsive_css_SPACING( $render_slug, 
            array( 
                'setting' 			=> 'chiac_body_padding',
                'selector' 			=> "{$this->main_css_element} .chiac-content",
                'property' 			=> 'padding',
                'additional_css'	=> '',
                'field_type'		=> 'custom_padding',
                'priority'			=> '',
                'default'           => '20px|20px|20px|20px|false|false'
            )
        );

        /**
         * add/remove header classes
         */
        $header_classes = ["chiac-header"];
        
        // set default state of item on page load
        $item_opened = $chiac_state == 'on' ? 'chiac_opened' : 'chiac_closed';
        // enable/disable subtitle
        $subtitle_state = $chiac_subtitle_state == 'on' ? 'chiac_subtitle' : 'chiac_subtitle_hidden';
        // enable/disable left icon
        $left_icon = $chiac_l_icon_state == 'on' ? 'chiac_left_icon_show' : 'chiac_left_icon_hidden';
        // enable/disable right icon
        $right_icon = $chiac_r_icon_state == 'on' ? 'chiac_right_icon_show' : 'chiac_right_icon_hidden';
        // enable/disable left icon rotation
        $rotate_left_icon = $chiac_l_icon_rotate == 'on' ? 'chiac_l_icon_rotate' : '';
        // enable/disable right icon rotation
        $rotate_right_icon = $chiac_r_icon_rotate == 'on' ? 'chiac_r_icon_rotate' : '';
        // set subtitle visibility
        $subtitle_hide_desktop = $chiac_subtitle_visibility[0] == 'on' ? 'chiac_hide_subtitle_desktop' : '';
        $subtitle_hide_tablet  = $chiac_subtitle_visibility_1 == 'on' ? 'chiac_hide_subtitle_tablet' : '';
        $subtitle_hide_phone   = $chiac_subtitle_visibility_2 == 'on' ? 'chiac_hide_subtitle_phone' : '';
        
        array_push($header_classes, $item_opened, $subtitle_state, $left_icon, $right_icon, $rotate_left_icon, $rotate_right_icon, $subtitle_hide_desktop, $subtitle_hide_tablet, $subtitle_hide_phone);
        
        $header_classes = implode(" ", $header_classes);

        /**
         * add/remove subtitle classes
         */
        $subtitle_classes = ["chiac-subtitle"];
        
        $subtitle_classes = implode(" ", $subtitle_classes);

        $chiac_icon_or_image = $this->props['chiac_icon_or_image'];

        /**
         * add/remove left icon classes
         */
        $left_i_classes = ["chiac_left_icon"];
        // add the et-pb-icon class if image IS NOT selected for the Left side
        $et_pb_icon = $chiac_icon_or_image !== 'image' ? 'et-pb-icon' : '';
        
        array_push($left_i_classes, $et_pb_icon);
        
        $left_i_classes = implode(" ", $left_i_classes);
        
        /**
         * add/remove left icon container classes
         */
        $left_icon_classes = ["chiac_left_icon_container"];
        // set left icon visibility
        $left_icon_hide_desktop = $chiac_l_icon_visibility[0] == 'on' ? 'chiac_hide_desktop' : '';
        $left_icon_hide_tablet  = $chiac_l_icon_visibility_1 == 'on' ? 'chiac_hide_tablet' : '';
        $left_icon_hide_phone   = $chiac_l_icon_visibility_2 == 'on' ? 'chiac_hide_phone' : '';
        
        array_push($left_icon_classes, $left_icon_hide_desktop, $left_icon_hide_tablet, $left_icon_hide_phone);
        
        $left_icon_classes = implode(" ", $left_icon_classes);

        $chiac_r_icon_or_image = $this->props['chiac_r_icon_or_image'];

        /**
         * add/remove right icon classes
         */
        $right_i_classes = ["chiac_right_icon"];
        // add the et-pb-icon class if image IS NOT selected for the Right side
        $et_pb_icon = $chiac_r_icon_or_image !== 'image' ? 'et-pb-icon' : '';

        array_push($right_i_classes, $et_pb_icon);

        $right_i_classes = implode(" ", $right_i_classes);
        
        /**
         * add/remove right icon container classes
         */
        $right_icon_classes = ["chiac_right_icon_container"];
        // set right icon visibility
        $right_icon_hide_desktop = $chiac_r_icon_visibility[0] == 'on' ? 'chiac_hide_desktop' : '';
        $right_icon_hide_tablet  = $chiac_r_icon_visibility_1 == 'on' ? 'chiac_hide_tablet' : '';
        $right_icon_hide_phone   = $chiac_r_icon_visibility_2 == 'on' ? 'chiac_hide_phone' : '';
        
        array_push($right_icon_classes, $right_icon_hide_desktop, $right_icon_hide_tablet, $right_icon_hide_phone);
        
        $right_icon_classes = implode(" ", $right_icon_classes);
        
        // select icon for left icon
        $left_font_icon = $this->props['chiac_select_fonticon'];
        
        // temporary fix for icons render bug in Divi core
        $left_font_icon = str_replace( array( '&#91;', '\\', '&#93;', '"' ), array( '%91', '%92', '%93', '%22' ), $left_font_icon );
        
        // select image for left icon
        $left_image_icon = esc_url( $this->props['chiac_left_image'] );
        
        // left icon fonticon
        $left_icon_icon = isset( $left_font_icon ) ? et_pb_process_font_icon( $left_font_icon ) : et_pb_process_font_icon('%%99%%');
        
        // left icon image
        $left_icon_image = isset( $left_image_icon ) && $left_image_icon !== '' ? $left_image_icon : $this->left_image_icon_placeholder;
        
        // print left icon fonticon or image depending on which one is selected
        $left_icon_fonticon_or_image = $chiac_icon_or_image === 'image' ? sprintf( 
            '<img src="%1$s" />',
            esc_attr( $left_icon_image )
        ) : esc_attr( $left_icon_icon );
        
        // select icon for right icon
        $right_font_icon = $this->props['chiac_select_r_fonticon'];
        
        // temporary fix for icons render bug in Divi core
        $right_font_icon = str_replace( array( '&#91;', '\\', '&#93;', '"' ), array( '%91', '%92', '%93', '%22' ), $right_font_icon );
        
        // select image for right icon
        $right_image_icon = esc_url( $this->props['chiac_right_image'] );
        
        // right icon fonticon
        $right_icon_icon = isset( $right_font_icon ) ? et_pb_process_font_icon( $right_font_icon ) : et_pb_process_font_icon('%%18%%');
        // right icon image
        $right_icon_image = isset( $right_image_icon ) && $right_image_icon !== '' ? $right_image_icon : $this->right_image_icon_placeholder;
        
        // print right icon fonticon or image depending on which one is selected
        $right_icon_fonticon_or_image = $chiac_r_icon_or_image === 'image' ? sprintf( 
            '<img src="%1$s" />',
            esc_attr( $right_icon_image )
        ) : esc_attr( $right_icon_icon );
		
		// Module content
		$output = sprintf(
			'
            <div class="%1$s">
                <div class="%8$s">
                    <i class="%12$s"><span>%7$s</span></i>
                </div>
                <div class="chiac_heading_container">
                    <%5$s class="chiac-title">%2$s</%5$s>
                    <%6$s class="%11$s">%3$s</%6$s>
                </div>
                <div class="%10$s">
                    <i class="%13$s"><span>%9$s</span></i>
                </div>
            </div>   
			<div class="chiac-content">%4$s</div>',
            esc_html( $header_classes ),
			esc_html( $title ),
			esc_html( $subtitle ),
			et_core_sanitized_previously( $item_content ),
            esc_html( $chiac_title_level ),
            esc_html( $chiac_subtitle_level ),
            $left_icon_fonticon_or_image,
            esc_html( $left_icon_classes ),
            $right_icon_fonticon_or_image,
            esc_html( $right_icon_classes ),
            esc_html( $subtitle_classes ),
            esc_html( $left_i_classes ),
            esc_html( $right_i_classes )
		);
		// Render module content
		return $output;
        
	}
}
new CHIAC_Divi_Accordions_Child;