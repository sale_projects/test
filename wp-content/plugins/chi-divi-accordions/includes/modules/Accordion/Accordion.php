<?php

// Die if accessed directly
if ( ! defined( 'ABSPATH' ) ) {
	die( 'Access denied.' );
}

class CHIAC_Divi_Accordions extends ET_Builder_Module {

    // Module slug (also used as shortcode tag)
	public $slug       = 'chiac_divi_accordions';
    
	// Full Visual Builder support
	public $vb_support = 'on';
    
	// Module item's slug
	public $child_slug = 'chiac_divi_accordions_item';
	
	protected $module_credits = array(
		'module_uri' => 'https://divicio.us',
		'author'     => 'Ivan Chiurcci',
		'author_uri' => 'https://divicio.us',
	);
    
    /**
	 * Module properties initialization
	 *
	 * @since 1.0.0
	 */
	public function init() {
        
        // Module name
		$this->name = esc_html__( 'Accordions Plus', 'chiac-divi-accordions' );
        
        // Module Icon
		$this->icon_path               =  plugin_dir_path( __FILE__ ) . 'accordion.svg';
        
        // Toggle settings
		$this->settings_modal_toggles  = array(
			'general'  => array(
				'toggles' => array(
					'chiac_toggles_speed' 	=> esc_html__( 'Speed', 'chiac-divi-accordions' ),
					'chiac_accordion_mode' 	=> esc_html__( 'Mode', 'chiac-divi-accordions' ),
				),
			),
			'advanced' => array(
				'toggles' => array(
					'chiac_i_header'		=> esc_html__( 'Header', 'chiac-divi-accordions' ),
                    'chiac_i_left_icon'		=> esc_html__( 'Left Icon', 'chiac-divi-accordions' ),
                    'chiac_i_right_icon' 	=> esc_html__( 'Right Icon', 'chiac-divi-accordions' ),
					'chiac_i_title'			=> esc_html__( 'Title', 'chiac-divi-accordions' ),
					'chiac_i_body'			=> esc_html__( 'Body', 'chiac-divi-accordions' ),
                    'chiac_i_header_text' 	=> array(
						'title'    => esc_html__( 'Header Text', 'chiac-divi-accordions' ),
						'tabbed_subtoggles' => true,
						'sub_toggles' => array(
							'title' => array(
								'name' => 'Title',
							),
							'subtitle' => array(
								'name' => 'Subtitle',
							),
						),
					),
                    'chiac_i_content_heading' => array(
						'title'    => esc_html__( 'Body Headings', 'chiac-divi-accordions' ),
						'tabbed_subtoggles' => true,
						'sub_toggles' => array(
							'h1' => array(
								'name' => 'H1',
								'icon' => 'text-h1',
							),
							'h2' => array(
								'name' => 'H2',
								'icon' => 'text-h2',
							),
							'h3' => array(
								'name' => 'H3',
								'icon' => 'text-h3',
							),
							'h4' => array(
								'name' => 'H4',
								'icon' => 'text-h4',
							),
							'h5' => array(
								'name' => 'H5',
								'icon' => 'text-h5',
							),
							'h6' => array(
								'name' => 'H6',
								'icon' => 'text-h6',
							),
						),
					),
					'default'	=> array(
						'title' => esc_html__( 'Body Text', 'chiac-divi-accordions' ),
					),
				)
			)
		);
        
        // Add help videos (to be added soon)
		$this->help_videos = array(
			array(
//				'id'   => esc_html__( '', 'chiac-divi-accordions' ), // YouTube video ID
//				'name' => esc_html__( 'Divi Accordions Plus Module Video', 'chiac-divi-accordions' ),
			),
		);

	}
    
    /**
	 * Module's specific fields
	 *
	 * @since 1.0.0
	 *
	 * @return array
	 */
	public function get_fields() {
		return array(
			'chiac_toggle_speed' => array(
				'label'           => esc_html__( 'Toggle open/close speed', 'chiac-divi-accordions' ),
				'type'            => 'range',
				'option_category' => 'configuration',
				'default'         => '200',
				'default_unit'    => '',
                'description'     => esc_html__( 'Set the opening and closing speed of the accordion item' ),
				'default_on_front' => '',
				'range_settings'  => array(
					'min'  => '0',
					'max'  => '3000',
					'step' => '50',
				),
				'tab_slug'        => 'general',
				'toggle_slug'     => 'chiac_toggles_speed',
			),
			'chiac_mode' => array(
				'label'             => esc_html__( 'Accordion Mode', 'chiac-divi-accordions' ),
                'description'       => esc_html__( 'Allow single or multiple open toggles', 'chiac-divi-accordions' ),
				'type'              => 'select',
				'option_category'   => 'configuration',
				'options'           => array(
					'multiple'   => esc_html__( 'Multiple Active Toggles', 'chiac-divi-accordions' ),
					'single'     => esc_html__( 'Single Active Toggle', 'chiac-divi-accordions' ),
				),
//				'priority'          => 80,
				'default'           => 'multiple',
				'default_on_front'  => '',
				'tab_slug'          => 'general',
				'toggle_slug'       => 'chiac_accordion_mode',
			),
            'chiac_i_header_bg_clr' => array(
				'label'           => esc_html__( 'Item Header Background Color', 'chiac-divi-accordions' ),
				'type'            => 'color-alpha',
				'option_category' => 'basic_option',
                'mobile_options'  => true,
                'responsive'      => true,
                'custom_color'    => true,
                'description'     => esc_html__('Set the background color for the accordion item header.', 'chiac-divi-accordions'),
                'default'         => '',
				'tab_slug'        => 'advanced',
				'toggle_slug'     => 'chiac_i_header',
				// 'hover' 		  => 'tabs' // not working yet
			),
            'chiac_i_header_bg_clr_o' => array(
				'label'           => esc_html__( 'Item Header Background Color(Open Toggle)', 'chiac-divi-accordions' ),
				'type'            => 'color-alpha',
				'option_category' => 'basic_option',
                'mobile_options'  => true,
                'responsive'      => true,
                'custom_color'    => true,
                'description'     => esc_html__('Set the item header background color for open toggle.', 'chiac-divi-accordions'),
                'default'         => '',
				'tab_slug'        => 'advanced',
				'toggle_slug'     => 'chiac_i_header',
			),
            'chiac_i_header_margin' => array(
				'label'             => esc_html__('Item Header Margin', 'chiac-divi-accordions'),
				'description'     	=> esc_html__( 'Margin adds extra space to the outside of the element, increasing the distance between the element and other items on the page.', 'chiac-divi-accordions' ),
				'type'              => 'custom_margin',
				'option_category' 	=> 'layout',
                'mobile_options'    => true,
                'responsive'        => true,
				// 'default'           => '0px|0px|0px|0px|false|false',
                'default_unit'      => 'px',
                'tab_slug'          => 'advanced',
                'toggle_slug'       => 'chiac_i_header',
            ),
            'chiac_i_header_padding' => array(
                'label'             => esc_html__('Item Header Padding', 'chiac-divi-accordions'),
				'description'     	=> esc_html__( 'Padding adds extra space to the inside of the element, increasing the distance between the edge of the element and its inner contents.', 'chiac-divi-accordions' ),
                'type'              => 'custom_padding',
				'option_category' 	=> 'layout',
                'mobile_options'    => true,
                'responsive'        => true,
//                'default'           => '0px|0px|0px|0px|false|false',
                'default_unit'      => 'px',
                'tab_slug'          => 'advanced',
                'toggle_slug'       => 'chiac_i_header',
			),
			'chiac_i_left_icon_clr' => array(
				'label'           => esc_html__( 'Left Icon Color', 'chiac-divi-accordions' ),
				'type'            => 'color-alpha',
				'option_category' => 'basic_option',
                'mobile_options'  => true,
                'responsive'      => true,
                'custom_color'    => true,
                'description'     => esc_html__('Set the left icon color.', 'chiac-divi-accordions'),
                'default'         => '',
				'tab_slug'        => 'advanced',
				'toggle_slug'     => 'chiac_i_left_icon',
			),
			'chiac_i_left_icon_clr_o' => array(
				'label'           => esc_html__( 'Left Icon Color(Open Toggle)', 'chiac-divi-accordions' ),
				'type'            => 'color-alpha',
				'option_category' => 'basic_option',
                'mobile_options'  => true,
                'responsive'      => true,
                'custom_color'    => true,
                'description'     => esc_html__('Set the left icon color for open toggle.', 'chiac-divi-accordions'),
                'default'         => '',
				'tab_slug'        => 'advanced',
				'toggle_slug'     => 'chiac_i_left_icon',
			),
			'chiac_i_left_icon_bg_clr' => array(
				'label'           => esc_html__( 'Left Icon Background Color', 'chiac-divi-accordions' ),
				'type'            => 'color-alpha',
				'option_category' => 'basic_option',
                'mobile_options'  => true,
                'responsive'      => true,
                'custom_color'    => true,
                'description'     => esc_html__('Set the left icon background color.', 'chiac-divi-accordions'),
                'default'         => '',
				'tab_slug'        => 'advanced',
				'toggle_slug'     => 'chiac_i_left_icon',
			),
			'chiac_i_left_icon_bg_clr_o' => array(
				'label'           => esc_html__( 'Left Icon Background Color(Open Toggle)', 'chiac-divi-accordions' ),
				'type'            => 'color-alpha',
				'option_category' => 'basic_option',
                'mobile_options'  => true,
                'responsive'      => true,
                'custom_color'    => true,
                'description'     => esc_html__('Set the left icon background color for open toggle.', 'chiac-divi-accordions'),
                'default'         => '',
				'tab_slug'        => 'advanced',
				'toggle_slug'     => 'chiac_i_left_icon',
			),
            'chiac_i_left_icon_margin' => array(
				'label'             => esc_html__('Left Icon Margin', 'chiac-divi-accordions'),
				'description'     	=> esc_html__( 'Margin adds extra space to the outside of the element, increasing the distance between the element and other items on the page.', 'chiac-divi-accordions' ),
				'type'              => 'custom_margin',
				'option_category' 	=> 'layout',
                'mobile_options'    => true,
                'responsive'        => true,
				// 'default'           => '0px|0px|0px|0px|false|false',
                'default_unit'      => 'px',
                'tab_slug'          => 'advanced',
                'toggle_slug'       => 'chiac_i_left_icon',
            ),
            'chiac_i_left_icon_padding' => array(
                'label'             => esc_html__('Left Icon Padding', 'chiac-divi-accordions'),
				'description'     	=> esc_html__( 'Padding adds extra space to the inside of the element, increasing the distance between the edge of the element and its inner contents.', 'chiac-divi-accordions' ),
                'type'              => 'custom_padding',
				'option_category' 	=> 'layout',
                'mobile_options'    => true,
                'responsive'        => true,
				'default'           => '20px|20px|20px|20px|false|false',
                'default_unit'      => 'px',
                'tab_slug'          => 'advanced',
                'toggle_slug'       => 'chiac_i_left_icon',
			),
			'chiac_i_right_icon_clr' => array(
				'label'           => esc_html__( 'Right Icon Color', 'chiac-divi-accordions' ),
				'type'            => 'color-alpha',
				'option_category' => 'basic_option',
                'mobile_options'  => true,
                'responsive'      => true,
                'custom_color'    => true,
                'description'     => esc_html__('Set the right icon color.', 'chiac-divi-accordions'),
                'default'         => '',
				'tab_slug'        => 'advanced',
				'toggle_slug'     => 'chiac_i_right_icon',
			),
			'chiac_i_right_icon_clr_o' => array(
				'label'           => esc_html__( 'Right Icon Color(Open Toggle)', 'chiac-divi-accordions' ),
				'type'            => 'color-alpha',
				'option_category' => 'basic_option',
                'mobile_options'  => true,
                'responsive'      => true,
                'custom_color'    => true,
                'description'     => esc_html__('Set the right icon color for open toggle.', 'chiac-divi-accordions'),
                'default'         => '',
				'tab_slug'        => 'advanced',
				'toggle_slug'     => 'chiac_i_right_icon',
			),
			'chiac_i_right_icon_bg_clr' => array(
				'label'           => esc_html__( 'Right Icon Background Color', 'chiac-divi-accordions' ),
				'type'            => 'color-alpha',
				'option_category' => 'basic_option',
                'mobile_options'  => true,
                'responsive'      => true,
                'custom_color'    => true,
                'description'     => esc_html__('Set the right icon background color.', 'chiac-divi-accordions'),
                'default'         => '',
				'tab_slug'        => 'advanced',
				'toggle_slug'     => 'chiac_i_right_icon',
			),
			'chiac_i_right_icon_bg_clr_o' => array(
				'label'           => esc_html__( 'Right Icon Background Color(Open Toggle)', 'chiac-divi-accordions' ),
				'type'            => 'color-alpha',
				'option_category' => 'basic_option',
                'mobile_options'  => true,
                'responsive'      => true,
                'custom_color'    => true,
                'description'     => esc_html__('Set the right icon background color for open toggle.', 'chiac-divi-accordions'),
                'default'         => '',
				'tab_slug'        => 'advanced',
				'toggle_slug'     => 'chiac_i_right_icon',
			),
            'chiac_i_right_icon_margin' => array(
				'label'             => esc_html__('Right Icon Margin', 'chiac-divi-accordions'),
				'description'     	=> esc_html__( 'Margin adds extra space to the outside of the element, increasing the distance between the element and other items on the page.', 'chiac-divi-accordions' ),
				'type'              => 'custom_margin',
				'option_category' 	=> 'layout',
                'mobile_options'    => true,
                'responsive'        => true,
				// 'default'           => '0px|0px|0px|0px|false|false',
                'default_unit'      => 'px',
                'tab_slug'          => 'advanced',
                'toggle_slug'       => 'chiac_i_right_icon',
            ),
            'chiac_i_right_icon_padding' => array(
                'label'             => esc_html__('Right Icon Padding', 'chiac-divi-accordions'),
				'description'     	=> esc_html__( 'Padding adds extra space to the inside of the element, increasing the distance between the edge of the element and its inner contents.', 'chiac-divi-accordions' ),
                'type'              => 'custom_padding',
				'option_category' 	=> 'layout',
                'mobile_options'    => true,
                'responsive'        => true,
				'default'           => '20px|20px|20px|20px|false|false',
                'default_unit'      => 'px',
                'tab_slug'          => 'advanced',
                'toggle_slug'       => 'chiac_i_right_icon',
			),
			'chiac_i_title_bg_clr' => array(
				'label'           => esc_html__( 'Item Title Background Color', 'chiac-divi-accordions' ),
				'type'            => 'color-alpha',
				'option_category' => 'basic_option',
                'mobile_options'  => true,
                'responsive'      => true,
                'custom_color'    => true,
                'description'     => esc_html__('Set the background color for the accordion item title.', 'chiac-divi-accordions'),
                'default'         => '',
				'tab_slug'        => 'advanced',
				'toggle_slug'     => 'chiac_i_title',
			),
			'chiac_i_title_bg_clr_o' => array(
				'label'           => esc_html__( 'Item Title Background Color(Open Toggle)', 'chiac-divi-accordions' ),
				'type'            => 'color-alpha',
				'option_category' => 'basic_option',
                'mobile_options'  => true,
                'responsive'      => true,
                'custom_color'    => true,
                'description'     => esc_html__('Set the item title background color for open toggle.', 'chiac-divi-accordions'),
                'default'         => '',
				'tab_slug'        => 'advanced',
				'toggle_slug'     => 'chiac_i_title',
			),
            'chiac_i_title_margin' => array(
				'label'             => esc_html__('Item Title Margin', 'chiac-divi-accordions'),
				'description'     	=> esc_html__( 'Margin adds extra space to the outside of the element, increasing the distance between the element and other items on the page.', 'chiac-divi-accordions' ),
				'type'              => 'custom_margin',
				'option_category' 	=> 'layout',
                'mobile_options'    => true,
                'responsive'        => true,
				// 'default'           => '0px|0px|0px|0px|false|false',
                'default_unit'      => 'px',
                'tab_slug'          => 'advanced',
                'toggle_slug'       => 'chiac_i_title',
            ),
            'chiac_i_title_padding' => array(
                'label'             => esc_html__('Item Title Padding', 'chiac-divi-accordions'),
				'description'     	=> esc_html__( 'Padding adds extra space to the inside of the element, increasing the distance between the edge of the element and its inner contents.', 'chiac-divi-accordions' ),
                'type'              => 'custom_padding',
				'option_category' 	=> 'layout',
                'mobile_options'    => true,
                'responsive'        => true,
				'default'           => '8px|20px|8px|20px|false|false',
                'default_unit'      => 'px',
                'tab_slug'          => 'advanced',
                'toggle_slug'       => 'chiac_i_title',
			),
			'chiac_i_body_bg_clr' => array(
				'label'           => esc_html__( 'Item Body', 'chiac-divi-accordions' ),
				'type'            => 'color-alpha',
				'option_category' => 'basic_option',
                'mobile_options'  => true,
                'responsive'      => true,
                'custom_color'    => true,
                'description'     => esc_html__('Set the background color for the accordion item body.', 'chiac-divi-accordions'),
                'default'         => '',
				'tab_slug'        => 'advanced',
				'toggle_slug'     => 'chiac_i_body',
			),
            'chiac_i_body_margin' => array(
				'label'             => esc_html__('Item Body Margin', 'chiac-divi-accordions'),
				'description'     	=> esc_html__( 'Margin adds extra space to the outside of the element, increasing the distance between the element and other items on the page.', 'chiac-divi-accordions' ),
				'type'              => 'custom_margin',
				'option_category' 	=> 'layout',
                'mobile_options'    => true,
                'responsive'        => true,
				// 'default'           => '0px|0px|0px|0px|false|false',
                'default_unit'      => 'px',
                'tab_slug'          => 'advanced',
                'toggle_slug'       => 'chiac_i_body',
            ),
            'chiac_i_body_padding' => array(
                'label'             => esc_html__('Item Body Padding', 'chiac-divi-accordions'),
				'description'     	=> esc_html__( 'Padding adds extra space to the inside of the element, increasing the distance between the edge of the element and its inner contents.', 'chiac-divi-accordions' ),
                'type'              => 'custom_padding',
				'option_category' 	=> 'layout',
                'mobile_options'    => true,
                'responsive'        => true,
				'default'           => '20px|20px|20px|20px|false|false',
                'default_unit'      => 'px',
                'tab_slug'          => 'advanced',
                'toggle_slug'       => 'chiac_i_body',
			),
            'chiac_i_item_margin' => array(
				'label'             => esc_html__('Item Margin', 'chiac-divi-accordions'),
				'description'     	=> esc_html__( 'Margin adds extra space to the outside of the element, increasing the distance between the element and other items on the page.', 'chiac-divi-accordions' ),
				'type'              => 'custom_margin',
				'option_category' 	=> 'layout',
                'mobile_options'    => true,
                'responsive'        => true,
				// 'default'           => '0px|0px|0px|0px|false|false',
                'default_unit'      => 'px',
                'tab_slug'          => 'advanced',
                'toggle_slug'       => 'margin_padding',
            ),
            'chiac_i_item_padding' => array(
                'label'             => esc_html__('Item Padding', 'chiac-divi-accordions'),
				'description'     	=> esc_html__( 'Padding adds extra space to the inside of the element, increasing the distance between the edge of the element and its inner contents.', 'chiac-divi-accordions' ),
                'type'              => 'custom_padding',
				'option_category' 	=> 'layout',
                'mobile_options'    => true,
                'responsive'        => true,
				// 'default'           => '20px|20px|20px|20px|false|false',
                'default_unit'      => 'px',
                'tab_slug'          => 'advanced',
                'toggle_slug'       => 'margin_padding',
			),
		);
	}
    
    // Advanced fields
	public function get_advanced_fields_config() {
        
        // DB plugin main class
		$this->main_css_element = '%%order_class%%';
		
		// Advanced fields
		$advanced_fields = array();
		
		/* remove default text options */
		$advanced_fields['text']  = false;

		// Body Text
		$advanced_fields['fonts']['default'] = array(
			'label' => esc_html__( 'Body', 'chiac-divi-accordions' ),
			'css'   => array(
				'main' => "{$this->main_css_element}  .chiac-content",
				// 'plugin_main' => "{$this->main_css_element}  .chiac-content",
			),
			'list-style-type' => "{$this->main_css_element}  .chiac-content ul li",
			'toggle_slug' => 'default',
			// 'sub_toggle'  => 'p',
			'block_elements'   => array(
				'tabbed_subtoggles' => true,
			),
		);

		// Spacing
		$advanced_fields['margin_padding'] = array(
			'css'   => array(
				'main'          => "{$this->main_css_element}",
				// 'plugin_main'   => "{$this->main_css_element}",
				'important'     => 'all',
			),
			// 'label_prefix'    => esc_html__( 'Main', 'chiac-divi-accordions' ),
		);

		// Borders
		$advanced_fields['borders'] = array(
			'default'   => array(
				'css'   => array(
					'main'          => ".chiac_divi_accordions{$this->main_css_element}",
					'important'     => 'all',
				),
				'defaults' => array(
					'border_radii' => 'on||||',
					'border_styles' => array(
						'width' => '0px',
						'color' => '#d9d9d9',
						'style' => 'solid',
					),
				),
				'label_prefix'    => esc_html__( 'Main', 'chiac-divi-accordions' ),
			),
		);

		// Box Shadow
		$advanced_fields['box_shadow'] = array(
			'default' => array(
				'css' => array(
					'main' => "{$this->main_css_element}",
				),
			),
		);
			
		// Item Header border options
		$advanced_fields['borders']['chiac_i_header'] = array(
			'css' => array(
				'main' => array(
					'border_radii' => "{$this->main_css_element} .chiac-header",
					'border_styles' => "{$this->main_css_element} .chiac-header",
				),
			),
            'defaults' => array(
                'border_radii' => 'on||||',
                'border_styles' => array(
                    'width' => '0px',
                    'color' => '#cccccc',
                    'style' => 'solid',
                ),
            ),
			'label_prefix'    => esc_html__( 'Item Header', 'chiac-divi-accordions' ),
			'tab_slug'        => 'advanced',
			'toggle_slug'     => 'chiac_i_header',
		);

		// Item Left Icon border options
		$advanced_fields['borders']['chiac_i_left_icon'] = array(
			'css' => array(
				'main' => array(
					'border_radii' => "{$this->main_css_element} .chiac-header .chiac_left_icon",
					'border_styles' => "{$this->main_css_element} .chiac-header .chiac_left_icon",
				),
			),
            'defaults' => array(
                'border_radii' => 'on||||',
                'border_styles' => array(
                    'width' => '0px',
                    'color' => '#f4f4f4',
                    'style' => 'solid',
                ),
            ),
            'priority'      => 10,
			'label_prefix'    => esc_html__( 'Item Left Icon', 'chiac-divi-accordions' ),
            // 'depends_on'      => array( 'chiac_l_icon_state' ),
            // 'depends_show_if' => 'on',
			'tab_slug'        => 'advanced',
			'toggle_slug'     => 'chiac_i_left_icon',
		);

		// Item Right Icon border options
		$advanced_fields['borders']['chiac_i_right_icon'] = array(
			'css' => array(
				'main' => array(
					'border_radii' => "{$this->main_css_element} .chiac-header .chiac_right_icon",
					'border_styles' => "{$this->main_css_element} .chiac-header .chiac_right_icon",
				)
			),
            'defaults' => array(
                'border_radii' => 'on||||',
                'border_styles' => array(
                    'width' => '0px',
                    'color' => '#f4f4f4',
                    'style' => 'solid',
                ),
            ),
			'label_prefix'    => esc_html__( 'Item Right Icon', 'chiac-divi-accordions' ),
            // 'depends_on'      => array( 'chiac_r_icon_state' ),
            // 'depends_show_if' => 'on',
			'tab_slug'        => 'advanced',
			'toggle_slug'     => 'chiac_i_right_icon',
		);

		// Item title border options
		$advanced_fields['borders']['chiac_i_title'] = array(
			'css' => array(
				'main' => array(
					'border_radii' => "{$this->main_css_element} .chiac-header .chiac_heading_container",
					'border_styles' => "{$this->main_css_element} .chiac-header .chiac_heading_container",
				)
			),
            'defaults' => array(
                'border_radii' => 'on||||',
                'border_styles' => array(
                    'width' => '0px',
                    'color' => '#f4f4f4',
                    'style' => 'solid',
                ),
            ),
			'label_prefix'    => esc_html__( 'Item Title', 'chiac-divi-accordions' ),
			'tab_slug'        => 'advanced',
			'toggle_slug'     => 'chiac_i_title',
		);

		// Item Body(content) border options
		$advanced_fields['borders']['chiac_i_content'] = array(
			'css' => array(
				'main' => array(
					'border_radii' => "{$this->main_css_element} .chiac-content",
					'border_styles' => "{$this->main_css_element} .chiac-content",
				)
			),
            'defaults' => array(
                'border_radii' => 'on||||',
                'border_styles' => array(
                    'width' => '0px',
                    'color' => '#f4f4f4',
                    'style' => 'solid',
                ),
            ),
			'label_prefix'    => esc_html__( 'Item Body', 'chiac-divi-accordions' ),
			'tab_slug'        => 'advanced',
			'toggle_slug'     => 'chiac_i_body',
		);

		// Item Main border options
		$advanced_fields['borders']['chiac_i_item'] = array(
			'css' => array(
				'main' => array(
					'border_radii' => "{$this->main_css_element} .chiac_divi_accordions_item",
					'border_styles' => "{$this->main_css_element} .chiac_divi_accordions_item",
				)
			),
            'defaults' => array(
                'border_radii' => 'on||||',
                'border_styles' => array(
                    'width' => '1px',
                    'color' => '#ccc',
                    'style' => 'solid',
                ),
            ),
			'label_prefix'    => esc_html__( 'Item', 'chiac-divi-accordions' ),
			'tab_slug'        => 'advanced',
			'toggle_slug'     => 'border',
		);

		// Header box-shadow options
		$advanced_fields['box_shadow']['chiac_i_header'] = array(
			'css' => array(
				'main' => "{$this->main_css_element} .chiac-header",
			),
			'label_prefix'    => esc_html__( 'Header', 'chiac-divi-accordions' ),
			'tab_slug'        => 'advanced',
			'toggle_slug'     => 'chiac_i_header',
		);

        // Left Icon box-shadow options
		$advanced_fields['box_shadow']['chiac_i_left_icon'] = array(
			'css' => array(
				'main' => "{$this->main_css_element} .chiac-header .chiac_left_icon",
			),
			'label_prefix'    => esc_html__( 'Left Icon', 'chiac-divi-accordions' ),
			'tab_slug'        => 'advanced',
			'toggle_slug'     => 'chiac_i_left_icon',
		);
		
        // Right Icon box-shadow options
		$advanced_fields['box_shadow']['chiac_i_right_icon'] = array(
			'css' => array(
				'main' => "{$this->main_css_element} .chiac-header .chiac_right_icon",
			),
			'label_prefix'    => esc_html__( 'Right Icon', 'chiac-divi-accordions' ),
			'tab_slug'        => 'advanced',
			'toggle_slug'     => 'chiac_i_right_icon',
		);

        // Title box-shadow options
		$advanced_fields['box_shadow']['chiac_i_title'] = array(
			'css' => array(
				'main' => "{$this->main_css_element} .chiac-header .chiac_heading_container",
			),
			'label_prefix'    => esc_html__( 'Title', 'chiac-divi-accordions' ),
			'tab_slug'        => 'advanced',
			'toggle_slug'     => 'chiac_i_title',
		);

        // Body box-shadow options
		$advanced_fields['box_shadow']['chiac_i_content'] = array(
			'css' => array(
				'main' => "{$this->main_css_element} .chiac-content",
			),
			'label_prefix'    => esc_html__( 'Body', 'chiac-divi-accordions' ),
			'tab_slug'        => 'advanced',
			'toggle_slug'     => 'chiac_i_body',
		);

        // Item Main box-shadow options
		$advanced_fields['box_shadow']['chiac_i_item'] = array(
			'css' => array(
				'main' => "{$this->main_css_element} .chiac_divi_accordions_item",
			),
			'label'    => esc_html__( 'Item Box shadow', 'chiac-divi-accordions' ),
			'label_prefix'    => esc_html__( 'Item', 'chiac-divi-accordions' ),
			'tab_slug'        => 'advanced',
			'toggle_slug'     => 'box_shadow',
		);

		/**
         * Header Text
		 * 
		 * @since	v1.3.0
         */
		// Item Header Title
        $advanced_fields['fonts']['chiac_i_title'] = array(
			'label'    => esc_html__( 'Heading', 'chiac-divi-accordions' ),
			'css'      => array(
				'main' => "{$this->main_css_element} .chiac-title",
                // 'important' => 'all'
			),
            'header_level' => array(
                'default' => 'h4',
            ),
            'label'        => esc_html__( 'Title', 'chiac-divi-accordions' ),
			'tab_slug'    => 'advanced',
            'toggle_slug' => 'chiac_i_header_text',
			'sub_toggle'  => 'title',
		);
		// Item Header Subtitle
        $advanced_fields['fonts']['chiac_i_subtitle'] = array(
			'label'    => esc_html__( 'Subheading', 'chiac-divi-accordions' ),
			'css'      => array(
				'main' => "{$this->main_css_element} .chiac-subtitle",
                // 'important' => 'all'
			),
            'header_level' => array(
                'default' => 'h6',
            ),
            'label'        => esc_html__( 'Subtitle', 'chiac-divi-accordions' ),
			'tab_slug'    => 'advanced',
            'toggle_slug' => 'chiac_i_header_text',
			'sub_toggle'  => 'subtitle',
		);

		/**
		 * Body Headings
		 * 
		 * @since	v1.4.0
		 */
		// Body heading: H1
		$advanced_fields['fonts']['chiac_i_h1'] = array(
			'label'    => esc_html__( 'Heading 1', 'chiac-divi-accordions' ),
			'css'      => array(
				'main' => "{$this->main_css_element} .chiac-content h1",
			),
			'font_size' => array(
				'default' => absint( et_get_option( 'body_header_size', '30' ) ) . 'px',
				// 'default_on_child' => true,
			),
			'letter_spacing' => array(
				'default' => '0px',
				// 'default_on_child' => true,
			),
			'line_height' => array(
				'default' => '1em',
				// 'default_on_child' => true,
			),
			'tab_slug'    => 'advanced',
			'toggle_slug' => 'chiac_i_content_heading',
			'sub_toggle'  => 'h1',
		);
		// Body heading: H2
		$advanced_fields['fonts']['chiac_i_h2'] = array(
            'label'    => esc_html__( 'Heading 2', 'chiac-divi-accordions' ),
            'css'      => array(
				'main' => "{$this->main_css_element} .chiac-content h2",
            ),
            'font_size' => array(
                'default' => '26px',                
                // 'default_on_child' => true,
            ),
            'letter_spacing' => array(
                'default' => '0px',
                // 'default_on_child' => true,
            ),
            'line_height' => array(
                'default' => '1em',
                // 'default_on_child' => true,
            ),
			'tab_slug'    => 'advanced',
            'toggle_slug' => 'chiac_i_content_heading',
            'sub_toggle'  => 'h2',
        );
		// Body heading: H3
		$advanced_fields['fonts']['chiac_i_h3'] = array(
            'label'    => esc_html__( 'Heading 3', 'chiac-divi-accordions' ),
            'css'      => array(
				'main' => "{$this->main_css_element} .chiac-content h3",
            ),
            'font_size' => array(
                'default' => '22px',                
                // 'default_on_child' => true,
            ),
            'letter_spacing' => array(
                'default' => '0px',
                // 'default_on_child' => true,
            ),
            'line_height' => array(
                'default' => '1em',
                // 'default_on_child' => true,
            ),
			'tab_slug'    => 'advanced',
            'toggle_slug' => 'chiac_i_content_heading',
            'sub_toggle'  => 'h3',
        );
		// Body heading: H4
		$advanced_fields['fonts']['chiac_h4'] = array(
            'label'    => esc_html__( 'Heading 4', 'chiac-divi-accordions' ),
            'css'      => array(
				'main' => "{$this->main_css_element} .chiac-content h4",
            ),
            'font_size' => array(
                'default' => '18px',                
                // 'default_on_child' => true,
            ),
            'letter_spacing' => array(
                'default' => '0px',
                // 'default_on_child' => true,
            ),
            'line_height' => array(
                'default' => '1em',
                // 'default_on_child' => true,
            ),
			'tab_slug'    => 'advanced',
            'toggle_slug' => 'chiac_i_content_heading',
            'sub_toggle'  => 'h4',
        );
		// Body heading: H5
		$advanced_fields['fonts']['chiac_h5'] = array(
            'label'    => esc_html__( 'Heading 5', 'chiac-divi-accordions' ),
            'css'      => array(
				'main' => "{$this->main_css_element} .chiac-content h5",
            ),
            'font_size' => array(
                'default' => '16px',                
                // 'default_on_child' => true,
            ),
            'letter_spacing' => array(
                'default' => '0px',
                // 'default_on_child' => true,
            ),
            'line_height' => array(
                'default' => '1em',
                // 'default_on_child' => true,
            ),
			'tab_slug'    => 'advanced',
            'toggle_slug' => 'chiac_i_content_heading',
            'sub_toggle'  => 'h5',
        );
		// Body heading: H6
		$advanced_fields['fonts']['chiac_h6'] = array(
            'label'    => esc_html__( 'Heading 6', 'chiac-divi-accordions' ),
            'css'      => array(
				'main' => "{$this->main_css_element} .chiac-content h6",
            ),
            'font_size' => array(
                'default' => '14px',                
                // 'default_on_child' => true,
            ),
            'letter_spacing' => array(
                'default' => '0px',
                // 'default_on_child' => true,
            ),
            'line_height' => array(
                'default' => '1em',
                // 'default_on_child' => true,
            ),
			'tab_slug'    => 'advanced',
            'toggle_slug' => 'chiac_i_content_heading',
            'sub_toggle'  => 'h6',
		);

        return $advanced_fields;
        
	}
    
    /**
	 * Module's custom CSS fields configuration
	 *
	 * @since 1.0.0
	 *
	 * @return array
	 */
    public function get_custom_css_fields_config() {
		return array(
            
		);
	}

	/**
	 * Returns the generate_responsive_css() helper method.
	 * 
	 * @see		Chiac_Divi_Modules_Helper class.
	 * 
	 * @since	1.2.0
	 * 
	 * @param 	string 	$render_slug    		Module slug.
	 * @param 	array  	$args					Arguments.
	 *
	 */
	public function generate_responsive_css( $render_slug, $args ){

		return chiac_modules_helper_methods()->generate_responsive_css( $this->props, $render_slug, $args );

	}

	/**
	 * Returns the generate_responsive_css_SPACING() helper method.
	 * 
	 * @see		Chiac_Divi_Modules_Helper class.
	 * 
	 * @since	1.2.0
	 * 
	 * @param 	string 	$render_slug    		Module slug.
	 * @param 	array  	$args					Arguments.
	 *
	 */
	public function generate_responsive_css_SPACING( $render_slug, $args ){

		return chiac_modules_helper_methods()->generate_responsive_css_SPACING( $this->props, $render_slug, $args );

	}
	
	/**
	 * Make certain props global.
	 * 
	 * Make certain parent module props global
	 * to make it possible to access them from within the child module.
	 * 
	 * @since	v1.3.0  
	 */
	function before_render() {

		global $chiac_parent_module_global_props;

		// Item title heading level
		$chiac_i_title_level = isset($this->props['chiac_i_title_level']) ? $this->props['chiac_i_title_level'] : "h4";

		// Item subtitle heading level
		$chiac_i_subtitle_level = isset($this->props['chiac_i_subtitle_level']) ? $this->props['chiac_i_subtitle_level'] : "h6";

		// global props array
		$chiac_parent_module_global_props = array(
			'chiac_i_title_level' 		=> $chiac_i_title_level,
			'chiac_i_subtitle_level' 	=> $chiac_i_subtitle_level,
		);

	}

    /**
	 * Render module output.
	 *
	 * @since 	1.0.0
	 *
	 * @param 	array  	$attrs       List of unprocessed attributes.
	 * @param 	string 	$content     Content being processed.
	 * @param 	string 	$render_slug Slug of module that is used for rendering output.
	 *
	 * @return 	string 	module's rendered output
	 */
	public function render( $attrs, $content = null, $render_slug ) {

		// print_r($this->props);

		$chiac_i_header_bg_clr		 = $this->props['chiac_i_header_bg_clr'];
		$chiac_i_header_bg_clr_o	 = $this->props['chiac_i_header_bg_clr_o'];
		$chiac_i_left_icon_clr		 = $this->props['chiac_i_left_icon_clr'];
		$chiac_i_left_icon_clr_o	 = $this->props['chiac_i_left_icon_clr_o'];
		$chiac_i_left_icon_bg_clr	 = $this->props['chiac_i_left_icon_bg_clr'];
		$chiac_i_left_icon_bg_clr_o	 = $this->props['chiac_i_left_icon_bg_clr_o'];
		$chiac_i_right_icon_clr		 = $this->props['chiac_i_right_icon_clr'];
		$chiac_i_right_icon_clr_o	 = $this->props['chiac_i_right_icon_clr_o'];
		$chiac_i_right_icon_bg_clr	 = $this->props['chiac_i_right_icon_bg_clr'];
		$chiac_i_right_icon_bg_clr_o = $this->props['chiac_i_right_icon_bg_clr_o'];
		$chiac_i_title_bg_clr		 = $this->props['chiac_i_title_bg_clr'];
		$chiac_i_title_bg_clr_o		 = $this->props['chiac_i_title_bg_clr_o'];
		$chiac_i_body_bg_clr		 = $this->props['chiac_i_body_bg_clr'];

		// Process header bg color into style
		$this->generate_responsive_css( $render_slug, 
			array( 
				'setting' 			=> 'chiac_i_header_bg_clr',
				'selector' 			=> "{$this->main_css_element} .chiac-header",
				'property' 			=> 'background-color',
				'additional_css'	=> '',
				'field_type'		=> 'color',
				'priority'			=> ''
			)
		);

		// Process open toggle header bg color into style
		if ( $chiac_i_header_bg_clr !== $chiac_i_header_bg_clr_o ) {
			$this->generate_responsive_css( $render_slug, 
				array( 
					'setting' 			=> 'chiac_i_header_bg_clr_o',
					'selector' 			=> "{$this->main_css_element} .chiac-header.chiac_opened",
					'property' 			=> 'background-color',
					'additional_css'	=> '',
					'field_type'		=> 'color',
					'priority'			=> ''
				)
			);
		}

		// Process left icon color into style
		$this->generate_responsive_css( $render_slug, 
			array( 
				'setting' 			=> 'chiac_i_left_icon_clr',
				'selector' 			=> "{$this->main_css_element} .chiac-header .chiac_left_icon",
				'property' 			=> 'color',
				'additional_css'	=> '',
				'field_type'		=> 'color',
				'priority'			=> ''
			)
		);

		// Process open toggle left icon color into style
		if ( $chiac_i_left_icon_clr !== $chiac_i_left_icon_clr_o ) {
			$this->generate_responsive_css( $render_slug, 
				array( 
					'setting' 			=> 'chiac_i_left_icon_clr_o',
					'selector' 			=> "{$this->main_css_element} .chiac-header.chiac_opened .chiac_left_icon",
					'property' 			=> 'color',
					'additional_css'	=> '',
					'field_type'		=> 'color',
					'priority'			=> ''
				)
			);
		}

		// Process left icon background color into style
		$this->generate_responsive_css( $render_slug, 
			array( 
				'setting' 			=> 'chiac_i_left_icon_bg_clr',
				'selector' 			=> "{$this->main_css_element} .chiac-header .chiac_left_icon",
				'property' 			=> 'background-color',
				'additional_css'	=> '',
				'field_type'		=> 'color',
				'priority'			=> ''
			)
		);

		// Process open toggle left icon background color into style
		if ( $chiac_i_left_icon_bg_clr !== $chiac_i_left_icon_bg_clr_o ) {
			$this->generate_responsive_css( $render_slug, 
				array( 
					'setting' 			=> 'chiac_i_left_icon_bg_clr_o',
					'selector' 			=> "{$this->main_css_element} .chiac-header.chiac_opened .chiac_left_icon",
					'property' 			=> 'background-color',
					'additional_css'	=> '',
					'field_type'		=> 'color',
					'priority'			=> ''
				)
			);
		}

		// Process right icon color into style
		$this->generate_responsive_css( $render_slug, 
			array( 
				'setting' 			=> 'chiac_i_right_icon_clr',
				'selector' 			=> "{$this->main_css_element} .chiac-header .chiac_right_icon",
				'property' 			=> 'color',
				'additional_css'	=> '',
				'field_type'		=> 'color',
				'priority'			=> ''
			)
		);

		// Process open toggle right icon color into style
		if ( $chiac_i_right_icon_clr !== $chiac_i_right_icon_clr_o ) {
			$this->generate_responsive_css( $render_slug, 
				array( 
					'setting' 			=> 'chiac_i_right_icon_clr_o',
					'selector' 			=> "{$this->main_css_element} .chiac-header.chiac_opened .chiac_right_icon",
					'property' 			=> 'color',
					'additional_css'	=> '',
					'field_type'		=> 'color',
					'priority'			=> ''
				)
			);
		}

		// Process right icon background color into style
		$this->generate_responsive_css( $render_slug, 
			array( 
				'setting' 			=> 'chiac_i_right_icon_bg_clr',
				'selector' 			=> "{$this->main_css_element} .chiac-header .chiac_right_icon",
				'property' 			=> 'background-color',
				'additional_css'	=> '',
				'field_type'		=> 'color',
				'priority'			=> ''
			)
		);

		// Process open toggle right icon background color into style
		if ( $chiac_i_right_icon_bg_clr !== $chiac_i_right_icon_bg_clr_o ) {
			$this->generate_responsive_css( $render_slug, 
				array( 
					'setting' 			=> 'chiac_i_right_icon_bg_clr_o',
					'selector' 			=> "{$this->main_css_element} .chiac-header.chiac_opened .chiac_right_icon",
					'property' 			=> 'background-color',
					'additional_css'	=> '',
					'field_type'		=> 'color',
					'priority'			=> ''
				)
			);
		}
		
		// Process header title background color into style
		$this->generate_responsive_css( $render_slug, 
			array( 
				'setting' 			=> 'chiac_i_title_bg_clr',
				'selector' 			=> "{$this->main_css_element} .chiac_heading_container",
				'property' 			=> 'background-color',
				'additional_css'	=> '',
				'field_type'		=> 'color',
				'priority'			=> ''
			)
		);

		// Process open toggle header title background color into style
		if ( $chiac_i_title_bg_clr !== $chiac_i_title_bg_clr_o ) {
			$this->generate_responsive_css( $render_slug, 
				array( 
					'setting' 			=> 'chiac_i_title_bg_clr_o',
					'selector' 			=> "{$this->main_css_element} .chiac-header.chiac_opened .chiac_heading_container",
					'property' 			=> 'background-color',
					'additional_css'	=> '',
					'field_type'		=> 'color',
					'priority'			=> ''
				)
			);
		}

		// Process header body bg color into style
		$this->generate_responsive_css( $render_slug, 
			array( 
				'setting' 			=> 'chiac_i_body_bg_clr',
				'selector' 			=> "{$this->main_css_element} .chiac-content",
				'property' 			=> 'background-color',
				'additional_css'	=> '',
				'field_type'		=> 'color',
				'priority'			=> ''
			)
		);

		// Process header margin into style
		$this->generate_responsive_css_SPACING( $render_slug, 
			array( 
				'setting' 			=> 'chiac_i_header_margin',
				'selector' 			=> "{$this->main_css_element} .chiac-header",
				'property' 			=> 'margin',
				'additional_css'	=> '',
				'field_type'		=> 'custom_margin',
				'priority'			=> ''
			)
		);

		// Process header padding into style
		$this->generate_responsive_css_SPACING( $render_slug, 
			array( 
				'setting' 			=> 'chiac_i_header_padding',
				'selector' 			=> "{$this->main_css_element} .chiac-header",
				'property' 			=> 'padding',
				'additional_css'	=> '',
				'field_type'		=> 'custom_padding',
				'priority'			=> ''
			)
		);

		// Process left icon margin into style
		$this->generate_responsive_css_SPACING( $render_slug, 
			array( 
				'setting' 			=> 'chiac_i_left_icon_margin',
				'selector' 			=> "{$this->main_css_element} .chiac-header .chiac_left_icon",
				'property' 			=> 'margin',
				'additional_css'	=> '',
				'field_type'		=> 'custom_margin',
				'priority'			=> ''
			)
		);

		// Process left icon padding into style
		$this->generate_responsive_css_SPACING( $render_slug, 
			array( 
				'setting' 			=> 'chiac_i_left_icon_padding',
				'selector' 			=> "{$this->main_css_element} .chiac-header .chiac_left_icon",
				'property' 			=> 'padding',
				'additional_css'	=> '',
				'field_type'		=> 'custom_padding',
				'priority'			=> '',
				'default'           => '20px|20px|20px|20px|false|false'
			)
		);

		// Process right icon margin into style
		$this->generate_responsive_css_SPACING( $render_slug, 
			array( 
				'setting' 			=> 'chiac_i_right_icon_margin',
				'selector' 			=> "{$this->main_css_element} .chiac-header .chiac_right_icon",
				'property' 			=> 'margin',
				'additional_css'	=> '',
				'field_type'		=> 'custom_margin',
				'priority'			=> ''
			)
		);

		// Process right icon padding into style
		$this->generate_responsive_css_SPACING( $render_slug, 
			array( 
				'setting' 			=> 'chiac_i_right_icon_padding',
				'selector' 			=> "{$this->main_css_element} .chiac-header .chiac_right_icon",
				'property' 			=> 'padding',
				'additional_css'	=> '',
				'field_type'		=> 'custom_padding',
				'priority'			=> '',
				'default'           => '20px|20px|20px|20px|false|false'
			)
		);

		// Process title margin into style
		$this->generate_responsive_css_SPACING( $render_slug, 
			array( 
				'setting' 			=> 'chiac_i_title_margin',
				'selector' 			=> "{$this->main_css_element} .chiac-header .chiac_heading_container",
				'property' 			=> 'margin',
				'additional_css'	=> '',
				'field_type'		=> 'custom_margin',
				'priority'			=> ''
			)
		);

		// Process title padding into style
		$this->generate_responsive_css_SPACING( $render_slug, 
			array( 
				'setting' 			=> 'chiac_i_title_padding',
				'selector' 			=> "{$this->main_css_element} .chiac-header .chiac_heading_container",
				'property' 			=> 'padding',
				'additional_css'	=> '',
				'field_type'		=> 'custom_padding',
				'priority'			=> '',
				'default'           => '8px|20px|8px|20px|false|false'
			)
		);

		// Process body margin into style
		$this->generate_responsive_css_SPACING( $render_slug, 
			array( 
				'setting' 			=> 'chiac_i_body_margin',
				'selector' 			=> "{$this->main_css_element} .chiac-content",
				'property' 			=> 'margin',
				'additional_css'	=> '',
				'field_type'		=> 'custom_margin',
				'priority'			=> ''
			)
		);

		// Process body padding into style
		$this->generate_responsive_css_SPACING( $render_slug, 
			array( 
				'setting' 			=> 'chiac_i_body_padding',
				'selector' 			=> "{$this->main_css_element} .chiac-content",
				'property' 			=> 'padding',
				'additional_css'	=> '',
				'field_type'		=> 'custom_padding',
				'priority'			=> '',
				'default'           => '20px|20px|20px|20px|false|false'
			)
		);

		// Process item margin into style
		$this->generate_responsive_css_SPACING( $render_slug, 
			array( 
				'setting' 			=> 'chiac_i_item_margin',
				'selector' 			=> "{$this->main_css_element} .chiac_divi_accordions_item",
				'property' 			=> 'margin',
				'additional_css'	=> '',
				'field_type'		=> 'custom_margin',
				'priority'			=> ''
			)
		);

		// Process item padding into style
		$this->generate_responsive_css_SPACING( $render_slug, 
			array( 
				'setting' 			=> 'chiac_i_item_padding',
				'selector' 			=> "{$this->main_css_element} .chiac_divi_accordions_item",
				'property' 			=> 'padding',
				'additional_css'	=> '',
				'field_type'		=> 'custom_padding',
				'priority'			=> '',
				// 'default'           => '20px|20px|20px|20px|false|false'
			)
		);
        
        /**
         * add/remove content wrapper classes
         */
        $content_wrapper_classes = ["chiac-content-wrapper"];
        
        $toggle_speed = isset( $this->props['chiac_toggle_speed'] ) && $this->props['chiac_toggle_speed'] !== '' ? $this->props['chiac_toggle_speed'] : '200';
        
        // clean the value a bit in case it has units
        $toggle_speed = str_replace( array('px', 'em', 'rem', '%'), array( '', '', '', '' ), $toggle_speed );
        
        // create toggle speed class
        $toggle_speed_class = 'chiac_toggle_speed-' . $toggle_speed; 
        
        // add speed class to wrapper classes
        array_push($content_wrapper_classes, $toggle_speed_class);
        
        $content_wrapper_classes = implode(" ", $content_wrapper_classes);
        
        // get accordion mode value
        $accordion_mode = $this->props['chiac_mode'] !== 'single' ? 'multiple' : $this->props['chiac_mode'];
        
        // Render module content
		$output = sprintf( 
            '<div class="%2$s" data-speed="%4$s" data-mode="%3$s">%1$s</div>',
			et_core_sanitized_previously( $this->content ),
            $content_wrapper_classes,
            $accordion_mode,
            $toggle_speed
		);
		
		// Render module content
		// return $output;

        // Render wrapper
		return $this->_render_module_wrapper( $output, $render_slug );
	}
}

new CHIAC_Divi_Accordions;