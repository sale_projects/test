<?php
/*
Plugin Name: Divi Accordions Plus
Plugin URI:  https://divicio.us
Description: Create better accordions for Divi & Extra themes by Elegant Themes.
Version:     1.4.1
Author:      Ivan Chiurcci(Chi)
Author URI:  https://divicio.us
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Text Domain: chiac-divi-accordions
Domain Path: /languages

Divi Accordions Plus is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.

Divi Accordions Plus is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Divi Accordions Plus. If not, see https://www.gnu.org/licenses/gpl-2.0.html.
*/

/**
 * Divi Accordions Plus version
 */
define('CHIAC_VERSION', '1.4.1');
update_option( 'chiac_license_key', 'chiac_license_key');
update_option( 'chiac_license_status', 'valid');
if ( ! function_exists( 'chiac_initialize_extension' ) ):

    /**
     * Creates the extension's main class instance.
     *
     * @since 1.0.0
     */
    function chiac_initialize_extension() {

        // Helper methods.
        require_once plugin_dir_path( __FILE__ ) . 'includes/helper.php';

        // Plugin main class
        require_once plugin_dir_path( __FILE__ ) . 'includes/DiviAccordions.php';
        
        // Plugin updater
        require_once plugin_dir_path( __FILE__ ) . 'includes/chiac_plugin_licensing.php';
        
    }
    add_action( 'divi_extensions_init', 'chiac_initialize_extension' );
    
endif;
