msgid ""
msgstr ""
"Project-Id-Version: Page Builder Everywhere\n"
"POT-Creation-Date: 2021-01-29 14:45+0100\n"
"PO-Revision-Date: 2021-01-29 14:45+0100\n"
"Last-Translator: \n"
"Language-Team: Divi Space\n"
"Language: en_US\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.4.2\n"
"X-Poedit-Basepath: ..\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-KeywordsList: __;esc_html__;_e;esc_attr__;esc_html_e\n"
"X-Poedit-SearchPath-0: .\n"

#: LayoutModule.php:13
msgid "PBE Layout"
msgstr ""

#: LayoutModule.php:14
msgid "PBE Layouts"
msgstr ""

#: LayoutModule.php:21 LayoutModule.php:300
msgid "Layout"
msgstr ""

#: admin.php:13
msgid "Documentation"
msgstr ""

#: admin.php:19
msgid "Instructions"
msgstr ""

#: admin.php:20
msgid "Addons"
msgstr ""

#: admin.php:21
msgid "License"
msgstr ""

#: admin.php:27
msgid ""
"With the Page Builder Everywhere plugin, you’ll be able to extend the reach "
"of the Divi Builder plugin to outside of its standard scope to include the "
"use of customizable modules in a range of new exciting areas, sections, and "
"pages. From headers to footers and sidebars, 404 error pages and more, Page "
"Builder Everywhere lets you use the Divi Builder in its normal state like "
"never before."
msgstr ""

#: admin.php:28
#, php-format
msgid "You can access the full documentation %shere%s."
msgstr ""

#: admin.php:29
#, php-format
msgid "Access the Page Builder Everywhere customizer settings %shere%s. "
msgstr ""

#: admin.php:32
msgid ""
"Divi is a registered trademark of Elegant Themes, Inc. This product is not "
"affiliated with nor endorsed by Elegant Themes. Links to the Elegant Themes "
"website on this page are affiliate links."
msgstr ""

#: ds-pbe-addons/addons.php:35
msgid ""
"We are unable to load the addons for this theme right now. Please try again "
"later!"
msgstr ""

#: ds-pbe-addons/addons.php:93
msgid "Install"
msgstr ""

#: ds-pbe-addons/addons.php:99
msgid ""
"You do not have sufficient permissions to install plugins. If you think this "
"message is in error, go to Plugins > Add New and search for the plugin name."
msgstr ""

#: ds-pbe-addons/addons.php:102
msgid "Already Installed"
msgstr ""

#: parts/layout-widget.php:10
msgid "Divi PBE Widget"
msgstr ""

#: parts/layout-widget.php:10
msgid ""
"Add any Divi Library item as a widget thanks to the Page Builder Everywhere "
"Plugin."
msgstr ""

#: parts/layout-widget.php:37
msgid "Please add a Divi Library item to your Divi PBE Widget"
msgstr ""

#: parts/layout-widget.php:63
msgid "No Library Item Selected"
msgstr ""

#: parts/layout-widget.php:69
msgid "Which Library Item would you like to display?"
msgstr ""

#: parts/layout-widget.php:73
msgid "No Library Items"
msgstr ""

#: parts/layout-widget.php:75
msgid "Choose a Library Item"
msgstr ""

#: parts/layout-widget.php:85
msgid "Display Library Item Title?"
msgstr ""

#: parts/layout-widget.php:88
#, php-format
msgid "Add Library Items to be used as widgets %shere%s"
msgstr ""

#: parts/new-widget-areas.php:11
msgid "PBE Above Header"
msgstr ""

#: parts/new-widget-areas.php:19
msgid "PBE Below Header"
msgstr ""

#: parts/new-widget-areas.php:27
msgid "PBE Footer"
msgstr ""

#: parts/new-widget-areas.php:35
msgid "PBE Above Main Content"
msgstr ""

#: parts/new-widget-areas.php:43
msgid "PBE Below Main Content"
msgstr ""

#: parts/new-widget-areas.php:51
msgid "PBE Replace Main Content"
msgstr ""

#: parts/pbe-customizer.php:14
msgid "Page Builder Everywhere"
msgstr ""

#: parts/pbe-customizer.php:15
msgid "Advanced options for controlling the way PBE behaves."
msgstr ""

#: parts/pbe-customizer.php:22
msgid "Main Header"
msgstr ""

#: parts/pbe-customizer.php:28
msgid "Above Header"
msgstr ""

#: parts/pbe-customizer.php:34
msgid "Footer"
msgstr ""

#: parts/pbe-customizer.php:46
msgid "Stop logo overlapping section above header"
msgstr ""

#: parts/pbe-customizer.php:59
msgid "Remove Default Main Header"
msgstr ""

#: parts/pbe-customizer.php:72
msgid "Hide above header section on scroll"
msgstr ""

#: parts/pbe-customizer.php:85
msgid "Hide bottom footer"
msgstr ""

#: pbe.php:146
msgid "PBE Customizer"
msgstr ""

#: pbe.php:365
msgid "License Key Information"
msgstr ""

#: pbe.php:365
msgid "Activate License Key"
msgstr ""

#: updater/EDD_SL_Plugin_Updater.php:193
#, php-format
msgid ""
"There is a new version of %1$s available. %2$sView version %3$s details%4$s."
msgstr ""

#: updater/EDD_SL_Plugin_Updater.php:201
#, php-format
msgid ""
"There is a new version of %1$s available. %2$sView version %3$s details%4$s "
"or %5$supdate now%6$s."
msgstr ""

#: updater/EDD_SL_Plugin_Updater.php:382
msgid "You do not have permission to install plugin updates"
msgstr ""

#: updater/EDD_SL_Plugin_Updater.php:382
msgid "Error"
msgstr ""

#: updater/license-key-activation.php:26 updater/license-key-activation.php:73
#: updater/license-key-activation.php:125
msgid "An error occurred, please try again."
msgstr ""

#: updater/license-key-activation.php:40
#, php-format
msgid "Your license key expired on %s."
msgstr ""

#: updater/license-key-activation.php:47
msgid "Your license key has been disabled."
msgstr ""

#: updater/license-key-activation.php:52
msgid "Invalid license key."
msgstr ""

#: updater/license-key-activation.php:58
msgid "Your license key is not active for this URL."
msgstr ""

#: updater/license-key-activation.php:63
#, php-format
msgid "This appears to be an invalid license key for %s."
msgstr ""

#: updater/license-key-activation.php:68
msgid ""
"Your license key has reached its activation limit. Please deactivate the key "
"on one of your other sites before activating it on this site."
msgstr ""

#: updater/license-key-activation.php:144
msgid ""
"An error occurred during license key deactivation. Please try again or "
"contact support."
msgstr ""

#: updater/updater.php:71
#, php-format
msgid "Thank you for purchasing %s! %s Please enter your license key below."
msgstr ""

#: updater/updater.php:82 updater/updater.php:118
msgid "License Key:"
msgstr ""

#: updater/updater.php:88 updater/updater.php:124
msgid "An unknown error has occurred. Please try again."
msgstr ""

#: updater/updater.php:91
msgid "Continue"
msgstr ""

#: updater/updater.php:126
msgid "Deactivate License Key"
msgstr ""

#: widget-mods/widget-conditions.php:59
msgid "All category pages"
msgstr ""

#: widget-mods/widget-conditions.php:71
msgid "Logged In"
msgstr ""

#: widget-mods/widget-conditions.php:72
msgid "Logged Out"
msgstr ""

#: widget-mods/widget-conditions.php:77
msgid "All author pages"
msgstr ""

#: widget-mods/widget-conditions.php:93
msgid "All tag pages"
msgstr ""

#: widget-mods/widget-conditions.php:105
msgid "All date archives"
msgstr ""

#: widget-mods/widget-conditions.php:106
msgid "Daily archives"
msgstr ""

#: widget-mods/widget-conditions.php:107
msgid "Monthly archives"
msgstr ""

#: widget-mods/widget-conditions.php:108
msgid "Yearly archives"
msgstr ""

#: widget-mods/widget-conditions.php:119
msgid "Front page"
msgstr ""

#: widget-mods/widget-conditions.php:120
msgid "Posts page"
msgstr ""

#: widget-mods/widget-conditions.php:121
msgid "Archive page"
msgstr ""

#: widget-mods/widget-conditions.php:122
msgid "404 error page"
msgstr ""

#: widget-mods/widget-conditions.php:123
msgid "Search results"
msgstr ""

#: widget-mods/widget-conditions.php:133
msgid "All taxonomy pages"
msgstr ""

#: widget-mods/widget-conditions.php:148
msgid "All pages"
msgstr ""

#: widget-mods/widget-conditions.php:250
msgid "Conditional Logic"
msgstr ""

#: widget-mods/widget-conditions.php:252
msgid ""
"If a display condition is set, the library item will only display if that "
"condition is met; otherwise, it will always display by default. In both "
"cases, the library item will not display if a hide condition is met."
msgstr ""

#: widget-mods/widget-conditions.php:265
msgid "Hide if"
msgstr ""

#: widget-mods/widget-conditions.php:265
msgid "Display if"
msgstr ""

#: widget-mods/widget-conditions.php:279
msgid "Category"
msgstr ""

#: widget-mods/widget-conditions.php:310
msgid "Delete"
msgstr ""

#: widget-mods/widget-conditions.php:310
msgid "Add"
msgstr ""
