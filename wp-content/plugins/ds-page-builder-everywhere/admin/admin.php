<?php
$customize_url = admin_url('customize.php?autofocus[panel]=pbe_customizer_options'); // Direct to Customizer Panel
?>

<div id="ds-pbe-settings-container">
    <div id="ds-pbe-settings">
        <div id="ds-pbe-settings-header">
            <div class="ds-pbe-settings-logo">
                <h3><?php echo(esc_html(DS_PBE_ITEM_NAME)); ?></h3>
            </div>
            <div id="ags-settings-header-links">
                <a id="ags-settings-header-link-support" href="https://support.aspengrovestudios.com/article/70-page-builder-everywhere"
                   target="_blank"><?php esc_html_e('Documentation', 'ds-page-builder-everywhere') ?></a>
            </div>

        </div>

        <ul id="ds-pbe-settings-tabs">
            <li class="ds-pbe-settings-active"><a href="#settings"><?php esc_html_e('Instructions', 'ds-page-builder-everywhere'); ?></a></li>
            <li><a href="#addons"><?php esc_html_e('Addons', 'ds-page-builder-everywhere'); ?></a></li>
            <li><a href="#license"><?php esc_html_e('License', 'ds-page-builder-everywhere') ?></a></li>
        </ul>

        <div id="ds-pbe-settings-tabs-content">

            <div id="ds-pbe-settings-settings" class="ds-pbe-settings-active">
                <p><?php esc_html_e('With the Page Builder Everywhere plugin, you’ll be able to extend the reach of the Divi Builder plugin to outside of its standard scope to include the use of customizable modules in a range of new exciting areas, sections, and pages. From headers to footers and sidebars, 404 error pages and more, Page Builder Everywhere lets you use the Divi Builder in its normal state like never before.', 'ds-page-builder-everywhere'); ?></p>
                <p><?php echo sprintf(esc_html__('You can access the full documentation %shere%s.', 'ds-page-builder-everywhere'), '<a href="https://support.aspengrovestudios.com/article/70-page-builder-everywhere" target="_blank">', '</a>'); ?></p>
                <p><?php echo sprintf(esc_html__('Access the Page Builder Everywhere customizer settings %shere%s. ', 'ds-page-builder-everywhere'), '<a href=" ' . $customize_url . '" alt="Page Builder Everywhere Customizer">', '</a>'); ?> </p>
                <br>
                <hr>
                <p><em><?php esc_html_e('Divi is a registered trademark of Elegant Themes, Inc. This product is not affiliated with nor endorsed by Elegant Themes. Links to the Elegant Themes website on this page are affiliate links.', 'ds-page-builder-everywhere'); ?></em></p>

            </div> <!-- #ds-pbe-settings-settings -->

            <div id="ds-pbe-settings-addons">
                <?php
                define('DS_PBE_ADDONS_URL', 'https://divi.space/wp-content/uploads/product-addons/page-builder-everywhere.json');
                require_once(dirname(__FILE__) . '/addons/addons.php');
                DS_PBE_Addons::outputList();
                ?>
            </div>  <!-- #ds-pbe-settings-addons -->

            <!-- LICENSE TAB -->
            <div id="ds-pbe-settings-license">
                <?php DS_PBE_license_key_box(); ?>
            </div>

        </div> <!-- ds-pbe-settings-tabs-content -->
    </div> <!-- ds-pbe-settings -->
</div> <!-- ds-pbe-settings-container -->

<script>
    var ds_pbe_tabs_navigate = function () {
        var tabs = [
                {
                    tabsContainerId: 'ds-pbe-settings-tabs',
                    contentIdPrefix: 'ds-pbe-settings-'
                }
            ],
            activeClass = 'ds-pbe-settings-active';
        for (var i = 0; i < tabs.length; ++i) {
            var $tabContent = jQuery('#' + tabs[i].contentIdPrefix + location.hash.substr(1));
            if ($tabContent.length) {
                var $tabs = jQuery('#' + tabs[i].tabsContainerId + ' > li');
                $tabContent
                    .siblings()
                    .add($tabs)
                    .removeClass(activeClass);
                $tabContent.addClass(activeClass);
                $tabs
                    .filter(':has(a[href="' + location.hash + '"])')
                    .addClass(activeClass);
                break;
            }
        }
    };
    if (location.hash) {
        ds_pbe_tabs_navigate();
    }

    jQuery(window).on('hashchange', ds_pbe_tabs_navigate);
</script>
