<?php
/*
 * This file was modified by Dominika Rauk:
 * - 2020-02-21: translations
 */

function pbe_customizer_settings($wp_customize) {

    // --------------- Pascal Customizer Panel --------------- // PANEL

    $wp_customize->add_panel('pbe_customizer_options', array(
        'priority'    => 60,
        'capability'  => 'edit_theme_options',
        'title'       => esc_html__('Page Builder Everywhere', 'ds-page-builder-everywhere'),
        'description' => esc_html__('Advanced options for controlling the way PBE behaves.', 'ds-page-builder-everywhere'),
    ));

    // -------------- Device Preview ---------------------- // SECTION

    $wp_customize->add_section('main_header_options', array(
        'priority' => 10,
        'title'    => esc_html__('Main Header', 'ds-page-builder-everywhere'),
        'panel'    => 'pbe_customizer_options',
    ));

    $wp_customize->add_section('above_header_options', array(
        'priority' => 20,
        'title'    => esc_html__('Above Header', 'ds-page-builder-everywhere'),
        'panel'    => 'pbe_customizer_options',
    ));

    $wp_customize->add_section('pbe_footer_options', array(
        'priority' => 30,
        'title'    => esc_html__('Footer', 'ds-page-builder-everywhere'),
        'panel'    => 'pbe_customizer_options',
    ));

    $wp_customize->add_setting('fix_logo_size', array(
        'default'    => false,
        'type'       => 'option',
        'capability' => 'edit_theme_options'
    ));

    $wp_customize->add_control('fix_logo_size', array(
        'settings' => 'fix_logo_size',
        'label'    => esc_html__('Stop logo overlapping section above header', 'ds-page-builder-everywhere'),
        'section'  => 'main_header_options',
        'type'     => 'checkbox',
    ));

    $wp_customize->add_setting('hide_main_header', array(
        'default'    => false,
        'type'       => 'option',
        'capability' => 'edit_theme_options'
    ));

    $wp_customize->add_control('hide_main_header', array(
        'settings' => 'hide_main_header',
        'label'    => esc_html__('Remove Default Main Header', 'ds-page-builder-everywhere'),
        'section'  => 'main_header_options',
        'type'     => 'checkbox',
    ));

    $wp_customize->add_setting('hide_above_header', array(
        'default'    => false,
        'type'       => 'option',
        'capability' => 'edit_theme_options'
    ));

    $wp_customize->add_control('hide_above_header', array(
        'settings' => 'hide_above_header',
        'label'    => esc_html__('Hide above header section on scroll', 'ds-page-builder-everywhere'),
        'section'  => 'above_header_options',
        'type'     => 'checkbox',
    ));

    $wp_customize->add_setting('hide_bottom_footer', array(
        'default'    => false,
        'type'       => 'option',
        'capability' => 'edit_theme_options'
    ));

    $wp_customize->add_control('hide_bottom_footer', array(
        'settings' => 'hide_bottom_footer',
        'label'    => esc_html__('Hide bottom footer', 'ds-page-builder-everywhere'),
        'section'  => 'pbe_footer_options',
        'type'     => 'checkbox',
    ));
}

add_action('customize_register', 'pbe_customizer_settings', 999);