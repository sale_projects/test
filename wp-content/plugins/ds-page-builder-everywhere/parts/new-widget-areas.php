<?php
/*
 * Modifications by Aspen Grove Studios:
 * - 2019-01-01: change function prefix, rename above/below content widget areas, add "Replace Main Content" widget area
 * - 2020-02-21: translations
 */

// Create the new widget area
function ds_pbe_widget_area() {
    register_sidebar(array(
        'name'          => esc_html__('PBE Above Header', 'ds-page-builder-everywhere'),
        'id'            => 'pbe-above-header-wa',
        'before_widget' => '<div id="%1$s" class="et_pb_widget %2$s">',
        'after_widget'  => '</div> <!-- end .et_pb_widget -->',
        'before_title'  => '<h4 class="widgettitle">',
        'after_title'   => '</h4>',
    ));
    register_sidebar(array(
        'name'          => esc_html__('PBE Below Header', 'ds-page-builder-everywhere'),
        'id'            => 'pbe-below-header-wa',
        'before_widget' => '<div id="%1$s" class="et_pb_widget %2$s">',
        'after_widget'  => '</div> <!-- end .et_pb_widget -->',
        'before_title'  => '<h4 class="widgettitle">',
        'after_title'   => '</h4>',
    ));
    register_sidebar(array(
        'name'          => esc_html__('PBE Footer', 'ds-page-builder-everywhere'),
        'id'            => 'pbe-footer-wa',
        'before_widget' => '<div id="%1$s" class="et_pb_widget %2$s">',
        'after_widget'  => '</div> <!-- end .et_pb_widget -->',
        'before_title'  => '<h4 class="widgettitle">',
        'after_title'   => '</h4>',
    ));
    register_sidebar(array(
        'name'          => esc_html__('PBE Above Main Content', 'ds-page-builder-everywhere'),
        'id'            => 'pbe-above-content-wa',
        'before_widget' => '<div id="%1$s" class="et_pb_widget %2$s">',
        'after_widget'  => '</div> <!-- end .et_pb_widget -->',
        'before_title'  => '<h4 class="widgettitle">',
        'after_title'   => '</h4>',
    ));
    register_sidebar(array(
        'name'          => esc_html__('PBE Below Main Content', 'ds-page-builder-everywhere'),
        'id'            => 'pbe-below-content-wa',
        'before_widget' => '<div id="%1$s" class="et_pb_widget %2$s">',
        'after_widget'  => '</div> <!-- end .et_pb_widget -->',
        'before_title'  => '<h4 class="widgettitle">',
        'after_title'   => '</h4>',
    ));
    register_sidebar(array(
        'name'          => esc_html__('PBE Replace Main Content', 'ds-page-builder-everywhere'),
        'id'            => 'pbe-replace-content-wa',
        'before_widget' => '<div id="%1$s" class="et_pb_widget %2$s">',
        'after_widget'  => '</div> <!-- end .et_pb_widget -->',
        'before_title'  => '<h4 class="widgettitle">',
        'after_title'   => '</h4>',
    ));
}

add_action('widgets_init', 'ds_pbe_widget_area');

?>