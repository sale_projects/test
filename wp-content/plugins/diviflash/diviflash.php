<?php
/*
Plugin Name: DiviFlash
Plugin URI:  http://www.diviflash.com
Description: Module pack for Divi
Version:     1.0.3
Author:      DiviFlash
Author URI:  http://www.diviflash.com
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Text Domain: divi_flash
Domain Path: /languages
*/

if(!defined('DIFL_MAIN_DIR')) {
	define('DIFL_MAIN_DIR', __DIR__);
}
if(!defined('DIFL_ADMIN_DIR')) {
	define('DIFL_ADMIN_DIR', trailingslashit(plugin_dir_url(__FILE__)) . 'core/admin/');
}
if(!defined('DIFL_MAIN_FILE_PATH')) {
	define('DIFL_MAIN_FILE_PATH', __FILE__);
}
if(!defined('DIFL_ASSETS')) {
	define('DIFL_ASSETS', trailingslashit(plugin_dir_url(__FILE__)) . 'assets/');
}
if(!defined('DIFL_VERSION')) {
	define('DIFL_VERSION','1.0.3');
}

// requiere core
require_once ( DIFL_MAIN_DIR . '/core/init.php');

if ( ! function_exists( 'difl_initialize_extension' ) ):
/**
 * Creates the extension's main class instance.
 *
 * @since 1.0.0
 */
function difl_initialize_extension() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/Diviflash.php';
	add_action('wp_enqueue_scripts', 'df_styles_and_scripts');
}
add_action( 'divi_extensions_init', 'difl_initialize_extension' );
endif;

// include plugin functions
require_once (__DIR__ . '/includes/functions.php');

// required styles and scripts
function df_styles_and_scripts () {
	// custom lib styles
	wp_register_style( 'df-lib-styles', DIFL_ASSETS . 'styles/lib/df_lib_styles.css', array(), DIFL_VERSION );
	wp_enqueue_style('df-lib-styles');
	// custom lib scripts
	wp_register_script( 'imageload', DIFL_ASSETS . 'scripts/lib/imagesloaded.pkgd.min.js', array(), '1.0.0', true );
	wp_register_script( 'animejs', DIFL_ASSETS . 'scripts/lib/anime.js');
	wp_register_script( 'df-tilt-lib', DIFL_ASSETS . 'scripts/lib/vanilla-tilt.min.js', array(), DIFL_VERSION, true );
	wp_register_script( 'bxslider-script', DIFL_ASSETS . 'scripts/lib/jquery.bxslider.min.js', array(), DIFL_VERSION, true );
	wp_register_script( 'swiper-script', DIFL_ASSETS . 'scripts/lib/swiper.min.js', array(), DIFL_VERSION, true );
	wp_register_script( 'df-imagegallery-lib', DIFL_ASSETS . 'scripts/lib/gallerylib.js', array('jquery'), DIFL_VERSION, true );
	wp_register_script( 'justified-gallery-script', DIFL_ASSETS . 'scripts/lib/jquery.justifiedGallery.js', array('jquery'), DIFL_VERSION, true );
	wp_register_script( 'lightgallery-script', DIFL_ASSETS . 'scripts/lib/lightgallery.js', array('jquery'), DIFL_VERSION, true );
	wp_register_script( 'packery-script', DIFL_ASSETS . 'scripts/lib/packery.pkgd.js', array('jquery'), DIFL_VERSION, true );
	wp_register_script( 'sticky-script', DIFL_ASSETS . 'scripts/lib/hc-sticky.js', array('jquery'), DIFL_VERSION, true );
	// custom scripts
	wp_register_script( 'df-tilt-script', DIFL_ASSETS . 'scripts/tiltcard.js', array(), DIFL_VERSION, true );
	wp_register_script( 'df-floatimage-script', DIFL_ASSETS . 'scripts/floatImage.js', array('animejs'), DIFL_VERSION, true );
	wp_register_script( 'df-logocarousel', DIFL_ASSETS . 'scripts/logoCarousel.js', array('jquery', 'bxslider-script'), DIFL_VERSION, true );
	wp_register_script( 'df-instagramcarousel', DIFL_ASSETS . 'scripts/instagramCarousel.js', array(), DIFL_VERSION, true );
	wp_register_script( 'df-imagecarousel', DIFL_ASSETS . 'scripts/imageCarousel.js', array(), DIFL_VERSION, true );
	wp_register_script( 'df-testcarousel', DIFL_ASSETS . 'scripts/testCarousel.js', array(), DIFL_VERSION, true );
	wp_register_script( 'df-contentcarousel', DIFL_ASSETS . 'scripts/contentcarousel.js', array(), DIFL_VERSION, true );
	wp_register_script( 'df-imagegallery', DIFL_ASSETS . 'scripts/imageGallery.js', array(), DIFL_VERSION, true );
	wp_register_script( 'df-instagramgallery', DIFL_ASSETS . 'scripts/instagramGallery.js', array(), DIFL_VERSION, true );
	wp_register_script( 'df-jsgallery', DIFL_ASSETS . 'scripts/justifyGallery.js', array('jquery'), DIFL_VERSION, true );
	wp_register_script( 'df-packery', DIFL_ASSETS . 'scripts/df-packery.js', array('jquery'), DIFL_VERSION, true );
	wp_register_script( 'headline-scripts', DIFL_ASSETS . 'scripts/headline.js', array('jquery'), DIFL_VERSION, true );
	wp_register_script( 'df-tabs', DIFL_ASSETS . 'scripts/df-tabs.js', array('jquery', 'sticky-script'), DIFL_VERSION, true );
	// styles for builder
	wp_register_style( 'df-builder-styles', DIFL_ASSETS . 'styles/df-builder-styles.css', array(), DIFL_VERSION );
	wp_enqueue_style('df-builder-styles');
}


/**
 * Load scripts and styles for admin
 * 
 */
function df_load_admin_scripts() {
    $screen = get_current_screen();

    if (isset($screen->base) && $screen->id === 'edit-et_pb_layout') {
		wp_register_script( 'df-shortcode-copy', DIFL_ADMIN_DIR . 'assets/scripts/df-shortcode-copy.js', array(), DIFL_VERSION, true );
		wp_enqueue_script('df-shortcode-copy');
	}
	wp_register_style( 'df-admin-style', DIFL_ADMIN_DIR . 'assets/styles/admin.css', array(), DIFL_VERSION );
	wp_enqueue_style('df-admin-style');
    
}
add_action( 'admin_enqueue_scripts', 'df_load_admin_scripts' );

// print_r(get_transient('df_instagram_feed_data_difl_instagramgallery_0_100_17841407580633010'));
