<?php
require_once( DIFL_MAIN_DIR . '/includes/functions/df_instagram.php');
require_once( DIFL_MAIN_DIR . '/includes/functions/df_imagegallery_functions.php');
require_once( DIFL_MAIN_DIR . '/includes/functions/df_jsgallery_functions.php');
require_once( DIFL_MAIN_DIR . '/includes/functions/df_packery_functions.php');

/**
 * VB HTML on AJAX request for CFSeven
 * @return json response
 */
add_action( 'wp_ajax_df_cfseven_requestdata', 'df_cfseven_requestdata' );
function df_cfseven_requestdata() {
    global $paged, $post;

    $data = json_decode(file_get_contents('php://input'), true);
    if (! wp_verify_nonce( $data['et_admin_load_nonce'], 'et_admin_load_nonce' )) {
        wp_die();
    }
    $options = $data['props'];

    $args = array(
        'post_type' => 'wpcf7_contact_form',
    );

    if ($options['cf7_forms'] === 'default') {
        $contact_forms = "Please select an contact form!";
    } else {
        $contact_forms = do_shortcode('[contact-form-7 id="'.$options['cf7_forms'].'" ]');
    }
    
    $posts = $contact_forms;
    wp_send_json_success($posts);
}

/**
 * VB HTML on AJAX request for WPForms
 * @return json response
 * 
 */
add_action( 'wp_ajax_df_wpforms_requestdata', 'df_wpforms_requestdata' );
function df_wpforms_requestdata() {
    global $paged, $post, $wp_scripts, $wp_styles;

    $data = json_decode(file_get_contents('php://input'), true);
    if (! wp_verify_nonce( $data['et_admin_load_nonce'], 'et_admin_load_nonce' )) {
        wp_die();
    }
    $options = $data['props'];

    unset( $wp_scripts->registered );

    $preview_template = DIFL_MAIN_DIR . "/template/preview.php";

    $shortcode = '[wpforms id="'.$options['wpforms'].'"]';

    ob_start();
    echo et_core_esc_wp(include($preview_template));
    $wpforms = ob_get_clean();

    $form = [
        'content'   => $wpforms,
        'styles'    => $wp_styles,
        'scripts'   => $wp_scripts
    ];
    wp_send_json_success($form);
}

function df_wpforms_et_builder_load_actions( $actions ) {
	$actions[] = 'df_wpforms_requestdata';

	return $actions;
}
add_filter( 'et_builder_load_actions', 'df_wpforms_et_builder_load_actions' );

/**
 * Add URL fields to media uploader
 *
 * @param $form_fields array, fields to include in attachment form
 * @param $post object, attachment record in database
 * @return $form_fields, modified form fields
 */
function df_ig_add_attachment_field( $form_fields, $post ) {

    $form_fields['df-ig-url'] = array(
        'label' => 'URL',
        'input' => 'url',
        'value' => esc_attr(get_post_meta( $post->ID, 'df_ig_url', true )),
        'helps' => 'Add URL',
    );
 
    return $form_fields;
}
 
add_filter( 'attachment_fields_to_edit', 'df_ig_add_attachment_field', 10, 2 );
 
/**
 * Save values URL in media uploader
 *
 * @param $post array, the post data for database
 * @param $attachment array, attachment fields from $_POST form
 * @return $post array, modified post data
 */
function df_ig_save_attachment_field_save( $post, $attachment ) {
 
    if( isset( $attachment['df-ig-url'] ) ) {
        update_post_meta( $post['ID'], 'df_ig_url', esc_url( $attachment['df-ig-url'] ) );
    }

    return $post;
}
 
add_filter( 'attachment_fields_to_save', 'df_ig_save_attachment_field_save', 10, 2 );


// Create New Admin Column
add_filter( 'manage_et_pb_layout_posts_columns', 'df_shortcode_create_shortcode_column', 5 );
function df_shortcode_create_shortcode_column( $columns ) {
    $columns['df_shortcode_id'] = __( 'Shortcode', 'divi_flash');
    return $columns;
}
// Display Shortcode
add_action( 'manage_et_pb_layout_posts_custom_column', 'df_shortcode_content', 5, 2 );
function df_shortcode_content ( $column, $id ) {
    if( 'df_shortcode_id' == $column ) {
        $value = sprintf('[df_layout_shortcode id="%1$s"]', esc_attr( $id ));
        ?>
        <div class="df-shortcode-wrapper">
            <p class="df-shortcode-copy"><?php echo $value; ?></p>
            <p class="df-cpy-tooltip">Click to copy</p>
        </div>
        <?php
    }
}
// Function to show the module
function df_module_shortcode_callback($atts) {
	$atts = shortcode_atts(array('id' => ''), $atts);
	return do_shortcode('[et_pb_section global_module="'.  esc_attr($atts['id']).'"][/et_pb_section]');	
}
add_shortcode('df_layout_shortcode', 'df_module_shortcode_callback');






