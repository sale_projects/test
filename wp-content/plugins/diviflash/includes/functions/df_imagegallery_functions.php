<?php
/**
 * ImageGallery: Return Options for Gallery
 * 
 * @return $options
 */
function df_ig_options($iamges, $object) {
    $options = array(
        'show_caption' => $object['show_caption'],
        'show_description' => $object['show_description'],
        'image_size' => $object['image_size'],
        'filter_nav' => $object['filter_nav'],
        'layout_mode' => $object['layout_mode'],
        'load_more' => $object['load_more'],
        'init_count' => $object['init_count'],
        'image_count' => $object['image_count'],
        'use_lightbox' => $object['use_lightbox'],
        'use_lightbox_content' => $object['use_lightbox_content'],
        'use_lightbox_download' => $object['use_lightbox_download'],
        'caption_tag' => $object['caption_tag'],
        'description_tag' => $object['description_tag'],
        'image_scale' => $object['image_scale'],
        'content_position' => $object['content_position'],
        'content_reveal_caption' => $object['content_reveal_caption'],
        'content_reveal_description' => $object['content_reveal_description'],
        'border_anim' => $object['border_anim'],
        'border_anm_style' => $object['border_anm_style'],
        'always_show_title' => $object['always_show_title'],
        'always_show_description' => $object['always_show_description'],
        'use_url' => $object['use_url'],
        'url_target' => $object['url_target'],
        'overlay' => $object['overlay'],
    );
    return array_merge($iamges, $options);
}

/**
 * ImageGallery: Request from Gallery FB
 * 
 * @return response | JSON
 */
add_action('wp_ajax_df_image_gallery', 'df_image_gallery');
function df_image_gallery() {
    // create the display gallery code
    $data = json_decode(file_get_contents('php://input'), true);
    if (! wp_verify_nonce( $data['et_admin_load_nonce'], 'et_admin_load_nonce' )) {
        wp_die();
    }

    $_objects = json_decode($data['images']);
    $images_array = [];

    foreach($_objects as $_object) {
        $title = isset($_object->gallery_title) && $_object->gallery_title !== '' ?
            $_object->gallery_title : '';
        $category = isset($_object->gallery_title) && $_object->gallery_title !== '' ?
            strtolower(preg_replace('/[^A-Za-z0-9-]/', "-", $_object->gallery_title)) : '';
        
        $ids = explode( ',', $_object->gallery_ids);

        foreach($ids as $id) {
            $images_array[$id][] = $category;
        }
    }

    $options = df_ig_options(
        array('images_array' => $images_array),
        $data
    );

    $gallery = df_ig_render_images( $options );

    wp_send_json_success($gallery);
}

function df_display_galler_load_actions( $actions ) {
	$actions[] = 'df_image_gallery';

	return $actions;
}
add_filter( 'et_builder_load_actions', 'df_display_galler_load_actions' );

/**
 * ImageGallery: Load More image with Fetch
 * 
 * @return json response
 */
add_action('wp_ajax_df_image_gallery_fetch', 'df_image_gallery_more_image');
add_action('wp_ajax_nopriv_df_image_gallery_fetch', 'df_image_gallery_more_image');
function df_image_gallery_more_image() {

    if (isset($_POST['et_frontend_nonce']) && !wp_verify_nonce( sanitize_text_field($_POST['et_frontend_nonce']), 'et_frontend_nonce' )) {
        wp_die();
    }

    $settings = isset($_POST["options"]) ? json_decode(stripslashes(sanitize_text_field($_POST["options"])), true) : '';
    $data = isset($_POST['images']) ? sanitize_text_field($_POST['images']) : '';
    $page = isset($_POST['page']) ? sanitize_text_field($_POST['page']) : '';
    $image_count = isset($_POST['image_count']) ? sanitize_text_field($_POST['image_count']) : '';
    $loaded = isset($_POST['loaded']) ? sanitize_text_field($_POST['loaded']) : '';
    $images_array = explode(',', $data);
    $images_array = array_unique($images_array);
    $images_array = array_slice($images_array, $loaded, $image_count);

    $options = df_ig_options(
        array('images_array' => $images_array),
        $settings
    );
    $images = df_ig_render_images( $options, true );
    
    wp_send_json_success($images);
}

/**
 * ImageGallery: Image markup for gallery
 * 
 * @param $options array
 * @param $load_more_type boolean, whether it is load more request or not
 * @return $images | HTML Markup
 */
function df_ig_render_images( $options=[], $load_more_type = false ) {

    $default = array(
        'images_array' => [],
        'show_caption' => 'off',
        'show_description' => 'off',
        'image_size' => 'medium',
        'filter_nav' => 'off',
        'load_more' => 'off',
        'init_count' => 6,
        'image_count' => 3,
        'use_lightbox' => 'off',
        'use_lightbox_content' => 'off',
        'caption_tag' => '',
        'description_tag' => '',
        'image_scale' => '',
        'content_position' => '',
        'content_reveal_caption' => '',
        'content_reveal_description' => '',
        'border_anim' => 'off',
        'border_anm_style' => '',
        'always_show_title' => '',
        'always_show_description' => '',
        'use_url' => 'off',
        'url_target' => 'same_window',
        'overlay' => ''
    );

    $options = wp_parse_args($options, $default);
    extract($options); // phpcs:ignore WordPress.PHP.DontExtract
    $images = '';
    $classes = '';
    $_i = 0;

    if ( $load_more_type === true ) {
        $images_array = array_flip($images_array);
    }

    $always_title = $always_show_title === 'on' ? 
        'always-show-title c4-fade-up' : $content_reveal_caption;
    $always_description = $always_show_description === 'on' ? 
        'always-show-description c4-fade-up' : $content_reveal_description;

    $border_anim_class = $border_anim === 'on' ? $border_anm_style : '';
    
    foreach( $images_array as $id => $value ) {
        if ($load_more === 'on' && $filter_nav !== 'on') {
            if ($_i >= $init_count && $load_more_type === false) {
                break;
            }  
        }
        $_i++;
        $media = wp_get_attachment_image_src($id, $image_size);
        $media_lightbox = wp_get_attachment_image_src($id, 'original');

        $content_box = '';
    
        if($show_caption ==='on' || $show_description === 'on'){
            $details = get_post($id);

            $caption = $show_caption === 'on' && $details->post_excerpt !== ''?
                sprintf('<div class="%3$s"><%2$s class="df_ig_caption">%1$s</%2$s></div>', 
                    esc_html__($details->post_excerpt, 'divi_flash'), 
                    esc_attr($caption_tag), 
                    esc_attr($always_title)) : '';

            $description = $show_description === 'on' && $details->post_content !== ''?
                sprintf('<div class="%3$s"><%2$s class="df_ig_description">%1$s</%2$s></div>', 
                    esc_html__($details->post_content, 'divi_flash'), 
                    esc_attr($description_tag), 
                    esc_attr($always_description)) : '';

            $content_box = sprintf('%1$s%2$s', $caption, $description);
        }

        $data_lightbox_html = $use_lightbox_content === 'on' && $content_box !== '' ? 
            'data-sub-html=".df_ig_content"' : '';

        if ( $load_more_type === false ) {
            $classes = implode(" ", $value);
        }

        $custom_url = $use_url === 'on' ? 
            sprintf('data-url="%1$s"', esc_attr(get_post_meta( $id, 'df_ig_url', true ))) 
            : '';        
        
        $empty_class = $content_box === '' ? ' empty_content' : '';

        // Lightbox Caption : https://sachinchoolur.github.io/lightgallery.js/demos/captions.html
        $image = sprintf('<div class="df_ig_image grid-item %3$s" data-src="%8$s" %4$s>
                <div class="item-content %5$s" %10$s>
                    <figure class="%7$s c4-izmir %9$s">
                        %13$s
                        <img class="ig-image" src="%1$s" alt="%12$s" class=""/>
                        <figcaption class="df_ig_content %6$s %11$s">
                            %2$s
                        </figcaption>
                    </figure>
                </div>
            </div>', 
            esc_attr($media[0]), 
            $content_box, 
            esc_attr($classes), 
            $data_lightbox_html, 
            esc_attr($image_scale), 
            esc_attr($content_position),
            esc_attr($border_anim_class), 
            esc_attr($media_lightbox[0]),
            esc_attr(' has_overlay'), 
            $custom_url, 
            esc_attr($empty_class),
            esc_attr(get_post_meta($id , '_wp_attachment_image_alt', true)),
            $overlay === 'on' ? '<span class="df-overlay"></span>' : ''
        );
        $images .= $image;
    }
    return $images;
}