<?php

class DIFL_AdvancedBlurb extends ET_Builder_Module
{
    public $slug       = 'difl_advanced_blurb';
    public $vb_support = 'on';
    use DF_UTLS;

    protected $module_credits = array(
        'module_uri' => '',
        'author'     => 'DiviFlash',
        'author_uri' => '',
    );

    public function init()
    {
        $this->name = esc_html__('Advanced Blurb', 'divi_flash');
        $this->main_css_element = "%%order_class%%";
        $this->icon_path        =  plugin_dir_path( __FILE__ ) . 'advanced-blurb.svg';
    }

    public function get_settings_modal_toggles()
    {
        return array(
            'general'  => array(
                'toggles' => array(
                    'main_content'                      => esc_html__('Content', 'divi_flash'),
                    'badge'                             => esc_html__('Badge', 'divi_flash'),
                    'button'                            => esc_html__('Button', 'divi_flash'),
                    'image'                             => esc_html__('Image and Icon', 'divi_flash'),
                    'item_order'                        => esc_html__('Item Order', 'divi_flash')
                ),
            ),
            'advanced'  =>  array(
                'toggles'   =>  array(
                    'design_zindex'                 => esc_html__('Z-index ', 'divi_flash'),
                    'design_image'                  => esc_html__('Image', 'divi_flash'),
                    'design_icon'                   => esc_html__('Icon', 'divi_flash'),
                    'design_content_area'           => esc_html__('Content Area', 'divi_flash'),
                    'design_text'                   => esc_html__('Text', 'divi_flash'),
                    'design_title'                  => esc_html__('Title', 'divi_flash'),
                    'design_sub_title'              => esc_html__('Sub title', 'divi_flash'),
                    'design_content'                => array(
                        'title' => esc_html__('Content', 'dicm-divi-custom-modules'),
                        // Groups can be organized into tab
                        'tabbed_subtoggles' => true,
                        // Subtoggle tab configuration. Add `sub_toggle` attribute on field to put them here
                        'sub_toggles' => array(
                            'body'     => array(
                                'name' => 'P',
                                'icon' => 'text-left',
                            ),
                            'link'     => array(
                                'name' => 'A',
                                'icon' => 'text-link',
                            ),
                            'unorder_list'     => array(
                                'name' => 'A',
                                'icon' => 'list',
                            ),
                            'order_list'     => array(
                                'name' => 'A',
                                'icon' => 'numbered-list',
                            ),
                            'quote' => array(
                                'name' => 'QUOTE',
                                'icon' => 'text-quote',
                            ),
                        ),
                    ),
                    'design_content_bg_and_others'            => esc_html__(' Content Style', 'divi_flash'),
                    'design_badge'            => esc_html__('Badge', 'divi_flash'),
                    'design_badge_text'      => array(
                        'title'             => esc_html__('Badge Font', 'divi_flash'),
                        'tabbed_subtoggles' => true,
                        'sub_toggles' => array(
                            'badge_text_both' => array(
                                'name' => 'Both',
                            ),
                            'badge_text_1'   => array(
                                'name' => 'Line 1',
                            ),
                            'badge_text_2'     => array(
                                'name' => 'Line 2',
                            )
                        ),
                    ),
                    'design_button'            => esc_html__('Button', 'divi_flash'),
                    'custom_spacing'    => array(
                        'title'             => esc_html__('Custom Spacing', 'divi_flash'),
                        'tabbed_subtoggles' => true,
                        'sub_toggles' => array(
                            'wrapper'   => array(
                                'name' => 'Wrapper',
                            ),
                            'content'     => array(
                                'name' => 'Content',
                            )
                        ),
                    )

                )
            ),

            // Advance tab's slug is "custom_css"
            'custom_css' => array(
                'toggles' => array(
                    'limitation' => esc_html__('Limitation', 'divi_flash'), // totally made up
                ),
            ),
        );
    }
    public function get_advanced_fields_config()
    {
        $advanced_fields = array();
        $advanced_fields['fonts'] = array(
            'title'   => array(
                'label'         => esc_html__('Title', 'divi_flash'),
                'toggle_slug'   => 'design_title',
                'tab_slug'        => 'advanced',
                'line_height' => array(
                    'default' => '1em',
                ),
                'font_size' => array(
                    'default' => '24px',
                ),
                'font-weight' => array(
                    'default' => 'bold'
                ),
                'css'      => array(
                    'main' => " %%order_class%% .df_ab_blurb_title ",
                    'hover' => "%%order_class%% .df_ab_blurb_container:hover .df_ab_blurb_title",
                    'important' => 'all',
                ),
                'header_level' => array(
                    'default' => 'h3',
                ),
            ),
            'sub_title'   => array(
                'label'         => esc_html__('Sub Title', 'divi_flash'),
                'toggle_slug'   => 'design_sub_title',
                'tab_slug'        => 'advanced',
                'line_height' => array(
                    'default' => '1em',
                ),
                'font_size' => array(
                    'default' => '18px',
                ),
                'font-weight' => array(
                    'default' => 'semi-bold'
                ),
                'css'      => array(
                    'main' => "%%order_class%% .df_ab_blurb_sub_title ",
                    'hover' => "%%order_class%% .df_ab_blurb_container:hover .df_ab_blurb_sub_title",
                    'important' => 'all',
                ),

                'header_level' => array(
                    'default' => 'h4',
                ),
            ),

            'content_body'   => array(
                'label'         => esc_html__('Body', 'divi_flash'),
                'toggle_slug'   => 'design_content',
                'sub_toggle'  => 'body',
                'line_height' => array(
                    'default' => '1em',
                ),
                'font_size' => array(
                    'default' => '14px',
                ),
                'css'      => array(
                    'main' => "%%order_class%% .df_ab_blurb_description",
                    'hover' => "%%order_class%% .df_ab_blurb_container:hover .df_ab_blurb_description",
                    'important' => 'all',
                ),
            ),

            'content_link'   => array(
                'label'         => esc_html__('Body Link', 'divi_flash'),
                'toggle_slug'   => 'design_content',
                'sub_toggle'  => 'link',
                'line_height' => array(
                    'default' => '1em',
                ),
                'font_size' => array(
                    'default' => '14px',
                ),
                'css'      => array(
                    'main' => "%%order_class%% .df_ab_blurb_description a",
                    'hover' => "%%order_class%% .df_ab_blurb_container:hover .df_ab_blurb_description a",
                    'important' => 'all',
                ),
            ),

            'content_unorder_list'   => array(
                'label'         => esc_html__('Body Unorder List', 'divi_flash'),
                'toggle_slug'   => 'design_content',
                'sub_toggle'  => 'unorder_list',
                'line_height' => array(
                    'default' => '1em',
                ),
                'font_size' => array(
                    'default' => '14px',
                ),
                'css'      => array(
                    'main' => "%%order_class%% .df_ab_blurb_description ul li",
                    'hover' => "%%order_class%% .df_ab_blurb_container:hover .df_ab_blurb_description ul li",
                    'important' => 'all',
                ),
            ),

            'content_order_list'   => array(
                'label'         => esc_html__('Body Order List', 'divi_flash'),
                'toggle_slug'   => 'design_content',
                'sub_toggle'  => 'order_list',
                'line_height' => array(
                    'default' => '1em',
                ),
                'font_size' => array(
                    'default' => '14px',
                ),
                'css'      => array(
                    'main' => "%%order_class%% .df_ab_blurb_description ol li",
                    'hover' => "%%order_class%% .df_ab_blurb_container:hover .df_ab_blurb_description ol li",
                    'important' => 'all',
                ),
            ),

            'content_quote'   => array(
                'label'         => esc_html__('Body Blockquote', 'divi_flash'),
                'toggle_slug'   => 'design_content',
                'sub_toggle'  => 'quote',
                'line_height' => array(
                    'default' => '1em',
                ),
                'font_size' => array(
                    'default' => '14px',
                ),
                'css'      => array(
                    'main' => "%%order_class%% .df_ab_blurb_description blockquote",
                    'hover' => "%%order_class%% .df_ab_blurb_container:hover .df_ab_blurb_description blockquote",
                    'important' => 'all',
                ),
            ),


            'button_text'   => array(
                'label'         => esc_html__('Button', 'divi_flash'),
                'toggle_slug'   => 'design_button',
                'tab_slug'        => 'advanced',
                'line_height' => array(
                    'default' => '1em',
                ),
                'font_size' => array(
                    'default' => '18px',
                ),
                'css'      => array(
                    'main' => "%%order_class%% .df_ab_blurb_button",
                    'hover' => "%%order_class%% .df_ab_blurb_container:hover .df_ab_blurb_button",
                    'important' => 'all',
                ),
            ),

            'badge_text'   => array(
                'label'         => esc_html__('Badge Both', 'divi_flash'),
                'tab_slug'        => 'advanced',
                'toggle_slug'   => 'design_badge_text',
                'sub_toggle'  => 'badge_text_both',
                'line_height' => array(
                    'default' => '1em',
                ),
                'font_size' => array(
                    'default' => '18px',
                ),
                'css'      => array(
                    'main' => "%%order_class%% .df_ab_blurb_badge .badge_text_wrapper",
                    'hover' => "%%order_class%% .df_ab_blurb_container:hover .df_ab_blurb_badge .badge_text_wrapper",
                    'important' => 'all',
                ),
            ),
            'badge_text_1'   => array(
                'label'         => esc_html__('Badge Line 1', 'divi_flash'),
                'tab_slug'        => 'advanced',
                'toggle_slug'   => 'design_badge_text',
                'sub_toggle'  => 'badge_text_1',
                'line_height' => array(
                    'default' => '1em',
                ),
                'font_size' => array(
                    'default' => '18px',
                ),
                'css'      => array(
                    'main' => "%%order_class%% .df_ab_blurb_badge .badge_text_1",
                    'hover' => "%%order_class%% .df_ab_blurb_container:hover .df_ab_blurb_badge .badge_text_1",
                    'important' => 'all',
                ),
            ),
            
            'badge_text_2'   => array(
                'label'         => esc_html__('Badge Line 2', 'divi_flash'),
                'tab_slug'        => 'advanced',
                'toggle_slug'   => 'design_badge_text',
                'sub_toggle'  => 'badge_text_2',
                'line_height' => array(
                    'default' => '1em',
                ),
                'font_size' => array(
                    'default' => '18px',
                ),
                'css'      => array(
                    'main' => "%%order_class%% .df_ab_blurb_badge .badge_text_2",
                    'hover' => "%%order_class%% .df_ab_blurb_container:hover .df_ab_blurb_badge .badge_text_2",
                    'important' => 'all',
                ),
            ),

        );

        $advanced_fields['borders'] = array(
            'default'               => array(),
            'title_border'         => array(
                'css'               => array(
                    'main'  => array(
                        'border_radii' => "{$this->main_css_element} .df_ab_blurb_title",
                        'border_radii_hover' => "{$this->main_css_element} .df_ab_blurb_container:hover .df_ab_blurb_title",
                        'border_styles' => "{$this->main_css_element} .df_ab_blurb_title",
                        'border_styles_hover' => "{$this->main_css_element} .df_ab_blurb_container:hover .df_ab_blurb_title",
                    )
                ),
                'label'    => esc_html__('Title Border', 'divi_flash'),
                'tab_slug'        => 'advanced',
                'toggle_slug'     => 'design_title',
            ),
            'sub_title_border'         => array(
                'css'               => array(
                    'main'  => array(
                        'border_radii' => "{$this->main_css_element} .df_ab_blurb_sub_title",
                        'border_radii_hover' => "{$this->main_css_element} .df_ab_blurb_container:hover .df_ab_blurb_sub_title",
                        'border_styles' => "{$this->main_css_element} .df_ab_blurb_sub_title",
                        'border_styles_hover' => "{$this->main_css_element} .df_ab_blurb_container:hover .df_ab_blurb_sub_title",
                    )
                ),
                'label'    => esc_html__('Sub Title Border', 'divi_flash'),
                'tab_slug'        => 'advanced',
                'toggle_slug'     => 'design_sub_title',
            ),
            'content_border'         => array(
                'css'               => array(
                    'main'  => array(
                        'border_radii' => "{$this->main_css_element} .df_ab_blurb_description",
                        'border_radii_hover' => "{$this->main_css_element} .df_ab_blurb_container:hover .df_ab_blurb_description",
                        'border_styles' => "{$this->main_css_element} .df_ab_blurb_description",
                        'border_styles_hover' => "{$this->main_css_element} .df_ab_blurb_container:hover .df_ab_blurb_description",
                    )
                ),
                'label'    => esc_html__('Content Border', 'divi_flash'),
                'tab_slug'        => 'advanced',
                'toggle_slug'   => 'design_content_bg_and_others',
                'sub_toggle'  => 'body',
            ),
            'button_border'         => array(
                'css'               => array(
                    'main'  => array(
                        'border_radii' => "{$this->main_css_element}  .df_ab_blurb_button",
                        'border_radii_hover' => "{$this->main_css_element} .df_ab_blurb_container:hover  .df_ab_blurb_button:hover",
                        'border_styles' => "{$this->main_css_element}  .df_ab_blurb_button",
                        'border_styles_hover' => "{$this->main_css_element} .df_ab_blurb_container:hover .df_ab_blurb_button",
                    )
                ),
                'label'    => esc_html__('Button Border', 'divi_flash'),
                'tab_slug'        => 'advanced',
                'toggle_slug'     => 'design_button',
            ),

            'badge_border'         => array(
                'css'               => array(
                    'main'  => array(
                        'border_radii' => "{$this->main_css_element}  .df_ab_blurb_badge",
                        'border_radii_hover' => "{$this->main_css_element} .df_ab_blurb_container:hover  .df_ab_blurb_badge",
                        'border_styles' => "{$this->main_css_element}  .df_ab_blurb_badge",
                        'border_styles_hover' => "{$this->main_css_element} .df_ab_blurb_container:hover .df_ab_blurb_badge",
                    )
                ),
                'label'    => esc_html__('Badge Border', 'divi_flash'),
                'tab_slug'        => 'advanced',
                'toggle_slug'     => 'design_badge',
            ),
           
            'badge_text_1_border'         => array(
                'css'               => array(
                    'main'  => array(
                        'border_radii' => "{$this->main_css_element}  .df_ab_blurb_badge .badge_text_1 ",
                        'border_radii_hover' => "{$this->main_css_element} .df_ab_blurb_container:hover  .df_ab_blurb_badge .badge_text_1",
                        'border_styles' => "{$this->main_css_element}  .df_ab_blurb_badge .badge_text_1",
                        'border_styles_hover' => "{$this->main_css_element} .df_ab_blurb_container:hover .df_ab_blurb_badge .badge_text_1",
                    )
                ),
                'label'    => esc_html__('Badge Border', 'divi_flash'),
                'tab_slug'        => 'advanced',
                'toggle_slug'   => 'design_badge_text',
                'sub_toggle'  => 'badge_text_1',
            ),

            'badge_text_2_border'         => array(
                'css'               => array(
                    'main'  => array(
                        'border_radii' => "{$this->main_css_element}  .df_ab_blurb_badge .badge_text_2 ",
                        'border_radii_hover' => "{$this->main_css_element} .df_ab_blurb_container:hover  .df_ab_blurb_badge .badge_text_2",
                        'border_styles' => "{$this->main_css_element}  .df_ab_blurb_badge .badge_text_2",
                        'border_styles_hover' => "{$this->main_css_element} .df_ab_blurb_container:hover .df_ab_blurb_badge .badge_text_2",
                    )
                ),
                'label'    => esc_html__('Badge Border', 'divi_flash'),
                'tab_slug'        => 'advanced',
                'toggle_slug'   => 'design_badge_text',
                'sub_toggle'  => 'badge_text_1',
            ),
            
            'icon_border'         => array(
                'label'    => esc_html__('Icon Border', 'divi_flash'),
                'css'               => array(
                    'main'  => array(
                        'border_radii' => "{$this->main_css_element} .et-pb-icon",
                        'border_radii_hover' => "{$this->main_css_element} .df_ab_blurb_container:hover .et-pb-icon",
                        'border_styles' => "{$this->main_css_element} .et-pb-icon",
                        'border_styles_hover' => "{$this->main_css_element} .df_ab_blurb_container:hover .et-pb-icon",
                    )
                ),
                'tab_slug'        => 'advanced',
                'toggle_slug'     => 'design_icon',
            ),

            'image_border'         => array(
                'label'    => esc_html__('Image Border', 'divi_flash'),
                'css'               => array(
                    'main'  => array(
                        'border_radii' => "{$this->main_css_element} .df_ab_blurb_image_img",
                        'border_radii_hover' => '%%order_class%% .df_ab_blurb_container:hover .df_ab_blurb_image_img',
                        'border_styles' => "{$this->main_css_element} .df_ab_blurb_image_img",
                        'border_styles_hover' => "{$this->main_css_element} .df_ab_blurb_container:hover .df_ab_blurb_image_img",

                    ),
                    'important' => true,
                    
                ),
                'tab_slug'        => 'advanced',
                'toggle_slug'     => 'design_image',
            ),

            'content_area_border'         => array(
                'label'    => esc_html__('Content Area Border', 'divi_flash'),
                'css'               => array(
                    'main'  => array(
                        'border_radii' => "{$this->main_css_element} .df_ab_blurb_content_container",
                        'border_radii_hover' => '%%order_class%% .df_ab_blurb_container:hover .df_ab_blurb_content_container',
                        'border_styles' => "{$this->main_css_element} .df_ab_blurb_content_container",
                        'border_styles_hover' => "{$this->main_css_element} .df_ab_blurb_container:hover .df_ab_blurb_content_container",

                    ),
                    'important' => true,
                    
                ),
                'tab_slug'        => 'advanced',
                'toggle_slug'     => 'design_content_area',
            ),


        );
        $advanced_fields['box_shadow'] = array(
            'default'               => true,

            'button_shadow'             => array(
                'label'    => esc_html__('Button Box Shadow', 'divi_flash'),
                'css' => array(
                    'main' => "%%order_class%% .df_ab_blurb_button",
                    'hover' => "%%order_class%% .df_ab_blurb_container:hover .df_ab_blurb_button",
                ),
                'tab_slug'        => 'advanced',
                'toggle_slug'     => 'design_button',
            ),
            'badge_shadow'             => array(
                'label'    => esc_html__('Badge Box Shadow', 'divi_flash'),
                'css' => array(
                    'main' => "%%order_class%% .df_ab_blurb_badge",
                    'hover' => "%%order_class%% .df_ab_blurb_container:hover .df_ab_blurb_badge",
                ),
                'tab_slug'        => 'advanced',
                'toggle_slug'     => 'design_badge',
            ),
            'icon_shadow'             => array(
                'label'    => esc_html__('Icon Box Shadow', 'divi_flash'),
                'css' => array(
                    'main' => "%%order_class%% .et-pb-icon",
                    'hover' => "%%order_class%% .df_ab_blurb_container:hover .et-pb-icon",
                ),
                'tab_slug'        => 'advanced',
                'toggle_slug'     => 'design_icon',
            ),

            'image_shadow'             => array(
                'label'    => esc_html__('Image Box Shadow', 'divi_flash'),
                'css' => array(
                    'main' => "%%order_class%% .df_ab_blurb_image_img",
                    'hover' => "%%order_class%% .df_ab_blurb_container:hover .df_ab_blurb_image_img",
                ),
                'tab_slug'        => 'advanced',
                'toggle_slug'     => 'design_image',
            ),

            'content_area_shadow'             => array(
                'label'    => esc_html__('Content Area Box Shadow', 'divi_flash'),
                'css' => array(
                    'main' => "%%order_class%% .df_ab_blurb_content_container",
                    'hover' => "%%order_class%% .df_ab_blurb_container:hover .df_ab_blurb_content_container",
                ),
                'tab_slug'        => 'advanced',
                'toggle_slug'     => 'design_content_area',
            )

        );
        $advanced_fields['margin_padding'] = array(
            'css'   => array(
                'important' => 'all'
            )
        );

        $advanced_fields['filters'] = array(
            'child_filters_target' => array(
                'label'    => esc_html__('Image filter', 'divi_flash'),
                'tab_slug'        => 'advanced',
                'toggle_slug'     => 'design_image',
                'css' => array(
                    'main' => '%%order_class%% .df_ab_blurb_image_img',
                    'hover' => '%%order_class%% .df_ab_blurb_container:hover .df_ab_blurb_image_img'
                ),

            ),

        );
        $advanced_fields['image'] = array(
			'css' => array(
				'main' => array(
					'%%order_class%% img',
				)
			),
		);
        $advanced_fields['text'] = false;
        return $advanced_fields;
    }

    public function get_fields()
    {
        $general = array();

        $content = array(
            'title' => array(
                'label'           => esc_html__('Title', 'divi_flash'),
                'type'            => 'text',
                'option_category' => 'basic_option',
                'description'     => esc_html__('Title entered here will appear inside the module.', 'divi_flash'),
                'toggle_slug'     => 'main_content',
                'mobile_options'  => true,
            ),
            'sub_title' => array(
                'label'           => esc_html__('Sub Title', 'divi_flash'),
                'type'            => 'text',
                'option_category' => 'basic_option',
                'description'     => esc_html__('Sub Title entered here will appear inside the module.', 'divi_flash'),
                'toggle_slug'     => 'main_content',
            ),
            'content' => array(
                'label'           => esc_html__('Content', 'divi_flash'),
                'type'            => 'tiny_mce',
                'option_category' => 'basic_option',
                'description'     => esc_html__('Content entered here will appear inside the module.', 'divi_flash'),
                'toggle_slug'     => 'main_content',
            ),
            'badge_enable'  => array(
                'label'             => esc_html__('Use Badge', 'divi_flash'),
                'type'              => 'yes_no_button',
                'options'           => array(
                    'off' => esc_html__('Off', 'divi_flash'),
                    'on'  => esc_html__('On', 'divi_flash'),
                ),
                'default'           => 'off',
                'toggle_slug'       => 'badge',
            ),

           
        );
        $button  = array(
            'button_text' => array(
                'label'           => esc_html__('Button Text', 'divi_flash'),
                'type'            => 'text',
                'option_category' => 'basic_option',
                'description'     => esc_html__('Title entered here will appear inside the module.', 'diviblurb-diviblurb'),
                'toggle_slug'     => 'button',
            ),
            'button_url' => array(
                'label'           => esc_html__('Button URL', 'diviblurb-diviblurb'),
                'type'            => 'text',
                'option_category' => 'basic_option',
                'description'     => esc_html__('Title entered here will appear inside the module.', 'diviblurb-diviblurb'),
                'toggle_slug'     => 'button',
            ),
            'button_url_new_window' => array(
                'label'           => esc_html__('Button New tab ', 'diviblurb-diviblurb'),
                'type'            => 'select',
                'options'         => array(
                    'off' => esc_html__('In The Same Window', 'dicm-divi-custom-modules'),
                    'on'  => esc_html__('In The New Tab', 'dicm-divi-custom-modules'),
                ),
                'option_category' => 'basic_option',
                'toggle_slug'     => 'button',
                'description'     => esc_html__('Choose whether your link opens in a new window or not', 'dicm-divi-custom-modules')
            ),
            'button_full_width'  => array(
                'label'             => esc_html__('Enable Button Fullwidth', 'divi_flash'),
                'type'              => 'yes_no_button',
                'options'           => array(
                    'off' => esc_html__('Off', 'divi_flash'),
                    'on'  => esc_html__('On', 'divi_flash'),
                ),
                'default'           => 'off',
                'toggle_slug'       => 'design_button',
                'tab_slug'        => 'advanced',
            ),
            'button_alignment' => array(
                'label'           => esc_html__('Button Alignment', 'divi_flash'),
                'type'            => 'text_align',
                'options'         => et_builder_get_text_orientation_options(array('justified')),
                'toggle_slug'     => 'design_button',
                'tab_slug'        => 'advanced',
                'mobile_options'  => true,
                'show_if'         => array(
                    'button_full_width'     => 'off'
                )
            ),
        );
        $badge = array(
            'badge' => array(
                'label'           => esc_html__('Badge Line 1', 'divi_flash'),
                'type'            => 'text',
                'option_category' => 'basic_option',
                'description'     => esc_html__('Badge entered here will appear inside the module.', 'divi_flash'),
                'toggle_slug'     => 'badge',
                'show_if'         => array(
                    'badge_enable'     => 'on'
                ),
                'show_if_not'     => array(
                    'badge_icon_enable'       => 'on'
                )
            ),
            
            'badge_text_2' => array(
                'label'           => esc_html__('Badge Line 2', 'divi_flash'),
                'type'            => 'text',
                'option_category' => 'basic_option',
                'description'     => esc_html__('Badge entered here will appear inside the module.', 'divi_flash'),
                'toggle_slug'     => 'badge',
                'show_if'         => array(
                    'badge_enable'     => 'on'
                ),
                'show_if_not'     => array(
                    'badge_icon_enable'       => 'on'
                )
            ),
            'badge_icon_enable'  => array(
                'label'             => esc_html__('Use Badge Icon', 'divi_flash'),
                'type'              => 'yes_no_button',
                'options'           => array(
                    'off' => esc_html__('Off', 'divi_flash'),
                    'on'  => esc_html__('On', 'divi_flash'),
                ),
                'default'           => 'off',
                'toggle_slug'     => 'badge',
                'show_if'         => array(
                    'badge_enable'     => 'on'
                )
            ),
            'badge_icon' => array(
                'label'                 => esc_html__('Badge Icon', 'divi_flash'),
                'type'                  => 'select_icon',
                'class' => array('et-pb-font-icon'),
                'default' => '4',
                'option_category' => 'basic_option',
                'toggle_slug'     => 'badge',
                //'depends_show_if'     => 'on',
                'show_if'         => array(
                    'badge_icon_enable'     => 'on'
                )
            ),
            'badge_alignment' => array(
                'label'           => esc_html__('Badge Alignment', 'divi_flash'),
                'type'            => 'text_align',
                'options'         => et_builder_get_text_orientation_options(array('justified')),
                'toggle_slug'     => 'design_badge',
                'tab_slug'        => 'advanced',
                'mobile_options'  => true,
                'show_if'         => array(
                    'badge_enable'     => 'on'
                )
            ),

            'badge_icon_color' => array(
                'label'                 => esc_html__('Badge Icon Color', 'divi_flash'),

                'type'            => 'color-alpha',
                'toggle_slug'   => 'design_badge',
                'tab_slug'        => 'advanced',
                //'depends_show_if'     => 'on',
                'show_if'         => array(
                    'badge_icon_enable'     => 'on'
                ),
                'hover'            => 'tabs'
            ),

            'badge_icon_size' => array(
                'label'             => esc_html__('Badge Icon Size', 'divi_flash'),
                'type'              => 'range',
                'toggle_slug'       => 'design_badge',
                'tab_slug'       => 'advanced',
                'default'           => '90px',
                'allowed_units'     => array('px'),
                'range_settings'    => array(
                    'min'  => '0',
                    'max'  => '500',
                    'step' => '1',
                ),
                'responsive'        => true,
                'mobile_options'    => true,
                'show_if'         => array(
                    'badge_icon_enable'     => 'on'
                ),
            ),
           
        );
        $title_background = $this->df_add_bg_field(array(
            'label'                 => 'Title Background',
            'key'                   => 'title_background',
            'toggle_slug'           => 'design_title',
            'tab_slug'              => 'advanced'
        ));
        $sub_title_background = $this->df_add_bg_field(array(
            'label'                 => 'Sub Title Background',
            'key'                   => 'sub_title_background',
            'toggle_slug'           => 'design_sub_title',
            'tab_slug'              => 'advanced'
        ));
        $content_background = $this->df_add_bg_field(array(
            'label'                 => 'Content Background',
            'key'                   => 'content_background',
            'toggle_slug'           => 'design_content_bg_and_others',
            'tab_slug'              => 'advanced'
        ));
        $button_background = $this->df_add_bg_field(array(
            'label'                 => 'Button Background',
            'key'                   => 'button_background',
            'toggle_slug'           => 'design_button',
            'tab_slug'              => 'advanced'
        ));

        $badge_background = $this->df_add_bg_field(array(
            'label'                 => 'Badge Background',
            'key'                   => 'badge_background',
            'toggle_slug'           => 'design_badge',
            'tab_slug'              => 'advanced',
            'show_if'         => array(
                'badge_enable'     => 'on'
            )
        ));

        $image = array(

            'blurb_icon_enable'  => array(
                'label'             => esc_html__('Use Icon', 'divi_flash'),
                'type'              => 'yes_no_button',
                'options'           => array(
                    'off' => esc_html__('Off', 'divi_flash'),
                    'on'  => esc_html__('On', 'divi_flash'),
                ),
                'default'           => 'off',
                'toggle_slug'       => 'image',
            ),

            'blurb_icon' => array(
                'label'                 => esc_html__('Icon', 'divi_flash'),
                'type'                  => 'select_icon',
                'class' => array('et-pb-font-icon'),
                'default' => '4',
                'option_category' => 'basic_option',
                'toggle_slug'           => 'image',
                //'depends_show_if'     => 'on',
                'show_if'         => array(
                    'blurb_icon_enable'     => 'on'
                )
            ),


            'image' => array(
                'label'                 => esc_html__('Image', 'divi_flash'),
                'type'                  => 'upload',
                'upload_button_text'    => esc_attr__('Upload an image', 'divi_flash'),
                'choose_text'           => esc_attr__('Choose an Image', 'divi_flash'),
                'update_text'           => esc_attr__('Set As Image', 'divi_flash'),
                'toggle_slug'           => 'image',
                'show_if'         => array(
                    'blurb_icon_enable'     => 'off'
                )

            ),

            'blurb_icon_color' => array(
                'label'                 => esc_html__('Icon Color', 'divi_flash'),

                'type'            => 'color-alpha',
                'toggle_slug'   => 'design_icon',
                'tab_slug'        => 'advanced',
                //'depends_show_if'     => 'on',
                'show_if'         => array(
                    'blurb_icon_enable'     => 'on'
                ),
                'hover'            => 'tabs'
            ),

            'icon_size' => array(
                'label'             => esc_html__('Icon Size', 'divi_flash'),
                'type'              => 'range',
                'toggle_slug'       => 'design_icon',
                'tab_slug'       => 'advanced',
                'default'           => '90px',
                'allowed_units'     => array('px'),
                'range_settings'    => array(
                    'min'  => '0',
                    'max'  => '500',
                    'step' => '1',
                ),
                'responsive'        => true,
                'mobile_options'    => true,
                'show_if'         => array(
                    'blurb_icon_enable'     => 'on'
                ),
            ),

            'blurb_icon_background_color' => array(
                'label'                 => esc_html__('Icon Background Color', 'divi_flash'),

                'type'            => 'color-alpha',
                'toggle_slug'   => 'design_icon',
                'tab_slug'        => 'advanced',
                //'depends_show_if'     => 'on',
                'show_if'         => array(
                    'blurb_icon_enable'     => 'on'
                ),
                'hover'            => 'tabs'
            ),
         
            'blurb_img_background_color' => array(
                'label'                 => esc_html__('Image Background Color', 'divi_flash'),

                'type'            => 'color-alpha',
                'toggle_slug'   => 'design_image',
                'tab_slug'        => 'advanced',
                'show_if'     => array(
                    'blurb_icon_enable'     => 'off'
                ),
                'hover'            => 'tabs'

            ),

            'image' => array(
                'label'                 => esc_html__('Image', 'divi_flash'),
                'type'                  => 'upload',
                'upload_button_text'    => esc_attr__('Upload an image', 'divi_flash'),
                'choose_text'           => esc_attr__('Choose an Image', 'divi_flash'),
                'update_text'           => esc_attr__('Set As Image', 'divi_flash'),
                'toggle_slug'           => 'image',
                'show_if'         => array(
                    'blurb_icon_enable'     => 'off'
                )

            ),
            'image_icon_container_position'  => array(
                'label'           => esc_html__('Image or Icon Container Placement', 'divi_flash'),
                'type'            => 'select',
                'options'         => array(
                    'inside'      => esc_html__('Inside Content Area', 'divi_flash'),
                    'outside'     => esc_html__('Outside Content Area', 'divi_flash'),
                 ),
                'default'           => 'inside',
                'option_category' => 'basic_option',
                'toggle_slug'     => 'image',
                // 'mobile_options'    => true,
                'description'     => esc_html__('Choose Image or Icon container placement', 'divi_flash')
            ),

            'image_placement'  => array(
                'label'           => esc_html__('Image or Icon Position ', 'divi_flash'),
                'type'            => 'select',
                'options'         => array(
                    'flex_top'      => esc_html__('Top', 'divi_flash'),
                    'flex_left'     => esc_html__('Left', 'divi_flash'),
                    'flex_right'    => esc_html__('Right', 'divi_flash'),
                    'flex_bottom'    => esc_html__('Bottom', 'divi_flash'),
                 ),
                'default'           => 'flex_top',
                'option_category' => 'basic_option',
                'toggle_slug'     => 'image',
                 'show_if'         => array(
                    'image_icon_container_position'     => 'outside'
                 ),
                // 'mobile_options'    => true,
                'description'     => esc_html__('Choose image position', 'divi_flash')
            ),
            'image_icon_alignment' => array(
                'label'           => esc_html__('Image or Icon Alignment', 'divi_flash'),
                'type'            => 'text_align',
                'options'         => et_builder_get_text_orientation_options(array('justified')),
                'toggle_slug'     => 'image',
                'mobile_options'    => true,
               
            ),
            'image_icon_item_align' => array(
                'label'           => esc_html__('Image or Icon Item Align ', 'divi_flash'),
                'type'            => 'select',
                'options'         => array(
                    'flex-start' => esc_html__('Start', 'divi_flash'),
                    'center'     => esc_html__('Center', 'divi_flash'),
                    'flex-end'   => esc_html__('End', 'divi_flash'),
                ),
                'default'           => 'normal',
                'option_category' => 'basic_option',
                'toggle_slug'     => 'image',
                'show_if'         => array(
                    'image_placement'     => array('flex_left', 'flex_right')
                ),
                'description'     => esc_html__('Choose Image or Icon Item Align', 'divi_flash')
            ),
            'image_container_width' => array(
                'label'             => esc_html__('Image Container Width', 'divi_flash'),
                'type'              => 'range',
                'toggle_slug'       => 'design_image',
                'tab_slug'       => 'advanced',
                'default'           => '20%',
                'allowed_units'     => array('%'),
                'range_settings'    => array(
                    'min'  => '0',
                    'max'  => '100',
                    'step' => '1',
                ),
                'responsive'        => true,
                'mobile_options'    => true,
                'show_if'         => array(
                    'image_placement'     => array('flex_left', 'flex_right'),
                    'blurb_icon_enable'     => 'off',
                    'image_icon_container_position' => 'outside'
                ),
                'description'     => esc_html__('Set Image container Width', 'divi_flash')
            ),
            'alt_text' => array(
                'label'                 => esc_html__('Image Alt Text', 'divi_flash'),
                'type'                  => 'text',
                'toggle_slug'           => 'image',
                'show_if'         => array(
                    'blurb_icon_enable'     => 'off'
                ),
                'show_if_not'     => array(
                    'image' => array('')
                ),
            ),

        );
        $content_area = array(
            'content_area_alignment'     => array(
                'label'             => esc_html__('Content Area Alignment', 'divi_flash'),
                'type'              => 'text_align',
                'tab_slug'          => 'advanced',
                'toggle_slug'       => 'design_content_area',
                'options'           =>  et_builder_get_text_orientation_options( array( 'justified' ) ),
                'mobile_options'    => true,
            )
        ); 
        $content_area_background = $this->df_add_bg_field(array(
            'label'                 => 'Content Area Background',
            'key'                   => 'content_area_background',
            'toggle_slug'           => 'design_content_area',
            'tab_slug'              => 'advanced',
            'show_if'         => array(
                'image_icon_container_position'     => 'outside'
            ),
        ));
        $z_index = array(
            'blurb_img_zindex' => array(
                'label'             => esc_html__('Image or Icon z-index', 'divi_flash'),
                'type'              => 'range',
                'toggle_slug'       => 'design_zindex',
                'tab_slug'          => 'advanced',
                'default'           => '0',
                'range_settings'    => array(
                    'min'  => '-100',
                    'max'  => '1000',
                    'step' => '1',
                ),
                'validate_unit'     => false,
                'mobile_options'    => true,
                'show_if_not'     => array(
                    'image' => array('')
                ),
            ),

            'title_zindex' => array(
                'label'             => esc_html__('Title z-index', 'divi_flash'),
                'type'              => 'range',
                'toggle_slug'       => 'design_zindex',
                'tab_slug'          => 'advanced',
                'default'           => '0',
                'range_settings'    => array(
                    'min'  => '-100',
                    'max'  => '1000',
                    'step' => '1',
                ),
                'validate_unit'     => false,
                'mobile_options'    => true,
            ),

            'sub_title_zindex' => array(
                'label'             => esc_html__('Sub Title z-index', 'divi_flash'),
                'type'              => 'range',
                'toggle_slug'       => 'design_zindex',
                'tab_slug'          => 'advanced',
                'default'           => '0',
                'range_settings'    => array(
                    'min'  => '-100',
                    'max'  => '1000',
                    'step' => '1',
                ),
                'validate_unit'     => false,
                'mobile_options'    => true,
            ),

            'content_zindex' => array(
                'label'             => esc_html__('Content z-index', 'divi_flash'),
                'type'              => 'range',
                'toggle_slug'       => 'design_zindex',
                'tab_slug'          => 'advanced',
                'default'           => '0',
                'range_settings'    => array(
                    'min'  => '-100',
                    'max'  => '1000',
                    'step' => '1',
                ),
                'validate_unit'     => false,
                'mobile_options'    => true,
            ),

            'badge_zindex' => array(
                'label'             => esc_html__('Badge z-index', 'divi_flash'),
                'type'              => 'range',
                'toggle_slug'       => 'design_zindex',
                'tab_slug'          => 'advanced',
                'default'           => '0',
                'range_settings'    => array(
                    'min'  => '-100',
                    'max'  => '1000',
                    'step' => '1',
                ),
                'validate_unit'     => false,
                'mobile_options'    => true,
                'show_if'         => array(
                    'badge_enable'     => 'on'
                ),
            ),

           'button_zindex' => array(
                'label'             => esc_html__('Button z-index', 'divi_flash'),
                'type'              => 'range',
                'toggle_slug'       => 'design_zindex',
                'tab_slug'          => 'advanced',
                'default'           => '0',
                'range_settings'    => array(
                    'min'  => '-100',
                    'max'  => '1000',
                    'step' => '1',
                ),
                'validate_unit'     => false,
                'mobile_options'    => true,
            ),

        );
        $sizeing = array(
            'content_width' => array(
                'label'             => esc_html__('Content Width', 'divi_flash'),
                'type'              => 'range',
                'toggle_slug'       => 'width',
                'tab_slug'       => 'advanced',
                'default'           => '540px',
                'allowed_units'     => array('px', '%'),
                'range_settings'    => array(
                    'min'  => '0',
                    'max'  => '1920',
                    'step' => '1',
                ),
                'responsive'        => true,
                'mobile_options'    => true,
            ),

            'image_width' => array(
                'label'             => esc_html__('Image Width(%)', 'divi_flash'),
                'type'              => 'range',
                'toggle_slug'       => 'width',
                'tab_slug'          => 'advanced',
                'default'           => '100%',
                'default_unit'      => '%',
                'allowed_units'     => array('%', 'px'),
                'range_settings'    => array(
                    'min'  => '0',
                    'max'  => '100',
                    'step' => '1',
                ),
                'responsive'        => true,
                'mobile_options'    => true,
                'show_if_not'       => array(
                    'image' => array('')
                ),
            ),
        );
        $wrapper_spacing = array(
            'wrapper_margin' => array(
                'label'               => sprintf(esc_html__('Wrapper Margin', 'divi_flash')),
                'toggle_slug' => 'custom_spacing',
                'tab_slug'    => 'advanced',
                'sub_toggle'  => 'wrapper',
                'type'            => 'custom_margin',
                'hover'            => 'tabs',
                'responsive'        => true,
                'mobile_options'    => true,
            ),
            'wrapper_padding' => array(
                'label'               => sprintf(esc_html__('Wrapper Padding', 'divi_flash')),
                'toggle_slug' => 'custom_spacing',
                'tab_slug'    => 'advanced',
                'sub_toggle'  => 'wrapper',
                'type'         => 'custom_margin',
                'hover'            => 'tabs',
                'responsive'        => true,
                'mobile_options'    => true,
            ),
    
            'blurb_img_margin' => array(
                'label'               => sprintf(esc_html__('Image or Icon Wrapper Margin', 'divi_flash')),
                'toggle_slug' => 'custom_spacing',
                'tab_slug'    => 'advanced',
                'sub_toggle'  => 'wrapper',
                'type'        => 'custom_margin',
                'hover'            => 'tabs',
                'responsive'        => true,
                'mobile_options'    => true,
            ),

            'blurb_icon_spacing' => array(
                'label'           => esc_html__('Icon Wrapper Padding', 'divi_flash'),
                'type'            => 'custom_margin',
                'toggle_slug'       => 'custom_spacing',
                'tab_slug'        => 'advanced',
                'sub_toggle'  => 'wrapper',
                'show_if'         => array(
                    'blurb_icon_enable'     => 'on'
                ),
                'hover'            => 'tabs',
                'responsive'        => true,
                'mobile_options'    => true,
            ),

            'blurb_img_spacing' => array(
                'label'           => esc_html__('Image Wrapper Padding', 'divi_flash'),
                'type'            => 'custom_margin',
                'toggle_slug'       => 'custom_spacing',
                'tab_slug'    => 'advanced',
                'sub_toggle'  => 'wrapper',
                'show_if'         => array(
                    'blurb_icon_enable'     => 'off'
                ), 
                'show_if_not'     => array(
                    'image' => array('')
                ),
                'hover'            => 'tabs',
                'mobile_options'    => true,
            ),
           

            'button_wrapper_margin' => array(
                'label'               => sprintf(esc_html__('Button Wrapper Margin', 'divi_flash')),
                'toggle_slug' => 'custom_spacing',
                'tab_slug'    => 'advanced',
                'sub_toggle'  => 'wrapper',
                'type'        => 'custom_margin',
                'hover'            => 'tabs',
                'responsive'        => true,
                'mobile_options'    => true,
            ),
            'button_wrapper_padding' => array(
                'label'               => sprintf(esc_html__('Button Wrapper Padding', 'divi_flash')),
                'toggle_slug' => 'custom_spacing',
                'tab_slug'    => 'advanced',
                'sub_toggle'  => 'wrapper',
                'type'        => 'custom_margin',
                'hover'            => 'tabs',
                'responsive'        => true,
                'mobile_options'    => true,
            ),

            'badge_wrapper_margin' => array(
                'label'               => sprintf(esc_html__('Badge Wrapper Margin', 'divi_flash')),
                'toggle_slug' => 'design_badge',
                'tab_slug'    => 'advanced',
                'sub_toggle'  => 'content',
                'type'            => 'custom_margin',
                'hover'            => 'tabs',
                'responsive'        => true,
                'mobile_options'    => true,
                'show_if'         => array(
                    'badge_enable'     => 'on'
                )
            ),

        );
        $item_order = array(
            'order_enable'  => array(
                'label'             => esc_html__('Enable Order', 'divi_flash'),
                'type'              => 'yes_no_button',
                'options'           => array(
                    'off' => esc_html__('Off', 'divi_flash'),
                    'on'  => esc_html__('On', 'divi_flash'),
                ),
                'default'           => 'off',
                'toggle_slug'       => 'item_order',
            ),
            'image_order' => array(
                'label'             => esc_html__('Image/Icon Order', 'divi_flash'),
                'type'              => 'range',
                'toggle_slug'       => 'item_order',
                'default'           => '9',
                'range_settings'    => array(
                    'min'  => '1',
                    'max'  => '15',
                    'step' => '1',
                ),
                'validate_unit'     => false,
                'show_if'         => array(
                    'order_enable'     => 'on',
                    'image_icon_container_position'     => 'inside'
                ),
              
                'description'       => esc_html__('Increase the order number to position the item lower.', 'divi_flash')
            ),
            'title_order' => array(
                'label'             => esc_html__('Title Order', 'divi_flash'),
                'type'              => 'range',
                'toggle_slug'       => 'item_order',
                'default'           => '9',
                'range_settings'    => array(
                    'min'  => '1',
                    'max'  => '15',
                    'step' => '1',
                ),
                'validate_unit'     => false,
                'show_if'         => array(
                    'order_enable'     => 'on'
                ),
                'description'       => esc_html__('Increase the order number to position the item lower.', 'divi_flash')
            ),
            'sub_title_order' => array(
                'label'             => esc_html__('Sub Title Order', 'divi_flash'),
                'type'              => 'range',
                'toggle_slug'       => 'item_order',
                'default'           => '9',
                'range_settings'    => array(
                    'min'  => '1',
                    'max'  => '15',
                    'step' => '1',
                ),
                'validate_unit'     => false,
                'show_if'         => array(
                    'order_enable'     => 'on'
                ),
                'description'       => esc_html__('Increase the order number to position the item lower.', 'divi_flash')
            ),
            'content_order' => array(
                'label'             => esc_html__('Content Order', 'divi_flash'),
                'type'              => 'range',
                'toggle_slug'       => 'item_order',
                'default'           => '9',
                'range_settings'    => array(
                    'min'  => '1',
                    'max'  => '15',
                    'step' => '1',
                ),
                'validate_unit'     => false,
                'show_if'         => array(
                    'order_enable'     => 'on'
                ),
                'description'       => esc_html__('Increase the order number to position the item lower.', 'divi_flash')
            ),
            'button_order' => array(
                'label'             => esc_html__('Button Order', 'divi_flash'),
                'type'              => 'range',
                'toggle_slug'       => 'item_order',
                'default'           => '9',
                'range_settings'    => array(
                    'min'  => '1',
                    'max'  => '15',
                    'step' => '1',
                ),
                'validate_unit'     => false,
                'show_if'         => array(
                    'order_enable'     => 'on'
                ),
                'description'       => esc_html__('Increase the order number to position the item lower.', 'divi_flash')
            ),
            'badge_order' => array(
                'label'             => esc_html__('Badge Order', 'divi_flash'),
                'type'              => 'range',
                'toggle_slug'       => 'item_order',
                'default'           => '9',
                'range_settings'    => array(
                    'min'  => '1',
                    'max'  => '15',
                    'step' => '1',
                ),
                'validate_unit'     => false,
                'show_if'         => array(
                    'order_enable'     => 'on',
                    'badge_enable'     => 'on',
                ),
                'description'       => esc_html__('Increase the order number to position the item lower.', 'divi_flash')
            )

        );
        $content_spacing = array(
            'content_area_margin' => array(
                'label'               => sprintf(esc_html__('Content Area Margin', 'divi_flash')),
                'toggle_slug' => 'custom_spacing',
                'tab_slug'    => 'advanced',
                'sub_toggle'  => 'content',
                'type'        => 'custom_margin',
                'hover'            => 'tabs',
                'responsive'        => true,
                'mobile_options'    => true,
                'show_if'         => array(
                    'image_icon_container_position' => 'outside'
                ),
            ),
            'content_area_padding' => array(
                'label'               => sprintf(esc_html__('Content Area Padding', 'divi_flash')),
                'toggle_slug' => 'custom_spacing',
                'tab_slug'    => 'advanced',
                'sub_toggle'  => 'content',
                'type'        => 'custom_margin',
                'hover'            => 'tabs',
                'responsive'        => true,
                'mobile_options'    => true,
                'show_if'         => array(
                    'image_icon_container_position' => 'outside'
                ),
            ),
            'title_margin' => array(
                'label'               => sprintf(esc_html__('Title Margin', 'divi_flash')),
                'toggle_slug' => 'custom_spacing',
                'tab_slug'    => 'advanced',
                'sub_toggle'  => 'content',
                'type'            => 'custom_margin',
                'hover'            => 'tabs',
                'responsive'        => true,
                'mobile_options'    => true,

            ),
            'title_padding' => array(
                'label'               => sprintf(esc_html__('Title Padding', 'divi_flash')),
                'toggle_slug' => 'custom_spacing',
                'sub_toggle'  => 'content',
                'tab_slug'    => 'advanced',
                'type'            => 'custom_margin',
                'hover'            => 'tabs',
                'responsive'        => true,
                'mobile_options'    => true,
            ),
            'sub_title_margin' => array(
                'label'               => sprintf(esc_html__('Sub Title Margin', 'divi_flash')),
                'toggle_slug' => 'custom_spacing',
                'sub_toggle'  => 'content',
                'tab_slug'    => 'advanced',
                'type'            => 'custom_margin',
                'hover'            => 'tabs',
                'responsive'        => true,
                'mobile_options'    => true,
            ),
            'sub_title_padding' => array(
                'label'               => sprintf(esc_html__('Sub Title Padding', 'divi_flash')),
                'toggle_slug' => 'custom_spacing',
                'sub_toggle'  => 'content',
                'tab_slug'    => 'advanced',
                'type'            => 'custom_margin',
                'hover'            => 'tabs',
                'responsive'        => true,
                'mobile_options'    => true,
            ),
            'content_margin' => array(
                'label'               => sprintf(esc_html__('Content Margin', 'divi_flash')),
                'toggle_slug' => 'custom_spacing',
                'tab_slug'    => 'advanced',
                'sub_toggle'  => 'content',
                'type'            => 'custom_margin',
                'hover'            => 'tabs',
                'responsive'        => true,
                'mobile_options'    => true,
            ),
            'content_padding' => array(
                'label'               => sprintf(esc_html__('Content Padding', 'divi_flash')),
                'toggle_slug' => 'custom_spacing',
                'tab_slug'    => 'advanced',
                'sub_toggle'  => 'content',
                'type'            => 'custom_margin',
                'hover'            => 'tabs',
                'responsive'        => true,
                'mobile_options'    => true,
            ),

            'button_margin' => array(
                'label'               => sprintf(esc_html__('Button Margin', 'divi_flash')),
                'toggle_slug' => 'custom_spacing',
                'tab_slug'    => 'advanced',
                'sub_toggle'  => 'content',
                'type'            => 'custom_margin',
                'hover'            => 'tabs',
                'responsive'        => true,
                'mobile_options'    => true,
            ),
            'button_padding' => array(
                'label'               => sprintf(esc_html__('Button Padding', 'divi_flash')),
                'toggle_slug' => 'custom_spacing',
                'tab_slug'    => 'advanced',
                'sub_toggle'  => 'content',
                'type'            => 'custom_margin',
                'hover'            => 'tabs',
                'responsive'        => true,
                'mobile_options'    => true,
            ),
            'badge_margin' => array(
                'label'               => sprintf(esc_html__('Badge Margin', 'divi_flash')),
                'toggle_slug' => 'design_badge',
                'tab_slug'    => 'advanced',
                'sub_toggle'  => 'content',
                'type'            => 'custom_margin',
                'hover'            => 'tabs',
                'responsive'        => true,
                'mobile_options'    => true,
                'show_if'         => array(
                    'badge_enable'     => 'on'
                )
            ),
            'badge_padding' => array(
                'label'               => sprintf(esc_html__('Badge Padding', 'divi_flash')),
                'toggle_slug' => 'design_badge',
                'tab_slug'    => 'advanced',
                'sub_toggle'  => 'content',
                'type'            => 'custom_margin',
                'hover'            => 'tabs',
                'responsive'        => true,
                'mobile_options'    => true,
                'show_if'=> array(
                    'badge_enable' => 'on'
                )
            ),
            'badge_icon_margin' => array(
                'label'               => sprintf(esc_html__('Badge Icon Margin', 'divi_flash')),
                'toggle_slug' => 'design_badge',
                'tab_slug'    => 'advanced',
                'sub_toggle'  => 'content',
                'type'            => 'custom_margin',
                'hover'            => 'tabs',
                'responsive'        => true,
                'mobile_options'    => true,
                'show_if'         => array(
                    'badge_enable'          => 'on',
                    'badge_icon_enable'     => 'on'
                )
            ),
            'badge_text_1_margin' => array(
                'label'               => sprintf(esc_html__('Badge Line 1 Margin', 'divi_flash')),
                'toggle_slug' => 'design_badge',
                'tab_slug'    => 'advanced',
                'sub_toggle'  => 'content',
                'type'            => 'custom_margin',
                'hover'            => 'tabs',
                'responsive'        => true,
                'mobile_options'    => true,
                'show_if'         => array(
                    'badge_enable'     => 'on'
                )
            ),
            'badge_text_1_padding' => array(
                'label'               => sprintf(esc_html__('Badge Line 1 Padding', 'divi_flash')),
                'toggle_slug' => 'design_badge',
                'tab_slug'    => 'advanced',
                'sub_toggle'  => 'content',
                'type'            => 'custom_margin',
                'hover'            => 'tabs',
                'responsive'        => true,
                'mobile_options'    => true,
                'show_if'         => array(
                    'badge_enable'     => 'on'
                )
            ),
        );

        return array_merge(
            $content,
            $button,
            $badge_background,
            $badge,
            $image,
            $title_background,
            $sub_title_background,
            $content_background,
            $button_background,
            $content_area,
            $content_area_background,
            $item_order,
            $z_index,
            $sizeing,
            $wrapper_spacing,
            $content_spacing
        );
    } 

    public function df_ab_flex_direction($placement_slug)
    {
        $flex_direction = '';
        if ($placement_slug == 'right') {
            $flex_direction = 'row-reverse';
            $add_css = 'margin-left: 20px';
        } else if ($placement_slug == 'top') {
            $flex_direction = 'column';
            $add_css = 'margin-left: 0';
        } else if ($placement_slug == 'left') {
            $flex_direction = 'row';
            $add_css = 'margin-right: 20px';
        }
        return $flex_direction;
    }

    public function get_transition_fields_css_props()
    {
        $fields = parent::get_transition_fields_css_props();

        $button_wrapper  = '%%order_class%% .df_ab_blurb_button_wrapper';
        $title           = '%%order_class%% .df_ab_blurb_title_title';
        $subtitle        = '%%order_class%% .df_ab_blurb_sub_title';
        $content         = '%%order_class%% .df_ab_blurb_description';
        $button          = '%%order_class%% .df_ab_blurb_button';
        $badge_wrapper   = '%%order_class%% .df_ab_blurb_badge_wrapper';
        $badge           = '%%order_class%% .df_ab_blurb_badge';
        $badge_text_1    = '%%order_class%% .df_ab_blurb_badge .badge_text_1';
        $badge_text_2    = '%%order_class%% .df_ab_blurb_badge .badge_text_2';
        $badge_icon      = '%%order_class%% .df_ab_blurb_badge .et-pb-icon'; 
        $icon            = '%%order_class%% .et-pb-icon'; 
        $image           =  '%%order_class%% .df_ab_blurb_image_img';  
        $image_and_icon_wrapper = '%%order_class%% .df_ab_blurb_image'; 
        $content_area = '%%order_class%%  .df_ab_blurb_content_container';
        // spacing
        $fields['button_wrapper_margin']    = array('margin' => $button_wrapper);
        $fields['button_wrapper_padding']   = array('padding' => $button_wrapper);
        
        $fields['button_margin']            = array('margin' => $button);
        $fields['button_padding']           = array('padding' => $button);
        $fields['badge_wrapper_margin']   = array('margin' => $badge_wrapper);
        $fields['badge_margin']            = array('margin' => $badge);
        $fields['badge_padding']           = array('padding' => $badge);
        $fields['badge_icon_margin']            = array('margin' => $badge_icon);
        $fields['badge_text_1_margin']            = array('margin' => $badge_text_1);
        $fields['badge_text_1_padding']           = array('padding' => $badge_text_1);
        // $fields['badge_text_2_margin']            = array('margin' => $badge_text_1);
        // $fields['badge_text_2_padding']           = array('padding' => $badge_text_2);
        $fields['blurb_icon_spacing']           = array('padding' => $icon);
        $fields['blurb_img_spacing']           = array('padding' => $image);
        

        $fields['wrapper_padding']  = array('padding' => '%%order_class%% .df_ab_blurb_container');
        $fields['wrapper_margin']   = array('margin' => '%%order_class%% .df_ab_blurb_container');

        $fields['blurb_img_margin'] = array('margin' => '%%order_class%% .df_ab_blurb_image');
  
        $fields['image_margin'] = array('margin' => '%%order_class%% .df_ab_blurb_image img');
      
        $fields['content_area_margin'] = array('margin' => $content_area);
        $fields['content_area_padding'] = array('padding' => $content_area);
        $fields['title_margin'] = array('margin' => $title);
        $fields['title_padding'] = array('padding' => $title);

        $fields['sub_title_margin'] = array('margin' => $subtitle);
        $fields['sub_title_padding'] = array('padding' => $subtitle);

        $fields['content_margin'] = array('margin' => $content);
        $fields['content_padding'] = array('padding' => $content);

       // Color
       $fields['blurb_icon_color'] = array('color' => $icon);
       $fields['badge_icon_color'] = array('color' => $badge_icon);
       $fields['blurb_icon_background_color'] = array('background' => $icon);

       
       $fields['blurb_img_background_color'] = array('background' => $image);

        // background
        $fields = $this->df_background_transition(array(
            'fields'        => $fields,
            'key'           => 'button_background',
            'selector'      => $button
        ));
        $fields = $this->df_background_transition(array(
            'fields'        => $fields,
            'key'           => 'badge_background',
            'selector'      => $badge
        ));
        $fields = $this->df_background_transition(array(
            'fields'        => $fields,
            'key'           => 'content_area_background',
            'selector'      => $content_area
        ));
        $fields = $this->df_background_transition(array(
            'fields'        => $fields,
            'key'           => 'title_background',
            'selector'      => $title
        ));
        $fields = $this->df_background_transition(array(
            'fields'        => $fields,
            'key'           => 'sub_title_background',
            'selector'      => $subtitle
        ));
        $fields = $this->df_background_transition(array(
            'fields'        => $fields,
            'key'           => 'content_background',
            'selector'      => $content
        ));

        // border fix
        $fields = $this->df_fix_border_transition(
            $fields,
            'content_area_border',
            $content_area
        );
        $fields = $this->df_fix_border_transition(
            $fields,
            'sub_title_border',
            $button
        );
        $fields = $this->df_fix_border_transition(
            $fields,
            'title_border',
            $button
        );
        $fields = $this->df_fix_border_transition(
            $fields,
            'content_border',
            $button
        );
        $fields = $this->df_fix_border_transition(
            $fields,
            'button_border',
            $button
        );
        $fields = $this->df_fix_border_transition(
            $fields,
            'badge_border',
            $button
        );
        $fields = $this->df_fix_border_transition(
            $fields,
            'image_border',
            $image
        );
        $fields = $this->df_fix_border_transition(
            $fields,
            'icon_border',
            $image
        );

        return $fields;
    }
    public function additional_css_styles($render_slug)
    {

        if ('off' !== $this->props['order_enable']) {
            $this->df_process_range(array(
                'render_slug'       => $render_slug,
                'slug'              => 'image_order',
                'type'              => 'order',
                'selector'          => '%%order_class%% .df_ab_blurb_image'
            ));

            $this->df_process_range(array(
                'render_slug'       => $render_slug,
                'slug'              => 'title_order',
                'type'              => 'order',
                'selector'          => '%%order_class%% .df_ab_blurb_title'
            ));
            $this->df_process_range(array(
                'render_slug'       => $render_slug,
                'slug'              => 'sub_title_order',
                'type'              => 'order',
                'selector'          => '%%order_class%% .df_ab_blurb_sub_title'
            ));
            $this->df_process_range(array(
                'render_slug'       => $render_slug,
                'slug'              => 'content_order',
                'type'              => 'order',
                'selector'          => '%%order_class%% .df_ab_blurb_description'
            ));
            $this->df_process_range(array(
                'render_slug'       => $render_slug,
                'slug'              => 'button_order',
                'type'              => 'order',
                'selector'          => '%%order_class%% .df_ab_blurb_button_wrapper'
            ));

            $this->df_process_range(array(
                'render_slug'       => $render_slug,
                'slug'              => 'badge_order',
                'type'              => 'order',
                'selector'          => '%%order_class%% .df_ab_blurb_badge_wrapper'
            ));
        }
        // Z index
        $this->df_process_range(array(
            'render_slug'       => $render_slug,
            'slug'              => 'blurb_img_zindex',
            'type'              => 'z-index',
            'selector'          => '%%order_class%% .df_ab_blurb_image'
        ));
       
        $this->df_process_range(array(
            'render_slug'       => $render_slug,
            'slug'              => 'badge_zindex',
            'type'              => 'z-index',
            'selector'          => '%%order_class%% .df_ab_blurb_badge_wrapper'
        ));

        $this->df_process_range(array(
            'render_slug'       => $render_slug,
            'slug'              => 'button_zindex',
            'type'              => 'z-index',
            'selector'          => '%%order_class%% .df_ab_blurb_button_wrapper'
        ));

        $this->df_process_range(array(
            'render_slug'       => $render_slug,
            'slug'              => 'title_zindex',
            'type'              => 'z-index',
            'selector'          => '%%order_class%% .df_ab_blurb_title'
        ));

        $this->df_process_range(array(
            'render_slug'       => $render_slug,
            'slug'              => 'sub_title_zindex',
            'type'              => 'z-index',
            'selector'          => '%%order_class%% .df_ab_blurb_sub_title'
        ));

        $this->df_process_range(array(
            'render_slug'       => $render_slug,
            'slug'              => 'content_zindex',
            'type'              => 'z-index',
            'selector'          => '%%order_class%% .df_ab_blurb_description'
        ));
      
        // Image placement
        $image_placement =  $this->props['image_placement'];

        $this->df_process_string_attr(array(
            'render_slug'       => $render_slug,
            'slug'              => 'image_placement',
            'type'              => 'flex-direction',
            'selector' => "%%order_class%% .df_ab_blurb_container",
            'default'           => 'column'
        ));
        
        if('flex_top' !== $image_placement){
            ET_Builder_Element::set_style($render_slug, array(
                'selector' => "%%order_class%% .df_ab_blurb_container",
                'declaration' => 'flex-direction: column;',
                'media_query' => ET_Builder_Element::get_media_query('max_width_767'),
            ));
        }
        $this->df_process_range(array(
            'render_slug'       => $render_slug,
            'slug'              => 'content_width',
            'type'              => 'max-width',
            'selector'    => '%%order_class%% .df_ab_blurb_container',
        ));

        $this->df_process_string_attr(array(
            'render_slug'       => $render_slug,
            'slug'              => 'image_icon_alignment',
            'type'              => 'text-align',
            'selector'          => "%%order_class%% .df_ab_blurb_image",
            'default'           => 'left'
        ));
    
        $this->df_process_range(array(
            'render_slug'       => $render_slug,
            'slug'              => 'image_width',
            'type'              => 'max-width',
            'selector'    => "%%order_class%% .df_ab_blurb_image_img",
        ));
        
        if ($this->props['image_icon_container_position'] !== 'inside' && ($image_placement === 'flex_left' || $image_placement === 'flex_right')) {
            // Image container and content container design
           if ('off' === $this->props['blurb_icon_enable']) {
                $this->df_process_range(array(
                    'render_slug'       => $render_slug,
                    'slug'              => 'image_container_width',
                    'type'              => 'width',
                    'selector'    => '%%order_class%% .df_ab_blurb_image',
                ));
            }

            $slug = 'image_container_width';
            $selector_property = 'width';
            $image_container_width_desktop  =  !empty($this->props[$slug]) ? 
                $this->df_process_values($this->props[$slug]) : '20%';
            $image_container_width_tablet   =  !empty($this->props[$slug.'_tablet']) ? 
                $this->df_process_values($this->props[$slug.'_tablet']) : $image_container_width_desktop;
            
            $image_container_width_phone   =  !empty($this->props[$slug.'_phone']) ? 
                $this->df_process_values($this->props[$slug.'_phone']) : $image_container_width_tablet;
            
            ET_Builder_Element::set_style($render_slug, array(
                'selector' => "%%order_class%% .df_ab_blurb_content_container",
                'declaration' => $selector_property.':  calc(100% - '.$image_container_width_desktop.');',
            ));
            
            ET_Builder_Element::set_style($render_slug, array(
                'selector' => "%%order_class%% .df_ab_blurb_content_container",
                'declaration' =>$selector_property.':  calc(100% - '.$image_container_width_tablet.');',
                'media_query' => ET_Builder_Element::get_media_query('max_width_980'),
            ));
         

            ET_Builder_Element::set_style($render_slug, array(
                'selector' => "%%order_class%% .df_ab_blurb_content_container",
                'declaration' =>$selector_property.':  100%;',
                'media_query' => ET_Builder_Element::get_media_query('max_width_767'),
            ));

            ET_Builder_Element::set_style($render_slug, array(
                'selector' => "%%order_class%% .df_ab_blurb_container",
                'declaration' => sprintf('
                align-items: %1$s;',  $this->props['image_icon_item_align']),
            ));

         
            if ('' !== $this->props['image_icon_item_align']) {
                ET_Builder_Element::set_style($render_slug, array(
                    'selector' => "%%order_class%% .df_ab_blurb_container",
                    'declaration' => sprintf('
                    align-items: %1$s;',  $this->props['image_icon_item_align']),
                ));
            }
        }

        //  background
        $this->df_process_bg(array(
            'render_slug'       => $render_slug,
            'slug'              => 'content_area_background',
            'selector'          => "%%order_class%% .df_ab_blurb_content_container",
            'hover'             => '%%order_class%% .df_ab_blurb_container:hover .df_ab_blurb_content_container'
        ));
        $this->df_process_bg(array(
            'render_slug'       => $render_slug,
            'slug'              => 'title_background',
            'selector'          => "%%order_class%% .df_ab_blurb_title",
            'hover'             => '%%order_class%% .df_ab_blurb_container:hover .df_ab_blurb_title'
        ));
        $this->df_process_bg(array(
            'render_slug'       => $render_slug,
            'slug'              => 'sub_title_background',
            'selector'          => "%%order_class%% .df_ab_blurb_sub_title",
            'hover'             => '%%order_class%% .df_ab_blurb_container:hover .df_ab_blurb_sub_title'
        ));
        $this->df_process_bg(array(
            'render_slug'       => $render_slug,
            'slug'              => 'content_background',
            'selector'          => "%%order_class%% .df_ab_blurb_description",
            'hover'             => '%%order_class%% .df_ab_blurb_container:hover .df_ab_blurb_description'
        ));
        $this->df_process_bg(array(
            'render_slug'       => $render_slug,
            'slug'              => 'button_background',
            'selector'            => "%%order_class%% .df_ab_blurb_button",
            'hover'             => '%%order_class%% .df_ab_blurb_container:hover .df_ab_blurb_button'
        ));

        $this->df_process_bg(array(
            'render_slug'       => $render_slug,
            'slug'              => 'badge_background',
            'selector'            => "%%order_class%% .df_ab_blurb_badge",
            'hover'             => '%%order_class%% .df_ab_blurb_container:hover .df_ab_blurb_badge'
        ));
        // Badge icon
        if ('on' === $this->props['badge_icon_enable']) {
            $this->df_process_color(array(
                'render_slug'       => $render_slug,
                'slug'              => 'badge_icon_color',
                'type'              => 'color',
                'selector'            => "%%order_class%% .df_ab_blurb_badge .et-pb-icon",
                'hover'             => '%%order_class%% .df_ab_blurb_container:hover .df_ab_blurb_badge .et-pb-icon'
            ));

            $this->df_process_range(array(
                'render_slug'       => $render_slug,
                'slug'              => 'badge_icon_size',
                'type'              => 'font-size',
                'selector'            => "%%order_class%% .df_ab_blurb_badge .et-pb-icon",
            ));
        }
        // Blurb Icon
        if ('on' === $this->props['blurb_icon_enable']) {
            $this->df_process_color(array(
                'render_slug'       => $render_slug,
                'slug'              => 'blurb_icon_color',
                'type'              => 'color',
                'selector'            => "%%order_class%% .et-pb-icon",
                'hover'             => '%%order_class%% .df_ab_blurb_container:hover .et-pb-icon'
            ));

            $this->df_process_range(array(
                'render_slug'       => $render_slug,
                'slug'              => 'icon_size',
                'type'              => 'font-size',
                'selector'            => "%%order_class%% .et-pb-icon",
                'hover'             => '%%order_class%% .df_ab_blurb_container:hover .et-pb-icon'
            ));

            if ('' !== $this->props['blurb_icon_background_color']) {
                $this->df_process_color(array(
                    'render_slug'       => $render_slug,
                    'slug'              => 'blurb_icon_background_color',
                    'type'              => 'background-color',
                    'selector'            => "%%order_class%% .et-pb-icon",
                    'hover'             => '%%order_class%% .df_ab_blurb_container:hover .et-pb-icon'
                ));
            }
        }
        if ('on' === $this->props['blurb_icon_enable'] && '' !== $this->props['blurb_icon_spacing']) {
            $this->set_margin_padding_styles(array(
                'render_slug'       => $render_slug,
                'slug'              => 'blurb_icon_spacing',
                'type'              => 'padding',
                'selector'          => '%%order_class%% .et-pb-icon',
                'hover'             => '%%order_class%% .df_ab_blurb_container:hover .et-pb-icon',
                'important'         => false
            ));
        }

        // Blurb Image
        if ('' !== $this->props['image']) {
            if ('' !== $this->props['blurb_img_background_color']) {

                $this->df_process_color(array(
                    'render_slug'       => $render_slug,
                    'slug'              => 'blurb_img_background_color',
                    'type'              => 'background-color',
                    'selector'            => "%%order_class%% .df_ab_blurb_image_img",
                    'hover'             => '%%order_class%% .df_ab_blurb_container:hover .df_ab_blurb_image_img'
                ));
            }
           $this->set_margin_padding_styles(array(
                    'render_slug'       => $render_slug,
                    'slug'              => 'blurb_img_spacing',
                    'type'              => 'padding',
                    'selector'          => '%%order_class%% .df_ab_blurb_image_img',
                    'hover'             => '%%order_class%% .df_ab_blurb_container:hover .df_ab_blurb_image_img',
                    'important'         => true
            ));
           
        }

        if ('' !== $this->props['blurb_img_margin']) {
            $this->set_margin_padding_styles(array(
                'render_slug'       => $render_slug,
                'slug'              => 'blurb_img_margin',
                'type'              => 'margin',
                'selector'          => '%%order_class%% .df_ab_blurb_image',
                'hover'             => '%%order_class%% .df_ab_blurb_image:hover',
                'important'         => false
            ));
        }

        // Blurb Container spacing
        $this->set_margin_padding_styles(array(
            'render_slug'       => $render_slug,
            'slug'              => 'wrapper_margin',
            'type'              => 'margin',
            'selector'          => '%%order_class%% .df_ab_blurb_container',
            'hover'             => '%%order_class%% .df_ab_blurb_container:hover',
            'important'         => false
        ));
        $this->set_margin_padding_styles(array(
            'render_slug'       => $render_slug,
            'slug'              => 'wrapper_padding',
            'type'              => 'padding',
            'selector'          => '%%order_class%% .df_ab_blurb_container',
            'hover'             => '%%order_class%% .df_ab_blurb_container:hover',
            'important'         => false
        ));

        // Button Design
        if ('on' === $this->props['button_full_width']) {
            ET_Builder_Element::set_style($render_slug, array(
                'selector'    => "%%order_class%% .df_ab_blurb_button",
                'declaration' => 'display: block !important;',
            ));
        }
        if ('off' === $this->props['button_full_width'] &&  '' !== $this->props['button_alignment']) {
            $this->df_process_string_attr(array(
                'render_slug'       => $render_slug,
                'slug'              => 'button_alignment',
                'type'              => 'text-align',
                'selector'          => "%%order_class%% .df_ab_blurb_button_wrapper",
                'default'           => 'left'
            ));
          
        }

        $this->set_margin_padding_styles(array(
            'render_slug'       => $render_slug,
            'slug'              => 'button_wrapper_margin',
            'type'              => 'margin',
            'selector'          => '%%order_class%% .df_ab_blurb_button_wrapper',
            'hover'             => '%%order_class%% .df_ab_blurb_container:hover .df_ab_blurb_button_wrapper',
            'important'         => false
        ));
        $this->set_margin_padding_styles(array(
            'render_slug'       => $render_slug,
            'slug'              => 'button_wrapper_padding',
            'type'              => 'padding',
            'selector'          => '%%order_class%% .df_ab_blurb_button_wrapper',
            'hover'             => '%%order_class%% .df_ab_blurb_container:hover .df_ab_blurb_button_wrapper',
            'important'         => false
        ));
   
        $this->set_margin_padding_styles(array(
            'render_slug'       => $render_slug,
            'slug'              => 'badge_wrapper_margin',
            'type'              => 'margin',
            'selector'          => '%%order_class%% .df_ab_blurb_badge_wrapper',
            'hover'             => '%%order_class%% .df_ab_blurb_container:hover .df_ab_blurb_badge_wrapper',
            'important'         => false
        ));
        

        $this->set_margin_padding_styles(array(
            'render_slug'       => $render_slug,
            'slug'              => 'button_margin',
            'type'              => 'margin',
            'selector'          => '%%order_class%% .df_ab_blurb_button',
            'hover'             => '%%order_class%% .df_ab_blurb_container:hover .df_ab_blurb_button',
            'important'         => false
        ));
        $this->set_margin_padding_styles(array(
            'render_slug'       => $render_slug,
            'slug'              => 'button_padding',
            'type'              => 'padding',
            'selector'          => '%%order_class%% .df_ab_blurb_button',
            'hover'             => '%%order_class%% .df_ab_blurb_container:hover .df_ab_blurb_button',
            'important'         => false
        ));
        // Content area design
        $this->df_process_string_attr(array(
            'render_slug'       => $render_slug,
            'slug'              => 'content_area_alignment',
            'type'              => 'text-align',
            'selector'          => "%%order_class%% .df_ab_blurb_content_container",
            'default'           => 'left'
        ));
        $this->set_margin_padding_styles(array(
            'render_slug'       => $render_slug,
            'slug'              => 'content_area_margin',
            'type'              => 'margin',
            'selector'          => '%%order_class%% .df_ab_blurb_content_container',
            'hover'             => '%%order_class%% .df_ab_blurb_container:hover .df_ab_blurb_content_container',
            'important'         => false
        ));
        $this->set_margin_padding_styles(array(
            'render_slug'       => $render_slug,
            'slug'              => 'content_area_padding',
            'type'              => 'padding',
            'selector'          => '%%order_class%% .df_ab_blurb_content_container',
            'hover'             => '%%order_class%% .df_ab_blurb_container:hover .df_ab_blurb_content_container',
            'important'         => false
        ));

        // Title design
        $this->set_margin_padding_styles(array(
            'render_slug'       => $render_slug,
            'slug'              => 'title_margin',
            'type'              => 'margin',
            'selector'          => '%%order_class%% .df_ab_blurb_title',
            'hover'             => '%%order_class%% .df_ab_blurb_container:hover .df_ab_blurb_title',
            'important'         => false
        ));
        $this->set_margin_padding_styles(array(
            'render_slug'       => $render_slug,
            'slug'              => 'title_padding',
            'type'              => 'padding',
            'selector'          => '%%order_class%% .df_ab_blurb_title',
            'hover'             => '%%order_class%% .df_ab_blurb_container:hover .df_ab_blurb_title',
            'important'         => false
        ));

        // Sub Title
        $this->set_margin_padding_styles(array(
            'render_slug'       => $render_slug,
            'slug'              => 'sub_title_margin',
            'type'              => 'margin',
            'selector'          => '%%order_class%% .df_ab_blurb_sub_title',
            'hover'             => '%%order_class%% .df_ab_blurb_container:hover .df_ab_blurb_sub_title',
            'important'         => false
        ));
        $this->set_margin_padding_styles(array(
            'render_slug'       => $render_slug,
            'slug'              => 'sub_title_padding',
            'type'              => 'padding',
            'selector'          => '%%order_class%% .df_ab_blurb_sub_title',
            'hover'             => '%%order_class%% .df_ab_blurb_container:hover .df_ab_blurb_sub_title',
            'important'         => false
        ));

        // Content design
        $this->set_margin_padding_styles(array(
            'render_slug'       => $render_slug,
            'slug'              => 'content_margin',
            'type'              => 'margin',
            'selector'          => '%%order_class%% .df_ab_blurb_description',
            'hover'             => '%%order_class%% .df_ab_blurb_container:hover .df_ab_blurb_description',
            'important'         => false
        ));
        $this->set_margin_padding_styles(array(
            'render_slug'       => $render_slug,
            'slug'              => 'content_padding',
            'type'              => 'padding',
            'selector'          => '%%order_class%% .df_ab_blurb_description',
            'hover'             => '%%order_class%% .df_ab_blurb_container:hover .df_ab_blurb_description',
            'important'         => false
        ));

        // Badge design 
        if ('on' === $this->props['badge_enable'] &&  '' !== $this->props['badge_alignment']) {
            $this->df_process_string_attr(array(
                'render_slug'       => $render_slug,
                'slug'              => 'badge_alignment',
                'type'              => 'text-align',
                'selector'          => "%%order_class%% .df_ab_blurb_badge_wrapper",
            ));
          
        }

       //if ('on' === $this->props['badge_enable'] &&  '' !== $this->props['badge_text_2']) {
            ET_Builder_Element::set_style($render_slug, array(
                'selector'    => "%%order_class%% .badge_text_1",
                'declaration' => 'display: block !important;',
            ));
          
            ET_Builder_Element::set_style($render_slug, array(
                'selector'    => "%%order_class%% .badge_text_2",
                'declaration' => 'display: block !important;',
            ));
       // }

        $this->set_margin_padding_styles(array(
            'render_slug'       => $render_slug,
            'slug'              => 'badge_margin',
            'type'              => 'margin',
            'selector'          => '%%order_class%% .df_ab_blurb_badge',
            'hover'             => '%%order_class%%  .df_ab_blurb_container:hover .df_ab_blurb_badge',
            'important'         => false
        ));
        $this->set_margin_padding_styles(array(
            'render_slug'       => $render_slug,
            'slug'              => 'badge_padding',
            'type'              => 'padding',
            'selector'          => '%%order_class%% .df_ab_blurb_badge',
            'hover'             => '%%order_class%%  .df_ab_blurb_container:hover .df_ab_blurb_badge',
            'important'         => false
        ));
        $this->set_margin_padding_styles(array(
            'render_slug'       => $render_slug,
            'slug'              => 'badge_icon_margin',
            'type'              => 'margin',
            'selector'          => '%%order_class%% .df_ab_blurb_badge .et-pb-icon',
            'hover'             => '%%order_class%%  .df_ab_blurb_container:hover .df_ab_blurb_badge .et-pb-icon',
            'important'         => false
        ));
     

        $this->set_margin_padding_styles(array(
            'render_slug'       => $render_slug,
            'slug'              => 'badge_text_1_margin',
            'type'              => 'margin',
            'selector'          => '%%order_class%% .badge_text_1',
            'hover'             => '%%order_class%%  .df_ab_blurb_container:hover .badge_text_1',
            'important'         => false
        ));
        $this->set_margin_padding_styles(array(
            'render_slug'       => $render_slug,
            'slug'              => 'badge_text_1_padding',
            'type'              => 'padding',
            'selector'          => '%%order_class%% .badge_text_1',
            'hover'             => '%%order_class%%  .df_ab_blurb_container:hover .badge_text_1',
            'important'         => false
        ));

    }
    public function df_render_image_icon()
    {
        if (isset($this->props['blurb_icon_enable']) && $this->props['blurb_icon_enable'] === 'on') {

            return sprintf(
                '<span class="et-pb-icon">%1$s</span>',
                isset($this->props['blurb_icon']) && $this->props['blurb_icon'] !== '' ?
                    esc_attr(et_pb_process_font_icon($this->props['blurb_icon'])) : '5'
            );
        } else if (isset($this->props['image']) && $this->props['image'] !== '') {
            $src = 'src';
            return sprintf(
                '<img class="df_ab_blurb_image_img" %3$s="%1$s" alt="%2$s" />',
                $this->props['image'],
                $this->props['alt_text'],
                $src
            );
        }
    }

    public function df_render_badge_icon()
    {
        if (isset($this->props['badge_icon_enable']) && $this->props['badge_icon_enable'] === 'on') {

            return sprintf(
                '<span class="et-pb-icon badge_icon">%1$s</span>',
                isset($this->props['badge_icon']) && $this->props['badge_icon'] !== '' ?
                    esc_attr(et_pb_process_font_icon($this->props['badge_icon'])) : '5'
            );
        } 
    }

    public function df_render_button()
    {
        $text = isset($this->props['button_text']) ? $this->props['button_text'] : '';
        $url = isset($this->props['button_url']) ? $this->props['button_url'] : '';
        $target = $this->props['button_url_new_window'] === 'on'  ?
            'target="_blank"' : '';
        if ($text !== '' || $url !== '') {
            return sprintf('<div class="df_ab_blurb_button_wrapper">
            <a href="%1$s" %3$s class="df_ab_blurb_button" data-icon="5">%2$s</a>
        </div>', esc_attr($url), esc_html($text), $target);
        } else {
            return '';
        }
    }
    
    public function render($attrs, $content = null, $render_slug)
    {
        
        $title_level  = $this->props['title_level'];
        $sub_title_level  = $this->props['sub_title_level'];
        $title_html = $this->props['title'] !== '' ?
            sprintf('<%1$s  class="df_ab_blurb_title">%2$s</%1$s >', et_pb_process_header_level($title_level, 'h4'), $this->props['title']) : '';
        $sub_title_html = $this->props['sub_title'] !== '' ?
            sprintf('<%1$s  class="df_ab_blurb_sub_title">%2$s</%1$s >', et_pb_process_header_level($sub_title_level, 'h6'), $this->props['sub_title']) : '';
        $content = $this->props['content'] !== '' ?
            sprintf('<div class="df_ab_blurb_description">%1$s</div>', $this->props['content']) : '';

        $badge_text_1_html = ( $this->props['badge_enable']==='on' && $this->props['badge'] !== '' ) ?
            sprintf('<span class="badge_text_1">%1$s</span>', $this->props['badge']) : '';
        
        $badge_text_2_html = ( $this->props['badge_enable']==='on' && $this->props['badge_text_2'] !== '' ) ?
            sprintf('<span class="badge_text_2">%1$s</span>', $this->props['badge_text_2']) : '';
        
    
        $badge_text_html = ( $this->props['badge'] !== '' && $this->props['badge_icon_enable'] !== 'on') ?
            sprintf('<span class="badge_text_wrapper">
                        %1$s
                        %2$s
                    </span>
                    ',  $badge_text_1_html, $badge_text_2_html) : '';
                    
        $badge_html = ( $this->props['badge_enable']==='on' ) ?
        sprintf('<div class="df_ab_blurb_badge_wrapper"> 
                    <div class="df_ab_blurb_badge"> 
                        %2$s
                        %1$s
                    </div>
                </div>',  $badge_text_html , $this->df_render_badge_icon()) : '';
       
        $this->additional_css_styles($render_slug);

        // filter for images
		if (array_key_exists('image', $this->advanced_fields) && array_key_exists('css', $this->advanced_fields['image'])) {
			$this->add_classname($this->generate_css_filters(
				$render_slug,
				'child_',
				self::$data_utils->array_get($this->advanced_fields['image']['css'], 'main', '%%order_class%%')
			));
		 }
        
        $placement_class = ($this->props['image_placement'] !== '' && $this->props['blurb_icon_enable'] === 'off') ? 'placement_image_' . $this->props['image_placement'] : 'placement_icon_' . $this->props['image_placement'];
        //$placement_image_icon_class = ($this->props['image_placement'] !== '') ? 'image_icon_position_' . $this->props['image_placement'] : '';
        $icon_available_class = ($this->props['blurb_icon_enable'] === 'on' && $this->props['image_placement'] ==='flex_top') ? 'icon' : 'image';

        $image_html  = sprintf('<div class="df_ab_blurb_image %3$s %2$s">%1$s</div>', $this->df_render_image_icon(), $placement_class, $icon_available_class);
        // if ('off' !== $this->props['order_enable'] && ( 'flex_top' === $this->props['image_placement'] )) {
        //     $html_code = '<div class="df_ab_blurb_container"> %2$s %3$s %4$s %1$s %5$s %6$s</div>';
        // } else {
        //     $html_code = '<div class="df_ab_blurb_container"> %2$s<div class="df_ab_blurb_content_container"> %3$s %4$s %1$s %5$s %6$s</div></div>';
        // }
        if ( 'outside'!== $this->props['image_icon_container_position']  ) {
            $html_code = '<div class="df_ab_blurb_container"> <div class="df_ab_blurb_content_container">%2$s %3$s %4$s %1$s %5$s %6$s</div></div>';
        } else {
            $html_code = '<div class="df_ab_blurb_container"> %2$s<div class="df_ab_blurb_content_container"> %3$s %4$s %1$s %5$s %6$s</div></div>';
        }

        return sprintf($html_code, $content, $image_html, $title_html, $sub_title_html, $this->df_render_button() , $badge_html);
    }
}

new DIFL_AdvancedBlurb;