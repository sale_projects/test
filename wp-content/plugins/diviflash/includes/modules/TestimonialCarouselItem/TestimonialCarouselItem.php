<?php

class DIFL_TestimonialCarouselItem extends ET_Builder_Module {
    public $slug       = 'difl_testimonialcarouselitem';
    public $vb_support = 'on';
    public $type       = 'child';
    public $child_title_var          = 'author';
	public $child_title_fallback_var = 'admin_label';
    use DF_UTLS;

    protected $module_credits = array(
		'module_uri' => '',
		'author'     => 'DiviFlash',
		'author_uri' => '',
    );

    public function init() {
        $this->name = esc_html__( 'Carousel Item', 'divi_flash' );
        $this->main_css_element = "%%order_class%%";
    }

    public function get_settings_modal_toggles(){
        return array(
            'general'   => array(
                'toggles'      => array(
                    'content'               => esc_html__('Content', 'divi_flash'),
                    'images'                => esc_html__('Images', 'divi_flash'),
                    'settings'              => esc_html__('Settings', 'divi_flash'),
                    'image'                 => esc_html__('Author Image', 'divi_flash'),
                    'company_logo'          => esc_html__('Company Logo', 'divi_flash'),
                    'rating'                => esc_html__('Rating', 'divi_flash')
                ),
            ),
            'advanced'   => array(
                'toggles'   => array(
                    'font'                      => array (
                        'title'         => esc_html__('Font Style', 'divi_flash'),
                        'tabbed_subtoggles' => true,
                        'sub_toggles' => array(
                            'name'   => array(
                                'name' => 'Name'
                            ),
                            'title'     => array(
                                'name' => 'Title',
                            ),
                            'company'     => array(
                                'name' => 'Company',
                            ),
                            'body'     => array(
                                'name' => 'Body',
                            )
                        )
                    ),
                )
            ),
        );
    }

    public function get_advanced_fields_config() {
        $advanced_fields = array();

        $advanced_fields['fonts'] = array (
            'name'     => array(
                'label'         => esc_html__( 'Name', 'divi_flash' ),
                'toggle_slug'   => 'font',
                'sub_toggle'    => 'name',
                'tab_slug'		=> 'advanced',
                'hide_text_shadow'  => true,
                'hide_text_align'  => true,
                'line_height' => array (
                    'default' => '1em',
                ),
                'font_size' => array(
                    'default' => '18px',
                ),
                'css'      => array(
                    'main' => ".difl_testimonialcarousel {$this->main_css_element} .df_tc_author_info h4",
                    'hover' => ".difl_testimonialcarousel {$this->main_css_element}:hover .df_tc_author_info h4",
                    'important'	=> 'all'
                ),
            ),
            'title'     => array(
                'label'         => esc_html__( 'Job Title', 'divi_flash' ),
                'toggle_slug'   => 'font',
                'sub_toggle'    => 'title',
                'tab_slug'		=> 'advanced',
                'hide_text_shadow'  => true,
                'hide_text_align'  => true,
                'line_height' => array (
                    'default' => '1em',
                ),
                'font_size' => array(
                    'default' => '14px',
                ),
                'css'      => array(
                    'main' => ".difl_testimonialcarousel {$this->main_css_element} .tc_job_title",
                    'hover' => ".difl_testimonialcarousel {$this->main_css_element}:hover .tc_job_title",
                    'important'	=> 'all'
                ),
            ),
            'company'     => array(
                'label'         => esc_html__( 'Company', 'divi_flash' ),
                'toggle_slug'   => 'font',
                'sub_toggle'    => 'company',
                'tab_slug'		=> 'advanced',
                'hide_text_shadow'  => true,
                'hide_text_align'  => true,
                'line_height' => array (
                    'default' => '1em',
                ),
                'font_size' => array(
                    'default' => '14px',
                ),
                'css'      => array(
                    'main' => ".difl_testimonialcarousel {$this->main_css_element} .tc_company",
                    'hover' => ".difl_testimonialcarousel {$this->main_css_element}:hover .tc_company",
                    'important'	=> 'all'
                ),
            ),
            'body'     => array(
                'label'         => esc_html__( 'Body', 'divi_flash' ),
                'toggle_slug'   => 'font',
                'sub_toggle'    => 'body',
                'tab_slug'		=> 'advanced',
                'hide_text_shadow'  => true,
                'line_height' => array (
                    'default' => '1em',
                ),
                'font_size' => array(
                    'default' => '14px',
                ),
                'css'      => array(
                    'main' => ".difl_testimonialcarousel {$this->main_css_element} .df_tc_content",
                    'hover' => ".difl_testimonialcarousel {$this->main_css_element}:hover .df_tc_content",
                    'important'	=> 'all'
                ),
            ),
        );
        $advanced_fields['borders'] = array (
            'default'             => array (
                'css'               => array(
                    'main' => array(
                        'border_radii' => ".difl_testimonialcarousel {$this->main_css_element} > div:first-child",
                        'border_styles' => ".difl_testimonialcarousel {$this->main_css_element} > div:first-child",
                        'border_styles_hover' => ".difl_testimonialcarousel {$this->main_css_element} > div:first-child:hover",
                    )
                )
            )
        );

        $advanced_fields['background'] = array (
            'css' => array (
                'main'  => ".difl_testimonialcarousel {$this->main_css_element} .df_tci_container"
            )
        );
        $advanced_fields['text'] = false;
        $advanced_fields['filters'] = false;
        $advanced_fields['box_shadow'] = false;
        $advanced_fields['max_width'] = false;
    
        return $advanced_fields;
    }

    public function get_fields() {
        $general = array (
            'admin_label' => array (
                'label'           => esc_html__( 'Admin Label', 'divi_flash' ),
                'type'            => 'text',
                'option_category' => 'basic_option',
                'toggle_slug'     => 'admin_label',
                'default_on_front'=> 'Carousel Item'
            )
        );
        $content = array (
            'author' => array (
                'label'                 => esc_html__( 'Author', 'divi_flash' ),
				'type'                  => 'text',
                'toggle_slug'           => 'content'
            ),
            'job_title' => array (
                'label'                 => esc_html__( 'Job Title', 'divi_flash' ),
				'type'                  => 'text',
                'toggle_slug'           => 'content'
            ),
            'company' => array (
                'label'                 => esc_html__( 'Company', 'divi_flash' ),
				'type'                  => 'text',
                'toggle_slug'           => 'content'
            ),
            'company_url' => array (
                'label'                 => esc_html__( 'Company Url', 'divi_flash' ),
				'type'                  => 'text',
                'toggle_slug'           => 'content'
            ),
            'content'        => array (
                'label'                 => esc_html__('Body', 'divi_flash'),
                'type'                  => 'tiny_mce',
                'toggle_slug'           => 'content'
            )
        );
        $image = array (
            'image' => array (
                'label'                 => esc_html__( 'Author Image', 'divi_flash' ),
				'type'                  => 'upload',
				'upload_button_text'    => esc_attr__( 'Upload an image', 'divi_flash' ),
				'choose_text'           => esc_attr__( 'Choose an Image', 'divi_flash' ),
				'update_text'           => esc_attr__( 'Set As Image', 'divi_flash' ),
                'toggle_slug'           => 'images'
            ),
            'company_logo' => array (
                'label'                 => esc_html__( 'Company Logo', 'divi_flash' ),
				'type'                  => 'upload',
				'upload_button_text'    => esc_attr__( 'Upload an image', 'divi_flash' ),
				'choose_text'           => esc_attr__( 'Choose an Image', 'divi_flash' ),
				'update_text'           => esc_attr__( 'Set As Image', 'divi_flash' ),
                'toggle_slug'           => 'images'
            )
        );
        $rating = array (
            'rating'    => array (
                'label'             => esc_html__('Enable Rating', 'divi_flash'),
                'type'              => 'yes_no_button',
                'options'           => array(
					'off' => esc_html__( 'Off', 'divi_flash' ),
					'on'  => esc_html__( 'On', 'divi_flash' ),
                ),
                'default'           => 'off',
                'toggle_slug'       => 'settings'
            )
        );
        $quote = $this->df_add_icon_settings(array (
            'title'                 => 'Enable Quote Icon',
            'image_title'           => 'Image for Quote Icon',
            'key'                   => 'quote_icon',
            'toggle_slug'           => 'settings',
            'default_size'          => '48px',
            'icon_alignment'        => false,
            'image_styles'          => false,
            'circle_icon'           => false,
            'icon_color'            => false,
            'icon_size'             => false
        ));

        return array_merge(
            $general,
            $content,
            $image,
            $rating,
            $quote
        );
    }

    public function get_transition_fields_css_props() {
        $fields = parent::get_transition_fields_css_props();

        return $fields;
    }
    
    public function additional_css_styles($render_slug) {}

    public function render( $attrs, $content = null, $render_slug ) {
        $this->additional_css_styles($render_slug);
        array_push($this->classname, 'swiper-slide');

        $company_logo = $this->props['company_logo'] !== '' ? sprintf('
            <div class="df_tc_company_logo">
                <img class="tc_company_logo" src="%1$s" />
            </div>
        ', $this->props['company_logo']) : '';
        
        $content = $this->props['content'] !== '' ? sprintf('
            <div class="df_tc_content">
                %1$s
            </div>    
        ', $this->props['content']) : '';
        
        $author_image = $this->props['image'] !== '' ? sprintf('
            <div class="df_tc_author_image">
                <img class="tc_author_image" src="%1$s" />
            </div>  
        ', $this->props['image']) : '';

        $author_name = $this->props['author'] !== '' ? 
            sprintf('<h4>%1$s</h4>', $this->props['author']) : '';
        
        $job_title = $this->props['job_title'] !== '' ?
            sprintf('<span class="tc_job_title">%1$s</span>', $this->props['job_title']) : '';

        $company = '';
        if ($this->props['company'] !== '') {
            if ($this->props['company_url'] !== '') {
                $company = sprintf('
                    <a href="%2$s" target="_blank" class="tc_company">%1$s</a>', 
                    $this->props['company'], esc_attr($this->props['company_url'])) ;
            } else {
                $company = sprintf('
                    <span class="tc_company">%1$s</span>', 
                    $this->props['company']);
            }
        }
        
        $separator = $job_title !== '' && $company !== '' ? ', ' : '';

        $info = $author_name !== '' || $job_title !== '' || $company !== '' ?
            sprintf('<div class="df_tc_author_info">%1$s%2$s %3$s</div>',
            $author_name, $job_title, $company) : '';

        $info_box = $author_image !== '' || $info !== '' ? sprintf('
                <div class="df_tc_author_box">%1$s%2$s</div>
            ', $author_image, $info) : '';

        $rating = $this->props['rating'] === 'on' ? sprintf('
                <div class="df_tc_ratings">
                    <span></span><span></span>
                    <span></span><span></span>
                    <span></span>
                </div>
            ') : '';

        return sprintf('<div class="df_tci_container">
                <div class="df_tci_inner">
                    %5$s
                    %1$s
                    %2$s
                    %3$s
                    %4$s
                </div>
            </div>',
            $company_logo,
            $content,
            $info_box,
            $rating,
            $this->df_render_image('quote_icon')
        );
    }

    /**
     * Render image for the front image
     * 
     * @param String $key
     * @return HTML | markup for the image
     */
    public function df_render_image($key = '') {
        if ( isset($this->props[$key . '_use_icon']) && $this->props[$key . '_use_icon'] === 'on' ) {
            return sprintf('<div class="df_tc_quote_image"><span class="et-pb-icon df_tc_quote_icon">%1$s</span></div>', 
                isset($this->props[$key . '_font_icon']) && $this->props[$key . '_font_icon'] !== '' ? 
                    esc_attr(et_pb_process_font_icon( $this->props[$key . '_font_icon'] )) : '{'
            );
        } else if ( isset($this->props[$key . '_image']) && $this->props[$key . '_image'] !== ''){
            return sprintf('<div class="df_tc_quote_image">
                    <img class="tc_quote_image" src="%1$s" />
                </div>',
                $this->props[$key . '_image']
            );
        }
    }
}
new DIFL_TestimonialCarouselItem;