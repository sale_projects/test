<?php

class DGBM_DgBlogModule extends DiviExtension {

	/**
	 * The gettext domain for the extension's translations.
	 *
	 * @since 1.0.0
	 *
	 * @var string
	 */
	public $gettext_domain = 'dgbm-dg-blog-module';

	/**
	 * The extension's WP Plugin name.
	 *
	 * @since 1.0.0
	 *
	 * @var string
	 */
	public $name = 'dg-blog-module';

	/**
	 * The extension's version
	 *
	 * @since 1.0.4
	 *
	 * @var string
	 */
	public $version = '1.0.4';

	/**
	 * DGBM_DgBlogModule constructor.
	 *
	 * @param string $name
	 * @param array  $args
	 */
	public function __construct( $name = 'dg-blog-module', $args = array() ) {
		$this->plugin_dir     = plugin_dir_path( __FILE__ );
		$this->plugin_dir_url = plugin_dir_url( $this->plugin_dir );

		parent::__construct( $name, $args );

		add_action( 'wp_enqueue_scripts', array( $this, '_dgbm_enqueue_scripts' ) );
	}

	public function _dgbm_enqueue_scripts() {
		global $post;
		$data = [
			'is_single' => 'off',
			'single_id'	=> null
		];
		if (is_single()) {
			$data['is_single'] = 'on';
			$data['single_id'] = $post->ID;
		}
		wp_localize_script( "{$this->name}-builder-bundle", "DGBMBuilderData", $data );
	}
}

new DGBM_DgBlogModule;
