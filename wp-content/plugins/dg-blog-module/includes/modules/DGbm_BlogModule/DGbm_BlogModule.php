<?php
require_once ( DGBM_MAIN_DIR . '/functions/extends.php');

class DGbm_BlogModule extends ET_Builder_Module {
    public $slug = 'dgbm_blog_module';
    public $vb_support = 'on';

    protected $module_credits = array(
		'module_uri' => 'https://www.divigear.com/',
		'author'     => 'DiviGear',
		'author_uri' => 'https://www.divigear.com/',
    );
    
    public function init() {
		$this->name 				= esc_html__( 'DG Blog Module', 'et_builder' );
		$this->icon_path = plugin_dir_path( __FILE__ ). 'icon.svg';
		$this->main_css_element 	= '%%order_class%%';
    }

    public function get_settings_modal_toggles(){
        return array(
			'general'  => array(
					'toggles' => array(
							'main_content' 					=> esc_html__( 'Content', 'et_builder' ),
							'elements' 						=> esc_html__( 'Elements', 'et_builder' ),
							'carousel_settings'				=> esc_html__('Carousel Settings', 'et_builder'),
					),
			),
			'advanced'  =>  array(
					'toggles'   =>  array(
							'layouts'						=> esc_html__('Layout', 'et_builder'),
							'image_settings'				=> esc_html__('Image Settings', 'et_builder'),
							'title_text'					=> esc_html__('Title', 'et_builder'),
							'meta_text'						=> array(
								'title'		=> esc_html__('Meta', 'et_builder'),
								'tabbed_subtoggles' => true,
								'sub_toggles' => array(
									'font'     => array(
										'name' => 'Font',	
									),
									'alignment'     => array(
										'name' => 'Alignment',
									),
									'background'     => array(
										'name' => 'Background',
									)
								),
							),
							'author'						=> esc_html__( 'Author', 'et_builder' ),
							'date'							=> esc_html__( 'Date', 'et_builder' ),
							'category'					    => esc_html__( 'Category', 'et_builder' ),
							'comment'					    => esc_html__( 'Comment', 'et_builder' ),
							'single_meta'				=> array(
								'title'						=> esc_html__('Meta Style by type', 'et_builder'),
								'tabbed_subtoggles' 		=> true,
								'sub_toggles' 				=> array(
									'author'		=> array(
										'name'		=> 'Author',
									),
									'date'			=> array(
										'name'		=> 'Date',
									),
									'category'		=> array(
										'name'		=> 'Category',
									),
									'comment'		=> array(
										'name'		=> 'Comment',
									)
								)
							),
							'content_text'					=> esc_html__('Content', 'et_builder'),
							'button_style'					=> esc_html__('Read More', 'et_builder'),
							'pagination'					=> esc_html__('Pagination', 'et_builder'),
							'arrow_style'					=> esc_html__('Next & Prev Button', 'et_builder'),
							'dots_style'					=> esc_html__('Dots', 'et_builder'),
							// 'border'						=> array(
							// 	'title'				=> esc_html__('Border', 'et_builder'),
							// ),
							'custom_spacing'				=> array (
								'title'				=> esc_html__('Custom Spacing', 'et_builder'),
								'tabbed_subtoggles' => true,
								'sub_toggles' => array(
									'container'     => array(
										'name' => 'Container',
									),
									'content'     => array(
										'name' => 'Content',
									),
									'meta'     => array(
										'name' => 'Meta',
									),
									'button'     => array(
										'name' => 'Button',
									),
								),
							),
							'custom_boxshadow'					=> esc_html__('Box Shadow', 'et_builder'),
							'custom_border'						=> esc_html__('Border', 'et_builder'),
							'content_boxshadow'					=> esc_html__('Content Box Shadow', 'et_builder'),
							'article_filter'					=> esc_html__('Article Filter', 'et_builder'),
							'content_transform'					=> esc_html__('Content Transform', 'et_builder'),
					)
			),
			
		);
    }

    public function get_fields(){
		$_ex = "DGBM_Extends";
        $main_content = array(
			'use_current_loop' => array(
				'label'             => esc_html__( 'Posts For Current Page', 'et_builder' ),
				'type'              => 'yes_no_button',
				'option_category'   => 'configuration',
				'options'           => array(
					'on'  => esc_html__( 'Yes', 'et_builder' ),
					'off' => esc_html__( 'No', 'et_builder' ),
				),
				'description'       => esc_html__( 'Display posts for the current page. Useful on archive and index pages.', 'et_builder' ),
				'computed_affects'   => array(
					'__posts',
				),
				'toggle_slug'       => 'main_content',
				'default'           => 'off',
				'show_if'           => array(
					'function.isTBLayout' => 'on',
				),
				'show_if_not'		=> array (
					'related_posts'		=> 'on'
				)
			),
            'posts_number' => array(
				'default'           => '12',
				'label'             => esc_html__( 'Blog Posts Count', 'et_builder' ),
				'type'              => 'text',
				'option_category'   => 'configuration',
				'description'       => esc_html__( 'Define the number of blog posts to show.', 'et_builder' ),
				'computed_affects'  => array(
					'__posts',
				),
				'toggle_slug'       => 'main_content',
            ),
            'type' => array(
				'label'             => esc_html__( 'Posts by Type', 'et_builder' ),
				'type'              => 'select',
				'option_category'   => 'configuration',
				'options'           => array(
					'1' => esc_html__( 'default', 'et_builder' ),
					'1' => esc_html__( 'Recent Posts', 'et_builder' ),
					'2' => esc_html__( 'Popular Posts', 'et_builder' ),
					'3' => esc_html__( 'Posts by category', 'et_builder' ),
					'4' => esc_html__( 'Display Random Posts', 'et_builder' ),
				),
				'default'			=> '1',
				'computed_affects'  => array(
					'__posts',
				),
				'toggle_slug'       => 'main_content',
				'show_if_not'       => array(
					'related_posts' => 'on',
				)
            ),	
            'include_categories'   => array(
				'label'            => esc_html__( 'Include Categories', 'et_builder' ),
				'type'             => 'categories',
				'renderer_options' => array(
					'use_terms'    => true,
					'term_name'    => 'category',
				),
				'taxonomy_name'    => 'category',
				'toggle_slug'      => 'main_content',
				'computed_affects' => array(
					'__posts',
				),
				'show_if'         => array(
					'type' => '3',
				),
				'show_if_not'       => array(
					'related_posts' => 'on',
				)
            ),
            'orderby' => array(
				'label'             => esc_html__( 'Orderby', 'et_builder' ),
				'type'              => 'select',
				'option_category'   => 'configuration',
				'options'           => array(
					'1' => esc_html__( 'default', 'et_builder' ),
					'1' => esc_html__( 'Newest to oldest', 'et_builder' ),
					'2' => esc_html__( 'Oldest to newest', 'et_builder' ),
					'3' => esc_html__( 'Random', 'et_builder' ),
				),
				'default'			=> '1',
				'computed_affects'  => array(
					'__posts',
				),
				'toggle_slug'       => 'main_content',
				'show_if'         => array(
					'type' => '3',
				),
				'show_if_not'       => array(
					'related_posts' => 'on',
				)
			),	
			'offset_number' => array(
				'label'            => esc_html__( 'Offset Number', 'et_builder' ),
				'type'             => 'text',
				'option_category'  => 'configuration',
				'description'      => esc_html__( 'Choose how many posts you would like to offset by', 'et_builder' ),
				'toggle_slug'      => 'main_content',
				'computed_affects' => array(
					'__posts',
				),
				'default'          => 0,
				'show_if_not'       => array(
					'related_posts' => 'on',
				)
			),
			'related_posts' => array (
				'label'             => esc_html__( 'Related Post', 'et_builder' ),
				'type'              => 'yes_no_button',
				'option_category'   => 'configuration',
				'options'           => array(
					'on'  => esc_html__( 'Yes', 'et_builder' ),
					'off' => esc_html__( 'No', 'et_builder' ),
				),
				'description'       => esc_html__( 'Display related posts for single post. This will only work for single post.', 'et_builder' ),
				'toggle_slug'       => 'main_content',
				'default'           => 'off',
				'show_if_not'		=> array (
					'use_current_loop'		=> 'on'
				)
			)
		);
		
        $element = array(
            'show_thumbnail' => array(
				'label'             => esc_html__( 'Show Featured Image', 'et_builder' ),
				'type'              => 'yes_no_button',
				'option_category'   => 'configuration',
				'options'           => array(
					'on'  => esc_html__( 'Yes', 'et_builder' ),
					'off' => esc_html__( 'No', 'et_builder' ),
				),
				'description'       => esc_html__( 'This will turn thumbnails on and off.', 'et_builder' ),
				'computed_affects'  => array(
					'__posts',
				),
				'toggle_slug'       => 'elements',
				'default_on_front'  => 'on',
			),
			'show_excerpt' => array(
				'label'             => esc_html__( 'Show Excerpt', 'et_builder' ),
				'type'              => 'yes_no_button',
				'option_category'   => 'configuration',
				'options'           => array(
					'on'  => esc_html__( 'Yes', 'et_builder' ),
					'off' => esc_html__( 'No', 'et_builder' ),
				),
				'description'       => esc_html__( 'This will turn Excerpt on and off.', 'et_builder' ),
				'toggle_slug'       => 'elements',
				'computed_affects'  => array(
					'__posts',
				),
				'default_on_front'  => 'on',
			),
			'show_excerpt_length' => array(
				'label'             => esc_html__( 'Excerpt Length', 'et_builder' ),
				'type'              => 'number',
				'option_category'   => 'configuration',
				'toggle_slug'       => 'elements',
				'default'			=> '120',
				'show_if_not'         => array(
					'show_excerpt' => 'off',
				),
				'computed_affects'  => array(
					'__posts',
				),
			),
			'show_categories' => array(
				'label'             => esc_html__( 'Show Categories', 'et_builder' ),
				'type'              => 'yes_no_button',
				'option_category'   => 'configuration',
				'options'           => array(
					'on'  => esc_html__( 'Yes', 'et_builder' ),
					'off' => esc_html__( 'No', 'et_builder' ),
				),
				'description'        => esc_html__( 'Turn the category links on or off.', 'et_builder' ),
				'computed_affects'   => array(
					'__posts',
				),
				'toggle_slug'        => 'elements',
				'default_on_front'   => 'on',
			),
			'show_author' => array(
				'label'             => esc_html__( 'Show Author', 'et_builder' ),
				'type'              => 'yes_no_button',
				'option_category'   => 'configuration',
				'options'           => array(
					'on'  => esc_html__( 'Yes', 'et_builder' ),
					'off' => esc_html__( 'No', 'et_builder' ),
				),
				'description'        => esc_html__( 'Turn on or ff the author link.', 'et_builder' ),
				'computed_affects'   => array(
					'__posts',
				),
				'toggle_slug'        => 'elements',
				'default_on_front'   => 'on',
			),
			'show_date' => array(
				'label'             => esc_html__( 'Show Date', 'et_builder' ),
				'type'              => 'yes_no_button',
				'option_category'   => 'configuration',
				'options'           => array(
					'on'  => esc_html__( 'Yes', 'et_builder' ),
					'off' => esc_html__( 'No', 'et_builder' ),
				),
				'description'        => esc_html__( 'Turn the date on or off.', 'et_builder' ),
				'computed_affects'   => array(
					'__posts',
				),
				'toggle_slug'        => 'elements',
				'default_on_front'   => 'on',
			),
			'meta_date' => array(
				'label'             => esc_html__( 'Meta Date Format', 'et_builder' ),
				'type'              => 'text',
				'option_category'   => 'configuration',
				'description'       => esc_html__( 'If you would like to adjust the date format, input the appropriate PHP date format here.', 'et_builder' ),
				'toggle_slug'       => 'elements',
				'computed_affects'  => array(
					'__posts',
				),
				'show_if'			=> array (
					'show_date'		=> 'on'
				),
				'default'           => 'M j, Y',
			),
			'show_comments' => array(
				'label'             => esc_html__( 'Show Comment Count', 'et_builder' ),
				'type'              => 'yes_no_button',
				'option_category'   => 'configuration',
				'options'           => array(
					'on'  => esc_html__( 'Yes', 'et_builder' ),
					'off' => esc_html__( 'No', 'et_builder' ),
				),
				'description'        => esc_html__( 'Turn comment count on and off.', 'et_builder' ),
				'computed_affects'   => array(
					'__posts',
				),
				'toggle_slug'        => 'elements',
				'default_on_front'   => 'off',
			),
			'show_pagination' => array(
				'label'             => esc_html__( 'Show Pagination', 'et_builder' ),
				'type'              => 'yes_no_button',
				'option_category'   => 'configuration',
				'options'           => array(
					'on'  => esc_html__( 'Yes', 'et_builder' ),
					'off' => esc_html__( 'No', 'et_builder' ),
				),
				'description'        => esc_html__( 'Turn pagination on and off.', 'et_builder' ),
				'computed_affects'   => array(
					'__posts',
				),
				'toggle_slug'        => 'elements',
				'default_on_front'   => 'on',
			),
			'show_more' => array(
				'label'             => esc_html__( 'Read More Button', 'et_builder' ),
				'type'              => 'yes_no_button',
				'option_category'   => 'configuration',
				'options'           => array(
					'off' => esc_html__( 'No', 'et_builder' ),
					'on'  => esc_html__( 'Yes', 'et_builder' ),
				),
				'description'       => esc_html__( 'Here you can define whether to show "read more" link after the excerpts or not.', 'et_builder' ),
				'computed_affects'   => array(
					'__posts',
				),
				'toggle_slug'       => 'elements',
				'default_on_front'  => 'off',
			),
			'read_more_text' => array(
				'label'             => esc_html__( 'Read More Button Text', 'et_builder' ),
				'type'              => 'text',
				'option_category'   => 'configuration',
				'computed_affects'   => array(
					'__posts',
				),
				'toggle_slug'       => 'elements',
				'default_on_front'  => 'Read More',
				'show_if'         => array(
					'show_more' => 'on',
				),
			),
		);
		$layout = array(
			'layout' => array(
				'label'             => esc_html__( '&rArr; Layout', 'et_builder' ),
				'type'              => 'select',
				'options'           => array(
					'grid' 				=> esc_html__( 'Default', 'et_builder' ),
					'grid' 				=> esc_html__( 'Grid', 'et_builder' ),
					'masonry' 			=> esc_html__( 'Masonry', 'et_builder' ),
					'full-width' 		=> esc_html__( 'Full width', 'et_builder' ),
				),
				'default'			=> 'grid',
				'computed_affects'  => array(
					'__posts',
				),
				'toggle_slug'       => 'layouts',
				'tab_slug'			=> 'advanced'
			),
			'item_in_desktop' => array(
				'label'             => esc_html__( '&rArr; Columns in desktop', 'et_builder' ),
				'type'              => 'select',
				'options'           => array(
					'3' => esc_html__( 'default', 'et_builder' ),
					'2' => esc_html__( '2 columns', 'et_builder' ),
					'3' => esc_html__( '3 columns', 'et_builder' ),
					'4' => esc_html__( '4 columns', 'et_builder' ),
					'5' => esc_html__( '5 columns', 'et_builder' ),
				),
				'default'			=> '3',
				'tab_slug'        => 'advanced',
				'toggle_slug'     => 'layouts',
				'computed_affects'  => array(
					'__posts',
				),
				'show_if_not'         => array(
					'layout' => 'full-width',
				),
			),	
			'item_in_tablet' => array(
				'label'             => esc_html__( '&rArr; Columns in tablet', 'et_builder' ),
				'type'              => 'select',
				'options'           => array(
					'2' => esc_html__( 'default', 'et_builder' ),
					'1' => esc_html__( '1 column', 'et_builder' ),
					'2' => esc_html__( '2 columns', 'et_builder' ),
					'3' => esc_html__( '3 columns', 'et_builder' ),
				),
				'default'			=> '2',
				'tab_slug'        => 'advanced',
				'toggle_slug'     => 'layouts',
				'computed_affects'  => array(
					'__posts',
				),
				'show_if_not'         => array(
					'layout' => 'full-width',
				),
			),	
			'item_in_mobile' => array(
				'label'             => esc_html__( '&rArr; Columns in mobile', 'et_builder' ),
				'type'              => 'select',
				'options'           => array(
					'1' => esc_html__( 'default', 'et_builder' ),
					'1' => esc_html__( '1 column', 'et_builder' ),
					'2' => esc_html__( '2 columns', 'et_builder' ),
				),
				'default'			=> '1',
				'tab_slug'        => 'advanced',
				'toggle_slug'     => 'layouts',
				'computed_affects'  => array(
					'__posts',
				),
				'show_if_not'         => array(
					'layout' => 'full-width',
				),
			),	
			'space_between' => array(
				'label'           => esc_html__( '&rArr; Space Between', 'et_builder' ),
				'type'            => 'range',
				'mobile_options'    => true,
                'responsive'        => true,
                'default'           => '30px',
                'default_unit'      => 'px',
				'range_settings '   => array(
                    'min'       => '0',
                    'max'       => '100',
                    'step'      => '1',
				),
				'tab_slug'        => 'advanced',
				'toggle_slug'     => 'layouts',
				'show_if_not'         => array(
					'layout' => 'full-width',
				),
			),
			'layout_styles' => array(
				'label'             => esc_html__( '&rArr; Layout Styles', 'et_builder' ),
				'type'              => 'select',
				'options'           => array(
					'image-top' 				=> esc_html__( 'default', 'et_builder' ),
					'image-top' 				=> esc_html__( 'Image Top', 'et_builder' ),
					'image-left' 				=> esc_html__( 'Image Left', 'et_builder' ),
					'image-right' 				=> esc_html__( 'Image Right', 'et_builder' ),
					'image-left-right' 			=> esc_html__( 'Image Left Right Both', 'et_builder' ),
				),
				'default'			=> 'image-top',
				'computed_affects'  => array(
					'__posts',
				),
				'toggle_slug'       => 'layouts',
				'tab_slug'			=> 'advanced',
				'show_if'         => array(
					'layout' => 'full-width',
				),
			),
			'equal_height' => array(
				'label'             => esc_html__( '&rArr; Equal Height Article', 'et_builder' ),
				'type'              => 'yes_no_button',
				'option_category'   => 'configuration',
				'options'           => array(
					'on'  => esc_html__( 'Yes', 'et_builder' ),
					'off' => esc_html__( 'No', 'et_builder' ),
				),
				'toggle_slug'       => 'layouts',
				'tab_slug'			=> 'advanced',
				'default_on_front'  => 'off',
				'show_if'         => array(
					'layout' => 'grid',
				),
				'computed_affects'  => array(
					'__posts',
				),
			),
			'equal_height_column' => array(
				'label'             => esc_html__( '&rArr; Equal Height Column', 'et_builder' ),
				'type'              => 'yes_no_button',
				'option_category'   => 'configuration',
				'options'           => array(
					'on'  => esc_html__( 'Yes', 'et_builder' ),
					'off' => esc_html__( 'No', 'et_builder' ),
				),
				'toggle_slug'       => 'layouts',
				'tab_slug'			=> 'advanced',
				'default_on_front'  => 'off',
				'show_if'         => array(
					'layout' => 'full-width',
				),
				'show_if_not'         => array(
					'layout_styles' => 'image-top',
				),
				'computed_affects'  => array(
					'__posts',
				),
			),
			'vertical_align' => array(
				'label'             => esc_html__( '&rArr; Content Vertical Align', 'et_builder' ),
				'type'              => 'select',
				'options'           => array(
					'content-top' 				=> esc_html__( 'default', 'et_builder' ),
					'content-top' 				=> esc_html__( 'Top', 'et_builder' ),
					'content-middle' 			=> esc_html__( 'Middle', 'et_builder' ),
					'content-bottom' 			=> esc_html__( 'Bottom', 'et_builder' ),
				),
				'default'			=> 'content-top',
				'toggle_slug'       => 'layouts',
				'tab_slug'			=> 'advanced',
				'show_if'         => array(
					'layout' => 'full-width',
				),
				'show_if_not'         => array(
					'layout_styles' => 'image-top',
					'equal_height'	=> 'on'
				),
			),
			'image_as_background' => array(
				'label'             => esc_html__( '&rArr; Set Featured Image as Background', 'et_builder' ),
				'type'              => 'yes_no_button',
				'option_category'   => 'configuration',
				'options'           => array(
					'on'  => esc_html__( 'Yes', 'et_builder' ),
					'off' => esc_html__( 'No', 'et_builder' ),
				),
				'toggle_slug'       => 'layouts',
				'tab_slug'			=> 'advanced',
				'default_on_front'  => 'off',
				'show_if_not'         => array(
					'layout' => 'full-width',
				),
				'computed_affects'  => array(
					'__posts',
				),
			),
			'image_width' => array(
				'label'           => esc_html__( '&rArr; Image Width by %', 'et_builder' ),
				'type'            => 'range',
				'mobile_options'    => true,
                'responsive'        => true,
                'default'           => '50%',
                'default_unit'      => '%',
				'range_settings '   => array(
                    'min'       => '0',
                    'max'       => '100',
                    'step'      => '1',
				),
				'tab_slug'        => 'advanced',
				'toggle_slug'     => 'layouts',
				'show_if'         => array(
					'layout' => 'full-width',
				),
				'show_if_not'         => array(
					'layout_styles' => 'image-top',
				),
			),
			'side_overlap_setting' => array(
				'label'             => esc_html__( '&rArr; Side Overlap', 'et_builder' ),
				'type'              => 'yes_no_button',
				'option_category'   => 'configuration',
				'options'           => array(
					'on'  => esc_html__( 'Yes', 'et_builder' ),
					'off' => esc_html__( 'No', 'et_builder' ),
				),
				'toggle_slug'       => 'layouts',
				'tab_slug'			=> 'advanced',
				'default_on_front'  => 'off',
				'show_if'         => array(
					'layout' => 'full-width',
				),
				'show_if_not'         => array(
					'layout_styles' => 'image-top',
				),
			),
			'side_overlap' => array(
				'label'           => esc_html__( '&rArr; Side Overlap Value', 'et_builder' ),
				'type'            => 'range',
				'mobile_options'    => true,
                'responsive'        => true,
                'default'           => '50px',
                'default_unit'      => 'px',
				'range_settings '   => array(
                    'min'       => '0',
                    'max'       => '100',
                    'step'      => '1',
				),
				'tab_slug'        => 'advanced',
				'toggle_slug'     => 'layouts',
				'show_if'         => array(
					'layout' => 'full-width',
					'side_overlap_setting' => 'on',
				),
				'show_if_not'         => array(
					'layout_styles' => 'image-top',
				),
			),
		);
		$image = array(
			'image_size'	=> array(
				'label'				=> 	esc_html__('&rArr; Select Image Resolution', 'et_builder'),
				'type'				=>	'select',
				'options'           => array(
					'mid' 	=> esc_html__( 'default', 'et_builder' ),
					'large' => esc_html__( 'Large ( 1080 x 675 )', 'et_builder' ),
					'mid' 	=> esc_html__( 'Medium ( 400 x 250 )', 'et_builder' ),
					'default_image' 	=> esc_html__( 'Default Image', 'et_builder' ),
				),
				'default'  			=> '3',
				'tab_slug'			=> 'advanced',
				'toggle_slug'		=> 'image_settings',
				'computed_affects'   => array(
					'__posts',
				),
			),
			'image_overlay' => array(
				'label'             => esc_html__( '&rArr; Image Overlay on Hover', 'et_builder' ),
				'type'              => 'yes_no_button',
				'option_category'   => 'configuration',
				'options'           => array(
					'on'  => esc_html__( 'Yes', 'et_builder' ),
					'off' => esc_html__( 'No', 'et_builder' ),
				),
				'toggle_slug'       => 'image_settings',
				'tab_slug'			=> 'advanced',
				'default_on_front'  => 'off',
			),
			'overlay_color' => array(
				'label'           => esc_html__( '&rArr; Overlay Color', 'et_builder' ),
				'type'            => 'color-alpha',
				'tab_slug'        => 'advanced',
				'toggle_slug'	  => 'image_settings',
				'default'		  => 'rgba(51, 51, 51, 0.59)',
				'show_if'         => array(
					'image_overlay' => 'on',
				),
			),
			'use_overlay_icon'	=> array(
				'label'				=> 	esc_html__('&rArr; Use Overlay icon', 'et_builder'),
				'type'				=>	'yes_no_button',
				'options'         => array(
					'off' => esc_html__( 'No', 'et_builder' ),
					'on'  => esc_html__( 'Yes', 'et_builder' ),
				),
				'tab_slug'        	=> 'advanced',
				'toggle_slug'		=> 'image_settings',
				'default'			=>  'off',
				'show_if'			=> array(
					'image_overlay' => 'on'
				)
			),
			'select_overlay_icon' => array(
				'label'               => esc_html__( 'Select Overlay Icon', 'et_builder' ),
				'type'                => 'et_font_icon_select',
				'renderer'            => 'select_icon',
				'renderer_with_field' => true,
				'tab_slug'        	=> 'advanced',
				'toggle_slug'     	=> 'image_settings',
				'show_if'         	=> array(
					'use_overlay_icon' => 'on',
					'image_overlay' => 'on'
				),
			),
			'overlay_icon_color' => array(
				'label'           => esc_html__( '&rArr; Overlay Icon Color', 'et_builder' ),
				'type'            => 'color-alpha',
				'tab_slug'        => 'advanced',
				'toggle_slug'	  => 'image_settings',
				'default'		  => '#ffffff',
				'show_if'         => array(
					'use_overlay_icon' => 'on',
					'image_overlay' => 'on'
				),
			),
			'image_scale_on_hover' => array(
				'label'             => esc_html__( '&rArr; Image Scale On Hover', 'et_builder' ),
				'type'              => 'yes_no_button',
				'option_category'   => 'configuration',
				'options'           => array(
					'on'  => esc_html__( 'Yes', 'et_builder' ),
					'off' => esc_html__( 'No', 'et_builder' ),
				),
				'toggle_slug'       => 'image_settings',
				'tab_slug'			=> 'advanced',
				'default_on_front'  => 'off',
			),
		);
		$meta = array(
			// meta
			'use_meta_icon'	=> array(
				'label'				=> 	esc_html__('&rArr; Use Meta Icon', 'et_builder'),
				'type'				=>	'yes_no_button',
				'options'         => array(
					'off' => esc_html__( 'No', 'et_builder' ),
					'on'  => esc_html__( 'Yes', 'et_builder' ),
				),
				'tab_slug'        	=> 'advanced',
				'toggle_slug'		=> 'meta_text',
				'sub_toggle'   		=> 'font',
				'default'			=>  'off',
				'computed_affects'   => array(
					'__posts',
				),
			),
			'default_position_alignment'	=> array(
				'label'				=> 	esc_html__('&rArr; Default Position Alignment', 'et_builder'),
				'type'				=>	'text_align',
				'options'         	=>  et_builder_get_text_orientation_options(array('justified')),
				'tab_slug'        	=> 'advanced',
				'toggle_slug'		=> 'meta_text',
				'sub_toggle'   		=> 'alignment',
				'default'			=> 'left',
			),
			'top_position_alignment'	=> array(
				'label'				=> 	esc_html__('&rArr; Top Position Alignment', 'et_builder'),
				'type'				=>	'text_align',
				'options'         	=>  et_builder_get_text_orientation_options(array('justified')),
				'tab_slug'        	=> 'advanced',
				'toggle_slug'		=> 'meta_text',
				'sub_toggle'   		=> 'alignment',
				'default'			=> 'left',
			),
			'bottom_position_alignment'	=> array(
				'label'				=> 	esc_html__('&rArr; Bottom Position Alignment', 'et_builder'),
				'type'				=>	'text_align',
				'options'         	=>  et_builder_get_text_orientation_options(array('justified')),
				'tab_slug'        	=> 'advanced',
				'toggle_slug'		=> 'meta_text',
				'sub_toggle'   		=> 'alignment',
				'default'			=> 'left',
			),
			'default_position_bg' => array(
				'label'           	=> esc_html__( 'Default Position Background', 'et_builder' ),
				'type'            	=> 'color-alpha',
				'tab_slug'        	=> 'advanced',
				'toggle_slug'     	=> 'meta_text',
				'sub_toggle'   		=> 'background',
				'default'		  	=> 'rgba(255,255,255,0)',
				'hover'			  	=> 'tabs'
			),
			'top_position_bg' => array(
				'label'           	=> esc_html__( 'Top Position Background', 'et_builder' ),
				'type'            	=> 'color-alpha',
				'tab_slug'        	=> 'advanced',
				'toggle_slug'     	=> 'meta_text',
				'sub_toggle'   		=> 'background',
				'default'		  	=> 'rgba(255,255,255,0)',
				'hover'			  	=> 'tabs'
			),
			'bottom_position_bg' => array(
				'label'           	=> esc_html__( 'Bottom Position Background', 'et_builder' ),
				'type'            	=> 'color-alpha',
				'tab_slug'        	=> 'advanced',
				'toggle_slug'     	=> 'meta_text',
				'sub_toggle'   		=> 'background',
				'default'		  	=> 'rgba(255,255,255,0)',
				'hover'			  	=> 'tabs'
			),
		);
		$read_more_btn = array(
			'button_alignment'	=> array(
				'label'				=> 	esc_html__('Alignment', 'et_builder'),
				'type'				=>	'text_align',
				'options'         	=>  et_builder_get_text_orientation_options( array( 'justified' ) ),
				'tab_slug'        	=>  'advanced',
				'toggle_slug'		=>	'button_style',
				'default'			=> 'left',
				'default_on_front'	=> 'left',
			),
			'button_bg_color' => array(
				'label'           	=> esc_html__( 'Background', 'et_builder' ),
				'type'            	=> 'color-alpha',
				'tab_slug'        	=> 'advanced',
				'toggle_slug'     	=> 'button_style',
				'default'		  	=> 'rgba(255,255,255,0)',
				'hover'			  	=> 'tabs'
			),
			'button_fullwidth'	=> array(
				'label'				=> 	esc_html__('Fullwidth Button', 'et_builder'),
				'type'				=>	'yes_no_button',
				'options'         => array(
					'off' => esc_html__( 'No', 'et_builder' ),
					'on'  => esc_html__( 'Yes', 'et_builder' ),
				),
				'tab_slug'        	=> 'advanced',
				'toggle_slug'		=>	'button_style',
				'default'			=> 'off',
			),
			'button_at_bottom'	=> array(
				'label'				=> 	esc_html__('Button at the Bottom', 'et_builder'),
				'type'				=>	'yes_no_button',
				'options'         => array(
					'off' => esc_html__( 'No', 'et_builder' ),
					'on'  => esc_html__( 'Yes', 'et_builder' ),
				),
				'tab_slug'        	=> 'advanced',
				'toggle_slug'		=>	'button_style',
				'default'			=> 'off',
				'computed_affects'  => array(
					'__posts',
				),
			),
			'use_button_icon'	=> array(
				'label'				=> 	esc_html__('Use Icon', 'et_builder'),
				'type'				=>	'yes_no_button',
				'options'         => array(
					'off' => esc_html__( 'No', 'et_builder' ),
					'on'  => esc_html__( 'Yes', 'et_builder' ),
				),
				'tab_slug'        	=> 'advanced',
				'toggle_slug'		=>	'button_style',
				'default'			=> 'off',
				'computed_affects'  => array(
					'__posts',
				),
			),
			'button_icon' => array(
				'label'               => esc_html__( 'Select Button Icon', 'et_builder' ),
				'type'                => 'et_font_icon_select',
				'renderer'            => 'select_icon',
				'renderer_with_field' => true,
				'tab_slug'        	=> 'advanced',
				'toggle_slug'     	=> 'button_style',
				'show_if'         	=> array(
					'use_button_icon' => 'on',
				),
			),
		);
		$pagination_style = array(
			// Pagination button background
			'pagination_background' => array(
				'label'           => esc_html__( '&rArr; Background Color', 'et_builder' ),
				'type'            => 'color-alpha',
				'tab_slug'        => 'advanced',
				'toggle_slug'	  => 'pagination',
				'default'		  => 'rgba(255,255,255, 0)',
				'hover'			  => 'tabs',
			),
		);
		$single_meta_style = array(
			// author
			'author_location' => array(
				'label'             => esc_html__( '&rArr; Author Position', 'et_builder' ),
				'type'              => 'select',
				'options'           => array(
					'default' 					=> esc_html__( 'Default', 'et_builder' ),
					'default' 					=> esc_html__( 'Default', 'et_builder' ),
					'top' 						=> esc_html__( 'Top', 'et_builder' ),
					'bottom' 					=> esc_html__( 'Bottom', 'et_builder' ),
					'over-image' 				=> esc_html__( 'Over Image', 'et_builder' ),
				),
				'default'			=> 'default',
				'computed_affects'  => array(
					'__posts',
				),
				'toggle_slug'       => 'author',
				'tab_slug'			=> 'advanced',
				'priority' 			=> 50
			),
			'author_background_color' => array(
				'label'           => esc_html__( '&rArr; Author Background Color', 'et_builder' ),
				'type'            => 'color-alpha',
				'toggle_slug'       => 'author',
				'tab_slug'			=> 'advanced',
				'default'		  => 'rgba(255, 255, 255, 0)',
				'hover'			  => 'tabs',
				'priority' 		  => 30
			),
			
			// date
			'date_location' => array(
				'label'             => esc_html__( '&rArr; Date Position', 'et_builder' ),
				'type'              => 'select',
				'options'           => array(
					'default' 					=> esc_html__( 'Default', 'et_builder' ),
					'default' 					=> esc_html__( 'Default', 'et_builder' ),
					'top' 						=> esc_html__( 'Top', 'et_builder' ),
					'bottom' 					=> esc_html__( 'Bottom', 'et_builder' ),
					'over-image' 				=> esc_html__( 'Over Image', 'et_builder' ),
				),
				'default'			=> 'default',
				'computed_affects'  => array(
					'__posts',
				),
				'toggle_slug'       => 'date',
				'tab_slug'			=> 'advanced',
			),
			'date_background_color' => array(
				'label'           => esc_html__( '&rArr; Date Background Color', 'et_builder' ),
				'type'            => 'color-alpha',
				'toggle_slug'       => 'date',
				'tab_slug'			=> 'advanced',
				'default'		  => 'rgba(255, 255, 255, 0)',
				'hover'			  => 'tabs',
			),
			// category
			'category_location' => array(
				'label'             => esc_html__( '&rArr; Category Position', 'et_builder' ),
				'type'              => 'select',
				'options'           => array(
					'default' 					=> esc_html__( 'Default', 'et_builder' ),
					'default' 					=> esc_html__( 'Default', 'et_builder' ),
					'top' 						=> esc_html__( 'Top', 'et_builder' ),
					'bottom' 					=> esc_html__( 'Bottom', 'et_builder' ),
					'over-image' 				=> esc_html__( 'Over Image', 'et_builder' ),
				),
				'default'			=> 'default',
				'computed_affects'  => array(
					'__posts',
				),
				'toggle_slug'       => 'category',
				'tab_slug'			=> 'advanced',
			),
			'category_background_color' => array(
				'label'           => esc_html__( '&rArr; Category Background Color', 'et_builder' ),
				'type'            => 'color-alpha',
				'toggle_slug'       => 'category',
				'tab_slug'			=> 'advanced',
				'default'		  => 'rgba(255, 255, 255, 0)',
				'hover'			  => 'tabs',
			),
			// comment
			'comment_location' => array(
				'label'             => esc_html__( '&rArr; Comment Position', 'et_builder' ),
				'type'              => 'select',
				'options'           => array(
					'default' 					=> esc_html__( 'Default', 'et_builder' ),
					'default' 					=> esc_html__( 'Default', 'et_builder' ),
					'top' 						=> esc_html__( 'Top', 'et_builder' ),
					'bottom' 					=> esc_html__( 'Bottom', 'et_builder' ),
					'over-image' 				=> esc_html__( 'Over Image', 'et_builder' ),
				),
				'default'			=> 'default',
				'computed_affects'  => array(
					'__posts',
				),
				'toggle_slug'       => 'comment',
				'tab_slug'			=> 'advanced',
			),
			'comment_background_color' => array(
				'label'           => esc_html__( '&rArr; Comment Background Color', 'et_builder' ),
				'type'            => 'color-alpha',
				'toggle_slug'       => 'comment',
				'tab_slug'			=> 'advanced',
				'default'		  => 'rgba(255, 255, 255, 0)',
				'hover'			  => 'tabs',
			),
		);
		
		$container_margin = $_ex::add_margin_padding_field(
			'container_margin',
			'Container Margin',
			'custom_spacing',
			'container'
		);
		$container_padding = $_ex::add_margin_padding_field(
			'container_padding',
			'Container Padding',
			'custom_spacing',
			'container'
		);
		$article_margin = $_ex::add_margin_padding_field(
			'article_margin',
			'Article Margin',
			'custom_spacing',
			'container'
		);
		$article_padding = $_ex::add_margin_padding_field(
			'article_padding',
			'Article Padding',
			'custom_spacing',
			'container'
		);
		$image_margin = $_ex::add_margin_padding_field(
			'image_margin',
			'Image Container Margin',
			'custom_spacing',
			'content'
		);
		$content_margin = $_ex::add_margin_padding_field(
			'content_margin',
			'Content Margin',
			'custom_spacing',
			'content'
		);
		$content_padding = $_ex::add_margin_padding_field(
			'content_padding',
			'Content Padding',
			'custom_spacing',
			'content'
		);
		$title_margin = $_ex::add_margin_padding_field(
			'title_margin',
			'Title margin',
			'custom_spacing',
			'content'
		);

		// meta margin-padding default
		$meta_margin_default = $_ex::add_margin_padding_field(
			'meta_default_margin',
			'Meta Default Position margin',
			'custom_spacing',
			'meta'
		);
		$meta_padding_default = $_ex::add_margin_padding_field(
			'meta_default_padding',
			'Meta Default Position Padding',
			'custom_spacing',
			'meta'
		);
		// meta margin-padding top
		$meta_margin_top = $_ex::add_margin_padding_field(
			'meta_top_margin',
			'Meta Top Position margin',
			'custom_spacing',
			'meta'
		);
		$meta_padding_top = $_ex::add_margin_padding_field(
			'meta_top_padding',
			'Meta Top Position Padding',
			'custom_spacing',
			'meta'
		);
		// meta margin-padding top
		$meta_margin_bottom = $_ex::add_margin_padding_field(
			'meta_bottom_margin',
			'Meta Bottom Position margin',
			'custom_spacing',
			'meta'
		);
		$meta_padding_bottom = $_ex::add_margin_padding_field(
			'meta_bottom_padding',
			'Meta Bottom Position Padding',
			'custom_spacing',
			'meta'
		);

		$text_margin = $_ex::add_margin_padding_field(
			'text_margin',
			'Text margin',
			'custom_spacing',
			'content'
		);
		$button_wrapper_margin = $_ex::add_margin_padding_field(
			'button_wrapper_margin',
			'Read More Button Wrapper margin',
			'custom_spacing',
			'button'
		);
		$button_margin = $_ex::add_margin_padding_field(
			'button_margin',
			'Read More Button margin',
			'custom_spacing',
			'button'
		);
		$button_padding = $_ex::add_margin_padding_field(
			'button_padding',
			'Read More Button Padding',
			'custom_spacing',
			'button'
		);
		$pagination_margin = $_ex::add_margin_padding_field(
			'pagination_margin',
			'Pagination Button margin',
			'custom_spacing',
			'button'
		);
		$pagination_padding = $_ex::add_margin_padding_field(
			'pagination_padding',
			'Pagination Button Padding',
			'custom_spacing',
			'button'
		);
		// aythor margin-pading
		$author_margin = $_ex::add_margin_padding_field(
			'author_margin',
			'Author Margin',
			'custom_spacing',
			'meta'
		);
		$author_padding = $_ex::add_margin_padding_field(
			'author_padding',
			'Author Padding',
			'custom_spacing',
			'meta'
		);
		// date margin-padding
		$date_margin = $_ex::add_margin_padding_field(
			'date_margin',
			'Date Margin',
			'custom_spacing',
			'meta'
		);
		$date_padding = $_ex::add_margin_padding_field(
			'date_padding',
			'Date Padding',
			'custom_spacing',
			'meta'
		);
		// category margin-padding
		$category_margin = $_ex::add_margin_padding_field(
			'category_margin',
			'Category Margin',
			'custom_spacing',
			'meta'
		);
		$category_padding = $_ex::add_margin_padding_field(
			'category_padding',
			'Category Padding',
			'custom_spacing',
			'meta'
		);
		// comment margin-padding
		$comment_margin = $_ex::add_margin_padding_field(
			'comment_margin',
			'Comment Margin',
			'custom_spacing',
			'meta'
		);
		$comment_padding = $_ex::add_margin_padding_field(
			'comment_padding',
			'Comment Padding',
			'custom_spacing',
			'meta'
		);
        return array_merge(
			$main_content,
			$element,
			$layout,
			$image,
			$meta,
			$read_more_btn,
			$pagination_style,
			$container_margin,
			$container_padding,
			$article_margin,
			$article_padding,
			$image_margin,
			$content_margin,
			$content_padding,
			$title_margin,
			$meta_margin_default,
			$meta_padding_default,
			$meta_margin_top,
			$meta_padding_top,
			$meta_margin_bottom,
			$meta_padding_bottom,
			$text_margin,
			$button_wrapper_margin,
			$button_margin,
			$button_padding,
			$pagination_margin,
			$pagination_padding,
			$single_meta_style,
			$author_margin,
			$author_padding,
			$date_margin,
			$date_padding,
			$category_margin,
			$category_padding,
			$comment_margin,
			$comment_padding
        );
	}
	
	/**
	 * Additional Css Styles
	 */
	public function additional_css_styles($render_slug){
		$_ex = "DGBM_Extends";
		$alignment = array(
			'left' => 'flex-start',
            'center' => 'center',
            'right' => 'flex-end',
            'justify' => 'space-between'
		);
		
		// meta alignment
		if ( '' !== $this->props['default_position_alignment'] ) {
			ET_Builder_Element::set_style($render_slug, array(
				'selector' => '%%order_class%%.dgbm_blog_module .dg-blog-inner-wrapper .dgbm_post_item .content-wrapper .post-meta.post-meta-position-default',
				'declaration' => sprintf('justify-content: %1$s;', $alignment[$this->props['default_position_alignment']]),
			));
		}
		if ( '' !== $this->props['top_position_alignment'] ) {
			ET_Builder_Element::set_style($render_slug, array(
				'selector' => '%%order_class%%.dgbm_blog_module .dg-blog-inner-wrapper .dgbm_post_item .content-wrapper .post-meta.post-meta-position-top',
				'declaration' => sprintf('justify-content: %1$s;', $alignment[$this->props['top_position_alignment']]),
			));
		}
		if ( '' !== $this->props['bottom_position_alignment'] ) {
			ET_Builder_Element::set_style($render_slug, array(
				'selector' => '%%order_class%%.dgbm_blog_module .dg-blog-inner-wrapper .dgbm_post_item .content-wrapper .post-meta.post-meta-position-bottom',
				'declaration' => sprintf('justify-content: %1$s;', $alignment[$this->props['bottom_position_alignment']]),
			));
		}
		// side opverlap 
		if ($this->props['layout'] === 'full-width' && $this->props['layout_styles'] === 'image-left' && $this->props['side_overlap_setting'] === 'on') {
			$_ex::apply_single_value(
				$this,
				$render_slug,
				'side_overlap',
				'margin-left',
				'%%order_class%%.dgbm_blog_module .dg-blog-module .dg-blog-inner-wrapper .dgbm_post_item .content-wrapper',
				'px',
				false,
				false
			);
			ET_Builder_Element::set_style($render_slug, array(
				'selector' => '%%order_class%%.dgbm_blog_module .dg-blog-module .dg-blog-inner-wrapper .dgbm_post_item .column-content',
				'declaration' => 'z-index: 10;',
			));
		}
		if ($this->props['layout'] === 'full-width' && $this->props['layout_styles'] === 'image-right' && $this->props['side_overlap_setting'] === 'on') {
			$_ex::apply_single_value(
				$this,
				$render_slug,
				'side_overlap',
				'margin-right',
				'%%order_class%%.dgbm_blog_module .dg-blog-module .dg-blog-inner-wrapper .dgbm_post_item .content-wrapper',
				'px',
				false,
				false
			);
			ET_Builder_Element::set_style($render_slug, array(
				'selector' => '%%order_class%%.dgbm_blog_module .dg-blog-module .dg-blog-inner-wrapper .dgbm_post_item .column-content',
				'declaration' => 'z-index: 10;',
			));
		}
		if ($this->props['layout'] === 'full-width' && $this->props['layout_styles'] === 'image-left-right' && $this->props['side_overlap_setting'] === 'on') {
			$_ex::apply_single_value(
				$this,
				$render_slug,
				'side_overlap',
				'margin-left',
				'%%order_class%%.dgbm_blog_module .dg-blog-module .dg-blog-inner-wrapper .dgbm_post_item:nth-child(odd) .content-wrapper',
				'px',
				false,
				false
			);
			$_ex::apply_single_value(
				$this,
				$render_slug,
				'side_overlap',
				'margin-right',
				'%%order_class%%.dgbm_blog_module .dg-blog-module .dg-blog-inner-wrapper .dgbm_post_item:nth-child(even) .content-wrapper',
				'px',
				false,
				false
			);
			ET_Builder_Element::set_style($render_slug, array(
				'selector' => '%%order_class%%.dgbm_blog_module .dg-blog-module .dg-blog-inner-wrapper .dgbm_post_item .column-content',
				'declaration' => 'z-index: 10;',
			));
		}
		// item image and content width for full width design
		if ( $this->props['layout'] === 'full-width' && $this->props['layout_styles'] !== 'image-top' ) {
			$_ex::apply_single_value(
				$this,
				$render_slug,
				'image_width',
				'width',
				'%%order_class%%.dgbm_blog_module .dg-blog-inner-wrapper .dgbm_post_item .column-image'
			);
			$_ex::apply_single_value(
				$this,
				$render_slug,
				'image_width',
				'width',
				'%%order_class%%.dgbm_blog_module .dg-blog-inner-wrapper .dgbm_post_item .column-content',
				'%',
				true
			);
		}
		// item width for different divices with masonry
		$_ex::item_width_masonry(
			$this,
			$render_slug,
			$this->props['item_in_desktop'],
			$this->props['item_in_tablet'],
			$this->props['item_in_mobile'],
			'%%order_class%% .column',
			'%%order_class%% .dg-blog-masonry .dgbm_post_item',
			'space_between',
			$this->props['layout']
			// 'masonry'
		);
		
		// item width for different divices with masonry
		$_ex::grid_layout(
			$this,
			$render_slug,
			$this->props['item_in_desktop'],
			$this->props['item_in_tablet'],
			$this->props['item_in_mobile'],
			"{$this->main_css_element} .dgbm_post_item",
			'space_between',
			$this->props['layout']
		);
		// Apply container margin
		$_ex::apply_margin_padding(
			$this,
			$render_slug, 
			'container_margin', 
			'margin', 
			'%%order_class%%',
			'%%order_class%%:hover'
		);
		// Apply container padding
		$_ex::apply_margin_padding(
			$this,
			$render_slug, 
			'container_padding', 
			'padding', 
			'%%order_class%%',
			'%%order_class%%:hover'
		);
		// Apply article margin
		$_ex::apply_margin_padding(
			$this,
			$render_slug, 
			'article_margin', 
			'margin', 
			'%%order_class%% .dgbm_post_item',
			'%%order_class%% .dgbm_post_item:hover'
		);
		// Apply article padding
		$_ex::apply_margin_padding(
			$this,
			$render_slug, 
			'article_padding', 
			'padding', 
			'%%order_class%% .dgbm_post_item',
			'%%order_class%% .dgbm_post_item:hover'
		);
		// Apply image margin
		$_ex::apply_margin_padding(
			$this,
			$render_slug, 
			'image_margin', 
			'margin', 
			'%%order_class%% .dgbm_post_item .dg-post-thumb', 
			'%%order_class%% .dgbm_post_item:hover .dg-post-thumb' 
		);
		// Apply content margin
		$_ex::apply_margin_padding(
			$this,
			$render_slug, 
			'content_margin', 
			'margin', 
			'%%order_class%% .dgbm_post_item .content-wrapper', 
			'%%order_class%% .dgbm_post_item:hover .content-wrapper' 
		);
		// Apply content padding
		$_ex::apply_margin_padding(
			$this,
			$render_slug, 
			'content_padding', 
			'padding', 
			'%%order_class%% .dgbm_post_item .content-wrapper',
			'%%order_class%% .dgbm_post_item:hover .content-wrapper'
		);
		// Apply title margin
		$_ex::apply_margin_padding(
			$this,
			$render_slug, 
			'title_margin', 
			'margin', 
			'%%order_class%% .dgbm_post_item .content-wrapper .dg_bm_title',
			'%%order_class%% .dgbm_post_item:hover .content-wrapper .dg_bm_title'
		);
		// Apply meta default margin-padding
		$_ex::apply_margin_padding(
			$this,
			$render_slug, 
			'meta_default_margin', 
			'margin', 
			'%%order_class%% .dgbm_post_item .content-wrapper .post-meta.post-meta-position-default',
			'%%order_class%% .dgbm_post_item:hover .content-wrapper .post-meta.post-meta-position-default'
		);
		$_ex::apply_margin_padding(
			$this,
			$render_slug, 
			'meta_default_padding', 
			'padding', 
			'%%order_class%% .dgbm_post_item .content-wrapper .post-meta.post-meta-position-default',
			'%%order_class%% .dgbm_post_item:hover .content-wrapper .post-meta.post-meta-position-default'
		);
		// Apply meta top margin-padding
		$_ex::apply_margin_padding(
			$this,
			$render_slug, 
			'meta_top_margin', 
			'margin', 
			'%%order_class%% .dgbm_post_item .content-wrapper .post-meta.post-meta-position-top',
			'%%order_class%% .dgbm_post_item:hover .content-wrapper .post-meta.post-meta-position-top'
		);
		$_ex::apply_margin_padding(
			$this,
			$render_slug, 
			'meta_top_padding', 
			'padding', 
			'%%order_class%% .dgbm_post_item .content-wrapper .post-meta.post-meta-position-top',
			'%%order_class%% .dgbm_post_item:hover .content-wrapper .post-meta.post-meta-position-top'
		);
		// Apply meta bottom margin-padding
		$_ex::apply_margin_padding(
			$this,
			$render_slug, 
			'meta_bottom_margin', 
			'margin', 
			'%%order_class%% .dgbm_post_item .content-wrapper .post-meta.post-meta-position-bottom',
			'%%order_class%% .dgbm_post_item:hover .content-wrapper .post-meta.post-meta-position-bottom'
		);
		$_ex::apply_margin_padding(
			$this,
			$render_slug, 
			'meta_bottom_padding', 
			'padding', 
			'%%order_class%% .dgbm_post_item .content-wrapper .post-meta.post-meta-position-bottom',
			'%%order_class%% .dgbm_post_item:hover .content-wrapper .post-meta.post-meta-position-bottom'
		);
		// Apply text margin
		$_ex::apply_margin_padding(
			$this,
			$render_slug, 
			'text_margin', 
			'margin', 
			'%%order_class%% .dgbm_post_item .content-wrapper .post-content',
			'%%order_class%% .dgbm_post_item:hover .content-wrapper .post-content'
		);
		// Apply button wrapper margin
		$_ex::apply_margin_padding(
			$this,
			$render_slug, 
			'button_wrapper_margin', 
			'margin', 
			'%%order_class%% .dgbm_post_item .dg_read_more_wrapper',
			'%%order_class%% .dgbm_post_item:hover .dg_read_more_wrapper'
		);
		// Apply button margin
		$_ex::apply_margin_padding(
			$this,
			$render_slug, 
			'button_margin', 
			'margin', 
			'%%order_class%% .dgbm_post_item .read-more',
			'%%order_class%% .dgbm_post_item:hover .read-more'
		);
		// Apply button padding
		$_ex::apply_margin_padding(
			$this,
			$render_slug, 
			'button_padding', 
			'padding', 
			'%%order_class%% .dgbm_post_item .read-more',
			'%%order_class%% .dgbm_post_item:hover .read-more'
		);
		// button background color
		$_ex::apply_element_color(
			$this,
			$render_slug, 
			'button_bg_color', 
			'background-color', 
			'%%order_class%% .dgbm_post_item .read-more',
			'%%order_class%% .dgbm_post_item:hover .read-more', 
			true
		);

		// button alignment
		if ('' !== $this->props['button_alignment']) {
			ET_Builder_Element::set_style($render_slug, array(
				'selector' => '%%order_class%% .dgbm_post_item .dg_read_more_wrapper',
				'declaration' => sprintf('text-align: %1$s;', $this->props['button_alignment']),
			));
		}
		// button fullwidth
		if ('on' === $this->props['button_fullwidth']) {
			ET_Builder_Element::set_style($render_slug, array(
				'selector' => '%%order_class%% .dgbm_post_item .dg_read_more_wrapper .read-more',
				'declaration' => 'display: block;',
			));
		}
		// pagination background-color
		$_ex::apply_element_color(
			$this,
			$render_slug, 
			'pagination_background', 
			'background-color', 
			'%%order_class%% .pagination div a',
			'%%order_class%% .pagination div a:hover', 
			true
		);
		// pagination margin
		$_ex::apply_margin_padding(
			$this,
			$render_slug, 
			'pagination_margin', 
			'margin', 
			'%%order_class%% .pagination div a',
			'%%order_class%% .pagination div a:hover'
		);
		// pagination padding
		$_ex::apply_margin_padding(
			$this,
			$render_slug, 
			'pagination_padding', 
			'padding', 
			'%%order_class%% .pagination div a',
			'%%order_class%% .pagination div a:hover'
		);

		// image overlay color
		if ('' !== $this->props['overlay_color']) {
			ET_Builder_Element::set_style($render_slug, array(
				'selector' => '%%order_class%%.dgbm_blog_module .dgbm-image-overlay .dgbm_post_item:hover .dg-post-thumb:before',
				'declaration' => sprintf('background-color: %1$s;', $this->props['overlay_color']),
			));
		}
		// image overlay icon color
		if ('' !== $this->props['overlay_icon_color']) {
			ET_Builder_Element::set_style($render_slug, array(
				'selector' => '%%order_class%%.dgbm_blog_module .dgbm-image-overlay.dgbm-image-overlay-icon .dgbm_post_item .dg-post-thumb:after',
				'declaration' => sprintf('color: %1$s;', $this->props['overlay_icon_color']),
			));
		}
		// Single Meta background color
		// author
		$_ex::apply_element_color(
			$this,
			$render_slug, 
			'author_background_color', 
			'background-color', 
			'%%order_class%%.dgbm_blog_module .dgbm_post_item .post-meta .author',
			'%%order_class%%.dgbm_blog_module .dgbm_post_item:hover .post-meta .author', 
			true
		);
		// date
		$_ex::apply_element_color(
			$this,
			$render_slug, 
			'date_background_color', 
			'background-color', 
			'%%order_class%%.dgbm_blog_module .dgbm_post_item .post-meta .published',
			'%%order_class%%.dgbm_blog_module .dgbm_post_item:hover .post-meta .published', 
			true
		);
		// category
		$_ex::apply_element_color(
			$this,
			$render_slug, 
			'category_background_color', 
			'background-color', 
			'%%order_class%%.dgbm_blog_module .dgbm_post_item .post-meta .categories',
			'%%order_class%%.dgbm_blog_module .dgbm_post_item:hover .post-meta .categories', 
			true
		);
		// comment
		$_ex::apply_element_color(
			$this,
			$render_slug, 
			'comment_background_color', 
			'background-color', 
			'%%order_class%%.dgbm_blog_module .dgbm_post_item .post-meta .comments',
			'%%order_class%%.dgbm_blog_module .dgbm_post_item:hover .post-meta .comments', 
			true
		);
		// single meta spacing
		// author
		$_ex::apply_margin_padding(
			$this,
			$render_slug, 
			'author_margin', 
			'margin', 
			'%%order_class%%.dgbm_blog_module .dgbm_post_item .post-meta .author',
			'%%order_class%%.dgbm_blog_module .dgbm_post_item:hover .post-meta .author'
		);
		$_ex::apply_margin_padding(
			$this,
			$render_slug, 
			'author_padding', 
			'padding', 
			'%%order_class%%.dgbm_blog_module .dgbm_post_item .post-meta .author',
			'%%order_class%%.dgbm_blog_module .dgbm_post_item:hover .post-meta .author'
		);
		// date
		$_ex::apply_margin_padding(
			$this,
			$render_slug, 
			'date_margin', 
			'margin', 
			'%%order_class%%.dgbm_blog_module .dgbm_post_item .post-meta .published',
			'%%order_class%%.dgbm_blog_module .dgbm_post_item:hover .post-meta .published'
		);
		$_ex::apply_margin_padding(
			$this,
			$render_slug, 
			'date_padding', 
			'padding', 
			'%%order_class%%.dgbm_blog_module .dgbm_post_item .post-meta .published',
			'%%order_class%%.dgbm_blog_module .dgbm_post_item:hover .post-meta .published'
		);
		// category
		$_ex::apply_margin_padding(
			$this,
			$render_slug, 
			'category_margin', 
			'margin', 
			'%%order_class%%.dgbm_blog_module .dgbm_post_item .post-meta .categories',
			'%%order_class%%.dgbm_blog_module .dgbm_post_item:hover .post-meta .categories'
		);
		$_ex::apply_margin_padding(
			$this,
			$render_slug, 
			'category_padding', 
			'padding', 
			'%%order_class%%.dgbm_blog_module .dgbm_post_item .post-meta .categories',
			'%%order_class%%.dgbm_blog_module .dgbm_post_item:hover .post-meta .categories'
		);
		// comment
		$_ex::apply_margin_padding(
			$this,
			$render_slug, 
			'comment_margin', 
			'margin', 
			'%%order_class%%.dgbm_blog_module .dgbm_post_item .post-meta .comments',
			'%%order_class%%.dgbm_blog_module .dgbm_post_item:hover .post-meta .comments'
		);
		$_ex::apply_margin_padding(
			$this,
			$render_slug, 
			'comment_padding', 
			'padding', 
			'%%order_class%%.dgbm_blog_module .dgbm_post_item .post-meta .comments',
			'%%order_class%%.dgbm_blog_module .dgbm_post_item:hover .post-meta .comments'
		);
		// meta background color by position
		$_ex::apply_element_color(
			$this,
			$render_slug, 
			'default_position_bg', 
			'background-color', 
			'%%order_class%%.dgbm_blog_module .dgbm_post_item .post-meta.post-meta-position-default',
			'%%order_class%%.dgbm_blog_module .dgbm_post_item:hover .post-meta.post-meta-position-default', 
			true
		);
		$_ex::apply_element_color(
			$this,
			$render_slug, 
			'top_position_bg', 
			'background-color', 
			'%%order_class%%.dgbm_blog_module .dgbm_post_item .post-meta.post-meta-position-top',
			'%%order_class%%.dgbm_blog_module .dgbm_post_item:hover .post-meta.post-meta-position-top', 
			true
		);
		$_ex::apply_element_color(
			$this,
			$render_slug, 
			'bottom_position_bg', 
			'background-color', 
			'%%order_class%%.dgbm_blog_module .dgbm_post_item .post-meta.post-meta-position-bottom',
			'%%order_class%%.dgbm_blog_module .dgbm_post_item:hover .post-meta.post-meta-position-bottom', 
			true
		);
		// custom transition
		$this->dgbm_custom_transition($_ex, $render_slug);
		
	}

	/**
	 * Custom transition
	 */
	public function dgbm_custom_transition($_ex, $render_slug) {
		// // overlay transition
		$_ex::apply_custom_transition(
			$this,
			$render_slug,
			'%%order_class%%.dgbm_blog_module .dgbm_post_item .dg-post-thumb:before'
		);
		$_ex::apply_custom_transition(
			$this,
			$render_slug,
			'%%order_class%%.dgbm_blog_module .dgbm_post_item .dg-post-thumb:after'
		);
		$_ex::apply_custom_transition(
			$this,
			$render_slug,
			'%%order_class%%.dgbm_blog_module .dgbm_post_item .dg-post-thumb:after'
		);
		$_ex::apply_custom_transition(
			$this,
			$render_slug,
			'%%order_class%%.dgbm_blog_module .dgbm_post_item .dg-post-thumb img'
		);
		// // all transition
		// $_ex::apply_custom_transition(
		// 	$this,
		// 	$render_slug,
		// 	'%%order_class%%.dgbm_blog_module .dgbm_post_item, %%order_class%%.dgbm_blog_module .dgbm_post_item *'
		// );
		// overlay transition
		// $_ex::apply_custom_transition(
		// 	$this,
		// 	$render_slug,
		// 	'%%order_class%%.dgbm_blog_module'
		// );
		// $_ex::apply_custom_transition(
		// 	$this,
		// 	$render_slug,
		// 	'%%order_class%%.dgbm_blog_module'
		// );
		// $_ex::apply_custom_transition(
		// 	$this,
		// 	$render_slug,
		// 	'%%order_class%%.dgbm_blog_module'
		// );
		// all transition
		// $_ex::apply_custom_transition(
		// 	$this,
		// 	$render_slug,
		// 	'%%order_class%%.dgbm_blog_module'
		// );
	}
	/**
	 * Get advanced fields
	 */
	public function get_advanced_fields_config() {
		$advanced_settings = [];

		
		$advanced_settings['background'] = array(
			'css'      => array(
				'main'	=> '%%order_class%% .dgbm_post_item .content-wrapper',
				'hover'	=> '%%order_class%% .dgbm_post_item:hover .content-wrapper'
			),
			'use_background_color'          => true, // default
			'use_background_color_gradient' => true, // default
			'use_background_image'          => true, // default
			'use_background_video'          => false, // default
		);
		$advanced_settings['fonts']['title'] = array(
			'label'         => esc_html__( 'Title', 'et_builder' ),
			'toggle_slug'   => 'title_text',
			'tab_slug'		=> 'advanced',
			'hide_text_shadow'  => true,
			'line_height' => array (
				'default' => '1em',
			),
			'font_size' => array(
				'default' => '20px',
			),
			'css'      => array(
				'main' => "%%order_class%% .dgbm_post_item .content-wrapper .dg_bm_title",
				'hover' => "%%order_class%% .dgbm_post_item:hover .content-wrapper .dg_bm_title",
				'important'	=> 'all'
			),
		);
		$advanced_settings['fonts']['meta'] = array(
			'label'         			=> esc_html__( 'Meta', 'et_builder' ),
			'toggle_slug'   			=> 'meta_text',
			'sub_toggle'   				=> 'font',
			'tab_slug'					=> 'advanced',
			'hide_text_shadow'  		=> true,
			'hide_text_align'  			=> true,
			'line_height' => array (
				'default' => '1em',
			),
			'font_size' => array(
				'default' => '14px',
			),
			'css'      => array(
				'main' => "%%order_class%% .dgbm_post_item .content-wrapper .post-meta, %%order_class%% .dgbm_post_item .content-wrapper .post-meta a",
				'hover' => "%%order_class%% .dgbm_post_item:hover .content-wrapper .post-meta, %%order_class%% .dgbm_post_item:hover .content-wrapper .post-meta a",
				'important'	=> 'all'
			),
		);
		$advanced_settings['fonts']['content']   = array(
			'label'         => esc_html__( 'Content', 'et_builder' ),
			'toggle_slug'   => 'content_text',
			'tab_slug'		=> 'advanced',
			'hide_text_shadow'  => true,
			'line_height' => array (
				'default' => '1em',
			),
			'font_size' => array(
				'default' => '14px',
			),
			'css'      => array(
				'main' => "%%order_class%% .dgbm_post_item .content-wrapper .post-content",
				'hover' => "%%order_class%% .dgbm_post_item:hover .content-wrapper .post-content",
				'important'	=> 'all'
			),
		);
		$advanced_settings['fonts']['read_more']   = array(
			'toggle_slug'   => 'button_style',
			'tab_slug'		=> 'advanced',
			'hide_text_shadow'  => true,
			'line_height' => array (
				'default' => '1em',
			),
			'font_size' => array(
				'default' => '14px',
			),
			'css'      => array(
				'main' => "%%order_class%% .dgbm_post_item .dg_read_more_wrapper a,%%order_class%% .dgbm_post_item .dg_read_more_wrapper a",
				'hover' => "%%order_class%% .dgbm_post_item:hover .dg_read_more_wrapper a,%%order_class%% .dgbm_post_item:hover .dg_read_more_wrapper a",
				'important'	=> 'all'
			),
			// 'css'      => array(
			// 	'main' => "%%order_class%% .dgbm_post_item .dg_read_more_wrapper a,%%order_class%% .dgbm_post_item .dg_read_more_wrapper a span",
			// 	'hover' => "%%order_class%% .dgbm_post_item:hover .dg_read_more_wrapper a,%%order_class%% .dgbm_post_item:hover .dg_read_more_wrapper a span",
			// 	'important'	=> 'all'
			// ),
		);
		$advanced_settings['fonts']['pagination']   = array(
			'toggle_slug'   => 'pagination',
			'tab_slug'		=> 'advanced',
			'hide_text_shadow'  => true,
			'line_height' => array (
				'default' => '1em',
			),
			'font_size' => array(
				'default' => '14px',
			),
			'css'      => array(
				'main' => "%%order_class%% .pagination div a",
				'hover' => "%%order_class%% .pagination div a:hover",
				'important'	=> 'all'
			),
		);
		$advanced_settings['borders']['read_more'] = array(
			'css'             => array(
				'main' => array(
					'border_radii' => "%%order_class%% .dg-blog-inner-wrapper .dgbm_post_item .dg_read_more_wrapper a",
					'border_styles' => "%%order_class%% .dg-blog-inner-wrapper .dgbm_post_item .dg_read_more_wrapper a",
					'border_styles_hover' => "%%order_class%% .dg-blog-inner-wrapper .dgbm_post_item:hover .dg_read_more_wrapper a",
				)
			),
			'tab_slug'        => 'advanced',
			'toggle_slug'     => 'button_style',	
		);
		$advanced_settings['borders']['pagination'] = array(
			'css'             => array(
				'main' => array(
					'border_radii' => "%%order_class%% .pagination div a",
					'border_styles' => "%%order_class%% .pagination div a",
					'border_styles_hover' => "%%order_class%% .pagination div a:hover",
				)
			),
			'tab_slug'        => 'advanced',
			'toggle_slug'     => 'pagination',	
		);
		$advanced_settings['borders']['image'] = array(
			'css'             => array(
				'main' => array(
					'border_radii' => "%%order_class%% .dg-blog-inner-wrapper .dgbm_post_item .dg-post-thumb",
					'border_styles' => "%%order_class%% .dg-blog-inner-wrapper .dgbm_post_item .dg-post-thumb",
					'border_styles_hover' => "%%order_class%% .dg-blog-inner-wrapper .dgbm_post_item:hover .dg-post-thumb",
				)
			),
			'tab_slug'        => 'advanced',
			'toggle_slug'     => 'image_settings',	
		);
		$advanced_settings['borders']['post_item'] = array(
			'css'             => array(
				'main' => array(
					'border_radii' => "%%order_class%% .dg-blog-inner-wrapper .dgbm_post_item",
					'border_styles' => "%%order_class%% .dg-blog-inner-wrapper .dgbm_post_item",
					'border_styles_hover' => "%%order_class%% .dg-blog-inner-wrapper .dgbm_post_item:hover",
				)
			),
			'tab_slug'        	=> 'advanced',
			'toggle_slug'     	=> 'custom_border',	
			'label_prefix'    => esc_html__( '&rArr; Article', 'et_builder' ),
		);
		$advanced_settings['borders']['content'] = array(
			'css'             => array(
				'main' => array(
					'border_radii' => "%%order_class%% .dg-blog-inner-wrapper .dgbm_post_item .content-wrapper",
					'border_styles' => "%%order_class%% .dg-blog-inner-wrapper .dgbm_post_item .content-wrapper",
					'border_styles_hover' => "%%order_class%% .dg-blog-inner-wrapper .dgbm_post_item:hover .content-wrapper",
				)
			),
			'label_prefix'    => esc_html__( '&rArr; Content Area', 'et_builder' ),
			'tab_slug'        => 'advanced',
			'toggle_slug'     => 'custom_border',
		);
		// author border
		$advanced_settings['borders']['author'] = array(
			'css'             => array(
				'main' => array(
					'border_radii' => "%%order_class%% .dg-blog-inner-wrapper .dgbm_post_item .post-meta .author",
					'border_styles' => "%%order_class%% .dg-blog-inner-wrapper .dgbm_post_item .post-meta .author",
					'border_styles_hover' => "%%order_class%% .dg-blog-inner-wrapper .dgbm_post_item:hover .post-meta .author",
				)
			),
			'tab_slug'        => 'advanced',
			'toggle_slug'     => 'author',	
		);
		// date border
		$advanced_settings['borders']['date'] = array(
			'css'             => array(
				'main' => array(
					'border_radii' => "%%order_class%% .dg-blog-inner-wrapper .dgbm_post_item .post-meta .published",
					'border_styles' => "%%order_class%% .dg-blog-inner-wrapper .dgbm_post_item .post-meta .published",
					'border_styles_hover' => "%%order_class%% .dg-blog-inner-wrapper .dgbm_post_item:hover .post-meta .published",
				)
			),
			'tab_slug'        => 'advanced',
			'toggle_slug'     => 'date',	
		);
		// category border
		$advanced_settings['borders']['category'] = array(
			'css'             => array(
				'main' => array(
					'border_radii' => "%%order_class%% .dg-blog-inner-wrapper .dgbm_post_item .post-meta .categories",
					'border_styles' => "%%order_class%% .dg-blog-inner-wrapper .dgbm_post_item .post-meta .categories",
					'border_styles_hover' => "%%order_class%% .dg-blog-inner-wrapper .dgbm_post_item:hover .post-meta .categories",
				)
			),
			'tab_slug'        => 'advanced',
			'toggle_slug'     => 'category',	
		);
		// comment border
		$advanced_settings['borders']['comment'] = array(
			'css'             => array(
				'main' => array(
					'border_radii' => "%%order_class%% .dg-blog-inner-wrapper .dgbm_post_item .post-meta .comments",
					'border_styles' => "%%order_class%% .dg-blog-inner-wrapper .dgbm_post_item .post-meta .comments",
					'border_styles_hover' => "%%order_class%% .dg-blog-inner-wrapper .dgbm_post_item:hover .post-meta .comments",
				)
			),
			'tab_slug'        => 'advanced',
			'toggle_slug'     => 'comment',	
		);
		$advanced_settings['box_shadow']['image'] = array (
			'css' => array(
				'main' => "%%order_class%% .dg-blog-inner-wrapper .dgbm_post_item .dg-post-thumb",
			),
			'tab_slug'        => 'advanced',
			'toggle_slug'     => 'image_settings',	
		);
		$advanced_settings['box_shadow']['container'] = array (
			'css' => array(
				'main' => "%%order_class%% .dg-blog-inner-wrapper .dgbm_post_item",
			),
			'tab_slug'        => 'advanced',
			'toggle_slug'     => 'custom_boxshadow',
			'label'	          => esc_html__('&rArr; Article Container Box Shadow', 'et_builder')
		);
		$advanced_settings['box_shadow']['content'] = array (
			'css' => array(
				'main' => "%%order_class%% .dg-blog-inner-wrapper .dgbm_post_item .content-wrapper",
			),
			'tab_slug'        => 'advanced',
			'toggle_slug'     => 'custom_boxshadow',
			'label'	  		  => esc_html__('&rArr; Content Area Box Shadow', 'et_builder')
		);
		$advanced_settings['transform'] = array (
			'css' => array(
				'main' => "%%order_class%% .dg-blog-inner-wrapper .dgbm_post_item",
			),
		);
		$advanced_settings["filters"] = array(
			'child_filters_target' => array(
				'tab_slug' => 'advanced',
				'toggle_slug' => 'image_settings',
				'css' => array(
					'main' => '%%order_class%% .dg-blog-inner-wrapper .dgbm_post_item .dg-post-thumb img',
				),
			),
		);
		$advanced_settings['image'] = array(
			'css' => array(
				'main' => array(
					'%%order_class%% .dg-blog-inner-wrapper .dgbm_post_item .dg-post-thumb img',
				)
			),
		);

		// single meta text styles
		// author
		$advanced_settings['fonts']['author']   = array(
			'toggle_slug'   => 'author',
			'tab_slug'		=> 'advanced',
			'priority' 		=> 10,
			'hide_text_shadow'  => true,
			'hide_text_align'  => true,
			'line_height' => array (
				'default' => '1em',
			),
			'font_size' => array(
				'default' => '14px',
			),
			'css'      => array(
				'main' => "%%order_class%%.dgbm_blog_module .dgbm_post_item .post-meta .author",
				'hover' => "%%order_class%%.dgbm_blog_module .dgbm_post_item:hover .post-meta .author",
				'important'	=> 'all'
			),
			// 'css'      => array(
			// 	'main' => "%%order_class%%.dgbm_blog_module .dgbm_post_item .post-meta .author,
			// 	%%order_class%%.dgbm_blog_module .dgbm_post_item .post-meta .author a",
			// 	'hover' => "%%order_class%%.dgbm_blog_module .dgbm_post_item:hover .post-meta .author,
			// 	%%order_class%%.dgbm_blog_module .dgbm_post_item:hover .post-meta .author a",
			// 	'important'	=> 'all'
			// ),
		);
		// date
		$advanced_settings['fonts']['date']   = array(
			'toggle_slug'   => 'date',
			'tab_slug'		=> 'advanced',
			'priority' 		=> 10,
			'hide_text_shadow'  => true,
			'hide_text_align'  => true,
			'line_height' => array (
				'default' => '1em',
			),
			'font_size' => array(
				'default' => '14px',
			),
			'css'      => array(
				'main' => "%%order_class%%.dgbm_blog_module .dgbm_post_item .post-meta .published",
				'hover' => "%%order_class%%.dgbm_blog_module .dgbm_post_item:hover .post-meta .published",
				'important'	=> 'all'
			),
		);
		// category
		$advanced_settings['fonts']['category']   = array(
			'toggle_slug'   => 'category',
			'tab_slug'		=> 'advanced',
			'priority' 		=> 10,
			'hide_text_shadow'  => true,
			'hide_text_align'  => true,
			'line_height' => array (
				'default' => '1em',
			),
			'font_size' => array(
				'default' => '14px',
			),
			'css'      => array(
				'main' => "%%order_class%%.dgbm_blog_module .dgbm_post_item .post-meta .categories",
				'hover' => "%%order_class%%.dgbm_blog_module .dgbm_post_item:hover .post-meta .categories",
				'important'	=> 'all'
			),
			// 'css'      => array(
			// 	'main' => "%%order_class%%.dgbm_blog_module .dgbm_post_item .post-meta .categories,
			// 	%%order_class%%.dgbm_blog_module .dgbm_post_item .post-meta .categories a",
			// 	'hover' => "%%order_class%%.dgbm_blog_module .dgbm_post_item:hover .post-meta .categories,
			// 	%%order_class%%.dgbm_blog_module .dgbm_post_item:hover .post-meta .categories a",
			// 	'important'	=> 'all'
			// ),
		);
		// comment
		$advanced_settings['fonts']['comment']   = array(
			'toggle_slug'   => 'comment',
			'tab_slug'		=> 'advanced',
			'priority' 		=> 10,
			'hide_text_shadow'  => true,
			'hide_text_align'  => true,
			'line_height' => array (
				'default' => '1em',
			),
			'font_size' => array(
				'default' => '14px',
			),
			'css'      => array(
				'main' => "%%order_class%%.dgbm_blog_module .dgbm_post_item .post-meta .comments",
				'hover' => "%%order_class%%.dgbm_blog_module .dgbm_post_item:hover .post-meta .comments",
				'important'	=> 'all'
			),
		);
		// $advanced_settings['max_width'] = false;
		$advanced_settings['link_options'] = false;
		$advanced_settings['animation'] = false;
		$advanced_settings['text'] = false;
		$advanced_settings['text_shadow'] = false;
		$advanced_settings['margin_padding'] = false;

			
		return $advanced_settings;
	}

	/**
	 * Adding Custom CSS field to the 
	 */
	public function get_custom_css_fields_config() {
		return array(
			'blog_item' => array(
				'label'    => esc_html__( 'Blog Item', 'et_builder' ),
				'selector' => '%%order_class%%.dgbm_blog_module .dgbm_post_item',
			),
			'content_container' => array(
				'label'    => esc_html__( 'Content container', 'et_builder' ),
				'selector' => '%%order_class%%.dgbm_blog_module .dgbm_post_item .content-wrapper',
			),
			'image-container' => array(
				'label'    => esc_html__( 'Image Container', 'et_builder' ),
				'selector' => '%%order_class%%.dgbm_blog_module .dgbm_post_item .dg-post-thumb',
			),
			'image' => array(
				'label'    => esc_html__( 'Image', 'et_builder' ),
				'selector' => '%%order_class%%.dgbm_blog_module .dgbm_post_item .dg-post-thumb img',
			),
			'title' => array(
				'label'    => esc_html__( 'Title', 'et_builder' ),
				'selector' => '%%order_class%%.dgbm_blog_module .dgbm_post_item .content-wrapper .dg_bm_title',
			),
			'content' => array(
				'label'    => esc_html__( 'Content', 'et_builder' ),
				'selector' => '%%order_class%%.dgbm_blog_module .dgbm_post_item .content-wrapper .post-content',
			),
			'post-meta-top' => array(
				'label'    => esc_html__( 'Post meta position top', 'et_builder' ),
				'selector' => '%%order_class%%.dgbm_blog_module .dgbm_post_item .post-meta-position-top',
			),
			'post-meta-middle' => array(
				'label'    => esc_html__( 'Post meta position middle', 'et_builder' ),
				'selector' => '%%order_class%%.dgbm_blog_module .dgbm_post_item .post-meta-position-default',
			),
			'post-meta-bottom' => array(
				'label'    => esc_html__( 'Post meta position bottom', 'et_builder' ),
				'selector' => '%%order_class%%.dgbm_blog_module .dgbm_post_item .post-meta-position-bottom',
			),
			'button' => array(
				'label'    => esc_html__( 'Button', 'et_builder' ),
				'selector' => '%%order_class%%.dgbm_blog_module .dgbm_post_item .content-wrapper .read-more',
			),
			'author' => array(
				'label'    => esc_html__( 'Author', 'et_builder' ),
				'selector' => '%%order_class%%.dgbm_blog_module .dgbm_post_item .post-meta .author',
			),
			'data' => array(
				'label'    => esc_html__( 'Date', 'et_builder' ),
				'selector' => '%%order_class%%.dgbm_blog_module .dgbm_post_item .post-meta .published',
			),
			'category' => array(
				'label'    => esc_html__( 'Category', 'et_builder' ),
				'selector' => '%%order_class%%.dgbm_blog_module .dgbm_post_item .post-meta .categories',
			),
			'comment' => array(
				'label'    => esc_html__( 'Comment', 'et_builder' ),
				'selector' => '%%order_class%%.dgbm_blog_module .dgbm_post_item .post-meta .comments',
			),
		);
	}
	/**
	 * Add transition items
	 */
	public function get_transition_fields_css_props() {
		$fields = parent::get_transition_fields_css_props();

		$module = '%%order_class%%';
		$post_item = '%%order_class%% .dgbm_post_item';
		$dg_post_thumb = '%%order_class%% .dgbm_post_item .dg-post-thumb';
		$content_wrapper = '%%order_class%% .dgbm_post_item .content-wrapper';
		$dg_bm_title = '%%order_class%% .dgbm_post_item .content-wrapper .dg_bm_title';
		$post_content = '%%order_class%% .dgbm_post_item .content-wrapper .post-content';
		$meta_position_default = '%%order_class%% .dgbm_post_item .content-wrapper .post-meta.post-meta-position-default';
		$meta_position_top = '%%order_class%% .dgbm_post_item .content-wrapper .post-meta.post-meta-position-top';
		$meta_position_bottom = '%%order_class%% .dgbm_post_item .content-wrapper .post-meta.post-meta-position-bottom';
		$author = '%%order_class%%.dgbm_blog_module .dgbm_post_item .post-meta .author';
		$date = '%%order_class%%.dgbm_blog_module .dgbm_post_item .post-meta .published';
		$category = '%%order_class%%.dgbm_blog_module .dgbm_post_item .post-meta .categories';
		$comments = '%%order_class%%.dgbm_blog_module .dgbm_post_item .post-meta .comments';
		$read_more_wrapper = '%%order_class%% .dgbm_post_item .dg_read_more_wrapper';
		$button = '%%order_class%% .dgbm_post_item .read-more';
		$pagination = '%%order_class%% .pagination div a';
		$overlay = '%%order_class%% .dgbm_post_item .dg-post-thumb';
		$overlay_hover = '%%order_class%% .dgbm_post_item:hover .dg-post-thumb';

		// custom spacing
		// container
		$fields['container_margin']       	= array( 'margin' => $module );
		$fields['container_padding']       	= array( 'padding' => $module );
		$fields['article_margin']       	= array( 'margin' => $post_item );
		$fields['article_padding']       	= array( 'padding' => $post_item );
		// content
		$fields['image_margin']       		= array( 'margin' => $dg_post_thumb );
		$fields['content_margin']       	= array( 'margin' => $content_wrapper );
		$fields['content_padding']       	= array( 'padding' => $content_wrapper );
		$fields['title_margin']       		= array( 'margin' => $dg_bm_title );
		$fields['text_margin']       		= array( 'margin' => $post_content );
		// meta
		$fields['meta_default_margin']      = array( 'margin' => $meta_position_default );
		$fields['meta_default_padding']     = array( 'padding' => $meta_position_default );
		$fields['meta_top_margin']      	= array( 'margin' => $meta_position_top );
		$fields['meta_top_padding']     	= array( 'padding' => $meta_position_top );
		$fields['meta_bottom_margin']      	= array( 'margin' => $meta_position_bottom );
		$fields['meta_bottom_padding']     	= array( 'padding' => $meta_position_bottom );
		$fields['author_margin']      		= array( 'margin' => $author );
		$fields['author_padding']     		= array( 'padding' => $author );
		$fields['date_margin']      		= array( 'margin' => $date );
		$fields['date_padding']     		= array( 'padding' => $date );
		$fields['category_margin']      	= array( 'margin' => $category );
		$fields['category_padding']     	= array( 'padding' => $category );
		$fields['comment_margin']      		= array( 'margin' => $comments );
		$fields['comment_padding']     		= array( 'padding' => $comments );
		// button
		$fields['button_wrapper_margin']    = array( 'margin' => $read_more_wrapper );
		$fields['button_margin']    		= array( 'margin' => $button );
		$fields['button_padding']    		= array( 'padding' => $button );
		$fields['pagination_margin']    	= array( 'margin' => $pagination );
		$fields['pagination_padding']    	= array( 'padding' => $pagination );
		
		// background-color
		$fields['default_position_bg']    	= array( 'background-color' => $meta_position_default );
		$fields['top_position_bg']    		= array( 'background-color' => $meta_position_top );
		$fields['bottom_position_bg']    	= array( 'background-color' => $meta_position_bottom );
		$fields['author_background_color']  = array( 'background-color' => $author );
		$fields['category_background_color']= array( 'background-color' => $category );
		$fields['date_background_color']	= array( 'background-color' => $date );
		$fields['comment_background_color']	= array( 'background-color' => $comments );
		$fields['pagination_background']	= array( 'background-color' => $pagination );
		$fields['button_bg_color']			= array( 'background-color' => $button );

		
		return $fields;
	}
    
	/**
	 * get blog posts
	 */
	public function get_posts($args = array()) {
		global $paged, $post, $wp_query, $wp_the_query, $__et_blog_module_paged;

		$args = array(
			'posts_number'                  => $this->props['posts_number'],
			'include_categories'            => $this->props['include_categories'],
			'show_thumbnail'                => $this->props['show_thumbnail'],
			'show_excerpt'                  => $this->props['show_excerpt'],
			'show_author'                   => $this->props['show_author'],
			'show_date'                     => $this->props['show_date'],
			'show_categories'               => $this->props['show_categories'],
			'show_comments'                 => $this->props['show_comments'],
			'show_more'                     => $this->props['show_more'],
			'read_more_text'				=> $this->props['read_more_text'],
			'show_pagination'				=> $this->props['show_pagination'],
			'layout'						=> $this->props['layout'],
			'layout_styles'					=> $this->props['layout_styles'],
			'item_in_desktop'				=> $this->props['item_in_desktop'],
			'item_in_tablet'				=> $this->props['item_in_tablet'],
			'item_in_mobile'				=> $this->props['item_in_mobile'],
			'equal_height'					=> $this->props['equal_height'],
			'equal_height_column'			=> $this->props['equal_height_column'],
			'image_as_background'			=> $this->props['image_as_background'],
			'use_button_icon'				=> $this->props['use_button_icon'],
			'button_icon'					=> $this->props['button_icon'],
			'image_size'					=> $this->props['image_size'],
			'show_excerpt_length'			=> $this->props['show_excerpt_length'],
			'type'							=> $this->props['type'],
			'orderby'						=> $this->props['orderby'],
			'offset_number'					=> $this->props['offset_number'],
			'button_at_bottom'				=> $this->props['button_at_bottom'],
			'use_meta_icon'					=> $this->props['use_meta_icon'],
			'use_overlay_icon'				=> $this->props['use_overlay_icon'],
			'select_overlay_icon'			=> $this->props['select_overlay_icon'],
			'author_location'				=> $this->props['author_location'],
			'date_location'					=> $this->props['date_location'],
			'category_location'				=> $this->props['category_location'],
			'comment_location'				=> $this->props['comment_location'],
			'meta_date'						=> $this->props['meta_date'],
			'use_current_loop'				=> $this->props['use_current_loop'],
			'related_posts'					=> $this->props['related_posts']
		);
		$query_args = array(
            'post_type' => 'post',
            'post_status'    => 'publish',
            'posts_per_page' => intval( $args['posts_number'] ),
        );
    
        if ( '' !== $args['include_categories'] && '3' === $args['type'] ) {
            $query_args['cat'] = $args['include_categories'];
		}

		if (is_single() && $args['related_posts'] === 'on') {
			$query_args['category__in'] = wp_get_post_categories($post->ID);
			$query_args['post__not_in'] = array($post->ID);
		}

		$dg_paged = is_front_page() ? get_query_var( 'page' ) : get_query_var( 'paged' );

		if ( is_front_page() ) {
            $paged = $dg_paged; //phpcs:ignore WordPress.Variables.GlobalVariables.OverrideProhibited
		}

		if ( $__et_blog_module_paged > 1 ) {
			$dg_paged      = $__et_blog_module_paged;
			$paged         = $__et_blog_module_paged; //phpcs:ignore WordPress.Variables.GlobalVariables.OverrideProhibited
			$query_args['paged'] = $__et_blog_module_paged;
		}

        if ( ! is_search() ) {
            $paged = $dg_paged; //phpcs:ignore WordPress.Variables.GlobalVariables.OverrideProhibited
            $query_args['paged'] = $dg_paged;
        }

		// wil be removed
        // if ( defined( 'DOING_AJAX' ) && isset( $current_page['paged'] ) ) {
		// 	$paged = intval( $current_page['paged'] ); //phpcs:ignore WordPress.Variables.GlobalVariables.OverrideProhibited
		// } else {
		// 	$paged = is_front_page() ? get_query_var( 'page' ) : get_query_var( 'paged' ); //phpcs:ignore WordPress.Variables.GlobalVariables.OverrideProhibited
		// }

		// support pagination in VB
		// if ( isset( $args['__page'] ) ) {
		// 	$paged = $args['__page']; //phpcs:ignore WordPress.Variables.GlobalVariables.OverrideProhibited
		// }

        // process icon
        $button_icon = '' !== $args['button_icon'] ?  esc_attr( et_pb_process_font_icon($args['button_icon']) ) : '9';
        $data_icon = ('on' === $args['use_button_icon']) ? 
        sprintf( 'data-icon="%1$s"', $button_icon ) : '';

        $overlay_icon = '' !== $args['select_overlay_icon'] ? esc_attr( et_pb_process_font_icon($args['select_overlay_icon']) ) : '1';
        $data_overlay_icon = 'on' === $args['use_overlay_icon'] ? 
        sprintf('data-ovrlayicon=%1$s', $overlay_icon) : '';
        
        // orderby
        if ( '2' === $args['type']){
            $query_args['meta_key'] = 'post_views_count';
            $query_args['orderby'] = 'meta_value_num';
            $query_args['order'] = 'DESC';
        } else {
            if ( '4' === $args['type'] ) {
                $query_args['orderby'] = 'rand';
            } else if ( '3' === $args['type'] ) {
                if ( '3' === $args['orderby'] ) {
                    $query_args['orderby'] = 'rand';
                } else if ( '2' === $args['orderby'] ) {
                    $query_args['orderby'] = 'date';
                    $query_args['order'] = 'ASC';
                } else {
                    $query_args['orderby'] = 'date';
                    $query_args['order'] = 'DESC';
                }
            } else {
                $query_args['orderby'] = 'date';
                $query_args['order'] = 'DESC';
            }
        }

        // offset 
        if ( '' !== $args['offset_number'] && ! empty( $args['offset_number'] ) ) {
			/**
			 * Offset + pagination don't play well. Manual offset calculation required
			 * @see: https://codex.wordpress.org/Making_Custom_Queries_using_Offset_and_Pagination
			 */
			if ( $paged > 1 ) {
				$query_args['offset'] = ( ( $paged - 1 ) * intval( $args['posts_number'] ) ) + intval( $args['offset_number'] );
			} else {
				$query_args['offset'] = intval( $args['offset_number'] );
			}
		}
        // Get query
        $q = apply_filters('dgbm_blog_query', $query_args);
        // start query
        ob_start();
    
        // newly added
		if ( 'off' === $args['use_current_loop'] ) {
			query_posts( $q );
		} elseif ( is_singular() ) {
			// Force an empty result set in order to avoid loops over the current post.
			query_posts( array( 'post__in' => array( 0 ) ) );
		} else {
			// Only allow certain args when `Posts For Current Page` is set.
			$original = $wp_query->query_vars;
			$custom   = array_intersect_key( $q, array_flip( array( 'posts_per_page', 'offset', 'paged' ) ) );
			
			// Trick WP into reporting this query as the main query so third party filters
			// that check for is_main_query() are applied.
			$wp_the_query = $wp_query = new WP_Query( array_merge( $original, $custom ) );
		}
		// newly added
		if ( '' !== $args['offset_number'] && ! empty($args['offset_number'] ) ) {
			global $wp_query;
			$wp_query->found_posts   = max( 0, $wp_query->found_posts - intval( $args['offset_number'] ) );
			$wp_query->max_num_pages = ceil( $wp_query->found_posts / intval( $args['posts_number'] ) );
		}
    
        if ( have_posts() ) {  
             
            if ('full-width' !== $args['layout']) {
                $classes = 'masonry' === $args['layout'] ? 'dg-blog-masonry et_pb_salvattore_content' : 'dg-blog-grid';
                $masonry_attr = 'masonry' === $args['layout'] ? sprintf('data-columns="%1$s"', $args['item_in_desktop']) : '';
                $container = sprintf('<div class="%1$s" %2$s>', $classes, $masonry_attr);
                echo $container;
            }   
            
            while ( have_posts() ) {
                the_post();
                global $et_fb_processing_shortcode_object;
	
                $image_size     = !empty($args['image_size']) ? $args['image_size'] : 'mid';
                $width          = $image_size === 'large' ? 1080 : 400;
                $width          = (int) apply_filters( 'et_pb_blog_image_width', $width );
                $height         = $image_size === 'large' ? 675 : 250;
                $height         = (int) apply_filters( 'et_pb_blog_image_height', $height );
                $classtext      = 'et_pb_post_main_image';
                $titletext      = get_the_title();
                $thumbnail      = get_thumbnail( $width, $height, $classtext, $titletext, $titletext, false, 'Blogimage' );
                $thumb          = $thumbnail["thumb"];
                $read_more_text = sprintf(esc_html__( '%1$s', 'et_builder' ), $args['read_more_text']);
                
                // read more button
                $btn_at_bottom = 'on' === $args['button_at_bottom'] ? ' btn-at-bottm' : '';
                $read_more_button = 'on' === $args['show_more'] ? 
                        sprintf('<div class="dg_read_more_wrapper%4$s"><a class="read-more" href="%2$s" %3$s><span>%1$s</span></a></div>', 
                        $read_more_text,
                        get_the_permalink(),
                        $data_icon,
                        $btn_at_bottom
                ) : '';
                // post title markup
                $post_title = sprintf( '<h2 class="dg_bm_title"><a href="%2$s">%1$s</a></h2>', $titletext, get_the_permalink() );
                $image_style = 'full-width' === $args['layout'] && 'on' === $args['equal_height_column'] && 'image-top' !== $args['layout_styles'] ?
                sprintf('style="background-image:url(%1$s);"', dgbm_get_thumbnail_url($thumb, $thumbnail["use_timthumb"], $image_size, $width, $height)) : '';
                $image_as_bg = 'on' === $args['image_as_background'] && 'full-width' !== $args['layout'] ?
                sprintf('style="background-image:url(%1$s);"', dgbm_get_thumbnail_url($thumb, $thumbnail["use_timthumb"], $image_size, $width, $height)) : '';
                ?>
                <article id="post-<?php the_ID(); ?>" <?php post_class( 'dgbm_post_item v2' ) ?> <?php echo $image_as_bg;?>>
                    <?php 
                        if ( '' !== $thumb && 'on' === $args['show_thumbnail']) {
                            column_open($args, 'column-image');
                            echo '<div class="dg-post-thumb" '.$data_overlay_icon.'>';
                            // render post meta position over-image
                            echo dgbm_render_post_meta_html($args, 'over-image');
                            echo '<a class="featured-image-link" '.$image_style.' href="'.get_the_permalink().'">';
                                if($image_size !== 'default_image') {
                                    print_thumbnail( $thumb, $thumbnail["use_timthumb"], $titletext, $width, $height );
                                } else {
                                    dgbm_print_default_image($thumb, $titletext );
                                }
                                
                            echo '</a></div>';
                            column_close($args);
                        }
                    ?>
                    <?php column_open($args, 'column-content'); ?>
                        <div class="content-wrapper">
                            <?php 
                                // render post meta position top
                                echo dgbm_render_post_meta_html($args, 'top');
                            ?>
                            <?php echo $post_title;?>
                            <?php 
                                // render post meta position default
                                echo dgbm_render_post_meta_html($args, 'default'); 
                            ?>
                            <?php
                            // render post content
                            if('on' === $args['show_excerpt']) {
                                $content_length = empty($args['show_excerpt_length']) ? 120 : $args['show_excerpt_length'];
        
                                if ( trim(get_the_excerpt()) !== '' ) {
                                    echo '<div class="post-content">'.apply_filters('dgbm_get_post_content', get_the_excerpt(), $content_length).'</div>';
                                } else {
                                    echo '<div class="post-content">' .et_core_intentionally_unescaped( wpautop( et_delete_post_first_video( strip_shortcodes( truncate_post( $content_length, false, '', true ) ) ) ), 'html' ). '</div>';
                                }
                            }
                            ?>
                            <?php 
                                if ('on' !== $args['button_at_bottom']) {
                                    echo $read_more_button; 
                                    echo dgbm_render_post_meta_html( $args, 'bottom' ); 
                                } else {
                                    echo dgbm_render_post_meta_html( $args, 'bottom' ); 
                                    echo $read_more_button; 
                                } 
                            ?>
                            <?php 
                                // render post meta position bottom
                                
                            ?>
                            
                        </div>
                    <?php column_close($args); ?>
                </article>
                <?php
                
            } //endwhile
            if ('full-width' !== $args['layout']) {
                echo '</div>';
            } 
            
			add_filter( 'get_pagenum_link', array( 'ET_Builder_Module_Blog', 'filter_pagination_url' ) );
            if ( 'on' === $args['show_pagination'] ) {
                if ( function_exists( 'wp_pagenavi' ) ) {
                    wp_pagenavi();
                } else {
                    if ( et_is_builder_plugin_active() ) {
                        include( ET_BUILDER_PLUGIN_DIR . 'includes/navigation.php' );
                    } else {
                        get_template_part( 'includes/navigation', 'index' );
                    }
                }
			}
			remove_filter( 'get_pagenum_link', array( 'ET_Builder_Module_Blog', 'filter_pagination_url' ) );
    
        }
        wp_reset_query();
        $posts = ob_get_contents();
        ob_end_clean();
        // return the blog HTML
        return $posts;
	}
	
	/**
	 * Render the module to frontend
	 */
	public function render( $attrs, $content = null, $render_slug ) {
		$masonry_class = 'masonry' === $this->props['layout'] ? 
		sprintf(' dg-blog-masonry column-%1$s column-tablet-%2$s column-mobile-%3$s', 
		$this->props['item_in_desktop'], 
		$this->props['item_in_tablet'],
		$this->props['item_in_mobile'] ) : '';
		$style_class = '';

		if ( $this->props['layout'] === 'masonry' ) {
			wp_enqueue_script( 'salvattore' );
		}
		// equal height item
		$style_class .=  'grid' === $this->props['layout']  && 'on' === $this->props['equal_height'] ?
		' equal-hieght' : '';
		// equal height column
		$style_class .=  'full-width' === $this->props['layout'] && 'image-top' !== $this->props['layout_styles'] && 'on' === $this->props['equal_height_column'] ?
		' equal-hieght' : '';
		// verticle align
		$style_class .= 'full-width' === $this->props['layout'] && 'image-top' !== $this->props['layout_styles'] && 'on' !== $this->props['equal_height'] ? 
		' ' .$this->props['vertical_align'] : '';

		// add overlay class
		$style_class .= 'on' === $this->props['image_overlay'] ? ' dgbm-image-overlay' : '';
		$style_class .= 'on' === $this->props['image_overlay'] && 'on' === $this->props['use_overlay_icon'] ? 
		' dgbm-image-overlay-icon' : '';
		// add scale on hover
		$style_class .= 'on' === $this->props['image_scale_on_hover'] ? ' dgbm-scale-effect' : '';
		// meta icon class
		$style_class .= 'on' === $this->props['use_meta_icon'] ? ' has-meta-icon' : '';
		// featured image as background
		$style_class .= 'on' === $this->props['image_as_background'] && 'full-width' !== $this->props['layout'] ?
		' featured-image-as-background' : '';
		

		// add layout classes
		$column_class = 'full-width' !== $this->props['layout'] ? sprintf(' column-%1$s column-tablet-%2$s column-mobile-%3$s', 
		$this->props['item_in_desktop'], 
		$this->props['item_in_tablet'],
		$this->props['item_in_mobile'] ) : '';
		$full_width_class = 'full-width' === $this->props['layout'] ? ' full-width' : '';
		
		$full_width_class .= ' ' . $this->props['layout_styles'];
		
		// filter for images
		if (array_key_exists('image', $this->advanced_fields) && array_key_exists('css', $this->advanced_fields['image'])) {
			$this->add_classname($this->generate_css_filters(
				$render_slug,
				'child_',
				self::$data_utils->array_get($this->advanced_fields['image']['css'], 'main', '%%order_class%%')
			));
		 }
		 // render custom module css
		$this->additional_css_styles($render_slug);
		 // render the module
		return sprintf('<div class="dg-blog-module%4$s">
							<div class="dg-blog-inner-wrapper%2$s%3$s">
								<div class="et_pb_ajax_pagination_container">
									%1$s 
								</div>
							</div>
                        </div>',
						$this->get_posts(),
						$column_class,
						$full_width_class,
						$style_class
                    );
	}	

}

new DGbm_BlogModule;