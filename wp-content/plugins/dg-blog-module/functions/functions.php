<?php
if ( !function_exists( 'DGgetPostViews' ) ) {
    function DGgetPostViews($postID) {
        $count_key = 'post_views_count';
        $count = get_post_meta($postID, $count_key, true);
        if($count==''){
            delete_post_meta($postID, $count_key);
            add_post_meta($postID, $count_key, '0');
            return "0 View";
        }
        return $count.' Views';
    }
}

if ( !function_exists( 'DGsetPostViews' ) ) {
    function DGsetPostViews($postID) {
        $count_key = 'post_views_count';
        $count = get_post_meta($postID, $count_key, true);
        if($count==''){
            $count = 0;
            delete_post_meta($postID, $count_key);
            add_post_meta($postID, $count_key, '0');
        }else{
            $count++;
            update_post_meta($postID, $count_key, $count);
        }
    }
}

if ( !function_exists( 'dgbm_filter_the_content_in_the_main_loop' ) ) {
    function dgbm_filter_the_content_in_the_main_loop( $content ) {
        if ( is_single() && in_the_loop() && is_main_query() ) {
            DGsetPostViews(get_the_ID());
        }
        return $content;
    }
    add_filter( 'the_content', 'dgbm_filter_the_content_in_the_main_loop' );
}


/**
 * Add a filter for post content
 * @return content
 */
if ( ! function_exists( 'dgbm_get_post_content_output' ) ) {
    function dgbm_get_post_content_output( $content_text, $content_length ) {
        $content_text = ($content_length > 0) && !empty($content_text) ? 
                        substr($content_text, 0, $content_length) . '...': $content_text ;
        return $content_text;
    }
    add_filter('dgbm_get_post_content', 'dgbm_get_post_content_output', 10, 2);
}

/**
 * Render post meta
 * take $args and $location
 * 
 * render HTML
 */
if ( ! function_exists( 'dgbm_render_post_meta_html' ) ) {
    function dgbm_render_post_meta_html($args = array(), $location) {
        $markup = array(
            'default'       => '',
            'top'           => '',
            'bottom'        => '',
            'over-image'    => array(
                'over-image-top' => '',
                'over-image-bottom' => ''
            )
        );
        $post_meta = dgbm_post_meta_array( $args, $location );
        $sapartor = 'on' === $args['use_meta_icon'] ? '' : '<span class="pipe-separator">|</span>';

        foreach ( $post_meta as $key => $value ) {
            $with_seperator = '' !== $value ? $sapartor . $value : '';
            $over_image_classes = ' post-meta-over-image over-image-top';

            if ( ($key == 'author' && $args['author_location'] === $location) ||
             ($key == 'date' && $args['date_location'] === $location) || 
             ($key == 'category' && $args['category_location'] === $location) || 
             ($key == 'comment' && $args['comment_location'] === $location) ) {
                if ( 'over-image' == $location ) {
                    $markup[$location]['over-image-top'] .= $key == 'author' || $key == 'date' ? $value : '';
                    $markup[$location]['over-image-bottom'] .= $key == 'category' || $key == 'comment' ? $value : '' ;
                } else {
                    $markup[$location] .= '' === $markup[$location] ? $value : $with_seperator;
                }
            }
            
        }

        $html = '';
        if( 'over-image' == $location ) {
            $over_image_top_markup = '' !== $markup['over-image']['over-image-top'] ? 
            sprintf('<p class="post-meta%2$s">%1$s</p>', $markup[$location]['over-image-top'],$over_image_classes ) : '';
            $over_image_bottom_markup = '' !== $markup['over-image']['over-image-bottom'] ? 
            sprintf('<p class="post-meta over-image-bottom">%1$s</p>',$markup[$location]['over-image-bottom'],$over_image_classes) : '';

            $html = sprintf('%1$s %2$s', 
            $over_image_top_markup,
            $over_image_bottom_markup, 
            $over_image_classes );
        } else if( '' !== $markup[$location] && 'over-image' !== $location ) {
            $html = sprintf('<p class="post-meta post-meta-position-%2$s">%1$s</p>', $markup[$location], $location );
        }

        return '' !== $markup[$location] ? $html : '';
    }
}
/**
 * Create post meta array
 * 
 * @return array
 */
if ( ! function_exists( 'dgbm_post_meta_array' ) ) {
    function dgbm_post_meta_array( $args = array() , $location = 'default') {
        $post_meta = array();
    
        $meta_icon = 'on' === $args['use_meta_icon'] ? '<span class="meta-icon"></span>' : '';
        $post_meta['author'] = 'on' === $args['show_author'] ? 
        et_get_safe_localization( sprintf( __( '%s', 'et_builder' ), '<span class="author vcard">' . $meta_icon .  et_pb_get_the_author_posts_link() . '</span>' ) ) : '';
        
        $post_meta['date'] = 'on' === $args['show_date'] ? 
        et_get_safe_localization( sprintf( __( '%s', 'et_builder' ), '<span class="published">' .$meta_icon. esc_html( get_the_date( $args['meta_date'] ) ) . '</span>' ) ) : '';
    
        $post_meta['category'] = 'on' === $args['show_categories'] ? 
        sprintf( '<span class="categories">%2$s %1$s</span>', get_the_category_list(', '), $meta_icon ) : '';
    
        $comments = sprintf( esc_html( _nx( '%s Comment', '%s Comments', get_comments_number(), 'number of comments', 'et_builder' ) ), number_format_i18n( get_comments_number() ) );
        $post_meta['comment'] = 'on' === $args['show_comments'] ? 
        sprintf( '<span class="comments">%2$s %1$s</span>', $comments, $meta_icon ) : '';
    
        return $post_meta;
    }
}

/**
 * render the post meta content
 * 
 * @return $post_meta
 */
if ( ! function_exists( 'dgbm_render_post_meta' ) ) {
    function dgbm_render_post_meta($args) {

        $post_meta = '';
    
        if ( 'on' === $args['show_author'] || 'on' === $args['show_date'] || 'on' === $args['show_categories'] || 'on' === $args['show_comments'] ) {
            $post_meta = sprintf( '<p class="post-meta">%1$s %2$s %3$s %4$s %5$s %6$s %7$s</p>',
                (
                    'on' === $args['show_author']
                        ? et_get_safe_localization( sprintf( __( 'by %s', 'et_builder' ), '<span class="author vcard">' .  et_pb_get_the_author_posts_link() . '</span>' ) )
                        : ''
                ),
                (
                    ( 'on' === $args['show_author'] && 'on' === $args['show_date'] )
                        ? ' | '
                        : ''
                ),
                (
                    'on' === $args['show_date']
                        ? et_get_safe_localization( sprintf( __( '%s', 'et_builder' ), '<span class="published">' . esc_html( get_the_date( $args['meta_date'] ) ) . '</span>' ) )
                        : ''
                ),
                (
                    (( 'on' === $args['show_author'] || 'on' === $args['show_date'] ) && 'on' === $args['show_categories'] )
                        ? ' | '
                        : ''
                ),
                (
                    'on' === $args['show_categories']
                        ? get_the_category_list(', ')
                        : ''
                ),
                (
                    (( 'on' === $args['show_author'] || 'on' === $args['show_date'] || 'on' === $args['show_categories'] ) && 'on' === $args['show_comments'])
                        ? ' | '
                        : ''
                ),
                (
                    'on' === $args['show_comments']
                        ? sprintf( esc_html( _nx( '%s Comment', '%s Comments', get_comments_number(), 'number of comments', 'et_builder' ) ), number_format_i18n( get_comments_number() ) )
                        : ''
                )
            );
        }
        return $post_meta;
    }
}


/**
 * Render the image with default image size
 * @return html output
 */
if ( ! function_exists( 'dgbm_print_default_image' ) ) {
    function dgbm_print_default_image($thumbnail = '', $alttext = '',  $class = '' ) {
        if ( is_array( $thumbnail ) ) {
            extract( $thumbnail );
        }
        $output = '<img src="' . esc_url( $thumbnail )  . '"';
    
        if ( ! empty( $class ) ) $output .= " class='" . esc_attr( $class ) . "' ";
    
        $output .= " alt='" . esc_attr( strip_tags( $alttext ) ) . "' />";
        echo et_core_intentionally_unescaped( $output, 'html' );
    }
}

/**
 * Get the thumbnail url
 * @return $thumbnail
 */
if ( ! function_exists( 'dgbm_get_thumbnail_url' ) ) {

    function dgbm_get_thumbnail_url( $thumbnail = '' , $use_timthumb = true, $option, $width, $height ) {
        if ( is_array( $thumbnail ) ) {
			extract( $thumbnail );
		}

		if ( empty( $post ) ) global $post, $et_theme_image_sizes;

		$output = '';
		$raw = false;

        $et_post_id = ! empty( $et_post_id ) ? (int) $et_post_id : $post->ID;
        
        if ( has_post_thumbnail( $et_post_id ) ) {
            if ( 'default_image' !== $option) {
                $thumb_array['use_timthumb'] = false;

                $image_size_name = $width . 'x' . $height;
                $et_size = isset( $et_theme_image_sizes ) && array_key_exists( $image_size_name, $et_theme_image_sizes ) ? $et_theme_image_sizes[$image_size_name] : array( $width, $height );
    
                $et_attachment_image_attributes = wp_get_attachment_image_src( get_post_thumbnail_id( $et_post_id ), $et_size );
                $thumbnail = $et_attachment_image_attributes[0];
            } else {
                $thumbnail = $thumbnail;
            }
		} else {
            $thumbnail = '';
        }

        return $thumbnail;
    }

}

/**
 * Full with column open
 */
function column_open($args = array(), $class) {
    if ('full-width' === $args['layout'] && 'image-top' !== $args['layout_styles'] ) {
        echo '<div class="'.$class.'">';
    }
}
/**
 * Full with column close
 */
function column_close($args = array()) {
    if ('full-width' === $args['layout'] && 'image-top' !== $args['layout_styles'] ) {
        echo '</div>';
    }
}


/**
 * VB HTML on AJAX request
 * @return json response
 */
add_action( 'wp_ajax_dgbm_requestdata', 'dgbm_requestdata' );

function dgbm_requestdata() {
    global $paged, $post, $wp_query;
        
    $dg_paged = is_front_page() ? get_query_var( 'page' ) : get_query_var( 'paged' );
    $data = json_decode(file_get_contents('php://input'), true);
    $options = $data['props'];

    $defaults = array(
        'posts_number'                  => '',
        'include_categories'            => '',
        'show_thumbnail'                => '',
        'show_excerpt'                  => '',
        'show_author'                   => '',
        'show_date'                     => '',
        'show_categories'               => '',
        'show_comments'                 => '',
        'show_more'                     => '',
        'read_more_text'				=> '',
        'use_button_icon'				=> '',
        'button_icon'					=> '',
        'image_size'					=> '',
        'show_excerpt_length'			=> '',
        'type'							=> '',
        'orderby'						=> '',
        'offset_number'					=> '',
        'button_at_bottom'				=> '',
        'use_meta_icon'					=> '',
        'show_pagination'				=> '',
        'layout'						=> '',
        'layout_styles'					=> '',
        'item_in_desktop'				=> '',
        'item_in_tablet'				=> '',
        'item_in_mobile'				=> '',
        'equal_height'					=> '',
        'equal_height_column'			=> '',
        'image_as_background'			=> '',
        'use_overlay_icon'				=> '',
        'select_overlay_icon'			=> '',
        'author_location'				=> '',
        'date_location'					=> '',
        'category_location'				=> '',
        'comment_location'				=> '',
        'meta_date'						=> '',
        'related_posts'                 => ''
    );


    $args = wp_parse_args( $options, $defaults );
    // $args = unserialize($atts['args']);
    if ( is_front_page() ) {
        $paged = $dg_paged; //phpcs:ignore WordPress.Variables.GlobalVariables.OverrideProhibited
    }    
    
    $query_args = array(
        'post_type' => 'post',
        'post_status'    => 'publish',
        'posts_per_page' => intval( $args['posts_number'] ),
    );

    if ( '' !== $args['include_categories'] && '3' === $args['type'] ) {
        $query_args['cat'] = $args['include_categories'];
    }

    // added at version 1.0.5
    if ($data['is_single'] === 'on' && $args['related_posts'] === 'on') {
        if ( $data['single_id'] !== null) {
            $query_args['category__in'] = wp_get_post_categories($data['single_id']);
            $query_args['post__not_in'] = array($data['single_id']);
        } 
    }

    if ( ! is_search() ) {
        $paged = $dg_paged; //phpcs:ignore WordPress.Variables.GlobalVariables.OverrideProhibited
        $query_args['paged'] = $dg_paged;
    }

    if ( defined( 'DOING_AJAX' ) && isset( $current_page['paged'] ) ) {
        $paged = intval( $current_page['paged'] ); //phpcs:ignore WordPress.Variables.GlobalVariables.OverrideProhibited
    } else {
        $paged = is_front_page() ? get_query_var( 'page' ) : get_query_var( 'paged' ); //phpcs:ignore WordPress.Variables.GlobalVariables.OverrideProhibited
    }


    // support pagination in VB
    if ( isset( $args['__page'] ) ) {
        $paged = $args['__page']; //phpcs:ignore WordPress.Variables.GlobalVariables.OverrideProhibited
    }

    // process icon
    $button_icon = '' !== $args['button_icon'] ?  esc_attr( et_pb_process_font_icon($args['button_icon']) ) : '9';
    $data_icon = ('on' === $args['use_button_icon']) ? 
    sprintf( 'data-icon="%1$s"', $button_icon ) : '';

    $overlay_icon = '' !== $args['select_overlay_icon'] ? esc_attr( et_pb_process_font_icon($args['select_overlay_icon']) ) : '1';
    $data_overlay_icon = 'on' === $args['use_overlay_icon'] ? 
    sprintf('data-ovrlayicon=%1$s', $overlay_icon) : '';
    
    // orderby
    if ( '2' === $args['type']){
        $query_args['meta_key'] = 'post_views_count';
        $query_args['orderby'] = 'meta_value_num';
        $query_args['order'] = 'DESC';
    } else {
        if ( '4' === $args['type'] ) {
            $query_args['orderby'] = 'rand';
        } else if ( '3' === $args['type'] ) {
            if ( '3' === $args['orderby'] ) {
                $query_args['orderby'] = 'rand';
            } else if ( '2' === $args['orderby'] ) {
                $query_args['orderby'] = 'date';
                $query_args['order'] = 'ASC';
            } else {
                $query_args['orderby'] = 'date';
                $query_args['order'] = 'DESC';
            }
        } else {
            $query_args['orderby'] = 'date';
            $query_args['order'] = 'DESC';
        }
    }

    // offset 
    if ( '' !== $args['offset_number'] && ! empty( $args['offset_number'] ) ) {
        /**
         * Offset + pagination don't play well. Manual offset calculation required
         * @see: https://codex.wordpress.org/Making_Custom_Queries_using_Offset_and_Pagination
         */
        if ( $paged > 1 ) {
            $query_args['offset'] = ( ( $paged - 1 ) * intval( $args['posts_number'] ) ) + intval( $args['offset_number'] );
        } else {
            $query_args['offset'] = intval( $args['offset_number'] );
        }
    }
    // Get query
    $q = apply_filters('dgbm_blog_query', $query_args);
    // start query
    ob_start();

    query_posts( $q );

    if ( have_posts() ) {  
            
        if ('full-width' !== $args['layout']) {
            $classes = 'masonry' === $args['layout'] ? 'dg-blog-masonry et_pb_salvattore_content' : 'dg-blog-grid';
            $masonry_attr = 'masonry' === $args['layout'] ? sprintf('data-columns="%1$s"', $args['item_in_desktop']) : '';
            $container = sprintf('<div class="%1$s" %2$s>', $classes, $masonry_attr);
            echo $container;
        }   
        
        while ( have_posts() ) {
            the_post();
            global $et_fb_processing_shortcode_object;

            $image_size     = !empty($args['image_size']) ? $args['image_size'] : 'mid';
            $width          = $image_size === 'large' ? 1080 : 400;
            $width          = (int) apply_filters( 'et_pb_blog_image_width', $width );
            $height         = $image_size === 'large' ? 675 : 250;
            $height         = (int) apply_filters( 'et_pb_blog_image_height', $height );
            $classtext      = 'et_pb_post_main_image';
            $titletext      = get_the_title();
            $thumbnail      = get_thumbnail( $width, $height, $classtext, $titletext, $titletext, false, 'Blogimage' );
            $thumb          = $thumbnail["thumb"];
            $read_more_text = sprintf(esc_html__( '%1$s', 'et_builder' ), $args['read_more_text']);
            
            // read more button
            $btn_at_bottom = 'on' === $args['button_at_bottom'] ? ' btn-at-bottm' : '';
            $read_more_button = 'on' === $args['show_more'] ? 
                    sprintf('<div class="dg_read_more_wrapper%4$s"><a class="read-more" href="%2$s" %3$s><span>%1$s</span></a></div>', 
                    $read_more_text,
                    get_the_permalink(),
                    $data_icon,
                    $btn_at_bottom
            ) : '';
            // post title markup
            $post_title = sprintf( '<h2 class="dg_bm_title"><a href="%2$s">%1$s</a></h2>', $titletext, get_the_permalink() );
            $image_style = 'full-width' === $args['layout'] && 'on' === $args['equal_height_column'] && 'image-top' !== $args['layout_styles'] ?
            sprintf('style="background-image:url(%1$s);"', dgbm_get_thumbnail_url($thumb, $thumbnail["use_timthumb"], $image_size, $width, $height)) : '';
            $image_as_bg = 'on' === $args['image_as_background'] && 'full-width' !== $args['layout'] ?
            sprintf('style="background-image:url(%1$s);"', dgbm_get_thumbnail_url($thumb, $thumbnail["use_timthumb"], $image_size, $width, $height)) : '';
            ?>
            <article id="post-<?php the_ID(); ?>" <?php post_class( 'dgbm_post_item' ) ?> <?php echo $image_as_bg;?>>
                <?php 
                    if ( '' !== $thumb && 'on' === $args['show_thumbnail']) {
                        column_open($args, 'column-image');
                        echo '<div class="dg-post-thumb" '.$data_overlay_icon.'>';
                        // render post meta position over-image
                        echo dgbm_render_post_meta_html($args, 'over-image');
                        echo '<a class="featured-image-link" '.$image_style.' href="'.get_the_permalink().'">';
                            if($image_size !== 'default_image') {
                                print_thumbnail( $thumb, $thumbnail["use_timthumb"], $titletext, $width, $height );
                            } else {
                                dgbm_print_default_image($thumb, $titletext );
                            }
                            
                        echo '</a></div>';
                        column_close($args);
                    }
                ?>
                <?php column_open($args, 'column-content'); ?>
                    <div class="content-wrapper">
                        <?php 
                            // render post meta position top
                            echo dgbm_render_post_meta_html($args, 'top');
                        ?>
                        <?php echo $post_title;?>
                        <?php 
                            // render post meta position default
                            echo dgbm_render_post_meta_html($args, 'default'); 
                        ?>
                        <?php
                        // render post content
                        if('on' === $args['show_excerpt']) {
                            $content_length = empty($args['show_excerpt_length']) ? 120 : $args['show_excerpt_length'];
    
                            if ( trim(get_the_excerpt()) !== '' ) {
                                echo '<div class="post-content">'.apply_filters('dgbm_get_post_content', get_the_excerpt(), $content_length).'</div>';
                            } else {
                                echo '<div class="post-content">' .et_core_intentionally_unescaped( wpautop( et_delete_post_first_video( strip_shortcodes( truncate_post( $content_length, false, '', true ) ) ) ), 'html' ). '</div>';
                            }
                        }
                        ?>
                        <?php 
                            if ('on' !== $args['button_at_bottom']) {
                                echo $read_more_button; 
                                echo dgbm_render_post_meta_html( $args, 'bottom' ); 
                            } else {
                                echo dgbm_render_post_meta_html( $args, 'bottom' ); 
                                echo $read_more_button; 
                            } 
                        ?>
                        <?php 
                            // render post meta position bottom
                            
                        ?>
                        
                    </div>
                <?php column_close($args); ?>
            </article>
            <?php
            
        } //endwhile
        if ('full-width' !== $args['layout']) {
            echo '</div>';
        } 
        

        if ( 'on' === $args['show_pagination'] && ! is_search() ) {
            if ( function_exists( 'wp_pagenavi' ) ) {
                wp_pagenavi();
            } else {
                if ( et_is_builder_plugin_active() ) {
                    include( ET_BUILDER_PLUGIN_DIR . 'includes/navigation.php' );
                } else {
                    get_template_part( 'includes/navigation', 'index' );
                }
            }
        }

    }

    wp_reset_query();
    $posts = ob_get_contents();
    ob_end_clean();
    // return the blog HTML
    // return $posts;
    wp_send_json_success($posts);
}
/**
 * Make sure the VB getting the data on ajax load
 * @return json response
 */
function dgbm_et_builder_load_actions( $actions ) {
	$actions[] = 'dgbm_requestdata';

	return $actions;
}
add_filter( 'et_builder_load_actions', 'dgbm_et_builder_load_actions' );




