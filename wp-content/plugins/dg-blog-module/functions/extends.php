<?php
if (class_exists('ET_Builder_Element')) {
	class DGBM_Extends {
    
		/**
		 * Adding margin padding fields with 
		 * hover and responsive settings
		 * @param key the field key
		 * @param title field title
		 * @param toggle_slug slug for the settings group
		 * @param sub_toggle slug for the sub settings group
		 * @param priority setting view priority
		 * @return array()
		 */
		static function add_margin_padding_field($key, $title, $toggle_slug, $sub_toggle = '', $priority = 30 ) {
			$margin_padding = array();
			$margin_padding[$key] = array(
				'label'				=> sprintf(esc_html__('%1$s', 'et_builder'), $title),
				'type'				=> 'custom_margin',
				'toggle_slug'       => $toggle_slug,
				'sub_toggle'		=> $sub_toggle,
				'tab_slug'			=> 'advanced',
				'mobile_options'    => true,
				'hover'				=> 'tabs',
				'priority' 			=> $priority,
			);
			$margin_padding[$key . '_tablet'] = array(
				'type'            	=> 'skip',
				'tab_slug'        	=> 'advanced',
				'toggle_slug'		=> $toggle_slug,
			);
			$margin_padding[$key.'_phone'] = array(
				'type'            	=> 'skip',
				'tab_slug'        	=> 'advanced',
				'toggle_slug'		=> $toggle_slug,
			);
			$margin_padding[$key.'_last_edited'] = array(
				'type'            	=> 'skip',
				'tab_slug'        	=> 'advanced',
				'toggle_slug'		=> $toggle_slug,
			);
	
			return $margin_padding;
		}
		
		/**
		 * Generate margin and padding styles with the value
		 * @param module the module object itself
		 * @param render_slug 
		 * @param slug the slug of the settings
		 * @param type margin/padding
		 * @param class the selector for the styles
		 * @param hoverSelector the selector for the hover
		 * @param important whether or not the important applied
		 * @return null
		 */
		static function apply_margin_padding($module, $render_slug, $slug, $type, $class, $hoverSelector, $important = true) {
			$desktop 				= $module->props[$slug];
			$tablet 				= $module->props[$slug.'_tablet'];
			$phone 					= $module->props[$slug.'_phone'];
	
			if (class_exists('ET_Builder_Element')) {
				if(isset($desktop) && !empty($desktop)) {
					ET_Builder_Element::set_style($render_slug, array(
						'selector' => $class,
						'declaration' => et_builder_get_element_style_css($desktop, $type, $important),
					));
				}
				if (isset($tablet) && !empty($tablet)) {
					ET_Builder_Element::set_style($render_slug, array(
						'selector' => $class,
						'declaration' => et_builder_get_element_style_css($tablet, $type, $important),
						'media_query' => ET_Builder_Element::get_media_query('max_width_980'),
					));
				}
				if (isset($phone) && !empty($phone)) {
					ET_Builder_Element::set_style($render_slug, array(
						'selector' => $class,
						'declaration' => et_builder_get_element_style_css($phone, $type, $important),
						'media_query' => ET_Builder_Element::get_media_query('max_width_767'),
					));
				}
	
				if (et_builder_is_hover_enabled( $slug, $module->props ) && isset($module->props[$slug.'__hover']) ) {
					$hover = $module->props[$slug.'__hover'];
					ET_Builder_Element::set_style($render_slug, array(
						'selector' => $hoverSelector,
						'declaration' => et_builder_get_element_style_css($hover, $type, $important),
					));
				}
			}
		}
	
		/**
		 * Generate styles with the color value
		 * @param module the module object itself
		 * @param render_slug 
		 * @param slug the slug of the settings
		 * @param type color/background-color
		 * @param class css selector
		 * @param hover_class hover selector
		 * @param important important text
		 * @return null
		 */
		static function apply_element_color( $module, $render_slug, $slug, $type, $class, $hover_class, $important = false) {
			$key = $module->props[$slug];
			$important_text = true === $important ? '!important' : '';
			if ('' !== $key) {
				ET_Builder_Element::set_style($render_slug, array(
					'selector' => $class,
					'declaration' => sprintf('%2$s: %1$s %3$s;', $key, $type, $important_text),
				));
			}
			if ( et_builder_is_hover_enabled( $slug, $module->props ) && isset($module->props[$slug . '__hover']) ) {
				$slug_hover = $module->props[$slug . '__hover'];
				ET_Builder_Element::set_style($render_slug, array(
					'selector' => $hover_class,
					'declaration' => sprintf('%2$s: %1$s %3$s;', $slug_hover, $type, $important_text),
				));
			}
		}
		
		/**
		 * Generate styles for grid layout
		 * @param module the module object itself
		 * @param render_slug 
		 * @param desktop 
		 * @param tablet 
		 * @param phone 
		 * @param selector css selector
		 * @param space_between
		 * @param layout
		 * @return null
		 */
		static function grid_layout($module, $render_slug, $desktop, $tablet, $phone, $selector, $space_between, $layout) {
			$space = $module->props[$space_between];
			$spaceTablet = isset($module->props[$space_between . '_tablet']) && !empty($module->props[$space_between . '_tablet']) ? 
				$module->props[$space_between . '_tablet'] : $module->props[$space_between];
			$spacePhone = isset($module->props[$space_between . '_phone']) && !empty($module->props[$space_between . '_phone']) ? 
				$module->props[$space_between . '_phone'] : $module->props[$space_between];
			
			if( '' !== $desktop && 'grid' === $layout ) {
				$item_width_desktop = 100/$desktop . '%';
				$item_space_desktop = intval($space) - intval($space)/$desktop;
				ET_Builder_Element::set_style($render_slug, array(
					'selector' => $selector,
					'declaration' => sprintf('width:calc(%1$s - %2$spx)!important; margin-right: %3$spx; margin-bottom: %3$spx;', 
					$item_width_desktop, $item_space_desktop, intval($space)),
				));
			}
			if( '' !== $tablet && 'grid' === $layout ) {
				$item_width_tablet = 100/$tablet . '%';
				$item_space_tablet = intval($spaceTablet) - intval($spaceTablet)/$tablet;
				ET_Builder_Element::set_style($render_slug, array(
					'selector' => $selector,
					'declaration' => sprintf('width:calc(%1$s - %2$spx)!important; margin-right: %3$spx; margin-bottom: %3$spx;', 
					$item_width_tablet, $item_space_tablet, intval($spaceTablet)),
					'media_query' => ET_Builder_Element::get_media_query('max_width_980'),
				));
			}
			if( '' !== $phone && 'grid' === $layout ) {
				$item_width_phone = 100/$phone . '%';
				$item_space_phone = intval($spacePhone) - intval($spacePhone)/$phone;
				ET_Builder_Element::set_style($render_slug, array(
					'selector' => $selector,
					'declaration' => sprintf('width:calc(%1$s - %2$spx)!important; margin-right: %3$spx; margin-bottom: %3$spx;', 
					$item_width_phone, $item_space_phone, intval($spacePhone)),
					'media_query' => ET_Builder_Element::get_media_query('max_width_767'),
				));
			}
		}
		/**
		 * Generate styles for masonry layout
		 * @param module the module object itself
		 * @param render_slug 
		 * @param desktop 
		 * @param tablet 
		 * @param phone 
		 * @param selector css selector for column
		 * @param itemselector css selector for article
		 * @param space_between
		 * @param layout
		 * @return null
		 */
		static function item_width_masonry($module, $render_slug, $desktop, $tablet, $phone, $selector, $itemselector, $space_between, $layout) {
			$space = $module->props[$space_between];
			$spaceTablet = isset($module->props[$space_between . '_tablet']) && !empty($module->props[$space_between . '_tablet']) ? 
				$module->props[$space_between . '_tablet'] : $module->props[$space_between];
			$spaceMobile = isset($module->props[$space_between . '_phone']) && !empty($module->props[$space_between . '_phone']) ? 
				$module->props[$space_between . '_phone'] : $module->props[$space_between];
			
			if( '' !== $desktop && 'masonry' === $layout ) {
				$item_width_desktop = 100/$desktop . '%';
				$item_space_desktop = intval($space) - intval($space)/$desktop;
				ET_Builder_Element::set_style($render_slug, array(
					'selector' => $selector,
					'declaration' => sprintf('width:calc(%1$s - %2$spx)!important; margin-right: %3$spx;', 
					$item_width_desktop, $item_space_desktop, intval($space)),
				));
			}
			if( '' !== $tablet && 'masonry' === $layout ) {
				$item_width_tablet = 100/$tablet . '%';
				$item_space_tablet = intval($spaceTablet) - intval($spaceTablet)/$tablet;
				ET_Builder_Element::set_style($render_slug, array(
					'selector' => $selector,
					'declaration' => sprintf('width:calc(%1$s - %2$spx)!important; margin-right: %3$spx;', 
					$item_width_tablet, $item_space_tablet, intval($spaceTablet)),
					'media_query' => ET_Builder_Element::get_media_query('max_width_980'),
				));
			}
			if( '' !== $phone && 'masonry' === $layout ) {
				$item_width_phone = 100/$phone . '%';
				$item_space_phone = intval($spaceMobile) - intval($spaceMobile)/$phone;
				ET_Builder_Element::set_style($render_slug, array(
					'selector' => $selector,
					'declaration' => sprintf('width:calc(%1$s - %2$spx)!important; margin-right: %3$spx;', 
					$item_width_phone, $item_space_phone, intval($spaceMobile)),
					'media_query' => ET_Builder_Element::get_media_query('max_width_767'),
				));
			}
			if ( '' !== $desktop && 'masonry' === $layout ) {
				ET_Builder_Element::set_style($render_slug, array(
					'selector' => $itemselector,
					'declaration' => sprintf('margin-bottom: %1$spx;', intval($space)),
				));
			}
			if ( '' !== $tablet && 'masonry' === $layout ) {
				ET_Builder_Element::set_style($render_slug, array(
					'selector' => $itemselector,
					'declaration' => sprintf('margin-bottom: %1$spx;', intval($spaceTablet)),
					'media_query' => ET_Builder_Element::get_media_query('max_width_980'),
				));
			}
			if ( '' !== $phone && 'masonry' === $layout ) {
				ET_Builder_Element::set_style($render_slug, array(
					'selector' => $itemselector,
					'declaration' => sprintf('margin-bottom: %1$spx;', intval($spaceMobile)),
					'media_query' => ET_Builder_Element::get_media_query('max_width_767'),
				));
			}
		}
		/**
		 * Generate styles wiht the single 
		 * value setting ( like: font-size, spacing )
		 * @param module
		 * @param render_slug
		 * @param slug
		 * @param type
		 * @param class
		 * @param unit
		 * @param decrease
		 * @param addition
		 * @return null
		 */
		static function apply_single_value($module, $render_slug, $slug, $type, $class,$unit = '%', $decrease = false, $addition = true) {
			$desk_v = intval($module->props[$slug]);
			$tab_v = !empty($module->props[$slug.'_tablet']) || $module->props[$slug.'_tablet'] !== '' ? 
				intval($module->props[$slug.'_tablet']) : $desk_v;
			$mobile_v = !empty($module->props[$slug.'_phone']) ? 
				intval($module->props[$slug.'_phone']) : $desk_v;
	
			$desktop 	= $decrease === false ? $desk_v : 100 - $desk_v ;
			$tablet 	= $decrease === false ? $tab_v : 100 - $tab_v;
			$phone 		= $decrease === false ? $mobile_v : 100 - $mobile_v;
			$negative = $addition == false ? '-' : '';
	
			$desktop.= $unit;
			$tablet.= $unit;
			$phone.= $unit;
	
			if(isset($desktop) && !empty($desktop)) {
				ET_Builder_Element::set_style($render_slug, array(
					'selector' => $class,
					'declaration' => sprintf('%1$s:%3$s%2$s !important;', $type, $desktop, $negative),
				));
			}
			if (isset($tablet) && !empty($tablet)) {
				ET_Builder_Element::set_style($render_slug, array(
					'selector' => $class,
					'declaration' => sprintf('%1$s:%3$s%2$s !important;', $type, $tablet,$negative),
					'media_query' => ET_Builder_Element::get_media_query('max_width_980'),
				));
			}
			if (isset($phone) && !empty($phone)) {
				ET_Builder_Element::set_style($render_slug, array(
					'selector' => $class,
					'declaration' => sprintf('%1$s:%3$s%2$s !important;', $type, $phone,$negative),
					'media_query' => ET_Builder_Element::get_media_query('max_width_767'),
				));
			}
		}
		/**
		 * Custom transition to elements
		 */
		static function apply_custom_transition($module, $render_slug, $selector, $type = 'all') {
			ET_Builder_Element::set_style($render_slug, array(
				'selector' => $selector,
				'declaration' => sprintf('transition:%1$s %2$s %3$s %4$s !important;', 
					$type, 
					$module->props['hover_transition_duration'],
					$module->props['hover_transition_speed_curve'],
					$module->props['hover_transition_delay']
				),
			));
		}
		
	
	}
}
