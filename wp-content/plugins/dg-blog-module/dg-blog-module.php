<?php
/*
Plugin Name: Dg Blog Module
Plugin URI: https://www.divigear.com/
Description: A blog module for divi theme
Version: 1.0.5
Author: DiviGear
Author URI: https://www.divigear.com/
License: GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Text Domain: dgbm-dg-blog-module
Domain Path: /languages
*/


if ( ! function_exists( 'dgbm_initialize_extension' ) ):
/**
 * Creates the extension's main class instance.
 *
 * @since 1.0.0
 */
function dgbm_initialize_extension() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/DgBlogModule.php';
}
add_action( 'divi_extensions_init', 'dgbm_initialize_extension' );
endif;

define( "DGBM_MAIN_DIR", __DIR__ );


// include plugin functions
require_once (__DIR__ . '/functions/functions.php');
// include plugin settings
require_once (__DIR__ . '/core/init.php');
