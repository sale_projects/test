<?php

namespace Divi_Essential\Includes;

defined( 'ABSPATH' ) || die();

class AssetsManager {

    public function __construct(){

        add_action('wp_enqueue_scripts', array($this, 'dnxte_enqueue_assets'));

        add_action('admin_enqueue_scripts', array($this, 'dnxte_admin_enqueue_assets'));
        
        add_action('plugins_loaded', array($this, 'i18n'));
    }

    public function get_styles() {
        return array(
            'dnext_reveal_animation'         =>  array(
                'src'               =>  plugin_dir_url(__FILE__) . '../styles/reveal-animation.css',
                'version'           =>  fileatime( DIVI_ESSENTIAL_PATH . '/styles/reveal-animation.css' ),
                'enqueue'           =>  false
            ),
            'dnext_hvr_common_css'      =>  array(
                'src'               =>  plugin_dir_url(__FILE__) . '../styles/hover-common.css',
                'version'           =>  fileatime( DIVI_ESSENTIAL_PATH . '/styles/hover-common.css'),
                'enqueue'           =>  false
            ),
            'dnext_twentytwenty_css'    =>  array(
                'src'               =>  plugin_dir_url(__FILE__) . '../styles/twentytwenty.css',
                'version'           =>  fileatime( DIVI_ESSENTIAL_PATH . '/styles/twentytwenty.css'),
                'enqueue'           =>  false
            ),
            'dnext_swiper-min-css'          =>  array(
                'src'               =>  plugin_dir_url(__FILE__) . '../styles/swiper.min.css',
                'version'           =>  fileatime( DIVI_ESSENTIAL_PATH . '/styles/swiper.min.css'),
                'enqueue'           =>  false
            ),
            'dnext_magnify_css'         =>  array(
                'src'               =>  plugin_dir_url(__FILE__) . '../styles/magnify.min.css',
                'version'           =>  fileatime( DIVI_ESSENTIAL_PATH . '/styles/magnify.min.css'),
                'enqueue'           =>  false
            ),
            'dnext_magnific_popup'  =>  array(
                'src'               =>  plugin_dir_url(__FILE__) . '../styles/magnific-popup.css',
                'version'           =>  fileatime( DIVI_ESSENTIAL_PATH . '/styles/magnific-popup.css'),
                'enqueue'           =>  false
            )
        );
    }

    public function get_scripts() {
        return array(
            'dnext_wow-public'      =>  array(
                'src'           =>  plugin_dir_url(__FILE__) . '../scripts/wow.min.js',
                'version'       =>  fileatime( DIVI_ESSENTIAL_PATH . '/scripts/wow.min.js'),
                'deps'          =>  array( 'jquery' ),
                'enqueue'       =>  false,
                'piroty'        =>  false
            ),
            'dnext_wow-activation'      =>  array(
                'src'           =>  plugin_dir_url(__FILE__) . '../scripts/wow-activation.js',
                'version'       =>  '',
                'deps'          =>  array(),
                'enqueue'       =>  false,
                'piroty'        =>  false
            ),
            'dnext_svg_shape_frontend'   =>  array(
                'src'           =>  plugin_dir_url(__FILE__) . '../scripts/shape.js',
                'version'       =>  fileatime( DIVI_ESSENTIAL_PATH . '/scripts/shape.js'),
                'deps'          =>  array( 'jquery' ),
                'enqueue'       =>  false,
                'piroty'        =>  true
            ),
            'dnext_swiper_frontend'      =>  array(
                'src'           =>  plugin_dir_url(__FILE__) . '../scripts/swiper.min.js',
                'version'       =>  fileatime( DIVI_ESSENTIAL_PATH . '/scripts/swiper.min.js'),
                'deps'          =>  array( 'jquery' ),
                'enqueue'       =>  false,
                'piroty'        =>  true
            ),
            'dnxt_divinexttexts-public'=>  array(
                'src'           =>  plugin_dir_url(__FILE__) . 'modules/NextTextAnimation/dnxt-text-animation.js',
                'version'       =>  fileatime( DIVI_ESSENTIAL_PATH . '/includes/modules/NextTextAnimation/dnxt-text-animation.js'),
                'deps'          =>  array( 'jquery' ),
                'enqueue'       =>  false,
                'piroty'        =>  false
            ),
            'dnxtblrb_divinextblurb-public'       =>  array(
                'src'           =>  plugin_dir_url(__FILE__) . '.././scripts/vanilla-tilt.min.js',
                'version'       =>  fileatime( DIVI_ESSENTIAL_PATH . '/scripts/vanilla-tilt.min.js'),
                'deps'          =>  array( 'jquery' ),
                'enqueue'       =>  false,
                'piroty'        =>  true
            ),
            'dnext_default_value'=>  array(
                'src'           =>  plugin_dir_url(__FILE__) . '../scripts/default-value.js',
                'version'       =>  fileatime( DIVI_ESSENTIAL_PATH . '/scripts/default-value.js'),
                'deps'          =>  array( 'jquery' ),
                'enqueue'       =>  true,
                'piroty'        =>  true
            ),
            'dnext_bodymovin'   =>  array(
                'src'           =>  plugin_dir_url(__FILE__) . '../scripts/bodymovin.js',
                'version'       =>  fileatime( DIVI_ESSENTIAL_PATH . '/scripts/bodymovin.js'),
                'deps'          =>  array( 'jquery' ),
                'enqueue'       =>  false,
                'piroty'        =>  true
            ),
            'dnext_magnific_popup'   =>  array(
                'src'           =>  plugin_dir_url(__FILE__) . '../scripts/magnific-popup.min.js',
                'version'       =>  fileatime( DIVI_ESSENTIAL_PATH . '/scripts/magnific-popup.min.js'),
                'deps'          =>  array( 'jquery' ),
                'enqueue'       =>  true,
                'piroty'        =>  true
            ),
            'dnext_imagesloaded'=>  array(
                'src'           =>  plugin_dir_url(__FILE__) . '../scripts/imagesloaded.min.js',
                'version'       =>  fileatime( DIVI_ESSENTIAL_PATH . '/scripts/imagesloaded.min.js'),
                'deps'          =>  array( 'jquery' ),
                'enqueue'       =>  true,
                'piroty'        =>  true
            ),
            'dnext_masonry'=>  array(
                'src'           =>  plugin_dir_url(__FILE__) . '../scripts/masonry.js',
                'version'       =>  fileatime( DIVI_ESSENTIAL_PATH . '/scripts/masonry.js'),
                'deps'          =>  array( 'jquery' ),
                'enqueue'       =>  false,
                'piroty'        =>  true
            ),
            'dnext_magnifier'   =>  array(
                'src'           =>  plugin_dir_url(__FILE__) . '../scripts/magnify.min.js',
                'version'       =>  '',
                'deps'          =>  array( 'jquery' ),
                'enqueue'       =>  false,
                'piroty'        =>  false
            ),
            'dnext_facebook_sdk'=>  array(
                'src'           =>  "https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v8.0",
                'version'       =>  '',
                'deps'          =>  '',
                'enqueue'       =>  false,
                'piroty'        =>  true
            ),
            'dnext_twitter_widgets'     =>  array(
                'src'           =>  "https://platform.twitter.com/widgets.js",
                'version'       =>  '',
                'deps'          =>  '',
                'enqueue'       =>  false,
                'piroty'        =>  true
            ),
            'dnext_event_move'  =>  array(
                'src'           =>  plugin_dir_url(__FILE__) . '../scripts/event-move.js',
                'version'       =>  fileatime( DIVI_ESSENTIAL_PATH . '/scripts/event-move.js'),
                'deps'          =>  array( 'jquery' ),
                'enqueue'       =>  false,
                'piroty'        =>  true
            ),
            'dnext_twentytwenty_js'=>  array(
                'src'           =>  plugin_dir_url(__FILE__) . '../scripts/twentytwenty.js',
                'version'       =>  fileatime( DIVI_ESSENTIAL_PATH . '/scripts/twentytwenty.js'),
                'deps'          =>  array( 'jquery' ),
                'enqueue'       =>  false,
                'piroty'        =>  true
            ),
            'dnext_scripts-public'     =>  array(
                'src'           =>  plugin_dir_url(__FILE__) . '../scripts/scripts.js',
                'version'       =>  fileatime( DIVI_ESSENTIAL_PATH . '/scripts/scripts.js'),
                'deps'          =>  array( 'jquery' ),
                'enqueue'       =>  true,
                'piroty'        =>  true
            )
        );
    }

    public function dnxte_enqueue_assets() {

        $styles     = $this->get_styles();

        $scripts    = $this->get_scripts();

        foreach ($styles as $handle => $style ) {

            $deps       = isset( $style['deps'] ) ? $style['deps']  : false;
            
            if ( $style['enqueue'] ) {

                wp_enqueue_style(   $handle, $style['src'], $deps, $style['version'] );

            }elseif ( $style['enqueue'] == false ) {
                wp_register_style(  $handle, $style['src'], $deps, $style['version'] );
            }
        }

        foreach ($scripts as $handle => $script ) {

            $deps   = isset( $script['deps'] ) ? $script['deps']  : false;

            if ( $script['enqueue'] ) {
                
                wp_enqueue_script(  $handle, $script['src'], $deps, $script['version'], $script['piroty'] );
                
            }elseif ( $script['enqueue'] == false ) {

                wp_register_script(  $handle, $script['src'], $deps, $script['version'], $script['piroty'] );
            
            }
        }
    }

    public function dnxte_admin_enqueue_assets() {

        wp_verify_nonce('dnext_admin_module_css');

        global $pagenow;

        if (("admin.php" === $pagenow) && (isset($_GET['page']) && 'et_theme_builder' === $_GET['page'])) {
            $src = plugin_dir_url(__FILE__) . '../styles/admin-module.css';
            wp_enqueue_style('dnext_admin_module_css', $src, array(), null, 'all');
        }
    }

    public function i18n(){
        load_plugin_textdomain( 'dnxte-divi-essential',false ,plugin_dir_path(__FILE__) . '/languages/');
    }
}