=== BrainAddons - Divi Modules & Divi Extensions ===
Contributors: divipeople, brainaddons
Tags: divi, divi theme, divi builder, divi module, divi page builder, divi plugins, divi carousel, divi image carousel
Requires at least: 4.8
Tested up to: 5.7
Stable tag: trunk
Requires PHP: 5.6
License: GPLv2
License URI: http://www.gnu.org/licenses/gpl-2.0.html

[BrainAddons for Divi](https://brainaddons.com) Comes With 20+ Free Divi Modules: Divi Flipbox, Testimonial, Image Carousel, News Ticker, Dual Button, Advanced Divider, Smart Post List, Post Grid Layout, 5+ Extensions, & More.

== Description ==

[BrainAddons for Divi Theme & Builder](https://brainaddons.com) Is One of the Best Divi Theme Addons That Comes With **50 Divi Free & Pro Modules and 5+ Problem-Solving Divi Extensions**. Enhance your Divi page building experience with 50+ creative modules and extensions.

[Pro Version](https://brainaddons.com/pricing/) | [Live Demo](https://divi.brainaddons.com/) | [Documentation](https://brainaddons.com/docs)

### **Why Should You Choose BrainAddons?**
BrainAddons is the pioneer of adding exclusive problem-solving features for Divi Theme/Builder. Reasons for choosing BrainAddons for Div over any other Divi plugin:

- You Can Create Advanced Popup Modal
- Exclusive Blog Archive Page Designing Capability
- Design your Login and Registration Page
- Completely Customizable Every Module
- Expert & Professional Support Team
- Light Weight & Instant Loading

[DIVI PAGE BUILDER IS REQUIRED FOR THIS PLUGIN](https://www.elegantthemes.com/gallery/divi/).

### 20+ FREE DIVI MODULES AND COUNTING

YES, you are getting 20+ free outstanding useful Divi modules for your Divi builders. These 20+ of the most useful Divi modules to enhance your Divi Page Building experience and allow you to climb the top of your design capabilities. And More features and improvements are coming on regular updates.  

Let’s explore all the premium free Divi modules of Brain Addons:

1. [Divi Flipbox](https://divi.brainaddons.com/flip-box) - Flip Box Divi module makes your content more interesting with an interactive before and after effect.

2. [Divi Image Carousel](https://divi.brainaddons.com/image-carousel/) - Create interesting image and text carousels using our image carousel module which comes with lot of options.

3. [Divi Testimonial](https://divi.brainaddons.com/testimonial/) - Incredibly powerful module to create beautiful content boxes using icons, rating stars, images, links, and texts.

4. [Twitter Feed Carousel](https://divi.brainaddons.com/twitter-feed-carousel/) - Incredibly powerful module to demonstrate your products, articles, news, creative posts using a beautiful combination of texts, links, badge, and image. Show those all in vertical or horizontal motions.

5. [Video Popup](https://divi.brainaddons.com/video-popup/) - Link up Youtube, Vimeo video URLs, or upload your own video. You can add images as cover, and onto the image, you can add overly. And noted it is not annoying auto pop up, after clicking or taping then the video appears in the display as pop up.

6. [Info Box](https://divi.brainaddons.com/info-box/) - Using this module you can generate beautiful information boxes using icons, links, and texts. And make a unique type of info box.

7. [Logo Carousel](https://divi.brainaddons.com/logo-carousel/) - Show your designed logo, your creativity with this beautiful logo carousel module. Or selling multiple brands products? You can link up a specific brand product page with this logo Carousel module. 

8. [Logo Grid Module](https://divi.brainaddons.com/logo-grid/) - Show your designed logo, your creativity with this outstanding logo grid module. Or selling multiple brands products? You can link up a specific brand product page with this logo module. And an extra feature compares to Logo Carouse is text tooltip. 

9. [News Ticker](https://divi.brainaddons.com/news-ticker/) - You can show updates, popular news,  trending news, message, and dynamic content as an example of breaking news on your website.

10. [Divi Number Module](https://divi.brainaddons.com/number/) - This module can make outstanding looks number block with various styles. 

11. [Post List](https://divi.brainaddons.com/post-list/) - This module has multiple options. Using those options you can create a list of any posts, media, page, and displaying them in a creative and innovative way.

12. [Divi Review](https://divi.brainaddons.com/review/) - Are you a food blogger or product reviewer? Or you are making a website for a food blogger or a product reviewer? If YES then this module for you. Incredibly powerful module to create beautiful content boxes, rating stars, images, links, and texts. 

13. [Advanced Divider](https://divi.brainaddons.com/advanced-divider/) - Create beautiful dividers between or into a Divi builder block. Adding icons, texts, six types of border line, image mask into the text, and linkable divider. That all of those features are given.

14. [Advanced Team](https://divi.brainaddons.com/team/) - A perfect module to show your leading team in various styles using texts, images, and social links.

15. [Card Module](https://divi.brainaddons.com/card/) - One of the advanced module to demonstrate your products, creative posts using a beautiful combination of texts, links, badge, and image. You can generate eye-candy designs in a short time. 

16. [Contact Form 7](https://divi.brainaddons.com/cf7-styler/) - This module helps you to integrate existing forms built using Contact Form 7 plugin across your web pages without spending too much time.

17. [Dual Button](https://divi.brainaddons.com/dual-button/) - Using the Dual Button module you can add two flexible and trendy action buttons in a block, in different styles.

18. [Icon Box Module](https://divi.brainaddons.com/icon-box/) - A Perfect for showcasing interesting information to your users in various styles.

19. [Image Compare](https://divi.brainaddons.com/image-compare/) - This module is perfect for the image before-after slider. This module has built-in styling options, vertical and horizontal orientation features, and it can help you design these before-after sections in a more creative way.

20. [Scroll Image](https://divi.brainaddons.com/scroll-image/) - Applying this module, you can show your products or services in a way that slides horizontally or vertically. You can also create a stunning photo gallery with this awesome module.

21. [Skill Bars](https://divi.brainaddons.com/skill-bars/) - Present user skills or your skills, task percentage, required tools, and other progressive information in different ways. It comes with incredible customizing options to suit your needs. And using this module you can show your project progression.

22. [Twitter Feed](https://divi.brainaddons.com/twitter-feed/) - Module to show your products, articles, news, creative posts using a beautiful combination of texts, links, badge, and images.

23. [Animated Text](https://divi.brainaddons.com/animated-text/) - The Animated Text module allows you to decorate the text from different sections of your site by adding eye-catchy transition effects.

24. [Business Hour](https://divi.brainaddons.com/business-hour/) - With the new Business Hour module oh Brain Addons, you can showcase your business hours with style and make your webpage stand out from the rest.

### 24 Divi Modules on [Premium Version](https://brainaddons.com/pricing/)

1. [Instagram Carousel Module](https://divi.brainaddons.com/instagram-carousel) - Create a sleek instagram carousel that site visitors can slide through with ease. A perfect fit for any website to add more imagery and interaction.

2. [Divi Instagram Feed Module](https://divi.brainaddons.com/instagram-feed) - Embed your Instagram feed in under 5 minutes with the easiest and powerful Instagram module for Divi.

3. [Divi Image Magnifier](https://divi.brainaddons.com/image-magnifier) - It converts your website more interesting and more interactive. You can see every part of the photos and zoom.

4. [Lottie Animation](https://divi.brainaddons.com/lottie-animation) - Add Lottie animations and files to your WordPress website in just a few clicks using the new Lottie module. No coding required.

5. [Image Accordion](https://divi.brainaddons.com/image-accordion) - Highlight your images with amazing hover and click effects using the Image Accordion module. Just click on the images and the image will automatically expand with many customization options.

6. [Inline SVG](https://divi.brainaddons.com/inline-svg) - Add SVG images to the pages built with Divi without using any extra plugins. The perfect way to add scalable graphics to your site. Change their size and color and add links.

7. [Divi Hotspots Modile](https://divi.brainaddons.com/hotspots) - This module will help you to point out a specific part of your image in an animated way and create a visual appearance that will attract the user’s attention.

8. [Post Carousel Module](https://divi.brainaddons.com/post-carousel/) - Get the visitors’ attention in an enriched way. The Post carousel feature is now trendy in News Portals or other blogging sites, and it ensures a stunning post carousel for your blog site.  

9. [Team Carousel Module](https://divi.brainaddons.com/team-carousel/) - Display pieces of information in a beautiful and excellent way with the help of this module. You will find here many customization options, those options make this divi module the best team carousel module in divi page builder.

10. [Advanced Heading](https://divi.brainaddons.com/advanced-heading/) - Create incredible headings with powerful options and features in our Advanced Heading module. Full responsive and many customizations supported.

11. [Divi Content Toggle Module](https://divi.brainaddons.com/content-toggle/) - By using this module you can showcase your key information beautifully,  and interactively on your website to get customers’ instant attention, quicken site traffic. Text or visual content in each section, and style in advance to make a standout website for visitors to boost your sales.

12. [Divi Hover Box](https://divi.brainaddons.com/hover-box/) - Display pieces of information in a beautiful and excellent way with the help of this module. And you will get here some advanced features. Link allowed and customization of the contents is allowed.

13. [Horizontal Timeline](https://divi.brainaddons.com/horizontal-timeline/) - If you want to showcase your website timeline horizontally and give it an artistic look, then this divi module of Divi Brain Addons could be the best option for you.

14. [Divi Image Masking Module](https://divi.brainaddons.com/image-masking/) - Transform your images into individual custom appearances with the Image Masking Feature of BrainAddons Pro. And you will find here multiple amazing image frames that will be able to create beautiful image masks. 

15. [Divi List Group](https://divi.brainaddons.com/list-group) - Build incredible lists with powerful customization features and animations. And here you will find some amazing customization options that will make your lists very beautiful. 

16. [Divi Post Grid](https://divi.brainaddons.com/post-grid/) - Scaling your blog post, or any post, and page with this outstanding module without breaking any content. And show these in an amazing grid.

17. [Post Masonry](https://divi.brainaddons.com/post-masonry/) - Feature your blog post in your targeted way. Get the Divi visual builder customizations advantage and make your own desired design with the Divi Post Masonry plugin.

18. [Divi Post Tiles](https://divi.brainaddons.com/post-carousel/) - Now you can showcase your most popular blog posts exclusively with the Post Tiles module and get prominent looks.

19. [Smart Post Lists](https://divi.brainaddons.com/smart-post/) - Divi Smart Post List is the perfect solution for heavy blog based websites. It lets you display your posts in an interactive manner, inside a grid layout. Every post in this list will be visually presented in separate cards.

20. [Price Menu](https://divi.brainaddons.com/price-menu/) - Present your table menu list with or without price in a proper and artistic way with this fully manageable module. By using this module, you can showcase your menu price innovatively.

21. [Vertical Timeline](https://divi.brainaddons.com/vertical-timeline/) The Vertical Timeline module is a great option for creating attractive timelines for projects, roadmaps, upcoming and ongoing events. 

22. [Flipbox Pro](https://divi.brainaddons.com/flip-box-pro/) - Divi Flip Box module makes your content more interesting with an interactive before and after effect.

23. [Animated Text Pro](https://divi.brainaddons.com/animated-text-pro/) - Use headings to capture your visitors’ attention whenever it’s needed with the breathtaking animation effects for your text within Divi.

24. [Divi Social Share](https://divi.brainaddons.com/social-share/) - Get more traffic. Increase social shares, likes and interactions. The easiest way to add & customize share buttons on Divi page.

### FREE & PRO BUILT-IN EXTENSIONS

#### [Divi Popup](https://divi.brainaddons.com/divi-popup-maker/)
Everything is drag & drop, you are able to use Divi modules to create the needed layout and add content to it. Divi Popup Maker is the best popup plugin Divi has to offer. It is incredibly versatile & flexible. Key Features:

* Include & exclude conditions: Display popups according to your needs only for the specific pages, using multiple conditions.
* Animation effects: Catch even more attention. Apply one of the existing animation effects to the popup.
* Trigger events: Make the popup 100% noticed by choosing the most effective trigger event for this kind of information.

#### [Blog Designer](https://divi.brainaddons.com/blog/) [PRO]
Blog Designer is a good handy and premium solution for everyone who is looking for an interactive blog page with a Divi default website without using a module.

#### [Divi Login Designer](https://divi.brainaddons.com/divi-login-designer/)
Rebrand login pages on your Divi websites without hiring a developer. Divi Login Designer extension is built using the customizer API which is getting popular for live-previewing any changes to the layouts of WordPress. So, with this extension, you can preview your login page changes instantly. This extension is easy enough & required no coding skills.

#### Unfiltered File Uploads
Please note! Allowing uploads SVG & JSON files is a potential security risk. We recommend you only enable this feature if you understand the security risks involved.

#### Library Shortcodes
This extension allows you to display any Divi library template as a shortcode. Embed any Divi library template inside any Divi module or inside a .php files by using a shortcode.

### Documentation and Support

* [Documentation](https://brainaddons.com/docs)

== Installation ==

Note: This plugin works with Divi. Make sure you have [Divi Theme](https://www.elegantthemes.com/) installed.

1. Upload the plugin folder to the `/wp-content/plugins/` directory, or install the plugin through the WordPress plugins screen directly.
2. Activate the plugin through the 'Plugins' screen in WordPress
3. You can type "Brain" on your module tabs within Divi editor and all the avialable modules will appear.

== Frequently Asked Questions ==

= Can I use the plugin without Divi Theme/Builder? =

Unfortunately, No. You’ve to install Divi first. BrainAddons is dependent on Divi Theme/Builder.

== Screenshots ==
1. screenshot-1
2. screenshot-2
3. screenshot-3
4. screenshot-4
5. screenshot-5
6. screenshot-6

== Changelog ==
2.1.3 - 24 June, 2021
- Fixed: Minor fix in animated text module.

2.1.1 - 13 June, 2021
- Fixed: Customizer style issue.

2.1.0 - 13 June, 2021
- Added: Business Hour Module
- Added: Popup Maker
- Added: Blog Designer
- Updated: Admin Dashboard
- Updated: Freemius SDK

2.0.2 - 12 May, 2021
- Added: Extra theme support

2.0.1 - 08 May, 2021
- Fixed: A conflict issue.

2.0.0 - 07 May, 2021
- Added: Animated Text Module
- Added: 3 New extensions
- Added: On demand Scripts feature
- Added: Admin label for every child module
- Added: Dashboard icon,
- Added: Module icon
- Added: Image animation overflow control option for card module
- Fixed: Image compare module render issue (Builder)
- Fixed: Image compare module verticle mode design issue
- Updated: Code optimize

1.1.2 - 17 March, 2021

- Fix: Card module issues
- Maintenance: Tested upto WordPress 5.7

1.1.1 - 23 February, 2021

- Flipbox not found issue fixed

1.1.0 - 23 February, 2021

- Added Flipbox

1.0.4 - 6 February, 2021

- Post module issues fixed

1.0.3 - 3 February, 2021

- Logo grid issue fixed

1.0.2
- update some core issue

1.0.1
- Fix minor bug

1.0.0
- Initial Release

== Upgrade Notice ==