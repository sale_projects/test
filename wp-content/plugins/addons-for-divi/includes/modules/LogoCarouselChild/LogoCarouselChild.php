<?php
class BA_Logo_Carousel_Child extends BA_Builder_Module {

	public $slug                     = 'ba_logo_carousel_child';
	public $vb_support               = 'on';
	public $type                     = 'child';
	public $child_title_var          = 'admin_title';
	public $child_title_fallback_var = 'brand_name';

	public function init() {

		$this->name = esc_html__( 'Logo', 'brain-divi-addons' );

		$this->settings_modal_toggles = array(
			'general'  => array(
				'toggles' => array(
					'main_content' => esc_html__( 'Content', 'brain-divi-addons' ),
					'tab_content'  => esc_html__( 'Tab Content', 'brain-divi-addons' ),
				),
			),
			'advanced' => array(
				'toggles' => array(
					'overlay' => esc_html__( 'Overlay', 'brain-divi-addons' ),
					'borders' => esc_html__( 'Borders', 'brain-divi-addons' ),
				),
			),
		);
	}

	public function get_fields() {

		$fields = array(

			'logo'         => array(
				'label'              => esc_html__( 'Upload Logo', 'brain-divi-addons' ),
				'type'               => 'upload',
				'option_category'    => 'basic_option',
				'upload_button_text' => esc_attr__( 'Upload a Logo', 'brain-divi-addons' ),
				'choose_text'        => esc_attr__( 'Choose a Logo', 'brain-divi-addons' ),
				'update_text'        => esc_attr__( 'Set As Logo', 'brain-divi-addons' ),
				'toggle_slug'        => 'main_content',
				'mobile_options'     => true,
				'hover'              => 'tabs',
			),

			'brand_name'   => array(
				'label'       => esc_html__( 'Brand Name', 'brain-divi-addons' ),
				'type'        => 'text',
				'default'     => esc_html__( 'Brand Name', 'brain-divi-addons' ),
				'toggle_slug' => 'main_content',
			),

			'is_link'      => array(
				'label'           => esc_html__( 'Use Link', 'brain-divi-addons' ),
				'type'            => 'yes_no_button',
				'option_category' => 'configuration',
				'options'         => array(
					'on'  => esc_html__( 'Yes', 'brain-divi-addons' ),
					'off' => esc_html__( 'No', 'brain-divi-addons' ),
				),
				'default'         => 'off',
				'toggle_slug'     => 'main_content',
			),

			'link_url'     => array(
				'label'           => esc_html__( 'Link Url', 'brain-divi-addons' ),
				'type'            => 'text',
				'default'         => '',
				'dynamic_content' => 'url',
				'show_if'         => array(
					'is_link' => 'on',
				),
				'toggle_slug'     => 'main_content',
			),

			'link_options' => array(
				'type'        => 'multiple_checkboxes',
				'default'     => 'off|off',
				'toggle_slug' => 'main_content',
				'options'     => array(
					'link_target' => 'Open in new window',
					'link_rel'    => 'Add nofollow',
				),
				'show_if'     => array(
					'is_link' => 'on',
				),
			),
		);

		$label = array(
			'admin_title' => array(
				'label'       => esc_html__( 'Admin Label', 'brain-divi-addons' ),
				'type'        => 'text',
				'description' => esc_html__( 'This will change the label of the item', 'brain-divi-addons' ),
				'toggle_slug' => 'admin_label',
			),
		);

		$overlay = $this->get_overlay_option_fields( 'overlay', 'off', array() );

		return array_merge( $label, $fields, $overlay );
	}

	public function get_advanced_fields_config() {

		$advanced_fields                = array();
		$advanced_fields['text']        = false;
		$advanced_fields['fonts']       = false;
		$advanced_fields['text_shadow'] = false;
		$advanced_fields['max_width']   = false;

		$advanced_fields['margin_padding'] = array(
			'css' => array(
				'main'      => '%%order_class%% .ba-logo-carousel-item',
				'important' => 'all',
			),
		);

		$advanced_fields['borders']['item'] = array(
			'css'          => array(
				'main'      => array(
					'border_radii'  => '%%order_class%%',
					'border_styles' => '%%order_class%%',
				),
				'important' => 'all',
			),
			'label_prefix' => esc_html__( 'Item', 'brain-divi-addons' ),
			'defaults'     => array(
				'border_radii'  => 'on|0px|0px|0px|0px',
				'border_styles' => array(
					'width' => '0px',
					'color' => '#333',
					'style' => 'solid',
				),
			),
			'tab_slug'     => 'advanced',
			'toggle_slug'  => 'borders',
		);

		return $advanced_fields;
	}

	public function render_ref_attr() {

		if ( $this->props['is_link'] === 'on' ) {

			$link_options = explode( '|', $this->props['link_options'] );

			if ( $link_options[1] === 'on' ) {
				return sprintf( 'ref="nofollow"' );
			}
		}

	}

	public function render_logo() {

		$logo        = $this->props['logo'];
		$data_schema = $this->get_swapped_img_schema( 'logo' );
		$brand_name  = $this->props['brand_name'];

		if ( $this->props['is_link'] === 'on' ) {

			$link_options = explode( '|', $this->props['link_options'] );
			$target       = $link_options[0] === 'on' ? '_blank' : '_self';
			$link_url     = $this->props['link_url'];

			return sprintf(
				'<a target="%1$s" href="%2$s" %3$s><img class="ba-swapped-img" data-mfp-src="%4$s" src="%4$s" alt="%5$s" %6$s /></a>',
				$target,
				$link_url,
				$this->render_ref_attr(),
				$logo,
				$brand_name,
				$data_schema
			);
		}

		return sprintf(
			'
            <div class="ba-lightbox-ctrl"><img class="ba-swapped-img" data-mfp-src="%1$s" src="%1$s" alt="%2$s" %3$s /></div>',
			$logo,
			$brand_name,
			$data_schema
		);
	}

	public function render( $attrs, $content = null, $render_slug ) {

		// Module classes.
		$this->remove_classname( 'et_pb_module' );
		$this->add_classname( 'ba_et_pb_module' );

		// Overlay Styles.
		$this->get_overlay_style( $render_slug, 'logo', '%%order_class%% .ba-carousel-item' );

		$processed_overlay_icon = esc_attr( et_pb_process_font_icon( $this->props['overlay_icon'] ) );
		$overlay_icon           = ! empty( $processed_overlay_icon ) ? $processed_overlay_icon : '';

		return sprintf(
			'<div class="ba-carousel-item ba-logo-carousel-item ba-swapped-img-selector">
			<div class = "ba-overlay" data-icon = "%2$s"></div>
				%1$s
			</div>',
			$this->render_logo(),
			$overlay_icon
		);
	}
}

new BA_Logo_Carousel_Child();
