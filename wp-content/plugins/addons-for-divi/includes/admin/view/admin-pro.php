
<div class="ba-admin-panel">
    <div class="ba-pro-body">
        <div class="ba-row ba-row-fixed-width ba-section-support">
            <div class="ba-col ba-col-feature-box">
                <div class="ba-border-box">
                    <h3 class="ba-feature-title">Why upgrade to Premium Version?</h3>
                    <p class="ba-text f18">The premium version helps us to continue development of the product incorporating even more features and enhancements. You will also get world class support from our dedicated team, 24/7.</p>
				<a href="https://brainaddons.com/pricing/" target="_blank" class="ba-btn ba-btn-save ba-btn-lg"> GET PRO </a>
                </div>
            </div>
         </div>
    </div>
</div>