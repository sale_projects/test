<?php
/*
Plugin Name: Divi Machine
Plugin URL: https://diviengine.com/product/divi-machine/
Description: Divi Machine is the powerhouse plugin to take your websites to the next level. Build complex custom post directory websites using ACF and Divi
Version: 3.0.1
Author: Divi Engine
Author URI: https://diviengine.com
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Text Domain: divi-machine
Domain Path: /languages
@author      diviengine.com
@copyright   2020 diviengine.com

Father God, I pray that you bless the people who interact and who own this website - I pray the blessing to be one that goes beyond worldly treasures but understanding the deep love you have for them. In Jesus name, Amen

The Way of Love - 1 Corinthians 13 MSG
If I speak with human eloquence and angelic ecstasy but don’t love, I’m nothing but the creaking of a rusty gate.
If I speak God’s Word with power, revealing all his mysteries and making everything plain as day, and if I have faith that says to a mountain, “Jump,” and it jumps, but I don’t love, I’m nothing.
If I give everything I own to the poor and even go to the stake to be burned as a martyr, but I don’t love, I’ve gotten nowhere. So, no matter what I say, what I believe, and what I do, I’m bankrupt without love.

Love never gives up.
Love cares more for others than for self.
Love doesn’t want what it doesn’t have.
Love doesn’t strut,
Doesn’t have a swelled head,
Doesn’t force itself on others,
Isn’t always "me first,"
Doesn’t fly off the handle,
Doesn’t keep score of the sins of others,
Doesn’t revel when others grovel,
Takes pleasure in the flowering of truth,
Puts up with anything,
Trusts God always,
Always looks for the best,
Never looks back,
But keeps going to the end.

Love never dies. Inspired speech will be over some day; praying in tongues will end; understanding will reach its limit. We know only a portion of the truth, and what we say about God is always incomplete. But when the Complete arrives, our incompletes will be canceled.
When I was an infant at my mother’s breast, I gurgled and cooed like any infant. When I grew up, I left those infant ways for good.
We don’t yet see things clearly. We’re squinting in a fog, peering through a mist. But it won’t be long before the weather clears and the sun shines bright! We’ll see it all then, see it all as clearly as God sees us, knowing him directly just as he knows us!
But for right now, until that completeness, we have three things to do to lead us toward that consummation: Trust steadily in God, hope unswervingly, love extravagantly. And the best of the three is love.
*/

if ( ! defined( 'ABSPATH' ) ) exit;

define('DE_DMACH_VERSION', '3.0.1');

define('DE_DMACH_AUTHOR', 'Divi Engine' );
define('DE_DMACH_PATH',   plugin_dir_path(__FILE__));
define('DE_DMACH_PATH_URL',    plugins_url('', __FILE__));
define('DE_DMACH_PRODUCT_ID',           'WP-DE-DMACH');
define('DE_DMACH_INSTANCE',             str_replace(array ("https://" , "http://"), "", home_url() ));
define('DE_DMACH_PRODUCT_URL', 'https://diviengine.com/product/divi-machine/' );
define('DE_DMACH_APP_API_URL', 'https://diviengine.com/index.php');
define('DE_DMACH_URL', 'https://www.diviengine.com' );

function acf_missing_notice_divi_machine() {
    echo '<div class="error"><p><strong>' . sprintf(esc_html__('Divi Machine requires ACF to be installed and active. You can download %s here.', 'divi-machine'), '<a href="https://wordpress.org/plugins/advanced-custom-fields/" target="_blank">Advanced Custom Fields</a>') . '</strong></p></div>';
}

if( !class_exists('acf') ) {
    add_filter('acf/format_value/type=textarea', 'do_shortcode');
    add_action('admin_notices', 'acf_missing_notice_divi_machine');
    return;
}

register_activation_hook( __FILE__, 'divimachine_activate_hook' );
function divimachine_activate_hook() {
    flush_rewrite_rules();
}


include(DE_DMACH_PATH . '/includes/classes/init.class.php');
include(DE_DMACH_PATH . '/includes/classes/class.wooslt.php');
include(DE_DMACH_PATH . '/includes/classes/class.licence.php');
include(DE_DMACH_PATH . '/includes/classes/class.options.php');
include(DE_DMACH_PATH . '/includes/classes/class.updater.php');

include(DE_DMACH_PATH . '/includes/classes/acf_fields.php');


require_once dirname( __FILE__ ) .'/functions.php';
require_once dirname( __FILE__ ) .'/lib/custom-post.php';
require_once dirname( __FILE__ ) .'/lib/create-post.php';
require_once dirname( __FILE__ ) .'/lib/custom-css-js.php';

require_once dirname( __FILE__ ) .'/includes/ajaxcalls/post-ajax.php';

// initialise Divi Modules
if ( !function_exists( 'de_dm_initialise_ext' ) ) {
    function de_dm_initialise_ext()
    {
        require_once plugin_dir_path( __FILE__ ) . 'includes/DiviMachineModules.php';
    }
    add_action( 'divi_extensions_init', 'de_dm_initialise_ext' );
}

if ( !function_exists('de_dm_load_actions_ajax')) {
  function de_dm_load_actions_ajax( $actions ) {

    $filter_handler_exist = false;
    $modal_handler_exist = false;
    $loadmore_handler_exist = false;
    $filter_counter_handler_exist = false;

    foreach ($actions as $key => $action) {
      if ( $action == 'divi_filter_ajax_handler' )
        $filter_handler_exist = true;
      if ( $action == 'divi_filter_get_post_modal_ajax_handler' )
        $modal_handler_exist = true;
      if ( $action == 'divi_filter_loadmore_ajax_handler')
        $loadmore_handler_exist = true;
      if ( $action == 'divi_filter_get_count_ajax_handler')
        $filter_counter_handler_exist = true;
    }

    if ( !$filter_handler_exist )
      $actions[] = 'divi_filter_ajax_handler';

    if ( !$modal_handler_exist )
      $actions[] = 'divi_filter_get_post_modal_ajax_handler';

    if ( !$loadmore_handler_exist )
      $actions[] = 'divi_filter_loadmore_ajax_handler';

    if ( !$filter_counter_handler_exist )
      $actions[] = 'divi_filter_get_count_ajax_handler';

    return $actions;
  }

  add_filter( 'et_builder_load_actions', 'de_dm_load_actions_ajax' );  
}

$get_divi_engine_css = get_option('divi-engine-css', null);
if ($get_divi_engine_css == "" || $get_divi_engine_css == "machine-added" ) {
    update_option('divi-engine-css', 'machine-added');

    add_action( 'admin_enqueue_scripts', 'load_divi_engine_style_machine' , 20);
    function load_divi_engine_style_machine() {

        $cssfile = plugins_url( 'styles/divi-engine.css', __FILE__ );
        wp_enqueue_style( 'divi_engine_admin_css', $cssfile , false, DE_DMACH_VERSION );

    }

}


add_action( 'admin_enqueue_scripts', 'divi_machine_admin_scripts' , 20);
function divi_machine_admin_scripts($hook_suffix) {

    $jsfile = plugins_url( 'scripts/admin.min.js', __FILE__ );
    $importjsfile = plugins_url( 'import_export.js', __FILE__ );
    wp_enqueue_script( 'divi-machine-admin-script', $jsfile, array( 'jquery' ), DE_DMACH_VERSION );
    wp_enqueue_script( 'divi-machine-admin-import-export', $importjsfile, array( 'jquery' ), DE_DMACH_VERSION );

}

add_filter( 'post_type_link', 'divi_machine_remove_baseslug', 20, 3 );

function divi_machine_remove_baseslug( $permalink, $post, $leavename ){
  global $wp_post_types;
  foreach( $wp_post_types as $type => $custom_post ){
    if( $custom_post->_builtin == false && $custom_post->name == 'dmach_post'){
      $custom_post->rewrite['slug'] = trim( $custom_post->rewrite['slug'], '/' );
      $permalink = str_replace( '/' . $custom_post->rewrite['slug'] . '/', '/', $permalink );
    }
  }
  return $permalink;
}


//
//
// // add_action( 'init', function() {
// //   ps_register_shortcode_ajax( 'misha_filter_function', 'misha_filter_function' );
// // } );
// //©
// // function ps_register_shortcode_ajax( $callable, $action ) {
// //
// //   if ( empty( $_POST['action'] ) || $_POST['action'] != $action )
// //     return;
// //
// //   call_user_func( $callable );
// // }
//
// add_action('wp_ajax_myfilter', 'misha_filter_function'); // wp_ajax_{ACTION HERE}
// add_action('wp_ajax_nopriv_myfilter', 'misha_filter_function');
//
// function misha_filter_function(){
//  $args = array(
//     'post_type' => 'cars',
//    'orderby' => 'date', // we will sort posts by date
//    'order' => $_POST['date'] // ASC or DESC
//  );
//
//  // for taxonomies / categories
//  if( isset( $_POST['categoryfilter'] ) )
//    $args['tax_query'] = array(
//      array(
//        'taxonomy' => 'category',
//        'field' => 'id',
//        'terms' => $_POST['categoryfilter']
//      )
//    );
//
//  // create $args['meta_query'] array if one of the following fields is filled
//  if( isset( $_POST['price_min'] ) && $_POST['price_min'] || isset( $_POST['price_max'] ) && $_POST['price_max'] || isset( $_POST['featured_image'] ) && $_POST['featured_image'] == 'on' )
//    $args['meta_query'] = array( 'relation'=>'AND' ); // AND means that all conditions of meta_query should be true
//
//  // if both minimum price and maximum price are specified we will use BETWEEN comparison
//  if( isset( $_POST['price_min'] ) && $_POST['price_min'] && isset( $_POST['price_max'] ) && $_POST['price_max'] ) {
//    $args['meta_query'][] = array(
//      'key' => '_price',
//      'value' => array( $_POST['price_min'], $_POST['price_max'] ),
//      'type' => 'numeric',
//      'compare' => 'between'
//    );
//  } else {
//    // if only min price is set
//    if( isset( $_POST['price_min'] ) && $_POST['price_min'] )
//      $args['meta_query'][] = array(
//        'key' => '_price',
//        'value' => $_POST['price_min'],
//        'type' => 'numeric',
//        'compare' => '>'
//      );
//
//    // if only max price is set
//    if( isset( $_POST['price_max'] ) && $_POST['price_max'] )
//      $args['meta_query'][] = array(
//        'key' => '_price',
//        'value' => $_POST['price_max'],
//        'type' => 'numeric',
//        'compare' => '<'
//      );
//  }
//
//
//  // if post thumbnail is set
//  if( isset( $_POST['featured_image'] ) && $_POST['featured_image'] == 'on' )
//    $args['meta_query'][] = array(
//      'key' => '_thumbnail_id',
//      'compare' => 'EXISTS'
//    );
//  // if you want to use multiple checkboxed, just duplicate the above 5 lines for each checkbox
//
//  $query = new WP_Query( $args );
//
//  if( $query->have_posts() ) :
//    while( $query->have_posts() ): $query->the_post();
//     //
//     // if( ! function_exists( 'do_shortcode' ) ) {
//     //   include( ABSPATH . 'wp-includes/shortcodes.php' );
//     //   echo "yes";
//     // }
//     //
//     // echo do_shortcode('[[et_pb_row global_module="54"][/et_pb_row]');
//     if ( function_exists( 'do_shortcode ' ) )  {
//             echo "do_shortcode exists";
//         } else {
//             echo "do_shortcode doesn't exist";  // this gets echoed
//         }
//
// echo do_shortcode('[[et_pb_row global_module="19"][/et_pb_row]');
//
//    endwhile;
//    wp_reset_postdata();
//  else :
//    echo 'No posts found';
//  endif;
//
//  die();
// }

function prefix_start_session() {
    if( !session_id() ) {
        session_start();
    }

    if ( !isset( $_SESSION['random'] ) ) {
        $_SESSION['random'] = rand();
    }
    session_write_close();
}

add_action( 'init', 'prefix_start_session' );

////////////////////////////////////////////////

// GET QUERY VARS

function divi_machine_add_query_vars_filter( $vars ){
/*$acf_fields = array();
$field_groups = acf_get_field_groups();
foreach ( $field_groups as $group ) {
  // DO NOT USE here: $fields = acf_get_fields($group['key']);
  // because it causes repeater field bugs and returns "trashed" fields
  $fields = get_posts(array(
    'posts_per_page'   => -1,
    'post_type'        => 'acf-field',
    'orderby'          => 'menu_order',
    'order'            => 'ASC',
    'suppress_filters' => true, // DO NOT allow WPML to modify the query
    'post_parent'      => $group['ID'],
    'post_status'      => 'any',
    'update_post_meta_cache' => false
  ));


  foreach ( $fields as $field ) {
    //$vars[] = $field->post_excerpt;
  }
}
unset($vars['cars_category']);*/
if ( !empty( $_GET['filter'] ) && $_GET['filter'] == 'true' ) {
  if ( !empty( $vars['s'] ) ) {
    unset( $vars['s'] );
  }
}
return $vars;

}
add_filter( 'request', 'divi_machine_add_query_vars_filter' );

// END GET QUERY VARS

$acf_fields = array();
$field_groups = acf_get_field_groups();

foreach ( $field_groups as $group ) {
  // DO NOT USE here: $fields = acf_get_fields($group['key']);
  // because it causes repeater field bugs and returns "trashed" fields
  $fields = get_posts(array(
    'posts_per_page'   => -1,
    'post_type'        => 'acf-field',
    'orderby'          => 'menu_order',
    'order'            => 'ASC',
    'suppress_filters' => true, // DO NOT allow WPML to modify the query
    'post_parent'      => $group['ID'],
    'post_status'      => 'any',
    'update_post_meta_cache' => false
  ));


  foreach ( $fields as $field ) {
    $acf_fields[$field->post_name] = $field->post_excerpt;
  }
}


// array of filters (field key => field name)
$GLOBALS['my_query_filters'] = $acf_fields;

add_action( 'parse_tax_query', 'dmach_parse_tax_query', 10);
function dmach_parse_tax_query( $query ){
    if ( isset( $_GET['filter'] ) && $_GET['filter'] == "true" ){
        $post_type = isset($query->query_vars['post_type'])?$query->query_vars['post_type']:'';
        if ( !empty( $post_type ) ){
            foreach ($query->tax_query->queries as $key => $tax_query) {
                if ( isset( $tax_query['taxonomy'] ) && ( $tax_query['taxonomy'] == $post_type . "_category" || ( $post_type == 'post' && $tax_query['taxonomy'] == 'category' ) ) ) {
                    if ( !isset( $GLOBALS['my_query_filters']['tax_query'] ) || $GLOBALS['my_query_filters']['tax_query']  != $post_type . "_category" ){
                        unset( $query->tax_query->queries[$key]);
                    }
                }
            }
        }
    }

}

///////////////////////////////////////////////


/**
 * [list_searcheable_acf list all the custom fields we want to include in our search query]
 * @return [array] [list of custom fields]
 */
function list_searcheable_acf(){


  $list_searcheable_acf = array();
  $field_groups = acf_get_field_groups();
  foreach ( $field_groups as $group ) {
    // DO NOT USE here: $fields = acf_get_fields($group['key']);
    // because it causes repeater field bugs and returns "trashed" fields
    $fields = get_posts(array(
      'posts_per_page'   => -1,
      'post_type'        => 'acf-field',
      'orderby'          => 'menu_order',
      'order'            => 'ASC',
      'suppress_filters' => true, // DO NOT allow WPML to modify the query
      'post_parent'      => $group['ID'],
      'post_status'      => 'any',
      'update_post_meta_cache' => false
    ));
    foreach ( $fields as $field ) {
      $list_searcheable_acf[] = $field->post_excerpt;
    }
  }


  return $list_searcheable_acf;
}


/**
 * [advanced_custom_search search that encompasses ACF/advanced custom fields and taxonomies and split expression before request]
 * @param  [query-part/string]      $where    [the initial "where" part of the search query]
 * @param  [object]                 $wp_query []
 * @return [query-part/string]      $where    [the "where" part of the search query as we customized]
 * see https://vzurczak.wordpress.com/2013/06/15/extend-the-default-wordpress-search/
 * credits to Vincent Zurczak for the base query structure/spliting tags section
 */
function dmach_advanced_custom_search( $where, $wp_query ) {

    global $wpdb, $current_screen;

    // get search expression
    $terms = $wp_query->query_vars[ 's' ];

    if ( empty( $terms ) ){
      return $where;
    }

    if (!empty( $current_screen ) && $current_screen->base == 'edit' && substr($current_screen->id, 0, 4) == 'edit' ){
      return $where;
    }

    // reset search in order to rebuilt it as we whish
    $where = '';

    // get searcheable_acf, a list of advanced custom fields you want to search content in
    $list_searcheable_acf = list_searcheable_acf();

    $i = 0;

    $where .= "
      AND (
        ({$wpdb->posts}.post_title LIKE '%$terms%')
        OR ({$wpdb->posts}.post_content LIKE '%$terms%')";

    if ( !empty( $list_searcheable_acf ) ){
      $where .= "OR {$wpdb->posts}.ID IN (
          SELECT post_id FROM $wpdb->postmeta
            WHERE ";
    foreach ($list_searcheable_acf as $searcheable_acf) :
      if ( $i == 0 ) :
        $where .= " (meta_key LIKE '%" . $searcheable_acf . "%' AND meta_value LIKE '%$terms%') ";
      else :
        $where .= " OR (meta_key LIKE '%" . $searcheable_acf . "%' AND meta_value LIKE '%$terms%') ";
      endif;
      $i++;
    endforeach;
      $where .= "
        )";
    }
    $where .= "
        OR {$wpdb->posts}.ID IN (
          SELECT comment_post_ID FROM $wpdb->comments
          WHERE comment_content LIKE '%$terms%'
        )
        OR {$wpdb->posts}.ID IN (
          SELECT object_id FROM $wpdb->terms
          INNER JOIN $wpdb->term_taxonomy
            ON {$wpdb->term_taxonomy}.term_id = {$wpdb->terms}.term_id
          INNER JOIN $wpdb->term_relationships
            ON {$wpdb->term_relationships}.term_taxonomy_id = {$wpdb->term_taxonomy}.term_taxonomy_id
          WHERE {$wpdb->terms}.name LIKE '%$terms%'
        )
    )";

    return $where;
}

add_filter( 'posts_search', 'dmach_advanced_custom_search', 20, 2 );

function dmach_load_actions_ajax( $actions ) {
  $actions[] = 'divi_machine_filterposts_handler';
  $actions[] = 'divi_machine_get_post_modal_ajax_handler';
  $actions[] = 'divi_machine_loadmore_ajax_handler';
  
  return $actions;
}
add_filter( 'et_builder_load_actions', 'dmach_load_actions_ajax' );


function machine_internet_explorer_jquery() {
?>
<script>
jQuery(document).ready(function(i){const c=window.navigator.userAgent;function t(c){i(".et_pb_de_mach_archive_loop").each(function(t,s){var e,n,o,d=i(this).find(".dmach-grid-item"),h=(e=i(".dmach-grid-sizes"),n=c,o=void 0,i(e.attr("class").split(" ")).each(function(){this.indexOf(n)>-1&&(o=this)}),o).replace(c,""),a=1,r=1;i(d).each(function(i,c){a++});var l=Math.ceil(a/h),m=l*h;i(d).each(function(c,t){var s=(r-1)%h+1,e=Math.ceil(r*l/m);i(this).closest(".grid-posts").find(".dmach-grid-item:nth-child("+r+")").css("-ms-grid-row",""+e),i(this).closest(".grid-posts").find(".dmach-grid-item:nth-child("+r+")").css("-ms-grid-column",""+s),r++})})}/MSIE|Trident/.test(c)&&i(window).on("resize",function(){i(window).width()>=981?(col_size="col-desk-",t(col_size)):(col_size="col-mob-",t(col_size))})});
</script>
<?php
  }
  add_action('wp_head', 'machine_internet_explorer_jquery');


function machine_internet_explorer_css() {
?>

<style>
.col-desk-1>:not(.no-results-layout){display:-ms-grid;-ms-grid-columns:1fr}.col-desk-2>:not(.no-results-layout){display:-ms-grid;-ms-grid-columns:1fr 1fr}.col-desk-3>:not(.no-results-layout){display:-ms-grid;-ms-grid-columns:1fr 1fr 1fr}.col-desk-4>:not(.no-results-layout){display:-ms-grid;-ms-grid-columns:1fr 1fr 1fr 1fr}.col-desk-5>:not(.no-results-layout){display:-ms-grid;-ms-grid-columns:1fr 1fr 1fr 1fr 1fr}.col-desk-6>:not(.no-results-layout){display:-ms-grid;-ms-grid-columns:1fr 1fr 1fr 1fr 1fr 1fr}@media(max-width:980px){body .col-mob-1>:not(.no-results-layout){display:-ms-grid;-ms-grid-columns:1fr}body .col-mob-2>:not(.no-results-layout){display:-ms-grid;-ms-grid-columns:1fr 1fr}}@media screen and (-ms-high-contrast:active),(-ms-high-contrast:none){.et_pb_gutters4 .dmach-grid-sizes>:not(.no-results-layout)>div{margin-left:8%!important;margin-right:8%!important}.et_pb_gutters3 .dmach-grid-sizes>:not(.no-results-layout)>div{margin-left:5.5%!important;margin-right:5.5%!important}.et_pb_gutters2 .dmach-grid-sizes>:not(.no-results-layout)>div{margin-left:3%!important;margin-right:3%!important}.et_pb_gutters1 .dmach-grid-sizes>:not(.no-results-layout)>div{margin-left:0!important;margin-right:0!important}}
</style>

<?php
}
add_action('wp_head', 'machine_internet_explorer_css');


function dmach_acf_map_key() {
  $et_google_api_settings = get_option( 'et_google_api_settings' );
  acf_update_setting('google_api_key', $et_google_api_settings['api_key']);
}
add_action('acf/init', 'dmach_acf_map_key');

// $et_google_api_settings = get_option( 'et_google_api_settings' );
// echo "peter";
// echo $et_google_api_settings['api_key'];
// wp_register_script('aa_js_googlemaps_script',  'https://maps.googleapis.com/maps/api/js?key='.$et_google_api_settings.''); // with Google Maps API fix
// wp_enqueue_script('aa_js_googlemaps_script');




//populate divi module with ACF field
add_filter('et_pb_module_shortcode_attributes', 'dmach_divi_modules_acf', 20, 3);

function dmach_divi_modules_acf($dmach_props, $cd_atts, $get_slug) {
    include(DE_DMACH_PATH . '/titan-framework/titan-framework-embedder.php');

    $titan = TitanFramework::getInstance( 'divi-machine' );

    $dmach_map_acf = $titan->getOption( 'dmach_map_acf' );
    $dmach_post_type = $titan->getOption( 'dmach_post_type' );

    if ($dmach_map_acf !== "none") {

        $location = get_field($dmach_map_acf);

        $dmach_module_slug = array('et_pb_map_pin', 'et_pb_map');

        if (!in_array($get_slug, $dmach_module_slug)) {
            return $dmach_props;
        }

        if ( get_post_type() == $dmach_post_type && is_array($location) ) {
            $dmach_props['pin_address_lat'] = esc_attr($location['lat']);
            $dmach_props['pin_address_lng'] = esc_attr($location['lng']);

            if ( $get_slug == 'et_pb_map'){
                $dmach_props['address_lat'] = esc_attr( $location['lat'] );
                $dmach_props['address_lng'] = esc_attr( $location['lng'] );  
                $dmach_props['address'] = esc_attr( $location['address'] );
            }

            return $dmach_props;
        }else return $dmach_props;
    }else return $dmach_props;
}


// function to add the expired date and time to the divi countdown timer
add_filter('et_pb_module_shortcode_attributes', 'dmach_countdown_timer_acf', 20, 3);

function dmach_countdown_timer_acf($dmach_props, $cd_atts, $cd_slug) {
  include(DE_DMACH_PATH . '/titan-framework/titan-framework-embedder.php');
  $titan = TitanFramework::getInstance( 'divi-machine' );
  $dmach_countdown_acf = $titan->getOption( 'dmach_countdown_acf' );
  $dmach_countdown_post_type = $titan->getOption( 'dmach_countdown_post_type' );
  
  if ($dmach_countdown_acf !== "none") {
    $cd_module_slugs = array('et_pb_countdown_timer'); 
    if (!in_array($cd_slug, $cd_module_slugs)) {
      return $dmach_props;
    }
    if ( get_post_type() == $dmach_countdown_post_type && $dmach_countdown_acf != "" ) {
      $dmach_props['date_time'] = get_field($dmach_countdown_acf, false, false); 
      return $dmach_props;
    } else return $dmach_props;
  } else return $dmach_props;
}


// // Add the custom columns to the cars post type:
// add_filter('manage_cars_posts_columns', 'ST4_columns_cars_head');
// add_action('manage_cars_posts_custom_column', 'ST4_columns_cars_content', 10, 2);

// // ADD TWO NEW COLUMNS
// function ST4_columns_cars_head($defaults) {
//     $defaults['dealer']  = 'Dealer';
//     return $defaults;
// }
 
// function ST4_columns_cars_content ( $column, $post_id ) {
//   switch ( $column ) {

//       case 'dealer':
//           $get_dealer = get_field('dealer', $post_id);
//           if ( isset($get_dealer) ) { 
//           // print_r($get_dealer);
//           $admin_edit = get_edit_post_link($get_dealer["0"]->ID);
//           echo '<a href="'. $admin_edit . '" target="_blank">'. $get_dealer["0"]->post_title .'</a>';

//           }
//           break;

//   }
// }



// add_action('init', 'divi_machine_rewrite_rules');
// function divi_machine_rewrite_rules( ){

//   $URL_request = $_SERVER['REQUEST_URI'];
//   echo $URL_request;
  
//   add_rewrite_rule( 'cars/filter/(.*)/?$', 'index.php?post_type=cars&filter=true&query=$matches[1]','top');
//   flush_rewrite_rules();

// }