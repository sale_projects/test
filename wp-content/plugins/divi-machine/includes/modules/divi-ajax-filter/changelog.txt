/* CHANGELOG */
* VERSION
* NEW ADDITION - Added setting to show a button that when they click will toggle the filter on mobile
* MOD - New setting to only show category/tags/attributes/custo tax that are assigned to the posts
* MOD - Added setting to scroll down to a section on ajax update - choose posts, orderby or filter.
* MOD - Added option to open the whole grid in a new tab
* FIX/MOD - Added box shadow for thumbnail image
* FIX - Link whole grid on filter
* FIX - filtering issue for multiple loop module
* FIX - filter count issue
* MOD - Hide Reset if any filter is not selected
* MOD - Add option to exclude category on machine loop module
* FIX - Map pin issue
* FIX - Non woocommerce Archive loop module php bug
* ADD - Add product weight & product rating filter
* FIX - Flip image issue on Thumbnail module
* FIX - Loadmore issue when include multiple categories & tags
* FIX - order by issue for custom query loop
*
* VERSION 1.0.2 - 04/12/2020
* FIX - Fixed pagination issue with BC
* FIX - PHP fatal error when installed on BC
* FIX - linked post tilte bug
* FIX - Product Title style
* FIX - Product List not working
* FIX - double box shadow setting for thumbnail
* MOD - Sync with bd ( modify js object name, fix default sortorder etc)
*
* VERSION 1.0.1 - 26/11/2020
* FIX - Same function names as Machine - fixed
*
* VERSION 1.0 - 26/11/2020
* Launch Initial Version