<?php
if ( ! defined( 'ABSPATH' ) ) exit;

class DE_Filter extends DiviExtension {

	/**
	 * The gettext domain for the extension's translations.
	 *
	 * @since 2.0.0
	 *
	 * @var string
	 */
	public $gettext_domain = 'divi-ajax-filter';

	/**
	 * The extension's WP Plugin name.
	 *
	 * @since 2.0.0
	 *
	 * @var string
	 */
	public $name = 'divi-ajax-filter';

	/**
	 * The extension's version
	 *
	 * @since 2.1.5
	 *
	 * @var string
	 */
	public $version = DE_DF_VERSION;
	/**
	 * DE_Machine constructor.
	 *
	 * @param string $name
	 * @param array  $args
	 */
	public function __construct( $name = 'divi-ajax-filter', $args = array() ) {
		$this->plugin_dir     = plugin_dir_path( __FILE__ );
		$this->plugin_dir_url = plugin_dir_url( $this->plugin_dir );

		parent::__construct( $name, $args );
	}

	public static function get_divi_layouts(  ){

		if (!is_admin()) {
			return;
		}

		$options = array();

		$layout_query = array(
			'post_type'=>'et_pb_layout'
			, 'posts_per_page'=>-1
			, 'meta_query' => array(
					array(
							'key' => '_et_pb_predefined_layout',
							'compare' => 'NOT EXISTS',
					),
			)
		);

		$options['none'] = 'No Layout (please choose one)';
		if ($layouts = get_posts($layout_query)) {
		foreach ($layouts as $layout) {
		$options[$layout->ID] = $layout->post_title;
		}
		}

		return $options;
	}

	public static function get_divi_post_types(  ){

		if (!is_admin()) {
			return;
		}

		$options_posttype = array();

		$args_posttype = array(
			'public'   => true,
		);

		$output = 'names'; // names or objects, note names is the default
		$operator = 'and'; // 'and' or 'or'

		$post_types = get_post_types( $args_posttype, $output, $operator );

		foreach ( $post_types  as $post_type ) {
			if ($post_type == "attachment") {} else {
			$options_posttype[$post_type] = $post_type;
			}
		}

		return $options_posttype;
	}

	public static function et_icon_css_content( $font_icon ){
		$icon = preg_replace( '/(&amp;#x)|;/', '', et_pb_process_font_icon( $font_icon ) );

		return '\\' . $icon;
	}


	public static function get_acf_fields(  ){

		$acf_fields = array();

		            $fields_all = get_posts(array(
		              'posts_per_page'   => -1,
		              'post_type'        => 'acf-field',
		              'orderby'          => 'name',
		              'order'            => 'ASC',
			            'post_status'       => 'publish',
		            ));

								$acf_fields['none'] = 'Please select an ACF field';

		            foreach ( $fields_all as $field ) {

									$post_parent = $field->post_parent;
									$post_parent_name = get_the_title($post_parent);
									$grandparent = wp_get_post_parent_id($post_parent);
									$grandparent_name = get_the_title($grandparent);

		              $acf_fields[$field->post_name] = $post_parent_name . " > " . $field->post_title . " - " . $grandparent_name;

		            }

							
		$field_groups = acf_get_field_groups();
		foreach ( $field_groups as $group ) {
			// DO NOT USE here: $fields = acf_get_fields($group['key']);
			// because it causes repeater field bugs and returns "trashed" fields
			$fields = get_posts(array(
				'posts_per_page'   => -1,
				'post_type'        => 'acf-field',
				'orderby'          => 'name',
				'order'            => 'ASC',
				'suppress_filters' => true, // DO NOT allow WPML to modify the query
				'post_parent'      => $group['ID'],
				'post_status'       => 'publish',
				'update_post_meta_cache' => false
			));

			$acf_fields['none'] = 'Please select an ACF field';

			foreach ( $fields as $field ) {

				$acf_fields[$field->post_name] = $field->post_title . " - " . $group['title'];

			}

		}


		return $acf_fields;
	}

}

new DE_Filter;
