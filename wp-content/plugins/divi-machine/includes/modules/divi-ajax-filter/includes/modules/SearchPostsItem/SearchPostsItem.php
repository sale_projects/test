<?php
if ( !function_exists("Divi_filter_item_module_import") ){
    add_action( 'et_builder_ready', 'Divi_filter_item_module_import');

    function Divi_filter_item_module_import(){
        if(class_exists("ET_Builder_Module") && !class_exists("et_pb_df_product_filter_item_code")){
            
            class et_pb_df_product_filter_item_code extends ET_Builder_Module {

                public $vb_support = 'on';

                protected $module_credits = array(
                    'module_uri' => DE_DF_PRODUCT_URL,
                    'author'     => DE_DF_AUTHOR,
                    'author_uri' => DE_DF_URL,
                );

                function init() {

                  if (defined('DE_DMACH_VERSION')) {
                    $this->name       = esc_html__( 'Search/Filter Item', 'divi-ajax-filter' );
                    $this->advanced_setting_title_text = esc_html__( 'New Search/Filter Item', 'et_builder' );
                    $this->settings_text               = esc_html__( 'Search/Filter Item Settings', 'et_builder' );
                } else {
                  $this->name       = esc_html__( 'Filter Item', 'divi-ajax-filter' );
                  $this->advanced_setting_title_text = esc_html__( 'New Filter Item', 'et_builder' );
                  $this->settings_text               = esc_html__( 'Filter Item Settings', 'et_builder' );
                }

                    $this->slug = 'et_pb_de_mach_search_posts_item';
                    $this->vb_support      = 'on';
                    $this->type                        = 'child';
                    $this->child_title_var             = 'title';

                    $this->fields_defaults = array(
                    // 'loop_layout'         => array( 'on' ),
                    );

                    $this->settings_modal_toggles = array(
                        'general' => array(
                            'toggles' => array(
                                'main_content' => esc_html__( 'Main Options', 'divi-ajax-filter' ),
                                'layout' => esc_html__( 'Layout Options', 'divi-ajax-filter' ),
                                'text_filter' => esc_html__( 'Text Filter Options', 'divi-ajax-filter' ),
                                'select_filter' => esc_html__( 'Select Filter Options', 'divi-ajax-filter' ),
                                'tags_filter' => esc_html__( 'Category / Tags / Custom Tax / Attribute Filter Options', 'divi-ajax-filter' ),
                                'radio_filter' => esc_html__( 'Checkbox / Radio Filter Options', 'divi-ajax-filter' ),
                                'range_filter' => esc_html__( 'Number/Range Filter Options', 'divi-ajax-filter' ),
                                'filter_style' => esc_html__( 'Radio / Checkbox Style', 'divi-ajax-filter' ),
                                'swatch_style_tab' => esc_html__( 'Filter Swatch Style', 'divi-ajax-filter' ),
                            ),
                        ),
                        'advanced' => array(
                            'toggles' => array(
                                'text' => esc_html__( 'Text', 'divi-ajax-filter' ),
                                'select' => array(
                                    'title' => esc_html__( 'Select Style', 'et_builder' ),
                                ),
                            ),
                        ),
                    );

                    $this->main_css_element = '%%order_class%%';

                    $this->advanced_fields = array(
                        'fonts' => array(
                            'title' => array(
                                'label'    => esc_html__( 'Item', 'divi-ajax-filter' ),
                                'css'      => array(
                                    'main' => "%%order_class%% .et_pb_contact_select, %%order_class%%, %%order_class%% input[type=text], %%order_class%% .divi-filter-item, %%order_class%% input[type=text]::-webkit-input-placeholder, %%order_class%% input[type=text]:-moz-placeholder, %%order_class%% input[type=text]::-moz-placeholder, %%order_class%% input[type=text]:-ms-input-placeholder",
                                    'important' => 'plugin_only',
                                ),
                                'font_size' => array(
                                    'default' => '14px',
                                ),
                                'line_height' => array(
                                    'default' => '1em',
                                ),
                            ),
                            'radio_button_text' => array(
                                'label'    => esc_html__( 'Checkbox / Radio Button Style', 'divi-ajax-filter' ),
                                'css'      => array(
                                    'main' => "%%order_class%% .divi-radio-buttons .et_pb_contact_field_radio label",
                                    'important' => 'plugin_only',
                                ),
                                'font_size' => array(
                                    'default' => '14px',
                                ),
                                'line_height' => array(
                                    'default' => '1em',
                                ),
                            ),
                            'radio_button_text_checked' => array(
                                'label'    => esc_html__( 'Checkbox / Radio Button Checked Style', 'divi-ajax-filter' ),
                                'css'      => array(
                                    'main' => "%%order_class%% .divi-radio-buttons .et_pb_contact_field_radio input:checked+label",
                                    'important' => 'plugin_only',
                                ),
                                'font_size' => array(
                                    'default' => '14px',
                                ),
                                'line_height' => array(
                                    'default' => '1em',
                                ),
                            ),
                            'range_primary_text' => array(
                                'label'    => esc_html__( 'Range Primary', 'divi-ajax-filter' ),
                                'css'      => array(
                                    'main' => "%%order_class%% .irs-from, %%order_class%% .irs-to, %%order_class%% .irs-single",
                                    'important' => 'plugin_only',
                                ),
                                'font_size' => array(
                                    'default' => '14px',
                                ),
                                'line_height' => array(
                                    'default' => '1em',
                                ),
                            ),
                            'range_secondary_text' => array(
                                'label'    => esc_html__( 'Range Secondary', 'divi-ajax-filter' ),
                                'css'      => array(
                                    'main' => "%%order_class%% .irs-min, %%order_class%% .irs-max",
                                    'important' => 'plugin_only',
                                ),
                                'font_size' => array(
                                    'default' => '14px',
                                ),
                                'line_height' => array(
                                    'default' => '1em',
                                ),
                            ),
                        ),
                        'background' => array(
                            'settings' => array(
                                'color' => 'alpha',
                            ),
                        ),
                        'button' => array(),
                        'form_field'           => array(
                            'form_field' => array(
                                'label'         => esc_html__( 'Fields', 'et_builder' ),
                                'css'           => array(
                                    'main' => '%%order_class%% .et_pb_contact_select, %%order_class%% .et_pb_contact_field[type="checkbox"] + label i, %%order_class%% .et_pb_contact_field[type="radio"] + label i',
                                    'background_color'       => '%%order_class%% .et_pb_contact_select, %%order_class%% input.divi-filter-item, %%order_class%% .et_pb_contact_field[type="checkbox"] + label i, %%order_class%% .et_pb_contact_field[type="radio"] + label i',
                                    'background_color_hover' => '%%order_class%% .et_pb_contact_select:hover, %%order_class%% input.divi-filter-item:hover, %%order_class%% .et_pb_contact_field[type="checkbox"]:hover + label i, %%order_class%% .et_pb_contact_field[type="radio"]:hover + label i',
                                    'focus_background_color' => '%%order_class%% .et_pb_contact_select:focus, %%order_class%% input.divi-filter-item:focus, %%order_class%% .et_pb_contact_field[type="checkbox"]:active + label i, %%order_class%% .et_pb_contact_field[type="radio"]:active + label i',
                                    'focus_background_color_hover' => '%%order_class%% .et_pb_contact_select:focus:hover, %%order_class%% input.divi-filter-item:focus:hover, %%order_class%% .et_pb_contact_field[type="checkbox"]:active:hover + label i, %%order_class%% .et_pb_contact_field[type="radio"]:active:hover + label i',
                                    'placeholder_focus'      => '%%order_class%% p input:focus::-webkit-input-placeholder, %%order_class%% input.divi-filter-item:focus::-webkit-input-placeholder, %%order_class%% p input:focus::-moz-placeholder, %%order_class%% p input:focus:-ms-input-placeholder, %%order_class%% p textarea:focus::-webkit-input-placeholder, %%order_class%% p textarea:focus::-moz-placeholder, %%order_class%% p textarea:focus:-ms-input-placeholder',
                                    'padding'                => '%%order_class%% .et_pb_contact_select, %%order_class%% .et_pb_contact_field[type="checkbox"] + label i, %%order_class%% .et_pb_contact_field[type="radio"] + label i',
                                    'margin'                 => '%%order_class%% .et_pb_contact_select, %%order_class%% .et_pb_contact_field[type="checkbox"] + label i, %%order_class%% .et_pb_contact_field[type="radio"] + label i',
                                    'form_text_color'        => '%%order_class%% .et_pb_contact_select, %%order_class%% input.divi-filter-item, %%order_class%% .et_pb_contact_field[type="checkbox"] + label, %%order_class%% .et_pb_contact_field[type="radio"] + label, %%order_class%% .et_pb_contact_field[type="checkbox"]:checked + label i:before',
                                    'form_text_color_hover'  => '%%order_class%% .et_pb_contact_select:hover, %%order_class%% input.divi-filter-item:hover, %%order_class%% .et_pb_contact_field[type="checkbox"]:hover + label, %%order_class%% .et_pb_contact_field[type="radio"]:hover + label, %%order_class%% .et_pb_contact_field[type="checkbox"]:checked:hover + label i:before',
                                    'focus_text_color'       => '%%order_class%% .et_pb_contact_select:focus, %%order_class%% input.divi-filter-item:focus, %%order_class%% .et_pb_contact_field[type="checkbox"]:active + label, %%order_class%% .et_pb_contact_field[type="radio"]:active + label, %%order_class%% .et_pb_contact_field[type="checkbox"]:checked:active + label i:before',
                                    'focus_text_color_hover' => '%%order_class%% .et_pb_contact_select:focus:hover, %%order_class%% input.divi-filter-item:focus:hover, %%order_class%% .et_pb_contact_field[type="checkbox"]:active:hover + label, %%order_class%% .et_pb_contact_field[type="radio"]:active:hover + label, %%order_class%% .et_pb_contact_field[type="checkbox"]:checked:active:hover + label i:before',
                                ),
                                
                                'box_shadow'    => false,
                                'border_styles' => false,
                                'font_field'    => array(
                                    'css' => array(
                                        'main'  => implode( ', ', array(
                                            "{$this->main_css_element} input",
                                            "{$this->main_css_element} select",
                                            "{$this->main_css_element} input::placeholder",
                                            "{$this->main_css_element} input::-webkit-input-placeholder",
                                            "{$this->main_css_element} input::-moz-placeholder",
                                            "{$this->main_css_element} input:-ms-input-placeholder",
                                            "{$this->main_css_element} input[type=checkbox] + label",
                                            "{$this->main_css_element} input[type=radio] + label",
                                        ) ),
                                        'hover' => array(
                                            "{$this->main_css_element} input:hover",
                                            "{$this->main_css_element} select:hover",
                                            "{$this->main_css_element} input:hover::placeholder",
                                            "{$this->main_css_element} input:hover::-webkit-input-placeholder",
                                            "{$this->main_css_element} input:hover::-moz-placeholder",
                                            "{$this->main_css_element} input:hover:-ms-input-placeholder",
                                            "{$this->main_css_element} input[type=checkbox]:hover + label",
                                            "{$this->main_css_element} input[type=radio]:hover + label",
                                        ),
                                    ),
                                ),
                                'margin_padding' => array(
                                    'css'        => array(
                                        'main'    => '%%order_class%% .et_pb_contact_field',
                                        'padding' => '%%order_class%% .et_pb_contact_field input',
                                        'margin'  => '%%order_class%% .et_pb_contact_field',
                                    ),
                                    'important' => 'all',
                                ),
                            ),
                        ),
                        'height'         => array(
                            'css' => array(
                                'main' => implode( ', ',
                                    array(
                                        '%%order_class%% input[type=text]',
                                        '%%order_class%% input[type=email]',
                                        '%%order_class%% textarea',
                                        '%%order_class%% [data-type=checkbox]',
                                        '%%order_class%% [data-type=radio]',
                                        '%%order_class%% [data-type=select]',
                                        '%%order_class%% select',
                                    )
                                ),
                            ),
                        ),
                        'box_shadow' => array(
                            'default' => array(),
                            'product' => array(
                                'label' => esc_html__( 'Default Layout - Box Shadow', 'divi-ajax-filter' ),
                                'css' => array(
                                    'main' => "%%order_class%% .products .product",
                                ),
                                'option_category' => 'layout',
                                'tab_slug'        => 'advanced',
                                'toggle_slug'     => 'product',
                            ),
                        ),
                    );

                    $this->custom_css_fields = array( );

                    $this->help_videos = array( );

                }

                public function get_acf_fields(  ){

                    $acf_fields = array();

                    if ( function_exists( 'acf_get_field_groups' ) ) {
                        $fields_all = get_posts(array(
                            'posts_per_page'   => -1,
                            'post_type'        => 'acf-field',
                            'orderby'          => 'name',
                            'order'            => 'ASC',
                            'post_status'       => 'publish',
                        ));

                        $acf_fields['none'] = 'Please select an ACF field';

                        foreach ( $fields_all as $field ) {

                            $post_parent = $field->post_parent;
                            $post_parent_name = get_the_title($post_parent);
                            $grandparent = wp_get_post_parent_id($post_parent);
                            $grandparent_name = get_the_title($grandparent);

                            $acf_fields[$field->post_name] = $post_parent_name . " > " . $field->post_title . " - " . $grandparent_name;

                        }


                        $field_groups = acf_get_field_groups();
                        foreach ( $field_groups as $group ) {
                            // DO NOT USE here: $fields = acf_get_fields($group['key']);
                            // because it causes repeater field bugs and returns "trashed" fields
                            $fields = get_posts(array(
                                'posts_per_page'   => -1,
                                'post_type'        => 'acf-field',
                                'orderby'          => 'name',
                                'order'            => 'ASC',
                                'suppress_filters' => true, // DO NOT allow WPML to modify the query
                                'post_parent'      => $group['ID'],
                                'post_status'       => 'publish',
                                'update_post_meta_cache' => false
                            ));

                            $acf_fields['none'] = 'Please select an ACF field';

                            foreach ( $fields as $field ) {
                                $acf_fields[$field->post_name] = $field->post_title . " - " . $group['title'];
                            }
                        }
                    }

                    return $acf_fields;
                }

                function get_fields() {

                    global $wpdb;

                    $et_accent_color = et_builder_accent_color();
                    
                    ///////////////////////////////

                    $acf_fields = $this->get_acf_fields();

                    //////////////////////////////

                    $filter_post_type = array(
                        'category'          => 'Category',
                        'tags'    => 'Tags',
                        'search'          => 'Search Text',
                        'taxonomy'          => 'Custom Taxonomy',
                        // 'posttypes'          => 'Post Type Select (Search only)',
                    );

                    if ( function_exists( 'acf_get_field_groups' ) ){
                        $filter_post_type['acf'] = 'Advanced Custom Field (ACF Plugin)';
                    }

                    $productattr = array();

                    if ( class_exists('WooCommerce') ){
                        $filter_post_type['productattr'] = 'Product Attributes';
                        $attribute_taxonomies = wc_get_attribute_taxonomies();
                        $productattr['none'] = 'Please select an Product Attribute';
                        if ( $attribute_taxonomies ) {
                            foreach ( $attribute_taxonomies as $tax ) {
                                $productattr[esc_attr( wc_attribute_taxonomy_name( $tax->attribute_name ) )] = esc_html( $tax->attribute_label );
                            }
                        }
                        $filter_post_type['productprice'] = 'Product Price';

                        $sql = "SELECT COUNT(*) FROM {$wpdb->posts} a JOIN (SELECT * FROM {$wpdb->postmeta} WHERE meta_key='_weight') b ON a.id=b.post_id";
                        $weight_count = $wpdb->get_var($sql);
                        if ( $weight_count > 0 ){
                            $filter_post_type['productweight']  = 'Product Weight';
                        }
                        $filter_post_type['productrating'] = 'Product Rating';
                    }

                    $fields = array(
                        'title' => array(
                            'label'           => esc_html__( 'Admin Filter Name', 'et_builder' ),
                            'type'            => 'text',
                            'description'     => esc_html__( 'Change the name of the filter for admin purposes ONLY, this is just used so you can see what the filter is.', 'et_builder' ),
                            'toggle_slug'     => 'main_content',
                            'dynamic_content' => 'text',
                            'option_category' => 'configuration',
                        ),
                        'filter_post_type' => array(
                            'toggle_slug'       => 'main_content',
                            'label'             => esc_html__( 'What do you want to search/filter?', 'divi-ajax-filter' ),
                            'type'              => 'select',
                            'options'           => $filter_post_type,
                            'default'           => 'category',
                            'affects'         => array(
                                'text_placeholder', 
                                'acf_name',
                                'select_options',
                                'custom_tax_choose',
                                'product_attribute',
                                'attribute_swatch',
                                'post_type_choose',
                                'acf_filter_type',
                            ),
                            'option_category'   => 'configuration',
                            'description'       => esc_html__( 'Choose what you want to search or filter', 'divi-ajax-filter' ),
                        ),
                        'custom_tax_choose' => array(
                            'toggle_slug'       => 'main_content',
                            'label'             => esc_html__( 'Choose Your Taxonomy', 'divi-ajax-filter' ),
                            'type'              => 'select',
                            'options'           => get_taxonomies( array( '_builtin' => FALSE ) ),
                            'option_category'   => 'configuration',
                            'default'           => 'post',
                            'depends_show_if'  => 'taxonomy',
                            'description'       => esc_html__( 'Choose the custom taxonomy that you have made and want to filter', 'divi-ajax-filter' ),
                        ),
                        // 'select_options' => array(
                        //     'label'           => esc_html__( 'Posts to Search', 'et_builder' ),
                        //     'type'            => 'sortable_list',
                        //     'option_category' => 'configuration',
                        //     'depends_show_if' => 'posttypes',
                        //     'toggle_slug'     => 'main_content',
                        //     'description'       => esc_html__( 'Enter in the post types you want by their slug (lowercase no spaces) - we will then find the posts you want the customer to choose from to search', 'divi-ajax-filter' ),
                        // ),
                        'post_type_choose' => array(
                            'toggle_slug'       => 'main_content',
                            'label'             => esc_html__( 'Post Type', 'divi-ajax-filter' ),
                            'type'              => 'select',
                            'options'           => et_get_registered_post_type_options( false, false ),
                            'option_category'   => 'configuration',
                            'default'           => 'post',
                            'description'       => esc_html__( 'Choose the post type that you want to filter', 'divi-ajax-filter' ),
                            'show_if' => [
                                'filter_post_type' => [ 'category', 'tags', 'search', 'taxonomy', 'posttypes', 'acf' ],
                            ],
                        ),
                        'acf_name' => array(
                            'toggle_slug'       => 'main_content',
                            'label'             => esc_html__( 'ACF Name', 'divi-ajax-filter' ),
                            'type'              => 'select',
                            'options'           => $acf_fields,
                            'default'           => 'none',
                            'depends_show_if'   => 'acf',
                            'option_category'   => 'configuration',
                            'description'       => esc_html__( 'Choose the ACF item that you want to filter by. Make sure it belongs to the post type you have/want to filter. It cannot be inside a group or repeater type. ', 'divi-ajax-filter' ),
                        ),
                        'product_attribute' => array(
                            'toggle_slug'       => 'main_content',
                            'label'             => esc_html__( 'Product Attributes', 'divi-ajax-filter' ),
                            'type'              => 'select',
                            'options'           => $productattr,
                            'default'           => 'none',
                            'depends_show_if'   => 'productattr',
                            'option_category'   => 'configuration',
                            'description'       => esc_html__( 'Add the name of the Product Attribute you want to display here', 'divi-ajax-filter' ),
                        ),
                        'attribute_swatch' => array(
                            'toggle_slug'       => 'main_content',
                            'label'             => esc_html__( 'Show Attributes Swatches', 'divi-ajax-filter' ),
                            'type'              => 'yes_no_button',
                            'options'   => array(
                                'on'    => esc_html__( 'Yes', 'divi-ajax-filter' ),
                                'off'   => esc_html__( 'No', 'divi-ajax-filter' ),
                            ),
                            'default'           => 'off',
                            'show_if'      => array(
                                'filter_post_type' => ['productattr', 'productrating']
                            ),
                            'option_category'   => 'configuration',
                            'description'       => esc_html__( 'If you want to display swatches on the filter, check this option. Filter type will be ignored if this option is on.', 'divi-ajax-filter' ),
                        ),
                        'acf_filter_type' => array(
                            'toggle_slug'       => 'main_content',
                            'label'             => esc_html__( 'Filter Type', 'divi-ajax-filter' ),
                            'type'              => 'select',
                            'options'           => array(
                                'select'        => 'Select',
                                'number_range'  => 'Number / Range',
                                'radio'         => 'Checkbox / Radio Buttons'
                            ),
                            'default'           => 'select',
                            'show_if_not'           => array(
                                'filter_post_type' => 'productprice',
                                'filter_post_type' => 'search'
                            ),
                            
                            'affects'         => array(
                                'default_num',
                                'radio_button_background_color',
                                'radio_button_background_color_selected',
                                'radio_button_background_color_selected_text',
                            ),
                            'option_category'   => 'configuration',
                            'description'       => esc_html__( 'Choose how you want the filter to be displayed', 'divi-ajax-filter' ),
                        ),
                        /*'price_filter_type' => array(
                            'toggle_slug'       => 'main_content',
                            'label'             => esc_html__( 'Filter Type', 'divi-filter' ),
                            'type'              => 'select',
                            'depends_show_if'   => 'productprice',
                            'options'           => array(
                                'select'        => 'Select',
                                'number_range'  => 'Number / Range',
                                'radio'         => 'Checkbox / Radio Buttons'
                            ),
                            'default'           => 'number_range',
                            'affects'         => array(
                                'default_num',
                                'radio_style',
                                'radio_select',
                                'radio_button_background_color',
                                'radio_button_background_color_selected',
                                'radio_button_background_color_selected_text',
                                'range_type',
                                'range_step',
                                'range_skin',
                                'range_hide_min_max',
                                'range_hide_from_to',
                                'range_prettify_enabled',
                                'range_prettify_separator',
                                'range_prefix',
                                'range_postfix',
                                'range_has_input'
                            ),
                            'option_category'   => 'configuration',
                            'description'       => esc_html__( 'Choose how you want the filter to be displayed', 'divi-ajax-filter' ),
                        ),*/
                        'acf_filter_width' => array(
                            'toggle_slug'       => 'main_content',
                            'label'             => esc_html__( 'Filter Width', 'divi-ajax-filter' ),
                            'type'              => 'select',
                            'options'           => array(
                                'et_pb_column_4_4'        => 'Full (4/4)',
                                'et_pb_column_3_4'        => 'Three Quarter (3/4)',
                                'et_pb_column_2_3'        => 'Two Thirds (2/3)',
                                'et_pb_column_1_2'        => 'Half (1/2)',
                                'et_pb_column_1_3'        => 'Third (1/3)',
                                'et_pb_column_1_4'        => 'Quarter (1/4)',
                            ),
                            'default'           => 'et_pb_column_4_4',
                            'option_category'   => 'configuration',
                            'description'       => esc_html__( 'Choose the width of the filter', 'divi-ajax-filter' ),
                        ),
                        
                      //   'hide_title' => array(
                      //     'toggle_slug'       => 'main_content',
                      //     'label'             => esc_html__( 'Hide Title', 'divi-ajax-filter' ),
                      //     'type'              => 'yes_no_button',
                      //     'option_category'   => 'configuration',
                      //     'options'           => array(
                      //         'on'  => esc_html__( 'Yes', 'et_builder' ),
                      //         'off' => esc_html__( 'No', 'et_builder' ),
                      //     ),
                      //     'default'   => 'off',
                      //     'description'       => esc_html__( 'If you want to hide the title of the filter, enable this', 'divi-ajax-filter' ),
                      // ),
                        'filter_params' => array(
                          'toggle_slug'       => 'main_content',
                          'label'             => esc_html__( 'Show Filter Parameters', 'divi-ajax-filter' ),
                          'type'              => 'select',
                          'options'           => array(
                            'no'        => 'No',
                            'yes_title'        => 'Yes with title',
                            'yes_no_title'        => 'Yes without title, just the value',
                          ),
                          'default'           => 'no',
                          'option_category'   => 'configuration',
                          'description'       => esc_html__( 'If you want to have the filter selections show up above the archive loop so the customer can see and remove them, enable this.', 'divi-ajax-filter' ),
                        ),
                        'show_label' => array(
                            'label' => esc_html__( 'Show Label', 'divi-ajax-filter' ),
                            'type' => 'yes_no_button',
                            'options_category' => 'configuration',
                            'options' => array(
                                'on' => esc_html__( 'Yes', 'divi-ajax-filter' ),
                                'off' => esc_html__( 'No', 'divi-ajax-filter' ),
                            ),
                            'affects' => array(
                                'custom_label'
                            ),
                            'default' => 'on',
                            'description' => esc_html__( 'Enable or disable the label.', 'divi-ajax-filter' ),
                            'toggle_slug'       => 'main_content',
                        ),
                        'custom_label' => array(
                          'toggle_slug'       => 'main_content',
                            'label'             => esc_html__( 'Custom label (leave blank for default label)', 'divi-ajax-filter' ),
                            'type'              => 'text',
                            'option_category'   => 'configuration',
                            'depends_show_if'   => 'on',
                            'description'       => esc_html__( 'Add a custom label here or leave it blank to get the default label.', 'divi-ajax-filter' ),
                        ),
                        // TEXT FILTER
                        'text_placeholder' => array(
                            'toggle_slug'       => 'text_filter',
                            'label'             => esc_html__( 'Placeholder Text', 'divi-ajax-filter' ),
                            'type'              => 'text',
                            'option_category'   => 'configuration',
                            'depends_show_if'   => 'search',
                            'description'       => esc_html__( 'Add the placeholder for the text input.', 'divi-ajax-filter' ),
                        ),
                        // Select FILTER
                        'select_placeholder' => array(
                            'toggle_slug'       => 'select_filter',
                            'label'             => esc_html__( 'First Option Text', 'divi-ajax-filter' ),
                            'type'              => 'text',
                            'option_category'   => 'configuration',
                            'default'   => '-- Select Option --',
                            'description'       => esc_html__( 'Choose what the first option of the select option will be.', 'divi-ajax-filter' ),
                        ),
                        // LAYOUT OPTIONS
                        'radio_select' => array(
                            'toggle_slug'       => 'radio_filter',
                            'label'             => esc_html__( 'Checkbox / Radio Button Functionality', 'divi-ajax-filter' ),
                            'type'              => 'select',
                            'options'           => array(
                                'radio'       => esc_html__( 'Choose One', 'divi-ajax-filter' ),
                                'checkbox'       => esc_html__( 'Multiple Select', 'divi-ajax-filter' ),
                            ),
                            'option_category'   => 'configuration',
                            'default'           => 'radio',
                            'description'       => esc_html__( 'Choose the style of your radio buttons', 'divi-ajax-filter' ),
                        ),
                        'radio_style' => array(
                            'toggle_slug'       => 'radio_filter',
                            'label'             => esc_html__( 'Checkbox / Radio Style', 'divi-ajax-filter' ),
                            'type'              => 'select',
                            'options'           => array(
                                'normal'       => esc_html__( 'Normal', 'divi-ajax-filter' ),
                                'tick_box'       => esc_html__( 'Tick Boxes', 'divi-ajax-filter' ),
                                'buttons'       => esc_html__( 'Buttons', 'divi-ajax-filter' ),
                                'select'       => esc_html__( 'Select Drop Down', 'divi-ajax-filter' ),
                            ),
                            'option_category'   => 'configuration',
                            'default'           => 'normal',
                            'description'       => esc_html__( 'Choose the style of your radio buttons', 'divi-ajax-filter' ),
                        ),
                        // 'checkbox_color' => array(
                        //   'label'             => esc_html__( 'Checkbox Checked Color', 'divi-bodyshop-woocommerce' ),
                        //   'type'              => 'color-alpha',
                        //   'custom_color'      => true,
                        //   'default'  => et_get_option( 'accent_color', '#2ea3f2' ),
                        //   'toggle_slug'       => 'radio_filter',
                        //   'option_category'   => 'configuration',
                        // ),
                        'radio_inline' => array(
                            'toggle_slug'       => 'radio_filter',
                            'label'             => esc_html__( 'Display Checkbox / Radio Inline?', 'divi-ajax-filter' ),
                            'type'              => 'yes_no_button',
                            'option_category'   => 'configuration',
                            'options'           => array(
                                'on'  => esc_html__( 'Yes', 'et_builder' ),
                                'off' => esc_html__( 'No', 'et_builder' ),
                            ),
                            'default'   => 'off',
                            'affects'         => array(
                                'radio_gap',
                            ),
                            'description'       => esc_html__( 'Choose if you want the checkboxes to be inline or on their own line', 'divi-ajax-filter' ),
                        ),
                        
                      'radio_gap' => array(
                        'toggle_slug'       => 'radio_filter',
                        'label'             => esc_html__( 'Gap Between each item', 'divi-ajax-filter' ),
                        'type'              => 'range',
                        'option_category'   => 'configuration',
                        'default'   => '20',
                        'depends_show_if'   => 'on',
                        'description'       => esc_html__( 'Specify the gap you want between each item that is inline.', 'divi-ajax-filter' ),
                      ),
                        'radio_all_text' => array(
                            'toggle_slug'       => 'radio_filter',
                            'label'             => esc_html__( 'All Label Text', 'divi-ajax-filter' ),
                            'type'              => 'text',
                            'option_category'   => 'configuration',
                            'default'   => 'All',
                            'description'       => esc_html__( 'Define the text you want to be shown as the "all" checkbox to show all the values.', 'divi-ajax-filter' ),
                        ),
                        'hide_radio_all'    => array(
                            'toggle_slug'       => 'radio_filter',
                            'label'             => esc_html__( 'Hide All option', 'divi-ajax-filter' ),
                            'type'              => 'yes_no_button',
                            'option_category'   => 'configuration',
                            'options'           => array(
                                'on'            => esc_html__( 'Yes', 'et_builder'),
                                'off'           => esc_html__( 'No', 'et_builder'),
                            ),
                            'default'           => 'off',
                            'description'       => esc_html__( 'Define the text you want to be shown as the "all" checkbox to show all the values.', 'divi-ajax-filter' ),  
                        ),
                        'radio_show_count' => array(
                            'toggle_slug'       => 'radio_filter',
                            'label'             => esc_html__( 'Display filter count?', 'divi-ajax-filter' ),
                            'type'              => 'yes_no_button',
                            'option_category'   => 'configuration',
                            'options'           => array(
                                'on'  => esc_html__( 'Yes', 'et_builder' ),
                                'off' => esc_html__( 'No', 'et_builder' ),
                            ),
                            'default'   => 'on',
                            'depends_show_if'   => 'radio',
                            'description'       => esc_html__( 'Choose if you want to show the filter count for every option.', 'divi-ajax-filter' ),
                        ),
                        'radio_show_empty' => array(
                            'toggle_slug'       => 'radio_filter',
                            'label'             => esc_html__( 'Display empty filter option?', 'divi-ajax-filter' ),
                            'type'              => 'yes_no_button',
                            'option_category'   => 'configuration',
                            'options'           => array(
                                'on'  => esc_html__( 'Yes', 'et_builder' ),
                                'off' => esc_html__( 'No', 'et_builder' ),
                            ),
                            'default'   => 'off',
                            'depends_show_if'   => 'radio',
                            'description'       => esc_html__( 'Choose if you want to show empty filter options.', 'divi-ajax-filter' ),
                        ),
                        'radio_button_background_color' => array(
                            'label'             => esc_html__( 'Checkbox / Radio Button Background Color', 'et_builder' ),
                            'type'              => 'color-alpha',
                            'tab_slug'          => 'advanced',
                            'depends_show_if'   => 'radio',
                            'toggle_slug'       => 'filter_style',
                            'description'       => esc_html__( 'This will change the fill color for the checkbox or radio button background.', 'et_builder' ),
                            'default'           => "#ffffff"
                        ),
                        'radio_button_background_color_selected' => array(
                            'label'             => esc_html__( 'Checkbox / Radio Button Background Color Selected', 'et_builder' ),
                            'type'              => 'color-alpha',
                            'tab_slug'          => 'advanced',
                            'depends_show_if'   => 'radio',
                            'toggle_slug'       => 'filter_style',
                            'description'       => esc_html__( 'This will change the fill color for the checkbox or radio button background when selected/clicked.', 'et_builder' ),
                            'default'           => $et_accent_color
                        ),
                        'radio_button_background_color_selected_text' => array(
                            'label'             => esc_html__( 'Checkbox / Radio Button Font Color Selected', 'et_builder' ),
                            'type'              => 'color-alpha',
                            'tab_slug'          => 'advanced',
                            'depends_show_if'   => 'radio',
                            'toggle_slug'       => 'filter_style',
                            'description'       => esc_html__( 'This will change the fill color for the checkbox or radio button text when selected/clicked.', 'et_builder' ),
                            'default'           => '#ffffff'
                        ),




                        'swatch_style' => array(
                          'toggle_slug'       => 'swatch_style_tab',
                          'label'             => esc_html__( 'Swatch Style', 'divi-ajax-filter' ),
                          'type'              => 'select',
                          'options'           => array(
                              'circle'        => 'Circle',
                              'square'        => 'Square'
                          ),
                          'default'           => 'circle',
                          'affects'         => array(
                              'default_num',
                          ),
                          'description'       => esc_html__( 'When using a color, image or label swatch - choose the style here.', 'divi-ajax-filter' ),
                      ),
                      'swatch_width' => array(
                        'toggle_slug'       => 'swatch_style_tab',
                        'label'             => esc_html__( 'Swatch Width', 'divi-ajax-filter' ),
                        'type'              => 'range',
                        'option_category'   => 'configuration',
                        'default'   => '40',
                        'description'       => esc_html__( 'Specify the swatch width.', 'divi-ajax-filter' ),
                      ),
                      'swatch_height' => array(
                        'toggle_slug'       => 'swatch_style_tab',
                        'label'             => esc_html__( 'Swatch Height', 'divi-ajax-filter' ),
                        'type'              => 'range',
                        'option_category'   => 'configuration',
                        'default'   => '40',
                        'description'       => esc_html__( 'Specify the swatch height.', 'divi-ajax-filter' ),
                      ),
                      'swatch_bg_color' => array(
                        'label'             => esc_html__( 'Swatch Background Color', 'et_builder' ),
                        'type'              => 'color-alpha',
                        'toggle_slug'       => 'swatch_style_tab',
                        'description'       => esc_html__( 'Change the color of the swatch background.', 'et_builder' ),
                        'default'           => "#ffffff"
                      ),
                      'swatch_border_color' => array(
                        'label'             => esc_html__( 'Swatch Border Color', 'et_builder' ),
                        'type'              => 'color-alpha',
                        'toggle_slug'       => 'swatch_style_tab',
                        'description'       => esc_html__( 'Change the color of the swatch border.', 'et_builder' ),
                        'default'           => "#000000"
                      ),
                      'swatch_bg_color_active' => array(
                        'label'             => esc_html__( 'Active Swatch Background Color', 'et_builder' ),
                        'type'              => 'color-alpha',
                        'toggle_slug'       => 'swatch_style_tab',
                        'description'       => esc_html__( 'Change the color of the swatch background when active.', 'et_builder' ),
                        'default'           => "#ffffff"
                      ),
                      'swatch_border_color_active' => array(
                        'label'             => esc_html__( 'Active Swatch Border Color', 'et_builder' ),
                        'type'              => 'color-alpha',
                        'toggle_slug'       => 'swatch_style_tab',
                        'description'       => esc_html__( 'Change the color of the swatch border when active.', 'et_builder' ),
                        'default'           => "#00b8ff"
                      ),
                      'swatch_border_width' => array(
                        'toggle_slug'       => 'swatch_style_tab',
                        'label'             => esc_html__( 'Swatch Border Width', 'divi-ajax-filter' ),
                        'type'              => 'range',
                        'option_category'   => 'configuration',
                        'default'   => '2',
                        'description'       => esc_html__( 'Change the width of the border.', 'divi-ajax-filter' ),
                      ),
                      'cat_tag_hide_empty' => array(
                        'toggle_slug'       => 'tags_filter',
                        'label'             => esc_html__( 'Hide Empty Options?', 'divi-ajax-filter' ),
                        'type'              => 'yes_no_button',
                        'option_category'   => 'configuration',
                        'options'           => array(
                          'on'  => esc_html__( 'Yes', 'et_builder' ),
                          'off' => esc_html__( 'No', 'et_builder' ),
                        ),
                        'default'   => 'off',
                        'description'       => esc_html__( 'Enable this to hide the empty values (ones that have no posts assigned to them)', 'divi-ajax-filter' ),
                      ),
                      'only_show_avail' => array(
                        'toggle_slug'       => 'tags_filter',
                        'label'             => esc_html__( 'Only show available Options?', 'divi-ajax-filter' ),
                        'type'              => 'yes_no_button',
                        'option_category'   => 'configuration',
                        'options'           => array(
                          'on'  => esc_html__( 'Yes', 'et_builder' ),
                          'off' => esc_html__( 'No', 'et_builder' ),
                        ),
                        'default'   => 'off',
                        'description'       => esc_html__( 'If you want to show only the categories, tags or custom taxonomies that are linked to the posts queried on the page - enable this', 'divi-ajax-filter' ),
                      ),
                      // 'tags_style' => array(
                      //       'toggle_slug'       => 'tags_filter',
                      //       'label'             => esc_html__( 'Category / Tags Appearance Style', 'divi-ajax-filter' ),
                      //       'type'              => 'select',
                      //       'options'           => array(
                      //       'select'       => esc_html__( 'Select', 'divi-ajax-filter' ),
                      //       'radio'       => esc_html__( 'Radio/Checkboxes', 'divi-ajax-filter' ),
                      //       ),
                      //       'option_category'   => 'configuration',
                      //       'default'           => 'select',
                      //       'description'       => esc_html__( 'Choose the appearance style of the slider', 'divi-ajax-filter' ),
                      //   ),
                        // RANGE SETTINGS

                        // Number Range FILTER
                        // 'default_num' => array(
                        // 'toggle_slug'       => 'range',
                        // 'label'             => esc_html__( 'Default Number', 'divi-ajax-filter' ),
                        // 'type'              => 'text',
                        // 'option_category'   => 'configuration',
                        // 'default'   => '3',
                        // 'depends_show_if'   => 'number_range',
                        // 'description'       => esc_html__( 'Choose the default image.', 'divi-ajax-filter' ),
                        // ),
                        // 'range_min' => array(
                        // 'toggle_slug'       => 'range_filter',
                        // 'label'             => esc_html__( 'Rumber Range Min', 'divi-ajax-filter' ),
                        // 'type'              => 'text',
                        // 'option_category'   => 'configuration',
                        // 'default'   => '1',
                        // 'depends_show_if'   => 'number_range',
                        // 'description'       => esc_html__( 'Add the min price for the slider.', 'divi-ajax-filter' ),
                        // ),

                        // 'range_type' => array(
                        // 'toggle_slug'       => 'range_filter',
                        //  'label'             => esc_html__( 'Range Type', 'divi-ajax-filter' ),
                        //  'type'              => 'select',
                        //  'options'           => array(
                        //  'single'       => esc_html__( 'Single', 'divi-ajax-filter' ),
                        //  'double'       => esc_html__( 'Double', 'divi-ajax-filter' ),
                        //  ),
                        //  'option_category'   => 'configuration',
                        //  'default'           => 'single',
                        //  'affects'         => array(
                        //    'range_from',
                        //    'range_to'
                        //  ),
                        //  'depends_show_if'   => 'number_range',
                        //  'description'       => esc_html__( 'Choose the type of range filter you want', 'divi-ajax-filter' ),
                        // ),
                        'range_from' => array(
                            'toggle_slug'       => 'range_filter',
                            'label'             => esc_html__( 'Range From Number', 'divi-ajax-filter' ),
                            'type'              => 'text',
                            'option_category'   => 'configuration',
                            'default'   => '100',
                            'depends_show_if'   => 'double',
                            'description'       => esc_html__( 'Choose the default from number.', 'divi-ajax-filter' ),
                        ),
                        'range_to' => array(
                            'toggle_slug'       => 'range_filter',
                            'label'             => esc_html__( 'Range To Number', 'divi-ajax-filter' ),
                            'type'              => 'text',
                            'option_category'   => 'configuration',
                            'default'   => '800',
                            'depends_show_if'   => 'double',
                            'description'       => esc_html__( 'Choose the default to number.', 'divi-ajax-filter' ),
                        ),
                        'range_step' => array(
                            'toggle_slug'       => 'range_filter',
                            'label'             => esc_html__( 'Number / Range Step', 'divi-ajax-filter' ),
                            'type'              => 'text',
                            'option_category'   => 'configuration',
                            'default'   => '1',
                            'description'       => esc_html__( 'Choose the number it steps up.', 'divi-ajax-filter' ),
                        ),
                        'range_skin' => array(
                            'toggle_slug'       => 'range_filter',
                            'label'             => esc_html__( 'Number / Range Appearance Style', 'divi-ajax-filter' ),
                            'type'              => 'select',
                            'options'           => array(
                                'flat'       => esc_html__( 'Flat', 'divi-ajax-filter' ),
                                'big'       => esc_html__( 'Big', 'divi-ajax-filter' ),
                                'modern'       => esc_html__( 'Modern', 'divi-ajax-filter' ),
                                'sharp'       => esc_html__( 'Sharp', 'divi-ajax-filter' ),
                                'round'       => esc_html__( 'Round', 'divi-ajax-filter' ),
                                'square'       => esc_html__( 'Square', 'divi-ajax-filter' ),
                            ),
                            'option_category'   => 'configuration',
                            'default'           => 'flat',
                            'description'       => esc_html__( 'Choose the appearance style of the slider', 'divi-ajax-filter' ),
                        ),
                        'range_prim_color' => array(
                            'label'             => esc_html__( 'Range Primary Color', 'divi-bodyshop-woocommerce' ),
                            'type'              => 'color-alpha',
                            'custom_color'      => true,
                            'default' => et_get_option( 'accent_color', '#2ea3f2' ),
                            'toggle_slug'       => 'range_filter',
                            'option_category'   => 'configuration',
                        ),
                        'range_sec_color' => array(
                            'label'             => esc_html__( 'Range Secondary Color', 'divi-bodyshop-woocommerce' ),
                            'type'              => 'color-alpha',
                            'custom_color'      => true,
                            'default' => '#efefef',
                            'toggle_slug'       => 'range_filter',
                            'option_category'   => 'configuration',
                        ),
                        'range_hide_min_max' => array(
                            'toggle_slug'       => 'range_filter',
                            'label'             => esc_html__( 'Range Hide Min and Max Labels', 'divi-ajax-filter' ),
                            'type'              => 'yes_no_button',
                            'option_category'   => 'configuration',
                            'options'           => array(
                                'on'  => esc_html__( 'Yes', 'et_builder' ),
                                'off' => esc_html__( 'No', 'et_builder' ),
                            ),
                            'default'   => 'off',
                            'depends_show_if'   => 'number_range'
                        ),
                        'range_hide_from_to' => array(
                            'toggle_slug'       => 'range_filter',
                            'label'             => esc_html__( 'Range Hide From and To Labels', 'divi-ajax-filter' ),
                            'type'              => 'yes_no_button',
                            'option_category'   => 'configuration',
                            'options'           => array(
                                'on'  => esc_html__( 'Yes', 'et_builder' ),
                                'off' => esc_html__( 'No', 'et_builder' ),
                            ),
                            'default'   => 'off',
                            'depends_show_if'   => 'number_range'
                        ),
                        'range_prettify_enabled' => array(
                            'toggle_slug'       => 'range_filter',
                            'label'             => esc_html__( 'Number / Range Prettify', 'divi-ajax-filter' ),
                            'type'              => 'yes_no_button',
                            'option_category'   => 'configuration',
                            'options'           => array(
                                'on'  => esc_html__( 'Yes', 'et_builder' ),
                                'off' => esc_html__( 'No', 'et_builder' ),
                            ),
                            'default'   => 'on',
                            'description'       => esc_html__( 'Enable this if you want to pretify your text, improve readibility of long numbers. 10000000 → 10 000 000', 'divi-ajax-filter' ),
                        ),
                        'range_prettify_separator' => array(
                            'toggle_slug'       => 'range_filter',
                            'label'             => esc_html__( 'Number / Range Prettify Separator', 'divi-ajax-filter' ),
                            'type'              => 'text',
                            'option_category'   => 'configuration',
                            'default'   => ' ',
                            'description'       => esc_html__( 'Set up your own separator for long numbers. 10 000, 10.000, 10-000 etc.', 'divi-ajax-filter' ),
                        ),
                        'range_prefix' => array(
                            'toggle_slug'       => 'range_filter',
                            'label'             => esc_html__( 'Number / Range Before Text', 'divi-ajax-filter' ),
                            'type'              => 'text',
                            'option_category'   => 'configuration',
                            'default'   => '',
                            'description'       => esc_html__( 'Set the text for values. Will be set up right before the number: $100', 'divi-ajax-filter' ),
                        ),
                        'range_postfix' => array(
                            'toggle_slug'       => 'range_filter',
                            'label'             => esc_html__( 'Number / Range After Text', 'divi-ajax-filter' ),
                            'type'              => 'text',
                            'option_category'   => 'configuration',
                            'default'   => '',
                            'description'       => esc_html__( 'Set the text for values. Will be set up right after the number: 100k', 'divi-ajax-filter' ),
                        )
                    );

                    return $fields;
                }

                function render( $attrs, $content = null, $render_slug ) {

                    if (is_admin()) {
                      return;
                    }

                    $post_type_choose   = $this->props['post_type_choose'];
                    $filter_post_type   = $this->props['filter_post_type'];
                    $custom_tax_choose   = $this->props['custom_tax_choose'];
                    
                    $acf_name           = $this->props['acf_name'];
                    $acf_filter_type    = $this->props['acf_filter_type'];
                    $acf_filter_width   = $this->props['acf_filter_width'];
                    $attribute_swatch   = $this->props['attribute_swatch'];
                    $show_label         = $this->props['show_label'];
                    $text_placeholder   = $this->props['text_placeholder'];
                    $select_placeholder = $this->props['select_placeholder'];

                    $product_attribute  = $this->props['product_attribute'];

                    // $default_num                         = $this->props['default_num'];
                    $radio_style        = $this->props['radio_style'];
                    $radio_select       = $this->props['radio_select'];
                    $radio_all_text     = $this->props['radio_all_text'];
                    $radio_all_hide     = $this->props['hide_radio_all'];

                    // $checkbox_color                         = $this->props['checkbox_color'];
                    $range_prim_color                               = $this->props['range_prim_color'];
                    $range_sec_color                                = $this->props['range_sec_color'];
                    $radio_button_background_color                  = $this->props['radio_button_background_color'];
                    $radio_button_background_color_selected         = $this->props['radio_button_background_color_selected'];
                    $radio_button_background_color_selected_text    = $this->props['radio_button_background_color_selected_text'];

                    // $range_type                         = $this->props['range_type'];
                    $range_from                 = $this->props['range_from'];
                    $range_to                   = $this->props['range_to'];
                    $range_step                 = $this->props['range_step'];
                    $range_skin                 = $this->props['range_skin'];
                    $range_hide_min_max         = $this->props['range_hide_min_max'];
                    $range_hide_from_to         = $this->props['range_hide_from_to'];
                    $range_prettify_enabled     = $this->props['range_prettify_enabled'];
                    $range_prettify_separator   = $this->props['range_prettify_separator'];
                    $range_prefix               = $this->props['range_prefix'];
                    $range_postfix              = $this->props['range_postfix'];

                    $select_options             = !empty($this->props['select_options'])?$this->props['select_options']:'';
                    $custom_label               = $this->props['custom_label'];
                    // $tags_style                 = $this->props['tags_style'];
                    $radio_inline               = $this->props['radio_inline'];
                    $radio_gap               = $this->props['radio_gap'];
                    $radio_show_count           = $this->props['radio_show_count'];
                    $radio_show_empty           = $this->props['radio_show_empty'];
                    

                    $swatch_style               = $this->props['swatch_style'];
                    $swatch_width               = $this->props['swatch_width'];
                    $swatch_height              = $this->props['swatch_height'];
                    $swatch_bg_color            = $this->props['swatch_bg_color'];
                    $swatch_border_color        = $this->props['swatch_border_color'];
                    $swatch_bg_color_active     = $this->props['swatch_bg_color_active'];
                    $swatch_border_color_active = $this->props['swatch_border_color_active'];
                    $swatch_border_width        = $this->props['swatch_border_width'];

                    $hide_title        = !empty($this->props['hide_title'])?$this->props['hide_title']:'';

                    $cat_tag_hide_empty           = $this->props['cat_tag_hide_empty'];
                    
                    $only_show_avail           = $this->props['only_show_avail'];
                
                    if ($cat_tag_hide_empty == "on") {
                      $cat_tag_hide_empty_dis = true;
                    } else {
                      $cat_tag_hide_empty_dis = false;
                    }

                    // SWATCH STYLE
                    ET_Builder_Element::set_style( $render_slug, array(
                      'selector' => '%%order_class%% .divi-swatch .et_pb_contact_field_radio label:not([data-value="all"])',
                      'declaration' => "
                      width: {$swatch_width}px;
                      height: {$swatch_height}px;
                      background-color: {$swatch_bg_color};
                      border-color: {$swatch_border_color};
                      border-width: {$swatch_border_width}px;
                      "
                      )
                    );
                    ET_Builder_Element::set_style( $render_slug, array(
                      'selector' => '%%order_class%% .divi-swatch-full .et_pb_contact_field_radio label:not([data-value="all"])',
                      'declaration' => "
                      width: 100%!important;
                      height: auto!important;
                      border:none!important;
                      background:none!important;
                      "
                      )
                    );
                    ET_Builder_Element::set_style( $render_slug, array(
                      'selector' => '%%order_class%% .divi-swatch-full .et_pb_contact_field_radio label:not([data-value="all"]) .star-rating',
                      'declaration' => "
                      float: left;
                      "
                      )
                    );
                    ET_Builder_Element::set_style( $render_slug, array(
                      'selector' => '%%order_class%% .divi-swatch .et_pb_contact_field_radio input:checked ~ label',
                      'declaration' => "
                      background-color: {$swatch_bg_color_active};
                      border-color: {$swatch_border_color_active};
                      "
                      )
                    );

                    ET_Builder_Element::set_style( $render_slug, array(
                      'selector' => '%%order_class%% .divi-swatch-full .et_pb_contact_field_radio input:checked ~ label',
                      'declaration' => "
                      background-color: none;
                      "
                      )
                    );

                    ET_Builder_Element::set_style( $render_slug, array(
                      'selector' => '%%order_class%% .divi-swatch .et_pb_contact_field_radio label .star-rating span:before',
                      'declaration' => "
                      color: {$swatch_bg_color};
                      "
                      )
                    );

                    ET_Builder_Element::set_style( $render_slug, array(
                      'selector' => '%%order_class%% .divi-swatch .et_pb_contact_field_radio input:checked ~ label .star-rating span:before',
                      'declaration' => "
                      color: {$swatch_bg_color_active};
                      "
                      )
                    );
                    // END SWATCH STYLE
             
                    
                    $filter_params           = $this->props['filter_params'];
                    if ($filter_params !== "no") {
                      $this->add_classname( 'filter_params_' . $filter_params);
                      $this->add_classname( 'filter_params');
                    } else {
                      $this->add_classname( 'no_filter_params');
                    }
                    
                    if ($hide_title == "on") {
                      $this->add_classname( 'hide_title');
                    }

                    if ($radio_inline == "on") {
                      $this->add_classname( 'inline_checkboxes');
                      self::set_style( $render_slug,
                        array(
                          'selector'    => '%%order_class%%.inline_checkboxes .divi-filter-item>*',
                          'declaration' => sprintf( 'margin-right: %1$spx;', esc_attr( $radio_gap ))
                        )                        
                      );
                      self::set_style( $render_slug,
                        array(
                          'selector'    => '%%order_class%%.inline_checkboxes .divi-filter-item>*:nth-last-child(1)',
                          'declaration' => 'margin-right: 0 !important;',
                        )
                      );
                    }

                    $num = mt_rand(100000,999999);
                    $css_class = $render_slug . "_" . $num;

                    if ($range_hide_min_max == "off") {$range_hide_min_max = "false";} else {$range_hide_min_max = "true";};
                    if ($range_hide_from_to == "off") {$range_hide_from_to = "false";} else {$range_hide_from_to = "true";};
                    if ($range_prettify_enabled == "off") {$range_prettify_enabled = "false";} else {$range_prettify_enabled = "true";};

                    $this->add_classname(
                      array(
                        $acf_filter_width,
                        'et_pb_column',
                      )
                    );

                    if ( ! empty( $radio_button_background_color ) ) {
                      self::set_style( $render_slug,
                      array(
                        'selector'    => '%%order_class%% .divi-radio-buttons .et_pb_contact_field_radio label',
                        'declaration' => sprintf( 'background-color: %1$s;', esc_attr( $radio_button_background_color ) ),
                      )
                    );
                  }

                  // TODO: add color for simple radiio
                  if ( ! empty( $radio_button_background_color_selected ) ) {
                    self::set_style( $render_slug,
                    array(
                      'selector'    => '%%order_class%% .divi-radio-buttons .et_pb_contact_field_radio input:checked+label, %%order_class%% .divi-radio-tick_box input:checked ~ .checkmark',
                      'declaration' => sprintf( 'background-color: %1$s;', esc_attr( $radio_button_background_color_selected ) ),
                    )
                  );
                }


                if ($show_label == "off") {
                  $label_css = "divi-hide";
                } else {
                  $label_css = "";
                }

                if ($acf_filter_width == "et_pb_column_4_4") {
                  $acf_filter_width_num = "100";
                } else if ($acf_filter_width == "et_pb_column_3_4") {
                  $acf_filter_width_num = "75";
                } else if ($acf_filter_width == "et_pb_column_2_3") {
                  $acf_filter_width_num = "60";
                } else if ($acf_filter_width == "et_pb_column_1_2") {
                  $acf_filter_width_num = "50";
                } else if ($acf_filter_width == "et_pb_column_1_3") {
                  $acf_filter_width_num = "40";
                } else if ($acf_filter_width == "et_pb_column_1_4") {
                  $acf_filter_width_num = "25";
                } else {

                }

                //////////////////////////////////////////////////////////////////////

                ob_start();

                global $wp_query;
?>
                <div class="search_filter_cont <?php echo $css_class ?>" data-count="<?php echo $acf_filter_width_num ?>">
                <?php
                
               // if show available is true

                  
                  // 1) First we get the term query
                  // if ($filter_post_type == "category") { 
                  //   if ( $post_type_choose == 'product' ){ // if products
                  //     $term_query = new WP_Term_Query( array( 'taxonomy' => 'product_cat' ) );
                  //   } else if ($post_type_choose == 'post') { // else if posts
                  //     $term_query = new WP_Term_Query( array( 'taxonomy' => 'category' ) );
                  //   }else { // else everything else
                  //     $ending = "_category";
                  //     $cat_key = $post_type_choose . $ending;
                  //     $term_query = new WP_Term_Query( array( 'taxonomy' => $cat_key ) );
                  //   }
                  // } else if ($filter_post_type == "tags") { 
                  //   $ending = "_tag";
                  //   $cat_key = $post_type_choose . $ending;
                  //   $term_query = new WP_Term_Query( array( 'taxonomy' => $cat_key ) );
                  // } else if ( $filter_post_type == "taxonomy" ) { 
                  //   $term_query = new WP_Term_Query( array( 'taxonomy' => $custom_tax_choose ) );
                  // }


                if ( $filter_post_type == "category" ) {

                  // GET AVAILABLE CATEGORIES
                  // 1) First we get the term query
                  if ($only_show_avail == "on") {
                    global $wp_query;
                    $ID_array = $wp_query->posts;
                    $avail_terms = '';
                    foreach($ID_array as $item) { // Then for each ID - we then look for the category
                      if ($post_type_choose == 'post') {
                        $terms_post = get_the_category( $item->ID );
                      } else if ( $post_type_choose == 'product' ) {
                        $terms_post = get_the_terms( $item->ID , 'product_cat' );
                      } else {
                        $cat_key = $post_type_choose . '_category';
                        if ( !empty( $cpt_taxonomies ) && in_array( 'category', $cpt_taxonomies ) ){
                          $terms_post = get_the_category( $item->ID );
                        } else {
                          $terms_post = get_the_terms( $item->ID , $cat_key );
                        }
                      }
                      // 3) for each category we then add the ID to a string
                      if (is_array($terms_post)) {
                        foreach ($terms_post as $term_cat) { 
                          $term_cat_id = $term_cat->term_id; 
                          $avail_terms .= $term_cat_id.",";
                        }
                      }
                    }
                    $avail_terms = implode(',',array_unique(explode(',', $avail_terms)));
                  } else {
                    $avail_terms = '';
                  }
                  // GET AVAILABLE CATEGORIES


                    if ($custom_label !== '') {
                        $custom_label_final = $custom_label;
                    } else {
                        $custom_label_final = 'Categories';
                    }

                    if ($post_type_choose == 'post') {
                        $cat_key = 'category';
                        $get_categories = get_categories(array('hide_empty' => $cat_tag_hide_empty_dis, 'include' => $avail_terms));
                    } else if ( $post_type_choose == 'product' ) {
                        $cat_key = 'product_cat';
                        $get_categories = get_terms( $cat_key, array('hide_empty' => $cat_tag_hide_empty_dis, 'include' => $avail_terms) );
                    } else {
                        $cpt_taxonomies = get_object_taxonomies( $post_type_choose );

                        if ( !empty( $cpt_taxonomies ) && in_array( 'category', $cpt_taxonomies ) ){
                            $cat_key = 'category';
                            $get_categories = get_categories(array('hide_empty' => $cat_tag_hide_empty_dis, 'include' => $avail_terms));
                        }else{
                            $ending = "_category";
                            $cat_key = $post_type_choose . $ending;
                            $get_categories = get_terms( $cat_key, array('hide_empty' => $cat_tag_hide_empty_dis, 'include' => $avail_terms) );
                        }
                    }

                    $queryvar = !empty($_GET[$cat_key])?$_GET[$cat_key]:(get_query_var( $cat_key )?get_query_var( $cat_key):'' );

                    if ($acf_filter_type == "select") {
?>
                      <p class="et_pb_contact_field_options_title <?php echo $label_css ?>"><?php echo esc_html__( $custom_label_final, 'divi-ajax-filter' ); ?></p>
                      <p class="et_pb_contact_field divi-filter-item" data-type="select" data-filtertype="radio" name="categories" data-field_type="select">
                        <select id="<?php echo $cat_key . '_' . $num; ?>" class="divi-acf et_pb_contact_select" data-filtertype="customcategory" data-field_type="select" name="<?php echo $cat_key ?>">
                          <option value="" <?php selected( $queryvar, ''); ?>><?php echo $select_placeholder ?></option>
                          <?php
                          if ( $get_categories ) :
                            foreach ( $get_categories as $cat ) :
                              ?>
                              <option id="<?php echo $cat->term_id . '_' . $num; ?>" value="<?php echo $cat->slug; ?>" <?php selected( $queryvar, $cat->slug); ?>><?php echo $cat->name; ?></option>
                              <?php
                            endforeach;
                          endif;
                          ?>
                        </select>
                      </p>
                      <?php
                    } else {

                        if ($radio_select == "checkbox") {
                            $checkboxtype = "divi-checkboxmulti";
                        } else {
                            $checkboxtype = "divi-checkboxsingle";
                        }
                        $queryvar = explode(',', $queryvar);
?>
                      <p class="et_pb_contact_field" data-type="radio" data-filtertype="radio">
                        <div class="et_pb_contact_field_options_wrapper divi-radio-<?php echo $radio_style ?>">
                          <span class="et_pb_contact_field_options_title <?php echo $label_css ?>"><?php echo esc_html__( $custom_label_final, 'divi-ajax-filter' ); ?></span>
                          <div class="et_pb_contact_field_options_list divi-filter-item <?php echo $checkboxtype ?>" data-filter-count="<?php echo $radio_show_count;?>" data-show-empty="<?php echo $radio_show_empty;?>" data-filter-option="<?php echo $cat_key;?>">
                            <?php if ( ( $radio_select != 'checkbox') && ($radio_all_hide != 'on') && $only_show_avail == "off" ) { ?>
                            <span class="et_pb_contact_field_radio">
                              <input type="<?php echo $radio_select ?>" id="<?php echo $cat_key; ?>_all_<?php echo $num;?>" class="divi-acf input" data-filtertype="<?php echo $checkboxtype ?>" value="" name="<?php echo $cat_key;?>" data-required_mark="required" data-field_type="radio" data-original_id="radio" <?php echo (sizeof($queryvar) == 1 && $queryvar[0] =='' )?'checked="checked"':'';?>>
                              <?php if ($radio_style == "tick_box") { echo '<span class="checkmark"></span>'; }?>
                              <label class="radio-label" data-value="all" for="<?php echo $cat_key; ?>_all_<?php echo $num;?>" title="All"><i></i><?php echo esc_html__( $radio_all_text, 'divi-ajax-filter' ); ?></label>
                            </span>
                            <?php } ?>
                            <?php
                            if ( $get_categories ) :
                              foreach ( $get_categories as $cat ) :
                                ?>
                                <span class="et_pb_contact_field_radio">
                                  <input type="<?php echo $radio_select ?>" id="<?php echo $cat->term_id; ?>_<?php echo $cat->slug . '_' . $num; ?>" class="divi-acf input" data-filtertype="<?php echo $checkboxtype ?>" value="<?php echo $cat->slug ?>" name="<?php echo $cat_key;?>" data-required_mark="required" data-field_type="radio" <?php echo in_array($cat->slug, $queryvar)?'checked':'';?> data-original_id="radio">
                                  <?php if ($radio_style == "tick_box") { echo '<span class="checkmark"></span>'; } else {} ?>
                                  <label class="radio-label" data-value="<?php echo $cat->slug ?>" for="<?php echo $cat->term_id; ?>_<?php echo $cat->slug . '_' . $num; ?>" title="<?php echo $cat->name;?>"><i></i><?php echo $cat->name; ?></label>
                                </span>
                                <?php
                              endforeach;
                            endif;
                            ?>
                          </div>
                        </div>
                      </p>
                      <?php
                    }
                } else if ( $filter_post_type == "acf" ) {
                  if( class_exists('ACF') ) {
                    $acf_get = get_field_object($acf_name);
                    $acf_type = $acf_get['type'];

                    $queryvar = get_query_var($acf_get['name']) ? get_query_var($acf_get['name']) : ( !empty( $_GET[$acf_get['name']] )? $_GET[$acf_get['name']] : '' );

                    if ($custom_label !== '') {
                      $custom_label_final = $custom_label;
                    } else {
                      $custom_label_final = $acf_get['label'];
                    }
                    ?>
                    <div class="et_pb_contact">
                      <?php
                      if ($acf_filter_type == "select") {

                        if ($acf_get['multiple'] == '1') {
                          $select_multiple = 'mulitple';
                        } else {
                          $select_multiple = '';
                        }

                        if ($acf_type == "select" || $acf_type == "checkbox") {

                          if ($acf_type == "select") {
                            $acf_type_filter = "acfselect";
                          } else {
                            if ($radio_select == "checkbox") {
                              $acf_type_filter = "divi-checkboxmulti";
                            } else {
                              $acf_type_filter = "divi-checkboxsingle";
                            }
                          }

                          $data_type = 'select';

                          if ( $acf_type == "checkbox" || ( $acf_type == "select" && $acf_get['multiple'] == '1' ) ){
                            $data_type = 'checkbox';
                          }
                          ?>
                          <p class="et_pb_contact_field_options_title <?php echo $label_css ?>"><?php echo esc_html__( $custom_label_final, 'divi-ajax-filter' ); ?></p>
                          <p class="et_pb_contact_field divi-filter-item" data-type="<?php echo $data_type;?>" data-filtertype="<?php echo $acf_filter_type;?>">
                            <select id="<?php echo $acf_get['name'] . '_' . $num; ?>" class="divi-acf et_pb_contact_select" data-filtertype="<?php echo $acf_type_filter ?><?php echo $select_multiple ?>" name="<?php echo $acf_get['name']; ?>" data-field_type="select">
                              <option value="" <?php echo (empty($queryvar) || $queryvar == '')?'selected':'';?>><?php echo $select_placeholder ?></option>
                              <?php
                              foreach($acf_get['choices'] as $key => $value) {
                                if ($queryvar == $key) {
                                  ?>
                                  <option value="<?php echo $key ?>" selected="selected"><?php echo $value; ?></option>
                                  <?php
                                } else {
                                  ?>
                                  <option value="<?php echo $key ?>"><?php echo $value; ?></option>
                                  <?php
                                }
                              }
                              ?>
                            </select>
                          </p>
                          <?php
                        } else {
                          echo "Please make sure your field is a select or checkbox type";
                        }
                      }else if ($acf_filter_type == "radio") {
                          $data_type = $acf_type;

                          if ( $acf_type == "checkbox" || ( $acf_type == "select" && $acf_get['multiple'] == '1' ) ){
                            $data_type = 'checkbox';
                          }
                        if ($radio_style == "select") {
                          ?>
                          <p class="et_pb_contact_field_options_title <?php echo $label_css ?>"><?php echo esc_html__( $custom_label_final, 'divi-ajax-filter' ); ?></p>
                          <p class="et_pb_contact_field divi-filter-item" data-type="<?php echo $data_type;?>" name="<?php echo $acf_get['name']; ?>" data-field_type="select" data-filtertype="<?php echo $acf_filter_type;?>">
                            <select id="<?php echo $acf_get['name'] . '_' . $num; ?>" class="divi-acf et_pb_contact_select" data-filtertype="divi-checkboxsingle" data-field_type="select" name="<?php echo $acf_get['name']; ?>">
                              <option value=""  <?php echo (empty($queryvar) || $queryvar == '')?'selected':'';?>><?php echo $select_placeholder ?></option>
                              <?php
                              foreach($acf_get['choices'] as $key => $value) {
                                if ($queryvar == $key) {
                                  ?>
                                  <option id="<?php echo $acf_get['name']; ?>_<?php echo $key . '_' . $num; ?>" value="<?php echo $key ?>" selected="selected"><?php echo $value; ?></option>
                                  <?php
                                } else {
                                  ?>
                                  <option id="<?php echo $acf_get['name']; ?>_<?php echo $key . '_' . $num; ?>" value="<?php echo $key ?>"><?php echo $value; ?></option>
                                  <?php
                                }
                              }
                              ?>
                            </select>
                          </p>
                          <?php
                        } else {
                          if ($radio_select == "checkbox") {
                            $checkboxtype = "divi-checkboxmulti";
                          } else {
                            $checkboxtype = "divi-checkboxsingle";
                          }

                          $additional_classes = '';
                          if ( isset( $radio_show_count ) && $radio_show_count == 'on' ){
                            $additional_classes .= ' divi-show-count';
                          }

                          $empty_class = '';
                          if ( $radio_show_empty == 'on' ){
                            $empty_class = 'show-empty';
                          }

                          $data_type = $acf_type;

                          if ( $acf_type == "checkbox" || ( $acf_type == "select" && $acf_get['multiple'] == '1' ) ){
                            $data_type = 'checkbox';
                          }

                          $selected_values = explode( ',', $queryvar );
                          ?>
                          <p class="et_pb_contact_field" data-type="<?php echo $data_type;?>" data-filtertype="<?php echo $acf_filter_type;?>">
                            <div class="et_pb_contact_field_options_wrapper divi-radio-<?php echo $radio_style ?>">
                              <span class="et_pb_contact_field_options_title <?php echo $label_css ?>"><?php echo esc_html__( $custom_label_final, 'divi-ajax-filter' ); ?></span>
                              <div class="et_pb_contact_field_options_list divi-filter-item <?php echo $checkboxtype ?> <?php echo $empty_class;?>" data-filter-option="<?php echo $acf_get['key'];?>" data-filter-count="<?php echo $radio_show_count;?>">
                              <form>
                                <?php if ( ( $radio_select != 'checkbox') && ($radio_all_hide != 'on' ) ) { ?>
                                  <span class="et_pb_contact_field_radio">
                                    <input type="<?php echo $radio_select ?>" id="<?php echo $acf_get['name']; ?>_all_<?php echo $num;?>" class="divi-acf input" data-filtertype="<?php echo $checkboxtype ?>" value="" name="<?php echo $acf_get['name']; ?>" data-required_mark="required" data-field_type="radio" data-original_id="radio" <?php echo (empty($selected_values) || (count($selected_values) == 1 && $selected_values[0] == ''))?'checked':'';?>>
                                    <?php if ($radio_style == "tick_box") { echo '<span class="checkmark"></span>'; } ?>
                                    <label class="radio-label" data-value="all" for="<?php echo $acf_get['name']; ?>_all_<?php echo $num;?>"><i></i><?php echo esc_html__( $radio_all_text, 'divi-ajax-filter' ); ?></label>
                                  </span>
                                <?php } ?>
                                  <?php
                                foreach($acf_get['choices'] as $key => $value) {
                                  ?>
                                  <span class="et_pb_contact_field_radio">
                                    <?php
                                    if ( in_array( $key, $selected_values ) ) {
                                      ?>
                                      <input type="<?php echo $radio_select ?>" id="<?php echo $acf_get['name']; ?>_<?php echo $key . '_' . $num; ?>" class="divi-acf input" data-filtertype="<?php echo $checkboxtype ?>" value="<?php echo $key ?>" name="<?php echo $acf_get['name']; ?>" data-required_mark="required" data-field_type="radio" data-original_id="radio" checked="checked">
                                      <?php if ($radio_style == "tick_box") { echo '<span class="checkmark"></span>'; } else {} ?>
                                      <label class="radio-label" data-value="<?php echo $key ?>" for="<?php echo $acf_get['name']; ?>_<?php echo $key . '_' . $num; ?>" title="<?php echo $value;?>"><i></i><?php echo $value; ?></label>
                                      <?php
                                    } else {
                                      ?>
                                      <input type="<?php echo $radio_select ?>" id="<?php echo $acf_get['name']; ?>_<?php echo $key . '_' . $num; ?>" class="divi-acf input" data-filtertype="<?php echo $checkboxtype ?>" value="<?php echo $key ?>" name="<?php echo $acf_get['name']; ?>" data-required_mark="required" data-field_type="radio" data-original_id="radio">
                                      <?php if ($radio_style == "tick_box") { echo '<span class="checkmark"></span>'; } else {} ?>
                                      <label class="radio-label" data-value="<?php echo $key ?>" for="<?php echo $acf_get['name']; ?>_<?php echo $key . '_' . $num; ?>" title="<?php echo $value;?>"><i></i><?php echo $value; ?></label>
                                      <?php
                                    }
                                    ?>
                                  </span>
                                  <?php
                                }
                                ?>
                                </form>
                              </div>
                            </div>
                          </p>
                          <?php
                        }
                      } else if ($acf_filter_type == "number_range") {
                        $acf_type = $acf_get['type'];

                        if ($acf_type == "range") {
                          $range_type = "double";
                        } else if ($acf_type == "number")  {
                          $range_type = "single";
                        } else {
                          $range_type = "none";
                        }

                        $data_type = $acf_type;

                        if ( $acf_type == "checkbox" || ( $acf_type == "select" && $acf_get['multiple'] == '1' ) ){
                          $data_type = 'checkbox';
                        }

                        if ($range_type == "none") {
                          echo "please make sure your ACF item is a number or a range field.";
                        } else {

                          if (isset($queryvar)) {
                            $queryvar_arr = (explode(";",$queryvar));
                            if ( is_array( $queryvar_arr ) ) {
                              if ( isset( $queryvar_arr[1] ) ){
                                $range_from_query = $queryvar_arr[0];
                                $range_to_query = $queryvar_arr[1];
                              }else{
                                $range_from_query = $queryvar_arr[0];
                                $range_to_query = '';
                              }
                            } else {
                              $range_from_query = "";
                              $range_to_query = "";
                            }
                          }

                          if ($range_from_query != "") {
                            $range_from = $range_from_query;
                          }
                          if ($range_to_query != "") {
                            $range_to = $range_to_query;
                          }

                          if ($acf_get['max'] < $range_to) {
                            $range_to = $acf_get['max'];
                          }
                          ?>
                          <p class="et_pb_contact_field" data-type="<?php echo $data_type;?>">
                            <span class="et_pb_contact_field_options_title <?php echo $label_css ?>"><?php echo esc_html__( $custom_label_final, 'divi-ajax-filter' ); ?></span>
                            <div class="divi-filter-item">
                              <input id="<?php echo $acf_get['name'] . '_' . $num; ?>" type="select" class="divi-acf js-range-slider" data-filtertype="rangeslider" data-field_type="range" name="<?php echo $acf_get['name']; ?>" value="" />
                            </div>
                          </p>
                          <?php
                          $css_classrend = "." . $css_class . "";
                          ?>
                          <style>
                          <?php echo $css_classrend ?> .irs-bar,
                          <?php echo $css_classrend ?> .irs-from,
                          <?php echo $css_classrend ?> .irs-to,
                          <?php echo $css_classrend ?> .irs-single,
                          <?php echo $css_classrend ?> .irs-handle>i:first-child {
                            background-color: <?php echo $range_prim_color; ?> !important;
                          }
                          <?php echo $css_classrend ?> .irs-from:before,
                          <?php echo $css_classrend ?> .irs-to:before,
                          <?php echo $css_classrend ?> .irs-single:before {
                            border-top-color: <?php echo $range_prim_color; ?> !important;
                          }
                          <?php echo $css_classrend ?> .irs-line,
                          <?php echo $css_classrend ?> .irs-min,
                          <?php echo $css_classrend ?> .irs-max {
                            background-color: <?php echo $range_sec_color; ?> !important;
                          }
                        </style>

                        <?php
                        if ($acf_get['min'] == "") {
                          $acf_get['min'] = "0";
                        }
                        ?>

                        <script>
                        jQuery(document).ready(function($) {

                          var type = '<?php echo $range_type ?>',
                          min = <?php echo $acf_get['min'] ?>,
                          max = <?php echo $acf_get['max'] ?>,
                          from = <?php echo $range_from ?>,
                          to = <?php echo $range_to ?>,
                          step = <?php echo $range_step ?>,
                          skin = "<?php echo $range_skin ?>",
                          hide_min_max = <?php echo $range_hide_min_max ?>,
                          hide_from_to = <?php echo $range_hide_from_to ?>,
                          prettify_enabled = <?php echo $range_prettify_enabled ?>,
                          prettify_separator = "<?php echo $range_prettify_separator ?>",
                          prefix = "<?php echo $range_prefix ?>",
                          postfix = "<?php echo $range_postfix ?>";

                          var filters = [];

                          var filter_item_name_arr = [];
                          var filter_item_id_arr = [];
                          var filter_item_val_arr = [];
                          var filter_input_type_arr = [];
                          jQuery('.divi-acf').each(function(i, obj) {
                            var filter_item_name = jQuery(this).attr("name"),
                            filter_item_id = jQuery(this).attr("id"),
                            filter_item_val = jQuery(this).val(),
                            filter_input_type = jQuery(this).attr('type');
                            filter_item_name_arr.push(filter_item_name);
                            filter_item_id_arr.push(filter_item_id);
                            filter_item_val_arr.push(filter_item_val);
                            filter_input_type_arr.push(filter_input_type);
                          });

                          var filter_item_name = jQuery(".<?php echo $css_class ?> .js-range-slider").attr("name");

                          $(".<?php echo $css_class ?> .js-range-slider").ionRangeSlider({
                            type: type,
                            min: min,
                            max: max,
                            from: from,
                            to: to,
                            step: step,
                            skin: skin,
                            hide_min_max: hide_min_max,
                            hide_from_to: hide_from_to,
                            prettify_enabled: prettify_enabled,
                            prettify_separator: prettify_separator,
                            prefix: prefix,
                            postfix: postfix,
                            onFinish: function (data) {
                              if ( $(".<?php echo $css_class ?> .js-range-slider").parents('.et_pb_de_mach_search_posts').length == 0 && $(".<?php echo $css_class ?> .js-range-slider").parents('.updatetype-update_button').length == 0 ){
                              
                                      var _this = $(".<?php echo $css_class ?> .js-range-slider");
                                      var iris_to = _this.closest(".et_pb_de_mach_search_posts_item").find(".irs-to").text();
                                      var irs_from = _this.closest(".et_pb_de_mach_search_posts_item").find(".irs-from").text();
                                      if ( _this.closest(".et_pb_de_mach_search_posts_item").hasClass( "filter_params" ) && (irs_from != 0 || iris_to != max ) ) {
                                        if ( _this.closest(".et_pb_de_mach_search_posts_item").hasClass( "filter_params_yes_title" ) ) {
                                          var filter_param_type = "title";
                                        } else {
                                          var filter_param_type = "no-title";
                                        }
                                        var value = _this.closest(".et_pb_de_mach_search_posts_item").find(".js-range-slider").val(),
                                        name = _this.closest(".et_pb_de_mach_search_posts_item").find(".et_pb_contact_field_options_title ").text(),
                                        slug = _this.attr("name"),
                                        type = 'range',
                                        iris_to = _this.closest(".et_pb_de_mach_search_posts_item").find(".irs-to").text(),
                                        irs_from = _this.closest(".et_pb_de_mach_search_posts_item").find(".irs-from").text();
                                        divi_filter_params(type, slug, value, name, filter_param_type, iris_to, irs_from);
                                      } else {
                                        if (jQuery('.param-' + _this.attr('name') ).length) {
                                          jQuery('.param-' + _this.attr('name') ).remove();
                                        }
                                      }
                                divi_find_filters_to_filter();
                              }
                            }
                          });
<?php
            if ( !empty( $_GET[$acf_get['name']] ) && ( $range_from != $acf_get['min'] || $range_to != $acf_get['max'] ) ) {
?>
                var acf_slider = $(".<?php echo $css_class ?> .js-range-slider");
                if ( acf_slider.closest(".et_pb_de_mach_search_posts_item").hasClass( "filter_params" ) ) {
                    if ( acf_slider.closest(".et_pb_de_mach_search_posts_item").hasClass( "filter_params_yes_title" ) ) {
                      var filter_param_type = "title";
                    } else {
                      var filter_param_type = "no-title";
                    }
                    var value = acf_slider.closest(".et_pb_de_mach_search_posts_item").find(".js-range-slider").val(),
                    name = acf_slider.closest(".et_pb_de_mach_search_posts_item").find(".et_pb_contact_field_options_title ").text(),
                    slug = acf_slider.attr("name"),
                    type = 'range',
                    iris_to = acf_slider.closest(".et_pb_de_mach_search_posts_item").find(".irs-to").text(),
                    irs_from = acf_slider.closest(".et_pb_de_mach_search_posts_item").find(".irs-from").text();
                    divi_filter_params(type, slug, value, name, filter_param_type, iris_to, irs_from);
                }
<?php
            }
?>
                        });
                      </script>
                      <?php
                    }
                  }
                  ?>
                </div>
                <?php
                  }
                } else if ($filter_post_type == "tags") {

                  // GET AVAILABLE TAGS
                  // 1) First we get the term query
                  if ($only_show_avail == "on") {
                    $ending = "_tag";
                    $cat_key = $post_type_choose . $ending;
                    // 2) We get the WP_Query - get all the posts ID's on the page
                    global $wp_query;
                    $avail_terms = "";
                    $ID_array = $wp_query->posts;
                    foreach($ID_array as $item) { // Then for each ID - we then look for the category
                      $terms_post = get_the_terms( $item->ID , $cat_key );
                      // 3) for each category we then add the ID to a string
                      if (is_array($terms_post)) {
                        foreach ($terms_post as $term_cat) { 
                          $term_cat_id = $term_cat->term_id; 
                          $avail_terms .= $term_cat_id.",";
                        }
                      }
                    }
                    $avail_terms = implode(',',array_unique(explode(',', $avail_terms)));
                  } else {
                    $avail_terms = '';
                  }
                  // GET AVAILABLE TAGS

                    if ($custom_label !== '') {
                        $custom_label_final = $custom_label;
                    } else {
                        $custom_label_final = 'Tags';
                    }

                    $ending = "_tag";
                    $cat_key = $post_type_choose . $ending;

                    $get_categories = get_terms( $cat_key, array('hide_empty' => $cat_tag_hide_empty_dis, 'include' => $avail_terms) );

                    $queryvar = get_query_var( $cat_key )?get_query_var( $cat_key):( !empty($_GET[$cat_key])?$_GET[$cat_key]:'' );

                    if ($acf_filter_type == "select") {
?>
                    <p class="et_pb_contact_field_options_title <?php echo $label_css ?>"><?php echo esc_html__( $custom_label_final, 'divi-ajax-filter' ); ?></p>
                    <p class="et_pb_contact_field divi-filter-item" data-type="select" name="tags" data-field_type="select">
                      <select id="<?php echo $cat_key . '_' . $num; ?>" class="divi-acf et_pb_contact_select" data-filtertype="customtag" name="<?php echo $cat_key ?>">
                        <option value="" <?php echo (empty($queryvar) || ($queryvar != ''))?'selected':'';?>><?php echo $select_placeholder ?></option>
                        <?php
                        if ( $get_categories ) :
                          foreach ( $get_categories as $cat ) :
                            ?>
                            <option id="<?php echo $cat->term_id . '_' . $num; ?>" value="<?php echo $cat->slug; ?>" <?php echo ($queryvar == $cat->slug)?'selected':''; ?>><?php echo $cat->name; ?></option>
                            <?php
                          endforeach;
                        endif;
                        ?>
                      </select>
                    </p>
<?php
                    } else {

                        if ($radio_select == "checkbox") {
                            $checkboxtype = "divi-checkboxmulti";
                        } else {
                            $checkboxtype = "divi-checkboxsingle";
                        }

                        $query_var_arr = explode( ',', $queryvar );
?>
                    <p class="et_pb_contact_field" data-type="radio" data-filtertype="radio">
                      <div class="et_pb_contact_field_options_wrapper divi-radio-<?php echo $radio_style ?>">
                        <span class="et_pb_contact_field_options_title <?php echo $label_css ?>"><?php echo esc_html__( $custom_label_final, 'divi-ajax-filter' ); ?></span>
                        <div class="et_pb_contact_field_options_list divi-filter-item <?php echo $checkboxtype ?>" data-filter-count="<?php echo $radio_show_count;?>" data-show-empty="<?php echo $radio_show_empty;?>" data-filter-option="<?php echo $cat_key;?>">
                          <?php if ( ( $radio_select != 'checkbox') && ($radio_all_hide != 'on' ) && $only_show_avail == "off") { ?>
                          <span class="et_pb_contact_field_radio">
                            <input type="<?php echo $radio_select ?>" id="<?php echo $cat_key;?>_all_<?php echo $num;?>" class="divi-acf input" data-filtertype="<?php echo $checkboxtype ?>" value="" name="<?php echo $cat_key;?>" data-required_mark="required" data-field_type="radio" data-original_id="radio" checked="checked">
                            <?php if ($radio_style == "tick_box") { echo '<span class="checkmark"></span>'; } else {} ?>
                            <label class="radio-label" data-value="all" for="<?php echo $cat_key;?>_all_<?php echo $num;?>"><i></i><?php echo esc_html__( $radio_all_text, 'divi-ajax-filter' ); ?></label>
                          </span>
                          <?php } ?>
                          <?php
                          if ( $get_categories ) :
                            foreach ( $get_categories as $cat ) :
                              ?>
                              <span class="et_pb_contact_field_radio">
                                <input type="<?php echo $radio_select ?>" id="<?php echo $cat->term_id; ?>_<?php echo $cat->slug . '_' . $num; ?>" class="divi-acf input" data-filtertype="<?php echo $checkboxtype ?>" value="<?php echo $cat->slug ?>" name="<?php echo $cat_key;?>" data-required_mark="required" data-field_type="radio" data-original_id="radio" <?php echo in_array($cat->slug, $query_var_arr)?'checked':'';?>>
                                <?php if ($radio_style == "tick_box") { echo '<span class="checkmark"></span>'; } else {} ?>
                                <label class="radio-label" data-value="<?php echo $cat->slug ?>" for="<?php echo $cat->term_id; ?>_<?php echo $cat->slug . '_' . $num; ?>" title="<?php echo $cat->name;?>"><i></i><?php echo $cat->name; ?></label>
                              </span>
                              <?php
                            endforeach;
                          endif;
                          ?>
                        </div>
                      </div>
                    </p>
<?php
                    }
                } else if ($filter_post_type == "search") {

                    if ($custom_label !== '') {
                        $custom_label_final = $custom_label;
                    } else {
                        $custom_label_final = '';
                    }

                    if (get_search_query() == "") {
                        $seach_value = get_search_query();
                    } else {
                        $seach_value = "";
                    }
?>
                    <p class="et_pb_contact_field" data-type="input" style="padding-left: 0;">
                        <span class="et_pb_contact_field_options_title"><?php echo esc_html__( $custom_label_final, 'divi-ajax-filter' ); ?></span>
                        <input id="s_<?php echo $num;?>" class="divi-acf divi-filter-item" data-filtertype="textsearch" type="text" name="s" placeholder="<?php echo esc_html__( $text_placeholder, 'divi-ajax-filter' ); ?>" value="<?php echo $seach_value ?>" style="width: 100%;">
                    </p>
                  <?php
                } else if ($filter_post_type == "posttypes") {

                  if ($custom_label !== '') {
                    $custom_label_final = $custom_label;
                  } else {
                    $custom_label_final = '';
                  }

                  $option_search  = array( '&#91;', '&#93;' );
                  $option_replace = array( '[', ']' );
                  $select_options = str_replace( $option_search, $option_replace, $select_options );
                  $select_options = json_decode( $select_options );
                  ?>
                  <p class="et_pb_contact_field_options_title"><?php echo esc_html__( $custom_label_final, 'divi-ajax-filter' ); ?></p>
                  <p class="et_pb_contact_field divi-filter-item" data-type="select">
                    <select id="select_post_types_<?php echo $num;?>" class="divi-acf et_pb_contact_select" data-filtertype="posttypes" name="select_post_types" data-field_type="select">
                      <option value="" selected="selected"><?php echo $select_placeholder ?></option>
                      <?php
                      foreach ( $select_options as $option ) {
                        $pt = get_post_type_object( $option->value );
                        ?>
                        <option value="<?php echo $option->value; ?>"><?php echo $pt->labels->name; ?></option>
                        <?php
                      }
                      ?>
                    </select>
                  </p>
                  <?php
                } else if ( $filter_post_type == "taxonomy" ) {

                   // GET AVAILABLE TAXONOMIES
                  // 1) First we get the term query
                  if ($only_show_avail == "on") {
                    global $wp_query;
                    $avail_terms = "";
                    $ID_array = $wp_query->posts;
                    foreach($ID_array as $item) { // Then for each ID - we then look for the category
                      $terms_post = get_the_terms( $item->ID , $custom_tax_choose );
                      // 3) for each category we then add the ID to a string
                      if (is_array($terms_post)) {
                        foreach ($terms_post as $term_cat) { 
                          $term_cat_id = $term_cat->term_id; 
                          $avail_terms .= $term_cat_id.",";
                        }
                      }
                    }
                    $avail_terms = implode(',',array_unique(explode(',', $avail_terms)));
                  } else {
                    $avail_terms = '';
                  }
                  // GET AVAILABLE TAXONOMIES
                  
                    $taxonomy_details = get_taxonomy( $custom_tax_choose );

                    if ($custom_label !== '') {
                        $custom_label_final = $custom_label;
                    } else {
                        $custom_label_final = $taxonomy_details->label;
                    }


                    $get_categories = get_terms( $custom_tax_choose, array('hide_empty' => $cat_tag_hide_empty_dis, 'include' => $avail_terms) );

                    $queryvar = get_query_var( $custom_tax_choose )?get_query_var( $custom_tax_choose):( !empty($_GET[$custom_tax_choose])?$_GET[$custom_tax_choose]:'' );

                    if ($acf_filter_type == "select") {
?>
                    <p class="et_pb_contact_field_options_title <?php echo $label_css ?>"><?php echo esc_html__( $custom_label_final, 'divi-ajax-filter' ); ?></p>
                    <p class="et_pb_contact_field divi-filter-item" data-type="select" name="<?php echo $taxonomy_details->name ?>" data-field_type="select">
                        <select id="<?php echo $custom_tax_choose . '_' . $num; ?>" class="divi-acf et_pb_contact_select" data-filtertype="customtaxonomy" data-field_type="select" name="<?php echo $custom_tax_choose ?>">
                            <option value="" <?php selected( $queryvar, ''); ?>><?php echo $select_placeholder ?></option>
<?php
                        if ( $get_categories ) :
                            foreach ( $get_categories as $cat ) :
?>
                            <option id="<?php echo $cat->term_id . '_' . $num; ?>" value="<?php echo $cat->slug; ?>" <?php selected( $queryvar, $cat->slug); ?>>
                                <?php echo $cat->name; ?>
                            </option>
<?php
                            endforeach;
                        endif;
                        ?>
                        </select>
                    </p>
<?php
                    } else {

                        $empty_class = '';
                        if ( $radio_show_empty == 'on' ){
                            $empty_class = 'show-empty';
                        }

                        if ($radio_select == "checkbox") {
                            $checkboxtype = "divi-checkboxmulti";
                        } else {
                            $checkboxtype = "divi-checkboxsingle";
                        }
?>
                    <p class="et_pb_contact_field" data-type="radio" data-filtertype="radio">
                      <div class="et_pb_contact_field_options_wrapper divi-radio-<?php echo $radio_style ?>">
                        <span class="et_pb_contact_field_options_title <?php echo $label_css ?>"><?php echo esc_html__( $custom_label_final, 'divi-ajax-filter' ); ?></span>
                        <div class="et_pb_contact_field_options_list divi-filter-item <?php echo $checkboxtype ?> <?php echo $empty_class;?>" data-filter-count="<?php echo $radio_show_count;?>" data-show-empty="<?php echo $radio_show_empty;?>" data-filter-option="<?php echo $custom_tax_choose;?>">
                          <?php if ( ( $radio_select != 'checkbox') && ($radio_all_hide != 'on' ) && $only_show_avail == "off") { ?>
                          <span class="et_pb_contact_field_radio">
                            <input type="<?php echo $radio_select ?>" id="<?php echo $custom_tax_choose;?>_all_<?php echo $num;?>" class="divi-acf input" data-filtertype="<?php echo $checkboxtype ?>" value="" name="<?php echo $custom_tax_choose;?>" data-required_mark="required" data-field_type="radio" data-original_id="radio" checked="checked">
                            <?php if ($radio_style == "tick_box") { echo '<span class="checkmark"></span>'; }?>
                            <label class="radio-label" data-value="all" for="<?php echo $custom_tax_choose;?>_all_<?php echo $num;?>"><i></i><?php echo esc_html__( $radio_all_text, 'divi-ajax-filter' ); ?></label>
                          </span>
                          <?php } ?>
                          <?php
                          if ( $get_categories ) :
                            foreach ( $get_categories as $cat ) :
                              ?>
                              <span class="et_pb_contact_field_radio">
                                <input type="<?php echo $radio_select ?>" id="<?php echo $cat->term_id; ?>_<?php echo $cat->slug . '_' . $num ?>" class="divi-acf input" data-filtertype="<?php echo $checkboxtype ?>" value="<?php echo $cat->slug ?>" name="<?php echo $custom_tax_choose;?>" data-required_mark="required" data-field_type="radio" data-original_id="radio">
                                <?php if ($radio_style == "tick_box") { echo '<span class="checkmark"></span>'; } else {} ?>
                                <label class="radio-label" data-value="<?php echo $cat->slug ?>" for="<?php echo $cat->term_id; ?>_<?php echo $cat->slug . '_' . $num ?>" title="<?php echo $cat->name;?>"><i></i><?php echo $cat->name; ?></label>
                              </span>
                              <?php
                            endforeach;
                          endif;
                          ?>
                        </div>
                      </div>
                    </p>
<?php
                    }
                } else if ($filter_post_type == "productattr") {

                   
                  // GET AVAILABLE PRO ATTR
                  // 1) First we get the term query
                  if ($only_show_avail == "on") {
                    // 2) We get the WP_Query - get all the posts ID's on the page
                    global $wp_query;
                    $avail_terms = "";
                    $ID_array = $wp_query->posts;
                    foreach($ID_array as $item) { // Then for each ID - we then look for the category
                      $terms_post = get_the_terms( $item->ID , $product_attribute );
                      // 3) for each category we then add the ID to a string
                      if (is_array($terms_post)) {
                        foreach ($terms_post as $term_cat) { 
                          $term_cat_id = $term_cat->term_id; 
                          $avail_terms .= $term_cat_id.",";
                        }
                      }
                    }
                    $avail_terms = implode(',',array_unique(explode(',', $avail_terms)));
                  } else {
                    $avail_terms = '';
                  }
                  // GET AVAILABLE PRO ATTR

                    $terms_array = array(
                        'taxonomy' => $product_attribute, 
                        'hide_empty' => $cat_tag_hide_empty_dis, 
                        'include' => $avail_terms
                    );
                    $terms = get_terms( $terms_array );
                    
                    $name = str_replace( 'pa_', '', wc_sanitize_taxonomy_name( $product_attribute ) ); 
                    $taxonomy_array = wc_get_attribute_taxonomies();
                    $taxonomies = wp_list_pluck( $taxonomy_array, 'attribute_label', 'attribute_name' ); 
                    $taxonomy_types = wp_list_pluck( $taxonomy_array, 'attribute_type', 'attribute_name' );

                    if ($custom_label !== '') {
                        $custom_label_final = $custom_label;
                    }else{
                        $custom_label_final = $taxonomies[$name];
                    }

                    $queryvar = get_query_var( $product_attribute )?get_query_var( $product_attribute):( !empty($_GET[$product_attribute])?$_GET[$product_attribute]:'' );

                    if ( $attribute_swatch == 'on' ){
                        if ( $taxonomy_types[$name] != 'select' && $taxonomy_types[$name] != 'button' ) {
?>
                    <div class="et_pb_contact_field divi-swatch" data-type="radio" data-filtertype="radio">
                        <div class="et_pb_contact_field_options_wrapper">
                            <span class="et_pb_contact_field_options_title <?php echo $label_css ?>"><?php echo esc_html__( $custom_label_final, 'divi-ajax-filter' ); ?></span>
                            <div class="et_pb_contact_field_options_list divi-filter-item divi-checkboxsingle" data-filter-count="<?php echo $radio_show_count;?>" data-show-empty="<?php echo $radio_show_empty;?>" data-filter-option="<?php echo $product_attribute;?>">
                                <span class="et_pb_contact_field_radio remove_filter">
                                    <input type="<?php echo $radio_select ?>" id="<?php echo $product_attribute;?>_all_<?php echo $num;?>" class="divi-acf input" data-filtertype="divi-checkboxsingle" value="" name="<?php echo $product_attribute;?>" data-required_mark="required" data-field_type="radio" data-original_id="radio" <?php checked( $queryvar, '' );?>>
                                    <label class="radio-label" data-value="all" for="<?php echo $product_attribute;?>_all_<?php echo $num;?>"><i></i><?php echo esc_html__( 'Remove Filter', 'divi-ajax-filter' ); ?></label>
                                </span>
<?php                        
                            if ( !empty( $terms ) ) {
                                foreach ($terms as $term) {
                                    $value = get_term_meta( $term->term_id, 'product_attribute_'.$taxonomy_types[$name], TRUE );
?>
                                <span class="et_pb_contact_field_radio">
                                    <input type="<?php echo $radio_select ?>" id="<?php echo $term->term_id; ?>_<?php echo $term->slug . '_' . $num ?>" class="divi-acf input" data-filtertype="divi-checkboxsingle" value="<?php echo $term->slug ?>" name="<?php echo $product_attribute;?>" data-required_mark="required" data-field_type="radio" data-original_id="radio" <?php checked( $queryvar, $term->slug );?>>
                                    <label class="radio-label" data-value="<?php echo $term->slug ?>" title="<?php echo $term->name;?>" for="<?php echo $term->term_id; ?>_<?php echo $term->slug . '_' . $num ?>"><i style="<?php echo ($taxonomy_types[$name] == 'color')?'background':'background-image';?>:<?php echo ($taxonomy_types[$name] == 'color' )?$value:"url('".$value."')";?>;"></i></label>
                                </span>
<?php
                                }
                            }
?>
                            </div>
                        </div>
                    </div>
<?php
                        } else if ( $taxonomy_types[$name] == 'button' ){
?>
                    <div class="et_pb_contact_field divi-swatch" data-type="radio" data-filtertype="radio">
                        <div class="et_pb_contact_field_options_wrapper">
                            <span class="et_pb_contact_field_options_title <?php echo $label_css ?>"><?php echo esc_html__( $custom_label_final, 'divi-ajax-filter' ); ?></span>
                            <div class="et_pb_contact_field_options_list divi-filter-item divi-checkboxsingle" data-filter-count="<?php echo $radio_show_count;?>" data-show-empty="<?php echo $radio_show_empty;?>" data-filter-option="<?php echo $product_attribute;?>">
                                <span class="et_pb_contact_field_radio remove_filter">
                                    <input type="<?php echo $radio_select ?>" id="<?php echo $product_attribute;?>_all_<?php echo $num;?>" class="divi-acf input" data-filtertype="divi-checkboxsingle" value="" name="<?php echo $product_attribute;?>" data-required_mark="required" data-field_type="radio" data-original_id="radio" <?php checked( $queryvar, '' );?>>
                                    <label class="radio-label" data-value="all" for="<?php echo $product_attribute;?>_all_<?php echo $num;?>"><i></i><?php echo esc_html__( 'Remove Filter', 'divi-ajax-filter' ); ?></label>
                                </span>
<?php                        
                            if ( !empty( $terms ) ) {
                                foreach ($terms as $term) {
                                    $value = get_term_meta( $term->term_id, 'product_attribute_'.$taxonomy_types[$name], TRUE );
?>
                                <span class="et_pb_contact_field_radio">
                                    <input type="<?php echo $radio_select ?>" id="<?php echo $term->term_id; ?>_<?php echo $term->slug . '_' . $num ?>" class="divi-acf input" data-filtertype="divi-checkboxsingle" value="<?php echo $term->slug ?>" name="<?php echo $product_attribute;?>" data-required_mark="required" data-field_type="radio" data-original_id="radio" <?php checked( $queryvar, $term->slug );?>>
                                    <label class="radio-label" data-value="<?php echo $term->slug ?>" title="<?php echo $term->name;?>" for="<?php echo $term->term_id; ?>_<?php echo $term->slug . '_' . $num ?>"><i><?php echo $term->name;?></i></label>
                                </span>
<?php
                                }
                            }
?>
                            </div>
                        </div>
                    </div>
<?php                        
                        } else if ( $taxonomy_types[$name] == 'select' ) {
?>
                        <p class="et_pb_contact_field_options_title <?php echo $label_css ?>"><?php echo esc_html__( $custom_label_final, 'divi-filter' ); ?></p>
                        <div class="et_pb_contact_field divi-filter-item" data-type="select" data-field_type="select">
                          <select id="<?php echo $product_attribute . '_' . $num; ?>" class="divi-acf et_pb_contact_select" data-filtertype="customtag" name="<?php echo $product_attribute ?>">
                            <option value="" <?php selected( $queryvar, '' );?>><?php echo $select_placeholder ?></option>
                            <?php
                            if ( !empty($terms) ) :
                              foreach ( $terms as $term ) :
                                ?>
                                <option id="<?php echo $term->term_id . '_' . $num; ?>" value="<?php echo $term->slug; ?>" <?php selected( $queryvar, $term->slug );?>>
                                  <?php echo $term->name; ?>
                                </option>
                                <?php
                              endforeach;
                            endif;
                            ?>
                          </select>
                        </div>
<?php                        
                        }
                    }else{
                        if ($acf_filter_type == "select") {
?>
                        <p class="et_pb_contact_field_options_title <?php echo $label_css ?>"><?php echo esc_html__( $custom_label_final, 'divi-filter' ); ?></p>
                        <div class="et_pb_contact_field divi-filter-item" data-type="select" data-field_type="select">
                            <select id="<?php echo $product_attribute . '_' . $num; ?>" class="divi-acf et_pb_contact_select" data-filtertype="customtag" name="<?php echo $product_attribute ?>">
                                <option value="" <?php selected( $queryvar, '' );?>><?php echo $select_placeholder ?></option>
                            <?php
                                if ( !empty($terms) ) :
                                    foreach ( $terms as $term ) :
                            ?>
                                <option id="<?php echo $term->term_id . '_' . $num; ?>" value="<?php echo $term->slug; ?>" <?php selected( $queryvar, $term->slug );?>>
                                    <?php echo $term->name; ?>
                                </option>
                        <?php
                                    endforeach;
                                endif;
                        ?>
                            </select>
                        </div>
<?php
                        } else if ( $acf_filter_type == "radio" ) {
                            if ($radio_select == "checkbox") {
                                $checkboxtype = "divi-checkboxmulti";
                            } else {
                                $checkboxtype = "divi-checkboxsingle";
                            }
                            $queryvar = explode(',', $queryvar);
    ?>
                          <p class="et_pb_contact_field" data-type="radio" data-filtertype="radio">
                            <div class="et_pb_contact_field_options_wrapper divi-radio-<?php echo $radio_style ?>">
                              <span class="et_pb_contact_field_options_title <?php echo $label_css ?>"><?php echo esc_html__( $custom_label_final, 'divi-filter' ); ?></span>
                              <div class="et_pb_contact_field_options_list divi-filter-item <?php echo $checkboxtype ?>" data-filter-option="<?php echo $product_attribute;?>">
                                <?php if ( ( $radio_select != 'checkbox') && ($radio_all_hide != 'on' ) && $only_show_avail == "off" ) { ?>
                                <span class="et_pb_contact_field_radio">
                                  <input type="<?php echo $radio_select ?>" id="<?php echo $product_attribute; ?>_all_<?php echo $num;?>" class="divi-acf input" data-filtertype="<?php echo $checkboxtype ?>" value="" name="<?php echo $product_attribute;?>" data-required_mark="required" data-field_type="radio" data-original_id="radio" <?php echo ($queryvar =='' )?'checked="checked"':'';?>>
                                  <?php if ($radio_style == "tick_box") { echo '<span class="checkmark"></span>'; }?>
                                  <label class="radio-label" data-value="all" for="<?php echo $product_attribute; ?>_all_<?php echo $num;?>" title="All"><i></i><?php echo esc_html__( $radio_all_text, 'divi-ajax-filter' ); ?></label>
                                </span>
                                <?php } ?>
                                <?php
                                if ( $terms ) :
                                  foreach ( $terms as $term ) :
                                    ?>
                                    <span class="et_pb_contact_field_radio">
                                      <input type="<?php echo $radio_select ?>" id="<?php echo $term->term_id; ?>_<?php echo $term->slug . '_' . $num; ?>" class="divi-acf input" data-filtertype="<?php echo $checkboxtype ?>" value="<?php echo $term->slug ?>" name="<?php echo $product_attribute;?>" data-required_mark="required" data-field_type="radio" <?php echo in_array($term->slug, $queryvar)?'checked':'';?> data-original_id="radio">
                                      <?php if ($radio_style == "tick_box") { echo '<span class="checkmark"></span>'; } else {} ?>
                                      <label class="radio-label" data-value="<?php echo $term->slug ?>" for="<?php echo $term->term_id; ?>_<?php echo $term->slug . '_' . $num; ?>" title="<?php echo $term->name;?>"><i></i><?php echo $term->name; ?></label>
                                    </span>
                                    <?php
                                  endforeach;
                                endif;
                                ?>
                              </div>
                            </div>
                          </p>
<?php
                        }
                    }
                } else if ( $filter_post_type == "productprice" ) {
                    $product_cat  = get_query_var( 'product_cat' )?get_query_var('product_cat') : ( !empty($_GET['product_cat'])?$_GET['product_cat'] : '');
                    $queryvar = get_query_var('product_price') ? get_query_var('product_price') : ( !empty( $_GET['product_price'] )? $_GET['product_price'] : '' );
                    global $wpdb;
                    $sql = "SELECT MAX(a.max_price) AS max_price FROM {$wpdb->wc_product_meta_lookup} a JOIN {$wpdb->posts} b ON a.product_id=b.id";
                    if ( !empty( $product_cat ) ) {
                        $sql .= " JOIN {$wpdb->term_relationships} c ON b.id = c.object_id JOIN {$wpdb->term_taxonomy} d ON c.term_taxonomy_id = d.term_taxonomy_id JOIN {$wpdb->terms} e ON d.term_id = e.term_id WHERE e.slug='".$product_cat."'";
                    }
                    $max_price = ceil( $wpdb->get_var( $sql ) );

                    if ( !empty( $range_to ) && $range_to != '800' ) {
                        $max_price = $range_to;
                    }

                    $min_price = (!empty( $range_from ) && $range_from < $max_price)?$range_from:0;

                    $range_from_query = 0;
                    $range_to_query = $max_price;
                    if ( !empty( $queryvar ) ){
                        $price_arr = (explode(";", $queryvar));
                        if ( is_array( $price_arr ) ) {
                            if ( isset( $price_arr[1] ) ) {
                                $range_from_query = $price_arr[0];
                                $range_to_query = $price_arr[1];
                            }else{
                                $range_from_query = $price_arr[0];
                                $range_to_query = '';
                            }
                        }
                    }

                    if ($custom_label !== '') {
                        $custom_label_final = $custom_label;
                    } else {
                        $custom_label_final = 'Price Range';
                    }

?>
                    <div class="et_pb_contact_field" data-type="range">
                        <span class="et_pb_contact_field_options_title <?php echo $label_css;?>"><?php echo esc_html__( $custom_label_final, 'divi-filter' ); ?></span>
                        <div class="divi-filter-item">
                            <input id="<?php echo "price_" . $num;?>" type="input" class="divi-acf js-range-slider" data-filtertype="rangeslider" name="product_price" value="" />
                        </div>
                    </div>
<?php
                    $css_classrend = '.' . $css_class;
?>
            <style>
                <?php echo $css_classrend ?> .irs-bar,
                <?php echo $css_classrend ?> .irs-from,
                <?php echo $css_classrend ?> .irs-to,
                <?php echo $css_classrend ?> .irs-single,
                <?php echo $css_classrend ?> .irs-handle>i:first-child {
                    background-color: <?php echo $range_prim_color; ?> !important;
                }
                <?php echo $css_classrend ?> .irs-from:before,
                <?php echo $css_classrend ?> .irs-to:before,
                <?php echo $css_classrend ?> .irs-single:before {
                    border-top-color: <?php echo $range_prim_color; ?> !important;
                }
                <?php echo $css_classrend ?> .irs-line,
                <?php echo $css_classrend ?> .irs-min,
                <?php echo $css_classrend ?> .irs-max {
                    background-color: <?php echo $range_sec_color; ?> !important;
                }
            </style>
        <script>
            jQuery(document).ready(function($) {

                var from = <?php echo $range_from_query ?>,
                to = <?php echo $range_to_query ?>,
                step = <?php echo $range_step ?>,
                skin = "<?php echo $range_skin ?>",
                hide_min_max = <?php echo $range_hide_min_max ?>,
                hide_from_to = <?php echo $range_hide_from_to ?>,
                prettify_enabled = <?php echo $range_prettify_enabled ?>,
                prettify_separator = "<?php echo $range_prettify_separator ?>",
                prefix = "<?php echo $range_prefix ?>"  // before the number
                postfix = "<?php echo $range_postfix ?>"; // after the number

                var filters = [];

                var filter_item_name_arr = [];
                var filter_item_id_arr = [];
                var filter_item_val_arr = [];
                var filter_input_type_arr = [];

                jQuery('.divi-acf').each(function(i, obj) {
                    var filter_item_name = jQuery(this).attr("name"),
                    filter_item_id = jQuery(this).attr("id"),
                    filter_item_val = jQuery(this).val(),
                    filter_input_type = jQuery(this).attr('type');
                    filter_item_name_arr.push(filter_item_name);
                    filter_item_id_arr.push(filter_item_id);
                    filter_item_val_arr.push(filter_item_val);
                    filter_input_type_arr.push(filter_input_type);
                });

                var filter_item_name = jQuery(".<?php echo $css_class ?> .js-range-slider").attr("name");

                $(".<?php echo $css_class ?> .js-range-slider").ionRangeSlider({
                    type: 'double',
                    min: <?php echo $min_price;?>,
                    max: <?php echo $max_price;?>,
                    from: from,
                    to: to,
                    step: step,
                    skin: skin,
                    hide_min_max: hide_min_max,
                    hide_from_to: hide_from_to,
                    prettify_enabled: prettify_enabled,
                    prettify_separator: prettify_separator,
                    prefix: prefix,
                    postfix: postfix,
                    onFinish: function (data) {
                        if ( $(".<?php echo $css_class ?> .js-range-slider").parents('.et_pb_de_mach_search_posts').length == 0 && $(".<?php echo $css_class ?> .js-range-slider").parents('.updatetype-update_button').length == 0 ){
                              
                              var _this = $(".<?php echo $css_class ?> .js-range-slider");
                              var iris_to = _this.closest(".et_pb_de_mach_search_posts_item").find(".irs-to").text();
                              var irs_from = _this.closest(".et_pb_de_mach_search_posts_item").find(".irs-from").text();
                              if ( _this.closest(".et_pb_de_mach_search_posts_item").hasClass( "filter_params" ) && (irs_from != 0 || iris_to != <?php echo $max_price;?> ) ) {
                                if ( _this.closest(".et_pb_de_mach_search_posts_item").hasClass( "filter_params_yes_title" ) ) {
                                  var filter_param_type = "title";
                                } else {
                                  var filter_param_type = "no-title";
                                }
                                var value = _this.closest(".et_pb_de_mach_search_posts_item").find(".js-range-slider").val(),
                                name = _this.closest(".et_pb_de_mach_search_posts_item").find(".et_pb_contact_field_options_title ").text(),
                                slug = _this.attr("name"),
                                type = 'range',
                                iris_to = _this.closest(".et_pb_de_mach_search_posts_item").find(".irs-to").text(),
                                irs_from = _this.closest(".et_pb_de_mach_search_posts_item").find(".irs-from").text();
                                divi_filter_params(type, slug, value, name, filter_param_type, iris_to, irs_from);
                              } else {
                                if (jQuery('.param-' + _this.attr('name') ).length) {
                                  jQuery('.param-' + _this.attr('name') ).remove();
                                }
                              }
                              divi_find_filters_to_filter();
                        }
                   }
                });

<?php
            if ( !empty( $_GET['product_price'] ) && ( $range_from_query != 0 || $range_to_query != $max_price ) ) {
?>
                var price_slider = $(".<?php echo $css_class ?> .js-range-slider");
                if ( price_slider.closest(".et_pb_de_mach_search_posts_item").hasClass( "filter_params" ) ) {
                    if ( price_slider.closest(".et_pb_de_mach_search_posts_item").hasClass( "filter_params_yes_title" ) ) {
                      var filter_param_type = "title";
                    } else {
                      var filter_param_type = "no-title";
                    }
                    var value = price_slider.closest(".et_pb_de_mach_search_posts_item").find(".js-range-slider").val(),
                    name = price_slider.closest(".et_pb_de_mach_search_posts_item").find(".et_pb_contact_field_options_title ").text(),
                    slug = price_slider.attr("name"),
                    type = 'range',
                    iris_to = price_slider.closest(".et_pb_de_mach_search_posts_item").find(".irs-to").text(),
                    irs_from = price_slider.closest(".et_pb_de_mach_search_posts_item").find(".irs-from").text();
                    divi_filter_params(type, slug, value, name, filter_param_type, iris_to, irs_from);
                }
<?php
            }
?>                
            });
        </script>
<?php
                } else if ( $filter_post_type == "productweight" ) {
                    $product_cat  = get_query_var( 'product_cat' )?get_query_var('product_cat') : ( !empty($_GET['product_cat'])?$_GET['product_cat'] : '');
                    $product_weight = get_query_var('product_weight') ? get_query_var('product_weight') : ( !empty( $_GET['product_weight'] )? $_GET['product_weight'] : '' );
                    global $wpdb;
                    $sql = "SELECT MAX(b.meta_value) FROM {$wpdb->posts} a JOIN (SELECT * FROM {$wpdb->postmeta} WHERE meta_key='_weight') b ON a.id=b.post_id";
                    if ( !empty( $product_cat ) ) {
                        $sql .= " JOIN {$wpdb->term_relationships} c ON a.id = c.object_id JOIN {$wpdb->term_taxonomy} d ON c.term_taxonomy_id = d.term_taxonomy_id JOIN {$wpdb->terms} e ON d.term_id = e.term_id WHERE e.slug='".$product_cat."'";
                    }
                    $max_weight = ceil( $wpdb->get_var( $sql ) );

                    if ( !empty( $range_to ) && $range_to != '800' ) {
                        $max_weight = $range_to;
                    }

                    $min_weight = (!empty( $range_from ) && $range_from < $max_weight)?$range_from:0;

                    $range_from_query = 0;
                    $range_to_query = $max_weight;
                    if ( !empty( $product_weight ) ){
                        $weight_arr = (explode(";", $product_weight));
                        if ( is_array( $weight_arr ) ) {
                            if ( isset( $weight_arr[1] ) ) {
                                $range_from_query = $weight_arr[0];
                                $range_to_query = $weight_arr[1];
                            }else{
                                $range_from_query = $weight_arr[0];
                                $range_to_query = '';
                            }
                        }
                    }

                    if ($custom_label !== '') {
                        $custom_label_final = $custom_label;
                    } else {
                        $custom_label_final = 'Weight Range';
                    }

?>
                    <div class="et_pb_contact_field" data-type="range">
                        <span class="et_pb_contact_field_options_title <?php echo $label_css;?>"><?php echo esc_html__( $custom_label_final, 'divi-filter' ); ?></span>
                        <div class="divi-filter-item">
                            <input id="<?php echo "weight_" . $num;?>" type="input" class="divi-acf js-range-slider" data-filtertype="rangeslider" name="product_weight" value="" />
                        </div>
                    </div>
<?php
                    $css_classrend = '.' . $css_class;
?>
            <style>
                <?php echo $css_classrend ?> .irs-bar,
                <?php echo $css_classrend ?> .irs-from,
                <?php echo $css_classrend ?> .irs-to,
                <?php echo $css_classrend ?> .irs-single,
                <?php echo $css_classrend ?> .irs-handle>i:first-child {
                    background-color: <?php echo $range_prim_color; ?> !important;
                }
                <?php echo $css_classrend ?> .irs-from:before,
                <?php echo $css_classrend ?> .irs-to:before,
                <?php echo $css_classrend ?> .irs-single:before {
                    border-top-color: <?php echo $range_prim_color; ?> !important;
                }
                <?php echo $css_classrend ?> .irs-line,
                <?php echo $css_classrend ?> .irs-min,
                <?php echo $css_classrend ?> .irs-max {
                    background-color: <?php echo $range_sec_color; ?> !important;
                }
            </style>
        <script>
            jQuery(document).ready(function($) {

                var from = <?php echo $range_from_query ?>,
                to = <?php echo $range_to_query ?>,
                step = <?php echo $range_step ?>,
                skin = "<?php echo $range_skin ?>",
                hide_min_max = <?php echo $range_hide_min_max ?>,
                hide_from_to = <?php echo $range_hide_from_to ?>,
                prettify_enabled = <?php echo $range_prettify_enabled ?>,
                prettify_separator = "<?php echo $range_prettify_separator ?>",
                prefix = "<?php echo $range_prefix ?>"  // before the number
                postfix = "<?php echo $range_postfix ?>"; // after the number

                var filters = [];

                var filter_item_name_arr = [];
                var filter_item_id_arr = [];
                var filter_item_val_arr = [];
                var filter_input_type_arr = [];

                jQuery('.divi-acf').each(function(i, obj) {
                    var filter_item_name = jQuery(this).attr("name"),
                    filter_item_id = jQuery(this).attr("id"),
                    filter_item_val = jQuery(this).val(),
                    filter_input_type = jQuery(this).attr('type');
                    filter_item_name_arr.push(filter_item_name);
                    filter_item_id_arr.push(filter_item_id);
                    filter_item_val_arr.push(filter_item_val);
                    filter_input_type_arr.push(filter_input_type);
                });

                var filter_item_name = jQuery(".<?php echo $css_class ?> .js-range-slider").attr("name");

                $(".<?php echo $css_class ?> .js-range-slider").ionRangeSlider({
                    type: 'double',
                    min: <?php echo $min_weight;?>,
                    max: <?php echo $max_weight;?>,
                    from: from,
                    to: to,
                    step: step,
                    skin: skin,
                    hide_min_max: hide_min_max,
                    hide_from_to: hide_from_to,
                    prettify_enabled: prettify_enabled,
                    prettify_separator: prettify_separator,
                    prefix: prefix,
                    postfix: postfix,
                    onFinish: function (data) {
                        if ( $(".<?php echo $css_class ?> .js-range-slider").parents('.et_pb_de_mach_search_posts').length == 0 && $(".<?php echo $css_class ?> .js-range-slider").parents('.updatetype-update_button').length == 0 ){
                              
                              var _this = $(".<?php echo $css_class ?> .js-range-slider");
                              var iris_to = _this.closest(".et_pb_de_mach_search_posts_item").find(".irs-to").text();
                              var irs_from = _this.closest(".et_pb_de_mach_search_posts_item").find(".irs-from").text();
                              if ( _this.closest(".et_pb_de_mach_search_posts_item").hasClass( "filter_params" ) && (irs_from != 0 || iris_to != <?php echo $max_weight;?> ) ) {
                                if ( _this.closest(".et_pb_de_mach_search_posts_item").hasClass( "filter_params_yes_title" ) ) {
                                  var filter_param_type = "title";
                                } else {
                                  var filter_param_type = "no-title";
                                }
                                var value = _this.closest(".et_pb_de_mach_search_posts_item").find(".js-range-slider").val(),
                                name = _this.closest(".et_pb_de_mach_search_posts_item").find(".et_pb_contact_field_options_title ").text(),
                                slug = _this.attr("name"),
                                type = 'range',
                                iris_to = _this.closest(".et_pb_de_mach_search_posts_item").find(".irs-to").text(),
                                irs_from = _this.closest(".et_pb_de_mach_search_posts_item").find(".irs-from").text();
                                divi_filter_params(type, slug, value, name, filter_param_type, iris_to, irs_from);
                              } else {
                                if (jQuery('.param-' + _this.attr('name') ).length) {
                                  jQuery('.param-' + _this.attr('name') ).remove();
                                }
                              }
                              divi_find_filters_to_filter();
                        }
                    }
                });

<?php
            if ( !empty( $_GET['product_weight'] ) && ( $range_from_query != 0 || $range_to_query != $max_weight ) ) {
?>
                var weight_slider = $(".<?php echo $css_class ?> .js-range-slider");
                if ( weight_slider.closest(".et_pb_de_mach_search_posts_item").hasClass( "filter_params" ) ) {
                    if ( weight_slider.closest(".et_pb_de_mach_search_posts_item").hasClass( "filter_params_yes_title" ) ) {
                      var filter_param_type = "title";
                    } else {
                      var filter_param_type = "no-title";
                    }
                    var value = weight_slider.closest(".et_pb_de_mach_search_posts_item").find(".js-range-slider").val(),
                    name = weight_slider.closest(".et_pb_de_mach_search_posts_item").find(".et_pb_contact_field_options_title ").text(),
                    slug = weight_slider.attr("name"),
                    type = 'range',
                    iris_to = weight_slider.closest(".et_pb_de_mach_search_posts_item").find(".irs-to").text(),
                    irs_from = weight_slider.closest(".et_pb_de_mach_search_posts_item").find(".irs-from").text();
                    divi_filter_params(type, slug, value, name, filter_param_type, iris_to, irs_from);
                }
<?php
            }
?>                
            });
        </script>
<?php
                } else if ( $filter_post_type == "productrating" ) {

                    $queryvar = get_query_var('product_rating') ? get_query_var('product_rating') : ( !empty( $_GET['product_rating'] )? $_GET['product_rating'] : '' );

                    if ($custom_label !== '') {
                      $custom_label_final = $custom_label;
                    } else {
                      $custom_label_final = 'Product Rating';
                    }

                    $terms = array(0, 1, 2, 3, 4, 5);

                    if ( $attribute_swatch == 'on' ){
?>
                    <div class="et_pb_contact_field divi-swatch divi-swatch-full" data-type="radio" data-filtertype="radio">
                        <div class="et_pb_contact_field_options_wrapper">
                            <span class="et_pb_contact_field_options_title <?php echo $label_css ?>"><?php echo esc_html__( $custom_label_final, 'divi-ajax-filter' ); ?></span>
                            <div class="et_pb_contact_field_options_list divi-filter-item divi-checkboxsingle" data-filter-count="<?php echo $radio_show_count;?>" data-show-empty="<?php echo $radio_show_empty;?>" data-filter-option="product_rating">
                                <span class="et_pb_contact_field_radio remove_filter">
                                    <input type="<?php echo $radio_select ?>" id="product_rating_all_<?php echo $num;?>" class="divi-acf input" data-filtertype="divi-checkboxsingle" value="" name="product_rating" data-required_mark="required" data-field_type="radio" data-original_id="radio" <?php checked( $queryvar, '' );?>>
                                    <label class="radio-label" data-value="all" for="product_rating_all_<?php echo $num;?>"><i></i><?php echo esc_html__( 'Remove Filter', 'divi-ajax-filter' ); ?></label>
                                </span>
<?php                        
                            if ( !empty( $terms ) ) {
                                foreach ($terms as $term) {
?>
                                <span class="et_pb_contact_field_radio">
                                    <input type="<?php echo $radio_select ?>" id="product_rating_<?php echo $term . '_' . $num ?>" class="divi-acf input" data-filtertype="divi-checkboxsingle" value="<?php echo $term; ?>" name="product_rating" data-required_mark="required" data-field_type="radio" data-original_id="radio" <?php checked( $queryvar, $term );?>>
                                    <label class="radio-label" data-value="<?php echo $term ?>" title="product_rating" for="product_rating_<?php echo $term . '_' . $num ?>">
                                        <div class="star-rating" role="img">
                                            <span style="width:<?php echo 20 * $term;?>%"></span>
                                        </div>
                                    </label>
                                </span>
<?php
                                }
                            }
?>
                            </div>
                        </div>
                    </div>
<?php                        
                    }else{
                        if ($acf_filter_type == "select") {
?>
                        <p class="et_pb_contact_field_options_title <?php echo $label_css ?>"><?php echo esc_html__( $custom_label_final, 'divi-filter' ); ?></p>
                        <div class="et_pb_contact_field divi-filter-item" data-type="select" data-field_type="select">
                            <select id="<?php echo 'product_rating_' . $num; ?>" class="divi-acf et_pb_contact_select" data-filtertype="divi-checkboxsingle" name="product_rating">
                                <option value="" <?php selected( $queryvar, '' );?>><?php echo $select_placeholder ?></option>
                            <?php
                                if ( !empty($terms) ) :
                                    foreach ( $terms as $term ) :
                            ?>
                                <option id="<?php echo 'product_rating_'.$term.'_' . $num; ?>" value="<?php echo $term; ?>" <?php selected( $queryvar, $term );?>>
                                    <?php echo ($term == 0)?esc_html__('No Rating', 'divi-ajax-filter'):sprintf(esc_html__('%d Rating(s)', 'divi-ajax-filter'), $term); ?>
                                </option>
                        <?php
                                    endforeach;
                                endif;
                        ?>
                            </select>
                        </div>
<?php
                        } else if ( $acf_filter_type == "radio" ) {
                            if ($radio_select == "checkbox") {
                                $checkboxtype = "divi-checkboxmulti";
                            } else {
                                $checkboxtype = "divi-checkboxsingle";
                            }
                            $queryvar = explode(',', $queryvar);
    ?>
                          <p class="et_pb_contact_field" data-type="radio" data-filtertype="radio">
                            <div class="et_pb_contact_field_options_wrapper divi-radio-<?php echo $radio_style ?>">
                              <span class="et_pb_contact_field_options_title <?php echo $label_css ?>"><?php echo esc_html__( $custom_label_final, 'divi-filter' ); ?></span>
                              <div class="et_pb_contact_field_options_list divi-filter-item <?php echo $checkboxtype ?>" data-filter-option="product_rating">
                                <?php if ( ( $radio_select != 'checkbox') && ($radio_all_hide != 'on' ) && $only_show_avail == "off" ) { ?>
                                <span class="et_pb_contact_field_radio">
                                  <input type="<?php echo $radio_select ?>" id="product_rating_all_<?php echo $num;?>" class="divi-acf input" data-filtertype="<?php echo $checkboxtype ?>" value="" name="product_rating" data-required_mark="required" data-field_type="radio" data-original_id="radio" <?php echo ($queryvar =='' )?'checked="checked"':'';?>>
                                  <?php if ($radio_style == "tick_box") { echo '<span class="checkmark"></span>'; }?>
                                  <label class="radio-label" data-value="all" for="product_rating_all_<?php echo $num;?>" title="All"><i></i><?php echo esc_html__( $radio_all_text, 'divi-ajax-filter' ); ?></label>
                                </span>
                                <?php } ?>
                                <?php
                                if ( $terms ) :
                                  foreach ( $terms as $term ) :
                                    ?>
                                    <span class="et_pb_contact_field_radio">
                                      <input type="<?php echo $radio_select ?>" id="product_rating_<?php echo $term . '_' . $num; ?>" class="divi-acf input" data-filtertype="<?php echo $checkboxtype ?>" value="<?php echo $term ?>" name="product_rating" data-required_mark="required" data-field_type="radio" <?php echo in_array($term, $queryvar)?'checked':'';?> data-original_id="radio">
                                      <?php if ($radio_style == "tick_box") { echo '<span class="checkmark"></span>'; } else {} ?>
                                      <label class="radio-label" data-value="<?php echo $term ?>" for="product_rating_<?php echo $term . '_' . $num; ?>" title="<?php echo $term;?>"><i></i><?php echo ($term == 0)?esc_html__('No Rating', 'divi-ajax-filter'):sprintf(esc_html__('%d Rating(s)', 'divi-ajax-filter'), $term); ?></label>
                                    </span>
                                    <?php
                                  endforeach;
                                endif;
                                ?>
                              </div>
                            </div>
                          </p>
<?php
                        }
                    }
                } else if ($filter_post_type == "sort") {
                } else if ($filter_post_type == "date") {
                } else {
                }
?>
                </div>
                <?php
                $data = ob_get_clean();

                //////////////////////////////////////////////////////////////////////

                return $data;
            }
        }

        new et_pb_df_product_filter_item_code;
        }
    }
}