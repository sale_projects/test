<?php
if ( ! defined( 'ABSPATH' ) ) exit; 

if ( !function_exists("Divi_filter_machine_title_module_import") ){
  add_action( 'et_builder_ready', 'Divi_filter_machine_title_module_import');
  
  function Divi_filter_machine_title_module_import(){
    if(class_exists("ET_Builder_Module") && !class_exists("de_mach_title_code") && !class_exists("et_pb_db_product_title")){
      class de_filter_machine_title_code extends ET_Builder_Module {

        
        public $vb_support = 'on';
        protected $module_credits = array(
          'module_uri' => DE_DF_PRODUCT_URL,
          'author'     => DE_DF_AUTHOR,
          'author_uri' => DE_DF_URL,
        );
        
        function init() {
          if (defined('DE_DMACH_VERSION')) {
            $this->name       = esc_html__( '.Post Title - Divi Machine', 'divi-machine' );
            $this->slug = 'et_pb_de_mach_title';
          } else {
            $this->name       = esc_html__( '.Post Title - Archive Pages', 'divi-filter' );
            $this->slug = 'et_pb_df_title';
          }
          
          $this->fields_defaults = array(
            // 'loop_layout'         => array( 'on' ),
			'background_layout' => array( 'light' ),
      'link_product'   => array( 'off' ),
          );
          $this->settings_modal_toggles = array(
      			'general' => array(
      				'toggles' => array(
      					'main_content' => esc_html__( 'Main Options', 'divi-machine' ),
      					'visual_builder' => esc_html__( 'Visual Builder', 'divi-machine' ),
      				),
      			),
      			'advanced' => array(
      				'toggles' => array(
      					'text' => esc_html__( 'Text', 'divi-machine' ),
      				),
      			),

          );
          
          $this->main_css_element = '%%order_class%%';
          $this->advanced_fields = array(
            'fonts' => array(
              'title' => array(
                'label'    => esc_html__( 'Title', 'divi-machine' ),
                'css'      => array(
                  'main' => "{$this->main_css_element} .de_title_module",
                  'important' => 'all',
                ),
                'font_size' => array(
                  'default' => '30px',
                ),
                'line_height' => array(
                  'default' => '1em',
                ),
              ),
              'header'   => array(
                'label'    => esc_html__( 'Product Title', 'divi-bodyshop-woocommerce' ),
                'css'      => array(
                  'main' => "{$this->main_css_element} .product_title",
                    'important' => 'all',
                ),
                'font_size' => array(
                  'default' => '30px',
                ),
                'line_height' => array(
                  'default' => '1em',
                ),
              ),
            ),
            'background' => array(
              'settings' => array(
                'color' => 'alpha',
              ),
            ),
            'custom_margin_padding' => array(
              'css' => array(
                'main' => "{$this->main_css_element} .entry-title",
                'important' => 'all',
              ),
            ),
            'button' => array(

            ),
            'box_shadow' => array(
              'default' => array(),
              'product' => array(
                'label' => esc_html__( 'Box Shadow', 'divi-machine' ),
                'css' => array(
                  'main' => "%%order_class%%",
                ),
                'option_category' => 'layout',
                'tab_slug'        => 'advanced',
                'toggle_slug'     => 'product',
              ),
            ),
            'margin_padding' => array(
              'css'           => array(
                'main' => "%%order_class%%",
                'important' => 'all',
              ),
            ),
          );
          
          $this->custom_css_fields = array(

          );
          
          $this->help_videos = array(

          );
        }
        
        function get_fields() {
          if (defined('DE_DB_WOO_VERSION')) {
          $options_posttype = DEBC_INIT::get_divi_post_types();
          } else {
            $options_posttype = DE_Filter::get_divi_post_types();
          }
          
          $fields = array(
            'title_tag' => array(
              'label'       => __( 'Title HTML Tag', 'et_builder' ),
              'type'        => 'select',
              'options'     => array(
                "h1"=>"h1",
                "h2"=>"h2",
                "h3"=>"h3",
                "h4"=>"h4",
                "h5"=>"h5",
                "h6"=>"h6",
                "p"=>"p"
              ),
              'default'     => 'h1',
              'description' => __( 'Set the title tag. For example you may want it to be h3 or h4 on the custom layout.', 'et_builder' ),
            ),
            'link_product' => array(
              'label'             => esc_html__( 'Link Title to Single Page', 'et_builder' ),
              'type'              => 'yes_no_button',
              'option_category'   => 'layout',
              'option_category'   => 'configuration',
              'options'           => array(
                'on'  => esc_html__( 'Yes', 'et_builder' ),
                'off' => esc_html__( 'No', 'et_builder' ),
              ),
              'affects'=>array(
                  'new_tab'
              ),
              'description'        => esc_html__( 'Enable this if you want to allow the user to click on the title to go to the single/product page.', 'et_builder' ),
            ),
            'new_tab' => array(
              'label'             => esc_html__( 'Open In New Tab?', 'et_builder' ),
              'type'              => 'yes_no_button',
              'option_category'   => 'layout',
              'option_category'   => 'configuration',
              'options'           => array(
                'on'  => esc_html__( 'Yes', 'et_builder' ),
                'off' => esc_html__( 'No', 'et_builder' ),
              ),
              'default'            => 'off',
              'depends_show_if'   => 'on',
              'description'        => esc_html__( 'Enable this if you want the link to open in a new tab.', 'et_builder' ),
            ),
            'vb_posttype' => array(
              'toggle_slug'       => 'visual_builder',
              'label'       => __( 'Visual Builder Post', 'et_builder' ),
              'type'        => 'select',
              'computed_affects' => array(
                '__getposttitle',
              ),
              'options'     => $options_posttype,
              'default'     => 'none',
              'description' => __( 'Set the post type you want to display in the Visual builder - we will look for the first post in this post type to get the data.', 'et_builder' ),
            ),
            '__getposttitle' => array(
              'type' => 'computed',
              'computed_callback' => array( 'de_filter_machine_title_code', 'get_post_title' ),
              'computed_depends_on' => array(
                'vb_posttype',
              ),
            ),
          );
          
          return $fields;
        }
        
        public static function get_post_title ( $args = array(), $conditional_tags = array(), $current_page = array() ){
          
          if (!is_admin()) {
            return;
          }

          
          $vb_posttype  = $args['vb_posttype'];
          
          ob_start();
          if ($vb_posttype == "none") {
            if ( defined('DE_DB_WOO_VERSION')) {
              $default_post = "product";
            } else {
              $default_post = "post";
            }
          } else {
            $default_post = $vb_posttype;
          }
          
          $get_cpt_args = array(
            'post_type' => $default_post,
            'post_status' => 'publish',
            'posts_per_page' => '1',
            'orderby' => 'ID',
            'order' => 'ASC',
          );
          
          query_posts( $get_cpt_args );
          $first = true;

          if ( have_posts() ) {
            while ( have_posts() ) {
              the_post();
              // setup_postdata( $post );
              
              if ( $first )  {

                //////////////////////////////////////////////////
                echo get_the_title();
                //////////////////////////////////////////////////
                $first = false;
              } else {

              }
            }
          }
          
          $data = ob_get_clean();
          
          return $data;

        }
        
        function render( $attrs, $content = null, $render_slug ) {
          
          $link_product		= $this->props['link_product'];
          $title_tag		= $this->props['title_tag'];
          $new_tab		= $this->props['new_tab'];
          
          if ($title_tag == "") {
            $title_tag = "h1";
          } else {
            $title_tag = $title_tag;
          }
          

          if ($new_tab == "on") {
            $new_tab_dis = 'target="_blank"';
          } else {
            $new_tab_dis = '';
          }
          //////////////////////////////////////////////////////////////////////
          
          ob_start();

          if (class_exists('woocommerce') && get_post_type() == "product") {
            $class_name = "product_title";
          } else if (defined('DE_DMACH_VERSION')) {
            $class_name = "dmach-post-title";
          } else {
            $class_name = "df-post-title";
          }
          
          if ($link_product == 'on' ) {
            if (class_exists('woocommerce') && get_post_type() == "product") {
              global $product, $woocommerce;
              if ( ! is_a( $product, 'WC_Product' ) ) {
                return;
              }
              $product_id = $product->get_id();
              $url = get_permalink( $product_id );
            } else {
              $url = get_permalink( get_the_ID() );
            }
            echo '<a href="'.$url.'" '.$new_tab_dis.'>';
          }
          
          echo '<'.$title_tag.' itemprop="name" class="entry-title de_title_module '.$class_name.'">';
          echo get_the_title();
          echo '</'.$title_tag.'>';
          
          if ($link_product == 'on' ) {
            echo '</a>';
          }
          
          $data = ob_get_clean();
          //////////////////////////////////////////////////////////////////////
          return $data;
        
        }
      
      }
      
      new de_filter_machine_title_code;
    
    }
  }
}
