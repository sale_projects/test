<?php
if ( ! defined( 'ABSPATH' ) ) exit;

if ( !function_exists("Divi_filter_machine_loop_module_import") ){
    add_action( 'et_builder_ready', 'Divi_filter_machine_loop_module_import');

    function Divi_filter_machine_loop_module_import(){
        if(class_exists("ET_Builder_Module") && !class_exists("de_mach_archive_loop_code")){
            class de_mach_archive_loop_code extends ET_Builder_Module {

                public $vb_support = 'on';

                protected $module_credits = array(
                    'module_uri' => DE_DF_PRODUCT_URL,
                    'author'     => DE_DF_AUTHOR,
                    'author_uri' => DE_DF_URL,
                );

                function init() {
                    $this->name       = esc_html__( '.Archive Loop - Divi Machine', 'divi-filter' );
                    $this->slug = 'et_pb_de_mach_archive_loop';


                    $this->fields_defaults = array(
                      // 'loop_layout'         => array( 'on' ),
                    );

                    $this->settings_modal_toggles = array(
                      'general' => array(
                        'toggles' => array(
                          'main_content' => esc_html__( 'Main Options', 'divi-filter' ),
                          'loop_options' => esc_html__( 'Loop Options', 'divi-filter' ),
                          'element_options' => esc_html__( 'Element Options', 'divi-filter' ),
                          'grid_options' => esc_html__( 'Grid Options', 'divi-filter' ),
                          'extra_options' => esc_html__( 'Extra Options', 'divi-filter' ),
                        ),
                      ),
                      'advanced' => array(
                        'toggles' => array(
                          'alignment'  => esc_html__( 'Alignment', 'et_builder' ),
                          'text' => esc_html__( 'Text', 'divi-filter' ),
                          'overlay' => esc_html__( 'Overlay', 'divi-filter' ),
                        ),
                      ),
                    );


                    $this->main_css_element = '%%order_class%%';


                    $this->advanced_fields = array(
                      'fonts' => array(
                        'title' => array(
                          'label'    => esc_html__( 'Default Layout - Title', 'divi-filter' ),
                          'css'      => array(
                            'main' => "%%order_class%% ul.products li.product .woocommerce-loop-product__title",
                            'important' => 'plugin_only',
                          ),
                          'font_size' => array(
                            'default' => '14px',
                          ),
                          'line_height' => array(
                            'default' => '1em',
                          ),
                        ),
                        'excerpt' => array(
                          'label'    => esc_html__( 'Default Layout - Excerpt', 'divi-filter' ),
                          'css'      => array(
                            'main' => "%%order_class%% ul.products li.product .woocommerce-product-details__short-description",
                            'important' => 'plugin_only',
                          ),
                          'font_size' => array(
                            'default' => '14px',
                          ),
                          'line_height' => array(
                            'default' => '1em',
                          ),
                        ),
                      ),
                      'background' => array(
                        'settings' => array(
                          'color' => 'alpha',
                        ),
                      ),
                      'button' => array(
                        'button' => array(
                          'label' => esc_html__( 'Button - Load More', 'divi-filter' ),
                          'css' => array(
                            'main' => "{$this->main_css_element} .et_pb_button.dmach-loadmore",
                            'important' => 'all',
                          ),
                          'box_shadow'  => array(
                            'css' => array(
                              'main' => "{$this->main_css_element} .et_pb_button.dmach-loadmore",
                              'important' => 'all',
                            ),
                          ),
                          'margin_padding' => array(
                            'css'           => array(
                              'main' => "{$this->main_css_element} .et_pb_button.dmach-loadmore",
                              'important' => 'all',
                            ),
                          ),
                        ),
                      ),
                      'box_shadow' => array(
                        'default' => array(),
                        'product' => array(
                          'label' => esc_html__( 'Default Layout - Box Shadow', 'divi-filter' ),
                          'css' => array(
                            'main' => "%%order_class%% .products .product",
                          ),
                          'option_category' => 'layout',
                          'tab_slug'        => 'advanced',
                          'toggle_slug'     => 'product',
                        ),
                      ),
                    );


                    $this->custom_css_fields = array(
                      'image' => array(
                        'label'    => esc_html__( 'Default Layout - Image', 'divi-filter' ),
                        'selector' => '%%order_class%% .et_shop_image',
                      ),
                      'overlay' => array(
                        'label'    => esc_html__( 'Default Layout - Overlay', 'divi-filter' ),
                        'selector' => '%%order_class%% .et_overlay,  %%order_class%% .et_pb_extra_overlay',
                      ),
                      'title' => array(
                        'label'    => esc_html__( 'Default Layout - Title', 'divi-filter' ),
                        'selector' => '%%order_class%% .woocommerce-loop-product__title',
                      ),
                    );


                    $this->help_videos = array(
                    );

                    add_filter( 'et_pb_set_style_selector', array( $this, 'change_section_css_selector' ), 10, 2 );
              }

              function get_fields() {

                $options = array();

                $layout_query = array(
                  'post_type'=>'et_pb_layout',
                  'posts_per_page'=>-1,
                  'meta_query' => array(
                    array(
                      'key' => '_et_pb_predefined_layout',
                      'compare' => 'NOT EXISTS',
                    ),
                  )
                );
                $options['none'] = 'No Layout (please choose one)';
                if ($layouts = get_posts($layout_query)) {
                  foreach ($layouts as $layout) {
                    $options[$layout->ID] = $layout->post_title;
                  }
                }

                ///////////////////////////////

                $acf_fields = DEDMACH_INIT::get_acf_fields();

                //////////////////////////////
                $fields = array(
                  'post_type_choose' => array(
                    'toggle_slug'       => 'main_content',
                    'label'             => esc_html__( 'Post Type', 'divi-filter' ),
                    'type'              => 'select',
                    'options'           => et_get_registered_post_type_options( false, false ),
                    'option_category'   => 'configuration',
                    'default'           => 'post',
                    'computed_affects' => array(
                      '__getarchiveloop',
                    ),
                    'description'       => esc_html__( 'Choose the post type you want to display', 'divi-filter' ),
                  ),
                  'loop_layout' => array(
                    'toggle_slug'       => 'main_content',
                    'label'             => esc_html__( 'Custom Loop Layout', 'divi-filter' ),
                    'type'              => 'select',
                    'option_category'   => 'configuration',
                    'default'           => 'none',
                    'computed_affects' => array(
                      '__getarchiveloop',
                    ),
                    'options'           => $options,
                    'description'        => esc_html__( 'Choose the layout you have made for each post in the loop.', 'divi-filter' ),
                  ),
                  'filter_update_animation' => array(
                    'toggle_slug'       => 'main_content',
                    'label'             => esc_html__( 'Filter Update Animation Style', 'divi-filter' ),
                    'type'              => 'select',
                    'options'           => array(
                      'load-1'       => esc_html__( 'Three Lines Vertical', 'divi-filter' ),
                      'load-2'       => esc_html__( 'Three Lines Horizontal', 'divi-filter' ),
                      'load-3'       => esc_html__( 'Three Dots Bouncing', 'divi-filter' ),
                      'load-4'       => esc_html__( 'Donut', 'divi-filter' ),
                      'load-5'       => esc_html__( 'Donut Multiple', 'divi-filter' ),
                      'load-6'       => esc_html__( 'Ripple', 'divi-filter' ),
                    ),
                    'option_category'   => 'configuration',
                    'default'           => 'load-6',
                    'description'       => esc_html__( 'Choose the animation style for when loading the posts', 'divi-filter' ),
                  ),
                  'animation_color' => array(
                    'label'             => esc_html__( 'Animation Icon Color', 'et_builder' ),
                    'description'       => esc_html__( 'Define the color of the animation you choose above.', 'et_builder' ),
                    'type'              => 'color-alpha',
                    'custom_color'      => true,
                    'option_category'   => 'configuration',
                    'toggle_slug'       => 'main_content',
                  ),
                  'loading_bg_color' => array(
                    'label'             => esc_html__( 'Loading Background Color', 'et_builder' ),
                    'description'       => esc_html__( 'Define the color of the background when it is loading.', 'et_builder' ),
                    'type'              => 'color-alpha',
                    'custom_color'      => true,
                    'option_category'   => 'configuration',
                    'toggle_slug'       => 'main_content',
                  ),
                  'no_posts_layout' => array(
                    'toggle_slug'       => 'main_content',
                    'label'             => esc_html__( 'No Posts Layout', 'divi-filter' ),
                    'type'              => 'select',
                    'option_category'   => 'configuration',
                    'default'           => 'none',
                    'options'           => $options,
                    'description'        => esc_html__( 'Choose the layout that will be shown if there are no posts in the selection.', 'divi-filter' ),
                  ),
                  'is_main_loop'  => array(
                    'toggle_slug'       => 'main_content',
                    'label'             => esc_html__( 'Is Main Loop?', 'divi-filter' ),
                    'type'              => 'yes_no_button',
                    'option_category'   => 'configuration',
                    'options'           => array(
                      'on'  => esc_html__( 'Yes', 'et_builder' ),
                      'off' => esc_html__( 'No', 'et_builder' ),
                    ),
                    'default'           => 'on',
                    'description'       => esc_html__( 'Choose if you want to make this loop as main loop on the page - the filter will affect this loop.', 'divi-filter' ),
                  ),
                  // LOOP SETTINGS
                  'post_status' => array(
                    'toggle_slug'       => 'loop_options',
                    'option_category'   => 'configuration',
                    'label'             => esc_html__( 'Post Status', 'divi-filter' ),
                    'type'              => 'select',
                    'options'           => array(
                      'publish' => sprintf( esc_html__( 'Publish', 'divi-filter' ) ),
                      'pending' => esc_html__( 'Pending', 'divi-filter' ),
                      'draft' => esc_html__( 'Draft', 'divi-filter' ),
                      'auto-draft' => esc_html__( 'Auto-draft', 'divi-filter' ),
                      'future' => esc_html__( 'Future', 'divi-filter' ),
                      'private' => esc_html__( 'Private', 'divi-filter' ),
                      'inherit' => esc_html__( 'Inherit', 'divi-filter' ),
                    ),
                    'default' => 'publish',
                    'computed_affects' => array(
                      '__getarchiveloop',
                    ),
                    'description'       => esc_html__( 'Choose the status of the posts you want to show.', 'divi-filter' ),
                  ),
                  'posts_number' => array(
                    'toggle_slug'       => 'loop_options',
                    'option_category'   => 'configuration',
                    'label'             => esc_html__( 'Post Count', 'divi-filter' ),
                    'type'              => 'text',
                    'default'           => 10,
                    'computed_affects' => array(
                      '__getarchiveloop',
                    ),
                    'description'       => esc_html__( 'Choose how many posts you would like to display per page."', 'divi-filter' ),
                  ),
                  'post_display_type' => array(
                    'toggle_slug'       => 'loop_options',
                    'option_category'   => 'configuration',
                    'label'             => esc_html__( 'Post Display Type', 'divi-filter' ),
                    'type'              => 'select',
                    'options'           => array(
                      'default' => sprintf( esc_html__( 'Default', 'divi-filter' ) ),
                      'related' => esc_html__( 'Related', 'divi-filter' ),
                      'linked_post' => esc_html__( 'Linked Post', 'divi-filter' ),
                    ),
                    'affects'         => array(
                      'related_content',
                      'acf_linked_acf'
                    ),
                    'default' => 'default',
                    'description'       => esc_html__( 'Choose the display type. If you want to have related posts for example, we will find posts in the same categories or tags to show.', 'divi-filter' ),
                  ),




                  'acf_linked_acf' => array(
                    'toggle_slug'       => 'loop_options',
                    'label'             => esc_html__( 'Linked Post Object ACF Name', 'divi-filter' ),
                    'type'              => 'select',
                    'options'           => $acf_fields,
                    'default'           => 'none',
                    'depends_show_if' => 'linked_post',
                    'option_category'   => 'configuration',
                    'description'       => esc_html__( 'Select the Post Object that you have used to link this post to another', 'divi-filter' ),
                  ),



                  'related_content' => array(
                    'toggle_slug'       => 'loop_options',
                    'option_category'   => 'configuration',
                    'label'             => esc_html__( 'Related Content', 'divi-filter' ),
                    'type'              => 'select',
                    'options'           => array(
                      'categories' => sprintf( esc_html__( 'Categories', 'divi-filter' ) ),
                      'tags' => esc_html__( 'Tags', 'divi-filter' ),
                      'post-object' => esc_html__( 'Post Object', 'divi-filter' ),
                    ),
                    'default' => 'categories',
                    'affects'         => array(
                      'acf_name_related',
                    ),
                    'depends_show_if' => 'related',
                    'description'       => esc_html__( 'Choose what would define the posts to be related.', 'divi-filter' ),
                  ),


                  
                  'acf_name_related' => array(
                    'toggle_slug'       => 'loop_options',
                    'label'             => esc_html__( 'Post Object ACF Name', 'divi-filter' ),
                    'type'              => 'select',
                    'options'           => $acf_fields,
                    'default'           => 'none',
                    'depends_show_if' => 'post-object',
                    'option_category'   => 'configuration',
                    'description'       => esc_html__( 'Select the Post Object that you want your related posts to look at to show these posts', 'divi-filter' ),
                  ),
                  'include_cats' => array(
                    'toggle_slug'       => 'loop_options',
                    'option_category'   => 'configuration',
                    'label'           => esc_html__( 'Include Categories (comma-seperated)', 'divi-filter' ),
                    'type'            => 'text',
                    'computed_affects' => array(
                      '__getarchiveloop',
                    ),
                    'description'     => esc_html__( 'Add a list of categories you want to include to show. This will remove all posts that dont have these categories. (comma-seperated)', 'divi-filter' ),
                  ),
                  'include_tags' => array(
                    'toggle_slug'       => 'loop_options',
                    'option_category'   => 'configuration',
                    'label'           => esc_html__( 'Include Tags (comma-seperated)', 'divi-filter' ),
                    'type'            => 'text',
                    'computed_affects' => array(
                      '__getarchiveloop',
                    ),
                    'description'     => esc_html__( 'Add a list of tags that you want to include to show. This will remove all posts that dont have these tags. (comma-seperated)', 'divi-filter' ),
                  ),
                  'exclude_cats' => array(
                    'toggle_slug'       => 'loop_options',
                    'option_category'   => 'configuration',
                    'label'           => esc_html__( 'Exclude Categories (comma-seperated)', 'divi-filter' ),
                    'type'            => 'text',
                    'computed_affects' => array(
                      '__getarchiveloop',
                    ),
                    'description'     => esc_html__( 'Add a list of categories you want to exclude to show. This will remove all posts that have these categories. (comma-seperated)', 'divi-filter' ),
                  ),
                  'exclude_products'      => array(
                    'label'             => esc_html__( 'Exclude Posts', 'divi-filter' ),
                    'type'              => 'text',
                    'option_category'   => 'configuration',
                    'toggle_slug'       => 'loop_options',
                    'description'       => esc_html__( 'Add a list of post ids that you want to exclude from show. (comma-seperated)', 'divi-filter' ),
                  ),
                  'custom_tax_choose' => array(
                  'toggle_slug'       => 'loop_options',
                    'label'             => esc_html__( 'Choose Your Taxonomy', 'divi-filter' ),
                    'type'              => 'select',
                    'options'           => get_taxonomies( array( '_builtin' => FALSE ) ),
                    'option_category'   => 'configuration',
                    'default'           => 'post',
            'depends_show_if'  => 'taxonomy',
                    'description'       => esc_html__( 'Choose the custom taxonomy that you have made and want to filter', 'divi-filter' ),
                  ),
                  'include_taxomony' => array(
                    'toggle_slug'       => 'loop_options',
                    'option_category'   => 'configuration',
                    'label'           => esc_html__( 'Include Custom Taxonomy (comma-seperated)', 'divi-filter' ),
                    'type'            => 'text',
                    'computed_affects' => array(
                      '__getarchiveloop',
                    ),
                    'description'     => esc_html__( 'Add a list of values that you want to show - make sure to specify the custom taxonomy above, it will then show the posts that have the values here from that custom taxonomy. (comma-seperated)', 'divi-filter' ),
                  ),
                  'acf_name' => array(
                    'toggle_slug'       => 'loop_options',
                    'label'             => esc_html__( 'Filter By ACF Name', 'divi-filter' ),
                    'type'              => 'select',
                    'options'           => $acf_fields,
                    'default'           => 'none',
                    'option_category'   => 'configuration',
                    'computed_affects' => array(
                      '__getarchiveloop',
                    ),
                    'description'       => esc_html__( 'Filter by this ACF name and then the value below', 'divi-filter' ),
                  ),
                  'acf_value' => array(
                    'toggle_slug'       => 'loop_options',
                    'option_category'   => 'configuration',
                    'label'           => esc_html__( 'ACF Value', 'divi-filter' ),
                    'type'            => 'text',
                    'computed_affects' => array(
                      '__getarchiveloop',
                    ),
                    'description'     => esc_html__( 'Add the value here, it will show posts only with the value of the ACF field above', 'divi-filter' ),
                  ),
                  // 'author' => array(
                  // 'toggle_slug'       => 'loop_options',
                  // 'option_category'   => 'configuration',
                  //   'label'           => esc_html__( 'Author/s (comma-seperated)', 'divi-filter' ),
                  //   'type'            => 'text',
                  //   'description'     => esc_html__( 'Add a list of authors IDs. (comma-seperated)', 'divi-filter' ),
                  // ),
                  'sort_order' => array(
                    'toggle_slug'       => 'loop_options',
                    'option_category'   => 'configuration',
                    'label'             => esc_html__( 'Sort Order', 'divi-filter' ),
                    'type'              => 'select',
                    'options'           => array(
                      'date' => sprintf( esc_html__( 'Date', 'divi-filter' ) ),
                      'title' => esc_html__( 'Title', 'divi-filter' ),
                      'ID' => esc_html__( 'ID', 'divi-filter' ),
                      'rand' => esc_html__( 'Random', 'divi-filter' ),
                      'menu_order' => esc_html__( 'Menu Order', 'divi-filter' ),
                      'name' => esc_html__( 'Name', 'divi-filter' ),
                      'modified' => esc_html__( 'Modified', 'divi-filter' ),
                      'acf_date_picker' => esc_html__( 'ACF Date Picker', 'divi-filter' ),
                    ),
                    'affects'         => array(
                      'acf_date_picker_field',
                      'acf_date_picker_method',
                    ),
                    'default' => 'date',
                    'computed_affects' => array(
                      '__getarchiveloop',
                    ),
                    'description'       => esc_html__( 'Choose the sort order of the products.', 'divi-filter' ),
                  ),
                  'acf_date_picker_field' => array(
                    'toggle_slug'       => 'loop_options',
                    'label'             => esc_html__( 'ACF Date Picker', 'divi-filter' ),
                    'type'              => 'select',
                    'options'           => $acf_fields,
                    'default'           => 'none',
                    'option_category'   => 'configuration',
                    'depends_show_if' => 'acf_date_picker',
                    'description'       => esc_html__( 'Choose your date picker ACF item', 'divi-filter' ),
                  ),
                  'acf_date_picker_method' => array(
                    'toggle_slug'       => 'loop_options',
                    'option_category'   => 'configuration',
                    'depends_show_if' => 'acf_date_picker',
                    'label'             => esc_html__( 'ACF Date Picker Method', 'divi-filter' ),
                    'type'              => 'select',
                    'options'           => array(
                      'default' => esc_html__( 'Default', 'divi-filter' ),
                      'today_future' => sprintf( esc_html__( 'Today and in the future', 'divi-filter' ) ),
                      'today_30' => sprintf( esc_html__( 'Today and next 30 days', 'divi-filter' ) ),
                      'before_today' => sprintf( esc_html__( 'Yesterday and before', 'divi-filter' ) ),
                      
                    ),
                    'default' => 'default',
                    'computed_affects' => array(
                      '__getarchiveloop',
                    ),
                    'description'       => esc_html__( 'Choose the sort order of the products.', 'divi-filter' ),
                  ),
                  'order_asc_desc' => array(
                    'toggle_slug'       => 'loop_options',
                    'option_category'   => 'configuration',
                    'label'             => esc_html__( 'Order', 'divi-filter' ),
                    'type'              => 'select',
                    'options'           => array(
                      'ASC' => esc_html__( 'Ascending', 'divi-filter' ),
                      'DESC' => sprintf( esc_html__( 'Descending', 'divi-filter' ) ),
                    ),
                    'default' => 'ASC',
                    'computed_affects' => array(
                      '__getarchiveloop',
                    ),
                    'description'       => esc_html__( 'Choose the sort order of the products.', 'divi-filter' ),
                  ),
                  // ELEMENT SETTINGS
                  'enable_loadmore' => array(
                    'label'             => esc_html__( 'Choose how to display load more posts', 'divi-filter' ),
                    'type'              => 'select',
                    'option_category'   => 'configuration',
                    'options'           => array(
                      'on'  => esc_html__( 'Load More', 'divi-filter' ),
                      'pagination' => esc_html__( 'Pagination', 'divi-filter' ),
                      'off' => esc_html__( 'None', 'divi-filter' ),
                    ),
                    'description'       => esc_html__( 'If you want to show a way for the visitors to get more posts you can choose either Load More or Pagination. If you do not want this option - choose "none".', 'divi-filter' ),
                    'affects'         => array(
                      'loadmore_text',
                      'loadmore_text_loading',
                    ),
                    'default' => 'off',
                    'toggle_slug'       => 'element_options',
                  ),
                  'loadmore_text' => array(
                    'toggle_slug'       => 'element_options',
                    'option_category'   => 'configuration',
                    'label'           => esc_html__( 'Load More Text', 'divi-filter' ),
                    'type'            => 'text',
                    'depends_show_if' => 'on',
                    'default' => 'Load More',
                    'description'     => esc_html__( 'Add the text for the load more button', 'divi-filter' ),
                  ),
                  'loadmore_text_loading' => array(
                    'toggle_slug'       => 'element_options',
                    'option_category'   => 'configuration',
                    'label'           => esc_html__( 'Load More Loading Text', 'divi-filter' ),
                    'type'            => 'text',
                    'depends_show_if' => 'on',
                    'default' => 'Loading...',
                    'description'     => esc_html__( 'Add the text for the load more button when it is loading', 'divi-filter' ),
                  ),
                  'enable_resultcount'  => array(
                    'label'             => esc_html__( 'Show result count?', 'divi-filter'),
                    'type'              => 'yes_no_button',
                    'option_category'   => 'configuration',
                    'options'           => array(
                      'on'    => esc_html__( 'Yes', 'divi-filter' ),
                      'off'   => esc_html__( 'No', 'divi-filter' ),
                    ),
                    'description'       => esc_html__( 'If you want to display result count - enable this.', 'divi-filter' ),
                    'affects'           => array(
                      'resultcount_position'
                    ),
                    'default'           => 'off',
                    'toggle_slug'       => 'element_options'
                  ),
                  'resultcount_position' => array(
                    'label'             => esc_html__( 'Result Count Position', 'divi-filter'),
                    'type'              => 'select',
                    'option_category'   => 'configuration',
                    'options'           => array(
                      'left'      => esc_html__( 'Left', 'divi-filter' ),
                      'right'     => esc_html__( 'Right', 'divi-filter' ),
                    ),
                    'description'       => esc_html__( 'Select the position that you want to show the result count.', 'divi-filter' ),
                    'depends_show_if'   => 'on',
                    'default'           => 'right',
                    'toggle_slug'       => 'element_options'
                  ),
                  'has_map' => array(
                    'label'             => esc_html__( 'Has Map', 'divi-filter' ),
                    'type'              => 'yes_no_button',
                    'option_category'   => 'configuration',
                    'options'           => array(
                      'on'  => esc_html__( 'Yes', 'divi-filter' ),
                      'off' => esc_html__( 'No', 'divi-filter' ),
                    ),
                    'description'       => esc_html__( 'If you have a map to show your posts - enable this.', 'divi-filter' ),
                    'affects'         => array(
                      'map_selector',
                      'map_infoview_layout',
                    ),
                    'default' => 'off',
                    'toggle_slug'       => 'element_options',
                  ),
                  'map_selector' => array(
                    'toggle_slug'       => 'element_options',
                    'option_category'   => 'configuration',
                    'label'           => esc_html__( 'Map Selector', 'divi-filter' ),
                    'type'            => 'text',
                    'depends_show_if' => 'on',
                    'description'     => esc_html__( 'Input ID or Class name of Map element.', 'divi-filter' ),
                  ),
                  'map_infoview_layout' => array(
                    'toggle_slug'       => 'element_options',
                    'option_category'   => 'configuration',
                    'label'           => esc_html__( 'Marker Tooltip Layout', 'divi-filter' ),
                    'type'              => 'select',
                    'default'           => 'none',
                    'options'           => $options,
                    'depends_show_if' => 'on',
                    'description'     => esc_html__( 'Select layout for Marker tooltip', 'divi-filter' ),
                  ),
                  // ADVANCED OPTIONS
                  'link_whole_gird' => array(
                    'toggle_slug'       => 'extra_options',
                    'label'             => esc_html__( 'Link each layout to single page', 'divi-filter' ),
                    'type'              => 'yes_no_button',
                    'options'           => array(
                      'off' => esc_html__( 'No', 'divi-filter' ),
                      'on'  => esc_html__( 'Yes', 'divi-filter' ),
                    ),
                    'affects'         => array(
                      'link_whole_gird_new_tab',
                    ),
                    'description'        => esc_html__( 'Enable this if you want to link each loop layout to the single post.', 'divi-filter' ),
                  ),
                  'link_whole_gird_new_tab' => array(
                    'toggle_slug'       => 'extra_options',
                    'label'             => esc_html__( 'Open in New Tab?', 'divi-filter' ),
                    'type'              => 'yes_no_button',
                    'options'           => array(
                      'off' => esc_html__( 'No', 'divi-filter' ),
                      'on'  => esc_html__( 'Yes', 'divi-filter' ),
                    ),
                    'depends_show_if' => 'on',
                    'description'        => esc_html__( 'Enable this if you want it to open in a new tab.', 'divi-filter' ),
                  ),
                  'grid_layout' => array(
                    'toggle_slug'       => 'grid_options',
                    'label'             => esc_html__( 'Grid Style', 'divi-filter' ),
                    'type'              => 'select',
                    'options'           => array(
                      'grid'       => esc_html__( 'Grid', 'divi-filter' ),
                      'masonry'       => esc_html__( 'Masonry', 'divi-filter' ),
                    ),
                    'affects'         => array(
                      'equal_height',
                      'align_last_bottom',
                    ),
                    'option_category'   => 'configuration',
                    'default'           => 'grid',
                    'description'       => esc_html__( 'Choose how you want your posts to be shown', 'divi-filter' ),
                  ),

                  'equal_height' => array(
                    'label'             => esc_html__( 'Equal Height Grid Cards', 'divi-filter' ),
                    'type'              => 'yes_no_button',
                    'option_category'   => 'configuration',
                    'depends_show_if' => 'grid',
                    'options'           => array(
                      'on'  => esc_html__( 'Yes', 'divi-filter' ),
                      'off' => esc_html__( 'No', 'divi-filter' ),
                    ),
                    'description' => esc_html__( 'Enable this if you have the grid layout and want all your cards to be the same height.', 'divi-filter' ),
                    'toggle_slug'       => 'grid_options',
                  ),
                  'align_last_bottom' => array(
                    'toggle_slug'       => 'grid_options',
                    'label'             => esc_html__( 'Align last module at the bottom', 'divi-filter' ),
                    'type'              => 'yes_no_button',
                    'depends_show_if' => 'grid',
                    'options'           => array(
                      'off' => esc_html__( 'No', 'divi-filter' ),
                      'on'  => esc_html__( 'Yes', 'divi-filter' ),
                    ),
                    'description'        => esc_html__( 'Enable this to align the last module (probably the add to cart) at the bottom. Works well when using the equal height.', 'divi-filter' ),
                  ),

                  'columns' => array(
                    'toggle_slug'       => 'grid_options',
                    'label'             => esc_html__( 'Grid Columns', 'divi-filter' ),
                    'type'              => 'select',
                    'option_category'   => 'layout',
                    'default'   => '4',
                    'options'           => array(
                      '1'  => esc_html__( 'One', 'divi-filter' ),
                      '2'  => esc_html__( 'Two', 'divi-filter' ),
                      '3' => esc_html__( 'Three', 'divi-filter' ),
                      '4' => esc_html__( 'Four', 'divi-filter' ),
                      '5' => esc_html__( 'Five', 'divi-filter' ),
                      '6' => esc_html__( 'Six', 'divi-filter' ),
                    ),
                    'computed_affects' => array(
                      '__getarchiveloop',
                    ),
                    'description'        => esc_html__( 'How many columns do you want to see', 'divi-filter' ),
                  ),
                  'columns_tablet' => array(
                    'toggle_slug'       => 'grid_options',
                    'label'             => esc_html__( 'Tablet Grid Columns', 'divi-filter' ),
                    'type'              => 'select',
                    'option_category'   => 'layout',
                    'default'   => '2',
                    'options'           => array(
                      1  => esc_html__( 'One', 'divi-filter' ),
                      2  => esc_html__( 'Two', 'divi-filter' ),
                      3 => esc_html__( 'Three', 'divi-filter' ),
                      4 => esc_html__( 'Four', 'divi-filter' ),
                    ),
                    'computed_affects' => array(
                      '__getarchiveloop',
                    ),
                    'description'        => esc_html__( 'How many columns do you want to see on tablet', 'divi-filter' ),
                  ),
                  'columns_mobile' => array(
                    'toggle_slug'       => 'grid_options',
                    'label'             => esc_html__( 'Mobile Grid Columns', 'divi-filter' ),
                    'type'              => 'select',
                    'option_category'   => 'layout',
                    'default'   => '1',
                    'options'           => array(
                      1  => esc_html__( 'One', 'divi-filter' ),
                      2  => esc_html__( 'Two', 'divi-filter' ),
                    ),
                    'computed_affects' => array(
                      '__getarchiveloop',
                    ),
                    'description'        => esc_html__( 'How many columns do you want to see on mobile', 'divi-filter' ),
                  ),

                  
                  'custom_gutter_width' => array(
                    'label'             => esc_html__( 'Custom Gutter Gaps', 'divi-filter' ),
                    'type'              => 'yes_no_button',
                    'option_category'   => 'configuration',
                    'options'           => array(
                      'on'  => esc_html__( 'Yes', 'divi-filter' ),
                      'off' => esc_html__( 'No', 'divi-filter' ),
                    ),
                    'description'       => esc_html__( 'Enable this if you want custom gutter gaps for row and columns.', 'divi-filter' ),
                    'affects'         => array(
                      'gutter_row_gap',
                      'gutter_row_column',
                    ),
                    'default' => 'off',
                    'toggle_slug'       => 'grid_options',
                  ),


                  'gutter_row_gap' => array(
                    'label'           => esc_html__('Gutter Row Gap', 'et_builder'),
                    'description'     => esc_html__('Set the distance between each grid item vertically.', 'et_builder'),
                    'type'            => 'range',
                    'option_category' => 'basic_option',
                    'toggle_slug'     => 'grid_options',
                    'validate_unit'   => true,
                    'depends_show_if' => 'on',
                    'allowed_units'   => array('%', 'em', 'rem', 'px', 'cm', 'mm', 'in', 'pt', 'pc', 'ex', 'vh', 'vw'),
                    'default'         => '25px',
                    'default_unit'    => 'px',
                    'default_on_front' => '',
                    'allow_empty'     => false,
                    'range_settings'  => array(
                      'min'  => '0',
                      'max'  => '100',
                      'step' => '1',
                    ),
                  ),
                  'gutter_row_column' => array(
                    'label'           => esc_html__('Gutter Row Column', 'et_builder'),
                    'description'     => esc_html__('Set the distance between each grid item horizontally.', 'et_builder'),
                    'type'            => 'range',
                    'option_category' => 'basic_option',
                    'toggle_slug'     => 'grid_options',
                    'validate_unit'   => true,
                    'depends_show_if' => 'on',
                    'allowed_units'   => array('%', 'em', 'rem', 'px', 'cm', 'mm', 'in', 'pt', 'pc', 'ex', 'vh', 'vw'),
                    'default'         => '25px',
                    'default_unit'    => 'px',
                    'default_on_front' => '',
                    'allow_empty'     => false,
                    'range_settings'  => array(
                      'min'  => '0',
                      'max'  => '100',
                      'step' => '1',
                    ),
                  ),



                  'button_alignment' => array(
                    'label'            => esc_html__( 'Button Alignment', 'et_builder' ),
                    'description'      => esc_html__( 'Align your button to the left, right or center of the module.', 'et_builder' ),
                    'type'             => 'text_align',
                    'option_category'  => 'configuration',
                    'options'          => et_builder_get_text_orientation_options( array( 'justified' ) ),
                    'tab_slug'         => 'advanced',
                    'toggle_slug'      => 'alignment',
                  ),
                  '__getarchiveloop' => array(
                    'type' => 'computed',
                    'computed_callback' => array( 'de_mach_archive_loop_code', 'get_archive_loop' ),
                    'computed_depends_on' => array(
                      'post_type_choose',
                      'loop_layout',
                      'columns',
                      'columns_tablet',
                      'columns_mobile',
                      'post_status',
                      'posts_number',
                      'include_cats',
                      'exclude_cats',
                      'include_tags',
                      'acf_name',
                      'acf_value',
                      'sort_order',
                      'order_asc_desc',
                    ),
                  ),
                );

                return $fields;
              }

              public static function get_archive_loop ( $args = array(), $conditional_tags = array(), $current_page = array() ){
                if (!is_admin()) {
                  return;
                }

                ob_start();

                $post_type_choose        = $args['post_type_choose'];
                $loop_layout        = $args['loop_layout'];
                $columns            = $args['columns'];
                $columns_tablet     = $args['columns_tablet'];
                $columns_mobile     = $args['columns_mobile'];

                $post_status     = $args['post_status'];
                $posts_number     = $args['posts_number'];
                $include_cats     = $args['include_cats'];
                $include_tags     = $args['include_tags'];
                $exclude_cats     = $args['exclude_cats'];
                $acf_name     = $args['acf_name'];
                $acf_value     = $args['acf_value'];
                $sort_order     = $args['sort_order'];
                $order_asc_desc     = $args['order_asc_desc'];




                $get_cpt_args = array(
                  'post_type' => $post_type_choose,
                  'post_status' => $post_status,
                  'posts_per_page' => $posts_number,
                  'orderby' => $sort_order,
                  'order' => $order_asc_desc,
                );

                if ($include_cats != "") {
                  if ($post_type_choose == "post") {
                      $get_cpt_args['category_name'] = $include_cats;
                  } else {

                      if ( !empty( $cpt_taxonomies ) && in_array( 'category', $cpt_taxonomies ) ){
                          $get_cpt_args['category_name'] = $include_cats;
                      }else{
                          $ending = "_category";
                          $cat_key = $post_type_choose . $ending;
                          if ($cat_key == "product_category") {
                              $cat_key = "product_cat";
                          } else {
                              $cat_key = $cat_key;
                          }

                          $include_cats_arr = explode(',', $include_cats);
                          $get_cpt_args['tax_query'][] = array(
                              'taxonomy'  => $cat_key,
                              'field'     => 'slug',
                              'terms'     => $include_cats_arr,
                              'operator' => 'IN'
                          );  
                      }
                  }
              }

              if ($exclude_cats != "") {
                  $exclude_cats_arr = explode(',', $exclude_cats);
                  if ($post_type_choose == "post") {
                      $get_cpt_args['category__not_in'] = $exclude_cats_arr;
                  } else {

                      if ( !empty( $cpt_taxonomies ) && in_array( 'category', $cpt_taxonomies ) ){
                          $get_cpt_args['category__not_in'] = $exclude_cats_arr;
                      }else{
                          $ending = "_category";
                          $cat_key = $post_type_choose . $ending;
                          if ($cat_key == "product_category") {
                              $cat_key = "product_cat";
                          } else {
                              $cat_key = $cat_key;
                          }

                          $get_cpt_args['tax_query'][] = array(
                              'taxonomy'  => $cat_key,
                              'field'     => 'slug',
                              'terms'     => $exclude_cats_arr,
                              'operator' => 'NOT IN'
                          );
                      }
                  }
              }

              if ($include_tags != "") {

                  if ($post_type_choose == "post") {
                      $get_cpt_args['tag'] = $include_tags;
                  } else {
                      $ending = "_tag";
                      $cat_key = $post_type_choose . $ending;
                      if ($cat_key == "product_tag") {
                          $cat_key = "product_tag";
                      } else {
                          $cat_key = $cat_key;
                      }

                      $include_tags_arr = explode(',', $include_tags);
                    $get_cpt_args['tax_query'][] = array(
                        'taxonomy'  => $cat_key,
                        'field'     => 'slug',
                        'terms'     => $include_tags_arr,
                        'operator' => 'IN'
                    );
                  }
              }

                query_posts( $get_cpt_args );

                if ($loop_layout == "none") {
                  echo "Please create a custom loop layout and specify it in the settings.";
                } else {
                  ?>
                  <div class="filtered-posts-cont">
                    <div class="dmach-grid-sizes filtered-posts col-desk-<?php echo $columns?> col-tab-<?php echo $columns_tablet?> col-mob-<?php echo $columns_mobile?>">
                      <?php

                      if ( have_posts() ) {
                        ?>
                        <div class="grid-posts loop-grid">
                          <?php
                          while ( have_posts() ) {
                            the_post();

                            ?>
                            <div class="grid-col">
                              <div class="grid-item-cont">
                                <?php
                                  $post_content = apply_filters( 'the_content', get_post_field('post_content', $loop_layout) );

                                  $post_content = preg_replace( '/et_pb_section_(\d+)_tb_body/', 'et_pb_dmach_ajax_filter_section_${1}_tb_body', $post_content );
                                  $post_content = preg_replace( '/et_pb_row_(\d+)_tb_body/', 'et_pb_dmach_ajax_filter_row_${1}_tb_body', $post_content );
                                  $post_content = preg_replace( '/et_pb_column_(\d+)_tb_body/', 'et_pb_dmach_ajax_filter_column_${1}_tb_body', $post_content );
                                  $post_content = preg_replace( '/et_pb_section_(\d+)( |")/', 'et_pb_dmach_ajax_filter_section_${1}${2}', $post_content );
                                  $post_content = preg_replace( '/et_pb_row_(\d+)( |")/', 'et_pb_dmach_ajax_filter_row_${1}${2}', $post_content );
                                  $post_content = preg_replace( '/et_pb_column_(\d+)( |")/', 'et_pb_dmach_ajax_filter_column_${1}${2}', $post_content );
                      
                                  $post_content = preg_replace( '/et_pb_([a-z]+)_(\d+)_tb_body/', 'et_pb_dmach_ajax_filter_${1}_${2}_tb_body', $post_content );
                                  $post_content = preg_replace( '/et_pb_([a-z]+)_(\d+)( |")/', 'et_pb_dmach_ajax_filter_${1}_${2}${3}', $post_content );
                      
                                  echo $post_content;

                                ?>
                              </div>
                            </div>
                            <?php
                          }

                          ?>
                        </div>
                        <?php
                        // retrieve the styles for the modules
                        $internal_style = ET_Builder_Element::get_style();
                        // reset all the attributes after we retrieved styles
                        ET_Builder_Element::clean_internal_modules_styles( false );
                        $et_pb_rendering_column_content = false;

                        // append styles
                        if ( $internal_style ) {
                          ?>
                              <div class="df-inner-styles">
                          <?php
                              $cleaned_styles = str_replace("#et-boc .et-l","#et-boc .et-l .filtered-posts", $internal_style);
                              $cleaned_styles = preg_replace( '/et_pb_([a-z]+)_(\d+)_tb_body/', 'et_pb_dmach_ajax_filter_${1}_${2}_tb_body', $internal_style );
                              $cleaned_styles = preg_replace( '/et_pb_([a-z]+)_(\d+)( |"|.)/', 'et_pb_dmach_ajax_filter_${1}_${2}${3}', $cleaned_styles );
                          
                                  printf(
                                      '<style type="text/css" class="dmach_ajax_inner_styles">
                                        %1$s
                                      </style>',
                                      et_core_esc_previously( $cleaned_styles )
                                  );
                          ?>
                              </div>
                          <?php
                              }

                      }

                      ?>
                    </div>
                  </div>
                  <?php

                }

                $data = ob_get_clean();

                return $data;

              }

              public function change_section_css_selector( $selector, $function_name ){
                global $current_in_archive_loop;
                if ( ( $current_in_archive_loop != '' ) && $function_name == 'et_pb_section' ){
                  if ( $current_in_archive_loop == 'archive_loop') {
                    $selector = str_replace( 'et_pb_section_', 'et_pb_dmach_section_', $selector );  
                  }else{
                    //$selector = str_replace( 'et_pb_section_', 'et_pb_dmach_' . $current_in_archive_loop . '_section_', $selector );  
                  }
                  
                }
                if ( ( $current_in_archive_loop != '' ) && $function_name == 'et_pb_row' ){
                  if ( $current_in_archive_loop == 'archive_loop') {
                    $selector = str_replace( 'et_pb_row_', 'et_pb_dmach_row_', $selector );
                  }else{
                    //$selector = str_replace( 'et_pb_row_', 'et_pb_dmach_' . $current_in_archive_loop . '_row_', $selector );
                  }
                }
                if ( ( $current_in_archive_loop != '' ) && $function_name == 'et_pb_column' ){
                  if ( $current_in_archive_loop == 'archive_loop') {
                    $selector = str_replace( 'et_pb_column_', 'et_pb_dmach_column_', $selector );
                  }else{
                    //$selector = str_replace( 'et_pb_column_', 'et_pb_dmach_' . $current_in_archive_loop . '_column_', $selector );
                  }
                }
                return $selector;
              }

              public function get_button_alignment( $device = 'desktop' ) {
                $suffix           = 'desktop' !== $device ? "_{$device}" : '';
                $text_orientation = isset( $this->props["button_alignment{$suffix}"] ) ? $this->props["button_alignment{$suffix}"] : '';

                return et_pb_get_alignment( $text_orientation );
              }

              function render( $attrs, $content = null, $render_slug ) {

                if (is_admin()) {
                  return;
                }

                include(DE_DMACH_PATH . '/titan-framework/titan-framework-embedder.php');
                $titan = TitanFramework::getInstance( 'divi-machine' );
                $enable_debug = $titan->getOption( 'enable_debug' );


                $loop_layout            = $this->props['loop_layout'];
                $post_type_choose       = $this->props['post_type_choose'];
                $post_status            = $this->props['post_status'];
                $post_display_type      = $this->props['post_display_type'];
                $related_content        = $this->props['related_content'];
                $acf_name_related        = $this->props['acf_name_related'];
                

                $include_cats           = $this->props['include_cats'];
                $include_tags           = $this->props['include_tags'];
                $exclude_cats           = $this->props['exclude_cats'];
                $exclude_products       = $this->props['exclude_products'];
                $is_main_loop           = $this->props['is_main_loop'];

                $custom_tax_choose           = $this->props['custom_tax_choose'];
                $include_taxomony           = $this->props['include_taxomony'];
                


                // $author       = $this->props['author'];
                $sort_order             = $this->props['sort_order'];
                $acf_date_picker_field  = $this->props['acf_date_picker_field'];
                $acf_date_picker_method = $this->props['acf_date_picker_method'];
                $order_asc_desc         = $this->props['order_asc_desc'];

                $posts_number           = $this->props['posts_number'];
                $no_posts_layout        = $this->props['no_posts_layout'];


                $columns                = $this->props['columns'];
                $columns_tablet         = $this->props['columns_tablet'];
                $columns_mobile         = $this->props['columns_mobile'];

                $custom_gutter_width    = $this->props['custom_gutter_width']; 
                $gutter_row_gap         = $this->props['gutter_row_gap']; 
                $gutter_row_column      = $this->props['gutter_row_column']; 

                $link_whole_gird        = $this->props['link_whole_gird'];

                
                $acf_linked_acf               = $this->props['acf_linked_acf'];

                $acf_name               = $this->props['acf_name'];
                $acf_value              = $this->props['acf_value'];

                $equal_height           = $this->props['equal_height'];
                $align_last_bottom    = $this->props['align_last_bottom'];

                $grid_layout            = $this->props['grid_layout'];
                $filter_update_animation = $this->props['filter_update_animation'];
                $animation_color        = $this->props['animation_color'];
                $loading_bg_color        = $this->props['loading_bg_color'];
                

                $enable_loadmore        = $this->props['enable_loadmore'];
                $loadmore_text          = $this->props['loadmore_text'];
                $loadmore_text_loading  = $this->props['loadmore_text_loading'];

                $enable_resultcount     = $this->props['enable_resultcount'];
                $resultcount_position   = $this->props['resultcount_position'];

                $button_alignment  = $this->props['button_alignment'];
                
                $button_use_icon            = $this->props['button_use_icon'];
                $custom_icon              = $this->props['button_icon'];
                $button_bg_color          = $this->props['button_bg_color'];

                $has_map              = $this->props['has_map'];
                $map_selector         = $this->props['map_selector'];
                $map_infoview_layout  = $this->props['map_infoview_layout'];

                
                $link_whole_gird_new_tab  = $this->props['link_whole_gird_new_tab'];
                if ( $link_whole_gird_new_tab == 'on' ) {
                  $this->add_classname('link_whole_new_tab');
                }

                if( $button_use_icon == 'on' && $custom_icon != '' ){
                  $custom_icon = 'data-icon="'. esc_attr( et_pb_process_font_icon( $custom_icon ) ) .'"';
                  ET_Builder_Element::set_style( $render_slug, array(
                    'selector'    => 'body #page-container %%order_class%% .dmach-loadmore:after',
                    'declaration' => "content: attr(data-icon);",
                  ) );
                }else{
                  ET_Builder_Element::set_style( $render_slug, array(
                    'selector'    => 'body #page-container %%order_class%% .dmach-loadmore:hover',
                    'declaration' => "padding: .3em 1em;",
                  ) );
                }

                if( !empty( $button_bg_color ) ){

                  ET_Builder_Element::set_style( $render_slug, array(
                    'selector'    => 'body #page-container %%order_class%% .dmach-loadmore',
                    'declaration' => "background-color:". esc_attr( $button_bg_color ) ."!important;",
                  ) );
                }

                $this->add_classname( 'loadmore-align-' . $button_alignment );

                $this->add_classname('grid-layout-' . $grid_layout);

                if ( isset( $is_main_loop ) && $is_main_loop == 'on' ){
                  $this->add_classname( 'main-archive-loop' );
                }

                if ( $equal_height == 'on' ) {
                  $this->add_classname('same-height-cards');
                }

                if ( $align_last_bottom == 'on' ) {
                  $this->add_classname('align-last-module');
                }

                if ($enable_loadmore == 'on') {
                  $this->add_classname('loadmore-enabled');
                }

                if ('on' === $custom_gutter_width) {
                  ET_Builder_Element::set_style( $render_slug, array(
                    'selector'    => '%%order_class%% .filtered-posts > :not(.no-results-layout)',
                    'declaration' => sprintf(
                      'grid-row-gap: %1$s !important;',
                      esc_html( $gutter_row_gap )
                    ),
                  ) );
                  ET_Builder_Element::set_style( $render_slug, array(
                    'selector'    => '%%order_class%% .filtered-posts > :not(.no-results-layout)',
                    'declaration' => sprintf(
                      'grid-column-gap: %1$s !important;',
                      esc_html( $gutter_row_column )
                    ),
                  ) );
                }

                
                if ( '' !== $loading_bg_color ) {
                  ET_Builder_Element::set_style( $render_slug, array(
                    'selector'    => '%%order_class%% .ajax-loading',
                    'declaration' => sprintf(
                      'background-color: %1$s !important;',
                      esc_html( $loading_bg_color )
                    ),
                  ) );
                }

                if ( '' !== $animation_color ) {
                  ET_Builder_Element::set_style( $render_slug, array(
                    'selector'    => '%%order_class%% .line',
                    'declaration' => sprintf(
                      'background-color: %1$s !important;',
                      esc_html( $animation_color )
                    ),
                  ) );
                }

                if ( '' !== $animation_color ) {
                  ET_Builder_Element::set_style( $render_slug, array(
                    'selector'    => '%%order_class%% .donut',
                    'declaration' => sprintf(
                      'border-top-color: %1$s !important;',
                      esc_html( $animation_color )
                    ),
                  ) );
                }

                if ( '' !== $animation_color ) {
                  ET_Builder_Element::set_style( $render_slug, array(
                    'selector'    => '%%order_class%% .donut.multi',
                    'declaration' => sprintf(
                      'border-bottom-color: %1$s !important;',
                      esc_html( $animation_color )
                    ),
                  ) );
                }

                if ( '' !== $animation_color ) {
                  ET_Builder_Element::set_style( $render_slug, array(
                    'selector'    => '%%order_class%% .ripple',
                    'declaration' => sprintf(
                      'border-color: %1$s !important;',
                      esc_html( $animation_color )
                    ),
                  ) );
                }

                //////////////////////////////////////////////////////////////////////

                ob_start();

              

                global $wp_query, $wpdb, $post, $woocommerce;

                $cpt_taxonomies = get_object_taxonomies( $post_type_choose );

                if ( $post_display_type == "linked_post") {

                  $acf_linked_acf_get = get_field_object($acf_linked_acf);

                  $args = array(
                    'post_type'         => $post_type_choose,
                    'post_status'       => $post_status,
                    'posts_per_page'    => (int) $posts_number,
                    'post__not_in'       => array($post->ID),
                  );

                  if ( $acf_linked_acf_get['multiple'] == true ){
                    $args['meta_query'] = array(
                      array(
                        'key' => $acf_linked_acf_get['name'],
                        'value' => get_the_ID(),
                        'compare' => 'LIKE'
                      )
                    );
                  }else{
                    //$args['meta_key'] = $acf_linked_acf_get['name'];
                    //$args['meta_value'] = get_the_ID();
                    $args['meta_query'] = array(
                      array(
                        'key' => $acf_linked_acf_get['name'],
                        'value' => get_the_ID(),
                      )
                    );
                  }
                } else if ($post_display_type == "related") {


                  if (isset($post->ID)) {

                    $tax_array[] = "";

                    if ($post_type_choose == "post") {


                      $args = array(
                        'post_type'         => $post_type_choose,
                        'post_status'       => $post_status,
                        'posts_per_page'    => (int) $posts_number,
                        'post__not_in'       => array($post->ID)
                      );

                        if ($related_content == "categories"){

                            $cats = wp_get_post_terms( $post->ID, 'category' );
                            foreach ( $cats as $cat ) {
                                $tax_array[] = $cat->term_id;
                            }

                            $args['cat'] = $tax_array;

                        } else if ($related_content == "post-object"){

                          $post_objects = get_field($acf_name_related);

                          $post_object_ids = array();
                          foreach ( $post_objects as $post_object ){
                            $post_object_ids[] = $post_object->ID;
                          }

                          $args['post__in'] = $post_object_ids ;
                          $args['tax_query'] = "";
                          
                        } else {

                            $cats = wp_get_post_terms( $post->ID, 'post_tag' );
                            foreach ( $cats as $cat ) {
                                $tax_array[] = $cat->term_id;
                            }

                            if ($post_type_choose == "post") {
                                $args['tag__in'] = $tax_array;
                            } else {
                                $args['tag'] = $tax_array;
                            }
                        }
                    } else {

                        if ($related_content == "categories"){

                            $cats = wp_get_post_terms( $post->ID, $post_type_choose . '_category' );
                            foreach ( $cats as $cat ) {
                                $tax_array[] = $cat->term_id;
                            }

                            $category_name = $post_type_choose . '_category';

                            if ($category_name == "product_category") {
                                $category_name = "product_cat";
                            } else {
                                $category_name = $category_name;
                            }

                        } else if ($related_content == "post-object"){

                          $post_objects = get_field($acf_name_related);
                          if (!empty($post_objects)) {
                            $post_object_ids = array();
                            foreach ( $post_objects as $post_object ){
                              $post_object_ids[] = $post_object->ID;
                            }
                          } else {
                            $post_object_ids = array(null);
                          }
                          
                        } else {

                            $cats = wp_get_post_terms( $post->ID, $post_type_choose . '_tag' );
                            foreach ( $cats as $cat ) {
                                $tax_array[] = $cat->term_id;
                            }

                            $category_name = $post_type_choose . '_tag';

                            if ($category_name == "product_tag") {
                                $category_name = "product_tag";
                            } else {
                                $category_name = $category_name;
                            }
                        }
                        $category_name = !empty($category_name)?$category_name:'';
                        
                        $args = array(
                          'post_type'         => $post_type_choose,
                          'post_status'       => $post_status,
                          'posts_per_page'    => (int) $posts_number,
                          'tax_query' => array(
                            'relation' => 'AND',
                            array(
                              'taxonomy' => $category_name,
                              'field' => 'id',
                              'terms' => $tax_array
                            )
                          ),
                          'post__not_in'       => array($post->ID)
                        );


                        if ($related_content == "post-object"){
                          $args['post__in'] = $post_object_ids ;
                          $args['tax_query'] = "";
                        }

                    }

                  }


                } else {

                    $args = array(
                        'post_type'         => $post_type_choose,
                        'post_status'       => $post_status,
                        'posts_per_page'    => (int) $posts_number,
                        'post__not_in'      => explode(',', $exclude_products),
                    );

                    // Check current page is single post page for selected post type and get current post id

                    $meta_query = array('relation' => 'AND');

                    $current_post = 0;
                    if ( $wp_query->is_main_query()
                        && $wp_query->is_singular()
                        && $wp_query->is_single()
                        && $wp_query->post->post_type == $post_type_choose ){
                        $current_post = $wp_query->post->ID;
                    }

                    if ( $current_post != 0 ){
                        $args['post__not_in'][] = $current_post;
                    }

                    $args['tax_query']['relation'] = 'AND';

                    if ($include_cats != "") {
                        if ($post_type_choose == "post") {
                          $include_cats_arr = explode(',', $include_cats);
                          if ( is_array( $include_cats_arr ) && count( $include_cats_arr ) > 1 ){
                            $args['tax_query'][] = array(
                                'taxonomy'  => 'category',
                                'field'     => 'slug',
                                'terms'     => $include_cats_arr,
                                'operator' => 'IN'
                            );
                          }else{
                            $args['category_name'] = $include_cats;
                          } 
                        } else {

                            if ( !empty( $cpt_taxonomies ) && in_array( 'category', $cpt_taxonomies ) ){
                                $args['category_name'] = $include_cats;
                            }else{
                                $ending = "_category";
                                $cat_key = $post_type_choose . $ending;
                                if ($cat_key == "product_category") {
                                    $cat_key = "product_cat";
                                } else {
                                    $cat_key = $cat_key;
                                }

                                $include_cats_arr = explode(',', $include_cats);
                                $args['tax_query'][] = array(
                                    'taxonomy'  => $cat_key,
                                    'field'     => 'slug',
                                    'terms'     => $include_cats_arr,
                                    'operator' => 'IN'
                                );  
                            }
                        }
                    }

                    if ($exclude_cats != "") {
                        $exclude_cats_arr = explode(',', $exclude_cats);
                        if ($post_type_choose == "post") {
                            $args['category__not_in'] = $exclude_cats_arr;
                        } else {

                            if ( !empty( $cpt_taxonomies ) && in_array( 'category', $cpt_taxonomies ) ){
                                $args['category__not_in'] = $exclude_cats_arr;
                            }else{
                                $ending = "_category";
                                $cat_key = $post_type_choose . $ending;
                                if ($cat_key == "product_category") {
                                    $cat_key = "product_cat";
                                } else {
                                    $cat_key = $cat_key;
                                }

                                $args['tax_query'][] = array(
                                    'taxonomy'  => $cat_key,
                                    'field'     => 'slug',
                                    'terms'     => $exclude_cats_arr,
                                    'operator' => 'NOT IN'
                                );
                            }
                        }
                    }

                    if ($include_tags != "") {

                        if ($post_type_choose == "post") {
                            $args['tag'] = $include_tags;
                        } else {
                            $ending = "_tag";
                            $cat_key = $post_type_choose . $ending;
                            if ($cat_key == "product_tag") {
                                $cat_key = "product_tag";
                            } else {
                                $cat_key = $cat_key;
                            }
              
                            $include_tags_arr = explode(',', $include_tags);
                          $args['tax_query'][] = array(
                              'taxonomy'  => $cat_key,
                              'field'     => 'slug',
                              'terms'     => $include_tags_arr,
                              'operator' => 'IN'
                          );
                        }
                    }

                    

                    if ($include_taxomony != "") {
                      
                              $args['tax_query'][] = array(
                                  'taxonomy'  => $custom_tax_choose,
                                  'field'     => 'slug',
                                  'terms'     => $include_taxomony,
                                  'operator' => 'IN'
                              );  
                    }       

                    if ($acf_name != "none") {

                        if ($acf_value != "") {

                            $acf_name_get = get_field_object($acf_name);

                 
                            if ($acf_name_get['type'] == "radio" || $acf_name_get['type'] == "checkbox") {
                              $val_array = explode(',', $acf_value);
                              if ( is_array( $val_array ) && sizeof( $val_array ) > 1 ){
                                  $query_arr = array( 'relation' => 'OR' );
                                  foreach ( $val_array as $meta_val ) {
                                      $query_arr[] = array(
                                          'key'       => $acf_name_get['name'],
                                          'value'     => '"' . $meta_val . '"',
                                          'compare'   => 'LIKE',
                                      );
                                  }
                                  $meta_query[] = $query_arr;
                              }else{
                                  $meta_query[] = array(
                                      'key' => $acf_name_get['name'],
                                      'value' => '"' . $acf_value . '"',
                                      'compare' => 'LIKE'
                                  );
                              }
                            } else if (isset($acf_name_get['type']) && $acf_name_get['type'] == "range") {

                              $price_value = (explode(";",$acf_value));

                              if ( sizeof( $price_value ) == 1 ){
                                $meta_query[] = array(
                                  'key' => $acf_name_get['name'],
                                  'value' => $price_value[0],
                                  'type' => 'NUMERIC',
                                  'compare' => '<='
                                );
                              }else{
                                $meta_query[] = array(
                                  'key' => $acf_name_get['name'],
                                  'value' => $price_value,
                                  'compare' => 'BETWEEN',
                                  'type' => 'NUMERIC'
                                );
                              }

                            } else if (isset($acf_name_get['type']) && $acf_name_get['type'] == "text") {
                              $args['meta_key'] = $acf_name_get['name'];
                              $args['meta_value'] = $acf_value;
                            } else {
                              if ( $acf_name_get['multiple'] == true ){
                                $meta_query[] = array(
                                    'key' => $acf_name_get['name'],
                                    'value' => $acf_value,
                                    'compare' => 'LIKE'
                                );
                              } else{
                              $meta_query[] = array(
                                  'key' => $acf_name_get['name'],
                                  'value' => $acf_value,
                                  'compare' => 'IN'
                                );  
                              }
                            }
                
                
                    
                
                            $args['meta_query'] = $meta_query;
                
                


                            // $args['meta_key']    = $acf_name_get['name'];
                            // $args['meta_value']  = $acf_value;
                        }
                    }

                  // if ($author != "") {
                  //   $args['author_name'] = $author;
                  // }
                
                  $args['tax_query']['relation'] = 'AND';

                  if ( isset( $is_main_loop ) && $is_main_loop == 'on' ) {

                    if ( $wp_query->is_main_query()
                        && $wp_query->is_archive()
                        && !empty( $wp_query->query_vars['taxonomy'] )
                        && $wp_query->query_vars['taxonomy'] == $post_type_choose . '_category' ){

                        $args['tax_query'][] = array(
                            'taxonomy'  => $wp_query->query_vars['taxonomy'],
                            'field'     => 'slug',
                            'terms'     => $wp_query->query_vars['term']
                        );

                        $GLOBALS['my_query_filters']['tax_query'] = $post_type_choose . '_category';
                    }


                    if ( $wp_query->is_main_query() && $wp_query->is_archive() ){


                      if ( !empty( $wp_query->query['author_name'] ) ){
                        $args['author_name'] = $wp_query->query['author_name'];
                      }

                      if ( !empty( $wp_query->query_vars['cat'] ) ){
                        $args['cat'] = $wp_query->query_vars['cat'];
                      }

                      if ( !empty( $wp_query->query_vars['tag'] ) ){
                        $args['tag'] = $wp_query->query_vars['tag'];
                      }

                      if ( !empty( $wp_query->query_vars['year'] ) ){
                        $args['year'] = $wp_query->query_vars['year'];
                      }

                      if ( !empty( $wp_query->query_vars['monthnum'] ) ){
                        $args['monthnum'] = $wp_query->query_vars['monthnum'];
                      }

                      if ( !empty( $wp_query->query_vars[$post_type_choose . '_tag'] ) ){

                        $cat_key = $post_type_choose . '_tag';
                        $cus_tag_show = $wp_query->query_vars[$post_type_choose . '_tag'];
                        $args['tax_query'][] = array(
                          'taxonomy'  => $cat_key,
                          'field'     => 'slug',
                          'terms'     => $cus_tag_show,
                          'operator' => 'IN'
                        );

                      }

                      if ( !empty( $wp_query->query_vars[$post_type_choose . '_category'] ) ){

                        $cat_key = $post_type_choose . '_category';
                        $cus_tag_show = $wp_query->query_vars[$post_type_choose . '_category'];
                        $args['tax_query'][] = array(
                            'taxonomy'  => $cat_key,
                            'field'     => 'slug',
                            'terms'     => $cus_tag_show,
                            'operator' => 'IN'
                        );

                      }

                    }
                  
                $groups = acf_get_field_groups(array('post_type' => $post_type_choose));
                $field_keys = array();
                foreach( $groups as $group ){
                  $fields = acf_get_fields($group['ID']);
                  foreach( $fields as $field ){
                    $field_keys[] = $field['key'];
                  }
                }
                  
                    // loop over filters
                    foreach( $GLOBALS['my_query_filters'] as $key => $name ) {
                      if ( !in_array( $key, $field_keys ) ) {
                    continue;
                  }
                      if ( !empty($_GET[ $name ])) {


                        if ( $key == 'tax_query') {
                          continue;
                        }

                        $acf_get = get_field_object($key);
                  
                        $acf_type = $acf_get['type'];

                      // append meta query
                      if ($acf_type == 'range') {

                        $value_between = str_replace("%3B",";", $_GET[ $name ]);

                        $value = ( explode( ";", $value_between ) );

                        if ( sizeof( $value ) == 1 ) {
                          $meta_query[] = array(
                            'key' => $name,
                            'value' => $value[0],
                            'type' => 'NUMERIC',
                            'compare' => '<='
                          );
                        }else{
                          $meta_query[] = array(
                            'key' => $name,
                            'value' => $value,
                            'compare' => 'BETWEEN',
                            'type' => 'NUMERIC'
                          );
                        }
                      } else if ($acf_type == "checkbox" ) {
                        $multi_values = explode( ',', $_GET[ $name ] );
                        if ( is_array( $multi_values ) && sizeof($multi_values) > 1 ){
                          $query_arr = array( 'relation' => 'OR' );
                          foreach ( $multi_values as $meta_val ) {
                            $query_arr[] = array(
                              'key'       => $name,
                              'value'     => '"' . $meta_val . '"',
                              'compare'   => 'LIKE',
                            );
                          }

                          $meta_query[] = $query_arr;
                        }else{
                          $meta_query[] = array(
                            'key'       => $name,
                            'value'     => '"' . $_GET[ $name ] . '"',
                            'compare'   => 'LIKE',
                          );
                        }
                      } else {
                        $value = explode(',', $_GET[ $name ]);
                        if ( $acf_get['multiple'] == true ){
                          $meta_query[] = array(
                              'key' => $name,
                              'value' => $_GET[ $name ],
                              'compare' => 'LIKE'
                          );
                        } else{
                        $meta_query[] = array(
                          'key'       => $name,
                          'value'     => $value,
                              'compare' => 'IN'
                          );  
                        }
                      }
                    }
                  
                  }
                  
                    if ( !empty( $_GET['s'] ) ){
                      $args['s'] = $_GET['s'];
                    }


                    foreach ( $_GET as $key => $value ) {
                      if ( ( $key == $post_type_choose . '_category' ) ||
                      ( $post_type_choose == 'post' && $key == 'category') ){
                        if ( $value != 'all' && $value != '' ) {
                          $args['tax_query'][] = array(
                              'taxonomy'  => $key,
                              'field'     => 'slug',
                              'terms'     => $value
                          );
                          $GLOBALS['my_query_filters']['tax_query'] = $post_type_choose . '_category';
                        }
                      }

                      if ( $key != $post_type_choose . '_category' && $key != 'category' && in_array( $key, $cpt_taxonomies ) ) {
                        if ( $value != '' && $value != 'all'){

                          $args['tax_query'][] = array(
                              'taxonomy'  => $key,
                              'field'     => 'slug',
                              'terms'     => $value
                          );
                          
                        }
                          
                      }
                    }
                  }
                
                // get category page we are on for custom cat
                if ($wp_query) {


                  if (isset($wp_query->query_vars["tax_query"])) {
                    foreach($wp_query->query_vars["tax_query"] as $item) {
                      if (isset($item['taxonomy'])){
                      if (strpos($item['taxonomy'], '_cat') !== false) {
                        $curernt_custom_cat = $item['taxonomy'];
                        $curernt_custom_cat_terms = $item['terms'];
              
                        if (is_array($curernt_custom_cat_terms)) {
                          $curernt_custom_cat_terms = implode (", ", $curernt_custom_cat_terms);
                        }
                      } else if (strpos($item['taxonomy'], '_tag') !== false) {
                        $curernt_custom_cat = $item['taxonomy'];
                        $curernt_custom_cat_terms = $item['terms'];
              
                        if (is_array($curernt_custom_cat_terms)) {
                          $curernt_custom_cat_terms = implode (", ", $curernt_custom_cat_terms);
                        }
                      }
                    }
                    }
                  }

                  if (isset($wp_query->query['author_name'])) {
                    $authorname = $wp_query->query['author_name'];
                  } else {
                    $authorname = "";
                  }


                }

              }
                

            if ($sort_order == 'acf_date_picker') {

              $acf_get = get_field_object($acf_date_picker_field);

              if ($acf_date_picker_method == 'today_future') {

                $args['meta_key'] = $acf_get['name'];
                $args['orderby'] = 'meta_value_num';
                $args['order'] = 'ASC';

                $meta_query[] = array(
                    'key' => $acf_get['name'],
                    'compare' => '>=',
                    'value' => date("Y-m-d"),
                    'type' => 'DATE'
                  );

              } elseif ($acf_date_picker_method == 'today_30') {

                $args['meta_key'] = $acf_get['name'];
                $args['orderby'] = 'meta_value_num';
                $args['order'] = 'ASC';

                $meta_query[] = array(
                    'key' => $acf_get['name'],
                    'compare' => '>=',
                    'value' => date("Y-m-d"),
                    'type' => 'DATE'
                );
                $meta_query[] = array(
                    'key' => $acf_get['name'],
                    'compare' => '<=',
                    'value' => date("Y-m-d", strtotime("+30 days")),
                    'type' => 'DATE'
                  );

              } elseif ($acf_date_picker_method == 'before_today') {
                
                $args['meta_key'] = $acf_get['name'];
                $args['orderby'] = 'meta_value_num';
    $args['order'] = $order_asc_desc;

                $meta_query[] = array(
                  'key' => $acf_get['name'],
                  'compare' => '<=',
                  'value' => date('Y-m-d',strtotime("-1 days")),
                  'type' => 'DATE'
              );

              } else {

                $args['meta_key'] = $acf_get['name'];
                $args['orderby'] = 'meta_value_num';
                $args['order'] = $order_asc_desc;
                
              }
            } else if ( $sort_order == "rand") {
              $args['orderby'] = 'rand(' . $_SESSION['random'] . ')';
            } else {

              $args['orderby'] = $sort_order;
              $args['order']= $order_asc_desc;

            }
                
              if (isset($meta_query)) {
                $args['meta_query'] = $meta_query;
              }





                $args = apply_filters('dmach_archive_post_args', $args);
                query_posts( $args );

                if ( $post_type_choose == 'post' ) {
                  if ( !empty( $include_cats ) && count( explode( ',', $include_cats ) ) > 1 ) {
                    $wp_query->query_vars['category_name'] = '';
                    $wp_query->query_vars['cat'] = '';
                  }

                  if ( !empty( $include_tags ) && count( explode(',', $include_tags ) ) > 1 ){
                    $wp_query->query_vars['tag_id'] = '';
                  }
                }
              
                $wp_query->is_tax = false;

                $map_array = array();

                if ($enable_debug == "1") {
                  $acf_get = get_field_object($acf_name);
                  ?>
                  <p>ACF: <?php echo $acf_name ?></p>
                  <div class="reporting_args hidethis" style="white-space: pre;">
                    <?php  print_r($args); ?>
                  </div>
                  <?php
                }

                if (!isset($curernt_custom_cat)) {
                  $curernt_custom_cat = "";
                }

                if (!isset($curernt_custom_cat_terms)) {
                  $curernt_custom_cat_terms = "";
                }

                /*wp_localize_script( 'divi-filter-ajax-loadmore-js', 'divi_machine_var', array(
                'ajaxurl' => site_url() . '/wp-admin/admin-ajax.php', // WordPress AJAX
                ) );
                */
                
                wp_enqueue_script( 'divi-machine-ajax-loadmore-js' );

                $get_current_tax = get_query_var('taxonomy');
                $get_current_term = get_query_var( 'term' );

                global $current_in_archive_loop;

                $dmach_map_acf = $titan->getOption( 'dmach_map_acf' );
                $dmach_post_type = $titan->getOption( 'dmach_post_type' );

                $current_in_archive_loop = '';

                if ($loop_layout == "none") {
                  echo "Please create a custom loop layout and specify it in the settings.";
                } else {
                  // FILTER PARAMS
                  ?>
                  <div class="filter-param-tags"></div>
                  <div class="filtered-posts-cont">
                    <div class="filtered-posts-loading <?php echo $filter_update_animation ?> "></div>
                    <div class="dmach-grid-sizes divi-filter-archive-loop <?php echo (isset($is_main_loop) && ($is_main_loop == 'on'))?'main-loop':'';?> <?php echo $grid_layout ?> col-desk-<?php echo $columns?> col-tab-<?php echo $columns_tablet?> col-mob-<?php echo $columns_mobile?>"
                      data-link_wholegrid="<?php echo $link_whole_gird ?>"
                      data-layoutid="<?php echo $loop_layout ?>"
                      data-posttype="<?php echo $post_type_choose ?>"
                      data-noresults="<?php echo $no_posts_layout ?>"
                      data-sortorder="<?php echo $sort_order ?>"
                    <?php if ( $sort_order == 'acf_date_picker' ) { ?>
                        data-acf-order-field="<?php echo $acf_date_picker_field; ?>"
                        data-acf-order-method="<?php echo $acf_date_picker_method; ?>"
                    <?php }?>
                      data-sortasc="<?php echo $order_asc_desc ?>"
                      data-gridstyle="<?php echo $grid_layout?>"
                      data-columnscount="<?php echo $columns?>"
                      data-postnumber="<?php echo $posts_number ?>"
                      data-loadmore="<?php echo $enable_loadmore ?>"
                      data-resultcount="<?php echo $enable_resultcount;?>"
                      data-countposition="<?php echo $resultcount_position;?>"
                      data-btntext="<?php echo $loadmore_text ?>"
                      data-btntext_loading="<?php echo $loadmore_text_loading ?>"
                      data-posttax=""
                      data-postterm=""
                      data-search="<?php echo get_query_var("s");?>"
                      data-include_category="<?php echo $include_cats;?>"
                      data-include_tag="<?php echo $include_tags;?>"
                      data-exclude_category="<?php echo $exclude_cats;?>"
                      data-current_category="<?php echo $wp_query->query_vars["category_name"] ?>"
                      data-current_custom_category="<?php echo $curernt_custom_cat ?>"
                      data-current_custom_category_terms="<?php echo $curernt_custom_cat_terms ?>"
                      data-current_author="<?php echo !empty($authorname)?$authorname:''; ?>"
                      data-filter-var="<?php echo htmlspecialchars( json_encode( $wp_query->query_vars ) );?>"
                      data-current-page="<?php echo get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;?>"
                      data-max-page="<?php echo $wp_query->max_num_pages;?>" style="grid-auto-rows: 1px;"
                      data-has-map="<?php echo $has_map;?>" data-map-selector="<?php echo $map_selector;?>"
                      data-map-marker-layout="<?php echo $map_infoview_layout;?>">
                      <?php
                       if ( have_posts() ) {
                        ?>
                        <div class="grid-posts loop-grid"> <?php

                        while ( have_posts() ) {
                          the_post();
                          setup_postdata( $post );
                          $current_in_archive_loop = 'archive_loop';

                          if ( $has_map == 'on' && $dmach_map_acf !== "none" && $post_type_choose == $dmach_post_type ){
                            $map_array[] = get_field($dmach_map_acf);
                            $map_infoview_content = apply_filters( 'the_content', get_post_field('post_content', $map_infoview_layout ) );
                            $map_array[count($map_array) - 1]['infoview'] = $map_infoview_content;
                            $map_array[count($map_array) - 1]['title'] = get_the_title();
                          }

                          $post_link = get_permalink(get_the_ID());

                          $post_id = $post->ID;

                          $terms = wp_get_object_terms( $post_id, get_object_taxonomies($post_type_choose) );
                          $terms_array = array();
                          foreach ( $terms as $term ) {
                            $terms_array[] = $term->taxonomy . '-' . $term->slug;
                          }
                          $terms_string = implode (" ", $terms_array);

                          if ($grid_layout == "masonry") {
                            ?>
                            <div class="grid-item dmach-grid-item <?php echo $terms_string ?>">
                              <div class="grid-item-cont">
                                <?php
                              } else {
                                ?>
                                <div class="grid-col dmach-grid-item <?php echo $terms_string ?>">
                                  <div class="grid-item-cont">
                                    <?php
                                  }

                                  if ($link_whole_gird == "on") {
                                    ?>
                                    <div class="bc-link-whole-grid-card" data-link-url="<?php echo $post_link ?>">
                                    <?php   
                                  }

                                  // echo do_shortcode('[et_pb_row global_module="' . $loop_layout . '"][/et_pb_row]');
                                  $post_content = apply_filters( 'the_content', get_post_field('post_content', $loop_layout) );

                                  $post_content = preg_replace( '/et_pb_section_(\d+)_tb_body/', 'et_pb_dmach_section_${1}_tb_body', $post_content );
                                  $post_content = preg_replace( '/et_pb_row_(\d+)_tb_body/', 'et_pb_dmach_row_${1}_tb_body', $post_content );
                                  $post_content = preg_replace( '/et_pb_column_(\d+)_tb_body/', 'et_pb_dmach_column_${1}_tb_body', $post_content );

                                  $post_content = preg_replace( '/et_pb_section_(\d+)_tb_footer/', 'et_pb_dmach_section_${1}_tb_footer', $post_content );
                                  $post_content = preg_replace( '/et_pb_row_(\d+)_tb_footer/', 'et_pb_dmach_row_${1}_tb_footer', $post_content );
                                  $post_content = preg_replace( '/et_pb_column_(\d+)_tb_footer/', 'et_pb_dmach_column_${1}_tb_footer', $post_content );

                                  $post_content = preg_replace( '/et_pb_section_(\d+)_tb_header/', 'et_pb_dmach_section_${1}_tb_header', $post_content );
                                  $post_content = preg_replace( '/et_pb_row_(\d+)_tb_header/', 'et_pb_dmach_row_${1}_tb_header', $post_content );
                                  $post_content = preg_replace( '/et_pb_column_(\d+)_tb_header/', 'et_pb_dmach_column_${1}_tb_header', $post_content );

                                  $post_content = preg_replace( '/et_pb_section_(\d+)( |")/', 'et_pb_dmach_section_${1}${2}', $post_content );
                                  $post_content = preg_replace( '/et_pb_row_(\d+)( |")/', 'et_pb_dmach_row_${1}${2}', $post_content );
                                  $post_content = preg_replace( '/et_pb_column_(\d+)( |")/', 'et_pb_dmach_column_${1}${2}', $post_content );

                                  echo $post_content;

                                  // $content = apply_filters('the_content', get_post_field('post_content', $loop_layout));

                                  // echo et_builder_render_layout($content );

                                  if ($link_whole_gird == "on") {
                                    ?>
                                    </div>  
                                    <?php       
                                  }
                                  ?>
                                </div>
                              </div>
                              <?php
                            } // end while have posts
                    wp_reset_postdata();
                            $current_in_archive_loop = '';
                            ?>
                          </div>
                      <?php
                        } else {
                          if ($no_posts_layout == "none") {
                            echo '<div class="no-results-layout">';
                            echo "No " . $post_type_choose;
                            echo '</div>';
                          } else {
                            ?>
                            <div class="no-results-layout">
                              <?php
                              echo do_shortcode('[et_pb_row global_module="' . $no_posts_layout . '"][/et_pb_row]');
                              ?>
                            </div>
                            <?php
                          }
                        }
                        ?>
                      </div>
                    </div> <!-- filtered-posts-cont -->
                    <?php 
                      $position_class = '';
                      if ( $enable_resultcount == "on" ) {
                        $position_class = 'result_count_' . $resultcount_position;
                        $current_page = get_query_var( 'paged' ) ? get_query_var( 'paged' ) : 1;
                        echo '<p class="divi-filter-result-count ' . $position_class . '">';
                        if ( $wp_query->found_posts == 1 ){
                          echo __('Showing the single result', 'divi-filter');
                        }else if ( $wp_query->found_posts == $wp_query->post_count ) {
                          printf( __('Showing all %d results', 'divi-filter'), $wp_query->found_posts );
                        }else {
                          printf( __('Showing %d-%d of %d results', 'divi-filter'), (($current_page - 1) * $posts_number + 1), (($current_page - 1) * $posts_number + $wp_query->post_count), $wp_query->found_posts );
                        }
                        echo '</p>';
                      }
                      if ($enable_loadmore == "on") {
                        if (  $wp_query->max_num_pages > 1 ){
                          ?>
                          <div class="dmach-loadmore et_pb_button <?php echo $position_class;?>" <?php echo $custom_icon ?>><?php echo $loadmore_text ?></div>
                          <?php
                        }
                      }else if ( $enable_loadmore == 'pagination' ) {
                      ?>
                        <div class="divi-filter-pagination <?php echo $position_class;?>"><?php echo paginate_links(array('type' => 'list')); ?></div>
                      <?php
                      }
                      if ( $has_map == 'on' && $dmach_map_acf !== "none" && $post_type_choose == $dmach_post_type ){ 
                      ?>
                    <script>
                      jQuery(document).ready(function($){
                        if ( $('<?php echo $map_selector;?>').length > 0 ){
                          $('<?php echo $map_selector;?>').each( function(){
                            $(this).find('.et_pb_map_pin').remove();
                          <?php 
                          foreach ($map_array as $key => $map) {
                          ?>
                            var t = `<?php echo $map['infoview'] ;?>`;
                            $(this).append('<div class="et_pb_map_pin" data-lat="<?php echo $map['lat'];?>" data-lng="<?php echo $map['lng'];?>" data-title="<?php echo $map['title'];?>"><div class="infowindow">' + t + '</div></div>');
                            
                          <?php
                          }
                          ?>
                          });
                        }
                      });  
                    </script>
                      <?php
                          }
                    }

                  wp_reset_query();

                  $data = ob_get_clean();
                  //////////////////////////////////////////////////////////////////////
                  return $data;
                }
              }

              new de_mach_archive_loop_code;
        }
    }

    if ( !function_exists('Divi_filter_restore_get_params') ) {
      add_action( 'template_redirect', 'Divi_filter_restore_get_params');

      function Divi_filter_restore_get_params() {
          global $divi_filter_removed_param;
          if ( !empty( $_GET['filter'] ) && $_GET['filter'] == 'true' ){
        if ( !empty($divi_filter_removed_param) ) {
          foreach ($divi_filter_removed_param as $key => $value ) {
            $_GET[$key] = $value;
          }
        }
          }
      }
    }
  
    add_action( 'wp_enqueue_scripts', 'divi_machine_loop_enqueue_scripts' );

    function divi_machine_loop_enqueue_scripts() {
        wp_enqueue_script( 'divi-filter-loadmore-js', plugins_url( '../../../js/divi-filter-loadmore.min.js', __FILE__ ), array( 'jquery' ), DE_DF_VERSION );
        wp_localize_script( 'divi-filter-loadmore-js', 'loadmore_ajax_object',
            array(
                'ajax_url' => admin_url( 'admin-ajax.php' ),
            )
        );
    }
}
?>