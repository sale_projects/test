<?php
if ( ! defined( 'ABSPATH' ) ) exit;

if ( !function_exists("Divi_filter_loop_module_import") ){
    add_action( 'et_builder_ready', 'Divi_filter_loop_module_import');

    function Divi_filter_loop_module_import(){
        if(class_exists("ET_Builder_Module") && !class_exists("db_filter_loop_code")){
            class db_filter_loop_code extends ET_Builder_Module {

                public $vb_support = 'on';

                protected $module_credits = array(
                    'module_uri' => DE_DF_PRODUCT_URL,
                    'author'     => DE_DF_AUTHOR,
                    'author_uri' => DE_DF_URL,
                );

                function init() {
                    if (defined('DE_DB_WOO_VERSION')) {
                        $this->slug = 'et_pb_db_shop_loop';
                        $this->name       = esc_html__( '.ARP Product Loop - Archive Pages', 'divi-bodyshop-woocommerce' );
                    } else {
                        $this->slug = 'et_pb_db_filter_loop';
                        $this->name       = esc_html__( '.Archive Loop - Divi Ajax Filter', 'divi-filter' );
                    }


                    $this->fields_defaults = array(
                        'cat_loop_style'        => array( 'off' ),
                        'fullwidth'             => array( 'list' ),
                        'columns'               => array( '3' ),
                        'show_pagination'       => array( 'on' ),
                        'show_add_to_cart'      => array( 'on' ),
                        'show_sorting_menu'     => array( 'off' ),
                        'show_results_count'    => array( 'off' ),
                    );

                    $this->settings_modal_toggles = array(
                        'general' => array(
                            'toggles' => array(
                                'main_content'      => esc_html__( 'Module Options', 'divi-filter' ),
                                'custom_content'    => esc_html__( 'Custom Layout Options', 'divi-filter' ),
                                'default_content'   => esc_html__( 'Default Style Options', 'divi-filter' ),
                            ),
                        ),
                        'advanced' => array(
                            'toggles' => array(
                                'text'      => esc_html__( 'Text', 'divi-filter' ),
                                'overlay'   => esc_html__( 'Overlay', 'divi-filter' ),
                                'product'   => esc_html__( 'Product', 'et_builder' ),
                            ),
                        ),
                    );


                    $this->main_css_element = '%%order_class%%';


                    $this->advanced_fields = array(
                        'fonts' => array(
                            'title' => array(
                                'label'    => esc_html__( 'Default Layout - Title', 'divi-filter' ),
                                'css'      => array(
                                    'main' => "%%order_class%% ul.products li.product .woocommerce-loop-product__title",
                                    'important' => 'plugin_only',
                                ),
                                'font_size' => array(
                                    'default' => '14px',
                                ),
                                'line_height' => array(
                                    'default' => '1em',
                                ),
                            ),
                            'price' => array(
                                'label'    => esc_html__( 'Default Layout - Price', 'divi-filter' ),
                                'css'      => array(
                                    'main' => "%%order_class%% ul.products li.product .price, %%order_class%% ul.products li.product .price .amount",
                                ),
                                'font_size' => array(
                                    'default' => '14px',
                                ),
                                'line_height' => array(
                                    'range_settings' => array(
                                        'min'  => '1',
                                        'max'  => '100',
                                        'step' => '1',
                                    ),
                                    'default' => '1.7em',
                                ),
                            ),
                            'excerpt' => array(
                                'label'    => esc_html__( 'Default Layout - Excerpt', 'divi-filter' ),
                                'css'      => array(
                                    'main' => "%%order_class%% ul.products li.product .woocommerce-product-details__short-description",
                                    'important' => 'plugin_only',
                                ),
                                'font_size' => array(
                                    'default' => '14px',
                                ),
                                'line_height' => array(
                                    'default' => '1em',
                                ),
                            ),
                            'cattitle' => array(
                                'label'    => esc_html__( 'Default Layout - Category Title', 'divi-filter' ),
                                'css'      => array(
                                    'main' => "%%order_class%% ul.products .woocommerce-loop-category__title",
                                    'important' => 'all',
                                ),
                            ),
                            'catcount' => array(
                                'label'    => esc_html__( 'Default Layout - Category Count', 'divi-filter' ),
                                'css'      => array(
                                    'main' => "%%order_class%% ul.products .woocommerce-loop-category__title .count",
                                    'important' => 'all',
                                ),
                            ),
                        ),
                        'background' => array(
                            'settings' => array(
                                'color' => 'alpha',
                            ),
                        ),
                        'button' => array(
                            'add_to_cart_button' => array(
                                'label' => esc_html__( 'Default Layout - Add To Cart Button', 'divi-filter' ),
                                'css' => array(
                                    'main' => "%%order_class%% ul.products li.product .button",
                                    'important' => 'all',
                                ),
                                'box_shadow' => array(
                                    'css' => array(
                                        'main' => "%%order_class%% ul.products li.product .button",
                                    ),
                                ),
                            ),
                        ),
                        'box_shadow' => array(
                            'default' => array(),
                            'product' => array(
                                'label' => esc_html__( 'Default Layout - Product Box Shadow', 'divi-filter' ),
                                'css' => array(
                                    'main' => "%%order_class%% .products .product",
                                ),
                                'option_category' => 'layout',
                                'tab_slug'        => 'advanced',
                                'toggle_slug'     => 'product',
                            ),
                        ),
                    );


                    $this->custom_css_fields = array(
                        'product' => array(
                            'label'    => esc_html__( 'Default Layout - Product', 'divi-filter' ),
                            'selector' => '%%order_class%% li.product',
                        ),
                        'onsale' => array(
                            'label'    => esc_html__( 'Default Layout - Onsale', 'divi-filter' ),
                            'selector' => '%%order_class%% li.product .onsale',
                        ),
                        'image' => array(
                            'label'    => esc_html__( 'Default Layout - Image', 'divi-filter' ),
                            'selector' => '%%order_class%% .et_shop_image',
                        ),
                        'overlay' => array(
                            'label'    => esc_html__( 'Default Layout - Overlay', 'divi-filter' ),
                            'selector' => '%%order_class%% .et_overlay,  %%order_class%% .et_pb_extra_overlay',
                        ),
                        'title' => array(
                            'label'    => esc_html__( 'Default Layout - Title', 'divi-filter' ),
                            'selector' => '%%order_class%% .woocommerce-loop-product__title',
                        ),
                        'rating' => array(
                            'label'    => esc_html__( 'Default Layout - Rating', 'divi-filter' ),
                            'selector' => '%%order_class%% .star-rating',
                        ),
                        'price' => array(
                            'label'    => esc_html__( 'Default Layout - Price', 'divi-filter' ),
                            'selector' => "body.woocommerce {$this->main_css_element} .et_pb_post .et_pb_post_type ul.products li.product .price, {$this->main_css_element} .et_pb_post .et_pb_post_type ul.products li.product .price",
                        ),
                        'price_old' => array(
                            'label'    => esc_html__( 'Default Layout - Old Price', 'divi-filter' ),
                            'selector' => '%%order_class%% li.product .price del span.amount',
                        ),
                        'excerpt' => array(
                            'label'    => esc_html__( 'Default Layout - Product Excerpt', 'divi-filter' ),
                            'selector' => '%%order_class%% li.product .woocommerce-product-details__short-description',
                        ),
                        'add_to_cart' => array(
                            'label'    => esc_html__( 'Default Layout - Add To Cart Button', 'divi-filter' ),
                            'selector' => '%%order_class%% li.product a.button',
                        ),
                    );


                    $this->help_videos = array(
                        array(
                            'id'   => esc_html__( '9EtJRhTf9_o', 'divi-filter' ), // CATEGORY VIDEO
                            'name' => esc_html__( 'BodyCommcerce Product Page Template Guide', 'divi-filter' ),
                        ),
                    );
                }



                function get_fields() {

                    if (defined('DE_DB_WOO_VERSION')) {
                        $options_posttype = DEBC_INIT::get_divi_post_types();
                        $options = DEBC_INIT::get_divi_layouts();
                        } else {
                          $options_posttype = DE_Filter::get_divi_post_types();
                          $options = DE_Filter::get_divi_layouts();
                        }

                    //////////////////////////////

                    $fields = array(
                        'post_type_choose' => array(
                            'toggle_slug'       => 'main_content',
                            'label'             => esc_html__( 'Post Type', 'divi-filter' ),
                            'type'              => 'select',
                            'options'           => $options_posttype,
                            'default'           => 'product',
                            'computed_affects' => array(
                              '__products',
                            ),
                            'description'       => esc_html__( 'Choose the post type you want to display', 'divi-filter' ),
                        ),
                        'cat_loop_style' => array(
                            'toggle_slug'       => 'main_content',
                            'label'             => esc_html__( 'Loop Style', 'divi-filter' ),
                            'type'              => 'select',
                            'default'           => 'on',
                            'option_category'   => 'configuration',
                            'affects'=>array(
                                'loop_layout'
                            ),
                            'computed_affects' => array(
                                '__products',
                            ),
                            'options' => array(
                                'on' => esc_html__( 'Default', 'divi-filter' ),
                                'off' => esc_html__( 'Custom Layout', 'divi-filter' ),
                            ),
                            'description'        => esc_html__( 'Choose the loop style you want, default will result in the normal WooCommerce style but if you want to creare your own one using a loop layout created in the Divi Library, choose custom.', 'divi-filter' ),
                        ),
                        'loop_layout' => array(
                            'toggle_slug'       => 'main_content',
                            'label'             => esc_html__( 'Loop Layout', 'divi-filter' ),
                            'type'              => 'select',
                            'option_category'   => 'configuration',
                            'depends_show_if'   => 'off',
                            'default'           => 'none',
                            'options'           => $options,
                            'description'       => esc_html__( 'Choose the layout you have made for each product in the loop.', 'divi-filter' ),
                        ),
                        'posts_number' => array(
                            'toggle_slug'       => 'main_content',
                            'label'             => esc_html__( 'Posts Number', 'divi-filter' ),
                            'type'              => 'text',
                            'default'           => 16,
                            'description'       => esc_html__( 'Choose how many posts you would like to display per page. Divi sometimes overrides this. To set it go to Divi > Theme Options and change the value "Number of Products displayed on WooCommerce archive pages"', 'divi-filter' ),
                        ),

                        'is_main_loop' => array(
                            'toggle_slug'       => 'main_content',
                            'label'             => esc_html__( 'Is Main Loop?', 'divi-filter' ),
                            'type'              => 'yes_no_button',
                            'option_category'   => 'configuration',
                            'options'           => array(
                                'on'            => esc_html__( 'Yes', 'et_builder' ),
                                'off'           => esc_html__( 'No', 'et_builder' ),
                            ),
                            'default'           => 'on',
                            'description'       => esc_html__( 'Choose if you want to make this loop as main loop on the page - the filter will affect this loop.', 'divi-filter' ),
                            'affects'           => array(
                                'show_sorting_menu',
                                'show_results_count',
                                'enable_pagination',
                            ),
                        ),
                        'show_sorting_menu' => array(
                            'label' => esc_html__( 'Show OrderBy Menu', 'divi-filter' ),
                            'type' => 'yes_no_button',
                            'option_category' => 'configuration',
                            'options' => array(
                                'on' => esc_html__( 'Yes', 'divi-filter' ),
                                'off' => esc_html__( 'No', 'divi-filter' ),
                            ),
                            'default' => 'on',
                            'description' => esc_html__( 'Show/Hide the sorting dropdown menu in the frontend.', 'divi-filter' ),
                            'toggle_slug' => 'main_content',
                            'depends_show_if'   => 'on',
                        ),
                        'show_results_count' => array(
                            'label' => esc_html__( 'Show Results Count', 'divi-filter' ),
                            'type' => 'yes_no_button',
                            'option_category' => 'configuration',
                            'options' => array(
                                'on' => esc_html__( 'Yes', 'divi-filter' ),
                                'off' => esc_html__( 'No', 'divi-filter' ),
                            ),
                            'default' => 'on',
                            'description' => esc_html__( 'Show/Hide products count.', 'divi-filter' ),
                            'toggle_slug' => 'main_content',
                            'depends_show_if'   => 'on',
                        ),

                        'enable_pagination' => array(
                            'label'             => esc_html__( 'Enable Pagination', 'divi-filter' ),
                            'type'              => 'yes_no_button',
                            'option_category'   => 'configuration',
                            'options'           => array(
                                'on'  => esc_html__( 'Yes', 'divi-filter' ),
                                'off' => esc_html__( 'No', 'divi-filter' ),
                            ),
                            'default' => 'on',
                            'toggle_slug'       => 'main_content',
                            'depends_show_if'   => 'on',
                        ),

                        'disable_products_has_cat' => array(
                            'label'             => esc_html__( 'Disable products when category has child categories?', 'divi-filter' ),
                            'type'              => 'yes_no_button',
                            'option_category'   => 'configuration',
                            'options'           => array(
                                'on'  => esc_html__( 'Yes', 'divi-filter' ),
                                'off' => esc_html__( 'No', 'divi-filter' ),
                            ),
                            'default' => 'off',
                            'toggle_slug'       => 'main_content',
                            'description'       => esc_html__( 'Only works on categories. If you are on a category page and the category has sub-categories, you can disable the products to be shown with this setting.', 'divi-filter' ),
                        ),

                        'exclude_products'      => array(
                            'label'             => esc_html__( 'Exclude Products', 'divi-filter' ),
                            'type'              => 'text',
                            'option_category'   => 'configuration',
                            'toggle_slug'       => 'main_content',
                            'description'       => esc_html__( 'Add a list of product ids that you want to exclude from show. (comma-seperated)', 'divi-filter' ),
                        ),

                        'filter_update_animation' => array(
                            'toggle_slug'       => 'main_content',
                            'label'             => esc_html__( 'Ajax Filter Update Animation Style', 'divi-machine' ),
                            'type'              => 'select',
                            'options'           => array(
                              'load-1'       => esc_html__( 'Three Lines Vertical', 'divi-machine' ),
                              'load-2'       => esc_html__( 'Three Lines Horizontal', 'divi-machine' ),
                              'load-3'       => esc_html__( 'Three Dots Bouncing', 'divi-machine' ),
                              'load-4'       => esc_html__( 'Donut', 'divi-machine' ),
                              'load-5'       => esc_html__( 'Donut Multiple', 'divi-machine' ),
                              'load-6'       => esc_html__( 'Ripple', 'divi-machine' ),
                            ),
                            'option_category'   => 'configuration',
                            'default'           => 'load-6',
                            'description'       => esc_html__( 'Choose the animation style for when loading the posts', 'divi-machine' ),
                          ),
                          'animation_color' => array(
                            'label'             => esc_html__( 'Ajax Filter Animation Icon Color', 'et_builder' ),
                            'description'       => esc_html__( 'Define the color of the animation you choose above.', 'et_builder' ),
                            'type'              => 'color-alpha',
                            'custom_color'      => true,
                            'option_category'   => 'configuration',
                            'toggle_slug'       => 'main_content',
                          ),
                          'loading_bg_color' => array(
                            'label'             => esc_html__( 'Ajax Filter Background Color', 'et_builder' ),
                            'description'       => esc_html__( 'Define the color of the background when it is loading.', 'et_builder' ),
                            'type'              => 'color-alpha',
                            'custom_color'      => true,
                            'option_category'   => 'configuration',
                            'toggle_slug'       => 'main_content',
                          ),
                        // 'product_type' => array(
                        //     'label' => esc_html__('Type', 'divi-filter'),
                        //     'type' => 'select',
                        //     'option_category' => 'configuration',
                        //     'options' => array(
                        //         'original' => esc_html__('Default (ALL)', 'divi-filter'),
                        //              'recent'  => esc_html__( 'Recent Products', 'divi-filter' ),
                        //              'featured' => esc_html__( 'Featured Products', 'divi-filter' ),
                        //              'sale' => esc_html__( 'Sale Products', 'divi-filter' ),
                        //              'best_selling' => esc_html__( 'Best Selling Products', 'divi-filter' ),
                        //              'top_rated' => esc_html__( 'Top Rated Products', 'divi-filter' ),
                        //     ),
                        //     'default' => 'original',
                        //     'toggle_slug'       => 'main_content',
                        //     'description' => esc_html__('Choose which type of products you would like to display', 'divi-filter'),
                        // ),

                        'no_results_layout' => array(
                            'toggle_slug'       => 'custom_content',
                            'label'             => esc_html__( 'No Results Layout', 'divi-filter' ),
                            'type'              => 'select',
                            'option_category'   => 'layout',
                            'default'           => 'none',
                            'options'           => $options,
                            'description'       => esc_html__( 'Choose the layout you want to appear when there are no products.', 'divi-filter' ),
                        ),
                        'hide_non_purchasable' => array(
                            'toggle_slug'       => 'custom_content',
                            'label'             => esc_html__( 'Hide non purchasable products?', 'divi-filter' ),
                            'type'              => 'yes_no_button',
                            'options'           => array(
                                'off' => esc_html__( 'No', 'divi-filter' ),
                                'on'  => esc_html__( 'Yes', 'divi-filter' ),
                            ),
                            'default'            => 'off',
                            'description'        => esc_html__( 'If you want to hide non purchasable products, enable this.', 'divi-filter' ),
                        ),
                        'link_whole_gird' => array(
                            'toggle_slug'       => 'custom_content',
                            'label'             => esc_html__( 'Link each layout to product', 'divi-filter' ),
                            'type'              => 'yes_no_button',
                            'options'           => array(
                                'off' => esc_html__( 'No', 'divi-filter' ),
                                'on'  => esc_html__( 'Yes', 'divi-filter' ),
                            ),
                            'affects'         => array(
                              'link_whole_gird_new_tab',
                            ),
                            'description'        => esc_html__( 'Enable this if you want to link each loop layout to the product. For example if you want the whole "grid card" to link to the product page. NB: You need to have no other links on the loop layout so do not link the image or the title to the product page.', 'divi-filter' ),
                        ),
                        'link_whole_gird_new_tab' => array(
                          'toggle_slug'       => 'custom_content',
                          'label'             => esc_html__( 'Open in New Tab?', 'divi-filter' ),
                          'type'              => 'yes_no_button',
                          'options'           => array(
                            'off' => esc_html__( 'No', 'divi-filter' ),
                            'on'  => esc_html__( 'Yes', 'divi-filter' ),
                          ),
                          'depends_show_if' => 'on',
                          'description'        => esc_html__( 'Enable this if you want it to open in a new tab.', 'divi-filter' ),
                        ),
                        'equal_height' => array(
                            'label'             => esc_html__( 'Equal Height Grid Cards', 'divi-filter' ),
                            'type'              => 'yes_no_button',
                            'option_category'   => 'configuration',
                            'options'           => array(
                                'on'  => esc_html__( 'Yes', 'divi-filter' ),
                                'off' => esc_html__( 'No', 'divi-filter' ),
                            ),
                            'affects'           =>array(
                                'equal_height_mob',
                                'align_last_bottom'
                            ),
                            'description'       => esc_html__( 'Enable this if you have the grid layout and want all your cards to be the same height.', 'divi-filter' ),
                            'toggle_slug'       => 'custom_content',
                        ),

                        'equal_height_mob' => array(
                            'label'             => esc_html__( 'Equal Height  on mobile?', 'divi-filter' ),
                            'type'              => 'yes_no_button',
                            'option_category'   => 'configuration',
                            'options'           => array(
                                'on'  => esc_html__( 'Yes', 'divi-filter' ),
                                'off' => esc_html__( 'No', 'divi-filter' ),
                            ),
                            'default'           => 'off',
                            'depends_show_if'   => 'on',
                            'description'       => esc_html__( 'We will disable equal height on mobile. If you want it to stay, enable this..', 'divi-filter' ),
                            'toggle_slug'       => 'custom_content',
                        ),
                        'align_last_bottom' => array(
                            'toggle_slug'       => 'custom_content',
                            'label'             => esc_html__( 'Align last module at the bottom', 'divi-filter' ),
                            'type'              => 'yes_no_button',
                            'depends_show_if'   => 'on',
                            'options'           => array(
                                'off' => esc_html__( 'No', 'divi-filter' ),
                                'on'  => esc_html__( 'Yes', 'divi-filter' ),
                            ),
                            'description'        => esc_html__( 'Enable this to align the last module (probably the add to cart) at the bottom. Works well when using the equal height.', 'divi-filter' ),
                        ),
                        'fullwidth' => array(
                            'toggle_slug'       => 'custom_content',
                            'label'             => esc_html__( 'Layout', 'divi-filter' ),
                            'type'              => 'select',
                            'option_category'   => 'layout',
                            'options'           => array(
                                'list'  => esc_html__( 'List', 'divi-filter' ),
                                'off' => esc_html__( 'Grid', 'divi-filter' ),
                            ),
                            'default' => 'list',
                            'affects'=>array(
                                'columns',
                                'columns_tablet',
                                'columns_mobile'
                            ),
                            'description'        => esc_html__( 'Choose if you want it displayed as a list or a grid layout', 'divi-filter' ),
                        ),
                        'columns' => array(
                            'toggle_slug'       => 'custom_content',
                            'label'             => esc_html__( 'Grid Columns', 'divi-filter' ),
                            'type'              => 'select',
                            'option_category'   => 'layout',
                            'default'   => '3',
                            'options'           => array(
                                2  => esc_html__( 'Two', 'divi-filter' ),
                                3 => esc_html__( 'Three', 'divi-filter' ),
                                4 => esc_html__( 'Four', 'divi-filter' ),
                                5 => esc_html__( 'Five', 'divi-filter' ),
                                6 => esc_html__( 'Six', 'divi-filter' ),
                            ),
                            'depends_show_if' => 'off',
                            'description'        => esc_html__( 'How many columns do you want to see', 'divi-filter' ),
                        ),
                        'columns_tablet' => array(
                            'toggle_slug'       => 'custom_content',
                            'label'             => esc_html__( 'Tablet Grid Columns', 'divi-filter' ),
                            'type'              => 'select',
                            'option_category'   => 'layout',
                            'default'   => '2',
                            'options'           => array(
                                1  => esc_html__( 'One', 'divi-filter' ),
                                2  => esc_html__( 'Two', 'divi-filter' ),
                                3 => esc_html__( 'Three', 'divi-filter' ),
                                4 => esc_html__( 'Four', 'divi-filter' ),
                            ),
                            'depends_show_if' => 'off',
                            'description'        => esc_html__( 'How many columns do you want to see on tablet', 'divi-filter' ),
                        ),
                        'columns_mobile' => array(
                            'toggle_slug'       => 'custom_content',
                            'label'             => esc_html__( 'Mobile Grid Columns', 'divi-filter' ),
                            'type'              => 'select',
                            'option_category'   => 'layout',
                            'default'   => '1',
                            'options'           => array(
                                1  => esc_html__( 'One', 'divi-filter' ),
                                2  => esc_html__( 'Two', 'divi-filter' ),
                            ),
                            'depends_show_if'   => 'off',
                            'description'       => esc_html__( 'How many columns do you want to see on mobile', 'divi-filter' ),
                        ),
                        // 'sort_order' => array(
                        // 'label'             => esc_html__( 'Product Sort Order', 'divi-filter' ),
                        // 'type'              => 'select',
                        // 'options'           => array(
                        // "default"  => esc_html__( 'Default', 'divi-filter' ),
                        // "name"  => esc_html__( 'Name', 'divi-filter' ),
                        // "popularity" => esc_html__( 'Popularity', 'divi-filter' ),
                        // "rating" => esc_html__( 'Averagez Rating', 'divi-filter' ),
                        // "recent" => esc_html__( 'Most Recent', 'divi-filter' ),
                        // "price_asc" => esc_html__( 'Price Asc', 'divi-filter' ),
                        // "price_desc" => esc_html__( 'Price Desc', 'divi-filter' ),
                        // "random" => esc_html__( 'Random', 'divi-filter' ),
                        // ),
                        // 'description'        => esc_html__( 'Select the sort order of the loop', 'divi-filter' ),
                        // 'toggle_slug'       => 'main_content',
                        // ),
                        // 'et_shortcode' => array(
                        // 'toggle_slug'       => 'custom_content',
                        // 'option_category'   => 'layout',
                        // 'label'             => esc_html__( 'ET Shortcode for loop layout', 'divi-filter' ),
                        // 'type'              => 'select',
                        // 'options'           => array(
                        // 'et_pb_row'  => esc_html__( 'et_pb_row', 'divi-filter' ),
                        // 'et_pb_section' => esc_html__( 'et_pb_section', 'divi-filter' ),
                        // ),
                        // 'default' => 'et_pb_row',
                        // 'description'        => esc_html__( 'Choose what you want the loop layout shortcode to be. By default the row will be fine but if you have this module inside a speciality section it can cause issues with the html structure - so change it to be the section to see.', 'divi-filter' ),
                        // ),
                        'custom_loop' => array(
                            'toggle_slug'       => 'custom_content',
                            'label'             => esc_html__( 'Custom Loop Query', 'divi-filter' ),
                            'type'              => 'yes_no_button',
                            'options'           => array(
                                'off' => esc_html__( 'No', 'divi-filter' ),
                                'on'  => esc_html__( 'Yes', 'divi-filter' ),
                            ),
                            'affects'=>array(
                                'post_type',
                                'include_tags', 
                                'include_cats', 
                                'featured_only', 
                                'popular_only', 
                                'on_sale_only', 
                                'outofstock_only', 
                                'new_only', 
                                'sort_order', 
                                'order_asc_desc'
                            ),
                            'description'        => esc_html__( 'Enable this to create your own query, you can set the post number and to include products with specific tags only.', 'divi-filter' ),
                        ),
                        'include_cats' => array(
                            'toggle_slug'       => 'custom_content',
                            'label'           => esc_html__( 'Include Categories', 'divi-filter' ),
                            'type'            => 'text',
                            'depends_show_if'   => 'on',
                            'description'     => esc_html__( 'Add a list of categories that you ONLY want to show. This will remove all products that dont have these. (comma-seperated)', 'divi-filter' ),
                        ),
                        'include_tags' => array(
                            'toggle_slug'       => 'custom_content',
                            'label'           => esc_html__( 'Include Tags', 'divi-filter' ),
                            'type'            => 'text',
                            'depends_show_if'   => 'on',
                            'description'     => esc_html__( 'Add a list of tags that you ONLY want to show. This will remove all products that dont have these tags. (comma-seperated)', 'divi-filter' ),
                        ),
                        'featured_only' => array(
                            'toggle_slug'       => 'custom_content',
                            'label'             => esc_html__( 'Display featured products?', 'et_builder' ),
                            'type'              => 'yes_no_button',
                            'options'           => array(
                                'on'  => esc_html__( 'Yes', 'et_builder' ),
                                'off' => esc_html__( 'No', 'et_builder' ),
                            ),
                            'depends_show_if'   => 'on',
                        ),
                        'popular_only' => array(
                            'toggle_slug'       => 'custom_content',
                            'label'             => esc_html__( 'Display Most Popular products?', 'et_builder' ),
                            'type'              => 'yes_no_button',
                            'options'           => array(
                                'on'  => esc_html__( 'Yes', 'et_builder' ),
                                'off' => esc_html__( 'No', 'et_builder' ),
                            ),
                            'depends_show_if'   => 'on',
                        ),
                        'on_sale_only' => array(
                            'toggle_slug'       => 'custom_content',
                            'label'             => esc_html__( 'Display On Sale products?', 'et_builder' ),
                            'type'              => 'yes_no_button',
                            'options'           => array(
                                'on'  => esc_html__( 'Yes', 'et_builder' ),
                                'off' => esc_html__( 'No', 'et_builder' ),
                            ),
                            'depends_show_if'   => 'on',
                        ),
                        'outofstock_only' => array(
                            'toggle_slug'       => 'custom_content',
                            'label'             => esc_html__( 'Out of Stock products only?', 'et_builder' ),
                            'type'              => 'yes_no_button',
                            'options'           => array(
                                'on'  => esc_html__( 'Yes', 'et_builder' ),
                                'off' => esc_html__( 'No', 'et_builder' ),
                            ),
                            'depends_show_if'   => 'on',
                        ),
                        'new_only' => array(
                            'toggle_slug'       => 'custom_content',
                            'label'             => esc_html__( 'New products only?', 'et_builder' ),
                            'type'              => 'yes_no_button',
                            'options'           => array(
                                'on'  => esc_html__( 'Yes', 'et_builder' ),
                                'off' => esc_html__( 'No', 'et_builder' ),
                            ),
                            'depends_show_if'   => 'on',
                            'affects'=>array(
                                'new_time'
                            ),
                        ),
                        'new_time' => array(
                            'toggle_slug'       => 'custom_content',
                            'label'           => esc_html__( 'Number of days', 'divi-filter' ),
                            'type'            => 'text',
                            'depends_show_if'   => 'on',
                            'description'     => esc_html__( 'Define the number of days you want to show the products', 'divi-filter' ),
                        ),
                        'show_hidden_prod' => array(
                            'toggle_slug'       => 'custom_content',
                            'label'             => esc_html__( 'Show Hidden Products?', 'et_builder' ),
                            'type'              => 'yes_no_button',
                            'options'           => array(
                                'on'  => esc_html__( 'Yes', 'et_builder' ),
                                'off' => esc_html__( 'No', 'et_builder' ),
                            ),
                            'depends_show_if'   => 'on',
                            'default'           => 'off',
                        ),
                        'sort_order' => array(
                            'toggle_slug'       => 'custom_content',
                            'label'             => esc_html__( 'What do you want to sort your products by?', 'divi-filter' ),
                            'type'              => 'select',
                            'options'           => array(
                                'date' => sprintf( esc_html__( 'Date', 'divi-filter' ) ),
                                'title' => esc_html__( 'Title', 'divi-filter' ),
                                'ID' => esc_html__( 'ID', 'divi-filter' ),
                                'rand' => esc_html__( 'Random', 'divi-filter' ),
                                'menu_order' => esc_html__( 'Menu Order', 'divi-filter' ),
                                'name' => esc_html__( 'Name', 'divi-filter' ),
                                'modified' => esc_html__( 'Modified', 'divi-filter' ),
                            ),
                            'depends_show_if'   => 'on',
                            'default' => 'date',
                            'description'       => esc_html__( 'Choose what you want to sort the product by.', 'divi-filter' ),
                        ),
                        'order_asc_desc' => array(
                            'toggle_slug'       => 'custom_content',
                            'label'             => esc_html__( 'Sort Order', 'divi-filter' ),
                            'type'              => 'select',
                            'options'           => array(
                                'ASC' => esc_html__( 'Ascending', 'divi-filter' ),
                                'DESC' => sprintf( esc_html__( 'Descending', 'divi-filter' ) ),
                            ),
                            'depends_show_if'   => 'on',
                            'default' => 'ASC',
                            'description'       => esc_html__( 'Choose the sort order of the products.', 'divi-filter' ),
                        ),

                        // DEFAULT
                        'display_type' => array(
                            'label'             => esc_html__( 'Display Type', 'divi-filter' ),
                            'type'              => 'select',
                            'option_category'   => 'layout',
                            'options'           => array(
                                'grid'      => esc_html__( 'Grid', 'divi-filter' ),
                                'list_view' => esc_html__( 'Classic Blog', 'divi-filter' ),
                            ),
                            'default' => 'grid',
                            'affects' => array(
                                'columns_number',
                            ),
                            'toggle_slug'       => 'default_content',
                        ),
                        'columns_number' => array(
                            'label'             => esc_html__( 'Columns Number', 'divi-filter' ),
                            'type'              => 'select',
                            'option_category'   => 'layout',
                            'options'           => array(
                                '0' => esc_html__( '-- Default --', 'divi-filter' ),
                                '6' => sprintf( esc_html__( '%1$s Columns', 'divi-filter' ), esc_html( '6' ) ),
                                '5' => sprintf( esc_html__( '%1$s Columns', 'divi-filter' ), esc_html( '5' ) ),
                                '4' => sprintf( esc_html__( '%1$s Columns', 'divi-filter' ), esc_html( '4' ) ),
                                '3' => sprintf( esc_html__( '%1$s Columns', 'divi-filter' ), esc_html( '3' ) ),
                                '2' => sprintf( esc_html__( '%1$s Columns', 'divi-filter' ), esc_html( '2' ) ),
                                '1' => esc_html__( '1 Column', 'divi-filter' ),
                            ),
                            'computed_affects'  => array(
                                '__products',
                            ),
                            'depends_show_if'   => 'grid',
                            'default'           => '3',
                            'description'       => esc_html__( 'Choose how many columns to display. Default is 3.', 'divi-filter' ),
                            'toggle_slug'       => 'default_content',
                        ),
                        'show_rating' => array(
                            'label'     => esc_html__( 'Show Rating', 'divi-filter' ),
                            'type'      => 'yes_no_button',
                            'option_category' => 'configuration',
                            'options' => array(
                                'on'  => esc_html__( 'Yes', 'divi-filter' ),
                                'off' => esc_html__( 'No', 'divi-filter' ),
                            ),
                            'default' => 'on',
                            'toggle_slug' => 'default_content',
                            'affects' => array(
                                'stars_color',
                            ),
                        ),
                        'show_price' => array(
                            'label' => esc_html__( 'Show Price', 'divi-filter' ),
                            'type' => 'yes_no_button',
                            'option_category' => 'configuration',
                            'options' => array(
                                'on'  => esc_html__( 'Yes', 'divi-filter' ),
                                'off' => esc_html__( 'No', 'divi-filter' ),
                            ),
                            'default' => 'on',
                            'toggle_slug' => 'default_content',
                        ),
                        'show_excerpt' => array(
                            'label' => esc_html__( 'Show Excerpt', 'divi-filter' ),
                            'type' => 'yes_no_button',
                            'option_category' => 'configuration',
                            'options' => array(
                                'off' => esc_html__( 'No', 'divi-filter' ),
                                'on'  => esc_html__( 'Yes', 'divi-filter' ),
                            ),
                            'toggle_slug' => 'default_content',
                            'computed_affects' => array(
                                '__products',
                            ),
                        ),
                        'show_add_to_cart' => array(
                            'label' => esc_html__( 'Show Add To Cart Button', 'divi-filter' ),
                            'type' => 'yes_no_button',
                            'option_category' => 'configuration',
                            'options' => array(
                                'off' => esc_html__( 'No', 'divi-filter' ),
                                'on'  => esc_html__( 'Yes', 'divi-filter' ),
                            ),
                            'toggle_slug' => 'default_content',
                            'computed_affects' => array(
                                '__products',
                            ),
                        ),
                        'sale_badge_color' => array(
                            'label'             => esc_html__( 'Sale Badge Color', 'divi-filter' ),
                            'type'              => 'color-alpha',
                            'custom_color'      => true,
                            'toggle_slug'       => 'default_content',
                        ),
                        'stars_color' => array(
                            'label'         => esc_html__( 'Rating Stars Color', 'divi-filter' ),
                            'type'          => 'color-alpha',
                            'toggle_slug'   => 'default_content',
                        ),
                        'use_overlay' => array(
                            'label' => esc_html__( 'Use Overlay', 'divi-filter' ),
                            'type' => 'yes_no_button',
                            'options' => array(
                                'on' => esc_html__( 'Yes', 'divi-filter' ),
                                'off' => esc_html__( 'No', 'divi-filter' ),
                            ),
                            'default' => 'on',
                            'affects' => array(
                                'icon_hover_color',
                                'hover_overlay_color',
                                'hover_icon',
                            ),
                            'tab_slug' => 'advanced',
                            'toggle_slug' => 'overlay',
                        ),
                        'icon_hover_color' => array(
                            'label'             => esc_html__( 'Icon Color', 'divi-filter' ),
                            'type'              => 'color-alpha',
                            'custom_color'      => true,
                            'depends_show_if'   => 'on',
                            'tab_slug'          => 'advanced',
                            'toggle_slug'       => 'overlay',
                        ),
                        'hover_overlay_color' => array(
                            'label'             => esc_html__( 'Overlay Color', 'divi-filter' ),
                            'type'              => 'color-alpha',
                            'custom_color'      => true,
                            'depends_show_if'   => 'on',
                            'tab_slug'          => 'advanced',
                            'toggle_slug'       => 'overlay',
                        ),
                        'hover_icon' => array(
                            'label'               => esc_html__( 'Icon Picker', 'divi-filter' ),
                            'type'                => 'select_icon',
                            'option_category'     => 'configuration',
                            'class'               => array( 'et-pb-font-icon' ),
                            'default'             => 'P',
                            'depends_show_if'     => 'on',
                            'tab_slug'            => 'advanced',
                            'toggle_slug'         => 'overlay',
                        ),
                        'product_background' => array(
                            'label'             => esc_html__( 'Product Background', 'divi-filter' ),
                            'type'              => 'color-alpha',
                            'custom_color'      => true,
                            'toggle_slug'       => 'default_content',
                        ),
                        'product_padding' => array(
                            'label'             => esc_html__( 'Product Padding', 'divi-filter' ),
                            'type'              => 'range',
                            'default'           => '0px',
                            'toggle_slug'       => 'default_content',
                        ),
                        '__products' => array(
                            'type' => 'computed',
                            'computed_callback' => array( 'db_filter_loop_code', 'get_products' ),
                            'computed_depends_on' => array(
                                'columns_number',
                                'show_add_to_cart',
                                'show_excerpt',
                                'loop_layout',
                                'cat_loop_style',
                                'fullwidth',
                                'include_tags',
                                'include_cats',
                                'posts_number',
                                'custom_loop',
                                'sort_order',
                                'order_asc_desc',
                                'post_type_choose',
                                'columns',
                                'columns_tablet',
                                'columns_mobile',
                                'show_sorting_menu',
                                'show_results_count',
                                'show_hidden_prod',
                                'enable_pagination'
                            ),
                        ),
                    );

                    return $fields;
                }


                // GET COMPUTED PRODUCTS
                public static function get_products( $args = array(), $conditional_tags = array(), $current_page = array() ){
                    if (!is_admin()) {
                        return;
                    }
                    
                    $post_type_choose  = $args['post_type_choose'];
           
                    
                    ob_start();
                    
                    if( isset( $args['cat_loop_style'] ) && $args['cat_loop_style'] == 'on' ){

                        
                        if ($post_type_choose !== "product") {

                            echo '<div class="no-html-output" style="background-color:#1d0d6f;color:#fff;padding:40px 60px;clear: both;"><p>Default layout only works with products from WooCommerce at the moment. <br>
                            To fix
                            <ol>
                            <li>Choose "Custom Layout" in the setting -> Loop Style</li>
                            <li>Create a custom loop layout in the Divi Library</li>
                            <li>Assign this layout created in step 2 in the setting -> Loop Layout</li>
                            </ol>
                            </p>
                            </div>';

                        } else {
                          global $post, $columns;
                           $term               = false;
                           $shortcode_options  = '';
                            
                           if( isset( $_REQUEST['et_post_id'] ) ){
                              $post_id = absint( $_REQUEST['et_post_id'] );
                          }elseif( isset( $_REQUEST['current_page']['id'] ) ){
                              $post_id = absint( $_REQUEST['current_page']['id'] );
                          }else{
                              $post_id = false;
                          }

                           $post               = get_post( $post_id );
                          $columns            = isset( $args['columns_number'] ) && absint( $args ['columns_number'] ) > 0 ? absint( $args['columns_number'] ) : 3;

                           // columns
                          add_filter( 'loop_shop_columns', function($c){
                              global $columns;
                              return $columns;
                          }, 9999 );

                          // get the current posts per page
                         $limit = 9;

                          // add to cart
                          if( isset( $args['show_add_to_cart'] ) && $args['show_add_to_cart'] == 'on' ){
                              add_action( 'woocommerce_after_shop_loop_item',     'woocommerce_template_loop_add_to_cart', 9 );
                           }

                          if( isset( $args['show_excerpt'] ) && $args['show_excerpt'] == 'on' ){
                              add_action( 'woocommerce_after_shop_loop_item',   'woocommerce_template_single_excerpt', 8 );
                          }

                           add_action( 'woocommerce_shop_loop_item_title', array( 'db_filter_loop_code', 'product_details_wrapper_start' ), 0 );
                          add_action( 'woocommerce_shop_loop_subcategory_title', array( 'db_filter_loop_code', 'product_details_wrapper_start' ), 0 );
                          add_action( 'woocommerce_after_shop_loop_item', array( 'db_filter_loop_code', 'product_details_wrapper_end' ), 10 );
                          add_action( 'woocommerce_after_subcategory', array( 'db_filter_loop_code', 'product_details_wrapper_end' ), 10 );
                          add_action( 'woocommerce_before_shop_loop_item_title', array( 'db_filter_loop_code', 'product_image_wrapper_start' ), 0 );
                          add_action( 'woocommerce_before_subcategory_title', array( 'db_filter_loop_code', 'product_image_wrapper_start' ), 0 );
                          add_action( 'woocommerce_before_shop_loop_item_title', array( 'db_filter_loop_code', 'product_image_wrapper_end' ), 20 );
                          add_action( 'woocommerce_before_subcategory_title', array( 'db_filter_loop_code', 'product_image_wrapper_end' ), 20 );

                          remove_all_actions('woocommerce_after_shop_loop');
                          $shortcode = "[products paginate='true' limit='{$limit}' {$shortcode_options}]";
                          
                        if( $enable_pagination == 'off' ){
                            remove_action( 'woocommerce_after_shop_loop', 'woocommerce_pagination', 10 );
                        }

                          echo do_shortcode( $shortcode );
                        }
                        
                    } else if ( isset( $args['cat_loop_style'] ) && $args['cat_loop_style'] == 'off' ) {

                        $loop_layout                    = $args['loop_layout'];
                        $columns                        = $args['columns'];
                        $columns_tablet                 = $args['columns_tablet'];
                        $columns_mobile                 = $args['columns_mobile'];    
                        $include_cats                   = $args['include_cats'];
                        $include_tags                   = $args['include_tags'];
                        $sort_order                     = $args['sort_order'];
                        $order_asc_desc                 = $args['order_asc_desc'];
                        $show_sorting_menu              = $args['show_sorting_menu'];
                        $show_results_count             = $args['show_results_count'];
                        $fullwidth                      = $args['fullwidth'];
                        $show_hidden_prod               = $args['show_hidden_prod'];
                        $enable_pagination              = $args['enable_pagination'];
                        
                        
    
                        $get_cpt_args = array(
                            'post_type' => $post_type_choose,
                            'post_status' => $post_status,
                            'posts_per_page' => (int) $posts_number,
                            'orderby' => $sort_order,
                            'order' => $order_asc_desc,
                          );

                          if ($include_cats != "") {
                            if ($post_type_choose == "post") {
                                $get_cpt_args['category_name'] = $include_cats;
                            } else if ( $post_type_choose == 'product' ) {
                                $get_cpt_args['product_cat'] = $include_cats;
                            } else {

                                if ( !empty( $cpt_taxonomies ) && in_array( 'category', $cpt_taxonomies ) ){
                                    $get_cpt_args['category_name'] = $include_cats;
                                }else{
                                    $ending = "_category";
                                    $cat_key = $post_type_choose . $ending;
                                    if ($cat_key == "product_category") {
                                        $cat_key = "product_cat";
                                    } else {
                                        $cat_key = $cat_key;
                                    }

                                    $get_cpt_args['tax_query'][] = array(
                                        'taxonomy'  => $cat_key,
                                        'field'     => 'slug',
                                        'terms'     => $include_cats,
                                        'operator' => 'IN'
                                    );  
                                }
                            }
                        }

                        if ($include_tags != "") {
                            $get_cpt_args['product_tag'] = $include_tags;
                            if ($post_type_choose == "post") {
                                $get_cpt_args['tag'] = $include_tags;
                            } else {
                                $ending = "_tag";
                                $cat_key = $post_type_choose . $ending;

                                $get_cpt_args['tax_query'][] = array(
                                    'taxonomy'  => $cat_key,
                                    'field'     => 'slug',
                                    'terms'     => $include_tags,
                                    'operator' => 'IN'
                                );
                            }
                        }

                        if ($featured_only == "on") {
                            $tax_query[] = array(
                                'taxonomy' => 'product_visibility',
                                'field'    => 'name',
                                'terms'    => 'featured',
                                'operator' => 'IN',
                            );

                            $get_cpt_args['tax_query'] = $tax_query;
                        }

                        // POPULAR
                        if ($popular_only == "on") {
                            $customclass = "popular-products";
                            $get_cpt_args['meta_key'] = 'total_sales';
                            $get_cpt_args['orderby']  = 'meta_value_num';
                        }

                        // ON SALE
                        if ($on_sale_only == "on") {
                            $customclass = "onsale-products";
                            $products_on_sale = wc_get_product_ids_on_sale();
                            $get_cpt_args['post__in'] = $products_on_sale;            
                        }

                        // NEW PRODUCT
                        if ($new_only == "on") {
                            $customclass = "new-products";
                            $get_cpt_args['date_query'] = array(
                                array(
                                    'after'     => '-'.$new_time.' days',
                                    'column'    => 'post_date',
                                )
                            );
                        }

                        if ($show_hidden_prod == "off") {

                            if ( ! empty( $product_visibility_not_in ) ) {
                                $get_cpt_args['tax_query'][] = array(
                                    'taxonomy' => 'product_visibility',
                                    'field'    => 'term_taxonomy_id',
                                    'terms'    => $product_visibility_not_in,
                                    'operator' => 'NOT IN',
                                );
                                if ( $featured_only == "on" ){
                                    $get_cpt_args['tax_query']['relation'] = 'AND';
                                }
                            }
                        }
                        
                          
                        if ($fullwidth == 'list') {
                            $module_class .= ' et_pb_db_filter_loop_list et_pb_db_filter_loop_hide custom-layout';
                            echo '<style>.et_shop_image {display:inline-block;}</style>';
                        } else {
                            $module_class .= ' et_pb_db_filter_loop_grid et_pb_db_filter_loop_hide custom-layout';
                        }
                        
                        if ($post_type_choose == "product") {
                            if( $show_sorting_menu == 'off' ){
                                remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
                            }
                            if( $show_results_count == 'off' ){
                                remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
                            }
                        }

                        query_posts($get_cpt_args);

                        if ($loop_layout == "none") {
                            echo '<div class="no-html-output" style="background-color:#1d0d6f;color:#fff;padding:40px 60px;clear: both;">
                            <p style="color:#fff;">You have selected a custom layout but have not selected a loop layout for your products</p>
                            <p>Please create a <a href="https://www.youtube.com/watch?v=mLiUJ_hvBjE" target="_blank">custom loop layout</a> and specify it in the settings, or change the layout style to be default.</p>
                            </div>';
                        } else {

                            if ( have_posts() ){
                                if ( $post_type_choose == 'product' ){
                                    ?>
                                    <div class="woocommerce-page">
                                    <?php
                                    do_action( 'woocommerce_before_shop_loop' ); 
                                }
                                ?>
                                <div class="filtered-posts-cont">
                                    <div class="divi-filter-archive-loop has-result">
                                        <div class="divi-filter-loop-container col-desk-<?php echo $cols;?> col-tab-<?php echo $columns_tablet;?> col-mob-<?php echo $columns_mobile;?>">

                                        <?php
                                        
                                        if ( $post_type_choose == 'product') {
                                            if ($fullwidth == 'off') { //grid
                                                echo '<ul class="et_pb_row_bodycommerce custom-loop-layout products bc_product_grid bc_product_' . $cols . ' bc_pro_tab_'. $columns_tablet .' bc_pro_mob_'. $columns_mobile .'">';
                                            }
                                            else if ($fullwidth  == 'list') {
                                                echo '<ul class="et_pb_row_bodycommerce custom-loop-layout products">';
                                            }    
                                        } else {
                                            echo '<ul class="et_pb_row_divifilter custom-loop-layout">';
                                        }  
                                        
                                        while ( have_posts() ) {
                                            the_post();
                                            global $product, $post;
                                            echo '<li>';
                                            $product_content = apply_filters('the_content', get_post_field('post_content', $loop_layout));
                            
                                            $product_content = preg_replace( '/et_pb_([a-z]+)_(\d+)_tb_body/', 'et_pb_df_ajax_filter_${1}_${2}_tb_body', $product_content );
                                            $product_content = preg_replace( '/et_pb_([a-z]+)_(\d+)( |")/', 'et_pb_df_ajax_filter_${1}_${2}${3}', $product_content );
                
                                            echo $product_content;
                                            echo '</li>';
                                        }
                                        
                                        echo '</ul>';
                                        wp_reset_query();
                                        ?>

                                        </div>
                                    </div>
                                 </div>
                                <?php
                                if ( 'on' === $enable_pagination ) {
                                    do_action( 'woocommerce_after_shop_loop' );
                                }

                                 if ( $post_type_choose == 'product' ){
                                    ?>
                                    </div>
                                    <?php
                                }
                            }

                             // retrieve the styles for the modules
                             $internal_style = ET_Builder_Element::get_style();
                             // reset all the attributes after we retrieved styles
                             ET_Builder_Element::clean_internal_modules_styles( false );
                             $et_pb_rendering_column_content = false;
                             // append styles
                             if ( $internal_style ) {
                                 ?>
                                 <div class="df-inner-styles">
                                     <?php
                                     //$cleaned_styles = str_replace("#et-boc .et-l","#et-boc .et-l .filtered-posts", $internal_style);
                                     $cleaned_styles = preg_replace( '/et_pb_([a-z]+)_(\d+)_tb_body/', 'et_pb_df_ajax_filter_${1}_${2}_tb_body', $internal_style );
                                     $cleaned_styles = preg_replace( '/et_pb_([a-z]+)_(\d+)( |"|.)/', 'et_pb_df_ajax_filter_${1}_${2}${3}', $cleaned_styles );
                                     printf(
                                         '<style type="text/css" class="dmach_ajax_inner_styles">
                                         %1$s
                                         </style>',
                                         et_core_esc_previously( $cleaned_styles )
                                    );
                                    ?>
                                </div>
                                <?php
                            }
                        }
                        
                    }
                    $shop = ob_get_clean();
                    return $shop;

                }

                // END COMPUTED PRODUCTS
                public function change_columns_number( $c ){

                    $columns = 3;
                    if( absint( $this->columns ) > 0 ){
                        $columns = absint( $this->columns );
                    }
                    return $columns;
                }

                public static function product_details_wrapper_start(){
                    echo "<div class='de_db_product_details'>";
                }
                public static function product_details_wrapper_end(){
                    echo "</div>";
                }
                public static function product_image_wrapper_start(){
                    echo "<div class='de_db_product_image'>";
                }
                public static function product_image_wrapper_end(){
                    echo "</div>";
                }

                function render( $attrs, $content = null, $render_slug ) {

                    if( is_admin() ){
                        return;
                    }

                    $cat_loop_style         = $this->props['cat_loop_style'];
                    $background_layout      = '';
                    $loop_layout            = $this->props['loop_layout'];
                    $cols                   = $this->props['columns'];
                    $columns_tablet         = $this->props['columns_tablet'];
                    $columns_mobile         = $this->props['columns_mobile'];
                    $module_id              = $this->props['module_id'];
                    $module_class           = $this->props['module_class'];
                    $fullwidth              = $this->props['fullwidth'];
                    $include_tags           = $this->props['include_tags'];
                    $include_cats           = $this->props['include_cats'];
                    $posts_number           = $this->props['posts_number'];
                    $custom_loop            = $this->props['custom_loop'];
                    $post_type_choose       = $this->props['post_type_choose'];
                    $sort_order             = $this->props['sort_order'];
                    $order_asc_desc         = $this->props['order_asc_desc'];
                    $featured_only          = $this->props['featured_only'];
                    $popular_only           = $this->props['popular_only'];
                    $on_sale_only           = $this->props['on_sale_only'];
                    $outofstock_only        = $this->props['outofstock_only'];
                    $link_whole_gird        = $this->props['link_whole_gird'];
                    $new_only               = $this->props['new_only'];
                    $new_time               = $this->props['new_time'];

                    // $product_type        = $this->props['product_type'];

                    // DEFAULT
                    $display_type                           = $this->props['display_type'];
                    $is_main_loop                           = $this->props['is_main_loop'];
                    $main_loop                              = ($is_main_loop == 'on')?true:false;
                    $show_sorting_menu                      = $main_loop?$this->props['show_sorting_menu']:'off';
                    $show_results_count                     = $main_loop?$this->props['show_results_count']:'off';
                    $show_rating                            = $this->props['show_rating'];
                    $show_price                             = $this->props['show_price'];
                    $show_excerpt                           = $this->props['show_excerpt'];
                    $show_add_to_cart                       = $this->props['show_add_to_cart'];
                    $enable_pagination                      = $main_loop?$this->props['enable_pagination']:'off';
                    $this->columns                          = $this->props['columns_number'];
                    $sale_badge_color                       = $this->props['sale_badge_color'];
                    $stars_color                            = $this->props['stars_color'];
                    $use_overlay                            = $this->props['use_overlay'];
                    $icon_hover_color                       = $this->props['icon_hover_color'];
                    $hover_overlay_color                    = $this->props['hover_overlay_color'];
                    $hover_icon                             = $this->props['hover_icon'];
                    $product_background                     = $this->props['product_background'];
                    $product_padding                        = $this->props['product_padding'];

                    $custom_add_to_cart_button              = $this->props['custom_add_to_cart_button'];
                    $add_to_cart_button_bg_color            = $this->props['add_to_cart_button_bg_color'];
                    $add_to_cart_button_icon                = $this->props['add_to_cart_button_icon'];
                    $add_to_cart_button_icon_placement      = $this->props['add_to_cart_button_icon_placement'];

                    $equal_height_mob       = $this->props['equal_height_mob'];
                    $equal_height           = $this->props['equal_height'];
                    $align_last_bottom      = $this->props['align_last_bottom'];
                    $hide_non_purchasable   = $this->props['hide_non_purchasable'];
                    // $et_shortcode        = $this->props['et_shortcode'];
                    $disable_products_has_cat = $this->props['disable_products_has_cat'];
                    $exclude_products       = $this->props['exclude_products'];
                    $no_results_layout      = $this->props['no_results_layout'];
                    $show_hidden_prod       = $this->props['show_hidden_prod'];

                    $filter_update_animation = $this->props['filter_update_animation'];
                    $animation_color        = $this->props['animation_color'];
                    $loading_bg_color        = $this->props['loading_bg_color'];

                    

                
                    $link_whole_gird_new_tab  = $this->props['link_whole_gird_new_tab'];
                    if ( $link_whole_gird_new_tab == 'on' ) {
                      $this->add_classname('link_whole_new_tab');
                    }

                    if ($fullwidth == 'list') {
                        $cols = 1;
                        $columns_tablet = 1;
                        $columns_mobile = 1;
                    }

                        //////////////////////////////////////////////////////////////////////

                    if ( $equal_height == 'on' ) {
                        $this->add_classname('same-height-cards');
                    }
                    if ( $align_last_bottom == 'on' ) {
                        $this->add_classname('align-last-module');
                    }

                    if ( !empty( $_GET['orderby'] ) ){
                        $sort_order = $_GET['orderby'];
                    }

                    if ( '' !== $loading_bg_color ) {
                        ET_Builder_Element::set_style( $render_slug, array(
                          'selector'    => '%%order_class%% .ajax-loading',
                          'declaration' => sprintf(
                            'background-color: %1$s !important;',
                            esc_html( $loading_bg_color )
                          ),
                        ) );
                      }
                  
                      if ( '' !== $animation_color ) {
                        ET_Builder_Element::set_style( $render_slug, array(
                          'selector'    => '%%order_class%% .line',
                          'declaration' => sprintf(
                            'background-color: %1$s !important;',
                            esc_html( $animation_color )
                          ),
                        ) );
                      }
                  
                      if ( '' !== $animation_color ) {
                        ET_Builder_Element::set_style( $render_slug, array(
                          'selector'    => '%%order_class%% .donut',
                          'declaration' => sprintf(
                            'border-top-color: %1$s !important;',
                            esc_html( $animation_color )
                          ),
                        ) );
                      }
                  
                      if ( '' !== $animation_color ) {
                        ET_Builder_Element::set_style( $render_slug, array(
                          'selector'    => '%%order_class%% .donut.multi',
                          'declaration' => sprintf(
                            'border-bottom-color: %1$s !important;',
                            esc_html( $animation_color )
                          ),
                        ) );
                      }
                  
                      if ( '' !== $animation_color ) {
                        ET_Builder_Element::set_style( $render_slug, array(
                          'selector'    => '%%order_class%% .ripple',
                          'declaration' => sprintf(
                            'border-color: %1$s !important;',
                            esc_html( $animation_color )
                          ),
                        ) );
                      }

                    global $wp_query;

                    ob_start();

                    if (function_exists('is_product_category') && is_product_category() && $disable_products_has_cat == "on") {
                        global $post;
                        $cate = get_queried_object();
                        $cateID = $cate->term_id;
                        $terms = get_terms('product_cat', array('orderby' => 'ASC', 'parent' => $cateID, ));
                        if ($terms) {
                            return;
                        }
                    }

                    $et_paged = is_front_page() ? get_query_var( 'page' ) : get_query_var( 'paged' );

                    if ( $is_main_loop == 'off' ){
                        $et_paged = 1;
                    }
                    
                    if ( $custom_loop != 'on') {
                        $order_asc_desc = 'DESC';
                        $sort_order = 'DATE';
                    }

                    $args = array(
                        'post_type'         => $post_type_choose,
                        'orderby'           => $sort_order,
                        'order'             => $order_asc_desc,
                        'post_status'       => 'publish',
                        'paged'             => $et_paged,
                        'posts_per_page'    => (int) $posts_number,
                        'meta_query'        => array(
                            'relation'      => 'AND'
                        ),
                        'tax_query'         => array(
                            'relation'      => 'AND'
                        ),
                        'post__not_in'      => explode(',', $exclude_products ),
                    );

                    $is_search = is_search();

                    if ( $is_search && $is_main_loop == 'on' ) {

                        // Add compatibility with Algolia plugin
                        add_filter('algolia_should_filter_query', function($should_filter, $query) { return true; }, 20, 2 );

                        // Add compatibility with Relevanssi plugin
                        add_filter('relevanssi_search_ok', function($search_ok, $query) { return true; }, 20, 2 );
                    }

                    if ( is_archive() ){
                        $args = array_merge( $wp_query->query_vars, $args);
                    }

                    if ( $custom_loop != 'on') {
                        $orderby_value = apply_filters( 'woocommerce_default_catalog_orderby', get_option( 'woocommerce_default_catalog_orderby', 'menu_order' ) );
                        $orderby_value = is_array( $orderby_value ) ? $orderby_value : explode( '-', $orderby_value );
                        $orderby       = esc_attr( $orderby_value[0] );
                        $order         = ! empty( $orderby_value[1] ) ? $orderby_value[1] : '';
                        if ( function_exists('WC' ) ){
                            $orderby_args = WC()->query->get_catalog_ordering_args( $orderby, $order );    
                        }else{
                            $orderby_args = array(
                                'orderby' => $orderby,
                                'order' => $order
                            );
                        }

                        $args = array_merge( $args, $orderby_args );
                    }

                    if ( $post_type_choose != 'product' ){
                        $taxonomies = get_object_taxonomies( $post_type_choose );

                        foreach ($taxonomies as $tax_name ) {
                            if ( $main_loop && !empty( $_GET[$tax_name] ) ) {
                                if ( !empty( $args['tax_query'] ) ){
                                    $args['tax_query'][] = array(
                                        'taxonomy'  => $tax_name,
                                        'field'     => 'slug',
                                        'terms'     => explode( ',' , $_GET[$tax_name] ),
                                        'operator' => 'IN'
                                    );
                                }
                            }
                        }
                    }

                    $acf_fields = array();
                    if ( function_exists('acf_get_field_groups')){
                        $field_groups = acf_get_field_groups();    
                        foreach ( $field_groups as $group ) {
                            // DO NOT USE here: $fields = acf_get_fields($group['key']);
                            // because it causes repeater field bugs and returns "trashed" fields
                            $fields = get_posts(array(
                                'posts_per_page'   => -1,
                                'post_type'        => 'acf-field',
                                'orderby'          => 'menu_order',
                                'order'            => 'ASC',
                                'suppress_filters' => true, // DO NOT allow WPML to modify the query
                                'post_parent'      => $group['ID'],
                                'post_status'      => 'any',
                                'update_post_meta_cache' => false
                            ));
                            foreach ( $fields as $field ) {
                                $acf_fields[$field->post_name] = $field->post_excerpt;
                            }
                        }
                    }

                    if ( $main_loop && !empty( $acf_fields ) ){
                        foreach ($acf_fields as $key => $field) {
                            if ( !empty( $_GET[$field] ) ){
                                $acf_get = get_field_object($key);
                                $acf_type = $acf_get['type'];

                                // append meta query
                                if ($acf_type == 'range') {

                                    $value_between = str_replace("%3B",";", $_GET[ $field ]);
                                    $value = ( explode( ";", $value_between ) );
                                    if ( sizeof( $value ) == 1 ) {
                                        $args['meta_query'][] = array(
                                            'key' => $field,
                                            'value' => $value[0],
                                            'type' => 'NUMERIC',
                                            'compare' => '<='
                                        );
                                    }else{
                                        $args['meta_query'][] = array(
                                            'key' => $field,
                                            'value' => $value,
                                            'compare' => 'BETWEEN',
                                            'type' => 'NUMERIC'
                                        );
                                    }
                                } else if ($acf_type == "checkbox" || "select" ) {
                                    if ( $acf_get['multiple'] == 1 ){
                                    $multi_values = explode( ',', $_GET[ $field ] );
                                    if ( is_array( $multi_values ) && sizeof($multi_values) > 1 ){
                                        $query_arr = array( 'relation' => 'OR' );
                                        foreach ( $multi_values as $meta_val ) {
                                            $query_arr[] = array(
                                                'key'       => $field,
                                                'value'     => '"' . $meta_val . '"',
                                                'compare'   => 'LIKE',
                                            );
                                        }
                                        $args['meta_query'][] = $query_arr;
                                    }else{
                                        $args['meta_query'][] = array(
                                            'key'       => $field,
                                            'value'     => '"' . $_GET[ $field ] . '"',
                                            'compare'   => 'LIKE',
                                        );
                                    }
                                    }else{
                                        $multi_values = explode( ',', $_GET[ $field ] );
                                        if ( is_array( $multi_values ) && sizeof($multi_values) > 1 ){
                                            $args['meta_query'][] = array(
                                                'key'       => $field,
                                                'value'     => $multi_values,
                                                'compare'   => 'IN',
                                            );
                                        }
                                    }
                                    
                                } else {
                                    $value = explode(',', $_GET[ $field ]);
                                    $args['meta_query'][] = array(
                                        'key'       => $field,
                                        'value'     => $value,
                                        'compare'   => 'IN',
                                    );
                                }
                            }
                        }
                    }

                    if ($cat_loop_style == "off") { // CUSTOM LAYOUT

                        if( $show_sorting_menu == 'off' ){
                            remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
                        }
                        if( $show_results_count == 'off' ){
                            remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
                        }

                        global $paged;

                        $module_class = ET_Builder_Element::add_module_order_class( $module_class, $render_slug );

                        $container_is_closed = false;

                        if ($fullwidth == 'list') {
                            $module_class .= ' et_pb_db_filter_loop_list et_pb_db_filter_loop_hide custom-layout';
                            echo '<style>.et_shop_image {display:inline-block;}</style>';
                        } else {
                            $module_class .= ' et_pb_db_filter_loop_grid et_pb_db_filter_loop_hide custom-layout';
                        }

                        if ($custom_loop == 'on') { // CUSTOM LOOP

                            if ($include_cats != "") {
                                if ($post_type_choose == "post") {
                                    $args['category_name'] = $include_cats;
                                } else if ( $post_type_choose == 'product' ) {
                                    $args['product_cat'] = $include_cats;
                                } else {

                                    if ( !empty( $cpt_taxonomies ) && in_array( 'category', $cpt_taxonomies ) ){
                                        $args['category_name'] = $include_cats;
                                    }else{
                                        $ending = "_category";
                                        $cat_key = $post_type_choose . $ending;
                                        if ($cat_key == "product_category") {
                                            $cat_key = "product_cat";
                                        } else {
                                            $cat_key = $cat_key;
                                        }

                                        $args['tax_query'][] = array(
                                            'taxonomy'  => $cat_key,
                                            'field'     => 'slug',
                                            'terms'     => $include_cats,
                                            'operator' => 'IN'
                                        );  
                                    }
                                }
                            }

                            if ($include_tags != "") {
                                $args['product_tag'] = $include_tags;
                                if ($post_type_choose == "post") {
                                    $args['tag'] = $include_tags;
                                } else {
                                    $ending = "_tag";
                                    $cat_key = $post_type_choose . $ending;

                                    $args['tax_query'][] = array(
                                        'taxonomy'  => $cat_key,
                                        'field'     => 'slug',
                                        'terms'     => $include_tags,
                                        'operator' => 'IN'
                                    );
                                }
                            }

                            
                            // FEATURED
                            if ($featured_only == "on") {
                                $tax_query[] = array(
                                    'taxonomy' => 'product_visibility',
                                    'field'    => 'name',
                                    'terms'    => 'featured',
                                    'operator' => 'IN',
                                );

                                $args['tax_query'] = $tax_query;
                            }

                            // POPULAR
                            if ($popular_only == "on") {
                                $customclass = "popular-products";
                                $args['meta_key'] = 'total_sales';
                                $args['orderby']  = 'meta_value_num';
                            }

                            // ON SALE
                            if ($on_sale_only == "on") {
                                $customclass = "onsale-products";
                                $products_on_sale = wc_get_product_ids_on_sale();
                                $args['post__in'] = $products_on_sale;            
                            }

                            // NEW PRODUCT
                            if ($new_only == "on") {
                                $customclass = "new-products";
                                $args['date_query'] = array(
                                    array(
                                        'after'     => '-'.$new_time.' days',
                                        'column'    => 'post_date',
                                    )
                                );
                            }

                            if ( is_single() && ! isset( $args['post__not_in'] ) ) {
                                $args['post__not_in'] = array( get_the_ID() );
                            }


                            // $wc_query = new WC_Query();
                            // $ordering = $wc_query->get_catalog_ordering_args();
                            // $args['orderby'] = $ordering['orderby'];
                            // $args['order']  = $ordering['order'];

                            if ( !empty( $ordering['meta_key'] ) ){
                                $args['meta_key'] = $ordering['meta_key'];
                            }

                            //query_posts( $args );

                        
                        } // END CUSTOM LOOP

                        if ( class_exists( 'woocommerce' ) && $post_type_choose == 'product' ) {
                            $product_visibility_terms  = wc_get_product_visibility_term_ids();
                            $product_visibility_not_in = array( is_search() ? $product_visibility_terms['exclude-from-search'] : $product_visibility_terms['exclude-from-catalog'] );
                            if ( 'yes' === get_option( 'woocommerce_hide_out_of_stock_items' ) ) {
                                $product_visibility_not_in[] = $product_visibility_terms['outofstock'];
                            }

                            if ( $hide_non_purchasable == "on" || $show_hidden_prod == "off" ) {

                                if ( ! empty( $product_visibility_not_in ) ) {
                                    $args['tax_query'][] = array(
                                        'taxonomy' => 'product_visibility',
                                        'field'    => 'term_taxonomy_id',
                                        'terms'    => $product_visibility_not_in,
                                        'operator' => 'NOT IN',
                                    );
                                }
                            }

                            if ( $hide_non_purchasable == "on" ){
                                $args['meta_query'] = array(
                                    array(
                                        'key'     => '_price',
                                        'value'   => '',
                                        'compare' => '!='
                                    )
                                );
                            }
                        }

                        //$args = $wp_query->query_vars;

                        if ( $main_loop && !empty( $_GET['page'] ) ) {
                            $args['paged'] = $_GET['page'];
                        }

                        if ( $post_type_choose == 'product' ) {

                            $product_taxonomies = get_object_taxonomies( 'product' );
                            if ( $main_loop ) {
                                foreach ($product_taxonomies as $tax_name ) {
                                    if ( !empty( $_GET[$tax_name] ) ) {
                                        if ( !empty( $args['tax_query'] ) ){
                                            $args['tax_query'][] = array(
                                                'taxonomy'  => $tax_name,
                                                'field'     => 'slug',
                                                'terms'     => explode( ',' , $_GET[$tax_name] ),
                                                'operator' => 'IN'
                                            );
                                        }
                                    }
                                }
                            }
                            
                            foreach ( array( "product_cat", "product_tag" ) as $tax_key ) {
                                if ( $main_loop && !empty( $_GET[$tax_key] ) ) {
                                    unset( $args[$tax_key] );
                                    if ( !empty( $args['taxonomy']) && $args['taxonomy'] == $tax_key ) {
                                        unset( $args['taxonomy']);
                                        unset( $args['term']);
                                    }
                                }else{
                                    if ( !empty( $args[$tax_key] ) ) {

                                        if ( strpos( $args[$tax_key], '+' ) === false ){
                                            $args['tax_query'][] = array(
                                                'taxonomy'  => $tax_key,
                                                'field'     => 'slug',
                                                'terms'     => explode( ',', $args[$tax_key] ),
                                                'operator'  => 'IN'
                                            );
                                        } else{
                                            $or_arr = array();
                                            foreach (explode( ',', $args[$tax_key] ) as $values ) {
                                                $and_arr = array();
                                                foreach (explode('+', $values) as $sub_val) {
                                                    $and_arr[] = array(
                                                        'taxonomy'  => $tax_key,
                                                        'field'     => 'slug',
                                                        'terms'     => $sub_val
                                                    );
                                                }
                                                $or_arr[] = array(
                                                    'relation' => 'AND',
                                                    $and_arr
                                                );
                                            }
                                            $args['tax_query'][] = array(
                                                'relation'  => 'OR',
                                                $or_arr
                                            );
                                        }
                                        
                                        unset( $args[$tax_key] );
                                        if ( !empty( $args['taxonomy']) && $args['taxonomy'] == $tax_key ) {
                                            unset( $args['taxonomy']);
                                            unset( $args['term']);
                                        }
                                    }
                                }
                            }

                            if ( $main_loop && !empty( $_GET['product_price'] ) ){
                                global $wpdb;
                                $sql = "SELECT MAX(a.max_price) AS max_price FROM {$wpdb->wc_product_meta_lookup} a JOIN {$wpdb->posts} b ON a.product_id=b.id;";
                                $min_price = 0;
                                $max_price = floatval($wpdb->get_var( $sql ));

                                $price_value = (explode(';', $_GET['product_price'] ) );
                                
                                if ( sizeof( $price_value ) == 1 ) {
                                    $min_filter_price = 0;
                                    $max_filter_price = floatval($price_value[0]);
                                }else{
                                    $max_filter_price = floatval($price_value[1]);
                                    $min_filter_price = floatval($price_value[0]);
                                }

                                if ( $min_filter_price != 0 || $max_filter_price != $max_price ) {
                                    $product_sql = "SELECT * FROM {$wpdb->wc_product_meta_lookup} a WHERE a.min_price >= $min_filter_price AND a.max_price <= $max_filter_price";

                                    $products = $wpdb->get_results($product_sql);

                                    $product_ids = array();

                                    foreach ( $products as $product ) {
                                        $product_ids[] = $product->product_id;
                                    }

                                    if ( !empty( $product_ids ) ){
                                        $args['post__in'] = array_merge( $args['post__in'], $product_ids );
                                    }else{
                                        $args['post__in'] = array( -1 );
                                    }
                                } else {
                                    $args['post__in'] = array();
                                }
                            }

                            if ( $main_loop && !empty( $_GET['product_weight'] ) ) {
                                foreach ($args['meta_query'] as $key => $meta) {
                                    if ( is_array( $meta ) && !empty( $meta['key'] ) && ( '_weight' == $meta['key'] ) ){
                                        unset( $args['meta_query'][$key] );
                                    }else if ( is_array( $meta ) ) {
                                        foreach ($meta as $subkey => $subMeta) {
                                            if ( is_array( $subMeta ) && !empty( $subMeta['key'] ) && ( '_weight' == $subMeta['key'] ) ){
                                                unset( $args['meta_query'][$key] );
                                            }
                                        }
                                    }
                                }
                                $range_value = (explode(";",$_GET['product_weight']));

                                if ( sizeof( $range_value ) == 1 ){
                                    $args['meta_query'][] = array(
                                        'key' => '_weight',
                                        'value' => $range_value[0],
                                        'type' => 'BINARY',
                                        'compare' => '<='
                                    );
                                }else{
                                    $args['meta_query'][] = array(
                                        'key' => '_weight',
                                        'value' => $range_value,
                                        'compare' => 'BETWEEN',
                                        'type' => 'BINARY'
                                    );
                                }
                            }

                            if ( $main_loop && isset( $_GET['product_rating'] ) ) {
                                foreach ($args['meta_query'] as $key => $meta) {
                                    if ( is_array( $meta ) && !empty( $meta['key'] ) && ( '_wc_average_rating' == $meta['key'] ) ){
                                        unset( $args['meta_query'][$key] );
                                    }else if ( is_array( $meta ) ) {
                                        foreach ($meta as $subkey => $subMeta) {
                                            if ( is_array( $subMeta ) && !empty( $subMeta['key'] ) && ( '_wc_average_rating' == $subMeta['key'] ) ){
                                                unset( $args['meta_query'][$key] );
                                            }
                                        }
                                    }
                                }

                                $product_rating_arr = explode(',', $_GET['product_rating']);
                                
                                if ( is_array( $product_rating_arr ) && count( $product_rating_arr ) > 1 ) {
                                    $rating_query = array( 'relation' => 'OR' );
                                    foreach ( $product_rating_arr as $key => $p_rating ) {
                                        if ( $p_rating == 0 ){
                                            $rating_query[] = array(
                                                'key' => '_wc_average_rating',
                                                'value' => $p_rating,
                                            );
                                        }else{
                                            $rating_query[] = array(
                                                'key' => '_wc_average_rating',
                                                'value' => array( $p_rating - 1, (int)$p_rating ),
                                                'type'  => 'BINARY',
                                                'compare' => 'BETWEEN',
                                            );
                                        }
                                    }
                                    $args['meta_query'][] = $rating_query;
                                } else {
                                    $range_value = (explode(";",$_GET['product_rating']));

                                    if ( sizeof( $range_value ) == 1 ){
                                        if ( $range_value[0] == 0 ) {
                                            $args['meta_query'][] = array(
                                                'key' => '_wc_average_rating',
                                                'value' => $range_value[0],
                                            );
                                        }else{
                                            $args['meta_query'][] = array(
                                                'key' => '_wc_average_rating',
                                                'value' => array( $range_value[0] - 1, (int)$range_value[0] ),
                                                'type' => 'BINARY',
                                                'compare' => 'BETWEEN'
                                            );
                                        }
                                    }else{
                                        $args['meta_query'][] = array(
                                            'key' => '_wc_average_rating',
                                            'value' => $range_value,
                                            'compare' => 'BETWEEN',
                                            'type' => 'BINARY'
                                        );
                                    }
                                }
                            }

                            if ( $main_loop && !empty($_GET['orderby'] ) ) {
                                $orderby_args = WC()->query->get_catalog_ordering_args( );
                                $args = array_merge( $args, $orderby_args );
                            }
                        }

                        $is_post_type_archive = $wp_query->is_post_type_archive;

                        $args = apply_filters('db_archive_module_args', $args);
                        query_posts( $args );

                        $wp_query->is_post_type_archive = $is_post_type_archive;

                        $wp_query->is_search = $is_search;


                        if ( $post_type_choose == 'product') {
                            $wp_query->set( 'wc_query', 'product_query' );    
                        }                        

                        if ($loop_layout == "none") {
                            echo "</div>";
                            echo "<h1>You have selected a custom layout but have not selected a loop layout for your products</h1>
                            Please create a <a href='https://www.youtube.com/watch?v=mLiUJ_hvBjE' target='_blank'>custom loop layout</a> and specify it in the settings, or change the layout style to be default.";
                        } else {


                            if ( $post_type_choose == 'product' ){
                                $have_post = false;

                                if ( have_posts() ){
                                    while ( have_posts() ){
                                        the_post();
                                        global $product;
                                        if (isset($product)) {
                                        if ( $product->is_purchasable() !== false ){
                                            $have_post = true;
                                        }
                                    }
                                    }
                                }    
                                add_filter( 'loop_shop_columns', array( $this, 'change_columns_number' ), 9999 );
                            }

                            if ( ($post_type_choose == 'product' && ((have_posts() && $hide_non_purchasable != "on") || ( $hide_non_purchasable == "on" && $have_post == true )) ) || (have_posts() && $post_type_choose != 'product' ) ) {

                                if ( $post_type_choose == 'product' ){
                                    do_action( 'woocommerce_before_shop_loop' );
                                }

                                $wp_query_var = $wp_query->query_vars;
                                if ( $main_loop && !empty( $_GET['product_price'] ) ){
                                    $wp_query_var['product_price'] = $_GET['product_price'];
                                }

                                ?>
                                <?php if ( $main_loop ) { ?>
                                <div class="filter-param-tags"></div>
                                <?php } ?>
                                <div class="filtered-posts-cont">
                                <div class="filtered-posts-loading <?php echo $filter_update_animation ?> "></div>
                                <div class="divi-filter-archive-loop has-result <?php echo $main_loop?'main-loop':'';?>"
                                    data-link_wholegrid="<?php echo $link_whole_gird ?>"
                                    data-layoutid="<?php echo $loop_layout ?>" data-columnscount="<?php echo $this->columns ?>" 
                                    data-filter-var="<?php echo htmlspecialchars( json_encode( $wp_query_var ) );?>"
                                    data-noresults="<?php echo $no_results_layout;?>" data-current-page="<?php echo $args['paged'];?>">
                                    <div class="divi-filter-loop-container col-desk-<?php echo $cols;?> col-tab-<?php echo $columns_tablet;?> col-mob-<?php echo $columns_mobile;?>">
                                <?php
                                

                                $product_id = null;
                                $class = '';
                                $shortcodes = '';

                                $i = 0;

                                                                   
                                    if ( $post_type_choose == 'product') {
                                        if ($fullwidth == 'off') { //grid
                                            echo '<ul class="et_pb_row_bodycommerce custom-loop-layout products bc_product_grid bc_product_' . $cols . ' bc_pro_tab_'. $columns_tablet .' bc_pro_mob_'. $columns_mobile .'">';
                                        } else if ($fullwidth  == 'list') {
                                            echo '<ul class="et_pb_row_bodycommerce custom-loop-layout products">';
                                        }    
                                    }else{
                                        echo '<ul class="et_pb_row_divifilter custom-loop-layout">';
                                    }                                
                                    
                                    $loop_prop = function_exists('wc_get_loop_prop')?wc_get_loop_prop( 'total' ):true;
                                    
                                    if ( $loop_prop ) {
                                    
                                    while ( have_posts() ) {
                                        the_post();
                                        global $product, $post;
                                        $post_link = get_permalink(get_the_ID());
                                        
                                        if ( $post_type_choose == 'product') {
                                                
                                                if (isset($product)) {
                                            if ( !($hide_non_purchasable == "on" 
                                            && ( !$product->is_type('grouped') && !$product->is_type('external') ) 
                                            && $product->is_purchasable() === false) ) {
                                                if( get_option('woocommerce_hide_out_of_stock_items') == "yes" && ( ! $product->managing_stock() && ! $product->is_in_stock() ) ){

                                                } else {
                                                    if ($fullwidth == 'off') {
                                                        echo '<li class="'. esc_attr( implode( " ", wc_get_product_class( $class, $product_id ) ) ) . ' ">';
                                                        if ($link_whole_gird == "on") {
                                                            ?>
                                                            <div class="bc-link-whole-grid-card" data-link-url="<?php echo $post_link ?>">
                                                            <?php   
                                                        }
                                                        
                                                        echo apply_filters('the_content', get_post_field('post_content', $loop_layout));
                                                        
                                                        if ($link_whole_gird == "on") {
                                                            ?>
                                                            </div>
                                                            <?php       
                                                        }
                                                        echo '</li>';
                                                    } else if ($fullwidth  == 'list') {
                                                        echo '<li class="'. esc_attr( implode( " ", wc_get_product_class( $class, $product_id ) ) ) . ' bc_product" style="width: 100%;margin-right: 0;">';
                                                        if ($link_whole_gird == "on") {
                                                            ?>
                                                            <div class="bc-link-whole-grid-card" data-link-url="<?php echo $post_link ?>">
                                                            <?php
                                                        }
                                                        
                                                        echo apply_filters('the_content', get_post_field('post_content', $loop_layout));
                                                        if ($link_whole_gird == "on") {
                                                            ?>
                                                            </div>  
                                                            <?php       
                                                        }
                                                        
                                                        echo '</li>';
                                                    }
                                                }
                                            }
                                                }
                                        } else {
                                            if ($fullwidth == 'off') {
                                                echo '<li>';
                                                if ($link_whole_gird == "on") {
                                                    ?>
                                                    <div class="bc-link-whole-grid-card" data-link-url="<?php echo $post_link ?>">
                                                    <?php   
                                                }
                                                
                                                echo apply_filters('the_content', get_post_field('post_content', $loop_layout));
                                                
                                                if ($link_whole_gird == "on") {
                                                    ?>
                                                    </div>  
                                                    <?php       
                                                }
                                                echo '</li>';
                                            } else if ($fullwidth  == 'list') {
                                                echo '<li style="width: 100%;margin-right: 0;">';
                                                if ($link_whole_gird == "on") {
                                                    ?>
                                                    <div class="bc-link-whole-grid-card" data-link-url="<?php echo $post_link ?>">
                                                    <?php
                                                    }
                                                    
                                                    echo apply_filters('the_content', get_post_field('post_content', $loop_layout));
                                                    
                                                    if ($link_whole_gird == "on") {
                                                        ?>
                                                        </div>  
                                                        <?php       
                                                    }
                                                    
                                                    echo '</li>';
                                            }
                                        }
                                    } // endwhile
                                    }
                                    
                                    echo '</ul>';
                                
                                echo '</div>';
                                echo '</div>';
                                echo '</div>';
                                
                                //woocommerce_product_loop_end();

                                wp_reset_query();

                                if ( 'on' === $enable_pagination ) {
                                    do_action( 'woocommerce_after_shop_loop' );
                                }

                                if ($equal_height_mob == "off") {
                        ?>
                            <style>@media only screen and (max-width:767px) {.woocommerce .et_pb_db_filter_loop.same-height-cards ul.products li.product {height: auto!important}}</style>
                        <?php
                                }
                            } else {
                                $wp_query_var = $wp_query->query_vars;
                                if ( $main_loop && !empty( $_GET['product_price'] ) ){
                                    $wp_query_var['product_price'] = $_GET['product_price'];
                                }
                        ?>
                            <?php if ( $main_loop ) { ?><div class="filter-param-tags"></div><?php } ?>
                            <div class="filtered-posts-cont">
                            <div class="filtered-posts-loading <?php echo $filter_update_animation ?> "></div>
                            <div class="divi-filter-archive-loop no-results-layout <?php echo $main_loop?'main-loop':'';?>" 
                            data-link_wholegrid="<?php echo $link_whole_gird ?>"
                                    data-layoutid="<?php echo $loop_layout ?>"  data-columnscount="<?php echo $this->columns ?>" 
                                    data-filter-var="<?php echo htmlspecialchars( json_encode( $wp_query_var ) );?>"
                                    data-noresults="<?php echo $no_results_layout;?>" data-current-page="<?php echo $args['paged'];?>">
                        <?php
                                if ($fullwidth == 'off') { //grid
                                    echo '<ul class="et_pb_row_bodycommerce custom-loop-layout products bc_product_grid bc_product_' . $cols . ' bc_pro_tab_'. $columns_tablet .' bc_pro_mob_'. $columns_mobile .'">';
                                }
                                else if ($fullwidth  == 'list') {
                                    echo '<ul class="et_pb_row_bodycommerce custom-loop-layout products">';
                                }

                                if ($no_results_layout == 'none') {
                                    if ( et_is_builder_plugin_active() ) {
                                        include( ET_BUILDER_PLUGIN_DIR . 'includes/no-results.php' );
                                    } else {
                                        get_template_part( 'includes/no-results', 'index' );
                                    }
                                } else {
                                    echo apply_filters('the_content', get_post_field('post_content', $no_results_layout));
                                }
                        ?>
                                </ul>
                            </div>
                            </div>
                        <?php
                            }
                        }

                        $posts = ob_get_contents();

                        ob_end_clean();

                        $class = " et_pb_module et_pb_bg_layout_{$background_layout}";

                        $output = sprintf(
                            '<div%4$s class="%1$s%3$s%5$s"%6$s>
                            %2$s</div>',
                            ( $fullwidth == 'list' ? 'et_pb_posts' : 'et_pb_blog_grid clearfix' ),
                            $posts,
                            esc_attr( $class ),
                            ( '' !== $module_id ? sprintf( ' id="%1$s"', esc_attr( $module_id ) ) : '' ),
                            ( '' !== $module_class ? sprintf( ' %1$s', esc_attr( $module_class ) ) : '' ),
                            ( 'on' !== $fullwidth ? ' data-columns' : '' )
                        );

                        if ( 'off' == $fullwidth ) {
                            $output = sprintf( '<div class="et_pb_blog_grid_wrapper">%1$s</div>', $output );
                        } else if ( 'list' == $fullwidth ) {
                            $output = sprintf( '<div class="et_pb_woo_list_wrapper">%1$s</div>', $output );
                        }

                        return $output;

                    } // END CUSTOM LAYOUT
                    // DEFAULT
                    else if ($cat_loop_style == "on") {

                        if ( $post_type_choose == 'product' ) {
                            
                            $product_taxonomies = get_object_taxonomies( 'product' );

                            if ( $main_loop ){
                                foreach ($product_taxonomies as $tax_name ) {
                                    if ( !empty( $_GET[$tax_name] ) ) {
                                        if ( !empty( $args['tax_query'] ) ){
                                            $args['tax_query'][] = array(
                                                'taxonomy'  => $tax_name,
                                                'field'     => 'slug',
                                                'terms'     => explode( ',' , $_GET[$tax_name] ),
                                                'operator' => 'IN'
                                            );
                                        }
                                    }
                                }
                            }

                            foreach ( array( "product_cat", "product_tag" ) as $tax_key ) {
                                if ( $main_loop && !empty( $_GET[$tax_key] ) ) {
                                    unset( $args[$tax_key] );
                                    if ( !empty( $args['taxonomy']) && $args['taxonomy'] == $tax_key ) {
                                        unset( $args['taxonomy']);
                                        unset( $args['term']);
                                    }
                                }else{
                                    if ( !empty( $args[$tax_key] ) ) {
                                        $args['tax_query'][] = array(
                                            'taxonomy'  => $tax_key,
                                            'field'     => 'slug',
                                            'terms'     => explode( ',', $args[$tax_key] ),
                                            'operator'  => 'IN'
                                        );
                                        unset( $args[$tax_key] );
                                        if ( !empty( $args['taxonomy']) && $args['taxonomy'] == $tax_key ) {
                                            unset( $args['taxonomy']);
                                            unset( $args['term']);
                                        }
                                    }
                                }
                            }

                            if ( $main_loop && !empty( $_GET['product_price'] ) ){
                                global $wpdb;
                                $sql = "SELECT MAX(a.max_price) AS max_price FROM {$wpdb->wc_product_meta_lookup} a JOIN {$wpdb->posts} b ON a.product_id=b.id;";
                                $min_price = 0;
                                $max_price = floatval($wpdb->get_var( $sql ));

                                $price_value = (explode(';', $_GET['product_price'] ) );
                                
                                if ( sizeof( $price_value ) == 1 ) {
                                    $min_filter_price = 0;
                                    $max_filter_price = floatval($price_value[0]);
                                }else{
                                    $max_filter_price = floatval($price_value[1]);
                                    $min_filter_price = floatval($price_value[0]);
                                }

                                if ( $min_filter_price != 0 || $max_filter_price != $max_price ) {
                                    $product_sql = "SELECT * FROM {$wpdb->wc_product_meta_lookup} a WHERE a.min_price >= $min_filter_price AND a.max_price <= $max_filter_price";

                                    $products = $wpdb->get_results($product_sql);

                                    $product_ids = array();

                                    foreach ( $products as $product ) {
                                        $product_ids[] = $product->product_id;
                                    }

                                    if ( !empty( $product_ids ) ){
                                        $args['post__in'] = array_merge( $args['post__in'], $product_ids );
                                    }else{
                                        $args['post__in'] = array( -1 );
                                    }
                                } else {
                                    $args['post__in'] = array();
                                }
                            }

                            if ( $main_loop && !empty( $_GET['product_weight'] ) ) {
                                foreach ($args['meta_query'] as $key => $meta) {
                                    if ( is_array( $meta ) && !empty( $meta['key'] ) && ( '_weight' == $meta['key'] ) ){
                                        unset( $args['meta_query'][$key] );
                                    }else if ( is_array( $meta ) ) {
                                        foreach ($meta as $subkey => $subMeta) {
                                            if ( is_array( $subMeta ) && !empty( $subMeta['key'] ) && ( '_weight' == $subMeta['key'] ) ){
                                                unset( $args['meta_query'][$key] );
                                            }
                                        }
                                    }
                                }
                                $range_value = (explode(";",$_GET['product_weight']));

                                if ( sizeof( $range_value ) == 1 ){
                                    $args['meta_query'][] = array(
                                        'key' => '_weight',
                                        'value' => $range_value[0],
                                        'type' => 'BINARY',
                                        'compare' => '<='
                                    );
                                }else{
                                    $args['meta_query'][] = array(
                                        'key' => '_weight',
                                        'value' => $range_value,
                                        'compare' => 'BETWEEN',
                                        'type' => 'BINARY'
                                    );
                                }
                            }

                            if ( $main_loop && isset( $_GET['product_rating'] ) ) {
                                foreach ($args['meta_query'] as $key => $meta) {
                                    if ( is_array( $meta ) && !empty( $meta['key'] ) && ( '_wc_average_rating' == $meta['key'] ) ){
                                        unset( $args['meta_query'][$key] );
                                    }else if ( is_array( $meta ) ) {
                                        foreach ($meta as $subkey => $subMeta) {
                                            if ( is_array( $subMeta ) && !empty( $subMeta['key'] ) && ( '_wc_average_rating' == $subMeta['key'] ) ){
                                                unset( $args['meta_query'][$key] );
                                            }
                                        }
                                    }
                                }

                                $product_rating_arr = explode(',', $_GET['product_rating']);
                                
                                if ( is_array( $product_rating_arr ) && count( $product_rating_arr ) > 1 ) {
                                    $rating_query = array( 'relation' => 'OR' );
                                    foreach ( $product_rating_arr as $key => $p_rating ) {
                                        if ( $p_rating == 0 ){
                                            $rating_query[] = array(
                                                'key' => '_wc_average_rating',
                                                'value' => $p_rating,
                                            );
                                        }else{
                                            $rating_query[] = array(
                                                'key' => '_wc_average_rating',
                                                'value' => array( $p_rating - 1, (int)$p_rating ),
                                                'type'  => 'BINARY',
                                                'compare' => 'BETWEEN',
                                            );
                                        }
                                    }
                                    $args['meta_query'][] = $rating_query;
                                } else {
                                    $range_value = (explode(";",$_GET['product_rating']));

                                    if ( sizeof( $range_value ) == 1 ){
                                        if ( $range_value[0] == 0 ) {
                                            $args['meta_query'][] = array(
                                                'key' => '_wc_average_rating',
                                                'value' => $range_value[0],
                                            );
                                        }else{
                                            $args['meta_query'][] = array(
                                                'key' => '_wc_average_rating',
                                                'value' => array( $range_value[0] - 1, (int)$range_value[0] ),
                                                'type' => 'BINARY',
                                                'compare' => 'BETWEEN'
                                            );
                                        }
                                    }else{
                                        $args['meta_query'][] = array(
                                            'key' => '_wc_average_rating',
                                            'value' => $range_value,
                                            'compare' => 'BETWEEN',
                                            'type' => 'BINARY'
                                        );
                                    }
                                }
                            }

                            if ( $main_loop && !empty($_GET['orderby'] ) ) {
                                $orderby_args = WC()->query->get_catalog_ordering_args( );
                                $args = array_merge( $args, $orderby_args );
                            }
                        }

                        if ( $main_loop && !empty( $_GET['page'] ) ) {
                            $args['paged'] = $_GET['page'];
                        }

                        if ( class_exists( 'woocommerce' ) && $post_type_choose == 'product' ) {
                            $product_visibility_terms  = wc_get_product_visibility_term_ids();
                            $product_visibility_not_in = array( is_search() ? $product_visibility_terms['exclude-from-search'] : $product_visibility_terms['exclude-from-catalog'] );
                            if ( 'yes' === get_option( 'woocommerce_hide_out_of_stock_items' ) ) {
                                $product_visibility_not_in[] = $product_visibility_terms['outofstock'];
                            }
                            
                            if ( ! empty( $product_visibility_not_in ) ) {
                                $args['tax_query'][] = array(
                                    'taxonomy' => 'product_visibility',
                                    'field'    => 'term_taxonomy_id',
                                    'terms'    => $product_visibility_not_in,
                                    'operator' => 'NOT IN',
                                );
                            }
                        }

                        $is_post_type_archive = $wp_query->is_post_type_archive;

                        $args = apply_filters('db_archive_module_args', $args);
                        query_posts( $args );

                        $wp_query->is_post_type_archive = $is_post_type_archive;

                        $wp_query->is_search = $is_search;

                        if( $show_sorting_menu == 'off' ){
                            remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
                        }else{
                            add_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
                        }
                        if( $show_results_count == 'off' ){
                            remove_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
                        }else{
                            add_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
                        }
                        if( $show_rating == 'off' ){
                            remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );
                        }else{
                            add_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );
                        }
                        if( $show_price == 'off' ){
                            remove_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );
                        }else{
                            add_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );
                        }
                        if( $show_excerpt == 'on' ){
                            add_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_single_excerpt', 8 );
                        }
                        if( $show_add_to_cart == 'on' ){
                            add_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 9 );
                        }
                        if( $enable_pagination == 'off' ){
                            remove_action( 'woocommerce_after_shop_loop', 'woocommerce_pagination', 10 );
                        }else{
                            add_action( 'woocommerce_after_shop_loop', 'woocommerce_pagination', 10 );
                        }

                        add_action( 'woocommerce_shop_loop_item_title', array( 'db_filter_loop_code', 'product_details_wrapper_start' ), 0 );
                        add_action( 'woocommerce_after_shop_loop_item', array( 'db_filter_loop_code', 'product_details_wrapper_end' ), 10 );

                        add_action( 'woocommerce_before_shop_loop_item_title', array( 'db_filter_loop_code', 'product_image_wrapper_start' ), 0 );
                        add_action( 'woocommerce_before_shop_loop_item_title', array( 'db_filter_loop_code', 'product_image_wrapper_end' ), 20 );

                        // list view
                        if( $display_type == 'list_view' ){
                            $this->add_classname( 'de_db_list_view' );
                            $this->columns = 1;
                        }

                        // columns
                        add_filter( 'loop_shop_columns', array( $this, 'change_columns_number' ), 9999 );

                        // show add to cart
                        if( $show_add_to_cart == 'on' ){
                            // add to cart button icon and background
                            if( $custom_add_to_cart_button == 'on' ){
                                // button icon
                                if( $add_to_cart_button_icon !== '' ){
                                    
                                    if (defined('DE_DB_WOO_VERSION')) {
                                        $addToCartIconContent = DEBC_INIT::et_icon_css_content( esc_attr($hover_icon) );
                                        } else {
                                          $addToCartIconContent = DE_Filter::et_icon_css_content( esc_attr($hover_icon) );
                                        }
                                    $addToCartIconSelector = '';
                                    if( $add_to_cart_button_icon_placement == 'right' ){
                                        $addToCartIconSelector = '%%order_class%% li.product .button:after';
                                    }elseif( $add_to_cart_button_icon_placement == 'left' ){
                                        $addToCartIconSelector = '%%order_class%% li.product .button:before';
                                    }

                                    if( !empty( $addToCartIconContent ) && !empty( $addToCartIconSelector ) ){
                                        ET_Builder_Element::set_style( $render_slug, array(
                                            'selector' => $addToCartIconSelector,
                                            'declaration' => "content: '{$addToCartIconContent}'!important;font-family:ETmodules!important;"
                                        ) );
                                    }
                                }

                                // button background
                                if( !empty( $add_to_cart_button_bg_color ) ){
                                    ET_Builder_Element::set_style( $render_slug, array(
                                        'selector'    => 'body #page-container %%order_class%% .button',
                                        'declaration' => "background-color:". esc_attr( $add_to_cart_button_bg_color ) ."!important;",
                                    ) );
                                }
                            }
                        }

                        if( $use_overlay == 'off' ){
                            $this->add_classname( 'hide_overlay' );
                        }elseif( $use_overlay == 'on' ){
                            // icon

                            if (defined('DE_DB_WOO_VERSION')) {
                                if( !empty( $hover_icon ) ){
                                    $icon_color = !( empty( $icon_hover_color ) ) ? 'color: ' . esc_attr( $icon_hover_color ) . ';' : '';
    
                                    ET_Builder_Element::set_style( $render_slug, array(
                                        'selector'    => '%%order_class%% .et_overlay:before, %%order_class%% .et_pb_extra_overlay:before',
                                        'declaration' => "content: '". esc_attr( DEBC_INIT::et_icon_css_content( $hover_icon ) ) ."'; font-family: 'ETmodules' !important; {$icon_color}",
                                    ) );
                                }
    
                                } else {
                                    if( !empty( $hover_icon ) ){
                                        $icon_color = !( empty( $icon_hover_color ) ) ? 'color: ' . esc_attr( $icon_hover_color ) . ';' : '';
        
                                        ET_Builder_Element::set_style( $render_slug, array(
                                            'selector'    => '%%order_class%% .et_overlay:before, %%order_class%% .et_pb_extra_overlay:before',
                                            'declaration' => "content: '". esc_attr( DE_Filter::et_icon_css_content( $hover_icon ) ) ."'; font-family: 'ETmodules' !important; {$icon_color}",
                                        ) );
                                    }
        
                                }



                            // hover background color
                            if( !empty( $hover_overlay_color ) ){

                                ET_Builder_Element::set_style( $render_slug, array(
                                  'selector'    => '%%order_class%% .et_overlay, %%order_class%% .et_pb_extra_overlay',
                                  'declaration' => "background: ". esc_attr( $hover_overlay_color ) .";",
                                ) );
                            }
                        }

                        // stars color
                        if( !empty( $stars_color ) ){
                            ET_Builder_Element::set_style( $render_slug, array(
                                'selector'    => 'body.woocommerce %%order_class%% .star-rating span:before, body.woocommerce-page %%order_class%% .star-rating span:before, %%order_class%% .star-rating span:before',
                                'declaration' => "color: ". esc_attr( $stars_color ) ."!important;",
                            ) );
                        }

                        if ( '' !== $sale_badge_color ) {
                            ET_Builder_Element::set_style( $render_slug, array(
                                'selector'    => '%%order_class%% span.onsale',
                                'declaration' => sprintf(
                                    'background-color: %1$s !important;',
                                    esc_html( $sale_badge_color )
                                ),
                            ) );
                        }

                        if ( '' !== $product_background ) {
                            ET_Builder_Element::set_style( $render_slug, array(
                                'selector'    => "%%order_class%% .products .product",
                                'declaration' => sprintf(
                                    'background-color: %1$s !important;',
                                    esc_html( $product_background )
                                ),
                            ) );
                        }

                        if ( '' !== $product_padding ) {
                            ET_Builder_Element::set_style( $render_slug, array(
                                'selector'    => "%%order_class%% .products .product",
                                'declaration' => sprintf(
                                    'padding: %1$s !important;',
                                    esc_html( $product_padding )
                                ),
                            ) );
                        }

                        if ( $post_type_choose == 'product'){
                            /**
                             * Products Loop Start
                             * If the module is used inside a product archive page, load the default loop to maintain compatibility with 3rd party plugins
                             * But if the module is used in any other page, use the [products] shortcode
                             * */
                            if( ( function_exists( 'is_product_taxonomy' ) && is_product_taxonomy() ) || ( function_exists( 'is_shop' ) && is_shop() ) ){
                                ob_start();
                                /**
                                 * This loop is from archive-product.php
                                 * @version 3.4.0 => WC
                                 */
                                if ( have_posts() ) {

                                    /**
                                     * Hook: woocommerce_before_shop_loop.
                                     *
                                     * @hooked wc_print_notices - 10
                                     * @hooked woocommerce_result_count - 20
                                     * @hooked woocommerce_catalog_ordering - 30
                                     */
                                    do_action( 'woocommerce_before_shop_loop' );
                                    $wp_query_var = $wp_query->query_vars;
                                    if ( $main_loop && !empty( $_GET['product_price'] ) ){
                                        $wp_query_var['product_price'] = $_GET['product_price'];
                                    }
                                    ?>
                                    <?php if ( $main_loop ) { ?><div class="filter-param-tags"></div><?php } ?>
                                    <div class="filtered-posts-cont">
                                    <div class="filtered-posts-loading <?php echo $filter_update_animation ?> "></div>
                                    <div class="divi-filter-archive-loop has-result <?php echo $main_loop?'main-loop':'';?>" 
                                    data-link_wholegrid="<?php echo $link_whole_gird ?>"
                                        data-layoutid="<?php echo $loop_layout ?>" data-columnscount="<?php echo $this->columns ?>" 
                                        data-filter-var="<?php echo htmlspecialchars( json_encode( $wp_query_var ) );?>"
                                        data-noresults="<?php echo $no_results_layout;?>"
                                        data-show_rating="<?php echo $show_rating;?>"
                                        data-show_price="<?php echo $show_price;?>"
                                        data-show_excerpt="<?php echo $show_excerpt;?>"
                                        data-show_add_to_cart="<?php echo $show_add_to_cart;?>" data-current-page="<?php echo $args['paged'];?>">
                                     <?php
                                    woocommerce_product_loop_start();

                                    if ( function_exists( 'wc_get_loop_prop' ) && wc_get_loop_prop( 'total' ) ) {
                                        while ( have_posts() ) {
                                            the_post();

                                            /**
                                             * Hook: woocommerce_shop_loop.
                                             *
                                             * @hooked WC_Structured_Data::generate_product_data() - 10
                                             */
                                            do_action( 'woocommerce_shop_loop' );

                                            wc_get_template_part( 'content', 'product' );
                                        }
                                    }

                                    woocommerce_product_loop_end();

                                    echo '</div>';

                                    echo '</div>';

                                    /**
                                     * Hook: woocommerce_after_shop_loop.
                                     *
                                     * @hooked woocommerce_pagination - 10
                                     */
                                    do_action( 'woocommerce_after_shop_loop' );
                                } else {
                                    /**
                                     * Hook: woocommerce_no_products_found.
                                     *
                                     * @hooked wc_no_products_found - 10
                                     */
                                    $wp_query_var = $wp_query->query_vars;
                                    if ( $main_loop && !empty( $_GET['product_price'] ) ){
                                        $wp_query_var['product_price'] = $_GET['product_price'];
                                    }
                                ?>
                                    <?php if ( $main_loop ) { ?><div class="filter-param-tags"></div><?php } ?>
                                    <div class="filtered-posts-cont">
                                    <div class="filtered-posts-loading <?php echo $filter_update_animation ?> "></div>
                                    <div class="divi-filter-archive-loop has-result <?php echo $main_loop?'main-loop':'';?>" 
                                    data-link_wholegrid="<?php echo $link_whole_gird ?>"
                                        data-layoutid="<?php echo $loop_layout ?>" data-columnscount="<?php echo $this->columns ?>" 
                                        data-filter-var="<?php echo htmlspecialchars( json_encode( $wp_query_var ) );?>"
                                        data-noresults="<?php echo $no_results_layout;?>"
                                        data-show_rating="<?php echo $show_rating;?>"
                                        data-show_price="<?php echo $show_price;?>"
                                        data-show_excerpt="<?php echo $show_excerpt;?>"
                                        data-show_add_to_cart="<?php echo $show_add_to_cart;?>" data-current-page="<?php echo $args['paged'];?>">
                                <?php
                                    do_action( 'woocommerce_no_products_found' );
                                    echo '</div>';
                                    echo '</div>';
                                }
                                $loop = ob_get_clean();
                                $loop = "<div class='woocommerce default-style columns-". (int)$this->columns ."'>" . $loop . "</div>";
                            }else{
                                
                  
                                
                                $wp_query_var = $wp_query->query_vars;
                                if ( $main_loop && !empty( $_GET['product_price'] ) ){
                                    $wp_query_var['product_price'] = $_GET['product_price'];
                                }

                                if (is_search()) {
                                    
                                    $wp_query_var['s'] = get_search_query();

                                    ob_start();
                                    ?>
                                    <?php if ( $main_loop ) { ?><div class="filter-param-tags"></div><?php } ?>
                                    <div class="filtered-posts-cont">
                                    <div class="filtered-posts-loading <?php echo $filter_update_animation ?> "></div>
                                    <div class="divi-filter-archive-loop has-result <?php echo $main_loop?'main-loop':'';?>" 
                                    data-link_wholegrid="<?php echo $link_whole_gird ?>"
                                        data-layoutid="<?php echo $loop_layout ?>" data-columnscount="<?php echo $this->columns ?>" 
                                        data-filter-var="<?php echo htmlspecialchars( json_encode( $wp_query_var ) );?>"
                                        data-noresults="<?php echo $no_results_layout;?>"
                                        data-show_rating="<?php echo $show_rating;?>"
                                        data-show_price="<?php echo $show_price;?>"
                                        data-show_excerpt="<?php echo $show_excerpt;?>"
                                        data-show_add_to_cart="<?php echo $show_add_to_cart;?>" data-current-page="<?php echo $args['paged'];?>">
                                    <?php                                    
                                    
                                    ?>
                                    <div class="reporting_args" style="white-space: pre;">
                                          <?php  print_r(get_search_query()); ?>
                                    </div>
                                      <?php

                                    echo '</div>';
                                    echo '</div>';
                                    $loop = ob_get_clean();
                                } else {
                                ob_start();
                                ?>
                                <?php if ( $main_loop ) { ?><div class="filter-param-tags"></div><?php } ?>
                                <div class="filtered-posts-cont">
                                <div class="filtered-posts-loading <?php echo $filter_update_animation ?> "></div>
                                <div class="divi-filter-archive-loop has-result <?php echo $main_loop?'main-loop':'';?>" 
                                data-link_wholegrid="<?php echo $link_whole_gird ?>"
                                    data-layoutid="<?php echo $loop_layout ?>" data-columnscount="<?php echo $this->columns ?>" 
                                    data-filter-var="<?php echo htmlspecialchars( json_encode( $wp_query_var ) );?>"
                                    data-noresults="<?php echo $no_results_layout;?>"
                                    data-show_rating="<?php echo $show_rating;?>"
                                    data-show_price="<?php echo $show_price;?>"
                                    data-show_excerpt="<?php echo $show_excerpt;?>"
                                    data-show_add_to_cart="<?php echo $show_add_to_cart;?>" data-current-page="<?php echo $args['paged'];?>">
                                <?php                                    
                                $columns = esc_attr($this->columns);
                                global $shortname; // theme name
                                $limit = function_exists( 'et_get_option' ) ? (int) et_get_option( $shortname . '_woocommerce_archive_num_posts', '9' ) : 9;
                                $pagination = $enable_pagination == 'on' ? 'true' : 'false';

                                $shortcode = "[products columns='{$columns}' limit='{$limit}' paginate='{$pagination}']";
                                echo do_shortcode( $shortcode );
                                echo '</div>';
                                echo '</div>';
                                $loop = ob_get_clean();
                                    
                            }
                            }

                            /* Products Loop Start */

                            /* reset in case the module used twice start */
                            if( $show_sorting_menu == 'off' ){
                                add_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30 );
                            }
                            if( $show_results_count == 'off' ){
                                add_action( 'woocommerce_before_shop_loop', 'woocommerce_result_count', 20 );
                            }
                            if( $show_rating == 'off' ){
                                add_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5 );
                            }
                            if( $show_price == 'off' ){
                                add_action( 'woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10 );
                            }
                            if( $show_excerpt == 'on' ){
                                remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_single_excerpt', 8 );
                            }
                            if( $show_add_to_cart == 'on' ){
                                remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 9 );
                            }
                            if( $enable_pagination == 'off' ){
                                add_action( 'woocommerce_after_shop_loop', 'woocommerce_pagination', 10 );
                            }

                            remove_filter( 'loop_shop_columns', array( $this, 'change_columns_number' ), 9999 );
                            remove_action( 'woocommerce_shop_loop_item_title', array( 'db_filter_loop_code', 'product_details_wrapper_start' ), 0 );
                            remove_action( 'woocommerce_after_shop_loop_item', array( 'db_filter_loop_code', 'product_details_wrapper_end' ), 10 );
                            remove_action( 'woocommerce_before_shop_loop_item_title', array( 'db_filter_loop_code', 'product_image_wrapper_start' ), 0 );
                            remove_action( 'woocommerce_before_shop_loop_item_title', array( 'db_filter_loop_code', 'product_image_wrapper_end' ), 20 );
                            
                        }else{
                            ob_start();
                            /**
                             * This loop is from archive-product.php
                             * @version 3.4.0 => WC
                             */
                            if ( have_posts() ) {
                                $wp_query_var = $wp_query->query_vars;
                                if ( $main_loop && !empty( $_GET['product_price'] ) ){
                                    $wp_query_var['product_price'] = $_GET['product_price'];
                                }
                                ob_start();
?>
                                <?php if ( $main_loop ) { ?><div class="filter-param-tags"></div><?php } ?>
                                <div class="filtered-posts-cont">
                                <div class="filtered-posts-loading <?php echo $filter_update_animation ?> "></div>
                                <div class="divi-filter-archive-loop has-result <?php echo $main_loop?'main-loop':'';?>"
                                data-link_wholegrid="<?php echo $link_whole_gird ?>" 
                                    data-layoutid="<?php echo $loop_layout ?>" data-columnscount="<?php echo $this->columns ?>" 
                                    data-filter-var="<?php echo htmlspecialchars( json_encode( $wp_query_var ) );?>"
                                    data-noresults="<?php echo $no_results_layout;?>"
                                    data-show_rating="<?php echo $show_rating;?>"
                                    data-show_price="<?php echo $show_price;?>"
                                    data-show_excerpt="<?php echo $show_excerpt;?>"
                                    data-show_add_to_cart="<?php echo $show_add_to_cart;?>" data-current-page="<?php echo $args['paged'];?>">
<?php
                                while ( have_posts() ) {
                                    the_post();
                                    get_template_part( 'single', $post_type_choose );
                                }
                                echo '</div>';
                                echo '</div>';
                            }else{
                                $wp_query_var = $wp_query->query_vars;
                                if ( $main_loop && !empty( $_GET['product_price'] ) ){
                                    $wp_query_var['product_price'] = $_GET['product_price'];
                                }
                                ob_start();
?>
                                <?php if ( $main_loop ) { ?><div class="filter-param-tags"></div><?php } ?>
                                <div class="filtered-posts-cont">
                                <div class="filtered-posts-loading <?php echo $filter_update_animation ?> "></div>
                                <div class="divi-filter-archive-loop no-results-layout <?php echo $main_loop?'main-loop':'';?>" 
                                data-link_wholegrid="<?php echo $link_whole_gird ?>"
                                    data-layoutid="<?php echo $loop_layout ?>" data-columnscount="<?php echo $this->columns ?>" 
                                    data-filter-var="<?php echo htmlspecialchars( json_encode( $wp_query_var ) );?>"
                                    data-noresults="<?php echo $no_results_layout;?>"
                                    data-show_rating="<?php echo $show_rating;?>"
                                    data-show_price="<?php echo $show_price;?>"
                                    data-show_excerpt="<?php echo $show_excerpt;?>"
                                    data-show_add_to_cart="<?php echo $show_add_to_cart;?>" data-current-page="<?php echo $args['paged'];?>">
                                    <p>No Result</p>
                                </div>
                                </div>
<?php
                            }
                            $loop = ob_get_clean();
                        }
                        
                        $output = $loop;

                        wp_reset_query();
                        
                        return $output;
                    }
                }
            }

            new db_filter_loop_code;
        }
    }

    if ( !function_exists('Divi_filter_restore_get_params') ) {
      add_action( 'template_redirect', 'Divi_filter_restore_get_params');

      function Divi_filter_restore_get_params() {
          global $divi_filter_removed_param;
          if ( !empty( $_GET['filter'] ) && $_GET['filter'] == 'true' ){
        if ( !empty($divi_filter_removed_param) ) {
          foreach ($divi_filter_removed_param as $key => $value ) {
            $_GET[$key] = $value;
          }
        }
          }
      }
    }

    add_action( 'wp_enqueue_scripts', 'divi_archive_loop_enqueue_scripts' );

    function divi_archive_loop_enqueue_scripts() {
        $ajax_pagination = true;
        if ( defined('DE_DB_WOO_VERSION' ) ) {
            $mydata = get_option( 'divi-bodyshop-woo_options' );
            $mydata = unserialize($mydata);

            if (isset($mydata['disable_ajax_pagination']) && $mydata['disable_ajax_pagination'] == '1' ){
                $ajax_pagination = false;
            }
            wp_enqueue_script( 'divi-filter-js', plugins_url( '../../../js/divi-filter.min.js', __FILE__ ), array( 'jquery' ), DE_DB_WOO_VERSION );
        }else{
            wp_enqueue_script( 'divi-filter-js', plugins_url( '../../../js/divi-filter.min.js', __FILE__ ), array( 'jquery' ), DE_DF_VERSION );
        }
        wp_localize_script( 'divi-filter-js', 'filter_ajax_object',
            array(
                'ajax_url' => admin_url( 'admin-ajax.php' ),
                'ajax_pagination' => $ajax_pagination
            )
        );
    }
}

?>