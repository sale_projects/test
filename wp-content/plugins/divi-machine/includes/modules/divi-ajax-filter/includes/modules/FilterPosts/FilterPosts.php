<?php
if ( !function_exists("Divi_filter_module_import") ){
    add_action( 'et_builder_ready', 'Divi_filter_module_import');

    function Divi_filter_module_import(){
        if(class_exists("ET_Builder_Module") && !class_exists("de_df_filter_product_code")){
            class de_df_filter_product_code extends ET_Builder_Module {

                public $vb_support = 'on';

                protected $module_credits = array(
                    'module_uri' => DE_DF_PRODUCT_URL,
                    'author'     => DE_DF_AUTHOR,
                    'author_uri' => DE_DF_URL,
                );

                function init() {
                    if (defined('DE_DMACH_VERSION')) {
                        $this->name       = esc_html__( '.Filter Posts - Divi Machine', 'divi-machine' );
                    } else {
                        $this->name       = esc_html__( '.Filter Posts - Divi Ajax Filter', 'divi-filter' );
                    }
                    
                    $this->slug = 'et_pb_de_mach_filter_posts';
                    $this->vb_support      = 'on';
                    $this->child_slug      = 'et_pb_de_mach_search_posts_item';
                    $this->child_item_text = esc_html__( 'Filter Item', 'divi-filter' );

             


                    $this->settings_modal_toggles = array(
                        'general' => array(
                            'toggles' => array(
                                'main_content' => esc_html__( 'Main Options', 'divi-filter' ),
                                    'layout' => esc_html__( 'Layout Options', 'divi-filter' ),
                                    'mobile_layout' => esc_html__( 'Mobile Options', 'divi-filter' ),
                                    'filter_item' => esc_html__( 'Filter Item', 'divi-filter' ),
                                    'toggle_appearance' => esc_html__( 'Toggle Appearance', 'divi-filter' ),
                            ),
                        ),
                        'advanced' => array(
                            'toggles' => array(
                                'text' => esc_html__( 'Text', 'divi-filter' ),
                            ),
                        ),
                    );

                    $this->main_css_element = '%%order_class%%';
                    $this->advanced_fields = array(
                        'fonts' => array(
                            'title' => array(
                                'label'    => esc_html__( 'Item', 'divi-filter' ),
                                'css'      => array(
                                    'main' =>  "%%order_class%%, 
                                                %%order_class%% input[type=text], 
                                                %%order_class%% .divi-filter-item, 
                                                %%order_class%% input[type=text]::-webkit-input-placeholder, %%order_class%% input[type=text]:-moz-placeholder, 
                                                %%order_class%% input[type=text]::-moz-placeholder, 
                                                %%order_class%% input[type=text]:-ms-input-placeholder,
                                                %%order_class%% .divi-radio-buttons label",
                                    'important' => 'plugin_only',
                                ),
                                'font_size' => array(
                                    'default' => '14px',
                                ),
                                'line_height' => array(
                                    'default' => '1em',
                                ),
                            ),
                            'filter_headings' => array(
                                'label'    => esc_html__( 'Filter Heading', 'divi-filter' ),
                                'css'      => array(
                                    'main' => "%%order_class%% .et_pb_contact_field_options_title",
                                    'important' => 'plugin_only',
                                ),
                                'font_size' => array(
                                    'default' => '14px',
                                ),
                                'line_height' => array(
                                    'default' => '1em',
                                ),
                            ),
                        ),
                        'background' => array(
                            'settings' => array(
                                'color' => 'alpha',
                            ),
                        ),
                        'button' => array(
                            'button' => array(
                                'label' => esc_html__( 'Button', 'divi-bodyshop-woocommerce' ),
                                'css' => array(
                                    'main' => "{$this->main_css_element} .et_pb_button",
                                    'important' => 'all',
                                ),
                                'box_shadow'  => array(
                                    'css' => array(
                                        'main' => "{$this->main_css_element} .et_pb_button",
                                        'important' => 'all',
                                    ),
                                ),
                                'margin_padding' => array(
                                    'css' => array(
                                        'main' => "{$this->main_css_element} .et_pb_button",
                                        'important' => 'all',
                                    ),
                                ),
                                'use_alignment' => true,
                            ),
            'filter_params' => array(
              'label' => esc_html__( 'Filter Param', 'divi-filter' ),
              'css' => array(
                'main' => ".filter-param-item",
                'important' => 'all',
              ),
              'box_shadow'  => array(
                'css' => array(
                  'main' => ".filter-param-item",
                      'important' => 'all',
                ),
              ),
              'margin_padding' => array(
              'css'           => array(
                'main' => ".filter-param-item",
                'important' => 'all',
              ),
              ),
                'use_alignment' => true,
            ),
            'mobile_toggle_button' => array(
              'label' => esc_html__( 'Mobile Toggle Button', 'divi-filter' ),
              'css' => array(
                'main' => ".mobile_toggle_trigger.et_pb_button",
                'important' => 'all',
              ),
              'box_shadow'  => array(
                'css' => array(
                  'main' => ".mobile_toggle_trigger.et_pb_button",
                      'important' => 'all',
                ),
              ),
              'margin_padding' => array(
              'css'           => array(
                'main' => ".mobile_toggle_trigger.et_pb_button",
                'important' => 'all',
              ),
              ),
            ),
                        ),
                        'form_field'           => array(
                            'form_field' => array(
                                'label'         => esc_html__( 'Fields', 'et_builder' ),
                                'css'           => array(
                                    'main' => '%%order_class%% .et_pb_contact_select, %%order_class%% .et_pb_contact_field[type="checkbox"] + label i, %%order_class%% .et_pb_contact_field[type="radio"] + label i',
                                    'background_color' => '%%order_class%% .et_pb_contact_select, %%order_class%% .et_pb_contact_field[type="checkbox"] + label i, %%order_class%% .et_pb_contact_field[type="radio"] + label i',
                                    'background_color_hover' => '%%order_class%% .et_pb_contact_select:hover, %%order_class%% .et_pb_contact_field[type="checkbox"]:hover + label i, %%order_class%% .et_pb_contact_field[type="radio"]:hover + label i',
                                    'focus_background_color' => '%%order_class%% .et_pb_contact_select:focus, %%order_class%% .et_pb_contact_field[type="checkbox"]:active + label i, %%order_class%% .et_pb_contact_field[type="radio"]:active + label i',
                                    'focus_background_color_hover' => '%%order_class%% .et_pb_contact_select:focus:hover, %%order_class%% .et_pb_contact_field[type="checkbox"]:active:hover + label i, %%order_class%% .et_pb_contact_field[type="radio"]:active:hover + label i',
                                    'placeholder_focus' => '%%order_class%% p .input:focus::-webkit-input-placeholder, %%order_class%% p .input:focus::-moz-placeholder, %%order_class%% p .input:focus:-ms-input-placeholder, %%order_class%% p textarea:focus::-webkit-input-placeholder, %%order_class%% p textarea:focus::-moz-placeholder, %%order_class%% p textarea:focus:-ms-input-placeholder',
                                    'padding' => '%%order_class%% .et_pb_contact_select, %%order_class%% .et_pb_contact_field[type="checkbox"] + label i, %%order_class%% .et_pb_contact_field[type="radio"] + label i',
                                    'margin' => '%%order_class%% .et_pb_contact_select, %%order_class%% .et_pb_contact_field[type="checkbox"] + label i, %%order_class%% .et_pb_contact_field[type="radio"] + label i',
                                    'form_text_color' => '%%order_class%% .et_pb_contact_select, %%order_class%% .et_pb_contact_field[type="checkbox"] + label, %%order_class%% .et_pb_contact_field[type="radio"] + label, %%order_class%% .et_pb_contact_field[type="checkbox"]:checked + label i:before',
                                    'form_text_color_hover' => '%%order_class%% .et_pb_contact_select:hover, %%order_class%% .et_pb_contact_field[type="checkbox"]:hover + label, %%order_class%% .et_pb_contact_field[type="radio"]:hover + label, %%order_class%% .et_pb_contact_field[type="checkbox"]:checked:hover + label i:before',
                                    'focus_text_color' => '%%order_class%% .et_pb_contact_select:focus, %%order_class%% .et_pb_contact_field[type="checkbox"]:active + label, %%order_class%% .et_pb_contact_field[type="radio"]:active + label, %%order_class%% .et_pb_contact_field[type="checkbox"]:checked:active + label i:before',
                                    'focus_text_color_hover' => '%%order_class%% .et_pb_contact_select:focus:hover, %%order_class%% .et_pb_contact_field[type="checkbox"]:active:hover + label, %%order_class%% .et_pb_contact_field[type="radio"]:active:hover + label, %%order_class%% .et_pb_contact_field[type="checkbox"]:checked:active:hover + label i:before',
                                ),
                                'box_shadow'    => false,
                                'border_styles' => false,
                                'font_field'    => array(
                                    'css' => array(
                                        'main'  => implode( ', ', array(
                                            "{$this->main_css_element} .input",
                                            "{$this->main_css_element} .input::placeholder",
                                            "{$this->main_css_element} .input::-webkit-input-placeholder",
                                            "{$this->main_css_element} .input::-moz-placeholder",
                                            "{$this->main_css_element} .input:-ms-input-placeholder",
                                            "{$this->main_css_element} .input[type=checkbox] + label",
                                            "{$this->main_css_element} .input[type=radio] + label",
                                        ) ),
                                        'hover' => array(
                                            "{$this->main_css_element} .input:hover",
                                            "{$this->main_css_element} .input:hover::placeholder",
                                            "{$this->main_css_element} .input:hover::-webkit-input-placeholder",
                                            "{$this->main_css_element} .input:hover::-moz-placeholder",
                                            "{$this->main_css_element} .input:hover:-ms-input-placeholder",
                                            "{$this->main_css_element} .input[type=checkbox]:hover + label",
                                            "{$this->main_css_element} .input[type=radio]:hover + label",
                                        ),
                                    ),
                                ),
                                'margin_padding' => array(
                                    'css' => array(
                                        'main'    => '%%order_class%% .et_pb_contact_field',
                                        'padding' => '%%order_class%% .et_pb_contact_field .input',
                                        'margin'  => '%%order_class%% .et_pb_contact_field',
                                    ),
                                ),
                            ),
                        ),
                        'height' => array(
                            'css' => array(
                                'main' => implode( ', ',
                                    array(
                                        '%%order_class%% input[type=text]',
                                        '%%order_class%% input[type=email]',
                                        '%%order_class%% textarea',
                                        '%%order_class%% [data-type=checkbox]',
                                        '%%order_class%% [data-type=radio]',
                                        '%%order_class%% [data-type=select]',
                                        '%%order_class%% select',
                                    )
                                ),
                            ),
                        ),
                        'box_shadow' => array(
                            'default' => array(),
                            'filter_item'   => array(
                                'label'           => esc_html__( 'Filter Item Box Shadow', 'et_builder' ),
                                'option_category' => 'layout',
                                'tab_slug'        => 'advanced',
                                'toggle_slug'     => 'filter_item',
                                'css'             => array(
                                    'main' => '%%order_class%% .divi-filter-item',
                                ),
                                'default_on_fronts'  => array(
                                    'color'    => '',
                                    'position' => '',
                                ),
                            ),
                        ),
                    );

                    $this->custom_css_fields = array();
                    $this->help_videos = array();
                }

                function get_fields() {

                    $fields = array(
                        'filter_location' => array(
                            'toggle_slug'       => 'main_content',
                            'label'             => esc_html__( 'Filter Location', 'divi-filter' ),
                            'type'              => 'select',
                            'options'           => array(
                                'side'       => esc_html__( 'Side Columm', 'divi-filter' ),
                                'fullwidth'       => esc_html__( 'Fullwidth Column', 'divi-filter' ),
                            ),
                            'option_category'   => 'configuration',
                                'affects'=>array(
                                    'fullwidth_columns',
                                    'column_gap',
                                    'column_min_width'
                                ),
                                'default'           => 'side',
                                'description'       => esc_html__( 'Choose where you are placing the filter, this will change the appearance and the settings in the design tab', 'divi-filter' ),
                            ),
                            'fullwidth_columns' => array(
                                'toggle_slug'       => 'main_content',
                                'label'             => esc_html__( 'Number of Columns', 'divi-filter' ),
                                'type'              => 'select',
                                'options'           => array(
                                'auto'       => esc_html__( 'Auto', 'divi-filter' ),
                                    '1'       => esc_html__( '1', 'divi-filter' ),
                                    '2'       => esc_html__( '2', 'divi-filter' ),
                                    '3'       => esc_html__( '3', 'divi-filter' ),
                                    '4'       => esc_html__( '4', 'divi-filter' ),
                                    '5'       => esc_html__( '5', 'divi-filter' ),
                                    '6'       => esc_html__( '6', 'divi-filter' ),
                                    '7'       => esc_html__( '7', 'divi-filter' ),
                                    '8'       => esc_html__( '8', 'divi-filter' ),
                                    '9'       => esc_html__( '9', 'divi-filter' ),
                                    '10'       => esc_html__( '10', 'divi-filter' ),
                                ),
                                'option_category'   => 'configuration',
                                'depends_show_if' => 'fullwidth',
                                'default'           => 'auto',
                                'description'       => esc_html__( 'We will evenly divide the space between the number of filters unless you specify the number here.', 'divi-filter' ),
                            ),
                            'column_min_width' => array(
                                'label'           => esc_html__( 'Column Min Width', 'et_builder' ),
                                'type'            => 'range',
                                'default'         => '100',
                                'default_unit'    => 'px',
                                'default_on_front' => '',
                                'depends_show_if' => 'fullwidth',
                                'range_settings'  => array(
                                    'min'  => '1',
                                    'max'  => '1000',
                                    'step' => '1',
                                ),
                                'toggle_slug'       => 'main_content',
                                'option_category'   => 'configuration',
                                'description'       => esc_html__( 'Set the gap between the columns.', 'divi-filter' ),
                            ),
                            'column_gap' => array(
                                'label'           => esc_html__( 'Gap Between Columns', 'et_builder' ),
                                'type'            => 'range',
                                'default'         => '20',
                                'default_unit'    => 'px',
                                'default_on_front' => '',
                                'depends_show_if' => 'fullwidth',
                                'range_settings'  => array(
                                    'min'  => '0',
                                    'max'  => '300',
                                    'step' => '1',
                                ),
                                'toggle_slug'       => 'main_content',
                                'option_category'   => 'configuration',
                                'description'       => esc_html__( 'Set the gap between the columns.', 'divi-filter' ),
                            ),
                        'filter_update_type' => array(
                            'toggle_slug'       => 'main_content',
                            'label'             => esc_html__( 'Filter Update Type', 'divi-filter' ),
                            'type'              => 'select',
                            'options'           => array(
                                'update_button'       => esc_html__( 'Update via button', 'divi-filter' ),
                                'update_field'       => esc_html__( 'Update after each field change', 'divi-filter' ),
                            ),
                            'option_category'   => 'configuration',
                            'affects'=>array(
                                'search_button_text',
                                'button_sidebyside'
                            ),
                            'default'           => 'update_button',
                            'description'       => esc_html__( 'Choose the post type you want to search', 'divi-filter' ),
                        ),
                        'scrollto' => array(
                            'label' => esc_html__( 'Scroll to Section After Ajax Update', 'divi-filter' ),
                            'type' => 'yes_no_button',
                            'option_category' => 'configuration',
                            'options' => array(
                                'on' => esc_html__( 'Yes', 'divi-filter' ),
                                'off' => esc_html__( 'No', 'divi-filter' ),
                            ),
                            'default' => 'off',
                            'description' => esc_html__( 'If you want to scroll to a section after the update of the ajax filter, enable this.', 'divi-filter' ),
                            'toggle_slug' => 'main_content',
                            'affects'=>array(
                                'scrollto_where',
                                'scrollto_section',
                                'scrollto_fine_tune'
                            ),
                        ),
                        'scrollto_where' => array(
                            'toggle_slug'       => 'main_content',
                            'label'             => esc_html__( 'When To use Scroll To?', 'divi-filter' ),
                            'type'              => 'select',
                            'options'           => array(
                                'all'       => esc_html__( 'Both Desktop, Tablet & Mobile', 'divi-filter' ),
                                'desktop'       => esc_html__( 'Desktop Only', 'divi-filter' ),
                                'tab_mob'       => esc_html__( 'Tablet & Mobile', 'divi-filter' ),
                                'mobile'       => esc_html__( 'Mobile Only', 'divi-filter' ),
                            ),
                            'option_category'   => 'configuration',
                            'default'           => 'all',
                            'description'       => esc_html__( 'Specify when you want it to scroll to - on desktop, mobile or all?', 'divi-filter' ),
                            'depends_show_if'   => 'on',
                        ),
                        'scrollto_section' => array(
                            'toggle_slug'       => 'main_content',
                            'label'             => esc_html__( 'Where to Scroll?', 'divi-filter' ),
                            'type'              => 'select',
                            'options'           => array(
                                'main-archive-loop'       => esc_html__( 'Top of Posts', 'divi-filter' ),
                                'main-orderby'       => esc_html__( 'Top of Orderby', 'divi-filter' ),
                                'main-filters'       => esc_html__( 'Top of Filters', 'divi-filter' ),
                            ),
                            'option_category'   => 'configuration',
                            'default'           => 'main-archive-loop',
                            'description'       => esc_html__( 'Choose where to scroll to.', 'divi-filter' ),
                            'depends_show_if'   => 'on',
                        ),
                        'scrollto_fine_tune' => array(
                            'label'             => esc_html__( 'Fine Tune the position', 'divi-filter' ),
                            'type'              => 'range',
                            'default'           => '0px',
                            'toggle_slug'       => 'main_content',
                            'option_category'   => 'configuration',
                            'depends_show_if'   => 'on',
                            'range_settings'   => array(
                                'min'  => '-500',
                                'max'  => '500',
                                'step' => '1',
                            ),
                        ),
                        'search_button_text' => array(
                            'toggle_slug'       => 'main_content',
                            'label'             => esc_html__( 'Filter Button Text', 'divi-filter' ),
                            'type'              => 'text',
                            'option_category'   => 'configuration',
                            'default'           => 'Search',
                            'depends_show_if' => 'update_button',
                            'description'       => esc_html__( 'Choose what you want the search button to say', 'divi-filter' ),
                        ),
                        'button_sidebyside' => array(
                            'label' => esc_html__( 'Filter & Reset Buttons Side By Side?', 'divi-filter' ),
                            'type' => 'yes_no_button',
                            'option_category' => 'configuration',
                            'options' => array(
                                'on' => esc_html__( 'Yes', 'divi-filter' ),
                                'off' => esc_html__( 'No', 'divi-filter' ),
                            ),
                            'default' => 'off',
                            'depends_show_if' => 'update_button',
                            'description' => esc_html__( 'If you want the buttons to be side by side, please enable this.', 'divi-filter' ),
                            'toggle_slug' => 'main_content',
                        ),
                        'reset_text' => array(
                            'toggle_slug'       => 'main_content',
                            'label'             => esc_html__( 'Reset Text', 'divi-filter' ),
                            'type'              => 'text',
                            'option_category'   => 'configuration',
                            'default'           => 'Reset',
                            'description'       => esc_html__( 'Choose what you want the reset button text to say', 'divi-filter' ),
                        ),
                        'appearance' => array(
                            'toggle_slug'       => 'layout',
                            'label'             => esc_html__( 'Filter Style', 'divi-filter' ),
                            'type'              => 'select',
                            'options'           => array(
                                'normal'       => esc_html__( 'Normal', 'divi-filter' ),
                                'toggle'       => esc_html__( 'Toggle', 'divi-filter' ),
                            ),
                            'option_category'   => 'configuration',
                            'affects'=>array(
                                'toggle_icon',
                                'toggle_dropdown_padding'
                            ),
                            'default'           => 'normal',
                            'description'       => esc_html__( 'Choose the appearance style of your filters', 'divi-filter' ),
                        ),
                        'mobile_toggle' => array(
                            'toggle_slug'       => 'mobile_layout',
                            'label'             => esc_html__( 'Toggle Whole Filter?', 'divi-filter' ),
                            'type'              => 'yes_no_button',
                            'options'           => array(
                                'on'       => esc_html__( 'Yes', 'divi-filter' ),
                                'off'       => esc_html__( 'No', 'divi-filter' ),
                            ),
                            'option_category'   => 'configuration',
                            'affects'           => array(
                                'mobile_toggle_style'
                            ),
                            'default'           => 'off',
                            'description'       => esc_html__( 'Enable this when you want to show your filters by toggle.', 'divi-filter' ),
                        ),
                        'mobile_toggle_style' => array(
                            'toggle_slug'       => 'mobile_layout',
                            'label'             => esc_html__( 'Mobile Toggle Style', 'divi-filter' ),
                            'type'              => 'select',
                            'options'           => array(
                                'button'          => esc_html__( 'Button', 'divi-filter' ),
                                // 'sticky'         => esc_html__( 'Sticky Icon on Side', 'divi-filter' ), // need to finish settings for this to work properly
                            ),
                            'option_category'   => 'configuration',
                            'affects'           => array(
                                'mobile_toggle_position',
                                'filter_toggle_name',
                                'filter_toggle_icon',
                                'filter_toggle_icon_color'
                            ),
                            'default'           => 'button',
                            'depends_show_if'   => 'on',
                            'description'       => esc_html__( 'Choose the type of Toggle style you want - how the visitor will open to see the filters.', 'divi-filter' ),
                        ),
                        'filter_toggle_name' => array(
                            'toggle_slug'       => 'mobile_layout',
                            'label'             => esc_html__( 'Filter Toggle Name', 'divi-filter' ),
                            'type'              => 'text',
                            'option_category'   => 'configuration',
                            'default'           => 'Filter',
                            'depends_show_if'   => 'button',
                            'description'       => esc_html__( 'Choose the name of the button that when they press it will toggle the filter', 'divi-filter' ),
                        ),
                        'filter_toggle_icon' => array(
                            'toggle_slug'       => 'mobile_layout',
                          'label'               => esc_html__('Filter Toggle Icon', 'et_builder'),
                          'type'                => 'select_icon',
                          'option_category'     => 'basic_option',
                          'class'               => array('et-pb-font-icon'),
                          'description'         => esc_html__('Choose the icon you want to use to slide the filter.', 'et_builder'),
                          'depends_show_if'     => 'sticky',
                        ),
                        'filter_toggle_icon_color' => array(
                            'default'           => $et_accent_color,
                            'label'             => esc_html__('Filter Toggle Icon Color', 'et_builder'),
                            'type'              => 'color-alpha',
                            'description'       => esc_html__('Here you can define a custom color for your icon.', 'et_builder'),
                            'depends_show_if'   => 'sticky',
                            'toggle_slug'       => 'mobile_layout',
                          ),
                        'mobile_toggle_position' => array(
                            'toggle_slug'       => 'mobile_layout',
                            'label'             => esc_html__( 'Sticky Toggle Position', 'divi-filter' ),
                            'type'              => 'select',
                            'options'           => array(
                                'left'          => esc_html__( 'Left', 'divi-filter' ),
                                'right'         => esc_html__( 'Right', 'divi-filter' ),
                            ),
                            'option_category'   => 'configuration',
                            'default'           => 'left',
                            'depends_show_if'   => 'sticky',
                            'description'       => esc_html__( 'Enable this when you want to show your filters by toggle.', 'divi-filter' ),
                        ),
                        'hide_reset' => array(
                            'toggle_slug'       => 'layout',
                            'label'             => esc_html__( 'Hide Reset Button?', 'divi-machine' ),
                            'type'              => 'yes_no_button',
                            'options'   => array(
                                'on'    => esc_html__( 'Yes', 'divi-machine' ),
                                'off'   => esc_html__( 'No', 'divi-machine' ),
                            ),
                            'default'           => 'off',
                            'affects'           => array(
                                'align_reset'
                            ),
                            'description'       => esc_html__( 'If you want to hide the reset button, enable this.', 'divi-machine' ),
                        ),
                        'align_reset' => array(
                            'toggle_slug'       => 'layout',
                            'label'             => esc_html__( 'Align Reset Button', 'divi-machine' ),
                            'type'              => 'select',
                            'options'   => array(
                                'none'      => esc_html__( 'None', 'divi-machine' ),
                                'left'      => esc_html__( 'Left', 'divi-machine' ),
                                'right'     => esc_html__( 'Right', 'divi-filter' ),
                            ),
                            'default'           => 'none',
                            'depends_show_if'   => 'off',
                            'description'       => esc_html__( 'Select this option if you want to align the reset button with search button.', 'divi-machine' ),
                        ),
                        'filter_item_top' => array(
                            'label'           => esc_html__( 'Filter Item From Top', 'et_builder' ),
                            'type'            => 'range',
                            'default'         => '40',
                            'default_unit'    => 'px',
                            'default_on_front' => '',
                            'range_settings'  => array(
                                'min'  => '0',
                                'max'  => '300',
                                'step' => '1',
                            ),
                            'tab_slug'        => 'advanced',
                            'toggle_slug'     => 'filter_item',
                        ),
                        'filter_item_bg_color' => array(
                            'label'             => esc_html__( 'Filter Item Background Color', 'et_builder' ),
                            'type'              => 'color-alpha',
                            'tab_slug'          => 'advanced',
                            'toggle_slug'       => 'filter_item',
                            'description'       => esc_html__( 'This will change the fill color for the filter item background.', 'et_builder' ),
                            'default'           => "#ffffff"
                        ),
                        'filter_item_padding_top' => array(
                            'label'           => esc_html__( 'Filter Item Padding Top', 'et_builder' ),
                            'type'            => 'range',
                            'default'         => '0',
                            'default_unit'    => 'px',
                            'default_on_front' => '',
                            'range_settings'  => array(
                                'min'  => '0',
                                'max'  => '100',
                                'step' => '1',
                            ),
                            'tab_slug'        => 'advanced',
                            'toggle_slug'     => 'filter_item',
                        ),
                        'filter_item_padding_right' => array(
                            'label'           => esc_html__( 'Filter Item Padding Right', 'et_builder' ),
                            'type'            => 'range',
                            'default'         => '0',
                            'default_unit'    => 'px',
                            'default_on_front' => '',
                            'range_settings'  => array(
                                'min'  => '0',
                                'max'  => '100',
                                'step' => '1',
                            ),
                            'tab_slug'        => 'advanced',
                            'toggle_slug'     => 'filter_item',
                        ),
                        'filter_item_padding_bottom' => array(
                            'label'           => esc_html__( 'Filter Item Padding Bottom', 'et_builder' ),
                            'type'            => 'range',
                            'default'         => '0',
                            'default_unit'    => 'px',
                            'default_on_front' => '',
                            'range_settings'  => array(
                                'min'  => '0',
                                'max'  => '100',
                                'step' => '1',
                            ),
                            'tab_slug'        => 'advanced',
                            'toggle_slug'     => 'filter_item',
                        ),
                        'filter_item_padding_left' => array(
                            'label'           => esc_html__( 'Filter Item Padding Left', 'et_builder' ),
                            'type'            => 'range',
                            'default'         => '0',
                            'default_unit'    => 'px',
                            'default_on_front' => '',
                            'range_settings'  => array(
                                'min'  => '0',
                                'max'  => '100',
                                'step' => '1',
                            ),
                            'tab_slug'        => 'advanced',
                            'toggle_slug'     => 'filter_item',
                        ),
                        'toggle_icon' => array(
                            'label'               => esc_html__( 'Toggle Icon', 'divi-filter' ),
                            'type'                => 'select_icon',
                            'option_category'     => 'configuration',
                            'class'               => array( 'et-pb-font-icon' ),
                            'description'         => esc_html__( 'Choose the icon to open and close the toggle.', 'divi-filter' ),
                            'tab_slug'        => 'advanced',
                            'default'             => '%%18%%',
                            'depends_show_if' => 'toggle',
                            'toggle_slug'     => 'toggle_appearance',
                        ),
                        'toggle_dropdown_padding' => array(
                            'label'           => esc_html__( 'Toggle dropdown padding', 'et_builder' ),
                            'type'            => 'range',
                            'default'         => '10',
                            'default_unit'    => 'px',
                            'default_on_front' => '',
                            'depends_show_if' => 'toggle',
                            'range_settings'  => array(
                                'min'  => '0',
                                'max'  => '200',
                                'step' => '1',
                            ),
                            'tab_slug'        => 'advanced',
                            'toggle_slug'     => 'toggle_appearance',
                            'option_category'   => 'configuration',
                            'description'       => esc_html__( 'Set the padding of the toggle drop down box.', 'divi-filter' ),
                        ),
                        /*'__getfilter' => array(
                            'type' => 'computed',
                            'computed_callback' => array( 'de_mach_filter_posts_code', 'get_filter' ),
                            'computed_depends_on' => array(
                                'name',
                            ),
                        ),*/
                    );

                    return $fields;
                }

                public function get_search_items_content() {
                    return $this->content;
                }

                public static function get_filter ( $args = array(), $conditional_tags = array(), $current_page = array() ){
                    if (!is_admin()) {
                        return;
                    }

                    ob_start();

                    $all_tabs_content = $this->get_search_items_content();
                    echo $all_tabs_content;

                    $data = ob_get_clean();

                    return $data;
                }

                public function get_transition_fields_css_props() {
                    $fields = parent::get_transition_fields_css_props();

                    $fields['form_field_background_color'] = array(
                        'background-color' => implode(', ', array(
                            '%%order_class%% .et_pb_contact_field',
                            '%%order_class%% .et_pb_contact_field[type="checkbox"]+label i',
                            '%%order_class%% .et_pb_contact_field[type="radio"]+label i',
                        ))
                    );

                    return $fields;
                }

                public function get_button_alignment() {
                    $text_orientation = isset( $this->props['button_alignment'] ) ? $this->props['button_alignment'] : '';
                    return et_pb_get_alignment( $text_orientation );
                }

                function render( $attrs, $content = null, $render_slug ) {
                    if (is_admin()) {
                        return;
                    }

                    $search_button_text             = $this->props['search_button_text'];
                    $reset_text                     = $this->props['reset_text'];
                    $filter_update_type             = $this->props['filter_update_type'];
                    $appearance                     = $this->props['appearance'];
                    $mobile_toggle                  = $this->props['mobile_toggle'];
                    $mobile_toggle_style                  = $this->props['mobile_toggle_style'];
                    $filter_toggle_name                  = $this->props['filter_toggle_name'];
                    $mobile_toggle_position         = $this->props['mobile_toggle_position'];
                    $filter_location                = $this->props['filter_location'];
                    $filter_item_bg_color           = $this->props['filter_item_bg_color'];

                    $filter_item_top                = $this->props['filter_item_top'];
                    $toggle_icon                    = $this->props['toggle_icon'];

                    $filter_item_padding_top        = $this->props['filter_item_padding_top'];
                    $filter_item_padding_right      = $this->props['filter_item_padding_right'];
                    $filter_item_padding_bottom     = $this->props['filter_item_padding_bottom'];
                    $filter_item_padding_left       = $this->props['filter_item_padding_left'];

                    $fullwidth_columns              = $this->props['fullwidth_columns'];
                    $column_gap                     = $this->props['column_gap'];
                    $column_min_width               = $this->props['column_min_width'];
                    $toggle_dropdown_padding        = $this->props['toggle_dropdown_padding'];

                    $hide_reset                     = $this->props['hide_reset'];
                    $align_reset                    = $this->props['align_reset'];
                    
                    $button_sidebyside                    = $this->props['button_sidebyside'];

                    $button_alignment               = $this->get_button_alignment();

                      $button_use_icon          	= $this->props['button_use_icon'];
                      $custom_icon          		= $this->props['button_icon'];
                      $button_bg_color       		= $this->props['button_bg_color'];

                      $filter_toggle_icon        =  $args['filter_toggle_icon'];
                      $filter_toggle_icon_color                      = $this->props['filter_toggle_icon_color'];
                    //   $filter_toggle_icon_rendered = 'data-icon="'. esc_attr( et_pb_process_font_icon( $filter_toggle_icon ) ) .'"';
                      
                    
                    
                    
                    
                      
      
                      $mobile_toggle_button_use_icon          	= $this->props['mobile_toggle_button_use_icon'];
                      $mobile_toggle_button_icon          		= $this->props['mobile_toggle_button_icon'];
                      $mobile_toggle_button_bg_color       		= $this->props['mobile_toggle_button_bg_color'];

                      
                      $scrollto          	= $this->props['scrollto'];
                      $scrollto_where          		= $this->props['scrollto_where'];
                      $scrollto_section       		= $this->props['scrollto_section'];
                      $scrollto_fine_tune       		= $this->props['scrollto_fine_tune'];

                      if( $button_use_icon == 'on' && $custom_icon != '' ){
                        $custom_icon = 'data-icon="'. esc_attr( et_pb_process_font_icon( $custom_icon ) ) .'"';
                        ET_Builder_Element::set_style( $render_slug, array(
                          'selector'    => 'body #page-container %%order_class%% .reset-filters:after',
                          'declaration' => "content: attr(data-icon);",
                        ) );
                      }else{
                        ET_Builder_Element::set_style( $render_slug, array(
                          'selector'    => 'body #page-container %%order_class%% .reset-filters:hover',
                          'declaration' => "padding: .3em 1em;",
                        ) );
                      }

                      if( $mobile_toggle_button_use_icon == 'on' && $mobile_toggle_button_icon != '' ){
                          $mobile_toggle_button_icon_dis = 'data-icon="'. esc_attr( et_pb_process_font_icon( $mobile_toggle_button_icon ) ) .'"';
                        ET_Builder_Element::set_style( $render_slug, array(
                          'selector'    => 'body #page-container %%order_class%% .mobile_toggle_trigger.et_pb_button:after',
                          'declaration' => "content: attr(data-icon);",
                        ) );
                      }

                      ET_Builder_Element::set_style($render_slug, array(
                        'selector'    => '%%order_class%% #divi_filter .mobile_toggle_trigger.sticky-toggle:after',
                        'declaration' => sprintf(
                          'content: "%1$s";',
                          $filter_toggle_icon_color
                        ),
                      ));


                    $toggle_icon_dis = DE_Filter::et_icon_css_content( esc_attr($toggle_icon) );

                    $all_tabs_content = $this->get_search_items_content();

                    $this->add_classname( 'divi-location-' . $filter_location );

                    if ($appearance == "toggle") {
                        $this->add_classname( 'divi-filer-toggle' );
                    }

                    if ($hide_reset == "on") {
                        $this->add_classname( 'hide_reset_btn' );
                    }

                    if ($button_sidebyside == "on") {
                        $this->add_classname( 'side_by_side_btns' );
                    }


                    if ( ! empty( $toggle_dropdown_padding ) ) {
                        self::set_style( $render_slug,
                            array(
                                'selector'    => '%%order_class%% .divi-filter-item',
                                'declaration' => sprintf( 'padding: %1$s !important;', esc_html( $toggle_dropdown_padding ) ),
                        ) );
                    }

                    if ( ! empty( $fullwidth_columns ) ) {
                        if ( $fullwidth_columns != "auto") {
                            self::set_style( $render_slug,
                                array(
                                    'selector'    => '%%order_class%%.divi-location-fullwidth .divi-filter-containter',
                                    'declaration' => sprintf( 'grid-template-columns: repeat(%1$s, auto);', esc_html( $fullwidth_columns ) ),
                            ) );
                        }
                    }

                    if ( ! empty( $column_gap ) ) {
                        self::set_style( $render_slug,
                            array(
                                'selector'    => '%%order_class%%.divi-location-fullwidth .divi-filter-containter',
                                'declaration' => sprintf( 'column-gap: %1$s;', esc_html( $column_gap ) ),
                        ) );
                    }

                    if ( ! empty( $column_min_width ) ) {
                        self::set_style( $render_slug,
                            array(
                                'selector'    => '%%order_class%%.divi-location-fullwidth .divi-filter-containter',
                                'declaration' => sprintf( 'grid-template-columns: repeat(auto-fit, minmax(%1$s, 1fr));', esc_html( $column_min_width ) ),
                        ) );
                    }

                    if ( $toggle_icon != ""  && $appearance == "toggle")  {
                        self::set_style( $render_slug,
                            array(
                                'selector'    => '%%order_class%% .et_pb_contact_field_options_title::after',
                                'declaration' => sprintf( 'content: "%1$s";', esc_html( $toggle_icon_dis ) ),
                        ) );
                    }

                    if ( ! empty( $filter_item_bg_color ) ) {
                        self::set_style( $render_slug,
                            array(
                                'selector'    => '%%order_class%% .divi-filter-item',
                                'declaration' => sprintf( 'background-color: %1$s;', esc_attr( $filter_item_bg_color ) ),
                        ) );
                    }

                    if ( ! empty( $filter_item_top ) ) {
                        self::set_style( $render_slug,
                            array(
                                'selector'    => '%%order_class%% .divi-filter-item',
                                'declaration' => sprintf( 'top: %1$s;', esc_attr( $filter_item_top ) ),
                        ) );
                    }

                    if ( ! empty( $filter_item_padding_top ) ) {
                        self::set_style( $render_slug,
                            array(
                                'selector'    => '%%order_class%% .divi-filter-item',
                                'declaration' => sprintf( 'padding-top: %1$s;', esc_attr( $filter_item_padding_top ) ),
                        ) );
                    }

                    if ( ! empty( $filter_item_padding_right ) ) {
                        self::set_style( $render_slug,
                            array(
                                'selector'    => '%%order_class%% .divi-filter-item',
                                'declaration' => sprintf( 'padding-right: %1$s;', esc_attr( $filter_item_padding_right ) ),
                        ) );
                    }

                    if ( ! empty( $filter_item_padding_bottom ) ) {
                        self::set_style( $render_slug,
                            array(
                                'selector'    => '%%order_class%% .divi-filter-item',
                                'declaration' => sprintf( 'padding-bottom: %1$s;', esc_attr( $filter_item_padding_bottom ) ),
                        ) );
                    }

                    if ( ! empty( $filter_item_padding_left ) ) {
                        self::set_style( $render_slug,
                            array(
                                'selector'    => '%%order_class%% .divi-filter-item',
                                'declaration' => sprintf( 'padding-left: %1$s;', esc_attr( $filter_item_padding_left ) ),
                        ) );
                    }


                    $this->add_classname( 'et_pb_button_alignment_' . $button_alignment . '' );


                    $this->add_classname( 'main-filters' );
                    
                    //////////////////////////////////////////////////////////////////////

                    ob_start();

                    global $post;

                    if (is_object($post)) {
                        $post_type = get_post_type( $post->ID );
                        $post_link = get_post_type_archive_link( $post_type );
                    } else {
                        $post_link = "";
                    }



                    if ( $mobile_toggle == 'on' ) {

                    $mobile_toggle_class = '';
                    if ( $mobile_toggle == 'on' && $mobile_toggle_style == "sticky" ) {
                        $mobile_toggle_class = 'mobile_toggle_' . $mobile_toggle_position;
                    } 
                    
                    $this->add_classname( $mobile_toggle_class );

                  
                        if ($mobile_toggle_style == "button") {
                            $this->add_classname( "toggle_mobile" );
                            echo '<a class="mobile_toggle_trigger et_pb_button" '.$mobile_toggle_button_icon_dis.'>'.$filter_toggle_name.'</a>';
                            $toggle_html = '';
                        } else {
                            $toggle_html = '<div class="mobile_toggle_trigger sticky-toggle" '.$filter_toggle_icon_rendered.'></div>';
                        }
                    }
                ?>

                <div id="divi_filter" class="et_pb_contact updatetype-<?php echo $filter_update_type ?> " data-settings='{"scrollto" : "<?php echo $scrollto ?>","scrollto_where" : "<?php echo $scrollto_where ?>","scrollto_section" : "<?php echo $scrollto_section ?>","scrollto_fine_tune" : "<?php echo $scrollto_fine_tune ?>"}'>
      
                    <?php echo $toggle_html ?>
                    
                    <div class="ajax-filter-results"></div>
                    <div class="divi-filter-containter">
                    <?php echo $all_tabs_content; ?>
                    </div>
                    <?php if ($filter_update_type == "update_button") { 
                        $align_search_class = '';
                        if ( $align_reset == 'left' ) {
                            $align_search_class = "align_reset_right";
                        }else if ( $align_reset == 'right' ){
                            $align_search_class = "align_reset_left";
                        }
                    ?>
                    <input id="divi_filter_button" class="et_pb_button button <?php echo $align_search_class;?>" type="submit" value="<?php echo $search_button_text ?>"  style="margin-top: 20px;" />
            <script>
                jQuery(document).ready(function($) {
                    $( "#divi_filter_button" ).click(function() {
                        divi_find_filters_to_filter();

                        var type_arr = [],
                        slug_arr = [],
                        value_arr = [],
                        name_arr = [],
                        filter_param_type_arr = [],
                        iris_to_arr = [],
                        irs_from_arr = [];

                        $('.et_pb_de_mach_search_posts_item').each(function(i, obj) {
                  
                            
                            if ( jQuery(this).hasClass( "filter_params" ) ) {
                            if ( jQuery(this).hasClass( "filter_params_yes_title" ) ) {
                                var filter_param_type = "title";
                            } else {
                                var filter_param_type = "no-title";
                            }
                            var type = jQuery(this).find(".et_pb_contact_field ").attr("data-type"),
                            slug = $(this).find('.divi-acf').attr("name"),
                            name = jQuery(this).find(".et_pb_contact_field_options_title ").html(),
                            iris_to = "",
                            irs_from = "";
                            if (type == "select") {
                                if ($(this).find('.divi-acf').val() !== "") {
                                var value = $(this).find('.divi-acf').find('option:selected').text();
                                } else {
                                var value = "";
                                }
                            } else if (type == "radio") {

                                var selected_radio = $(this).find('input[type="radio"]:checked');

                                if (selected_radio.closest('.et_pb_contact_field_radio').find('.radio-label').attr('title') !== "") {
                                    // console.log("firdt");
                                var value = selected_radio.closest('.et_pb_contact_field_radio').find('.radio-label').attr('title');
                                } else {
                                    // console.log("sdsds");
                                var value = selected_radio.closest('.et_pb_contact_field_radio').find('.divi-acf').val();
                                }
                            } else {
                                var value = $(this).find('.divi-acf').val();
                            }

                            iris_to = $(this).find(".irs-to").text(),
                            irs_from = $(this).find(".irs-from").text();
                            
                            type_arr.push(type);
                            slug_arr.push(slug);
                            value_arr.push(value);
                            name_arr.push(name);
                            filter_param_type_arr.push(filter_param_type);
                            iris_to_arr.push(iris_to);
                            irs_from_arr.push(irs_from);


                        }

                        });




                        divi_filter_params_array(type_arr, slug_arr, value_arr, name_arr, filter_param_type_arr, iris_to_arr, irs_from_arr);
                    });
                    $('#divi_filter select, #divi_filter input[type=radio], #divi_filter input[type=checkbox], #divi_filter input[type=range]').on('change', function() {
                        var filter_item_name = $(this).attr("name"),
                            filter_item_val = $(this).val();
                        divi_append_url(filter_item_name, filter_item_val);
                    });
                });
            </script>
                    <?php } ?>
                    <?php 
                    $align_reset_class = '';
                    if ( $align_reset != 'none' ) {
                        $align_reset_class = "align_reset_" . $align_reset;
                    }
                    ?>
                    <div class="button_container <?php echo $align_reset_class;?>" style="margin-top: 20px;">
                        <a class="reset-filters et_pb_button" <?php echo $custom_icon ?> href=""><?php echo $reset_text ?></a>
                    </div>
                </div>
            <?php
                    if ($filter_update_type == "update_field") {
            ?>
            <script>
                jQuery(document).ready(function($) {
                    $('#divi_filter select, #divi_filter input[type=radio], #divi_filter input[type=checkbox], #divi_filter input[type=range]').on('change', function() {

                        if ( jQuery(this).closest(".et_pb_de_mach_search_posts_item").hasClass( "filter_params" ) ) {
                            if ( jQuery(this).closest(".et_pb_de_mach_search_posts_item").hasClass( "filter_params_yes_title" ) ) {
                                var filter_param_type = "title";
                            } else {
                                var filter_param_type = "no-title";
                            }
                            var type = jQuery(this).closest(".et_pb_de_mach_search_posts_item").find(".et_pb_contact_field ").attr("data-type"),
                            slug = $(this).attr("name"),
                            name = jQuery(this).closest(".et_pb_de_mach_search_posts_item").find(".et_pb_contact_field_options_title ").html(),
                            iris_to = "",
                            irs_from = "";
                            if (type == "select") {
                                if ($(this).val() !== "") {
                                var value = $(this).find('option:selected').text();
                                } else {
                                var value = "";
                                }
                            } else if (type == "radio") {
                                if ($(this).closest(".et_pb_contact_field_radio").find('.radio-label').attr('title') !== "") {
                                var value = $(this).closest(".et_pb_contact_field_radio").find('.radio-label').attr('title');
                                } else {
                                var value = $(this).val();
                                }
                            } else {
                                var value = $(this).val();
                            }
                            divi_filter_params(type, slug, value, name, filter_param_type, iris_to, irs_from);
                        }

                        divi_find_filters_to_filter();
                    });
                    $('#divi_filter input[type=text]').blur(function() {
                        divi_find_filters_to_filter();
                    });
                    $('#divi_filter input[type=text]').bind("enterKey",function(e){
                        divi_find_filters_to_filter();
                    });
                    $('#divi_filter input[type=text]').keyup(function(e){
                        if(e.keyCode == 13)
                        {
                            $(this).trigger("enterKey");
                        }
                    });

                    $('.divi-tag-cloud a').click(function( event ) {
                        event.preventDefault();
                    });
                });
            </script>
            <?php
                    }

                    $data = ob_get_clean();

                    //////////////////////////////////////////////////////////////////////

                    return $data;
                }
            }

            new de_df_filter_product_code;
        }
    }
}