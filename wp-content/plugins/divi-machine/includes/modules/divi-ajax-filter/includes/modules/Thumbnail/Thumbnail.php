<?php
if ( ! defined( 'ABSPATH' ) ) exit; 

if ( !function_exists("Divi_filter_thumbnail_module_import") ){
  add_action( 'et_builder_ready', 'Divi_filter_thumbnail_module_import');
  function Divi_filter_thumbnail_module_import(){
    if(class_exists("ET_Builder_Module") && !class_exists("et_pb_de_mach_thumbnail") && !class_exists("et_pb_db_shop_thumbnail")){
      class de_filter_thumbnail_code extends ET_Builder_Module {
        public $vb_support = 'on';
        protected $module_credits = array(
          'module_uri' => DE_DF_PRODUCT_URL,
          'author'     => DE_DF_AUTHOR,
          'author_uri' => DE_DF_URL,
        );
        
        function init() {
          if (defined('DE_DB_WOO_VERSION')) {
            $this->name       = esc_html__( '.LL Thumbnail - Loop Layout', 'divi-bodyshop-woocommerce' );
            $this->slug = 'et_pb_db_shop_thumbnail';
          } else if (defined('DE_DMACH_VERSION')) {
            $this->name       = esc_html__( '.Thumbnail - Divi Machine', 'divi-machine' );
            $this->slug = 'et_pb_de_mach_thumbnail';
          } else {
            $this->name       = esc_html__( '.Thumbnail - Divi Ajax Filter', 'divi-filter' );
            $this->slug = 'et_pb_df_thumbnail';
          }
          
          $this->fields_defaults = array(
            // 'loop_layout'         => array( 'on' ),
                      'link_product'   => array( 'on' ),
          );
          
          $this->settings_modal_toggles = array(
            'general' => array(
              'toggles' => array(
                'main_content' => esc_html__( 'Main Options', 'divi-filter' ),
              ),
            ),
            'advanced' => array(
              'toggles' => array(
                'text' => esc_html__( 'Text', 'divi-filter' ),
              ),
            ),
          );
          
          $this->main_css_element = '%%order_class%%';
          $this->advanced_fields = array(
            'fonts' => array(

            ),
            'background' => array(
              'settings' => array(
              'color' => 'alpha',
              ),
            ),
            'button' => array(

            ),
            'box_shadow' => array(
              'default' => array(),
              'thumbnail_image' => array(
                'label' => esc_html__( 'Thumbnail Box Shadow', 'divi-filter' ),
                'css' => array(
                  'main' => "%%order_class%% img",
                ),
                'option_category' => 'layout',
                'tab_slug'        => 'advanced',
              ),
            ),
          );
          
          $this->custom_css_fields = array();
          $this->help_videos = array();

        }
        
        function get_fields() {
          $options = $col_options = array();
          $sizes = get_intermediate_image_sizes();
          
          foreach ($sizes as $size) {
            $options[$size] = $size;
          }
          
          for ($i = 1; $i <= 6; $i++) {
            $col_options[$i] = $i;
          }
          
          $acf_fields = array();

          if( class_exists('ACF') ) {

            if (defined('DE_DB_WOO_VERSION')) {
              } else {
                $acf_fields = DE_Filter::get_acf_fields();
              }
              
            $image_style_settings = array(
              'default'  => esc_html__( 'Default', 'et_builder' ),
              'no_overlay' => esc_html__( 'Image Only (no overlay)', 'et_builder' ),
              'flip_image_dmach'  => esc_html__( 'Flip Image', 'et_builder' ),
            );
          } else if ( defined('DE_DB_WOO_VERSION') ) {
            $image_style_settings = array(
              'default'  => esc_html__( 'Default', 'et_builder' ),
              'no_overlay' => esc_html__( 'Image Only (no overlay)', 'et_builder' ),
              'flip_image'  => esc_html__( 'Flip Image', 'et_builder' ),
            );
          } else {
            $image_style_settings = array(
              'default'  => esc_html__( 'Default', 'et_builder' ),
              'no_overlay' => esc_html__( 'Image Only (no overlay)', 'et_builder' ),
            );
          }


          if (defined('DE_DB_WOO_VERSION')) {
          $options_posttype = DEBC_INIT::get_divi_post_types();
          } else {
            $options_posttype = DE_Filter::get_divi_post_types();
          }


          $fields = array(
            'thumb_image_size' => array(
              'label' => __('Thumbnail Image Size', 'et_builder'),
              'type' => 'select',
              'computed_affects' => array(
                '__getthumbnail',
              ),
              'options' => $options,
              'default' => 'woocommerce_thumbnail',
              'description' => __('Pick a size for the thumbnail image from the list.', 'et_builder'),
            ),
            'link_product' => array(
              'label'             => esc_html__( 'Link Image to Single Page', 'et_builder' ),
              'type'              => 'yes_no_button',
              'option_category'   => 'configuration',
              'options'           => array(
                'on'  => esc_html__( 'Yes', 'et_builder' ),
                'off' => esc_html__( 'No', 'et_builder' ),
              ),
              'affects'=>array(
                  'new_tab'
              ),
              'description'        => esc_html__( 'Enable this if you want to allow the user to click on the image to go to the single page.)', 'et_builder' ),
            ),
            'new_tab' => array(
              'label'             => esc_html__( 'Open In New Tab?', 'et_builder' ),
              'type'              => 'yes_no_button',
              'option_category'   => 'layout',
              'option_category'   => 'configuration',
              'options'           => array(
                'on'  => esc_html__( 'Yes', 'et_builder' ),
                'off' => esc_html__( 'No', 'et_builder' ),
              ),
              'default'            => 'off',
              'depends_show_if'   => 'on',
              'description'        => esc_html__( 'Enable this if you want the link to open in a new tab.', 'et_builder' ),
            ),
            'image_style' => array(
              'label'             => esc_html__( 'Image Style', 'et_builder' ),
              'type'              => 'select',
              // 'option_category'   => 'color_option',
              'options'           => $image_style_settings,
              'affects'         => array(
                'acf_name',
              ),
              'description'       => esc_html__( 'Choose what style of thumbnail you want, for example if you want the first image of your gallery to flip to when hovering over the thumbnail, select "Flip Hover". Flip image only works with BodyCommerce or ACF installed', 'et_builder' ),
            ),
            'acf_name' => array(
              'label'             => esc_html__( 'Choose Second Image', 'divi-filter' ),
              'type'              => 'select',
              'depends_show_if'   => 'flip_image_dmach',
              'options'           => $acf_fields,
              'option_category'   => 'configuration',
              'description'       => esc_html__( 'Choose the second image you want to show when hovered (flip)', 'divi-filter' ),
            ),
            'icon_hover_color' => array(
              'label'             => esc_html__( 'Icon Hover Color', 'et_builder' ),
              'type'              => 'color-alpha',
              'custom_color'      => true,
              'tab_slug'          => 'advanced',
              'toggle_slug'       => 'overlay',
            ),
            'hover_overlay_color' => array(
              'label'             => esc_html__( 'Hover Overlay Color', 'et_builder' ),
              'type'              => 'color-alpha',
              'custom_color'      => true,
              'tab_slug'          => 'advanced',
              'toggle_slug'       => 'overlay',
            ),
            'hover_icon' => array(
              'label'               => esc_html__( 'Hover Icon Picker', 'et_builder' ),
              'type'                => 'select_icon',
              'option_category'     => 'configuration',
              'default'			=> 'P',
              'class'               => array( 'et-pb-font-icon' ),
              'depends_show_if'     => 'on',
              'tab_slug'            => 'advanced',
              'toggle_slug'         => 'overlay',
              'description'         => esc_html__( 'Here you can define a custom icon for the overlay', 'et_builder' ),
            ),
            'align' => array(
              'label'           => esc_html__( 'Image Alignment', 'et_builder' ),
              'type'            => 'text_align',
              'option_category' => 'layout',
              'options'         => et_builder_get_text_orientation_options( array( 'justified' ) ),
              'default_on_front' => 'left',
              'tab_slug'        => 'advanced',
              'toggle_slug'     => 'alignment',
              'description'     => esc_html__( 'Here you can choose the image alignment.', 'et_builder' ),
              'options_icon'    => 'module_align',
            ),
            'force_fullwidth' => array(
              'label'             => esc_html__( 'Force Fullwidth', 'et_builder' ),
              'type'              => 'yes_no_button',
              'option_category'   => 'layout',
              'options'           => array(
                'off' => esc_html__( 'No', 'et_builder' ),
                'on'  => esc_html__( 'Yes', 'et_builder' ),
              ),
              'default_on_front' => 'off',
              'tab_slug'    => 'advanced',
              'toggle_slug' => 'width',
              'affects' => array(
                'max_width',
              ),
            ),
            'always_center_on_mobile' => array(
              'label'             => esc_html__( 'Always Center Image On Mobile', 'et_builder' ),
              'type'              => 'yes_no_button',
              'option_category'   => 'layout',
              'options'           => array(
                'on'  => esc_html__( 'Yes', 'et_builder' ),
                'off' => esc_html__( 'No', 'et_builder' ),
              ),
              'default_on_front' => 'on',
              'tab_slug'          => 'advanced',
              'toggle_slug'       => 'alignment',
            ),
            'vb_posttype' => array(
              'toggle_slug'       => 'visual_builder',
              'label'       => __( 'Visual Builder Post', 'et_builder' ),
              'type'        => 'select',
              'options'     => $options_posttype,
              'default'     => 'none',
              'computed_affects' => array(
                '__getthumbnail',
              ),
              'description' => __( 'Set the post type you want to display in the Visual builder - we will look for the first post in this post type to get the data.', 'et_builder' ),
            ),
            '__getthumbnail' => array(
              'type' => 'computed',
              'computed_callback' => array( 'de_filter_thumbnail_code', 'get_thumbnail' ),
              'computed_depends_on' => array(
              'thumb_image_size',
              'vb_posttype'
              ),
            ),
          );
          
          return $fields;
        }
        
        public static function get_thumbnail ( $args = array(), $conditional_tags = array(), $current_page = array() ){

          
          if (!is_admin()) {
            return;
          }

          $thumb_image_size  = $args['thumb_image_size'];
          $vb_posttype  = $args['vb_posttype'];
          
          ob_start();
          if ($vb_posttype == "none") {
            if ( defined('DE_DB_WOO_VERSION')) {
              $default_post = "product";
            } else {
              $default_post = "post";
            }
          } else {
            $default_post = $vb_posttype;
          }
          
          $get_cpt_args = array(
            'post_type' => $default_post,
            'post_status' => 'publish',
            'posts_per_page' => '1',
            'orderby' => 'ID',
            'order' => 'ASC',
          );
          
          query_posts( $get_cpt_args );
          $first = true;
          if ( have_posts() ) {
            while ( have_posts() ) {
              the_post();
              // setup_postdata( $post );
              if ( $first )  {
                //////////////////////////////////////////////////
                if (class_exists('woocommerce') && get_post_type() == "product") {
                  do_action( 'woocommerce_before_shop_loop_item_title' );
                } else {
                  ?>
                  <span class="et_shop_image">
                  <?php
                  echo get_the_post_thumbnail( get_the_id(), $thumb_image_size, array( 'class' => 'featured-image' ) );
                  ?>
                  <span class="et_overlay"></span>
                  </span>
                  <?php
                }
                /////////////////////////////////////////////////
                $first = false;
              } else {

              }
            }
          }
          $data = ob_get_clean();
          return $data;

        }

        public function get_alignment() {
          $alignment = isset( $this->props['align'] ) ? $this->props['align'] : '';

          return et_pb_get_alignment( $alignment );
        }
        
        function render( $attrs, $content = null, $render_slug ) {
         
          $link_product               = $this->props['link_product'];
          $new_tab		= $this->props['new_tab'];
          $image_style                = $this->props['image_style'];
          $align                      = $this->get_alignment();
          $force_fullwidth            = $this->props['force_fullwidth'];
          $always_center_on_mobile    = $this->props['always_center_on_mobile'];
          $icon_hover_color           = $this->props['icon_hover_color'];
          $hover_overlay_color        = $this->props['hover_overlay_color'];
          $hover_icon                 = $this->props['hover_icon'];
          $animation_style            = $this->props['animation_style'];
          $thumb_image_size           = $this->props['thumb_image_size'];
          $acf_name                   = $this->props['acf_name'];

          
          if ($new_tab == "on") {
            $new_tab_dis = 'target="_blank"';
          } else {
            $new_tab_dis = '';
          }
          
          if ( '' !== $icon_hover_color ) {
            ET_Builder_Element::set_style( $render_slug, array(
              'selector'    => '%%order_class%% .et_overlay:before',
              'declaration' => sprintf(
                'color: %1$s !important;', esc_html( $icon_hover_color )
              ),
            ) );
          }
          if ( '' !== $hover_overlay_color ) {
            ET_Builder_Element::set_style( $render_slug, array(
              'selector'    => '%%order_class%% .et_overlay',
              'declaration' => sprintf(
                'background-color: %1$s !important;
                border-color: %1$s;',
                esc_html( $hover_overlay_color )
              ),
            ) );
          }
          if ( ! $this->_is_field_default( 'align', $align ) ) {
            ET_Builder_Element::set_style( $render_slug, array(
              'selector'    => '%%order_class%%',
              'declaration' => sprintf(
                'text-align: %1$s;',
                esc_html( $align )
              ),
            ) );
          }
          if ( 'center' !== $align ) {
            ET_Builder_Element::set_style( $render_slug, array(
              'selector'    => '%%order_class%%',
              'declaration' => sprintf(
                'margin-%1$s: 0;',
                esc_html( $align )
              ),
            ) );
          }
          if ( 'on' === $force_fullwidth ) {
            ET_Builder_Element::set_style( $render_slug, array(
              'selector'    => '%%order_class%%',
              'declaration' => 'max-width: 100% !important;',
            ) );
            ET_Builder_Element::set_style( $render_slug, array(
              'selector'    => '%%order_class%% .et_pb_image_wrap, %%order_class%% img',
              'declaration' => 'width: 100%;',
            ) );
          }
          if ( 'on' === $always_center_on_mobile ) {
            $this->add_classname( 'et_always_center_on_mobile' );
          }
          if ($image_style == "flip_image" || $image_style == "flip_image_dmach" ){
            $this->add_classname( 'flip-image-thumbnail' );
          }
          if ( ! in_array( $animation_style, array( '', 'none' ) ) ) {
            $this->add_classname( 'et-waypoint' );
          }
          if ( '' !== $hover_overlay_color ) {
            ET_Builder_Element::set_style( $render_slug, array(
              'selector'    => '%%order_class%% .et_overlay',
              'declaration' => sprintf(
              'background-color: %1$s;',
              esc_html( $hover_overlay_color )
              ),
            ) );
          }
          
          
          if (defined('DE_DB_WOO_VERSION')) {
            $css_icon_hover = DEBC_INIT::et_icon_css_content( esc_attr($hover_icon) );
            } else {
              $css_icon_hover = DE_Filter::et_icon_css_content( esc_attr($hover_icon) );
            }
          
          ET_Builder_Element::set_style( $render_slug, array(
            'selector'    => '%%order_class%% .et_overlay:before',
            'declaration' => sprintf(
              'content: "%1$s";',
              esc_html( $css_icon_hover )
            ),
          ) );
          
          //////////////////////////////////////////////////////////////////////
          
          ob_start();
          
          $post   = get_post( get_the_id() );
          
          if ($link_product == 'on') {
            if (class_exists('woocommerce') && get_post_type() == "product") {
              global $product, $woocommerce;
              if ( ! is_a( $product, 'WC_Product' ) ) {
                return;
              }
              $product_id = $product->get_id();
              $url = get_permalink( $product_id );
            } else {
              $url = get_permalink( get_the_ID() );
            }
            echo '<a href="'.$url.'" '.$new_tab_dis.'>';
          }
          if ($image_style == "flip_image" || $image_style == "flip_image_dmach"){
            
            if (class_exists('woocommerce') && get_post_type() == "product") {

              global $product;
              $attachment_ids = $product->get_gallery_image_ids();
              echo woocommerce_show_product_loop_sale_flash();
              ?>
              <div class="flip-image-cont">
              <?php
              
              if (!empty($attachment_ids)) {
                $image_info = wp_get_attachment_image_src( $attachment_ids[0], $thumb_image_size );
                $attachment_first[1] = get_post_thumbnail_id( $product->get_id() );
                $attachment = wp_get_attachment_image_src( $attachment_first[1], $thumb_image_size );
                $w = $attachment[1];
                $h = $attachment[2];
                
                $image_title = get_the_title($attachment_ids[0]);
                $image_alt = get_post_meta($attachment_ids[0], '_wp_attachment_image_alt', TRUE);
                echo '<img class="secondary-image" src="'.$image_info[0].'" height="'.$h.'" width="'.$w.'" title="'.$image_title.'" alt="'.$image_alt.'">';
              }
              global $product;
              $image_size = apply_filters( 'single_product_archive_thumbnail_size', $thumb_image_size );
              echo $product ? $product->get_image( $image_size ) : '';
              ?>
              </div>
              <?php

            } else if ( class_exists('ACF') ) {
              
              $acf_get = get_field_object($acf_name);
              $acf_type = $acf_get['type'];
              $image = null;
              if ( !empty( $acf_get['parent'] ) && get_post_type($acf_get['parent']) != 'acf-field-group' ) {
                $parent_object = get_post( $acf_get['parent'] );
                $parent_object_key = $parent_object->post_name;
                if ( have_rows($parent_object_key, get_the_ID()) ) {
                  while ( have_rows($parent_object_key ) ) : the_row();
                    $image = get_sub_field($acf_name);
                  endwhile;
                }
              }else{
                $image = get_field( $acf_name );
              }
              
              if ($acf_type == "image") {
                $return_format = $acf_get['return_format'];
                if ($return_format == "array") {
                  $thumb = wp_get_attachment_image_src( $image['id'], $thumb_image_size );
                  $srcset = wp_get_attachment_image_srcset( $image['id'], $thumb_image_size );
                  $image_info = esc_url($thumb[0]);
                  $alt = esc_attr($image['alt']);
                  $title = esc_attr($image['caption']);
                } else if ($return_format == "id") {
                  $thumb = wp_get_attachment_image_src( $image, $thumb_image_size );
                  $srcset = wp_get_attachment_image_srcset( $image, $thumb_image_size );
                  $image_info = esc_url( $thumb[0] );
                  $alt = "";
                  $title = "";
                } else if ( $return_format == "url") {
                  $image_info = $image;
                  $srcset = "";
                  $alt = "";
                  $title = "";
                }
                ?>
                <img class="secondary-image" src="<?php echo $image_info; ?>" alt="<?php echo $alt ?>" title="<?php echo $title ?>" srcset="<?php echo esc_attr($srcset);?>" />
                <?php
                echo get_the_post_thumbnail( get_the_id(), $thumb_image_size, array( 'class' => 'featured-image' ) );
              } else {
                echo get_the_post_thumbnail( get_the_id(), $thumb_image_size, array( 'class' => 'featured-image' ) );
              }
            } else {

            }

          } else if ($image_style == "no_overlay") {
            
            if (class_exists('woocommerce') && get_post_type() == "product") {
              echo woocommerce_show_product_loop_sale_flash();
              global $product;
              $image_size = apply_filters( 'single_product_archive_thumbnail_size', $thumb_image_size );
              echo $product ? $product->get_image( $image_size ) : '';
            } else {
              echo get_the_post_thumbnail( get_the_id(), $thumb_image_size, array( 'class' => 'featured-image' ) );          
            }
          
          } else {
            
            if (class_exists('woocommerce') && get_post_type() == "product") {
              do_action( 'woocommerce_before_shop_loop_item_title' );
            } else {
              ?>
              <span class="et_shop_image">
              <?php
              echo get_the_post_thumbnail( get_the_id(), $thumb_image_size, array( 'class' => 'featured-image' ) );
              ?>
              <span class="et_overlay"></span>
              </span>
              <?php
            }

          }
          
          if ($link_product == 'on') {
            echo '</a>';
          }
          
          $data = ob_get_clean();
          
          //////////////////////////////////////////////////////////////////////
          
          return $data;
        }
      }
      new de_filter_thumbnail_code;
    }
  }
}
