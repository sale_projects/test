<?php

/*if ( ! class_exists( 'ET_Builder_Element' ) ) {
	return;
}
*/
$module_files = glob( __DIR__ . '/modules/*/*.php' );

// Load custom Divi Builder modules
foreach ( (array) $module_files as $module_file ) {
	if ( $module_file && preg_match( "/\/modules\/\b([^\/]+)\/\\1\.php$/", $module_file ) ) {
		if ( strpos($module_file, 'MachineLoop.php') !== false ){
			if ( defined('DE_DMACH_VERSION') ) {
				require_once $module_file;
			}
		} else{
			require_once $module_file;
		}
		
	}
}
