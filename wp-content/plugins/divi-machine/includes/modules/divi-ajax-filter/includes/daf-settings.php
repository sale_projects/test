<?php

  add_action( 'admin_menu', 'divi_filter_add_menu' );
  function divi_filter_add_menu(){
    global $menu;
    $menuExist = false;
    foreach($menu as $item) {
        if(strtolower($item[0]) == strtolower('Divi Engine')) {
            $menuExist = true;
        }
    }
    if(!$menuExist) {
      $icon = DE_DF_PLUGIN_URL . 'images/dash-icon.svg';
      $page_title = 'Divi Engine';
      $menu_title = 'Divi Engine';
      $capability = 'edit_pages';
      $menu_slug  = 'divi-engine';
      $icon_url   = $icon;
      $position   = $icon;
      add_menu_page(
          $page_title,
          $menu_title,
          $capability,
          $menu_slug,
          $icon_url,
          $position
      );
    }
  }

  function divi_register_options_page() {
    global $menu;
    $menuExist = false;
    foreach($menu as $item) {
        if(strtolower($item[0]) == strtolower('Divi Engine')) {
            $menuExist = true;
        }
    }
    if(!$menuExist) {
      add_options_page('Divi Engine', 'Divi Engine', 'edit_pages', 'divi-engine', 'daf_de_page');
    }
    }
    add_action('admin_menu', 'divi_register_options_page');

  function daf_de_page() {
    global $menu;
    $menuExist = false;
    foreach($menu as $item) {
        if(strtolower($item[0]) == strtolower('Divi Engine')) {
            $menuExist = true;
        }
    }
    if(!$menuExist) {
    $support_icon = DE_DF_PLUGIN_URL . 'images/admin-area/support-ticket.png';
      ?>
      <table>
      <tr valign="top" class="even first tf-heading">
          <th scope="row" class="first last" colspan="2">
              <h3 id="welcome-to-divi-engine!">Welcome to Divi Engine!</h3>
                          </th>
      </tr>
              <tr valign="top" class="row-1 odd" >
      <th scope="row" class="first">
          <label for="divi-machine_9fe2f700f7c4416f"></label>
      </th>
      <td class="second tf-note">
      <p class='description'>Firstly we would like to thank you for purchasing one of our plugins! We hope that you find it useful in your web design adventures.</p>		</td>
      </tr>
              <tr valign="top" class="even first tf-heading">
          <th scope="row" class="first last" colspan="2">
              <h3 id="tech-support">Tech Support</h3>
                          </th>
      </tr>
              <tr valign="top" class="row-2 even" >
      <th scope="row" class="first">
          <label for="divi-machine_80ce9ba56528f771"></label>
      </th>
      <td class="second tf-note">
      <p class='description'>We know from time to time, things may not go to plan Adue to the number of different setups on WordPress sites.<br>Dont worry, we are here to help. First take a look at our documentation (see below), if you cannot find a solution, use our support section on our site and we will help you resolve any issues.<br>
<a href="https://diviengine.com/support/" target="_blank"><img style="position: relative;left: 0;top: 12px;width: 200px;transform: translateY(0);" src="<?php echo $support_icon ?>"></a>
</p>		</td>
      </tr>
      </table>
      <?php
    }
  }
