<?php
if ( ! defined( 'ABSPATH' ) ) exit;
/* test
Plugin Name: Divi Ajax Filters
Plugin URL: https://diviengine.com
Description: Create Ajax Filters for WooCommerce & ACF. Best used with one of our plugins BodyCommerce or Machine.
Version: 1.0.2
Author: Divi Engine
Author URI: https://diviengine.com
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
@author      diviengine.com
@copyright   2020 diviengine.com

Father God, I pray that you bless the people who interact and who own this website - I pray the blessing to be one that goes beyond worldy treasures but understandiong the deep love you have for them. In Jesus name, Amen

The Way of Love - 1 Corinthians 13 MSG
If I speak with human eloquence and angelic ecstasy but don’t love, I’m nothing but the creaking of a rusty gate.
If I speak God’s Word with power, revealing all his mysteries and making everything plain as day, and if I have faith that says to a mountain, “Jump,” and it jumps, but I don’t love, I’m nothing.
If I give everything I own to the poor and even go to the stake to be burned as a martyr, but I don’t love, I’ve gotten nowhere. So, no matter what I say, what I believe, and what I do, I’m bankrupt without love.

Love never gives up.
Love cares more for others than for self.
Love doesn’t want what it doesn’t have.
Love doesn’t strut,
Doesn’t have a swelled head,
Doesn’t force itself on others,
Isn’t always "me first,"
Doesn’t fly off the handle,
Doesn’t keep score of the sins of others,
Doesn’t revel when others grovel,
Takes pleasure in the flowering of truth,
Puts up with anything,
Trusts God always,
Always looks for the best,
Never looks back,
But keeps going to the end.

Love never dies. Inspired speech will be over some day; praying in tongues will end; understanding will reach its limit. We know only a portion of the truth, and what we say about God is always incomplete. But when the Complete arrives, our incompletes will be canceled.
When I was an infant at my mother’s breast, I gurgled and cooed like any infant. When I grew up, I left those infant ways for good.
We don’t yet see things clearly. We’re squinting in a fog, peering through a mist. But it won’t be long before the weather clears and the sun shines bright! We’ll see it all then, see it all as clearly as God sees us, knowing him directly just as he knows us!
But for right now, until that completeness, we have three things to do to lead us toward that consummation: Trust steadily in God, hope unswervingly, love extravagantly. And the best of the three is love.
*/


defined('DE_DF_VERSION') or define('DE_DF_VERSION', '1.0.2');

defined('DE_DF_PATH') or define('DE_DF_PATH',   plugin_dir_path(__FILE__));
defined('DE_DF_PLUGIN_URL') or define('DE_DF_PLUGIN_URL',   plugin_dir_url(__FILE__));
defined('DE_DF_APP_API_URL') or define('DE_DF_APP_API_URL',      'https://diviengine.com/index.php');
defined('DE_DF_PRODUCT_ID') or define('DE_DF_PRODUCT_ID',           'WP-DE-DF');
defined('DE_DF_PRODUCT_URL') or define('DE_DF_PRODUCT_URL', 'https://diviengine.com/product/divi-bodycommerce/');
defined('DE_DF_INSTANCE') or define('DE_DF_INSTANCE',             str_replace(array ("https://" , "http://"), "", network_site_url()));
defined('DE_DF_AUTHOR') or define('DE_DF_AUTHOR', 'Divi Engine');
defined('DE_DF_URL') or define('DE_DF_URL', 'https://www.diviengine.com');



// IF BC
if (defined('DE_DB_WOO_VERSION')) {
// IF MACHINE
} else if (defined('DE_DMACH_VERSION')) {
// IF STANDALONE
} else {

    include(DE_DF_PATH . '/includes/classes/class.wooslt.php');
    include(DE_DF_PATH . '/includes/classes/class.licence.php');
    include(DE_DF_PATH . '/includes/classes/class.options.php');
    include(DE_DF_PATH . '/includes/classes/class.updater.php');
    
    add_action( 'admin_enqueue_scripts', 'load_divi_engine_style_divi_filter' , 20);
    function load_divi_engine_style_divi_filter() {
        $cssfile = plugins_url( 'styles/divi-engine.css', __FILE__ );
        wp_enqueue_style( 'divi_engine_style', $cssfile , false, DE_DF_VERSION );
    }
    require_once plugin_dir_path( __FILE__ ) . 'includes/daf-settings.php';
    
}


// initialise Divi Modules
if ( !function_exists( 'de_df_initialise_ext' ) ) {
    function de_df_initialise_ext()
    {
        require_once plugin_dir_path( __FILE__ ) . 'includes/DiviFilterModules.php';
        require_once plugin_dir_path( __FILE__ ) . 'includes/loader.php';
        require_once plugin_dir_path( __FILE__ ) . 'includes/ajaxcalls/post_ajax.php';

        if ( defined('DE_DB_WOO_VERSION')) {
            $db_settings = get_option( 'divi-bodyshop-woo_options' );
            $enable_variation_swatches = unserialize( $db_settings );
            if ( isset( $enable_variation_swatches['enable_variation_swatches'][0] ) ) {
                $check_enable_variation_swatches = $enable_variation_swatches['enable_variation_swatches'][0];
            }else { $check_enable_variation_swatches = "1"; }
            if ($check_enable_variation_swatches == "0") {
                require_once dirname( __FILE__ ) .'/lib/variation-swatches.php';
                require_once dirname( __FILE__ ) .'/includes/classes/class-wvs-term-meta.php';
            }
        }else{

            require_once dirname( __FILE__ ) .'/lib/variation-swatches.php';
            require_once dirname( __FILE__ ) .'/includes/classes/class-wvs-term-meta.php';
        }
    }
}

if ( !class_exists( 'DiviExtension' ) ){
    add_action( 'divi_extensions_init', 'de_df_initialise_ext' );
}else{
    de_df_initialise_ext();
}

if ( !function_exists('divi_filter_load_actions_ajax') ) {
    function divi_filter_load_actions_ajax( $actions ) {
        $filter_handler_exist = false;
        $modal_handler_exist = false;
        $loadmore_handler_exist = false;
        $filter_counter_handler_exist = false;

        foreach ($actions as $key => $action) {
          if ( $action == 'divi_filter_ajax_handler' )
            $filter_handler_exist = true;
          if ( $action == 'divi_filter_get_post_modal_ajax_handler' )
            $modal_handler_exist = true;
          if ( $action == 'divi_filter_loadmore_ajax_handler')
            $loadmore_handler_exist = true;
          if ( $action == 'divi_filter_get_count_ajax_handler')
            $filter_counter_handler_exist = true;
        }

        if ( !$filter_handler_exist )
          $actions[] = 'divi_filter_ajax_handler';

        if ( !$modal_handler_exist )
          $actions[] = 'divi_filter_get_post_modal_ajax_handler';

        if ( !$loadmore_handler_exist )
          $actions[] = 'divi_filter_loadmore_ajax_handler';

        if ( !$filter_counter_handler_exist )
          $actions[] = 'divi_filter_get_count_ajax_handler';

        return $actions;
    }

    add_filter( 'et_builder_load_actions', 'divi_filter_load_actions_ajax' );
}

if ( !function_exists('bodycommerce_wpml_currency_ajax_actions')) {
  function bodycommerce_wpml_currency_ajax_actions( $actions ) {
    $actions[] = 'divi_filter_ajax_handler';
    $actions[] = 'divi_filter_get_post_modal_ajax_handler';
    $actions[] = 'divi_filter_loadmore_ajax_handler';

    return $actions;
  }

  add_filter( 'wcml_multi_currency_ajax_actions', 'bodycommerce_wpml_currency_ajax_actions', 10, 1 );
}

if ( !function_exists('divi_filter_enqueue_scripts') ) {
    function divi_filter_enqueue_scripts() {
        wp_enqueue_script( 'divi-filter-rangeSlider-js', plugins_url('/js/ion.rangeSlider.min.js', __FILE__ ), array( 'jquery' ), DE_DF_VERSION, true );
        wp_enqueue_style( 'divi-filter-rangeSlider-css', plugins_url( '/styles/ion.rangeSlider.min.css' , __FILE__ ), array(), DE_DF_VERSION, 'all' );
        //wp_enqueue_style( 'divi-filter-css', plugins_url( '/styles/style.min.css' , __FILE__ ), array(), DE_DF_VERSION, 'all' );
    }
    add_action( 'wp_enqueue_scripts', 'divi_filter_enqueue_scripts' );    
}


/*

register_uninstall_hook( __FILE__, 'df_uninstall_hook' );
register_deactivation_hook( __FILE__, 'df_deactivation_hook' );

function df_uninstall_hook() {
}

function df_deactivation_hook() {
}*/

if ( !function_exists('Divi_filter_remove_get_params')) {
    add_filter( 'query_vars', 'Divi_filter_remove_get_params', 10, 1 );

    function Divi_filter_remove_get_params( $query ){
        global $divi_filter_removed_param;
        if ( !empty( $_GET['filter' ] ) && $_GET['filter' ] == 'true'){
            foreach ($_GET as $key => $param) {
                if ( $key != 'filter'){
                    $divi_filter_removed_param[$key] = $param;
                    unset($_GET[$key]);
                }
            }
        }
        return $query;
  }  
}

if (defined('DE_DB_WOO_VERSION')) {
    // IF MACHINE
    } else if (defined('DE_DMACH_VERSION')) {
    // IF STANDALONE
    } else {
global $DE_DF;
$DE_DF = new DE_DF();
    }
?>
