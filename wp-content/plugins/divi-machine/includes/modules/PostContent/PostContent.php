<?php
if ( ! defined( 'ABSPATH' ) ) exit;

class de_mach_content_code extends ET_Builder_Module {

  public $vb_support = 'on';

  protected $module_credits = array(
    'module_uri' => DE_DMACH_PRODUCT_URL,
    'author'     => DE_DMACH_AUTHOR,
    'author_uri' => DE_DMACH_URL,
  );

  function init() {
    $this->name       = esc_html__( '.Post Content - Divi Machine', 'divi-machine' );
    $this->slug = 'et_pb_de_mach_content';


    $this->fields_defaults = array(
      // 'loop_layout'         => array( 'on' ),
    );

    $this->settings_modal_toggles = array(
      'general' => array(
        'toggles' => array(
          'main_content' => esc_html__( 'Main Options', 'divi-machine' ),
        ),
      ),
      'advanced' => array(
        'toggles' => array(
          'text' => array(
            'title'    => esc_html__( 'Content Text', 'divi-machine' ),
            'priority' => 45,
            'tabbed_subtoggles' => true,
            'bb_icons_support' => true,
            'sub_toggles' => array(
              'p'     => array(
                'name' => 'P',
                'icon' => 'text-left',
              ),
              'a'     => array(
                'name' => 'A',
                'icon' => 'text-link',
              ),
              'ul'    => array(
                'name' => 'UL',
                'icon' => 'list',
              ),
              'ol'    => array(
                'name' => 'OL',
                'icon' => 'numbered-list',
              ),
              'quote' => array(
                'name' => 'QUOTE',
                'icon' => 'text-quote',
              ),
            ),
          ),
        ),
      ),

    );


    $this->main_css_element = '%%order_class%%';


    $this->advanced_fields = array(
      'fonts' => array(
        'text'   => array(
          'label'    => esc_html__( 'Text', 'divi-machine' ),
          'css'      => array(
            'line_height' => "{$this->main_css_element} p",
            'color' => "{$this->main_css_element} p",
          ),
          'line_height' => array(
            'default' => floatval( et_get_option( 'body_font_height', '1.7' ) ) . 'em',
          ),
          'font_size' => array(
            'default' => absint( et_get_option( 'body_font_size', '14' ) ) . 'px',
          ),
          'toggle_slug' => 'text',
          'sub_toggle'  => 'p',
        ),
        'link'   => array(
          'label'    => esc_html__( 'Link', 'divi-machine' ),
          'css'      => array(
            'main' => "{$this->main_css_element} a",
            'color' => "{$this->main_css_element} a",
          ),
          'line_height' => array(
            'default' => '1em',
          ),
          'font_size' => array(
            'default' => absint( et_get_option( 'body_font_size', '14' ) ) . 'px',
          ),
          'toggle_slug' => 'text',
          'sub_toggle'  => 'a',
        ),
        'ul'   => array(
          'label'    => esc_html__( 'Unordered List', 'divi-machine' ),
          'css'      => array(
            'main'        => "{$this->main_css_element} ul",
            'color'       => "{$this->main_css_element} ul",
            'line_height' => "{$this->main_css_element} ul li",
          ),
          'line_height' => array(
            'default' => '1em',
          ),
          'font_size' => array(
            'default' => '14px',
          ),
          'toggle_slug' => 'text',
          'sub_toggle'  => 'ul',
        ),
        'ol'   => array(
          'label'    => esc_html__( 'Ordered List', 'divi-machine' ),
          'css'      => array(
            'main'        => "{$this->main_css_element} ol",
            'color'       => "{$this->main_css_element} ol",
            'line_height' => "{$this->main_css_element} ol li",
          ),
          'line_height' => array(
            'default' => '1em',
          ),
          'font_size' => array(
            'default' => '14px',
          ),
          'toggle_slug' => 'text',
          'sub_toggle'  => 'ol',
        ),
        'quote'   => array(
          'label'    => esc_html__( 'Blockquote', 'divi-machine' ),
          'css'      => array(
            'main' => "{$this->main_css_element} blockquote, {$this->main_css_element} blockquote p",
            'color' => "{$this->main_css_element} blockquote, {$this->main_css_element} blockquote p",
          ),
          'line_height' => array(
            'default' => '1em',
          ),
          'font_size' => array(
            'default' => '14px',
          ),
          'toggle_slug' => 'text',
          'sub_toggle'  => 'quote',
        ),
        'header_1'   => array(
          'label'    => esc_html__( 'Heading', 'divi-machine' ),
          'css'      => array(
            'main' => "{$this->main_css_element} h1",
          ),
          'font_size' => array(
            'default' => absint( et_get_option( 'body_header_size', '30' ) ) . 'px',
          ),
          'line_height' => array(
            'default' => '1em',
          ),
          'toggle_slug' => 'header',
          'sub_toggle'  => 'h1',
        ),
        'header_2'   => array(
          'label'    => esc_html__( 'Heading 2', 'divi-machine' ),
          'css'      => array(
            'main' => "{$this->main_css_element} h2",
          ),
          'font_size' => array(
            'default' => '26px',
          ),
          'line_height' => array(
            'default' => '1em',
          ),
          'toggle_slug' => 'header',
          'sub_toggle'  => 'h2',
        ),
        'header_3'   => array(
          'label'    => esc_html__( 'Heading 3', 'divi-machine' ),
          'css'      => array(
            'main' => "{$this->main_css_element} h3",
          ),
          'font_size' => array(
            'default' => '22px',
          ),
          'line_height' => array(
            'default' => '1em',
          ),
          'toggle_slug' => 'header',
          'sub_toggle'  => 'h3',
        ),
        'header_4'   => array(
          'label'    => esc_html__( 'Heading 4', 'divi-machine' ),
          'css'      => array(
            'main' => "{$this->main_css_element} h4",
          ),
          'font_size' => array(
            'default' => '18px',
          ),
          'line_height' => array(
            'default' => '1em',
          ),
          'toggle_slug' => 'header',
          'sub_toggle'  => 'h4',
        ),
        'header_5'   => array(
          'label'    => esc_html__( 'Heading 5', 'divi-machine' ),
          'css'      => array(
            'main' => "{$this->main_css_element} h5",
          ),
          'font_size' => array(
            'default' => '16px',
          ),
          'line_height' => array(
            'default' => '1em',
          ),
          'toggle_slug' => 'header',
          'sub_toggle'  => 'h5',
        ),
        'header_6'   => array(
          'label'    => esc_html__( 'Heading 6', 'divi-machine' ),
          'css'      => array(
            'main' => "{$this->main_css_element} h6",
          ),
          'font_size' => array(
            'default' => '14px',
          ),
          'line_height' => array(
            'default' => '1em',
          ),
          'toggle_slug' => 'header',
          'sub_toggle'  => 'h6',
        ),
      ),
      'background' => array(
        'settings' => array(
          'color' => 'alpha',
        ),
      ),
      'button' => array(
      ),
      'box_shadow' => array(
        'default' => array(),
        'product' => array(
          'label' => esc_html__( 'Default Layout - Box Shadow', 'divi-machine' ),
          'css' => array(
            'main' => "%%order_class%% .products .product",
          ),
          'option_category' => 'layout',
          'tab_slug'        => 'advanced',
          'toggle_slug'     => 'product',
        ),
      ),
    );


    $this->custom_css_fields = array(
    );


    $this->help_videos = array(
    );
  }

  function get_fields() {

    $fields = array(

      'content_type' => array(
        'option_category' => 'configuration',
        'toggle_slug'     => 'main_content',
        'label'             => esc_html__( 'Content Type', 'divi-machine' ),
        'type'              => 'select',
        'options'           => array(
          'content' => esc_html__( 'Content (what is in the editor)', 'divi-machine' ),
          'excert' => sprintf( esc_html__( 'Excert', 'divi-machine' ) ),
        ),
        'default' => 'content',
        'computed_affects' => array(
          '__getpostcontent',
        ),
        'description'       => esc_html__( 'Choose if you want to show the content or the excert (short description).', 'divi-machine' ),
      ),
      'content_wordcount' => array(
        'option_category' => 'configuration',
        'toggle_slug'     => 'main_content',
        'label'             => esc_html__( 'Content Word Count', 'divi-machine' ),
        'type'              => 'text',
        'computed_affects' => array(
          '__getpostcontent',
        ),
        'description'       => esc_html__( 'Input word count to show on this module.', 'divi-machine' ),
      ),
      'ul_type' => array(
        'label'             => esc_html__( 'Unordered List Style Type', 'divi-machine' ),
        'type'              => 'select',
        'option_category'   => 'configuration',
        'options'           => array(
          'disc'    => esc_html__( 'Disc', 'divi-machine' ),
          'circle'  => esc_html__( 'Circle', 'divi-machine' ),
          'square'  => esc_html__( 'Square', 'divi-machine' ),
          'none'    => esc_html__( 'None', 'divi-machine' ),
        ),
        'priority'          => 80,
        'default'           => 'disc',
        'tab_slug'          => 'advanced',
        'toggle_slug'       => 'text',
        'sub_toggle'        => 'ul',
      ),
      'ul_position' => array(
        'label'             => esc_html__( 'Unordered List Style Position', 'divi-machine' ),
        'type'              => 'select',
        'option_category'   => 'configuration',
        'options'           => array(
          'outside' => esc_html__( 'Outside', 'divi-machine' ),
          'inside'  => esc_html__( 'Inside', 'divi-machine' ),
        ),
        'priority'          => 85,
        'default'           => 'outside',
        'tab_slug'          => 'advanced',
        'toggle_slug'       => 'text',
        'sub_toggle'        => 'ul',
      ),
      'ul_item_indent' => array(
        'label'           => esc_html__( 'Unordered List Item Indent', 'divi-machine' ),
        'type'            => 'range',
        'option_category' => 'configuration',
        'tab_slug'        => 'advanced',
        'toggle_slug'     => 'text',
        'sub_toggle'      => 'ul',
        'priority'        => 90,
        'default'         => '0px',
        'range_settings'  => array(
          'min'  => '0',
          'max'  => '100',
          'step' => '1',
        ),
      ),
      'ol_type' => array(
        'label'             => esc_html__( 'Ordered List Style Type', 'divi-machine' ),
        'type'              => 'select',
        'option_category'   => 'configuration',
        'options'           => array(
          'decimal'              => 'decimal',
          'armenian'             => 'armenian',
          'cjk-ideographic'      => 'cjk-ideographic',
          'decimal-leading-zero' => 'decimal-leading-zero',
          'georgian'             => 'georgian',
          'hebrew'               => 'hebrew',
          'hiragana'             => 'hiragana',
          'hiragana-iroha'       => 'hiragana-iroha',
          'katakana'             => 'katakana',
          'katakana-iroha'       => 'katakana-iroha',
          'lower-alpha'          => 'lower-alpha',
          'lower-greek'          => 'lower-greek',
          'lower-latin'          => 'lower-latin',
          'lower-roman'          => 'lower-roman',
          'upper-alpha'          => 'upper-alpha',
          'upper-greek'          => 'upper-greek',
          'upper-latin'          => 'upper-latin',
          'upper-roman'          => 'upper-roman',
          'none'                 => 'none',
        ),
        'priority'          => 80,
        'default'           => 'decimal',
        'tab_slug'          => 'advanced',
        'toggle_slug'       => 'text',
        'sub_toggle'        => 'ol',
      ),
      'ol_position' => array(
        'label'             => esc_html__( 'Ordered List Style Position', 'divi-machine' ),
        'type'              => 'select',
        'option_category'   => 'configuration',
        'options'           => array(
          'outside' => esc_html__( 'Outside', 'divi-machine' ),
          'inside'  => esc_html__( 'Inside', 'divi-machine' ),
        ),
        'priority'          => 85,
        'default'           => 'outside',
        'tab_slug'          => 'advanced',
        'toggle_slug'       => 'text',
        'sub_toggle'        => 'ol',
      ),
      'ol_item_indent' => array(
        'label'           => esc_html__( 'Ordered List Item Indent', 'divi-machine' ),
        'type'            => 'range',
        'option_category' => 'configuration',
        'tab_slug'        => 'advanced',
        'toggle_slug'     => 'text',
        'sub_toggle'      => 'ol',
        'priority'        => 90,
        'default'         => '0px',
        'range_settings'  => array(
          'min'  => '0',
          'max'  => '100',
          'step' => '1',
        ),
      ),
      'quote_border_weight' => array(
        'label'           => esc_html__( 'Blockquote Border Weight', 'divi-machine' ),
        'type'            => 'range',
        'option_category' => 'configuration',
        'tab_slug'        => 'advanced',
        'toggle_slug'     => 'text',
        'sub_toggle'      => 'quote',
        'priority'        => 85,
        'default'         => '5px',
        'default_unit'    => 'px',
        'default_on_front' => '',
        'range_settings'  => array(
          'min'  => '0',
          'max'  => '100',
          'step' => '1',
        ),
      ),
      'quote_border_color' => array(
        'label'           => esc_html__( 'Blockquote Border Color', 'divi-machine' ),
        'type'            => 'color-alpha',
        'option_category' => 'configuration',
        'custom_color'    => true,
        'tab_slug'        => 'advanced',
        'toggle_slug'     => 'text',
        'sub_toggle'      => 'quote',
        'field_template'  => 'color',
        'priority'        => 90,
      ),
      '__getpostcontent' => array(
        'type' => 'computed',
        'computed_callback' => array( 'de_mach_content_code', 'get_post_content' ),
        'computed_depends_on' => array(
          'content_type',
        ),
      ),
    );

    return $fields;
  }

  public static function get_post_content ( $args = array(), $conditional_tags = array(), $current_page = array() ){
    if (!is_admin()) {
      return;
    }

    ob_start();

    $content_type = $args['content_type'];

    $post_slug = DEDMACH_INIT::get_vb_post_type();
    

    $get_cpt_args = array(
      'post_type' => $post_slug,
      'post_status' => 'publish',
      'posts_per_page' => '1',
      'orderby' => 'ID',
      'order' => 'ASC',
    );

    query_posts( $get_cpt_args );

    $first = true;

    if ( have_posts() ) {
      while ( have_posts() ) {
        the_post();
        // setup_postdata( $post );

        if ( $first )  {

          //////////////////////////////////////////////////

          $is_page_builder_used = et_pb_is_pagebuilder_used( get_the_ID() );
          if( $is_page_builder_used ) {
            $shop = "<div class='no-html-output'><p>We do not have support for when you use the Divi builder in the content yet.</p></div>";
            echo $shop;
          } else {
            if ($content_type == "excert") {
              the_excerpt();
            } else {
              the_content();
            }
          }

          //////////////////////////////////////////////////
          $first = false;
        } else {

        }

      }
    }

    $data = ob_get_clean();

    return $data;

  }

  public function get_search_items_content() {
    return $this->content;
  }

  public function limit_text($text, $limit) {
    if (str_word_count($text, 0) > $limit) {
        $words = str_word_count($text, 2);
        $pos   = array_keys($words);
        $text  = substr($text, 0, $pos[$limit]) . '...';
    }
    return $text;
  }

  function render( $attrs, $content = null, $render_slug ) {

    // if (is_admin()) {
    //     return;
    // }

    $ul_type              = $this->props['ul_type'];
    $ul_position          = $this->props['ul_position'];
    $ul_item_indent       = $this->props['ul_item_indent'];
    $ol_type              = $this->props['ol_type'];
    $ol_position          = $this->props['ol_position'];
    $ol_item_indent       = $this->props['ol_item_indent'];
    $quote_border_weight  = $this->props['quote_border_weight'];
    $quote_border_color   = $this->props['quote_border_color'];
    $content_wordcount    = !empty($this->props['content_wordcount'])?(int)$this->props['content_wordcount']:0;

    $content_type         = $this->props['content_type'];

    //////////////////////////////////////////////////////////////////////

    ob_start();

    if ($content_type == "excert") {
      the_excerpt();
    } else {
      the_content();
    }

    $data = ob_get_clean();

    if ( $content_wordcount != 0 ){
      $data = $this->limit_text( $data, $content_wordcount );
    }



    //////////////////////////////////////////////////////////////////////

    if ( '' !== $ul_type || '' !== $ul_position || '' !== $ul_item_indent ) {
      ET_Builder_Element::set_style( $render_slug, array(
        'selector'    => '%%order_class%% ul',
        'declaration' => sprintf(
          '%1$s
          %2$s
          %3$s',
          '' !== $ul_type ? sprintf( 'list-style-type: %1$s !important;', esc_html( $ul_type ) ) : '',
          '' !== $ul_position ? sprintf( 'list-style-position: %1$s !important;', esc_html( $ul_position ) ) : '',
          '' !== $ul_item_indent ? sprintf( 'padding-left: %1$s !important;', esc_html( $ul_item_indent ) ) : ''
        ),
      ) );
    }

    if ( '' !== $ol_type || '' !== $ol_position || '' !== $ol_item_indent ) {
      ET_Builder_Element::set_style( $render_slug, array(
        'selector'    => '%%order_class%% ol',
        'declaration' => sprintf(
          '%1$s
          %2$s
          %3$s',
          '' !== $ol_type ? sprintf( 'list-style-type: %1$s !important;', esc_html( $ol_type ) ) : '',
          '' !== $ol_position ? sprintf( 'list-style-position: %1$s !important;', esc_html( $ol_position ) ) : '',
          '' !== $ol_item_indent ? sprintf( 'padding-left: %1$s !important;', esc_html( $ol_item_indent ) ) : ''
        ),
      ) );
    }

    if ( '' !== $quote_border_weight || '' !== $quote_border_color ) {
      ET_Builder_Element::set_style( $render_slug, array(
        'selector'    => '%%order_class%% blockquote',
        'declaration' => sprintf(
          '%1$s
          %2$s',
          '' !== $quote_border_weight ? sprintf( 'border-width: %1$s;', esc_html( $quote_border_weight ) ) : '',
          '' !== $quote_border_color ? sprintf( 'border-color: %1$s;', esc_html( $quote_border_color ) ) : ''
        ),
      ) );
    }

    return $data;



  }

}

new de_mach_content_code;

?>
