<?php
if ( ! defined( 'ABSPATH' ) ) exit;

class de_mach_cat_loop_code extends ET_Builder_Module {

public $vb_support = 'on';

protected $module_credits = array(
  'module_uri' => DE_DMACH_PRODUCT_URL,
  'author'     => DE_DMACH_AUTHOR,
  'author_uri' => DE_DMACH_URL,
);

                function init() {
                    $this->name       = esc_html__( '.Category Loop - Divi Machine', 'divi-machine' );
                    $this->slug = 'et_pb_de_mach_cat_loop';

                    $this->fields_defaults = array(
                    'loop_layout'         => array( 'none' ),
                    'fullwidth'         => array( 'on' ),
                    'columns'         => array( '3' ),
                    'show_pagination'   => array( 'on' ),
                    'cat_order'   => array( '1' ),
                    'hide_empty'   => array( 'on' ),
                    'column_style'   => array( 'text-ontop' ),
                    );

          $this->settings_modal_toggles = array(
      			'general' => array(
      				'toggles' => array(
      					'main_content' => esc_html__( 'Module Options', 'divi-machine' ),
                'loop_options' => esc_html__( 'Loop Options', 'divi-machine' ),
                'element_options' => esc_html__( 'Element Options', 'divi-machine' ),
                'grid_options' => esc_html__( 'Grid Options', 'divi-machine' ),
      				),
      			),
            'advanced' => array(
              'toggles' => array(
                'overlay' => esc_html__( 'Overlay', 'divi-machine' ),
                'image'   => esc_html__( 'Image', 'divi-machine' ),
              ),
            ),

      		);


                      $this->main_css_element = '%%order_class%%';
                      $this->advanced_fields = array(
        			'fonts' => array(
                'text'   => array(
                                'label'    => esc_html__( 'Title', 'divi-machine' ),
                                'css'      => array(
                                        'main' => "{$this->main_css_element} h3, .woocommerce {$this->main_css_element} ul.products li.product h3, .woocommerce-page ul.products li.product h3, .woocommerce-page.et-db #et-boc .et-l {$this->main_css_element} ul.products li.product h3",
                                					'important' => 'all',
                                ),
                                'font_size' => array('default' => '14px'),
                                'line_height'    => array('default' => '1.5em'),
                ),
        			),
        			'background' => array(
        				'settings' => array(
        					'color' => 'alpha',
        				),
        			),
        			'border' => array(),
        			'custom_margin_padding' => array(
        				'css' => array(
        					'important' => 'all',
        				),
        			),
        		);

            $this->help_videos = array(
 
            );
          }

                  function get_fields() {

                    $acf_fields = DEDMACH_INIT::get_acf_fields();

                      $fields = array(


                        'post_type_define' => array(
                        'label'             => esc_html__( 'Return Type', 'divi-machine' ),
                        'type'              => 'select',
                        'option_category'   => 'layout',
                        'options'           => array(
                        'default'  => esc_html__( 'Current Post', 'divi-machine' ),
                        'custom' => esc_html__( 'Custom (you define)', 'divi-machine' ),
                        ),
                        'affects'         => array(
                          'post_type_choose',
                        ),
                        'description'        => esc_html__( 'Choose if you want to use the current post or define a custom one', 'divi-machine' ),
                        'toggle_slug'       => 'loop_options',
                        ),
                          'post_type_choose' => array(
                            'toggle_slug'       => 'loop_options',
                            'label'             => esc_html__( 'Post Type', 'divi-machine' ),
                            'type'              => 'select',
                            'options'           => et_get_registered_post_type_options( false, false ),
                            'option_category'   => 'layout',
                            'default'           => 'post',
                            'depends_show_if' => 'custom',
                            'description'       => esc_html__( 'Choose the post type you want to display', 'divi-machine' ),
                          ),
                        'acf_name' => array(
                        'toggle_slug'       => 'element_options',
                          'label'             => esc_html__( 'Category Image Name', 'divi-machine' ),
                          'type'              => 'select',
                          'options'           => $acf_fields,
                          'option_category'   => 'layout',
                          'default'           => 'none',
                  				'computed_affects' => array(
                  					'__getcategoryarchive',
                  				),
                          'description'       => esc_html__( 'If you want to display an image in the category loop, specify the image acf item that you have added to the post category. This image is a ACF field that you add on the category settings page', 'divi-machine' ),
                        ),
                        'return_format' => array(
                          'toggle_slug'     => 'element_options',
                          'label'             => esc_html__('Image Return Format', 'divi-machine'),
                          'type'              => 'select',
                          'options'           => array(
                            'array' => esc_html__('Array', 'divi-machine'),
                            'url' => sprintf(esc_html__('URL', 'divi-machine')),
                          ),
                  				'computed_affects' => array(
                  					'__getcategoryarchive',
                  				),
                          'default' => 'array',
                          'description'       => esc_html__('Choose how you have defined the return format in ACF settings.', 'divi-machine'),
                        ),
                          'column_style' => array(
                        'label'             => esc_html__( 'Column Style', 'divi-machine' ),
                        'type'              => 'select',
                        // 'computed_affects' => array(
                        //   '__getcategoryarchive',
                        // ),
                        'option_category'   => 'layout',
                        'options'           => array(
                        'text-ontop'  => esc_html__( 'Title On Top Image', 'divi-machine' ),
                        'text-below' => esc_html__( 'Title Below Image', 'divi-machine' ),
                        'image-left' => esc_html__( 'Image on left of Title', 'divi-machine' ),
                        ),
                        'depends_show_if' => 'off',
                        'default'         => 'text-ontop',
                        'description'        => esc_html__( 'Choose the style of the grid layout', 'divi-machine' ),
                        'toggle_slug'       => 'element_options',
                        ),
                        'columns' => array(
                        'label'             => esc_html__( 'Grid Columns', 'divi-machine' ),
                        'type'              => 'select',
                        'option_category'   => 'layout',
                        'options'           => array(
                        2  => esc_html__( 'Two', 'divi-machine' ),
                        3 => esc_html__( 'Three', 'divi-machine' ),
                        4 => esc_html__( 'Four', 'divi-machine' ),
                        5 => esc_html__( 'Five', 'divi-machine' ),
                        6 => esc_html__( 'Six', 'divi-machine' ),
                        ),
                        'description'        => esc_html__( 'How many columns do you want to see', 'divi-machine' ),
                        'computed_affects' => array(
                          '__getcategoryarchive',
                        ),
                        'toggle_slug'       => 'grid_options',
                        ),
                        'columns_tablet' => array(
                        'label'             => esc_html__( 'Tablet Grid Columns', 'divi-machine' ),
                        'type'              => 'select',
                        'option_category'   => 'layout',
                        'toggle_slug'       => 'grid_options',
                        'default'   => '2',
                        'options'           => array(
                        1  => esc_html__( 'One', 'divi-machine' ),
                        2  => esc_html__( 'Two', 'divi-machine' ),
                        3 => esc_html__( 'Three', 'divi-machine' ),
                        4 => esc_html__( 'Four', 'divi-machine' ),
                        ),
                        'computed_affects' => array(
                          '__getcategoryarchive',
                        ),
                        'description'        => esc_html__( 'How many columns do you want to see on tablet', 'divi-machine' ),
                        ),
                        'columns_mobile' => array(
                        'label'             => esc_html__( 'Mobile Grid Columns', 'divi-machine' ),
                        'type'              => 'select',
                        'option_category'   => 'layout',
                        'toggle_slug'       => 'grid_options',
                        'default'   => '1',
                        'options'           => array(
                        1  => esc_html__( 'One', 'divi-machine' ),
                        2  => esc_html__( 'Two', 'divi-machine' ),
                        ),
                        'computed_affects' => array(
                          '__getcategoryarchive',
                        ),
                        'description'        => esc_html__( 'How many columns do you want to see on mobile', 'divi-machine' ),
                        ),
                        'text_orientation' => array(
                                        'label'             => esc_html__( 'Text Orientation', 'divi-machine' ),
                                        'type'              => 'select',
                                        'option_category'   => 'layout',
                                        'options'           => et_builder_get_text_orientation_options(),
                                        'description'       => esc_html__( 'This controls the how your text is aligned within the module.', 'divi-machine' ),
                                        'toggle_slug'       => 'element_options',
                        ),
                        'hide_description' => array(
                        'label'             => esc_html__( 'Hide Descriptions', 'divi-machine' ),
                        'type'              => 'select',
                        'options'           => array(
                        "off"  => esc_html__( 'No', 'divi-machine' ),
                        "on" => esc_html__( 'Yes', 'divi-machine' ),
                        ),
                        'description'        => esc_html__( 'If you want to only show the title and image and hide the descriptions, check this.', 'divi-machine' ),
                        'toggle_slug'       => 'element_options',
                        ),
                        'cat_order' => array(
                        'label'             => esc_html__( 'Category Orderby', 'divi-machine' ),
                        'type'              => 'select',
                				'computed_affects' => array(
                					'__getcategoryarchive',
                				),
                        'options'           => array(
                        "1"  => esc_html__( 'Name', 'divi-machine' ),
                        "2" => esc_html__( 'Category Order', 'divi-machine' ),
                        ),
                        'description'        => esc_html__( 'Select how you want the categories ordered.', 'divi-machine' ),
                        'toggle_slug'       => 'loop_options',
                        ),

                                                'hide_empty' => array(
                                                  'label'             => esc_html__( 'Hide Empty Categories?', 'divi-machine' ),
                                                  'type'              => 'yes_no_button',
                                                  'option_category'   => 'layout',
                                                  'options'           => array(
                                                    'on'  => esc_html__( 'Yes', 'divi-machine' ),
                                                    'off' => esc_html__( 'No', 'divi-machine' ),
                                                  ),
                                          				'computed_affects' => array(
                                          					'__getcategoryarchive',
                                          				),
                                          				'description' => esc_html__( 'If you want to hide empty categories, enable this.', 'divi-machine' ),
                                                  'toggle_slug'       => 'loop_options',
                                                ),
                                                'show_all' => array(
                                                  'label'             => esc_html__( 'Show Only Child Categories?', 'divi-machine' ),
                                                  'type'              => 'yes_no_button',
                                                  'option_category'   => 'layout',
                                                  'options'           => array(
                                                    'on'  => esc_html__( 'Yes', 'divi-machine' ),
                                                    'off' => esc_html__( 'No', 'divi-machine' ),
                                                  ),
                                                  'default'           => 'off',
                                          				'computed_affects' => array(
                                          					'__getcategoryarchive',
                                          				),
                                          				'description' => esc_html__( 'If you want to show only the child categories or if on the archive page, show only the main categories - enable this.', 'divi-machine' ),
                                                  'toggle_slug'       => 'loop_options',
                                                ),
                                                'exclude_cats' => array(
                                                  'toggle_slug'       => 'loop_options',
                                                  'label'           => esc_html__( 'Exclude Categories (ID)(comma-seperated)', 'divi-machine' ),
                                                  'type'            => 'text',
                                                  'description'     => esc_html__( 'If you want to exclude some categories, add the IDs here. (comma-seperated)', 'divi-machine' ),
                                                  'computed_affects' => array(
                                                    '__getcategoryarchive',
                                                  ),
                                                ),

                                                
                        'zoom_icon_color' => array(
                          'label'             => esc_html__( 'Zoom Icon Color', 'divi-machine' ),
                          'type'              => 'color-alpha',
                          'custom_color'      => true,
                          'depends_show_if'   => 'off',
                          'tab_slug'          => 'advanced',
                          'toggle_slug'       => 'overlay',
                        ),
                        'hover_overlay_color' => array(
                          'label'             => esc_html__( 'Hover Overlay Color', 'divi-machine' ),
                          'type'              => 'color-alpha',
                          'custom_color'      => true,
                          'depends_show_if'   => 'off',
                          'tab_slug'          => 'advanced',
                          'toggle_slug'       => 'overlay',
                        ),
                        'hover_icon' => array(
                          'label'               => esc_html__( 'Hover Icon Picker', 'divi-machine' ),
                          'type'                => 'text',
                          'option_category'     => 'configuration',
                          'class'               => array( 'et-pb-font-icon' ),
                          'renderer'            => 'select_icon',
                          'renderer_with_field' => true,
                          'depends_show_if'     => 'off',
                          'tab_slug'          => 'advanced',
                          'toggle_slug'       => 'overlay',
                        ),
                        '__getcategoryarchive' => array(
                          'type' => 'computed',
                          'computed_callback' => array( 'de_mach_cat_loop_code', 'get_cat_archive' ),
                          'computed_depends_on' => array(
                          'cat_order',
                          'columns',
                          'columns_tablet',
                          'columns_mobile',
                          'acf_name',
                          'hide_empty',
                          'exclude_cats',
                          'return_format'
                          ),
                        ),

                      );

                      return $fields;
                  }


                  public static function get_cat_archive ( $args = array(), $conditional_tags = array(), $current_page = array() ){

                    ob_start();

                    $post_slug = DEDMACH_INIT::get_vb_post_type();
                    
                    $cat_order = $args['cat_order'];
                    $columns = $args['columns'];
                    $columns_tablet = $args['columns_tablet'];
                    $columns_mobile = $args['columns_mobile'];
                    $acf_name = $args['acf_name'];
                    $hide_empty = $args['hide_empty'];
                    $exclude_cats          = $args['exclude_cats'];
                    $return_format          = $args['return_format'];

                    $ending = "_category";
                    $cat_key = $post_slug . $ending;

                    if ($hide_empty == "off") {
                      $hide_empty = false;
                    } else {
                      $hide_empty = true;
                    }

                    if ($exclude_cats != "") {
                      $exclude_cats = $exclude_cats;
                    } else {
                      $exclude_cats = array();
                    }

                    if ($cat_order == 1) { // NAME
                      $terms = get_terms(
                        $cat_key, 
                        array(
                          'hide_empty' => $hide_empty, 
                          'orderby' => 'ASC', 
                          'exclude' => $exclude_cats,
                          'parent' => 0
                      )
                    );
                      }
                      else if ($cat_order == 2) { // CAT ORDER
                        $terms = get_terms(
                          $cat_key, 
                          array(
                            'hide_empty' => $hide_empty, 
                            'menu_order' => 'ASC', 
                            'exclude' => $exclude_cats,
                            'parent' => 0
                        )
                      );
                      }
                      else {
                        $terms = get_terms(
                          $cat_key, 
                          array(
                            'hide_empty' => $hide_empty, 
                            'orderby' => 'ASC', 
                            'exclude' => $exclude_cats,
                            'parent' => 0
                        )
                      );
                      }

                    if ( $terms != "0" && !is_wp_error($terms) ) {

                      ?>
                      <div class="et_pb_de_mach_archive_loop">
                      <div class="category-loop col-desk-<?php echo $columns?> col-tab-<?php echo $columns_tablet?> col-mob-<?php echo $columns_mobile?>">
                        <div class="grid-posts">
                        <?php
                        foreach ( $terms as $term ){
                          $category_id = $term->term_id;
                          $category_name = $term->name;
                          $category_slug = $term->slug;
                          $category_description = $term->description;

                          ?>
                          <div class="grid-col">
                            <?php

                        ///////////

                        $category_id = $term->term_id;
                        echo '<a href="'. get_term_link($category_slug, $cat_key) .'">
                        <h3 class="title-top">'. $category_name .'</h3>';


                        $thumbnail_id = get_term_meta( $term->term_id, 'thumbnail_id', true );

                        $term_id = $term->term_id;
                        $image = get_field( $acf_name, $cat_key . "_" . $term_id );
                        if ( $image ) {
                          if ($return_format == "array") {
                          ?>
                          <span class="et_portfolio_image">
                            <img src="<?php echo $image["url"]; ?>" alt="<?php echo $category_name; ?>" />
                          <?php echo $overlay; ?>
                          </span>
                          <?php
                          }  else {
                            ?>
                            <span class="et_portfolio_image">
                              <img src="<?php echo $image; ?>" alt="<?php echo $category_name; ?>" />
                            <?php echo $overlay; ?>
                            </span>
                            <?php
                          }
                        }

                        echo '
                        <h3 class="title-bottom">'. $category_name .'</h3>
                        </a>';

                        echo  $category_description;


                          ?>
                              </div>
                              <?php

                          }

                              ?>

                  </div>
                      </div>
                      </div>
                      <?php

                    }


                    $data = ob_get_clean();

                  return $data;

                  }



                  function render( $attrs, $content = null, $render_slug ) {

                    $background_layout = '';
                    $columns                   = $this->props['columns'];
                    $columns_tablet           = $this->props['columns_tablet'];
                    $columns_mobile          = $this->props['columns_mobile'];
                    $column_style           = $this->props['column_style'];
                    $text_orientation       = $this->props['text_orientation'];
                    $hide_description       = $this->props['hide_description'];
                    $cat_order              = $this->props['cat_order'];

                		$zoom_icon_color     = $this->props['zoom_icon_color'];
                		$hover_overlay_color = $this->props['hover_overlay_color'];
                		$hover_icon          = $this->props['hover_icon'];
                		$acf_name          = $this->props['acf_name'];
                    $hide_empty          = $this->props['hide_empty'];

                    $post_type_define         = $this->props['post_type_define'];
                    $post_type_choose          = $this->props['post_type_choose'];

                    $exclude_cats          = $this->props['exclude_cats'];
                    $return_format          = $this->props['return_format'];
                    
                    $show_all          = $this->props['show_all'];

                  //////////////////////////////////////////////////////////////////////


                                    if( is_admin() ){
                                      return;
                                    }

                                    ob_start();


                                                    global $paged;


                                                    $container_is_closed = false;

                                                    $overlay = "";
                                                    // Set inline style
                                                    if ( '' !== $zoom_icon_color ) {
                                                      ET_Builder_Element::set_style( $render_slug, array(
                                                        'selector'    => '%%order_class%% .et_overlay:before',
                                                        'declaration' => sprintf(
                                                          'color: %1$s !important;',
                                                          esc_html( $zoom_icon_color )
                                                        ),
                                                      ) );


                                                    }

                                                    if ( '' !== $hover_overlay_color ) {
                                                      ET_Builder_Element::set_style( $render_slug, array(
                                                        'selector'    => '%%order_class%% .et_overlay',
                                                        'declaration' => sprintf(
                                                          'background-color: %1$s;
                                                          border-color: %1$s;',
                                                          esc_html( $hover_overlay_color )
                                                        ),
                                                      ) );
                                                    }



                                                    if ( is_rtl() && 'left' === $text_orientation ) {
                                                                    $text_orientation = 'right';
                                                    }




                                            global $post, $wp_query;

                                            if ($post_type_define == "custom") {
                                              $current_post_type = $post_type_choose;
                                            } else {
                                            $current_post_type = get_post_type( get_the_ID() );
                                          }
                                            if ($current_post_type == "") {
                                              $current_post_type = $wp_query->query["post_type"];
                                            }

                                            if ($current_post_type == "post") {
                                              $cat_key = "category";
                                            } else {
                                            $ending = "_category";
                                            $cat_key = $current_post_type . $ending;
                                            }

                                            if ($hide_empty == "off") {
                                              $hide_empty = false;
                                            } else {
                                              $hide_empty = true;
                                            }

                                            if ($exclude_cats != "") {
                                              $exclude_cats = $exclude_cats;
                                            } else {
                                              $exclude_cats = array();
                                            }

                                            $get_terms_array = array();

                                            $get_current_id = get_queried_object()->term_id;
                                            $get_parent_only = 0;

                                            if (isset(get_queried_object()->term_id)){
                                              $get_parent = get_queried_object()->term_id;
                                            } else {
                                              $get_parent = '0';
                                            }

                                            if ($show_all == "off") {
                                              $get_parent = '';
                                            } else {
                                              $get_terms_array['parent'] = $get_parent;
                                            }

                                            $get_terms_array['hide_empty'] = $hide_empty;
                                            $get_terms_array['exclude'] = $exclude;
                                            $get_terms_array['orderby'] = 'ASC';

                                            $terms = get_terms(
                                              $cat_key, 
                                              $get_terms_array
                                          );

                                            ///////////////// CATEGORY

                                            if ( $terms != "0" && !is_wp_error($terms) ) {

                                            $shortcodes = '';

                                            $i = 0;

                                            ?>
                                            <div class="et_pb_de_mach_archive_loop">
                                            <div class="category-loop col-desk-<?php echo $columns?> col-tab-<?php echo $columns_tablet?> col-mob-<?php echo $columns_mobile?>">
                                                <div class="grid-posts">
                                            <?php
                                            foreach ( $terms as $term ){
                                              $category_id = $term->term_id;
                                              $category_name = $term->name;
                                              $category_slug = $term->slug;
                                              $category_description = $term->description;

                                              ?>
                                              <div class="grid-col dmach-grid-item">
                                                <?php

                                            ///////////

                                            if ($column_style == "text-ontop"){

                                            $category_id = $term->term_id;
                                            echo '<a href="'. get_term_link($category_slug, $cat_key) .'"><h3>'. $category_name .'</h3>';


                                            $thumbnail_id = get_term_meta( $term->term_id, 'thumbnail_id', true );

                                            $term_id = $term->term_id;
                                            $image = get_field( $acf_name, $cat_key . "_" . $term_id );
                                            if ( $image ) {
                                              if ($return_format == "array") {
                                              ?>
                                              <span class="et_portfolio_image">
                                                <img src="<?php echo $image["url"]; ?>" alt="<?php echo $category_name; ?>" />
                                              <?php echo $overlay; ?>
                                              </span>
                                              <?php
                                              }  else {
                                                ?>
                                                <span class="et_portfolio_image">
                                                  <img src="<?php echo $image; ?>" alt="<?php echo $category_name; ?>" />
                                                <?php echo $overlay; ?>
                                                </span>
                                                <?php
                                              }
                                            }

                                            echo '</a>';
                                            if ($hide_description == 'on') {
                                            }
                                            else {
                                            echo  $category_description;
                                            }

                                            }
                                            else if ($column_style == "text-below") {



                                            echo '<a href="'. get_term_link($category_slug, $cat_key) .'">';
                                            $thumbnail_id = get_term_meta( $term->term_id, 'thumbnail_id', true );
                                            $term_id = $term->term_id;
                                            $image = get_field( $acf_name, $cat_key . "_" . $term_id );
                                            if ( $image ) {
                                              ?>
                                              <span class="et_portfolio_image">
                                                <img src="<?php echo $image["url"]; ?>" alt="<?php echo $category_name; ?>" />
                                              <?php echo $overlay; ?>
                                              </span>
                                              <?php
                                            }

                                            echo '<h3>'. $category_name .'</h3></a>';
                                            if ($hide_description == 'on') {
                                            }
                                            else {
                                            echo  $category_description;
                                            }



                                            } else if ($column_style == "image-left") {


                                              echo '<div class="cat_loop_image_left">';
                                              $thumbnail_id = get_term_meta( $term->term_id, 'thumbnail_id', true );
                                              $term_id = $term->term_id;
                                              $image = get_field( $acf_name, $cat_key . "_" . $term_id );
                                              if ( $image ) {
                                                echo '<div class="category_loop_image">';
                                                echo '<a href="'. get_term_link($category_slug, $cat_key) .'">';
                                                ?>
                                                <span class="et_portfolio_image">
                                                  <img src="<?php echo $image["url"]; ?>" alt="<?php echo $category_name; ?>" />
                                                <?php echo $overlay; ?>
                                                </span>
                                                </a>
                                                </div>
                                                <?php
                                              }
  
                                              echo '<div class="category_loop_content">';
                                              echo '<a href="'. get_term_link($category_slug, $cat_key) .'">';
                                              echo '<h3>'. $category_name .'</h3></a>';
                                              if ($hide_description == 'on') {
                                              }
                                              else {
                                              echo  $category_description;
                                              }
                                              echo '</div>';

                                              
                                              echo '</div>';


                                            }


                                          /////////////////
                                                    ?>
                                                  </div>
                                                  <?php

                                                                            }


                                                                            ?>
                                                                          </div>
                                                                          </div>
                                                                        </div>
                                                                        <?php



                                            wp_reset_query();
                                            } else {
                                            }

                                            ////////////// CATEGORY




                                                //////
                                                    $data = ob_get_clean();

                                   //////////////////////////////////////////////////////////////////////

                                return $data;
                  }
              }

            new de_mach_cat_loop_code;