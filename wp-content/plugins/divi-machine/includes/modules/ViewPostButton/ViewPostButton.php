<?php
if ( ! defined( 'ABSPATH' ) ) exit;

class de_mach_view_button_code extends ET_Builder_Module {

    public static $button_text, $p;
    public static function change_button_text( $btn_text ){
        if( !empty( self::$button_text ) ){
            $btn_text = esc_html__( self::$button_text );
        }
        return $btn_text;
    }

    public $vb_support = 'on';

    protected $module_credits = array(
        'module_uri' => DE_DMACH_PRODUCT_URL,
        'author'     => DE_DMACH_AUTHOR,
        'author_uri' => DE_DMACH_URL,
    );

    function init() {
        $this->name       = esc_html__( '.View Post Btn - Divi Machine', 'divi-machine' );
        $this->slug = 'et_pb_de_mach_view_button';

        $this->settings_modal_toggles = array(
            'general' => array(
                'toggles' => array(),
            ),
        );

        $this->main_css_element = '%%order_class%%';
        $this->fields_defaults = array();



        $this->advanced_fields = array(
            'fonts' => array(
                'text' => array(
                    'label'    => esc_html__( 'Button', 'divi-machine' ),
                    'css'      => array(
                        'main' => "%%order_class%% .et_pb_module_inner",
                        'important' => 'plugin_only',
                    ),
                    'font_size' => array(
                        'default' => '14px',
                    ),
                    'line_height' => array(
                        'default' => '1em',
                    ),
                ),
            ),
            'button' => array(
                'button' => array(
                    'label' => esc_html__( 'Button', 'divi-machine' ),
                    'css' => array(
                        'main' => "{$this->main_css_element} .et_pb_button",
                        'important' => 'all',
                    ),
                    'box_shadow'  => array(
                        'css' => array(
                            'main' => "{$this->main_css_element} .et_pb_button",
                            'important' => 'all',
                        ),
                    ),
                    'margin_padding' => array(
                        'css'           => array(
                            'main' => "{$this->main_css_element} .et_pb_button",
                            'important' => 'all',
                        ),
                    ),
                ),
            ),
            'background' => array(
                'settings' => array(
                    'color' => 'alpha',
                ),
            ),
            'border' => array(),
            'custom_margin_padding' => array(
                'css' => array(
                    'important' => 'all',
                ),
            ),
        );

        $this->custom_css_fields = array();
    }
    

    function get_fields() {

        
    $acf_fields = DEDMACH_INIT::get_acf_fields();

        $options = array();

        $layout_query = array(
            'post_type'=>'et_pb_layout',
            'posts_per_page'=>-1,
            'meta_query' => array(
                array(
                    'key' => '_et_pb_predefined_layout',
                    'compare' => 'NOT EXISTS',
                ),
            )
        );

        $et_accent_color = et_builder_accent_color();

        $options['none'] = 'No Layout (please choose one)';
        if ($layouts = get_posts($layout_query)) {
            foreach ($layouts as $layout) {
                $options[$layout->ID] = $layout->post_title;
            }
        }

        $fields = array(
            'title' => array(
                'label'           => esc_html__( 'Button Text', 'divi-machine' ),
                'type'            => 'text',
                'option_category' => 'basic_option',
                'default'         => 'View Post',
                'toggle_slug'       => 'main_content',
                'description'     => esc_html__( 'Input your desired button text.', 'divi-machine' ),
            ),
            'custom_url' => array(
                'label'       => __( 'Custom URL End', 'divi-machine' ),
                'type'        => 'text',
                'toggle_slug'       => 'main_content',
                'option_category' => 'basic_option',
                'description' => __( 'If you want to add an extension after the URL such as an anchor link - add it here. For example add #buynow to go to a section on the product page that has the ID "buynow".', 'et_builder' ),
            ),
            'fullwidth_btn' => array(
                'toggle_slug'       => 'main_content',
                'label' => esc_html__( 'Fullwidth Button?', 'divi-machine' ),
                'type' => 'yes_no_button',
                'option_category' => 'basic_option',
                'options' => array(
                    'on' => esc_html__( 'Yes', 'divi-machine' ),
                    'off' => esc_html__( 'No', 'divi-machine' ),
                ),
                'default' => 'on',
                'description' => esc_html__( 'If you want to make your button fullwdith of the available space, enable this.', 'divi-machine' )
            ),
            'new_tab' => array(
                'toggle_slug'       => 'main_content',
                'label' => esc_html__( 'Open in New Tab?', 'divi-machine' ),
                'type' => 'yes_no_button',
                'option_category' => 'basic_option',
                'options' => array(
                    'on' => esc_html__( 'Yes', 'divi-machine' ),
                    'off' => esc_html__( 'No', 'divi-machine' ),
                ),
                'default' => 'off',
                'description' => esc_html__( 'Enable this if you want to open in a new tab.', 'divi-machine' )
            ),
            'open_external' => array(
                'toggle_slug'       => 'main_content',
                'label' => esc_html__( 'Open to External Site?', 'divi-machine' ),
                'type' => 'yes_no_button',
                'option_category' => 'basic_option',
                'options' => array(
                    'on' => esc_html__( 'Yes', 'divi-machine' ),
                    'off' => esc_html__( 'No', 'divi-machine' ),
                ),
                'default' => 'off',
                'affects'         => array(
                    'external_acf',
                    'showin_modal'
                ),
                'description' => esc_html__( 'If you want the button to link to an external site and not to the single page, enable this (then specify the ACF field next).', 'divi-machine' )
            ),
            'external_acf' => array(
              'toggle_slug'       => 'main_content',
              'label'             => esc_html__('External ACF Text Field', 'divi-machine'),
              'type'              => 'select',
              'options'           => $acf_fields,
              'default'           => 'none',
              'option_category'   => 'configuration',
              'depends_show_if' => 'on',
              'description'       => esc_html__('Choose the ACF you want to use to tell the system where to link to', 'divi-machine'),
            ),
            'showin_modal' => array(
                'toggle_slug'       => 'main_content',
                'label' => esc_html__( 'Show Post in Modal?', 'divi-machine' ),
                'type' => 'yes_no_button',
                'option_category' => 'basic_option',
                'options' => array(
                    'on' => esc_html__( 'Yes', 'divi-machine' ),
                    'off' => esc_html__( 'No', 'divi-machine' ),
                ),
                'depends_show_if' => 'off',
                'default' => 'off',
                'affects'         => array(
                    'modal_layout',
                    'modal_overlay_color',
                    'modal_close_icon',
                    'modal_close_icon_color',
                    'modal_close_icon_size',
                    'prev_next_option',
                    'modal_style'
                ),
                'description' => esc_html__( 'If you want to show post in modal instead to go post page, enable this.', 'divi-machine' )
            ),
            'modal_layout' => array(
                'toggle_slug'       => 'main_content',
                'label' => esc_html__( 'Modal Loop layout', 'divi-machine' ),
                'type' => 'select',
                'option_category' => 'basic_option',
                'options' => $options,
                'default' => 'none',
                'depends_show_if' => 'on',
                'description' => esc_html__( 'Select Loop Layout to show post in modal.', 'divi-machine' )
            ),
            'modal_style' => array(
                'toggle_slug'       => 'main_content',
                'label' => esc_html__( 'Modal Style', 'divi-machine' ),
                'type' => 'select',
                'option_category' => 'basic_option',
                'options' => array(
                    'center-modal'       => esc_html__( 'Center', 'divi-machine' ),
                    'side-modal'       => esc_html__( 'Side', 'divi-machine' ),
                ),
                'default' => 'center-modal',
                'depends_show_if' => 'on',
                'description' => esc_html__( 'Select the modal style.', 'divi-machine' )
            ),
            'modal_overlay_color' => array(
                'toggle_slug'       => 'main_content',
                'label' => esc_html__( 'Modal Overlay Background', 'divi-machine' ),
                'type' => 'color',
                'option_category' => 'basic_option',
                'default' => 'rgba(0,0,0,0.5)',
                'depends_show_if' => 'on',
                'description' => esc_html__( 'Select background color of modal overlay.', 'divi-machine' )
            ),
            'modal_close_icon' => array(
                'toggle_slug'       => 'main_content',
                'label' => esc_html__( 'Modal Close Icon', 'divi-machine' ),
                'type' => 'select_icon',
                'option_category' => 'basic_option',
                'class' => array('et-pb-font-icon'),
                'mobile_options'      => true,
                'default'           => '%%44%%',
                'depends_show_if' => 'on',
                'description' => esc_html__( 'Choose an icon for modal close icon.', 'divi-machine' )
            ),
            'modal_close_icon_color' => array(
                'default'           => $et_accent_color,
                'label'             => esc_html__('Modal Close Icon Color', 'et_builder'),
                'type'              => 'color-alpha',
                'description'       => esc_html__('Here you can define a custom color for modal close icon.', 'divi-machine'),
                'depends_show_if'   => 'on',
                'toggle_slug'       => 'main_content',
                'option_category'   => 'basic_option',
                'mobile_options'    => true,
            ),
            'modal_close_icon_size' => array(
                'label'            => esc_html__( 'Modal Close Icon Size', 'et_builder' ),
                'description'      => esc_html__( 'Control the size of the icon by increasing or decreasing the font size.', 'divi-machine' ),
                'type'             => 'range',
                'option_category'  => 'basic_option',
                'toggle_slug'      => 'main_content',
                'default'          => '46px',
                'default_unit'     => 'px',
                'default_on_front' => '',
                'allowed_units'    => array( '%', 'em', 'rem', 'px', 'cm', 'mm', 'in', 'pt', 'pc', 'ex', 'vh', 'vw' ),
                'range_settings'   => array(
                    'min'  => '1',
                    'max'  => '500',
                    'step' => '1',
                ),
                'depends_show_if'  => 'on',
                'responsive'       => true,
                'hover'            => 'tabs',
            ),
            'prev_next_option' => array(
                'toggle_slug'       => 'main_content',
                'label' => esc_html__( 'Next & Previous Post Options?', 'divi-machine' ),
                'type' => 'yes_no_button',
                'option_category' => 'basic_option',
                'options' => array(
                    'on' => esc_html__( 'Yes', 'divi-machine' ),
                    'off' => esc_html__( 'No', 'divi-machine' ),
                ),
                'default' => 'off',
                'affects'         => array(
                    'prev_icon',
                    'next_icon',
                    'prev_next_icon_color',
                    'prev_next_icon_size',
                ),
                'description' => esc_html__( 'If you want the ability to go to the next and previous posts, enable this.', 'divi-machine' )
            ),
            'prev_icon' => array(
                'toggle_slug'       => 'main_content',
                'label' => esc_html__( 'Modal Previous Icon', 'divi-machine' ),
                'type' => 'select_icon',
                'option_category' => 'basic_option',
                'class' => array('et-pb-font-icon'),
                'mobile_options'      => true,
                'default'           => '%%19%%',
                'depends_show_if' => 'on',
                'description' => esc_html__( 'Choose an icon for modal previous icon.', 'divi-machine' )
            ),
            'next_icon' => array(
                'toggle_slug'       => 'main_content',
                'label' => esc_html__( 'Modal Next Icon', 'divi-machine' ),
                'type' => 'select_icon',
                'option_category' => 'basic_option',
                'class' => array('et-pb-font-icon'),
                'mobile_options'      => true,
                'default'           => '%%20%%',
                'depends_show_if' => 'on',
                'description' => esc_html__( 'Choose an icon for modal next icon.', 'divi-machine' )
            ),
            'prev_next_icon_color' => array(
                'default'           => $et_accent_color,
                'label'             => esc_html__('Modal Previous & Next Icon Color', 'et_builder'),
                'type'              => 'color-alpha',
                'description'       => esc_html__('Here you can define a custom color for modal Next and Previoius icons.', 'divi-machine'),
                'depends_show_if'   => 'on',
                'toggle_slug'       => 'main_content',
                'option_category'   => 'basic_option',
                'mobile_options'    => true,
            ),
            'prev_next_icon_size' => array(
                'label'            => esc_html__( 'Modal Previous & Next Icon Size', 'et_builder' ),
                'description'      => esc_html__( 'Control the size of the icon by increasing or decreasing the font size.', 'divi-machine' ),
                'type'             => 'range',
                'option_category'  => 'basic_option',
                'toggle_slug'      => 'main_content',
                'default'          => '46px',
                'default_unit'     => 'px',
                'default_on_front' => '',
                'allowed_units'    => array( '%', 'em', 'rem', 'px', 'cm', 'mm', 'in', 'pt', 'pc', 'ex', 'vh', 'vw' ),
                'range_settings'   => array(
                    'min'  => '1',
                    'max'  => '500',
                    'step' => '1',
                ),
                'depends_show_if'  => 'on',
                'responsive'       => true,
                'hover'            => 'tabs',
            ),
            'button_alignment' => array(
                'label'            => esc_html__( 'Button Alignment', 'et_builder' ),
                'description'      => esc_html__( 'Align your button to the left, right or center of the module.', 'et_builder' ),
                'type'             => 'text_align',
                'option_category'  => 'configuration',
                'options'          => et_builder_get_text_orientation_options( array( 'justified' ) ),
                'tab_slug'         => 'advanced',
                'toggle_slug'      => 'alignment',
            ),
        );

        return $fields;
    }

    function render( $attrs, $content = null, $render_slug ) {

        self::$button_text          = $this->props['title'];

        $custom_url  = $this->props['custom_url'];
        $title  = $this->props['title'];
        $button_use_icon            = $this->props['button_use_icon'];
        $custom_icon                = $this->props['button_icon'];
        $button_bg_color            = $this->props['button_bg_color'];
        $fullwidth_btn              = $this->props['fullwidth_btn'];
        $showin_modal               = $this->props['showin_modal'];
        $modal_layout               = $this->props['modal_layout'];
        $modal_overlay_color        = $this->props['modal_overlay_color'];
        $modal_close_icon           = $this->props['modal_close_icon'];
        $modal_close_icon_color     = $this->props['modal_close_icon_color'];
        $modal_close_icon_size      = $this->props['modal_close_icon_size'];
        
        $prev_next_option      = $this->props['prev_next_option'];
        $prev_icon      = $this->props['prev_icon'];
        $next_icon      = $this->props['next_icon'];
        $prev_next_icon_color      = $this->props['prev_next_icon_color'];
        $prev_next_icon_size      = $this->props['prev_next_icon_size'];
        
        $open_external      = $this->props['open_external'];
        $external_acf      = $this->props['external_acf'];
        
        $new_tab      = $this->props['new_tab'];
        
        
        $modal_style      = $this->props['modal_style'];
        
        $button_alignment  = $this->props['button_alignment'];

        $data = '';

        $this->add_classname( 'dmach-btn-align-' . $button_alignment );

        if ($fullwidth_btn == 'on') {
            $this->add_classname('fullwidth-btn');
        }

        // wp_enqueue_style( 'dmach-magnific-css', DE_DMACH_PATH_URL . '/styles/magnific-popup.css', array(), DE_DMACH_VERSION );
        // wp_enqueue_script( 'dmach-magnific-js',  DE_DMACH_PATH_URL . '/scripts/jquery.magnific-popup.min.js', array('jquery'), DE_DMACH_VERSION, true );
        
        //////////////////////////////////////////////////////////////////////

        ob_start();
        $symbols = array( '21', '22', '23', '24', '25', '26', '27', '28', '29', '2a', '2b', '2c', '2d', '2e', '2f', '30', '31', '32', '33', '34', '35', '36', '37', '38', '39', '3a', '3b', '3c', '3d', '3e', '3f', '40', '41', '42', '43', '44', '45', '46', '47', '48', '49', '4a', '4b', '4c', '4d', '4e', '4f', '50', '51', '52', '53', '54', '55', '56', '57', '58', '59', '5a', '5b', '5c', '5d', '5e', '5f', '60', '61', '62', '63', '64', '65', '66', '67', '68', '69', '6a', '6b', '6c', '6d', '6e', '6f', '70', '71', '72', '73', '74', '75', '76', '77', '78', '79', '7a', '7b', '7c', '7d', '7e', 'e000', 'e001', 'e002', 'e003', 'e004', 'e005', 'e006', 'e007', 'e009', 'e00a', 'e00b', 'e00c', 'e00d', 'e00e', 'e00f', 'e010', 'e011', 'e012', 'e013', 'e014', 'e015', 'e016', 'e017', 'e018', 'e019', 'e01a', 'e01b', 'e01c', 'e01d', 'e01e', 'e01f', 'e020', 'e021', 'e022', 'e023', 'e024', 'e025', 'e026', 'e027', 'e028', 'e029', 'e02a', 'e02b', 'e02c', 'e02d', 'e02e', 'e02f', 'e030', 'e103', 'e0ee', 'e0ef', 'e0e8', 'e0ea', 'e101', 'e107', 'e108', 'e102', 'e106', 'e0eb', 'e010', 'e105', 'e0ed', 'e100', 'e104', 'e0e9', 'e109', 'e0ec', 'e0fe', 'e0f6', 'e0fb', 'e0e2', 'e0e3', 'e0f5', 'e0e1', 'e0ff', 'e031', 'e032', 'e033', 'e034', 'e035', 'e036', 'e037', 'e038', 'e039', 'e03a', 'e03b', 'e03c', 'e03d', 'e03e', 'e03f', 'e040', 'e041', 'e042', 'e043', 'e044', 'e045', 'e046', 'e047', 'e048', 'e049', 'e04a', 'e04b', 'e04c', 'e04d', 'e04e', 'e04f', 'e050', 'e051', 'e052', 'e053', 'e054', 'e055', 'e056', 'e057', 'e058', 'e059', 'e05a', 'e05b', 'e05c', 'e05d', 'e05e', 'e05f', 'e060', 'e061', 'e062', 'e063', 'e064', 'e065', 'e066', 'e067', 'e068', 'e069', 'e06a', 'e06b', 'e06c', 'e06d', 'e06e', 'e06f', 'e070', 'e071', 'e072', 'e073', 'e074', 'e075', 'e076', 'e077', 'e078', 'e079', 'e07a', 'e07b', 'e07c', 'e07d', 'e07e', 'e07f', 'e080', 'e081', 'e082', 'e083', 'e084', 'e085', 'e086', 'e087', 'e088', 'e089', 'e08a', 'e08b', 'e08c', 'e08d', 'e08e', 'e08f', 'e090', 'e091', 'e092', 'e0f8', 'e0fa', 'e0e7', 'e0fd', 'e0e4', 'e0e5', 'e0f7', 'e0e0', 'e0fc', 'e0f9', 'e0dd', 'e0f1', 'e0dc', 'e0f3', 'e0d8', 'e0db', 'e0f0', 'e0df', 'e0f2', 'e0f4', 'e0d9', 'e0da', 'e0de', 'e0e6', 'e093', 'e094', 'e095', 'e096', 'e097', 'e098', 'e099', 'e09a', 'e09b', 'e09c', 'e09d', 'e09e', 'e09f', 'e0a0', 'e0a1', 'e0a2', 'e0a3', 'e0a4', 'e0a5', 'e0a6', 'e0a7', 'e0a8', 'e0a9', 'e0aa', 'e0ab', 'e0ac', 'e0ad', 'e0ae', 'e0af', 'e0b0', 'e0b1', 'e0b2', 'e0b3', 'e0b4', 'e0b5', 'e0b6', 'e0b7', 'e0b8', 'e0b9', 'e0ba', 'e0bb', 'e0bc', 'e0bd', 'e0be', 'e0bf', 'e0c0', 'e0c1', 'e0c2', 'e0c3', 'e0c4', 'e0c5', 'e0c6', 'e0c7', 'e0c8', 'e0c9', 'e0ca', 'e0cb', 'e0cc', 'e0cd', 'e0ce', 'e0cf', 'e0d0', 'e0d1', 'e0d2', 'e0d3', 'e0d4', 'e0d5', 'e0d6', 'e0d7', 'e600', 'e601', 'e602', 'e603', 'e604', 'e605', 'e606', 'e607', 'e608', 'e609', 'e60a', 'e60b', 'e60c', 'e60d', 'e60e', 'e60f', 'e610', 'e611', 'e612', 'e008', );

        $close_icon_index   = (int) str_replace( '%', '', $modal_close_icon );
        $close_icon_rendered =  sprintf(
            '\%1$s',
            $symbols[$close_icon_index]
        );

        $prev_icon_index   = (int) str_replace( '%', '', $prev_icon );
        $prev_icon_rendered =  sprintf(
            '\%1$s',
            $symbols[$prev_icon_index]
        );

        $next_icon_index   = (int) str_replace( '%', '', $next_icon );
        $next_icon_rendered =  sprintf(
            '\%1$s',
            $symbols[$next_icon_index]
        );


        $post_link = get_permalink(get_the_ID());
        if ( $showin_modal == 'on' && $open_external == 'off'){
            ?>
            <style>
                .dmach-popup .modal-close:before {
                    content: "<?php echo $close_icon_rendered ?>";
                    font-size: <?php echo $modal_close_icon_size;?>;
                    color: <?php echo $modal_close_icon_color;?>;
                }
                .dmach-popup{
                    background-color: <?php echo $modal_overlay_color;?>!important;
                }
            </style>
            <?php
    ?>
        <a class="et_pb_button show_modal" data-modal-style="<?php echo $modal_style ?>" data-modal-layout="<?php echo $modal_layout;?>" data-id="<?php echo get_the_ID();?>" data-modal-src="#post-modal-<?php echo get_the_ID();?>" href="#"><?php echo $title; ?></a>
    <?php
    if ($prev_next_option == 'on') {
        ?>
        <style>
                .dmach-prev-post:before, .dmach-next-post:before {
                    content: "<?php echo $prev_icon_rendered ?>";
                    font-size: <?php echo $modal_close_icon_size;?>;
                    color: <?php echo $modal_close_icon_color;?>;
                }
                .dmach-next-post:before {
                content: "<?php echo $next_icon_rendered ?>";
                }
        </style>
        <?php
    } else {
        ?>
          <style>
                .dmach-prev-post:before, .dmach-next-post:before {
                    display: none !important;
                }
        </style>
        <?php
    }
        }else{
            if ($open_external == "on") {
                if (isset($external_acf) && $external_acf !== 'none') {
                    $acf_get = get_field_object($external_acf);
                    $post_link = $acf_get['value'];
                } else {
                    $post_link = $post_link;
                }
            } else {
                $post_link = $post_link;
            }

            if ($new_tab == 'on') {
                $newtab = 'target="_blank"';
            } else {
                $newtab = '';
            }
    ?>
        <a class="et_pb_button" href="<?php echo $post_link ?><?php echo $custom_url ?>" <?php echo $newtab ?>><?php echo $title; ?> </a>

    <?php
        }

        if( $button_use_icon == 'on' && $custom_icon != '' ){
            $custom_icon = 'data-icon="'. esc_attr( et_pb_process_font_icon( $custom_icon ) ) .'"';
            ET_Builder_Element::set_style( $render_slug, array(
                'selector'    => 'body #page-container %%order_class%% .et_pb_button:after',
                'declaration' => "content: attr(data-icon);",
            ) );
        }else{
            ET_Builder_Element::set_style( $render_slug, array(
                'selector'    => 'body #page-container %%order_class%% .et_pb_button:hover',
                'declaration' => "padding: .3em 1em;",
            ) );
        }

        if( !empty( $button_bg_color ) ){
            ET_Builder_Element::set_style( $render_slug, array(
                'selector'    => 'body #page-container %%order_class%% .et_pb_button',
                'declaration' => "background-color:". esc_attr( $button_bg_color ) ."!important;",
            ) );
        }

        $data = ob_get_clean();
        //////////////////////////////////////////////////////////////////////

        $data = str_replace(
            'class="et_pb_button"',
            'class="et_pb_button"' . $custom_icon
            , $data
        );

        return $data;

    }
}

new de_mach_view_button_code;

?>
