<?php
if (!defined('ABSPATH')) exit;

class de_mach_acf_item_code extends ET_Builder_Module
{

  public $vb_support = 'on';

  protected $module_credits = array(
    'module_uri' => DE_DMACH_PRODUCT_URL,
    'author'     => DE_DMACH_AUTHOR,
    'author_uri' => DE_DMACH_URL,
  );

  function init()
  {
    $this->name       = esc_html__('.ACF Item - Divi Machine', 'divi-machine');
    $this->slug = 'et_pb_de_mach_acf_item';
    $this->child_title_var             = 'custom_identifier';


    $this->fields_defaults = array(
      // 'loop_layout'         => array( 'on' ),
    );

    $this->settings_modal_toggles = array(
      'general' => array(
        'toggles' => array(
          'main_content' => esc_html__('Main Options', 'divi-machine'),
          'specific'        => esc_html__('Specific Settings', 'et_builder'),
          'relational'        => esc_html__('Relational Field Settings', 'et_builder'),
          'image'        => esc_html__('Image & Icon', 'et_builder'),
        ),
      ),
      'advanced' => array(
        'toggles' => array(
          'text' => esc_html__('Text', 'divi-machine'),
        ),
      ),

    );


    $this->main_css_element = '%%order_class%%';


    $this->advanced_fields = array(
      'fonts' => array(
        'title_css' => array(
          'label'    => esc_html__('Item', 'divi-machine'),
          'css'      => array(
            'main' => "%%order_class%% .dmach-acf-item-content",
            'important' => 'all',
          ),
          'font_size' => array(
            'default' => '14px',
          ),
          'line_height' => array(
            'default' => '1em',
          ),
        ),
        'acf_label_css' => array(
          'label'    => esc_html__('Label', 'divi-machine'),
          'css'      => array(
            'main' => "%%order_class%% .dmach-acf-label",
            'important' => 'all',
          ),
          'font_size' => array(
            'default' => '14px',
          ),
          'line_height' => array(
            'default' => '1em',
          ),
        ),
        'label_css' => array(
          'label'    => esc_html__('Value', 'divi-machine'),
          'css'      => array(
            'main' => "%%order_class%% .dmach-acf-value",
            'important' => 'all',
          ),
          'font_size' => array(
          ),
          'line_height' => array(
          ),
        ),
        'text_before_css' => array(
          'label'    => esc_html__('Before', 'divi-machine'),
          'css'      => array(
            'main' => "%%order_class%% .dmach-acf-before",
            'important' => 'all',
          ),
          'font_size' => array(
            'default' => '14px',
          ),
          'line_height' => array(
            'default' => '1em',
          ),
        ),
        'seperator' => array(
          'label'    => esc_html__('Seperator', 'divi-machine'),
          'css'      => array(
            'main' => "%%order_class%% .dmach-seperator",
            'important' => 'all',
          ),
          'font_size' => array(
            'default' => '14px',
          ),
          'line_height' => array(
            'default' => '1em',
          ),
        ),
        'relational_field_item' => array(
          'label'    => esc_html__('Relational Field List Item', 'divi-machine'),
          'css'      => array(
            'main' => "%%order_class%% .linked_list_item a",
            'important' => 'all',
          ),
          'font_size' => array(
            'default' => '14px',
          ),
          'line_height' => array(
            'default' => '1em',
          ),
        ),
      ),


      'borders'               => array(
        'default' => array(
          'css' => array(
            'main' => array(
              // Accordion Item can use %%parent_class%% because its slug is parent_slug + `_item` suffix
              'border_radii'  => "{$this->main_css_element} .dmach-acf-value",
              'border_styles' => "{$this->main_css_element} .dmach-acf-value",
            )
          ),
        ),
      ),

      'margin_padding' => array(
        'css' => array(
          'important' => 'all',
        ),
      ),

      'background' => array(
        'settings' => array(
          'color' => 'alpha',
        ),
      ),
      'button' => array(
        'button' => array(
          'label' => esc_html__('Button', 'et_builder'),
          'css' => array(
            'main' => "{$this->main_css_element} .et_pb_button",
            'plugin_main' => "{$this->main_css_element}.et_pb_module",
          ),
          'box_shadow'  => array(
            'css' => array(
              'main' => "{$this->main_css_element} .et_pb_button",
              'important' => 'all',
            ),
          ),
          'margin_padding' => array(
            'css'           => array(
              'main' => "{$this->main_css_element} .et_pb_button",
              'important' => 'all',
            ),
          ),
        ),
      ),
      'box_shadow' => array(
        'default' => array(),
      ),
    );


    $this->custom_css_fields = array(
      'item' => array(
        'label'       => esc_html__('Item', 'et_builder'),
        'selector'    => '.dmach-acf-value',
      ),
      'text_before' => array(
        'label'       => esc_html__('Text Before', 'et_builder'),
        'selector'    => '.dmach-acf-before',
      ),
      'label' => array(
        'label'       => esc_html__('Label', 'et_builder'),
        'selector'    => '.dmach-acf-label',
      ),
      'image' => array(
        'label'       => esc_html__('Image', 'et_builder'),
        'selector'    => '.dmach-icon-image-content img',
      ),
    );




    $this->help_videos = array();

    // add_filter( 'et_pb_module_content', array( $this, 'add_acf_item_content' ),10, 6 );
  }

  // function add_acf_item_content( $content, $props, $attrs, $render_slug, $_address, $global_content ){
  //
  //   if ( $render_slug == 'et_pb_de_mach_acf_item'){
  //
  //   $acf_name = $props['acf_name'];
  //   $pretify_text = $props['pretify_text'];
  //   $pretify_seperator = $props['pretify_seperator'];
  //   $prefix = $props['prefix'];
  //   $label_seperator = $props['label_seperator'];
  //
  //   $post_slug = DEDMACH_INIT::get_vb_post_type();
  //
  //     $get_cpt_args = array(
  //       'post_type' => $post_slug,
  //       'post_status' => 'publish',
  //       'posts_per_page' => '1',
  //       'orderby' => 'ID',
  //       'order' => 'ASC',
  //     );
  //
  //     query_posts( $get_cpt_args );
  //
  //     $first = true;
  //
  //     if ( have_posts() ) {
  //     while ( have_posts() ) {
  //       the_post();
  //       // setup_postdata( $post );
  //
  //         if ( $first )  {
  //
  //           //////////////////////////////////////////////////
  //
  //           $get_id = get_the_ID();
  //           $get_title = get_the_title();
  //           if( isset($acf_name) && $acf_name !== 'none') {
  //
  //             // $acf_value = get_field($acf_name, $get_id);
  //             $acf_get = get_field_object($acf_name);
  //             $acf_type = $acf_get['type'];
  //
  //             if ($pretify_text == "on" && ( $acf_type == 'number' || $acf_type == 'range' )) {
  //               $item_value_before = number_format( $acf_get['value'] , 0 , '.' , ',' );
  //               $item_value = str_replace(',', $pretify_seperator, $item_value_before);
  //             } else {
  //               $item_value = $acf_get['value'];
  //             }
  //
  //             $show_label_dis =  $acf_get['label'] . $label_seperator;
  //             $show_label_dis = '<span class="dmach-acf-label">' . $show_label_dis . '</span>';
  //
  //
  //             $contents = $show_label_dis . $prefix . $item_value;
  //
  //
  //           } else {
  //             $contents = 'Please select an ACF field you have created';
  //           }
  //
  //         //////////////////////////////////////////////////
  //           $first = false;
  //         } else {
  //
  //         }
  //
  //       }
  //     }
  //
  //           $content = $contents;
  //         }
  //
  //         return $content;
  //
  // }

  function get_fields()
  {

    $looplayout_options = array();

    $layout_query = array(
      'post_type' => 'et_pb_layout',
      'posts_per_page' => -1,
      'meta_query' => array(
        array(
          'key' => '_et_pb_predefined_layout',
          'compare' => 'NOT EXISTS',
        ),
      )
    );
    $looplayout_options['none'] = 'No Layout (please choose one)';
    if ($layouts = get_posts($layout_query)) {
      foreach ($layouts as $layout) {
        $looplayout_options[$layout->ID] = $layout->post_title;
      }
    }

    $et_accent_color = et_builder_accent_color();

    $acf_fields = DEDMACH_INIT::get_acf_fields();

    ///////////////////////////////
    $sizes = get_intermediate_image_sizes();
    foreach ($sizes as $size) {
      $options[$size] = $size;
    }


    //////////////////////////////
    $fields = array(
      'custom_identifier' => array(
        'label'       => __('Repeater Custom Label', 'divi-machine'),
        'type'        => 'text',
        'toggle_slug'     => 'main_content',
        'default'         => 'ACF Item',
        'option_category'   => 'configuration',
        'description' => __('This will change the label of the item inside the repeater module for easy identification.', 'divi-machine'),
      ),
      'acf_name' => array(
        'toggle_slug'       => 'main_content',
        'label'             => esc_html__('ACF Name', 'divi-machine'),
        'type'              => 'select',
        'options'           => $acf_fields,
        'default'           => 'none',
        'computed_affects' => array(
          '__getacfitem',
        ),
        'option_category'   => 'configuration',
        'description'       => esc_html__('Add the name of the ACF you want to display here', 'divi-machine'),
      ),
      'acf_tag' => array(
        'option_category' => 'configuration',
        'toggle_slug'     => 'main_content',
        'label'             => esc_html__('Value HTML Tag', 'divi-machine'),
        'type'              => 'select',
        'options'           => array(
          'p' => esc_html__('p', 'divi-machine'),
          'span' => esc_html__('span', 'divi-machine'),
          'h1' => esc_html__('h1', 'divi-machine'),
          'h2' => esc_html__('h2', 'divi-machine'),
          'h3' => esc_html__('h3', 'divi-machine'),
          'h4' => esc_html__('h4', 'divi-machine'),
          'h5' => esc_html__('h5', 'divi-machine'),
          'h6' => esc_html__('h6', 'divi-machine'),
          'img' => esc_html__('img', 'divi-machine'),
        ),
        'default' => 'p',
        'computed_affects' => array(
          '__getacfitem',
        ),
        'description'       => esc_html__('Choose what HTML tag you want for your ACF value', 'divi-machine'),
      ),
      'show_label' => array(
        'toggle_slug'       => 'main_content',
        'label' => esc_html__('Show Label', 'divi-machine'),
        'type' => 'yes_no_button',
        'options_category' => 'configuration',
        'options' => array(
          'on' => esc_html__('Yes', 'divi-machine'),
          'off' => esc_html__('No', 'divi-machine'),
        ),
        'affects'         => array(
          'label_seperator',
          'custom_label'
        ),
        'default' => 'on',
        'description' => esc_html__('Enable this if you want to show the label of the ACF item.', 'divi-machine')
      ),
      'custom_label' => array(
        'toggle_slug'       => 'main_content',
        'label'             => esc_html__('Custom label (leave blank for default label)', 'divi-machine'),
        'type'              => 'text',
        'option_category'   => 'configuration',
        'depends_show_if'   => 'on',
        'computed_affects' => array(
          '__getacfitem',
        ),
        'description'       => esc_html__('Add a custom label here or leave it blank to get the default label.', 'divi-machine'),
      ),
      'prefix' => array(
        'option_category' => 'configuration',
        'toggle_slug'     => 'main_content',
        'label'             => esc_html__('Prefix', 'divi-machine'),
        'type'              => 'text',
        'computed_affects' => array(
          '__getacfitem',
        ),
        'description'       => esc_html__('If you want text to appear directly before the ACF, add it here', 'divi-machine'),
      ),
      'suffix' => array(
        'option_category' => 'configuration',
        'toggle_slug'     => 'main_content',
        'label'             => esc_html__('Suffix', 'divi-machine'),
        'type'              => 'text',
        'computed_affects' => array(
          '__getacfitem',
        ),
        'description'       => esc_html__('If you want text to appear directly after the ACF, add it here', 'divi-machine'),
      ),
      'text_before' => array(
        'option_category' => 'configuration',
        'toggle_slug'     => 'main_content',
        'label'             => esc_html__('Text Before', 'divi-machine'),
        'type'              => 'text',
        'description'       => esc_html__('If you want text to appear before the choice field, add it here', 'divi-machine'),
      ),




      'label_seperator' => array(
        'toggle_slug'       => 'main_content',
        'label'             => esc_html__('Label Seperator', 'divi-machine'),
        'type'              => 'text',
        'option_category'   => 'configuration',
        'depends_show_if'   => 'on',
        'computed_affects' => array(
          '__getacfitem',
        ),
        'default'           => ': ',
        'description'       => esc_html__('Add the label seperator here, something like : ', 'divi-machine'),
      ),
      'use_icon' => array( // TODO: VB
        'label'           => esc_html__('Use Icon', 'et_builder'),
        'type'            => 'yes_no_button',
        'option_category' => 'basic_option',
        'options'         => array(
          'off' => esc_html__('No', 'et_builder'),
          'on'  => esc_html__('Yes', 'et_builder'),
        ),
        'toggle_slug'     => 'image',
        'computed_affects' => array(
          '__getacfitem',
        ),
        'affects'         => array(
          'font_icon',
          'image_max_width',
          'use_icon_font_size',
          'use_circle',
          'icon_color',
          'image',
          'alt',
          'child_filter_hue_rotate',
          'child_filter_saturate',
          'child_filter_brightness',
          'child_filter_contrast',
          'child_filter_invert',
          'child_filter_sepia',
          'child_filter_opacity',
          'child_filter_blur',
          'child_mix_blend_mode',
        ),
        'description' => esc_html__('Here you can choose whether icon set below should be used.', 'et_builder'),
        'default_on_front' => 'off',
      ),
      'font_icon' => array(
        'label'               => esc_html__('Icon', 'et_builder'),
        'type'                => 'select_icon',
        'option_category'     => 'basic_option',
        'class'               => array('et-pb-font-icon'),
        'toggle_slug'         => 'image',
        'computed_affects' => array(
          '__getacfitem',
        ),
        'description'         => esc_html__('Choose an icon to display with your ACF item.', 'et_builder'),
        'depends_show_if'     => 'on',
        'mobile_options'      => true,
        'hover'               => 'tabs',
      ),
      'icon_color' => array(
        'default'           => $et_accent_color,
        'label'             => esc_html__('Icon Color', 'et_builder'),
        'type'              => 'color-alpha',
        'description'       => esc_html__('Here you can define a custom color for your icon.', 'et_builder'),
        'depends_show_if'   => 'on',
        'tab_slug'          => 'advanced',
        'toggle_slug'       => 'icon_settings',
        'hover'             => 'tabs',
        'mobile_options'    => true,
      ),
      'use_circle' => array(
        'label'           => esc_html__('Circle Icon', 'et_builder'),
        'type'            => 'yes_no_button',
        'option_category' => 'configuration',
        'options'         => array(
          'off' => esc_html__('No', 'et_builder'),
          'on'  => esc_html__('Yes', 'et_builder'),
        ),
        'affects'           => array(
          'use_circle_border',
          'circle_color',
        ),
        'tab_slug'         => 'advanced',
        'toggle_slug'      => 'icon_settings',
        'description'      => esc_html__('Here you can choose whether icon set above should display within a circle.', 'et_builder'),
        'depends_show_if'  => 'on',
        'default_on_front' => 'off',
      ),
      'circle_color' => array(
        'default'         => $et_accent_color,
        'label'           => esc_html__('Circle Color', 'et_builder'),
        'type'            => 'color-alpha',
        'description'     => esc_html__('Here you can define a custom color for the icon circle.', 'et_builder'),
        'depends_show_if' => 'on',
        'tab_slug'        => 'advanced',
        'toggle_slug'     => 'icon_settings',
        'hover'           => 'tabs',
        'mobile_options'  => true,
      ),
      'use_circle_border' => array(
        'label'           => esc_html__('Show Circle Border', 'et_builder'),
        'type'            => 'yes_no_button',
        'option_category' => 'layout',
        'options'         => array(
          'off' => esc_html__('No', 'et_builder'),
          'on'  => esc_html__('Yes', 'et_builder'),
        ),
        'affects'           => array(
          'circle_border_color',
        ),
        'description' => esc_html__('Here you can choose whether if the icon circle border should display.', 'et_builder'),
        'depends_show_if'   => 'on',
        'tab_slug'          => 'advanced',
        'toggle_slug'       => 'icon_settings',
        'default_on_front'  => 'off',
      ),
      'circle_border_color' => array(
        'default'         => $et_accent_color,
        'label'           => esc_html__('Circle Border Color', 'et_builder'),
        'type'            => 'color-alpha',
        'description'     => esc_html__('Here you can define a custom color for the icon circle border.', 'et_builder'),
        'depends_show_if' => 'on',
        'tab_slug'        => 'advanced',
        'toggle_slug'     => 'icon_settings',
        'hover'           => 'tabs',
        'mobile_options'  => true,
      ),
			'use_icon_font_size'  => array(
				'label'            => esc_html__( 'Use Icon Font Size', 'et_builder' ),
				'description'      => esc_html__( 'If you would like to control the size of the icon, you must first enable this option.', 'et_builder' ),
				'type'             => 'yes_no_button',
				'option_category'  => 'font_option',
				'options'          => array(
          'off' => esc_html__('No', 'et_builder'),
          'on'  => esc_html__('Yes', 'et_builder'),
				),
				'affects'          => array(
					'icon_font_size',
				),
				'depends_show_if'  => 'on',
				'tab_slug'         => 'advanced',
				'toggle_slug'      => 'icon_settings',
				'default_on_front' => 'off',
			),
			'icon_font_size'      => array(
				'label'            => esc_html__( 'Icon Font Size', 'et_builder' ),
				'description'      => esc_html__( 'Control the size of the icon by increasing or decreasing the font size.', 'et_builder' ),
				'type'             => 'range',
				'option_category'  => 'font_option',
				'tab_slug'         => 'advanced',
				'toggle_slug'      => 'icon_settings',
				'default'          => '16px',
				'default_unit'     => 'px',
				'default_on_front' => '',
				'allowed_units'    => array( '%', 'em', 'rem', 'px', 'cm', 'mm', 'in', 'pt', 'pc', 'ex', 'vh', 'vw' ),
				'range_settings'   => array(
					'min'  => '1',
					'max'  => '500',
					'step' => '1',
				),
				'depends_show_if'  => 'on',
				'responsive'       => true,
				'hover'            => 'tabs',
			),
      'image' => array(
        'label'              => esc_html__('Custom Image before/after', 'et_builder'),
        'type'               => 'upload',
        'option_category'    => 'basic_option',
        'upload_button_text' => esc_attr__('Upload an image', 'et_builder'),
        'choose_text'        => esc_attr__('Choose an Image', 'et_builder'),
        'update_text'        => esc_attr__('Set As Image', 'et_builder'),
        'depends_show_if'    => 'off',
        'description'        => esc_html__('Upload an image to display at the top of your ACF item.', 'et_builder'),
        'toggle_slug'        => 'image',
        'dynamic_content'    => 'image',
        'mobile_options'     => true,
        'hover'              => 'tabs',
      ),
      'alt' => array(
        'label'           => esc_html__('Image Alt Text', 'et_builder'),
        'type'            => 'text',
        'option_category' => 'basic_option',
        'description'     => esc_html__('Define the HTML ALT text for your image here.', 'et_builder'),
        'depends_show_if' => 'off',
        'tab_slug'        => 'custom_css',
        'toggle_slug'     => 'attributes',
        'dynamic_content' => 'text',
      ),
      'icon_image_placement' => array(
        'option_category' => 'basic_option',
        'toggle_slug'     => 'image',
        'label'             => esc_html__('Image / Icon Placement', 'divi-machine'),
        'type'              => 'select',
        'options'           => array(
          'top' => esc_html__('Top', 'divi-machine'),
          'right' => sprintf(esc_html__('Right', 'divi-machine')),
          'bottom' => sprintf(esc_html__('Bottom', 'divi-machine')),
          'left' => sprintf(esc_html__('Left', 'divi-machine')),
        ),
        'default' => 'left',
        'description'       => esc_html__('Choose where you want the icon or image to be.', 'divi-machine'),
      ),

      'image_max_width' => array(
        'label'           => esc_html__('Custom Image before/after max width', 'et_builder'),
        'description'     => esc_html__('Adjust the width of the image.', 'et_builder'),
        'type'            => 'range',
        'option_category' => 'basic_option',
        'toggle_slug'     => 'image',
        'mobile_options'  => true,
        'validate_unit'   => true,
        'depends_show_if' => 'off',
        'allowed_units'   => array('%', 'em', 'rem', 'px', 'cm', 'mm', 'in', 'pt', 'pc', 'ex', 'vh', 'vw'),
        'default'         => '100%',
        'default_unit'    => '%',
        'default_on_front' => '',
        'allow_empty'     => true,
        'range_settings'  => array(
          'min'  => '0',
          'max'  => '100',
          'step' => '1',
        ),
        'responsive'      => true,
      ),

      'image_mobile_stacking' => array(
        'option_category' => 'basic_option',
        'toggle_slug'     => 'image',
        'label'             => esc_html__('Image Mobile Stacking', 'divi-machine'),
        'type'              => 'select',
        'options'           => array(
          'initial' => esc_html__('None', 'divi-machine'),
          'column' => sprintf(esc_html__('Stack', 'divi-machine')),
          'column-reverse' => sprintf(esc_html__('Stack Reverse', 'divi-machine')),
        ),
        'default' => 'initial',
        'description'       => esc_html__('CHoose how you want the image and ACF item to stack on mobile.', 'divi-machine'),
      ),

      // 'icon_image_padding_left' => array(
      // 	'label'           => esc_html__( 'Image / Icon Padding Left', 'et_builder' ),
      // 	'description'     => esc_html__( 'Adjust the passing on the left of the icon or the image.', 'et_builder' ),
      // 	'type'            => 'range',
      //   'option_category' => 'basic_option',
      //   'toggle_slug'     => 'image',
      // 	'validate_unit'   => true,
      // 	'allowed_units'   => array( '%', 'em', 'rem', 'px', 'cm', 'mm', 'in', 'pt', 'pc', 'ex', 'vh', 'vw' ),
      // 	'default'         => '0px',
      // 	'default_unit'    => 'px',
      // 	'default_on_front'=> '',
      // 	'allow_empty'     => true,
      // 	'range_settings'  => array(
      // 		'min'  => '0',
      // 		'max'  => '100',
      // 		'step' => '1',
      // 	),
      // ),
      //
      // 'icon_image_padding_right' => array(
      // 	'label'           => esc_html__( 'Image / Icon Padding Right', 'et_builder' ),
      // 	'description'     => esc_html__( 'Adjust the passing on the right of the icon or the image.', 'et_builder' ),
      // 	'type'            => 'range',
      //   'option_category' => 'basic_option',
      //   'toggle_slug'     => 'image',
      // 	'validate_unit'   => true,
      // 	'allowed_units'   => array( '%', 'em', 'rem', 'px', 'cm', 'mm', 'in', 'pt', 'pc', 'ex', 'vh', 'vw' ),
      // 	'default'         => '10px',
      // 	'default_unit'    => 'px',
      // 	'default_on_front'=> '',
      // 	'allow_empty'     => true,
      // 	'range_settings'  => array(
      // 		'min'  => '0',
      // 		'max'  => '100',
      // 		'step' => '1',
      // 	),
      // ),

      // Specific field

      'return_format' => array(
        'option_category' => 'basic_option',
        'toggle_slug'     => 'specific',
        'label'             => esc_html__('Image/File/Link: Return Format', 'divi-machine'),
        'type'              => 'select',
        'options'           => array(
          'array' => esc_html__('Array', 'divi-machine'),
          'url' => sprintf(esc_html__('URL', 'divi-machine')),
          'id' => sprintf(esc_html__('ID', 'divi-machine')),
          'value' => sprintf(esc_html__('Choice Value', 'divi-machine')),
          'label' => sprintf(esc_html__('Choice Label', 'divi-machine')),
          'both' => sprintf(esc_html__('Choice Both', 'divi-machine')),
        ),
        'default' => 'array',
        'computed_affects' => array(
          '__getacfitem',
        ),
        'description'       => esc_html__('Choose how you have defined the return format in ACF settings.', 'divi-machine'),
      ),

      'checkbox_style' => array(
        'option_category' => 'basic_option',
        'toggle_slug'     => 'specific',
        'label'             => esc_html__('Checkbox: Style', 'divi-machine'),
        'type'              => 'select',
        'options'           => array(
          'list' => esc_html__('List (Comma Seperated)', 'divi-machine'),
          'bullet' => sprintf(esc_html__('Bullet List', 'divi-machine')),
          'numbered' => sprintf(esc_html__('Numbered List', 'divi-machine')),
        ),
        'default' => 'array',
        'computed_affects' => array(
          '__getacfitem',
        ),
        'description'       => esc_html__('Choose how you want the checkbox to be displayed.', 'divi-machine'),
      ),

      'link_button' => array(
        'option_category' => 'basic_option',
        'toggle_slug'     => 'specific',
        'label' => esc_html__('File/Link/Email/Phone field: Make it a Button?', 'divi-machine'),
        'type' => 'yes_no_button',
        'options_category' => 'configuration',
        'options' => array(
          'on' => esc_html__('Yes', 'divi-machine'),
          'off' => esc_html__('No', 'divi-machine'),
        ),
        'default' => 'off',
        'computed_affects' => array(
          '__getacfitem',
        ),
        'description' => esc_html__('Enable this if you want to make the file or link a button.', 'divi-machine')
      ),

      'link_new_tab' => array(
        'option_category' => 'basic_option',
        'toggle_slug'     => 'specific',
        'label' => esc_html__('Link/URL: Open in a New Tab?', 'divi-machine'),
        'type' => 'yes_no_button',
        'options_category' => 'configuration',
        'options' => array(
          'on' => esc_html__('Yes', 'divi-machine'),
          'off' => esc_html__('No', 'divi-machine'),
        ),
        'default' => 'on',
        'description' => esc_html__('Enable this if you want the link to open up in a new tab.', 'divi-machine')
      ),
      'link_button_text' => array(
        'option_category' => 'basic_option',
        'toggle_slug'     => 'specific',
        'label'             => esc_html__('Custom Text for Button', 'divi-machine'),
        'type'              => 'text',
        'default'           => '',
        'computed_affects' => array(
          '__getacfitem',
        ),
        'description'       => esc_html__('If you want custom text instead of the URL or file name, add it here', 'divi-machine'),
      ),


      'url_link_icon' => array(
        'option_category' => 'basic_option',
        'toggle_slug'     => 'specific',
        'label' => esc_html__('File/Link field: remove website link name', 'divi-machine'),
        'type' => 'yes_no_button',
        'options_category' => 'configuration',
        'options' => array(
          'on' => esc_html__('Yes', 'divi-machine'),
          'off' => esc_html__('No', 'divi-machine'),
        ),
        'default' => 'off',
        'description' => esc_html__('Enable this if you want to remove the url name and have the icon/image link.', 'divi-machine')
      ),
      'image_size' => array(
        'option_category' => 'basic_option',
        'toggle_slug'     => 'specific',
        'label' => __('Image: Image Size', 'et_builder'),
        'type' => 'select',
        'options' => $options,
        'default' => 'full',
        'description' => __('Choose the size of the image you want to display, if you are using an image field and returning the ID or array.', 'et_builder'),
      ),
      'true_false_text_true' => array(
        'option_category' => 'basic_option',
        'toggle_slug'     => 'specific',
        'label'             => esc_html__('True/False: Text when True', 'divi-machine'),
        'type'              => 'text',
        'default'           => 'True',
        'description'       => esc_html__('Add the text you want to appear when they select true in the True / False field', 'divi-machine'),
      ),
      'true_false_text_false' => array(
        'option_category' => 'basic_option',
        'toggle_slug'     => 'specific',
        'label'             => esc_html__('True/False: Text when False', 'divi-machine'),
        'type'              => 'text',
        'default'           => 'False',
        'description'       => esc_html__('Add the text you want to appear when they select false in the True / False field', 'divi-machine'),
      ),
      'is_video'   => array(
        'options_category' => 'basic_option',
        'toggle_slug'       => 'specific',
        'label' => esc_html__('Is Video?', 'divi-machine'),
        'type' => 'yes_no_button',
        'options' => array(
          'on' => esc_html__('Yes', 'divi-machine'),
          'off' => esc_html__('No', 'divi-machine'),
        ),
        'affects' => array(
          'video_loop',
          'video_autoplay',
        ),
        'computed_affects' => array(
          '__getacfitem',
        ),
        'default' => 'off',
        'description' => esc_html__('Enable this if this field is video.', 'divi-machine')
      ),
      'video_loop'  => array(
        'options_category' => 'basic_option',
        'toggle_slug'       => 'specific',
        'label' => esc_html__('Loop Video?', 'divi-machine'),
        'type' => 'yes_no_button',
        'options' => array(
          'on' => esc_html__('Yes', 'divi-machine'),
          'off' => esc_html__('No', 'divi-machine'),
        ),
        'computed_affects' => array(
          '__getacfitem',
        ),
        'depends_show_if'   => 'on',
        'default' => 'on',
        'description' => esc_html__('Enable this if you want to loop video.', 'divi-machine')
      ),
      'video_autoplay'  => array(
        'options_category' => 'basic_option',
        'toggle_slug'       => 'specific',
        'label' => esc_html__('Autoplay Video?', 'divi-machine'),
        'type' => 'yes_no_button',
        'options' => array(
          'on' => esc_html__('Yes', 'divi-machine'),
          'off' => esc_html__('No', 'divi-machine'),
        ),
        'affects' => array(
          'video_thumbnail',
          'video_icon_color',
          'video_icon_font_size',
        ),
        'computed_affects' => array(
          '__getacfitem',
        ),
        'depends_show_if'   => 'on',
        'default' => 'on',
        'description' => esc_html__('Enable this if you want to play video automatically.', 'divi-machine')
      ),
      'video_thumbnail'  => array(
        'options_category' => 'basic_option',
        'toggle_slug'       => 'specific',
        'label' => esc_html__('Video Thumbnail', 'divi-machine'),
        'type' => 'upload',
        'depends_show_if'   => 'on',
        'upload_button_text'      => esc_html__( 'Upload an image' ),
        'choose_text'             => esc_attr__( 'Choose an Image', 'et_builder' ),
        'update_text'             => esc_attr__( 'Set As Image', 'et_builder' ),
        'classes'                 => 'et_pb_video_overlay',
        'dynamic_content'         => 'image',
        'mobile_options'          => true,
        'hover'                   => 'tabs',
        'computed_affects' => array(
          '__getacfitem',
        ),
        'depends_show_if'         => 'off',
        'description' => esc_html__('Enable this if you want to show the label of the ACF item.', 'divi-machine')
      ),
      'video_icon_color'         => array(
        'options_category' => 'basic_option',
        'toggle_slug'       => 'specific',
        'label'          => esc_html__( 'Play Icon Color', 'et_builder' ),
        'description'    => esc_html__( 'Here you can define a custom color for the play icon.', 'et_builder' ),
        'type'           => 'color-alpha',
        'custom_color'   => true,
        'hover'          => 'tabs',
        'mobile_options' => true,
        'depends_show_if'         => 'off',
      ),
      'video_icon_font_size'      => array(
        'options_category' => 'basic_option',
        'toggle_slug'       => 'specific',
        'label'            => esc_html__( 'Play Icon Custom Size?', 'et_builder' ),
        'description'      => esc_html__( 'If you would like to control the size of the icon, you must first enable this option.', 'et_builder' ),
        'type'             => 'yes_no_button',
        'options'          => array(
          'off' => esc_html__( 'No' ),
          'on'  => esc_html__( 'Yes' ),
        ),
        'affects'          => array(
          'video_icon_custom_size',
        ),
        'default'       => 'off',
        'depends_show_if'         => 'off',
      ),
      'video_icon_custom_size'          => array(
        'options_category' => 'basic_option',
        'toggle_slug'       => 'specific',
        'label'            => esc_html__( 'Play Icon Font Size', 'et_builder' ),
        'description'      => esc_html__( 'Control the size of the icon by increasing or decreasing the font size.', 'et_builder' ),
        'type'             => 'range',
        'allowed_units'    => array( '%', 'em', 'rem', 'px', 'cm', 'mm', 'in', 'pt', 'pc', 'ex', 'vh', 'vw' ),
        'default'          => '96px',
        'default_unit'     => 'px',
        'default_on_front' => '',
        'range_settings'   => array(
          'min'  => '1',
          'max'  => '120',
          'step' => '1',
        ),
        'depends_show_if'  => 'on',
      ),

      'pretify_text' => array(
        'toggle_slug'       => 'specific',
        'label' => esc_html__('Prettify Your text?', 'divi-machine'),
        'type' => 'yes_no_button',
        'options_category' => 'configuration',
        'computed_affects' => array(
          '__getacfitem',
        ),
        'options' => array(
          'on' => esc_html__('Yes', 'divi-machine'),
          'off' => esc_html__('No', 'divi-machine'),
        ),
        'affects'         => array(
          'pretify_seperator',
        ),
        'default' => 'off',
        'description' => esc_html__('Enable this if you want to pretify your text, improve readibility of long numbers. 10000000 → 10 000 000.', 'divi-machine')
      ),
      'pretify_seperator' => array(
        'toggle_slug'       => 'specific',
        'label'             => esc_html__('Pretify Separator', 'divi-machine'),
        'type'              => 'text',
        'options_category' => 'configuration',
        'computed_affects' => array(
          '__getacfitem',
        ),
        'depends_show_if'   => 'on',
        'default'           => ', ',
        'description'       => esc_html__('Set up your own separator for long numbers. 10 000, 10.000, 10-000 etc.', 'divi-machine'),
      ),
      'linked_post_style' => array(
        'option_category' => 'basic_option',
        'toggle_slug'     => 'relational',
        'label'             => esc_html__('Relational Field Style', 'divi-machine'),
        'type'              => 'select',
        'options'           => array(
          'custom' => esc_html__('Custom Loop Layout', 'divi-machine'),
          'list' => sprintf(esc_html__('Comma-Seperated List', 'divi-machine')),
        ),
        'default' => 'custom',
        'description'       => esc_html__('Choose the way you want to show your linked posts.', 'divi-machine'),
        'affects'         => array(
          'loop_layout',
          'columns',
          'columns_tablet',
          'columns_mobile',
          'link_post_seperator'
        ),
      ),
      'link_post_seperator' => array(
        'toggle_slug'       => 'relational',
        'label'             => esc_html__('List Seperator', 'divi-machine'),
        'type'              => 'text',
        'option_category'   => 'basic_option',
        'depends_show_if'   => 'list',
        'default'           => ', ',
        'description'       => esc_html__('Add the label seperator here, something like , ', 'divi-machine'),
      ),
      'loop_layout' => array(
        'label'             => esc_html__('Relational Field Loop Layout', 'divi-machine'),
        'type'              => 'select',
        'option_category' => 'basic_option',
        'toggle_slug'     => 'relational',
        'default'           => 'none',
        'depends_show_if'   => 'custom',
        'computed_affects' => array(
          '__getarchiveloop',
        ),
        'options'           => $looplayout_options,
        'description'        => esc_html__('Choose the layout you have made for each post in the loop.', 'divi-machine'),
      ),
      'columns' => array(
        'toggle_slug'       => 'relational',
        'label'             => esc_html__('Grid Columns', 'divi-machine'),
        'type'              => 'select',
        'option_category'   => 'basic_option',
        'default'   => '4',
        'depends_show_if'   => 'custom',
        'options'           => array(
          '1'  => esc_html__('One', 'divi-machine'),
          '2'  => esc_html__('Two', 'divi-machine'),
          '3' => esc_html__('Three', 'divi-machine'),
          '4' => esc_html__('Four', 'divi-machine'),
          '5' => esc_html__('Five', 'divi-machine'),
          '6' => esc_html__('Six', 'divi-machine'),
        ),
        'computed_affects' => array(
          '__getarchiveloop',
        ),
        'description'        => esc_html__('How many columns do you want to see', 'divi-machine'),
      ),
      'columns_tablet' => array(
        'toggle_slug'       => 'relational',
        'label'             => esc_html__('Tablet Grid Columns', 'divi-machine'),
        'type'              => 'select',
        'option_category'   => 'basic_option',
        'default'   => '2',
        'depends_show_if'   => 'custom',
        'options'           => array(
          1  => esc_html__('One', 'divi-machine'),
          2  => esc_html__('Two', 'divi-machine'),
          3 => esc_html__('Three', 'divi-machine'),
          4 => esc_html__('Four', 'divi-machine'),
        ),
        'computed_affects' => array(
          '__getarchiveloop',
        ),
        'description'        => esc_html__('How many columns do you want to see on tablet', 'divi-machine'),
      ),
      'columns_mobile' => array(
        'toggle_slug'       => 'relational',
        'label'             => esc_html__('Mobile Grid Columns', 'divi-machine'),
        'type'              => 'select',
        'option_category'   => 'basic_option',
        'default'   => '1',
        'depends_show_if'   => 'custom',
        'options'           => array(
          1  => esc_html__('One', 'divi-machine'),
          2  => esc_html__('Two', 'divi-machine'),
        ),
        'computed_affects' => array(
          '__getarchiveloop',
        ),
        'description'        => esc_html__('How many columns do you want to see on mobile', 'divi-machine'),
      ),

      'button_alignment' => array(
        'label'            => esc_html__('ACF Item Alignment', 'et_builder'),
        'description'      => esc_html__('Align your ACF item and icon/image.', 'et_builder'),
        'type'             => 'text_align',
        'option_category'  => 'configuration',
        'options'          => et_builder_get_text_orientation_options(array('justified')),
        'tab_slug'         => 'advanced',
        'toggle_slug'      => 'alignment',
      ),
      '__getacfitem' => array(
        'type' => 'computed',
        'computed_callback' => array('de_mach_acf_item_code', 'get_acf_item'),
        'computed_depends_on' => array(
          'acf_name',
          'acf_tag',
          'pretify_text',
          'pretify_seperator',
          'prefix',
          'suffix',
          'label_seperator',
          'custom_label',
          'return_format',
          'checkbox_style',
          'columns',
          'columns_tablet',
          'columns_mobile',
          'is_video',
          'video_loop',
          'video_autoplay',
          'loop_layout',
          'font_icon',
          'use_icon',
          'link_button_text',
          'link_button'
        ),
      ),
      '__get_css_class' => array(
        'type' => 'computed',
        'computed_callback' => array('de_mach_acf_item_code', 'get_css_class'),
        'computed_depends_on' => array(
          'acf_name',
        ),
      ),




    );

    return $fields;
  }

  public static function get_css_class($args = array(), $conditional_tags = array(), $current_page = array())
  {
    $num = mt_rand(100000,999999);
    $css_class              = "acfitem_" . $num;
    $GLOBALS['css_class'] = $css_class ;
    return $css_class;
  }


  public static function get_acf_item($args = array(), $conditional_tags = array(), $current_page = array())
  {
    if (!is_admin()) {
      return;
    }

    ob_start();
    $acf_name = $args['acf_name'];
    $acf_tag = $args['acf_tag'];
    $pretify_text = $args['pretify_text'];
    $pretify_seperator = $args['pretify_seperator'];
    $prefix = $args['prefix'];
    $suffix = $args['suffix'];
    $label_seperator = $args['label_seperator'];
    $custom_label = $args['custom_label'];
    $return_format = $args['return_format'];
    $checkbox_style = $args['checkbox_style'];
    $columns = $args['columns'];
    $columns_tablet = $args['columns_tablet'];
    $columns_mobile = $args['columns_mobile'];
    $loop_layout        = $args['loop_layout'];
    $use_icon       =  $args['use_icon'];
    $font_icon        =  $args['font_icon'];
    $link_button_text =  $args['link_button_text'];
    $link_button =  $args['link_button'];
    $css_class = $GLOBALS['css_class'];

    $css_class_css              = "." . $css_class;


    if ('off' === $use_icon) {
    } else {
    $symbols = array( '21', '22', '23', '24', '25', '26', '27', '28', '29', '2a', '2b', '2c', '2d', '2e', '2f', '30', '31', '32', '33', '34', '35', '36', '37', '38', '39', '3a', '3b', '3c', '3d', '3e', '3f', '40', '41', '42', '43', '44', '45', '46', '47', '48', '49', '4a', '4b', '4c', '4d', '4e', '4f', '50', '51', '52', '53', '54', '55', '56', '57', '58', '59', '5a', '5b', '5c', '5d', '5e', '5f', '60', '61', '62', '63', '64', '65', '66', '67', '68', '69', '6a', '6b', '6c', '6d', '6e', '6f', '70', '71', '72', '73', '74', '75', '76', '77', '78', '79', '7a', '7b', '7c', '7d', '7e', 'e000', 'e001', 'e002', 'e003', 'e004', 'e005', 'e006', 'e007', 'e009', 'e00a', 'e00b', 'e00c', 'e00d', 'e00e', 'e00f', 'e010', 'e011', 'e012', 'e013', 'e014', 'e015', 'e016', 'e017', 'e018', 'e019', 'e01a', 'e01b', 'e01c', 'e01d', 'e01e', 'e01f', 'e020', 'e021', 'e022', 'e023', 'e024', 'e025', 'e026', 'e027', 'e028', 'e029', 'e02a', 'e02b', 'e02c', 'e02d', 'e02e', 'e02f', 'e030', 'e103', 'e0ee', 'e0ef', 'e0e8', 'e0ea', 'e101', 'e107', 'e108', 'e102', 'e106', 'e0eb', 'e010', 'e105', 'e0ed', 'e100', 'e104', 'e0e9', 'e109', 'e0ec', 'e0fe', 'e0f6', 'e0fb', 'e0e2', 'e0e3', 'e0f5', 'e0e1', 'e0ff', 'e031', 'e032', 'e033', 'e034', 'e035', 'e036', 'e037', 'e038', 'e039', 'e03a', 'e03b', 'e03c', 'e03d', 'e03e', 'e03f', 'e040', 'e041', 'e042', 'e043', 'e044', 'e045', 'e046', 'e047', 'e048', 'e049', 'e04a', 'e04b', 'e04c', 'e04d', 'e04e', 'e04f', 'e050', 'e051', 'e052', 'e053', 'e054', 'e055', 'e056', 'e057', 'e058', 'e059', 'e05a', 'e05b', 'e05c', 'e05d', 'e05e', 'e05f', 'e060', 'e061', 'e062', 'e063', 'e064', 'e065', 'e066', 'e067', 'e068', 'e069', 'e06a', 'e06b', 'e06c', 'e06d', 'e06e', 'e06f', 'e070', 'e071', 'e072', 'e073', 'e074', 'e075', 'e076', 'e077', 'e078', 'e079', 'e07a', 'e07b', 'e07c', 'e07d', 'e07e', 'e07f', 'e080', 'e081', 'e082', 'e083', 'e084', 'e085', 'e086', 'e087', 'e088', 'e089', 'e08a', 'e08b', 'e08c', 'e08d', 'e08e', 'e08f', 'e090', 'e091', 'e092', 'e0f8', 'e0fa', 'e0e7', 'e0fd', 'e0e4', 'e0e5', 'e0f7', 'e0e0', 'e0fc', 'e0f9', 'e0dd', 'e0f1', 'e0dc', 'e0f3', 'e0d8', 'e0db', 'e0f0', 'e0df', 'e0f2', 'e0f4', 'e0d9', 'e0da', 'e0de', 'e0e6', 'e093', 'e094', 'e095', 'e096', 'e097', 'e098', 'e099', 'e09a', 'e09b', 'e09c', 'e09d', 'e09e', 'e09f', 'e0a0', 'e0a1', 'e0a2', 'e0a3', 'e0a4', 'e0a5', 'e0a6', 'e0a7', 'e0a8', 'e0a9', 'e0aa', 'e0ab', 'e0ac', 'e0ad', 'e0ae', 'e0af', 'e0b0', 'e0b1', 'e0b2', 'e0b3', 'e0b4', 'e0b5', 'e0b6', 'e0b7', 'e0b8', 'e0b9', 'e0ba', 'e0bb', 'e0bc', 'e0bd', 'e0be', 'e0bf', 'e0c0', 'e0c1', 'e0c2', 'e0c3', 'e0c4', 'e0c5', 'e0c6', 'e0c7', 'e0c8', 'e0c9', 'e0ca', 'e0cb', 'e0cc', 'e0cd', 'e0ce', 'e0cf', 'e0d0', 'e0d1', 'e0d2', 'e0d3', 'e0d4', 'e0d5', 'e0d6', 'e0d7', 'e600', 'e601', 'e602', 'e603', 'e604', 'e605', 'e606', 'e607', 'e608', 'e609', 'e60a', 'e60b', 'e60c', 'e60d', 'e60e', 'e60f', 'e610', 'e611', 'e612', 'e008', );

    $font_icon_index   = (int) str_replace( '%', '', $font_icon );
    $font_icon_rendered =  sprintf(
              '\%1$s',
              $symbols[$font_icon_index]
            );

    ?>
    <style>
      .dmach-icon:before {
        content: "<?php echo $font_icon_rendered ?>"
      }
      </style>
    <?php 

     }

    $post_slug = DEDMACH_INIT::get_vb_post_type();

    $get_cpt_args = array(
      'post_type' => $post_slug,
      'post_status' => 'publish',
      'posts_per_page' => '1',
      'orderby' => 'ID',
      'order' => 'ASC',
    );

    query_posts($get_cpt_args);

    $first = true;

    if (have_posts()) {
      while (have_posts()) {
        the_post();
        // setup_postdata( $post );

        if ($first) {

         ?>
          <?php 

          //////////////////////////////////////////////////

          $get_id = get_the_ID();
          $get_title = get_the_title();
          if (isset($acf_name) && $acf_name !== 'none') {

            // $acf_value = get_field($acf_name, $get_id);
            $acf_get = get_field_object($acf_name);
            $acf_type = $acf_get['type'];



            // TODO: Do VB for acf items

            ?>

            <?php

            $basic_acf_types = "text, textarea, number, range, password, wysiwyg, oembed, date_picker, date_time_picker, time_picker";
            $basic_acf_types_explode = explode(', ', $basic_acf_types);

            $choice_acf_choice_types = "select, checkbox, radio, button_group, true_false";
            $choice_acf_choice_types_explode = explode(', ', $choice_acf_choice_types);


            if ($custom_label == "") {
              $show_label_dis =  $acf_get['label'] . $label_seperator;
            } else {
              $show_label_dis =  $custom_label . $label_seperator;
            }

            $show_label_dis = '<span class="dmach-acf-label">' . $show_label_dis . '</span>';

           
            if ( isset($acf_get['prepend']) && $acf_get['prepend'] !== "" ) {
               $prepend = '<span class="acf_prepend">' . $acf_get['prepend'] . '</span>';
            } else {
              $prepend = "";
            }

            if ( isset($acf_get['append']) && $acf_get['append'] !== "" ) {
              $append = '<span class="acf_append">' . $acf_get['append'] . '</span>';
           } else {
             $append = "";
           }     

              


            ////////////////////////////////////////////////////
            // basic fields
            ////////////////////////////////////////////////////
            if (in_array($acf_type, $basic_acf_types_explode)) {

              if ($pretify_text == "on" && ($acf_type == 'number' || $acf_type == 'range')) {
                $item_value_before = number_format($acf_get['value'], 0, '.', ',');
                $item_value = str_replace(',', $pretify_seperator, $item_value_before);
                $item_value = $prepend . $item_value . $append;
              } else {
                $item_value = $prepend . $acf_get['value'] . $append;
              }

              $label_seperator = '<span class="dmach-seperator">' . esc_html($label_seperator) . '</span>';

              ?>

              <<?php echo $acf_tag ?> class="dmach-acf-value"><?php echo $show_label_dis ?><?php echo $prefix; ?><?php echo $item_value; ?><?php echo $suffix; ?></<?php echo $acf_tag ?>>

              <?php

            }
            ////////////////////////////////////////////////////
            // image fields
            ////////////////////////////////////////////////////

            else if ($acf_type == "image") {
              if ($return_format == "array") {

                $thumb = $acf_get['value']['sizes'][$image_size];

                if ($thumb == "") {
                  $thumb = $acf_get['value']['sizes']['large'];
                }

                ?>
                <?php echo $show_label_dis ?> <img class="dmach-acf-value" src="<?php echo esc_url($thumb); ?>" alt="<?php echo esc_attr($acf_get['value']['alt']); ?>" />
              <?php } else if ($return_format == "id") {
                echo wp_get_attachment_image($acf_get['value'], $image_size);
              }  else { ?>
                <?php echo $show_label_dis ?> <img class="dmach-acf-value" src="<?php echo $acf_get['value']; ?>" />
                <?php
              }
            }
            ////////////////////////////////////////////////////
            // file fields
            ////////////////////////////////////////////////////

            else if ($acf_type == "file") {

              $return_format = $acf_get['return_format'];
              $is_video = false;

              if ($return_format == "array") {
                $link_url = $acf_get['value']['url'];
              } else if ($return_format == "id") {
                $link_url = wp_get_attachment_url( $acf_get['value'] );
              } else { 
                $link_url = $acf_get['value'];
              }

              $mime_type = wp_check_filetype($link_url);
              if ( preg_match('/video\/*/', $mime_type['type'] ) ){
                $is_video = true;
              }
              if ($return_format == "array") {
                $title = $acf_get['value']['title'];
                $icon = $acf_get['value']['icon'];
                if ($acf_get['value']['type'] == 'image') {
                  $icon =  $acf_get['value']['sizes']['thumbnail'];
                }

                if ($link_button_text !== "") {
                  $link_button_text_dis = $link_button_text;
                } else {
                  $link_button_text_dis = esc_html($title);
                }
                ?>
                <?php echo $show_label_dis ?>
                <a class="dmach-acf-value et_pb_button" <?php echo $custom_icon?> href="<?php echo $acf_get['value']['url']; ?>"><span><?php echo $link_button_text_dis; ?></span></a>
                <a class="dmach-acf-value no-button" href="<?php echo $acf_get['value']['url']; ?>"><span><?php echo esc_html($title); ?></span></a>
                <?php
              } else if ($return_format == "id") {
                $url = wp_get_attachment_url($acf_get['value']); 
                if ($link_button_text !== "") {
                  $link_button_text_dis = $link_button_text;
                } else {
                  $link_button_text_dis = "Download File";
                }
                ?>
                <?php echo $show_label_dis ?>
                <a class="dmach-acf-value et_pb_button" <?php echo $custom_icon?> href="<?php echo esc_html($url); ?>"><?php echo $link_button_text_dis; ?></a>
                <a class="dmach-acf-value no-button" href="<?php echo esc_html($url); ?>">Download File</a> <?php
              } else { 
                if ($link_button_text !== "") {
                  $link_button_text_dis = $link_button_text;
                } else {
                  $link_button_text_dis = "Download File";
                }
                ?>
                <?php echo $show_label_dis ?>
                <a class="dmach-acf-value et_pb_button" <?php echo $custom_icon?> href="<?php echo $acf_get['value']; ?>"><?php echo $link_button_text_dis ?></a>
                <a class="dmach-acf-value no-button" href="<?php echo $acf_get['value']; ?>">Download File</a> <?php
              }
              if ( $is_video ){
                $video_loop = ($args['video_loop'] == 'on')?true:false;
                $video_autoplay = ($args['video_autoplay'] == 'on')?true:false;
                echo '<div class="dmach-acf-video-wrapper">';

                $video_args = array(
                  $mime_type['ext'] => $link_url,
                  'preload' => 'auto',
                  'autoplay' => $video_autoplay,
                  'loop'    => $video_loop
                );
                echo wp_video_shortcode( $video_args );

                if ( !$video_autoplay ){
                  $video_icon_color = $args['video_icon_color'];
                  $video_icon_font_size = ( $args['video_icon_font_size'] == 'on' )?true:false;
                  if ( $video_icon_font_size ){
                    $video_icon_custom_size = $args['video_icon_custom_size'];
                    $current_style = "color:#".$video_icon_color.";font-size:" . $video_icon_custom_size;
                  }
                ?>
                <div class="dmach-acf-video-poster">
                  <a href="#" class="dmach-acf-video-play" style="<?php echo $current_style;?>"></a>
                </div>
                <script>
                  jQuery(document).ready(function($){
                    $('.dmach-acf-video-play').click(function(e){
                      e.preventDefault();
                      $(this).closest('.dmach-acf-video-wrapper').addClass('playing');
                      $(this).closest('.dmach-acf-video-wrapper').find('video')[0].play();
                    });
                    $('.dmach-acf-video-wrapper video').on('pause',function(){
                      $(this).closest('.dmach-acf-video-wrapper').removeClass('playing');
                    });
                  });
                </script>
                <?php
                }

                echo '</div>';
              }
            }

              ////////////////////////////////////////////////////
            // Choice fields
            ////////////////////////////////////////////////////

            else if (in_array($acf_type, $choice_acf_choice_types_explode)) {

              if (isset($acf_get['multiple']) && $acf_get['multiple'] == "1") {
                $getselected_name = $acf_get['choices'][$acf_get['value']];
                ?>
                <<?php echo $acf_tag ?> class="dmach-acf-value"><?php echo $show_label_dis ?><?php echo implode(', ', $getselected_name); ?></<?php echo $acf_tag ?>>
                <?php
              } else {
                if ($acf_type == "checkbox") {
                  $getselected_name = $acf_get['value'];
                  if ($checkbox_style == "bullet") {
                    ?>
                    <<?php echo $acf_tag ?> class="dmach-acf-value"><?php echo $show_label_dis ?>
                    <?php
                echo "<ul>";
                foreach ($getselected_name as $key => $value) {
                  echo "<li>".$value."</li>";
                }
                echo "</ul>";
                ?>
                    </<?php echo $acf_tag ?>>
                    <?php
                  } else if ($checkbox_style == "numbered") {
                    ?>
                    <<?php echo $acf_tag ?> class="dmach-acf-value"><?php echo $show_label_dis ?>
                    <?php
                echo "<ol>";
                foreach ($getselected_name as $key => $value) {
                  echo "<li>".$value."</li>";
                }
                echo "</ol>";
                ?>
                    </<?php echo $acf_tag ?>>
                    <?php
                  } else {
                  ?>
                  <<?php echo $acf_tag ?> class="dmach-acf-value"><?php echo $show_label_dis ?><?php echo implode(', ', $getselected_name); ?></<?php echo $acf_tag ?>>
                  <?php
                  }

                } else if ($acf_type == "true_false") {
                  ?>
                  <<?php echo $acf_tag ?> class="dmach-acf-value">
                    <?php echo $show_label_dis ?>
                    <?php
                    if ($acf_get['value'] == "1") {
                      echo $true_false_text_true;
                    } else {
                      echo $true_false_text_false;
                    }
                    ?></<?php echo $acf_tag ?>>
                    <?php
                  } else {
                    $getselected_name = $acf_get['value'];
                    ?>
                    <<?php echo $acf_tag ?> class="dmach-acf-value"><?php echo $show_label_dis ?><?php echo $getselected_name ?></<?php echo $acf_tag ?>>
                    <?php
                  }
                }
              }
              ////////////////////////////////////////////////////
              //  link fields
              ////////////////////////////////////////////////////

              else if ($acf_type == "link" || $acf_type == "email" || $acf_type == "dmachphone") {

                if ($link_button == "on") {
                  $link_button_css = "et_pb_button";
                } else {
                  $link_button_css = "";
                }
                

                if ($return_format == "array") {
                  $title = $acf_get['value']['title'];
                  $target = $acf_get['value']['target'];
                  $url = $acf_get['value']['url'];

                  if ($link_button_text !== "") {
                    $link_button_text_dis = $link_button_text;
                  } else {
                    $link_button_text_dis = esc_html($title);
                  }
                  ?>
                  <?php echo $show_label_dis ?>
                  <a class="dmach-acf-value <?php echo $link_button_css ?>" <?php echo $custom_icon?> href="<?php echo $url; ?>" target="<?php echo esc_html($target) ?>">
                    <span><?php echo $link_button_text_dis; ?></span>
                  </a>
                  <?php
                } else { 
                  if ($link_button_text !== "") {
                    $link_button_text_dis = $link_button_text;
                  } else {
                    $link_button_text_dis = $acf_get['value'];
                  }
                  ?>
                  <?php echo $show_label_dis ?>
                  <a class="dmach-acf-value <?php echo $link_button_css ?>" <?php echo $custom_icon?> href="<?php echo $acf_get['value']; ?>" target="<?php echo esc_html($target) ?>"><?php echo $link_button_text_dis; ?></a><?php
                }
              }
              ////////////////////////////////////////////////////
              //  POST OBJECT fields
              ////////////////////////////////////////////////////

              else if ($acf_type == "post_object") {

                $item_value = $acf_get['value'];
          
          
                ?>
                <div class="dmach-acf-item-container">
                  <?php echo $icon_image_placement_left; ?>
                  <div class="dmach-acf-item-content">
                    <?php if ($text_before != "") { ?><p class="dmach-acf-before"><?php echo $text_before ?></p><?php } ?>
          
                    <div class="dmach-acf-value">
          
                      <?php if (is_array($item_value)) {
                        // TODO:
                        // get correct posts
          
                        ?>
          
                        <div class="repeater-cont grid col-desk-<?php echo $columns ?> col-tab-<?php echo $columns_tablet ?> col-mob-<?php echo $columns_mobile ?>">
                          <div class="grid-posts loop-grid"> <?php
          
              global $post;
                $current_post = $post;
                foreach ($item_value as $item) {
                  $post = $item;
                  setup_postdata( $item );
                  ?>
                              <div class="grid-col">
                                  <div class="grid-item-cont">
                  <?php
                  echo apply_filters('the_content', get_post_field('post_content', $loop_layout));
              ?>
              </div>
              </div>
              <?php
                }
                $post = $current_post;
          ?>
          </div>
          
          
                        </div>
          
                      </div>
                      <?php
                          
                    } else {
          
                      $args_postobject = array(
                        'post_type'         => $acf_get['post_type'][0],
                        'post_status' => 'publish',
                        'posts_per_page' => '1',
                        'post__in'       => array( $item_value->ID )
                      );
          
          
          query_posts( $args_postobject );
          
          if ( have_posts() ) {
            
            while ( have_posts() ) {
              the_post();
              setup_postdata( $args_postobject );
          
              $post_content = apply_filters( 'the_content', get_post_field('post_content', $loop_layout) );

              $post_content = preg_replace( '/et_pb_section_(\d+)_tb_body/', 'et_pb_dmach_section_${1}_tb_body', $post_content );

              $post_content = preg_replace( '/et_pb_row_(\d+)_tb_body/', 'et_pb_dmach_row_${1}_tb_body', $post_content );

              $post_content = preg_replace( '/et_pb_column_(\d+)_tb_body/', 'et_pb_dmach_column_${1}_tb_body', $post_content );

          
              $post_content = preg_replace( '/et_pb_section_(\d+)/', 'et_pb_dmach_section_${1}', $post_content );

              $post_content = preg_replace( '/et_pb_row_(\d+)/', 'et_pb_dmach_row_${1}', $post_content );

            echo $post_content;

            $internal_style = ET_Builder_Element::get_style();
            // reset all the attributes after we retrieved styles
            ET_Builder_Element::clean_internal_modules_styles( false );
            $et_pb_rendering_column_content = false;

            // append styles
            if ( $internal_style ) {
              ?>
                  <div class="dmach-inner-styles">
              <?php
                      $cleaned_styles = str_replace("#et-boc .et-l","#et-boc .et-l .filtered-posts", $internal_style);
                      $cleaned_styles = str_replace(".et_pb_section_",".filtered-posts .et_pb_ajax_filter_section_", $cleaned_styles);
                      $cleaned_styles = str_replace(".et_pb_row_",".filtered-posts .et_pb_ajax_filter_row_", $cleaned_styles);
                      $cleaned_styles = str_replace(".et_pb_module_",".filtered-posts .et_pb_module_", $cleaned_styles);
                      $cleaned_styles = str_replace(".et_pb_column_",".filtered-posts .et_pb_ajax_filter_column_", $cleaned_styles);
                      $cleaned_styles = str_replace(".et_pb_de_mach_",".filtered-posts .et_pb_de_mach_", $cleaned_styles);
                      $cleaned_styles = str_replace(".filtered-posts .filtered-posts",".filtered-posts", $cleaned_styles);
                      $cleaned_styles = str_replace(".filtered-posts .et_pb_section .filtered-posts",".filtered-posts", $cleaned_styles);
              
                      printf(
                          '<style type="text/css" class="dmach_ajax_inner_styles">
                            %1$s
                          </style>',
                          et_core_esc_previously( $cleaned_styles )
                      );
              ?>
                  </div>
              <?php
                  }
          
                    } // end while have posts
          
                  }
          
                  wp_reset_query();
          
                    }
                    ?>
          
                  </div>
          
                </div>
                <?php echo $icon_image_placement_right; ?>
              </div>
              <?php
          
          
            }



            } else {
              echo 'Please select an ACF field you have created';
            }

            //////////////////////////////////////////////////
            $first = false;

            ?>
           
            <?php 
          } else {
          }
        }
      }

      $data = ob_get_clean();

      return $data;
    }


    public function get_button_alignment($device = 'desktop')
    {
      $suffix           = 'desktop' !== $device ? "_{$device}" : '';
      $text_orientation = isset($this->props["button_alignment{$suffix}"]) ? $this->props["button_alignment{$suffix}"] : '';

      return et_pb_get_alignment($text_orientation);
    }



    function render($attrs, $content = null, $render_slug)
    {

      // if (is_admin()) {
      //     return;
      // }

      $acf_name                        = $this->props['acf_name'];
      $acf_tag                        = $this->props['acf_tag'];
      // $acf_type                        = $this->props['acf_type'];
      $use_icon                        = $this->props['use_icon'];
      $font_icon                       = $this->props['font_icon'];

      $image_mobile_stacking           = $this->props['image_mobile_stacking'];
      

      $icon_color                      = $this->props['icon_color'];
      $icon_color_hover                = $this->get_hover_value('icon_color');
      $icon_color_values               = et_pb_responsive_options()->get_property_values($this->props, 'icon_color');
      $icon_color_tablet               = isset($icon_color_values['tablet']) ? $icon_color_values['tablet'] : '';
      $icon_color_phone                = isset($icon_color_values['phone']) ? $icon_color_values['phone'] : '';

      $circle_color                    = $this->props['circle_color'];
      $circle_color_hover              = $this->get_hover_value('circle_color');
      $circle_color_values             = et_pb_responsive_options()->get_property_values($this->props, 'circle_color');
      $circle_color_tablet             = isset($circle_color_values['tablet']) ? $circle_color_values['tablet'] : '';
      $circle_color_phone              = isset($circle_color_values['phone']) ? $circle_color_values['phone'] : '';

      $show_label                      = $this->props['show_label'];
      $custom_label                    = $this->props['custom_label'];
      $label_seperator                 = $this->props['label_seperator'];
      $image                           = $this->props['image'];
      $alt                             = $this->props['alt'];
      $icon_image_placement            = $this->props['icon_image_placement'];
      $use_circle                      = $this->props['use_circle'];
      $use_circle_border               = $this->props['use_circle_border'];

      $circle_border_color             = $this->props['circle_border_color'];
      $circle_border_color_hover       = $this->get_hover_value('circle_border_color');
      $circle_border_color_values      = et_pb_responsive_options()->get_property_values($this->props, 'circle_border_color');
      $circle_border_color_tablet      = isset($circle_border_color_values['tablet']) ? $circle_border_color_values['tablet'] : '';
      $circle_border_color_phone       = isset($circle_border_color_values['phone']) ? $circle_border_color_values['phone'] : '';

      $icon_placement                  = $this->props['icon_image_placement'];
      $icon_placement_values           = et_pb_responsive_options()->get_property_values($this->props, 'icon_placement');
      $icon_placement_tablet           = isset($icon_placement_values['tablet']) ? $icon_placement_values['tablet'] : '';
      $icon_placement_phone            = isset($icon_placement_values['phone']) ? $icon_placement_values['phone'] : '';
      $is_icon_placement_responsive    = et_pb_responsive_options()->is_responsive_enabled($this->props, 'icon_placement');
      $is_icon_placement_top           = !$is_icon_placement_responsive ? 'top' === $icon_placement : in_array('top', $icon_placement_values);

      $return_format                   = $this->props['return_format'];
      // $choice_return_format            = $this->props['choice_return_format'];
      $image_size                      = $this->props['image_size'];
      $prefix                      = $this->props['prefix'];
      $suffix                      = $this->props['suffix'];
      $text_before                     = $this->props['text_before'];
      $true_false_text_true            = $this->props['true_false_text_true'];
      $true_false_text_false           = $this->props['true_false_text_false'];

      $image_max_width                 = $this->props['image_max_width'];
      $image_max_width_tablet          = $this->props['image_max_width_tablet'];
      $image_max_width_phone           = $this->props['image_max_width_phone'];
      $image_max_width_last_edited     = $this->props['image_max_width_last_edited'];

      $loop_layout                     = $this->props['loop_layout'];

      $linked_post_style                     = $this->props['linked_post_style'];
      $link_post_seperator                     = $this->props['link_post_seperator'];
 
      $columns                = $this->props['columns'];
      $columns_tablet         = $this->props['columns_tablet'];
      $columns_mobile         = $this->props['columns_mobile'];

      $pretify_text     = $this->props['pretify_text'];
      $pretify_seperator     = $this->props['pretify_seperator'];

      $link_button    = $this->props['link_button'];
      $link_button_text    = $this->props['link_button_text'];
      $url_link_icon    = $this->props['url_link_icon'];

      
      $link_new_tab    = $this->props['link_new_tab'];

      if ($link_new_tab == "on") {
        $link_new_tab_dis = 'target="_blank"';
      } else {
        $link_new_tab_dis = '';
      }

      $checkbox_style    = $this->props['checkbox_style'];

      $use_icon_font_size            = $this->props['use_icon_font_size'];
      $icon_font_size                = $this->props['icon_font_size'];
      $button_use_icon          	= $this->props['button_use_icon'];

      $custom_icon          		= $this->props['button_icon'];
      $button_custom        		= $this->props['custom_button'];
      $button_bg_color       		= $this->props['button_bg_color'];
      $button_bg_hover_color          = $this->props['button_bg_color__hover'];

      $button_alignment                = $this->get_button_alignment();
      $button_alignments = sprintf('et_pb_de_mach_alignment_%1$s', esc_attr($button_alignment));

      $this->add_classname($button_alignments);

      $font_icon_rendered = DEDMACH_INIT::et_icon_css_content(esc_attr($font_icon));

      $is_image_svg   = isset($image_pathinfo['extension']) ? 'svg' === $image_pathinfo['extension'] : false;

      $icon_selector = '%%order_class%% .dmach-icon';

      if ('' !== $image_max_width_tablet || '' !== $image_max_width_phone || '' !== $image_max_width || $is_image_svg) {
        $is_size_px = false;

        // If size is given in px, we want to override parent width
        if (
          false !== strpos($image_max_width, 'px') ||
          false !== strpos($image_max_width_tablet, 'px') ||
          false !== strpos($image_max_width_phone, 'px')
        ) {
          $is_size_px = true;
        }
        // SVG image overwrite. SVG image needs its value to be explicit
        if ('' === $image_max_width && $is_image_svg) {
          $image_max_width = '100%';
        }

        // Image max width selector.
        $image_max_width_selectors       = array();
        $image_max_width_reset_selectors = array();
        $image_max_width_reset_values    = array();

        $image_max_width_selector = $is_image_svg ? '%%order_class%% .dmach-icon-image-content' : '%%order_class%% .dmach-icon-image-content';


        foreach (array('tablet', 'phone') as $device) {
          $device_icon_placement = 'tablet' === $device ? $icon_placement_tablet : $icon_placement_phone;
          if (empty($device_icon_placement)) {
            continue;
          }

          $image_max_width_selectors[$device] = 'top' === $device_icon_placement && $is_image_svg ? '%%order_class%% .dmach-icon-image-content' : '%%order_class%% .dmach-icon-image-content';

          $prev_icon_placement = 'tablet' === $device ? $icon_placement : $icon_placement_tablet;
          if (empty($prev_icon_placement) || $prev_icon_placement === $device_icon_placement || !$is_image_svg) {
            continue;
          }

          // Image/icon placement setting is related to image width setting. In some cases,
          // user uses different image/icon placement settings for each devices. We need to
          // reset previous device image width styles to make it works with current style.
          $image_max_width_reset_selectors[$device] = '%%order_class%% .dmach-icon-image-content';
          $image_max_width_reset_values[$device]    = array('width' => '32px');

          if ('top' === $device_icon_placement) {
            $image_max_width_reset_selectors[$device] = '%%order_class%% .dmach-icon-image-content';
            $image_max_width_reset_values[$device]    = array('width' => 'auto');
          }
        }

        // Add image max width desktop selector if user sets different image/icon placement setting.
        if (!empty($image_max_width_selectors)) {
          $image_max_width_selectors['desktop'] = $image_max_width_selector;
        }

        $image_max_width_property = ($is_image_svg || $is_size_px) ? 'width' : 'max-width';

        $image_max_width_responsive_active = et_pb_get_responsive_status($image_max_width_last_edited);

        $image_max_width_values = array(
          'desktop' => $image_max_width,
          'tablet'  => $image_max_width_responsive_active ? $image_max_width_tablet : '',
          'phone'   => $image_max_width_responsive_active ? $image_max_width_phone : '',
        );

        $main_image_max_width_selector = $image_max_width_selector;

        // Overwrite image max width if there are image max width selectors for different devices.
        if (!empty($image_max_width_selectors)) {
          $main_image_max_width_selector = $image_max_width_selectors;

          if (!empty($image_max_width_selectors['tablet']) && empty($image_max_width_values['tablet'])) {
            $image_max_width_values['tablet'] = $image_max_width_responsive_active ? esc_attr(et_pb_responsive_options()->get_any_value($this->props, 'image_max_width_tablet', '100%', true)) : esc_attr($image_max_width);
          }

          if (!empty($image_max_width_selectors['phone']) && empty($image_max_width_values['phone'])) {
            $image_max_width_values['phone'] = $image_max_width_responsive_active ? esc_attr(et_pb_responsive_options()->get_any_value($this->props, 'image_max_width_phone', '100%', true)) : esc_attr($image_max_width);
          }
        }


        et_pb_responsive_options()->generate_responsive_css($image_max_width_values, $main_image_max_width_selector, $image_max_width_property, $render_slug);

        // Reset custom image max width styles.
        if (!empty($image_max_width_selectors) && !empty($image_max_width_reset_selectors)) {
          et_pb_responsive_options()->generate_responsive_css($image_max_width_reset_values, $image_max_width_reset_selectors, $image_max_width_property, $render_slug, '', 'input');
        }
      }


      if ('off' === $use_icon) {
      } else {
        $icon_style        = sprintf('color: %1$s;', esc_attr($icon_color));
        $icon_tablet_style = '' !== $icon_color_tablet ? sprintf('color: %1$s;', esc_attr($icon_color_tablet)) : '';
        $icon_phone_style  = '' !== $icon_color_phone ? sprintf('color: %1$s;', esc_attr($icon_color_phone)) : '';
        $icon_style_hover  = '';

        if (et_builder_is_hover_enabled('icon_color', $this->props)) {
          $icon_style_hover = sprintf('color: %1$s;', esc_attr($icon_color_hover));
        }

        if ('on' === $use_circle) {
          $icon_style .= sprintf(' background-color: %1$s;', esc_attr($circle_color));
          $icon_tablet_style .= '' !== $circle_color_tablet ? sprintf(' background-color: %1$s;', esc_attr($circle_color_tablet)) : '';
          $icon_phone_style  .= '' !== $circle_color_phone ? sprintf(' background-color: %1$s;', esc_attr($circle_color_phone)) : '';

          if (et_builder_is_hover_enabled('circle_color', $this->props)) {
            $icon_style_hover .= sprintf(' background-color: %1$s;', esc_attr($circle_color_hover));
          }

          if ('on' === $use_circle_border) {
            $icon_style .= sprintf(' border-color: %1$s;', esc_attr($circle_border_color));
            $icon_tablet_style .= '' !== $circle_border_color_tablet ? sprintf(' border-color: %1$s;', esc_attr($circle_border_color_tablet)) : '';
            $icon_phone_style  .= '' !== $circle_border_color_phone ? sprintf(' border-color: %1$s;', esc_attr($circle_border_color_phone)) : '';

            if (et_builder_is_hover_enabled('circle_border_color', $this->props)) {
              $icon_style_hover .= sprintf(' border-color: %1$s;', esc_attr($circle_border_color_hover));
            }
          }
        }

        ET_Builder_Element::set_style($render_slug, array(
          'selector'    => '%%order_class%% .dmach-icon:before',
          'declaration' => sprintf(
            'content: "%1$s";
            color: %2$s;',
            esc_html($font_icon_rendered),
            $icon_color
          ),
        ));

        ET_Builder_Element::set_style($render_slug, array(
          'selector'    => '%%order_class%% .dmach-icon:hover:before',
          'declaration' => sprintf(
            'color: %1$s',
            $icon_color_hover
          ),
        ));

        ET_Builder_Element::set_style($render_slug, array(
          'selector'    => $icon_selector,
          'declaration' => $icon_style,
        ));

        ET_Builder_Element::set_style($render_slug, array(
          'selector'    => $icon_selector,
          'declaration' => $icon_tablet_style,
          'media_query' => ET_Builder_Element::get_media_query('max_width_980'),
        ));

        ET_Builder_Element::set_style($render_slug, array(
          'selector'    => $icon_selector,
          'declaration' => $icon_phone_style,
          'media_query' => ET_Builder_Element::get_media_query('max_width_767'),
        ));


        if ('' !== $icon_style_hover) {
          ET_Builder_Element::set_style($render_slug, array(
            'selector'    => $this->add_hover_to_order_class($icon_selector),
            'declaration' => $icon_style_hover,
          ));
        }

        $image_classes[] = 'et-pb-icon';

        if ('on' === $use_circle) {
          $image_classes[] = 'et-pb-icon-circle';
        }

        if ('on' === $use_circle && 'on' === $use_circle_border) {
          $image_classes[] = 'et-pb-icon-circle-border';
        }
      
        if ( 'off' !== $use_icon_font_size ) {
          
          ET_Builder_Element::set_style($render_slug, array(
            'selector'    => '%%order_class%% .dmach-icon:before',
            'declaration' => sprintf(
              'font-size: %1$s;',
              $icon_font_size
            ),
          ));
    
          if ( et_builder_is_hover_enabled( 'icon_font_size', $this->props ) ) {
            $el_style = array(
              'selector'    => $this->add_hover_to_order_class( $icon_selector ),
              'declaration' => sprintf(
                'font-size: %1$s;',
                esc_html( $icon_font_size_hover )
              ),
            );
            ET_Builder_Element::set_style( $render_slug, $el_style );
          }
        }
      
      }

      
      if( $button_use_icon == 'on' && $custom_icon != '' ){
  			$custom_icon = 'data-icon="'. esc_attr( et_pb_process_font_icon( $custom_icon ) ) .'"';
  			ET_Builder_Element::set_style( $render_slug, array(
  				'selector'    => 'body #page-container %%order_class%% .et_pb_button:after',
  				'declaration' => "content: attr(data-icon);",
  			) );
  		}else{
  			ET_Builder_Element::set_style( $render_slug, array(
  				'selector'    => 'body #page-container %%order_class%% .et_pb_button:hover',
  				'declaration' => "padding: .3em 1em;",
  			) );
  		}

  		if( !empty( $button_bg_color ) ){

  			ET_Builder_Element::set_style( $render_slug, array(
  				'selector'    => 'body #page-container %%order_class%% .et_pb_button',
  				'declaration' => "background-color:". esc_attr( $button_bg_color ) ."!important;",
  			) );
  		}

      if( !empty( $button_bg_hover_color ) ){

        ET_Builder_Element::set_style( $render_slug, array(
          'selector'    => 'body #page-container %%order_class%% .et_pb_button:hover',
          'declaration' => "background-color:". esc_attr( $button_bg_hover_color ) ."!important;",
        ) );
      }


      ET_Builder_Element::set_style( $render_slug, array(
        'selector'    => 'body #page-container %%order_class%% .dmach-acf-item-container',
        'declaration' => "flex-direction:". esc_attr( $image_mobile_stacking ) .";",
        'media_query' => ET_Builder_Element::get_media_query( 'max_width_980' ),
      ) );

      
      //////////////////////////////////////////////////////////////////////

      ob_start();


      if (get_field($acf_name)) {
        $acf_get = get_field_object($acf_name);
        $noacf = false;
      } else if (get_sub_field($acf_name)) {
        $acf_get = get_sub_field_object($acf_name);
        $noacf = false;
      } else {
        $noacf = true;
      }

      if (is_archive() && $noacf == true) { // on category page and looking for tax ACF item
      $term = get_queried_object();
      $acf_get = get_field_object($acf_name, $term);
      $noacf = false;
      }

      if ($noacf == false) {
        

        $acf_type = $acf_get['type'];


        

    include(DE_DMACH_PATH . '/titan-framework/titan-framework-embedder.php');
    $titan = TitanFramework::getInstance( 'divi-machine' );
    $enable_debug = $titan->getOption( 'enable_debug' );

    if ($enable_debug == "1") {
      echo '<p class="reporting_args hidethis">ACF Type: ' . $acf_type . '</p>';
    }

        if ($acf_get['value'] == "") {
          $this->add_classname("hidethis");
        } else {
          $this->add_classname("dmach-acf-has-value");
        }

        $label_seperator = '<span class="dmach-seperator">' . esc_html($label_seperator) . '</span>';

        if ($show_label == "on") {
          if ($custom_label == "") {
            $show_label_dis =  $acf_get['label'] . $label_seperator;
          } else {
            $show_label_dis =  $custom_label . $label_seperator;
          }
          $show_label_dis = '<span class="dmach-acf-label">' . $show_label_dis . '</span>';
        } else {
          $show_label_dis = "";
        }

        if ($use_icon == "on") {
          $icon_image_dis = '<div class="dmach-icon-image-content"><span class="dmach-icon"></span></div>';
        } else {
          if ($image == "") {
            $icon_image_dis = "";
          } else {
            $icon_image_dis = '<div class="dmach-icon-image-content"><img src="' . $image . '"></div>';
          }
        }

        if ($icon_image_placement == "top" || $icon_image_placement == "left") {
          $icon_image_placement_left = $icon_image_dis;
          $icon_image_placement_right = "";
        } else {
          $icon_image_placement_left = "";
          $icon_image_placement_right = $icon_image_dis;
        }


        $this->add_classname('dmach-image-icon-placement-' . $icon_image_placement . '');

        $basic_acf_types = "text, textarea, number, range, password, wysiwyg, oembed, date_picker, date_time_picker, time_picker";
        $basic_acf_types_explode = explode(', ', $basic_acf_types);

        $choice_acf_choice_types = "select, checkbox, radio, button_group, true_false";
        $choice_acf_choice_types_explode = explode(', ', $choice_acf_choice_types);

    
        if ( isset($acf_get['prepend']) && $acf_get['prepend'] !== "") {
          $prepend = '<span class="acf_prepend">' . $acf_get['prepend'] . '</span>';
        } else {
          $prepend = "";
        }
 

    
        if ( isset($acf_get['append']) && $acf_get['append'] !== "") {
          $append = '<span class="acf_append">' . $acf_get['append'] . '</span>';
        } else {
          $append = "";
        }


        ////////////////////////////////////////////////////
        // basic fields
        ////////////////////////////////////////////////////
        if (in_array($acf_type, $basic_acf_types_explode)) {


          if ($pretify_text == "on" && ($acf_type == 'number' || $acf_type == 'range')) {

            $item_value_before = number_format($acf_get['value'], 0, '.', ',');
            $item_value = str_replace(',', $pretify_seperator, $item_value_before);
            $item_value = $prepend . $item_value . $append;
          } else {
            $item_value = $prepend . $acf_get['value'] . $append;
          }



          ?>
          <div class="dmach-acf-item-container">
            <?php echo $icon_image_placement_left; ?>
            <div class="dmach-acf-item-content">
              <?php if ($text_before != "") { ?><p class="dmach-acf-before"><?php echo $text_before ?></p><?php } ?>

            <?php if ($acf_tag == 'img') {

              ?>
              <img class="dmach-acf-value" src="<?php echo $item_value; ?>" />
              <?php 

            } else {

              ?>
              <<?php echo $acf_tag ?> class="dmach-acf-value"><?php echo $show_label_dis ?><?php echo $prefix; ?><?php echo $item_value; ?><?php echo $suffix; ?></<?php echo $acf_tag ?>>
              <?php 
            
            } ?>

            </div>
            <?php echo $icon_image_placement_right; ?>
          </div>
          <?php
        }

        ////////////////////////////////////////////////////
        // image fields
        ////////////////////////////////////////////////////

        else if ($acf_type == "image") {

        

          if (!empty($acf_get['value'])) :
            ?>
            <div class="dmach-acf-item-container">
              <?php echo $icon_image_placement_left; ?>
              <div class="dmach-acf-item-content">
                <?php if ($text_before != "") { ?><p class="dmach-acf-before"><?php echo $text_before ?></p><?php } ?>
                <?php if ($return_format == "array") {
                  
            

                  $thumb = $acf_get['value']['sizes'][$image_size];

                  if ($thumb == "") {
                    $thumb = $acf_get['value']['sizes']['large'];
                  }

                  ?>
                  <?php echo $show_label_dis ?> <img class="dmach-acf-value" src="<?php echo esc_url($thumb); ?>" alt="<?php echo esc_attr($acf_get['value']['alt']); ?>" />
                <?php } else if ($return_format == "id") {
                  echo wp_get_attachment_image($acf_get['value'], $image_size);
                } else if ($return_format == "id") {
                  echo wp_get_attachment_image($acf_get['value'], $image_size);
                } else { ?>
                  <?php echo $show_label_dis ?> <img class="dmach-acf-value" src="<?php echo $acf_get['value']; ?>" />
                <?php } ?>
              </div>
              <?php echo $icon_image_placement_right; ?>
            </div>
          <?php endif;
        }

        ////////////////////////////////////////////////////
        // file fields
        ////////////////////////////////////////////////////

        else if ($acf_type == "file") {

          
          if ($link_button == "on") {
            $link_button_css = "et_pb_button";
          } else {
            $link_button_css = "";
          }

          $return_format = $acf_get['return_format'];

          if ($acf_get['value']) : 

            if ($return_format == "array") {
              $link_url = $acf_get['value']['url'];
            } else if ($return_format == "id") {
              $link_url = wp_get_attachment_url( $acf_get['value'] );
            } else { 
              $link_url = $acf_get['value'];
            }

            $is_video = false;

            $mime_type = wp_check_filetype($link_url);
            if ( preg_match('/video\/*/', $mime_type['type'] ) ){
              $is_video = true;
            }

            if ($url_link_icon == 'on' && !$is_video) {

            
            ?> <a href="<?php echo $link_url; ?>" <?php echo $link_new_tab_dis;?>> <?php
            }
          ?>

          <div class="dmach-acf-item-container <?php echo ($is_video)?'dmach-acf-video-container':'';?>">
            <?php echo $icon_image_placement_left; ?>
            <div class="dmach-acf-item-content">
              <?php if ($text_before != "") { ?><p class="dmach-acf-before"><?php echo $text_before ?></p><?php } ?>
              <?php 
                if ($link_button_text !== "") {
                  $link_button_text_dis = $link_button_text;
                } else {
                  $link_button_text_dis = "Download File";
                }
                ?>


              <?php if ($return_format == "array") {
                $title = $acf_get['value']['title'];
                $icon = $acf_get['value']['icon'];
                if ($acf_get['value']['type'] == 'image') {
                  $icon =  $acf_get['value']['sizes']['thumbnail'];
                }
                ?>
                <?php echo $show_label_dis ?>

                <?php 
                if ($link_button_text !== "") {
                  $link_button_text_dis = $link_button_text;
                } else {
                  $link_button_text_dis = esc_html($title);
                }
                ?>

                <?php 
                if ($url_link_icon == 'off' && !$is_video) {
                ?>
                <a class="dmach-acf-value <?php echo $link_button_css ?>" <?php echo $custom_icon?> href="<?php echo $acf_get['value']['url']; ?>" <?php echo $link_new_tab_dis;?>>
                  <span><?php echo $link_button_text_dis; ?></span>
                </a>
                <?php
              }

              } else if ($return_format == "id") {
                
                if ($url_link_icon == 'off' && !$is_video) {
                $url = wp_get_attachment_url($acf_get['value']); ?>
                <?php echo $show_label_dis ?>
                <a class="dmach-acf-value <?php echo $link_button_css ?>" <?php echo $custom_icon?> href="<?php echo esc_html($url); ?>" <?php echo $link_new_tab_dis;?>><?php echo $link_button_text_dis; ?></a> 
                <?php
                }

              } else { 
                if ($url_link_icon == 'off' && !$is_video) {
                echo $show_label_dis ?>
                <a class="dmach-acf-value <?php echo $link_button_css ?>" <?php echo $custom_icon?> href="<?php echo $acf_get['value']; ?>" <?php echo $link_new_tab_dis;?>><?php echo $link_button_text_dis; ?></a> <?php
                }
              }
         
              ?>
              <?php 
              if ( $is_video ){

                $video_loop = ($this->props['video_loop'] == 'on')?true:false;
                $video_autoplay = ($this->props['video_autoplay'] == 'on')?true:false;
                echo '<div class="dmach-acf-video-wrapper">';

                $video_args = array(
                  $mime_type['ext'] => $link_url,
                  'preload' => 'auto',
                  'autoplay' => $video_autoplay,
                  'loop'    => $video_loop
                );
                echo wp_video_shortcode( $video_args );

                if ( !$video_autoplay ){
                  $video_thumbnail = $this->props['video_thumbnail'];
                  $video_icon_color = $this->props['video_icon_color'];
                  $video_icon_font_size = ( $this->props['video_icon_font_size'] == 'on' )?true:false;
                  if ( $video_icon_font_size ){
                    $video_icon_custom_size = $this->props['video_icon_custom_size'];
                    $current_style = "color:".$video_icon_color.";font-size:" . $video_icon_custom_size;
                  }
                ?>
                <div class="dmach-acf-video-poster">
                  <img src="<?php echo $video_thumbnail;?>" class="poster"/>
                  <a href="#" class="dmach-acf-video-play" style="<?php echo $current_style;?>"></a>
                </div>
                <script>
                  jQuery(document).ready(function($){
                    $('.dmach-acf-video-play').click(function(e){
                      e.preventDefault();
                      $(this).closest('.dmach-acf-video-wrapper').addClass('playing');
                      $(this).closest('.dmach-acf-video-wrapper').find('video')[0].play();
                    });
                    $('.dmach-acf-video-wrapper video').on('pause',function(){
                      $(this).closest('.dmach-acf-video-wrapper').removeClass('playing');
                    });
                  });
                </script>
                <?php
                }

                echo '</div>';
              }
              ?>
            </div>
            <?php echo $icon_image_placement_right; ?>

            <?php if ($url_link_icon == 'on' && !$is_video ) {
            ?> </a> <?php
            }
             ?>

          </div>
          <?php
          endif;
        }
      ////////////////////////////////////////////////////
      // Choice fields
      ////////////////////////////////////////////////////

      else if (in_array($acf_type, $choice_acf_choice_types_explode)) {
        if ($acf_get['value']) :


          ?>
          <div class="dmach-acf-item-container">
            <?php echo $icon_image_placement_left; ?>
            <div class="dmach-acf-item-content">
              <?php
              if ( isset($acf_get['multiple']) && $acf_get['multiple'] == "1") {
                if (is_array($acf_get['value'])) {  
                  $getselected_name = $acf_get['value'];
                  ?>
                  <?php if ($text_before != "") { ?>< class="dmach-acf-before"><?php echo $text_before ?></  p><?php } ?>
                  <<?php echo $acf_tag ?> class="dmach-acf-value"><?php echo $show_label_dis ?><?php echo implode(', ',  $getselected_name); ?></<?php echo $acf_tag ?>>
                  <?php
                }
              } else {
                if ($acf_type == "checkbox") {
                  $getselected_name = $acf_get['value'];
                  if ($checkbox_style == "bullet") {
                    ?>
                    <<?php echo $acf_tag ?> class="dmach-acf-value"><?php echo $show_label_dis ?>
                    <?php
                echo "<ul>";
                foreach ($getselected_name as $key => $value) {
                  echo "<li>".$value."</li>";
                }
                echo "</ul>";
                ?>
                    </<?php echo $acf_tag ?>>
                    <?php
                  } else if ($checkbox_style == "numbered") {
                    ?>
                    <<?php echo $acf_tag ?> class="dmach-acf-value"><?php echo $show_label_dis ?>
                    <?php
                echo "<ol>";
                foreach ($getselected_name as $key => $value) {
                  echo "<li>".$value."</li>";
                }
                echo "</ol>";
                ?>
                    </<?php echo $acf_tag ?>>
                    <?php
                  } else {
                  ?>
                  <<?php echo $acf_tag ?> class="dmach-acf-value"><?php echo $show_label_dis ?><?php echo implode(', ', $getselected_name); ?></<?php echo $acf_tag ?>>
                  <?php
                  }
                } else if ($acf_type == "true_false") {
                  ?>
                  <?php if ($text_before != "") { ?><p class="dmach-acf-before"><?php echo $text_before ?></p><?php } ?>
                  <<?php echo $acf_tag ?> class="dmach-acf-value">
                    <?php echo $show_label_dis ?>
                    <?php
                    if ($acf_get['value'] == "1") {
                      echo $true_false_text_true;
                    } else {
                      echo $true_false_text_false;
                    }
                    ?></<?php echo $acf_tag ?>>
                    <?php


                  } else {
                    $getselected_name = $acf_get['value'];
                    ?>
                    <?php if ($text_before != "") { ?><p class="dmach-acf-before"><?php echo $text_before ?></p><?php } ?>
                    <<?php echo $acf_tag ?> class="dmach-acf-value"><?php echo $show_label_dis ?><?php echo $getselected_name ?></<?php echo $acf_tag ?>>
                    <?php
                  }
                }
                ?>
              </div>
              <?php echo $icon_image_placement_right; ?>
            </div>
            <?php
          endif;
        }

        ////////////////////////////////////////////////////
        //  link fields
        ////////////////////////////////////////////////////

        else if ($acf_type == "link") {

          if ($link_button == "on") {
            $link_button_css = "et_pb_button";
          } else {
            $link_button_css = "";
          }

          if ($acf_get['value']) : ?>

          <?php if ($url_link_icon == 'on') {
            ?> <a href="<?php echo $acf_get['value']; ?>" target="_blank"> <?php
          }
          ?>

          <div class="dmach-acf-item-container">
            <?php echo $icon_image_placement_left; ?>
            <div class="dmach-acf-item-content">
              <?php if ($text_before != "") { ?><p class="dmach-acf-before"><?php echo $text_before ?></p><?php } ?>

              <?php if ($return_format == "array") {
                $title = $acf_get['value']['title'];
                $target = $acf_get['value']['target'];
                $url = $acf_get['value']['url'];

                if ($link_button_text !== "") {
                  $link_button_text_dis = $link_button_text;
                } else {
                  $link_button_text_dis = esc_html($title);
                }

                ?>
                <?php echo $show_label_dis ?>
                <a class="dmach-acf-value <?php echo $link_button_css ?>" <?php echo $custom_icon?> href="<?php echo $url; ?>" <?php echo $link_new_tab_dis ?>">
                  <span><?php echo $link_button_text_dis; ?></span>
                </a>
                <?php
              } else { ?>
                <?php echo $show_label_dis ?>
            <?php if ($url_link_icon == 'off') {

if ($link_button_text !== "") {
  $link_button_text_dis = $link_button_text;
} else {
  $link_button_text_dis = $acf_get['value'];
}


              ?>
                <a class="dmach-acf-value <?php echo $link_button_css ?>" <?php echo $custom_icon?> href="<?php echo $acf_get['value']; ?>" <?php echo $link_new_tab_dis ?>"><?php echo $link_button_text_dis; ?></a>
              <?php
            }
              }
              ?>
            </div>
            <?php echo $icon_image_placement_right; ?>

            <?php if ($url_link_icon == 'on') {
            ?> </a> <?php
            }
             ?>

          </div>
          <?php
        endif;
      }

             ////////////////////////////////////////////////////
        //  Email or phone fields
        ////////////////////////////////////////////////////

        else if ($acf_type == "email" || $acf_type == "dmachphone") {
          

          if ($acf_type == "email") {
            $linktype = 'mailto';
          } else {
            $linktype = 'tel';
          }
          
          if ($link_button == "on") {
            $link_button_css = "et_pb_button";
          } else {
            $link_button_css = "";
          }
          
          if ($acf_get['value']) : ?>
          <?php if ($url_link_icon == 'on') {
            ?> <a href="<?php echo $linktype ?>:<?php echo $acf_get['value']; ?>" target="_blank"> <?php
          }
          ?>
          
          <div class="dmach-acf-item-container">
          <?php echo $icon_image_placement_left; ?>
          <div class="dmach-acf-item-content">
          <?php if ($text_before != "") { ?><p class="dmach-acf-before"><?php echo $text_before ?></p><?php } ?>
          <?php echo $show_label_dis ?>
          <?php if ($url_link_icon == 'off') {
            if ($link_button_text !== "") {
              $link_button_text_dis = $link_button_text;
            } else {
              $link_button_text_dis = $acf_get['value'];
            }
            
            ?>
            <a class="dmach-acf-value <?php echo $link_button_css ?>" <?php echo $custom_icon?> href="<?php echo $linktype ?>:<?php echo $acf_get['value']; ?>" target="_blank"><?php echo $link_button_text_dis; ?></a>
            <?php
            }
            ?>
            </div>
            <?php echo $icon_image_placement_right; ?>

            <?php if ($url_link_icon == 'on') {
            ?> </a> <?php
            }
             ?>

          </div>
          <?php
        endif;
      }

      ////////////////////////////////////////////////////
      //  URL fields
      ////////////////////////////////////////////////////

      else if ($acf_type == "url") {

        if ($link_button == "on") {
          $link_button_css = "et_pb_button";
        } else {
          $link_button_css = "";
        }

        
        if ($link_button_text !== "") {
          $link_button_text_dis = $link_button_text;
        } else {
          $link_button_text_dis = $acf_get['value'];
        }

        if ($acf_get['value']) : ?>
        <div class="dmach-acf-item-container">

          <?php if ($url_link_icon == 'on') {
            ?> <a href="<?php echo $acf_get['value']; ?>" <?php echo $link_new_tab_dis ?>> <?php
          }
          ?>
          <?php echo $icon_image_placement_left; ?>
          <div class="dmach-acf-item-content">
            <?php if ($text_before != "") { ?><p class="dmach-acf-before"><?php echo $text_before ?></p><?php } ?>
            <?php echo $show_label_dis ?>
            <?php if ($url_link_icon == 'off') {
              ?>
              <a class="dmach-acf-value <?php echo $link_button_css ?>" <?php echo $custom_icon?> href="<?php echo $acf_get['value']; ?>" <?php echo $link_new_tab_dis ?>><?php echo $link_button_text_dis; ?></a>
              <?php
            }
            ?>
          </div>
          <?php echo $icon_image_placement_right; ?>
          <?php if ($url_link_icon == 'on') {
            ?> </a> <?php
          }
          ?>
        </div>
        <?php
      endif;
    }
    ////////////////////////////////////////////////////
    //  post_object fields
    ////////////////////////////////////////////////////

    else if ($acf_type == "post_object") {

      $item_value = $acf_get['value'];


      ?>
      <div class="dmach-acf-item-container">
        <?php echo $icon_image_placement_left; ?>
        <div class="dmach-acf-item-content">
          <?php if ($text_before != "") { ?><p class="dmach-acf-before"><?php echo $text_before ?></p><?php } ?>

          <div class="dmach-acf-value">

            <?php if (is_array($item_value)) {

               $item_value_arr = array(); 
               foreach ($item_value as $item) {
               $item_value_arr[] = $item->ID;
               $post_type = $acf_get['post_type'][0];
               }
               
            } else {

              $item_value_arr = array( $item_value->ID );
              $post_type = $acf_get['post_type'][0];

            }

            $args_postobject = array(
              'post_type'         => $post_type,
              'post_status' => 'publish',
              'posts_per_page' => '-1',
              'post__in'       => $item_value_arr
            );

            if ($linked_post_style == 'custom') {
              ?>
              <div class="repeater-cont grid col-desk-<?php echo $columns ?> col-tab-<?php echo $columns_tablet ?> col-mob-<?php echo $columns_mobile ?>">
                <div class="grid-posts loop-grid"> 
                <?php
            }

                  $new_query = new WP_Query( $args_postobject );
                  if ( $new_query->have_posts() ) {
                    $post_counter = 0;
                    while ( $new_query->have_posts() ) {
                      $new_query->the_post();
                      $post_counter++; 
                      $post_count = $new_query->post_count; 

                      if ($linked_post_style == 'custom') {
                      ?>
                      <div class="grid-col">
                      <div class="grid-item-cont">
                      <?php
                      echo apply_filters('the_content', get_post_field('post_content', $loop_layout));
                      ?>
                      </div>
                      </div>
                      <?php
                      } else {
                        $url = get_permalink( get_the_ID() );
                        ?>
                        <span class="linked_list_item">
                          <a href="<?php echo esc_html($url)?>"><?php echo esc_html(get_the_title()) ?></a><?php if ( $post_counter == $post_count ) { } else { echo '<span class="dmach-seperator">' . esc_html($link_post_seperator) . '</span>';}
                          ?>
                        </span>
                        <?php
                      }
                      }
                    }
                    //wp_reset_query();
                    wp_reset_postdata();
                    ?>
                    <?php 
                    if ($linked_post_style == 'custom') {
                      ?>
                      </div>
                      </div>
                      <?php 
                    }
                  ?>
            </div>
        </div>
    
      <?php echo $icon_image_placement_right; ?>
    </div>
    <?php


  }
} else {
  $this->add_classname("hidethis");
}



$data = ob_get_clean();
//////////////////////////////////////////////////////////////////////


return $data;


}
}

new de_mach_acf_item_code;

?>
