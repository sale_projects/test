<?php
if ( ! defined( 'ABSPATH' ) ) exit;

class de_mach_orderby_item_code extends ET_Builder_Module {

public $vb_support = 'on';

protected $module_credits = array(
  'module_uri' => DE_DMACH_PRODUCT_URL,
  'author'     => DE_DMACH_AUTHOR,
  'author_uri' => DE_DMACH_URL,
);

                function init() {
                    $this->name       = esc_html__( 'Orderby Item', 'divi-machine' );
                    $this->slug = 'et_pb_de_mach_orderby_item';
                		$this->vb_support      = 'on';
                		$this->type                        = 'child';
                		$this->child_title_var             = 'title';
                    $this->advanced_setting_title_text = esc_html__( 'New Orderby Item', 'et_builder' );
                		$this->settings_text               = esc_html__( 'Orderby Item Settings', 'et_builder' );


                    $this->fields_defaults = array(
                    // 'loop_layout'         => array( 'on' ),
                    );

          $this->settings_modal_toggles = array(
      			'general' => array(
      				'toggles' => array(
      					'main_content' => esc_html__( 'Main Options', 'divi-machine' ),
        					'layout' => esc_html__( 'Layout Options', 'divi-machine' ),
        				'text_filter' => esc_html__( 'Text Filter Options', 'divi-machine' ),
        				'select_filter' => esc_html__( 'Select Filter Options', 'divi-machine' ),
        				'radio_filter' => esc_html__( 'Checkbox / radio Filter Options', 'divi-machine' ),
        				'range_filter' => esc_html__( 'Number Range Filter Options', 'divi-machine' ),
        				'filter_style' => esc_html__( 'Filter Style', 'divi-machine' ),
      				),
      			),
      			'advanced' => array(
      				'toggles' => array(
      					'text' => esc_html__( 'Text', 'divi-machine' ),
      				),
      			),

      		);


                      $this->main_css_element = '%%order_class%%';


                      $this->advanced_fields = array(
                  			'fonts' => array(
                  				'title' => array(
                  					'label'    => esc_html__( 'Item', 'divi-machine' ),
                  					'css'      => array(
                  						'main' => "%%order_class%% ul.products li.product .woocommerce-loop-product__title",
                  						'important' => 'plugin_only',
                  					),
                  					'font_size' => array(
                  						'default' => '14px',
                  					),
                  					'line_height' => array(
                  						'default' => '1em',
                  					),
                  				),
                    				'radio_button_text' => array(
                    					'label'    => esc_html__( 'Checkbox / Radio Button Style', 'divi-machine' ),
                    					'css'      => array(
                    						'main' => "%%order_class%% .dmach-radio-buttons .et_pb_contact_field_radio label",
                    						'important' => 'plugin_only',
                    					),
                    					'font_size' => array(
                    						'default' => '14px',
                    					),
                    					'line_height' => array(
                    						'default' => '1em',
                    					),
                    				),
                    				'radio_button_text_checked' => array(
                    					'label'    => esc_html__( 'Checkbox / Radio Button Checked Style', 'divi-machine' ),
                    					'css'      => array(
                    						'main' => "%%order_class%% .dmach-radio-buttons .et_pb_contact_field_radio input:checked+label",
                    						'important' => 'plugin_only',
                    					),
                    					'font_size' => array(
                    						'default' => '14px',
                    					),
                    					'line_height' => array(
                    						'default' => '1em',
                    					),
                    				),
                  			),
              			'background' => array(
              				'settings' => array(
              					'color' => 'alpha',
              				),
              			),
                  			'button' => array(
                  			),
                  			'box_shadow' => array(
                  				'default' => array(),
                  				'product' => array(
                  					'label' => esc_html__( 'Default Layout - Box Shadow', 'divi-machine' ),
                  					'css' => array(
                  						'main' => "%%order_class%% .products .product",
                  					),
                  					'option_category' => 'layout',
                  					'tab_slug'        => 'advanced',
                  					'toggle_slug'     => 'product',
                  				),
                  			),
                  		);


                      $this->custom_css_fields = array(
                  		);


            $this->help_videos = array(
            );
          }

                  function get_fields() {
                    $et_accent_color = et_builder_accent_color();

                    ///////////////////////////////

                  $acf_fields = DEDMACH_INIT::get_acf_fields();

                    //////////////////////////////


                      $fields = array(
			'title' => array(
				'label'           => esc_html__( 'Orderby Name', 'et_builder' ),
				'type'            => 'text',
				'description'     => esc_html__( 'Change the name for the filter for admin purposes ONLY, this is just used so you can see what the filter is.', 'et_builder' ),
				'toggle_slug'     => 'main_content',
				'dynamic_content' => 'text',
				'option_category' => 'configuration',
			),

      'filter_post_type' => array(
      'toggle_slug'       => 'main_content',
        'label'             => esc_html__( 'What do you want to add to the orderby select option?', 'divi-machine' ),
        'type'              => 'select',
        'options'           => array(
        'relevance'          => 'Relevance',
          'acf'        => 'Advanced Custom Field (ACF Plugin)',
          'date'    => 'Date',
        ),
        'default'           => 'relevance',
        'affects'         => array(
          'acf_name',
        ),
        'option_category'   => 'configuration',
        'description'       => esc_html__( 'Choose what you want added to the orderby select dropdown', 'divi-machine' ),
      ),
                      'acf_name' => array(
                      'toggle_slug'       => 'main_content',
                        'label'             => esc_html__( 'ACF Name', 'divi-machine' ),
                        'type'              => 'select',
                        'options'           => $acf_fields,
                        'default'           => 'none',
                        'depends_show_if'   => 'acf',
                        'option_category'   => 'configuration',
                        'description'       => esc_html__( 'Add the name of the ACF you want to display here', 'divi-machine' ),
                      ),
                      'asc_desc' => array(
                      'toggle_slug'       => 'main_content',
                        'label'             => esc_html__( 'Ascending or Descending?', 'divi-machine' ),
                        'type'              => 'select',
                        'options'           => array(
                          'ASC'        => 'Ascending',
                          'DESC'          => 'Descending',
                        ),
                        'default'           => 'ASC',
                        'option_category'   => 'configuration',
                        'description'       => esc_html__( 'Choose if you want the orderby to be ascending or descending', 'divi-machine' ),
                      ),



                      );

                      return $fields;
                  }



                  function render( $attrs, $content = null, $render_slug ) {

                    if (is_admin()) {
                        return;
                    }

                    include(DE_DMACH_PATH . '/titan-framework/titan-framework-embedder.php');
                    $titan = TitanFramework::getInstance( 'divi-machine' );
                    $enable_debug = $titan->getOption( 'enable_debug' );

                    $filter_post_type                         = $this->props['filter_post_type'];
                    $acf_name                         = $this->props['acf_name'];
                    $asc_desc                  = $this->props['asc_desc'];
                    $title                 = $this->props['title'];


                  //////////////////////////////////////////////////////////////////////

                    ob_start();

                    if ($enable_debug == "1") {
                    $acf_get = get_field_object($acf_name);
                      ?>
                      <div class="reporting_args hidethis">
                        Filter Post Type: <?php echo esc_html__($filter_post_type); ?><br>
                        ACF Name: <?php echo esc_html__($acf_name); ?><br>
                        ASC OR DESC: <?php echo esc_html__($asc_desc); ?><br>
                        Title: <?php echo esc_html__($title); ?><br><br>
                        ACF Data: <br>
                        <?php print_r($acf_get); ?>
                      </div>
                      <?php
                    }

if ($filter_post_type == "acf"){
                    $acf_get = get_field_object($acf_name);
                  ?>
	                 <option value="<?php echo $acf_get['name']; ?>" data-ascdec="<?php echo $asc_desc; ?>" data-order-type="<?php echo $acf_get['type'];?>"><?php echo esc_html( $title ); ?></option>
                  <?php
} else if ($filter_post_type == "date"){
  ?>
   <option value="date" data-ascdec="<?php echo $asc_desc; ?>" data-order-type="num"><?php echo esc_html( $title ); ?></option>
   <?php
 } else if ($filter_post_type == "relevance"){
   ?>
    <option value="relevance" data-ascdec="<?php echo $asc_desc; ?>" data-order-type=""><?php echo esc_html( $title ); ?></option>
    <?php
 } else {

 }
                    $data = ob_get_clean();

                   //////////////////////////////////////////////////////////////////////

                  return $data;

              }

            }

            new de_mach_orderby_item_code;

?>
