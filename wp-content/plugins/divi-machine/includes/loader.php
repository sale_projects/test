<?php

if ( ! class_exists( 'ET_Builder_Element' ) ) {
	return;
}

$disable_ajax_filters = false;
$mydata = get_option( 'divi-bodyshop-woo_options' );
$mydata = unserialize($mydata);

if ( $mydata['disable_ajax_filters'] == '1' ){
	$disable_ajax_filters = true;
}

$module_files = glob( __DIR__ . '/modules/*/*.php' );

// Load custom Divi Builder modules
foreach ( (array) $module_files as $module_file ) {
	if (strpos($module_file, 'divi-ajax-filter.php') !== false) { 
		if ($disable_ajax_filters == false) {
			require_once $module_file;
		} else {
			defined('DE_DF_PRODUCT_URL') or define('DE_DF_PRODUCT_URL', 'https://diviengine.com/product/divi-machine/');
			defined('DE_DF_AUTHOR') or define('DE_DF_AUTHOR', 'Divi Engine');
			defined('DE_DF_URL') or define('DE_DF_URL', 'https://www.diviengine.com');
			require_once __DIR__ . '/modules/divi-ajax-filter/includes/modules/PostTitle/PostTitle.php';
			require_once __DIR__ . '/modules/divi-ajax-filter/includes/modules/Thumbnail/Thumbnail.php';
			require_once __DIR__ . '/modules/divi-ajax-filter/includes/modules/ArchiveLoop/ArchiveLoop.php';
		}
	} else {
		if ( $module_file && preg_match( "/\/modules\/\b([^\/]+)\/\\1\.php$/", $module_file ) ) {
			require_once $module_file;
		}
	}
}
