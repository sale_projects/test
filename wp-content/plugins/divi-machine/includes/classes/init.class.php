<?php
if( !defined( 'ABSPATH' ) ) exit; // exit if accessed directly

class DEDMACH_INIT{

	protected static $required_item = array();
	public static $product_layout_id = '0';
	public static $page_layout 	= false;
	public static $product_builder_used = 'divi_library';
	public $layout_type = false;
	public static $plugin_settings = false;
	public static $notices = '';

	public function __construct(){

		// enqueue scripts
		add_action( 'wp_enqueue_scripts', array( $this, 'load_scripts' ), 99 );
		add_action( 'admin_enqueue_scripts', array( $this, 'admin_load_scripts' ) );


		// clear modules cache
		add_action( 'et_builder_ready', function(){
			add_action( 'admin_head', array( $this, 'clear_modules_cache' ), 999 );
		} );


	}


	public function clear_modules_cache(){
	?>
		<script>
			var module_start = 'et_pb_templates_et_pb_de_mach_';

			var MOD_ARRAY = [
				module_start + 'archive_loop',
			];

			for(var module in localStorage){
				if(MOD_ARRAY.indexOf(module) != -1){
					localStorage.removeItem(module);
				}
			}
		</script>
	<?php
	}


	// load front end scripts
	public function load_scripts(){
		wp_enqueue_script( 'divi-machine-general-js', DE_DMACH_PATH_URL . '/scripts/frontend-general.min.js', array( 'jquery' ), DE_DMACH_VERSION, true );
		wp_enqueue_script( 'divi-machine-masonry-js', DE_DMACH_PATH_URL . '/scripts/masonry.min.js', array( 'jquery' ), DE_DMACH_VERSION, true );

		// Register ajax load more js library
		// global $wp_query;
		//wp_register_script( 'divi-machine-ajax-loadmore-js', DE_DMACH_PATH_URL . '/scripts/ajax-loadmore.min.js', array('jquery'), DE_DMACH_VERSION, true );

		/*wp_localize_script( 'divi-machine-ajax-loadmore-js', 'divi_machine_loadmore_params', array(
			'ajaxurl' => site_url() . '/wp-admin/admin-ajax.php', // WordPress AJAX
			'posts' => json_encode( $wp_query->query_vars ), // everything about your loop is here
			'current_page' => get_query_var( 'paged' ) ? get_query_var('paged') : 1,
			'max_page' => $wp_query->max_num_pages
		) );
		wp_enqueue_script( 'divi-machine-ajax-loadmore-js' );*/
		// JS RANGE SLIDER
		wp_enqueue_script( 'divi-machine-rangeSlider-js', DE_DMACH_PATH_URL . '/scripts/ion.rangeSlider.min.js', array( 'jquery' ), DE_DMACH_VERSION, true );
	  wp_enqueue_style( 'divi-machine-rangeSlider-css', DE_DMACH_PATH_URL . '/styles/ion.rangeSlider.min.css' , array(), DE_DMACH_VERSION, 'all' );
	}

	// load back end scripts
	public function admin_load_scripts(){
		// wp_enqueue_script( 'woo-pro-divi-admin-js', DE_DMACH_PATH_URL . '/includes/assets/admin/main.admin.js', array( 'jquery' ), null, true );
		wp_register_style( 'divi-machine-admin-css', DE_DMACH_PATH_URL . '/styles/admin-style.css', false, DE_DMACH_VERSION );
		wp_enqueue_style( 'divi-machine-admin-css' );

	}


	// render ET font icons content css property
	public static function et_icon_css_content( $font_icon ){
		$icon = preg_replace( '/(&amp;#x)|;/', '', et_pb_process_font_icon( $font_icon ) );

		return '\\' . $icon;
	}


	public static function get_acf_fields(  ){

		$acf_fields = array();

		            $fields_all = get_posts(array(
		              'posts_per_page'   => -1,
		              'post_type'        => 'acf-field',
		              'orderby'          => 'name',
		              'order'            => 'ASC',
			            'post_status'       => 'publish',
		            ));

								$acf_fields['none'] = 'Please select an ACF field';

		            foreach ( $fields_all as $field ) {

									$post_parent = $field->post_parent;
									$post_parent_name = get_the_title($post_parent);
									$grandparent = wp_get_post_parent_id($post_parent);
									$grandparent_name = get_the_title($grandparent);

		              $acf_fields[$field->post_name] = $post_parent_name . " > " . $field->post_title . " - " . $grandparent_name;

		            }


		$field_groups = acf_get_field_groups();
		foreach ( $field_groups as $group ) {
			// DO NOT USE here: $fields = acf_get_fields($group['key']);
			// because it causes repeater field bugs and returns "trashed" fields
			$fields = get_posts(array(
				'posts_per_page'   => -1,
				'post_type'        => 'acf-field',
				'orderby'          => 'name',
				'order'            => 'ASC',
				'suppress_filters' => true, // DO NOT allow WPML to modify the query
				'post_parent'      => $group['ID'],
				'post_status'       => 'publish',
				'update_post_meta_cache' => false
			));

			$acf_fields['none'] = 'Please select an ACF field';

			foreach ( $fields as $field ) {

				$acf_fields[$field->post_name] = $field->post_title . " - " . $group['title'];

			}

		}


		return $acf_fields;
	}


	public static function get_vb_post_type(  ){

		$get_dmach_args = array(
      'post_type' => 'dmach_post',
      'post_status' => 'publish',
      'posts_per_page' => '1',
	    'orderby' => 'ID',
	    'order' => 'ASC',
  );

  query_posts( $get_dmach_args );

  $first = true;

  if ( have_posts() ) {
    while ( have_posts() ) {
      the_post();
      // setup_postdata( $post );

    if ( $first )  {

			global $post;
			$post_slug = $post->post_name;
$first = false;
		} else {
}
	}
} else {
$post_slug = 'post';
}

$post_slug = str_replace('-', '_', $post_slug);
return $post_slug;
	}





}

new DEDMACH_INIT();
