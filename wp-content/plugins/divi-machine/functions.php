<?php
if ( ! defined( 'ABSPATH' ) ) exit;

include('titan-framework/titan-framework-embedder.php');

function divi_machine_options() {
$titan = TitanFramework::getInstance( 'divi-machine' );

$get_divi_engine_menu = get_option('divi-engine-menu', null);
if ($get_divi_engine_menu == "" || $get_divi_engine_menu == "machine-added") {
  update_option('divi-engine-menu', 'machine-added');
$titan = TitanFramework::getInstance( 'divi-machine' );
$icon = plugins_url( 'images/dash-icon.svg', __FILE__ );
$admin_panel2 = $titan->createAdminPanel( array( 'name' => 'Divi Engine', 'capability' => 'edit_pages' , 'icon' => $icon . '' , 'id' => 'divi-engine',) );
$welcometab = $admin_panel2->createTab( array('name' => 'Welcome',) );
$welcometab->createOption( array(
'name' => ''.esc_html__( "Welcome to Divi Engine!", 'divi-machine' ).'',
'type' => 'heading',
) );
$welcometab->createOption( array(
'type' => 'note',
'desc' => ''.esc_html__( "Firstly we would like to thank you for purchasing one of our plugins! We hope that you find it useful in your web design adventures.", 'divi-machine' ).''
) );

$welcometab->createOption( array(
'name' => ''.esc_html__( "Tech Support", 'divi-machine' ).'',
'type' => 'heading',
) );
$welcometab->createOption( array(
'type' => 'note',
'desc' => ''.esc_html__( 'We know from time to time, things may not go to plan due to the number of different setups on WordPress sites.', 'divi-machine' ).'<br>'.esc_html__( 'Dont worry, we are here to help. First take a look at our documentation (see below), if you cannot find a solution, use our support section on our site and we will help you resolve any issues.', 'divi-machine' ).'<br>
<a href="https://diviengine.com/support/" target="_blank"><img style="position: relative;left: 0;top: 12px;width: 200px;transform: translateY(0);" src="'.DE_DMACH_PATH_URL . '/images/admin-area/support-ticket.png"></a>
  ',
) );

// $welcometab->createOption( array(
// 'name' => ''.esc_html__( "Documentation", 'divi-machine' ).'',
// 'type' => 'heading',
// ) );
// $welcometab->createOption( array(
// 'type' => 'note',
// 'desc' => '<h4>'.esc_html__( "We have created documentation for all our plugins - you can read up on them using the links below.", 'divi-machine' ).'</h4>
// <a style="text-decoration: none;font-size: 17px;color: #1e136e;font-weight: 500;" href="https://help.diviengine.com/category/36-divi-nitro/" target="_blank"><img style="position:relative;left:0;top:0;transform: translateY(0);width: 50px;" src="'.DE_DMACH_PATH_URL . '/images/admin-area/divi-nitro-plugin-featured-150x150.png"><span style="position: relative;top: -13px;left: 10px;">'.esc_html__( "Divi Nitro", 'divi-machine' ).'</span></a><br>
// <a style="text-decoration: none;font-size: 17px;color: #1e136e;font-weight: 500;" href="https://help.diviengine.com/category/9-divi-bodycommerce" target="_blank"><img style="position:relative;left:0;top:0;transform: translateY(0);width: 50px;" src="'.DE_DMACH_PATH_URL . '/images/admin-area/divi-bodycommerce-plugin-featured-150x150.png"><span style="position: relative;top: -13px;left: 10px;">'.esc_html__( "Divi BodyCommerce", 'divi-machine' ).'</span></a><br>
// <a style="text-decoration: none;font-size: 17px;color: #1e136e;font-weight: 500;" href="https://help.diviengine.com/category/43-divi-protect/" target="_blank"><img style="position:relative;left:0;top:0;transform: translateY(0);width: 50px;" src="'.DE_DMACH_PATH_URL . '/images/admin-area/divi-protect-plugin-featured-150x150.png"><span style="position: relative;top: -13px;left: 10px;">'.esc_html__( "Divi Protect", 'divi-machine' ).'</span></a><br>
// <a style="text-decoration: none;font-size: 17px;color: #1e136e;font-weight: 500;" href="https://help.diviengine.com/category/45-divi-mega-menu/" target="_blank"><img style="position:relative;left:0;top:0;transform: translateY(0);width: 50px;" src="'.DE_DMACH_PATH_URL . '/images/admin-area/divi-mega-menu-plugin-featured-150x150.png"><span style="position: relative;top: -13px;left: 10px;">'.esc_html__( "Divi Mega Menu", 'divi-machine' ).'</span></a><br>
// <h4>'.esc_html__( "Divi Nitro video documentation", 'divi-machine' ).'</h4>
// <iframe width="560" height="315" src="https://www.youtube.com/embed/ds_iCldaiwE" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
// <h4>Divi BodyCommerce video documentation</h4>
// <iframe width="560" height="315" src="https://www.youtube.com/embed/U9gPHMvQ2Js" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
// '
// ) );
// $welcometab->createOption( array(
// 'name' => ''.esc_html__( "Feedback", 'divi-machine' ).'',
// 'type' => 'heading',
// ) );
// $welcometab->createOption( array(
// 'type' => 'note',
// 'desc' => ''.esc_html__( "We would love to hear from you, good or bad! We would really appreciate it if you could leave a review on our product page so that it helps others!", 'divi-machine' ).''
// ) );
//
// $welcometab->createOption( array(
// 'name' => ''.esc_html__( "Got an idea?", 'divi-machine' ).'',
// 'type' => 'heading',
// ) );
// $welcometab->createOption( array(
// 'type' => 'note',
// 'desc' => ''.esc_html__( "If you have an idea or would like us to implement a feature that you would benefit from, please dont hesitate to contact us about it", 'divi-machine' ).' <a href="https://diviengine.com/contact/" target="_blank">'.esc_html__( "here", 'divi-machine' ).'</a> '.esc_html__( "as we really want to make all our plugins better for everyone!", 'divi-machine' ).''
// ) );
}
else {
  # code...
}



$all_post_types = get_posts( 
    array( 
      'post_type' => 'dmach_post',
      'numberposts' => -1,
    )
  );

  $option_args = array();

  $option_args['post'] = 'Posts';
  $option_args['mphb_room_type'] = 'Accommodation Type';

  $all_cpts = get_post_types( array(
       'public'   => true,
       '_builtin' => false,
  ), 'object' );

  foreach ($all_cpts as $key => $obj) {
    $option_args[$key] = $obj->label;
  }

  foreach ( $all_post_types as $post_type ){
    //$dmach_post_type_key = $titan->getOption( 'dmach_post_type_key', $post_type->ID  );
    $dmach_post_type_key = get_post_meta($post_type->ID,  'divi-machine_dmach_post_type_key' , true );

    $option_args[$dmach_post_type_key] = $post_type->post_title;

  }

///////////////////////////////////////////////////////
// ET PB LAYOUT SETTINGS
///////////////////////////////////////////////////////
// $divi_layout_post = $titan->createMetaBox( array(
//        'name'      => ''.esc_html__( "Divi Machine", 'divi-machine' ).'',
//        'post_type' => array('et_pb_layout' ),
//        'priority'  => 'high',
//        'context' => 'side',
//    ) );
//
//    $divi_layout_post->createOption( array(
//    'name' => ''.esc_html__( "Layout type", 'divi-machine' ).'',
//    'id' => 'dmach_layout_type',
//    'type' => 'select',
//    'options' => array(
//    'normal' => 'Normal',
//    'custom-loop' => 'Custom Loop Layout',
//    ),
//    'default' => 'normal',
//    'desc' => ''.esc_html__( "If you are designing a custom loop layout, enable the 'Custom Loop Layout' setting to get a better preview of how it will look.", 'divi-machine' ).'',
//    ) );

///////////////////////////////////////////////////////
// POST TYPE SECTION
///////////////////////////////////////////////////////
$dmach_post = $titan->createMetaBox( array(
       'name'      => ''.esc_html__( "General Settings", 'divi-machine' ).'',
       'post_type' => array('dmach_post' ),
       'priority'  => 'low'
   ) );



   $dmach_post->createOption( array(
   'name' => ''.esc_html__( "Post Type Name", 'divi-machine' ).'',
   'id' => 'dmach_name_singular',
   'type' => 'text',
   'default' => '',
   'desc' => ''.esc_html__( "Set unique name for your post type. Eg. `Projects`", 'divi-machine' ).'',
   ) );

$dmach_post->createOption( array(
'name' => ''.esc_html__( "Post Type Slug", 'divi-machine' ).'',
'id' => 'dmach_post_type_key',
'type' => 'text',
'default' => '',
'desc' => ''.esc_html__( "Set slug for your post type. Slugs should only contain alphanumeric, latin characters. Underscores should be used in place of spaces.", 'divi-machine' ).'',
) );

$dmach_post->createOption( array(
'name' => ''.esc_html__( "Description", 'divi-machine' ).'',
'id' => 'dmach_description',
'type' => 'text',
'default' => '',
'desc' => ''.esc_html__( "A short descriptive summary of the post type.", 'divi-machine' ).'',
) );




   ///////////////////////////////////////////////////////
   // VISIBILITY SECTION
   ///////////////////////////////////////////////////////
   $dmach_visibilty = $titan->createMetaBox( array(
          'name'      => ''.esc_html__( "Post Visibility", 'divi-machine' ).'',
          'post_type' => array('dmach_post' ),
          'priority'  => 'low'
      ) );

   $dmach_visibilty->createOption( array(
   'name' => ''.esc_html__( "Public", 'divi-machine' ).'',
   'id' => 'dmach_public',
   'type' => 'enable',
   'default' => true,
   'desc' => ''.esc_html__( "Show post type in the admin UI.", 'divi-machine' ).'',
   ) );

// $dmach_visibilty->createOption( array(
// 'name' => ''.esc_html__( "Show UI", 'divi-machine' ).'',
// 'id' => 'dmach_show_ui',
// 'type' => 'select',
// 'type' => 'enable',
// 'default' => false,
// 'desc' => ''.esc_html__( "Show post type UI in the admin.", 'divi-machine' ).'',
// ) );

$dmach_visibilty->createOption( array(
'name' => ''.esc_html__( "Show in Admin Sidebar", 'divi-machine' ).'',
'id' => 'dmach_show_in_admin_sidebar',
'type' => 'enable',
'default' => true,
'desc' => ''.esc_html__( "Show post type in admin sidebar.", 'divi-machine' ).'',
) );

$dmach_visibilty->createOption( array(
'name' => ''.esc_html__( "Show in Admin Sidebar Position", 'divi-machine' ).'',
'id' => 'dmach_show_admin_sidebar_postion',
'type' => 'select',
'options' => array(
'5' => '5 - below Posts',
'10' => '10 - below Posts',
'15' => '15 - below Links',
'20' => '20 - below Pages',
'25' => '25 - below Comments',
'60' => '60 - below first separator',
'65' => '65 - below Plugins',
'70' => '70 - below Users',
'75' => '75 - below Tools',
'80' => '80 - below Settings',
'100' => '100 - below second separator',
),
'default' => '5',
'desc' => ''.esc_html__( "Show post type in admin sidebar position.", 'divi-machine' ).'',
) );

$dmach_visibilty->createOption( array(
'name' => ''.esc_html__( "Admin Sidebar Icon", 'divi-machine' ).'',
'id' => 'dmach_show_admin_sidebar_icon',
'type' => 'text',
'default' => 'dashicons-menu',
'desc' => ''.esc_html__( "Post type icon. Use ", 'divi-machine' ).'<a href="https://developer.wordpress.org/resource/dashicons/" target="_blank"> '.esc_html__( "dashicon name", 'divi-machine' ).'</a>.',
) );

$dmach_visibilty->createOption( array(
'name' => ''.esc_html__( "Show in Admin Bar", 'divi-machine' ).'',
'id' => 'dmach_show_in_admin_bar',
'type' => 'enable',
'default' => true,
'desc' => ''.esc_html__( "Show post type in admin bar.", 'divi-machine' ).'',
) );

$dmach_visibilty->createOption( array(
'name' => ''.esc_html__( "Show in Navigation Menus", 'divi-machine' ).'',
'id' => 'dmach_show_in_nav_menus',
'type' => 'enable',
'default' => true,
'desc' => ''.esc_html__( "Show post type in Navigation Menus.", 'divi-machine' ).'',
) );


///////////////////////////////////////////////////////
// OPTIONS SECTION
///////////////////////////////////////////////////////
$dmach_options = $titan->createMetaBox( array(
       'name'      => ''.esc_html__( "Advanced Options", 'divi-machine' ).'',
       'post_type' => array('dmach_post' ),
       'priority'  => 'low'
   ) );

   $dmach_options->createOption( array(
   'name' => ''.esc_html__( "Taxonomies", 'divi-machine' ).'',
   'id' => 'dmach_taxonomies',
   'type' => 'multicheck',
   'options' => array(
       'category' => 'Categories',
       'tag' => 'Tags',
   ),
   'default' => array( 'category', 'tag' ),
   'desc' => ''.esc_html__( "Comma-separated list of Taxonomies. These are the things you want for example you may want categories, tags, topics etc..", 'divi-machine' ).'',
   ) );

   $dmach_options->createOption( array(
       'name' => 'Supports',
       'id' => 'dmach_supports',
       'type' => 'multicheck',
       'desc' => 'Check what you want the custom post to support',
       'options' => array(
           'title' => 'Title',
           'editor' => 'Content (editor)',
           'excerpt' => 'Excerpt',
           'author' => 'Author',
           'thumbnail' => 'Featured Image',
           'comments' => 'Comments',
           'trackbacks' => 'Trackbacks',
           'revisions' => 'Revisions',
           'custom-fields' => 'Custom Fields',
           'page-attributes' => 'Page Attributes',
           'post-formats' => 'Post Formats',
       ),
       'default' => array( 'title', 'editor', 'thumbnail', 'custom-fields', 'page-attributes', 'excerpt' ),
   ) );

   $dmach_options->createOption( array(
    'name' => ''.esc_html__( "Enable Gutenberg", 'divi-machine' ).'',
    'id' => 'dmach_gutenberg',
    'type' => 'enable',
    'default' => false,
    'desc' => ''.esc_html__( "Enable the Gutenberg Builder?.", 'divi-machine' ).'',
    ) );

   $dmach_options->createOption( array(
    'name' => ''.esc_html__( "Prettify URL", 'divi-machine' ).'',
    'id' => 'dmach_prettify_post',
    'type' => 'enable',
    'default' => false,
    'desc' => ''.esc_html__( "If you want to prettify the URL, enable this. It will change the URL to be post-type/category/single instead of /post-type_category/single.", 'divi-machine' ).'',
    ) );

    // $dmach_options->createOption( array(
    //     'name' => ''.esc_html__( "Prettify Category Name", 'divi-machine' ).'',
    //     'id' => 'dmach_prettify_name_cat',
    //     'type' => 'text',
    //     'desc' => ''.esc_html__( "If you want to change the category name from 'posttype_category' to something better, add it here.", 'divi-machine' ).'',
    //     ) );

    //     $dmach_options->createOption( array(
    //         'name' => ''.esc_html__( "Prettify Tag Name", 'divi-machine' ).'',
    //         'id' => 'dmach_prettify_name_tag',
    //         'type' => 'text',
    //         'desc' => ''.esc_html__( "If you want to change the tag name from 'posttype_tag' to something better, add it here.", 'divi-machine' ).'',
    //         ) );

   $dmach_options->createOption( array(
   'name' => ''.esc_html__( "Exclude From Search", 'divi-machine' ).'',
   'id' => 'dmach_exclude_from_search',
   'type' => 'enable',
   'default' => false,
   'desc' => ''.esc_html__( "Posts of this type should be excluded from search results.", 'divi-machine' ).'',
   ) );

   $dmach_options->createOption( array(
   'name' => ''.esc_html__( "Enable Archives", 'divi-machine' ).'',
   'id' => 'dmach_enable_archives',
   'type' => 'select',
   'options' => array(
   'true' => 'Yes (use default slug)',
   'true_custom' => 'Yes (set a custom archive slug - below)',
   'false' => 'No (prevent archive pages)',
   ),
   'default' => 'true',
   'desc' => ''.esc_html__( "Enables post type archives. Post type key is used as default archive slug.", 'divi-machine' ).'',
   ) );

   $dmach_options->createOption( array(
   'name' => ''.esc_html__( "Custom Archive Slug", 'divi-machine' ).'',
   'id' => 'dmach_custom_archive_slug',
   'type' => 'text',
   'desc' => ''.esc_html__( "Set custom archive slug.", 'divi-machine' ).'',
   ) );

   $dmach_options->createOption( array(
   'name' => ''.esc_html__( "Custom Post Slug", 'divi-machine' ).'',
   'id' => 'dmach_custom_post_slug',
   'type' => 'text',
   'desc' => ''.esc_html__( "Set custom post slug. Leave it blank if you don't want to use custom slug.", 'divi-machine' ).'',
   ) );

   $dmach_options->createOption( array(
   'name' => ''.esc_html__( "Hierarchical", 'divi-machine' ).'',
   'id' => 'dmach_hierarchical',
   'type' => 'enable',
   'default' => true,
   'desc' => '​If you want your posts to have descendants (like pages) where you have parent and children posts - enable this.',
   ) );

   $dmach_options->createOption( array(
    'name' => ''.esc_html__( "Rewrite", 'divi-machine' ).'',
    'id' => 'dmach_rewrite',
    'type' => 'select',
    'options' => array(
    'true' => 'Yes',
    'false' => 'No',
    ),
    'default' => 'true',
    'desc' => ''.esc_html__( "Whether or not WordPress should use rewrites for this post type.", 'divi-machine' ).'',
    ) );

    // $dmach_options->createOption( array(
    // 'name' => ''.esc_html__( "Custom Archive Slug", 'divi-machine' ).'',
    // 'id' => 'dmach_rewrite_custom_slug',
    // 'type' => 'text',
    // 'desc' => ''.esc_html__( "Set custom archive slug.", 'divi-machine' ).'',
    // ) );

    $dmach_options->createOption( array(
     'name' => ''.esc_html__( "With Front", 'divi-machine' ).'',
     'id' => 'dmach_rewrite_withfront',
     'type' => 'select',
     'options' => array(
     'true' => 'True',
     'false' => 'False',
     ),
     'default' => 'false',
     'desc' => ''.esc_html__( "Should the permalink structure be prepended with the front base. (example: if your permalink structure is /blog/, then your links will be: false->/cars/, true->/blog/cars/).", 'divi-machine' ).'',
     ) );

///////////////////////////////////////////////////////
// Query SECTION
///////////////////////////////////////////////////////
// $dmach_query = $titan->createMetaBox( array(
//        'name'      => ''.esc_html__( "Query", 'divi-machine' ).'',
//        'post_type' => array('dmach_post' ),
//        'priority'  => 'low'
//    ) );

   //
   // $dmach_query->createOption( array(
   // 'name' => ''.esc_html__( "Query", 'divi-machine' ).'',
   // 'id' => 'dmach_query',
   // 'type' => 'select',
   // 'options' => array(
   // 'default' => 'Default (post type key)',
   // 'custom' => 'Custom query variable',
   // ),
   // 'default' => 'default',
   // 'desc' => ''.esc_html__( "Direct query variable used in WP_Query.", 'divi-machine' ).'',
   // ) );
   //
   // $dmach_query->createOption( array(
   // 'name' => ''.esc_html__( "Custom Query", 'divi-machine' ).'',
   // 'id' => 'dmach_custom_query',
   // 'type' => 'text',
   // 'desc' => ''.esc_html__( "Custom query variable.", 'divi-machine' ).'',
   // ) );

   //
   // ///////////////////////////////////////////////////////
   // // Permalink SECTION
   // ///////////////////////////////////////////////////////
   // $dmach_permalink = $titan->createMetaBox( array(
   //        'name'      => ''.esc_html__( "Query & Capabilitites", 'divi-machine' ).'',
   //        'post_type' => array('dmach_post' ),
   //        'priority'  => 'low'
   //    ) );
   //
   //    $dmach_permalink->createOption( array(
   //    'name' => ''.esc_html__( "Publicly Queryable", 'divi-machine' ).'',
   //    'id' => 'dmach_public_query',
   //    'type' => 'enable',
   //    'default' => true,
   //    'desc' => ''.esc_html__( "Enable front end queries as part of parse_request().", 'divi-machine' ).'',
   //    ) );
   //
   //    // $dmach_permalink->createOption( array(
   //    // 'name' => ''.esc_html__( "Permalink Rewrite", 'divi-machine' ).'',
   //    // 'id' => 'dmach_permalinks_rewrite',
   //    // 'type' => 'select',
   //    // 'options' => array(
   //    // 'none' => 'No permalink (prevent URL rewriting)',
   //    // 'default' => 'Default permalinks (post type key)',
   //    // ),
   //    // 'default' => 'default',
   //    // 'desc' => ''.esc_html__( "Use Default Permalinks (using post type key), prevent automatic URL rewriting (no pretty permalinks), or set custom permalinks.", 'divi-machine' ).'',
   //    // ) );
   //
   //    $dmach_permalink->createOption( array(
   //    'name' => ''.esc_html__( "Capability Type", 'divi-machine' ).'',
   //    'id' => 'dmach_capability_type',
   //    'type' => 'select',
   //    'options' => array(
   //    'post' => 'Same as Posts',
   //    'page' => 'Same as Pages',
   //    ),
   //    'default' => 'page',
   //    'desc' => ''.esc_html__( "Used as a base to construct capabilities.", 'divi-machine' ).'',
   //    ) );



    // ///////////////////////////////////////////////////////
   // // TAXONOMY SECTION
   // ///////////////////////////////////////////////////////




   $dmach_tax = $titan->createMetaBox( array(
    'name'      => ''.esc_html__( "General Settings", 'divi-machine' ).'',
    'post_type' => array('dmach_tax' ),
    'priority'  => 'low'
    ) );

   $dmach_tax->createOption( array(
    'name' => ''.esc_html__( "Taxonomy Slug", 'divi-machine' ).'',
    'id' => 'dmach_tax_slug',
    'type' => 'text',
    'default' => '',
    'desc' => ''.esc_html__( "Set slug for your taxonomy. Slugs should only contain alphanumeric, latin characters. Underscores should be used in place of spaces.", 'divi-machine' ).'',
    ) );

    $dmach_tax->createOption( array(
        'name' => ''.esc_html__( "Name (Plural)", 'divi-machine' ).'',
        'id' => 'dmach_tax_plural',
        'type' => 'text',
        'default' => '',
        'desc' => ''.esc_html__( "Used for the taxonomy admin menu item.", 'divi-machine' ).'',
    ) );

    $dmach_tax->createOption( array(
        'name' => ''.esc_html__( "Name (Single)", 'divi-machine' ).'',
        'id' => 'dmach_tax_single',
        'type' => 'text',
        'default' => '',
        'desc' => ''.esc_html__( "Used when a singular label is needed.", 'divi-machine' ).'',
    ) );
    
    // TODO: select post type
    $dmach_tax->createOption( array(
        'name' => ''.esc_html__( 'Attach to Post Type', 'divi-machine' ).'',
        'id' => 'dmach_tax_post_type',
        'type' => 'select',
        'options' => $option_args,
        'desc' => ''.esc_html__( 'Add the post type for the taxonomy to attach to', 'divi-machine' ).'',
    ) );

    $dmach_tax_labels = $titan->createMetaBox( array(
      'name'      => ''.esc_html__( "Labels", 'divi-machine' ).'',
      'post_type' => array('dmach_tax' ),
      'priority'  => 'low'
    ) );

    $dmach_tax_labels->createOption( array(
      'name' => ''.esc_html__( "Menu Name", 'divi-machine' ).'',
      'id' => 'dmach_tax_menu_name',
      'type' => 'text',
      'default' => '',
    ) );

    $dmach_tax_labels->createOption( array(
      'name' => ''.esc_html__( "All Items", 'divi-machine' ).'',
      'id' => 'dmach_tax_all_items',
      'type' => 'text',
      'default' => '',
    ) );


    $dmach_tax_labels->createOption( array(
      'name' => ''.esc_html__( "Edit Item", 'divi-machine' ).'',
      'id' => 'dmach_tax_edit_item',
      'type' => 'text',
      'default' => '',
    ) );



$dmach_tax_labels->createOption( array(
'name' => ''.esc_html__( "View Item", 'divi-machine' ).'',
'id' => 'dmach_tax_view_item',
'type' => 'text',
'default' => '',
) );

$dmach_tax_labels->createOption( array(
'name' => ''.esc_html__( "Update Item", 'divi-machine' ).'',
'id' => 'dmach_tax_update_item',
'type' => 'text',
'default' => '',
) );

$dmach_tax_labels->createOption( array(
'name' => ''.esc_html__( "Add New", 'divi-machine' ).'',
'id' => 'dmach_tax_add_new',
'type' => 'text',
'default' => 'Add New',
) );
                        
$dmach_tax_labels->createOption( array(
'name' => ''.esc_html__( "New Item", 'divi-machine' ).'',
'id' => 'dmach_tax_new_item',
'type' => 'text',
'default' => '',
) );

$dmach_tax_labels->createOption( array(
'name' => ''.esc_html__( "Parent Item", 'divi-machine' ).'',
'id' => 'dmach_tax_parent',
'type' => 'text',
'default' => '',
) );

$dmach_tax_labels->createOption( array(
'name' => ''.esc_html__( "Parent Item Colon", 'divi-machine' ).'',
'id' => 'dmach_tax_parent_colon',
'type' => 'text',
'default' => '',
) );

$dmach_tax_labels->createOption( array(
'name' => ''.esc_html__( "Search Items", 'divi-machine' ).'',
'id' => 'dmach_tax_search',
'type' => 'text',
'default' => '',
) );

$dmach_tax_labels->createOption( array(
'name' => ''.esc_html__( "Popular Items", 'divi-machine' ).'',
'id' => 'dmach_tax_popular',
'type' => 'text',
'default' => '',
) );

$dmach_tax_labels->createOption( array(
'name' => ''.esc_html__( "Separate Items with Commas", 'divi-machine' ).'',
'id' => 'dmach_tax_seperate_commas',
'type' => 'text',
'default' => '',
) );

$dmach_tax_labels->createOption( array(
'name' => ''.esc_html__( "Add or Remove Items", 'divi-machine' ).'',
'id' => 'dmach_tax_add_remove',
'type' => 'text',
'default' => '',
) );

$dmach_tax_labels->createOption( array(
'name' => ''.esc_html__( "Choose From Most Used", 'divi-machine' ).'',
'id' => 'dmach_tax_most_used',
'type' => 'text',
'default' => '',
) );

$dmach_tax_labels->createOption( array(
'name' => ''.esc_html__( "Not found", 'divi-machine' ).'',
'id' => 'dmach_tax_notfound',
'type' => 'text',
'default' => '',
) );

$dmach_tax_labels->createOption( array(
'name' => ''.esc_html__( "No terms", 'divi-machine' ).'',
'id' => 'dmach_tax_noterms',
'type' => 'text',
'default' => '',
) );

$dmach_tax_labels->createOption( array(
'name' => ''.esc_html__( "Items List Navigation", 'divi-machine' ).'',
'id' => 'dmach_tax_item_list_nav',
'type' => 'text',
'default' => '',
) );

$dmach_tax_labels->createOption( array(
'name' => ''.esc_html__( "Items List", 'divi-machine' ).'',
'id' => 'dmach_tax_item_list',
'type' => 'text',
'default' => '',
) );

///////////////////////////////////////////////////////
// Tax advanced SECTION
///////////////////////////////////////////////////////
$dmach_advanced = $titan->createMetaBox( array(
    'name'      => ''.esc_html__( "Advanced Options", 'divi-machine' ).'',
    'post_type' => array('dmach_tax'),
    'priority'  => 'low'
) );


$dmach_advanced->createOption( array(
    'name' => ''.esc_html__( "Public", 'divi-machine' ).'',
    'id' => 'dmach_tax_public',
    'type' => 'select',
    'options' => array (
        'true' => 'True',
        'false' => 'False'
    ),
    'default' => 'true',
    'desc' => ''.esc_html__( "(default: true) Whether a taxonomy is intended for use publicly either via the admin interface or by front-end users.", 'divi-machine' ).'',
    ) );

    $dmach_advanced->createOption( array(
        'name' => ''.esc_html__( "Public Queryable", 'divi-machine' ).'',
        'id' => 'dmach_tax_public_queryable',
        'type' => 'select',
        'options' => array (
            'true' => 'True',
            'false' => 'False'
        ),
        'default' => 'true',
        'desc' => ''.esc_html__( '(default: value of "public" setting) Whether or not the taxonomy should be publicly queryable.', 'divi-machine' ).'',
        ) );


$dmach_advanced->createOption( array(
    'name' => ''.esc_html__( "Hierarchical", 'divi-machine' ).'',
    'id' => 'dmach_tax_hierarchical',
    'type' => 'select',
    'options' => array (
        'true' => 'True',
        'false' => 'False'
    ),
    'default' => 'true',
    'desc' => ''.esc_html__( "(default: false) Whether the taxonomy can have parent-child relationships.", 'divi-machine' ).'',
    ) );  


$dmach_advanced->createOption( array(
    'name' => ''.esc_html__( "Show UI", 'divi-machine' ).'',
    'id' => 'dmach_tax_show_ui',
    'type' => 'select',
    'options' => array (
        'true' => 'True',
        'false' => 'False'
    ),
    'default' => 'true',
    'desc' => ''.esc_html__( "(default: true) Whether to generate a default UI for managing this custom taxonomy.", 'divi-machine' ).'',
    ) ); 


$dmach_advanced->createOption( array(
    'name' => ''.esc_html__( "Show In Menu", 'divi-machine' ).'',
    'id' => 'dmach_tax_show_in_menu',
    'type' => 'select',
    'options' => array (
        'true' => 'True',
        'false' => 'False'
    ),
    'default' => 'true',
    'desc' => ''.esc_html__( "(default: value of show_ui) Whether to show the taxonomy in the admin menu.", 'divi-machine' ).'',
    ) ); 


$dmach_advanced->createOption( array(
    'name' => ''.esc_html__( "Show In Nav Menus", 'divi-machine' ).'',
    'id' => 'dmach_tax_show_nav_menu',
    'type' => 'select',
    'options' => array (
        'true' => 'True',
        'false' => 'False'
    ),
    'default' => 'true',
    'desc' => ''.esc_html__( "(default: value of public) Whether to make the taxonomy available for selection in navigation menus.", 'divi-machine' ).'',
    ) ); 


$dmach_advanced->createOption( array(
    'name' => ''.esc_html__( "Query Var", 'divi-machine' ).'',
    'id' => 'dmach_tax_query_var',
    'type' => 'select',
    'options' => array (
        'true' => 'True',
        'false' => 'False'
    ),
    'default' => 'true',
    'desc' => ''.esc_html__( "(default: true) Sets the query_var key for this taxonomy.", 'divi-machine' ).'',
    ) ); 

    $dmach_advanced->createOption( array(
        'name' => ''.esc_html__( "Custom Query Var String", 'divi-machine' ).'',
        'id' => 'dmach_tax_query_var_custom',
        'type' => 'text',
        'default' => '',
        'desc' => ''.esc_html__( "Sets a custom query_var slug for this taxonomy.", 'divi-machine' ).'',
        ) );
    

$dmach_advanced->createOption( array(
    'name' => ''.esc_html__( "Rewrite", 'divi-machine' ).'',
    'id' => 'dmach_tax_rewrite',
    'type' => 'select',
    'options' => array (
        'true' => 'True',
        'false' => 'False'
    ),
    'default' => 'true',
    'desc' => ''.esc_html__( "(default: true) Whether or not WordPress should use rewrites for this taxonomy.", 'divi-machine' ).'',
    ) ); 

    $dmach_advanced->createOption( array(
        'name' => ''.esc_html__( "Custom Rewrite String", 'divi-machine' ).'',
        'id' => 'dmach_tax_query_rewrite_custom',
        'type' => 'text',
        'default' => '',
        'desc' => ''.esc_html__( "Custom taxonomy rewrite slug.", 'divi-machine' ).'',
        ) );


        $dmach_advanced->createOption( array(
    'name' => ''.esc_html__( "Rewrite With Front", 'divi-machine' ).'',
    'id' => 'dmach_tax_rewrite_front',
    'type' => 'select',
    'options' => array (
        'true' => 'True',
        'false' => 'False'
    ),
    'default' => 'true',
    'desc' => ''.esc_html__( "(default: true) Should the permastruct be prepended with the front base.", 'divi-machine' ).'',
    ) ); 


    $dmach_advanced->createOption( array(
'name' => ''.esc_html__( "Rewrite Hierarchical", 'divi-machine' ).'',
'id' => 'dmach_tax_rewrite_hierarchical',
'type' => 'select',
'options' => array (
    'true' => 'True',
    'false' => 'False'
),
'default' => 'false',
'desc' => ''.esc_html__( "(default: false) Should the permastruct allow hierarchical urls.", 'divi-machine' ).'',
) ); 

///////////////////////////////////////////////////////
// LABELS SECTION
///////////////////////////////////////////////////////
$dmach_labels = $titan->createMetaBox( array(
    'name'      => ''.esc_html__( "Labels", 'divi-machine' ).'',
    'post_type' => array('dmach_post' ),
    'priority'  => 'low'
) );


$dmach_labels->createOption( array(
'name' => ''.esc_html__( "Name (Plural)", 'divi-machine' ).'',
'id' => 'dmach_name_plural',
'type' => 'text',
'default' => '',
'desc' => ''.esc_html__( "Post type plural name. e.g. Products, Events or Movies.", 'divi-machine' ).'',
) );
$dmach_labels->createOption( array(
'name' => ''.esc_html__( "Menu Name", 'divi-machine' ).'',
'id' => 'dmach_menu_name',
'type' => 'text',
'default' => '',
) );

$dmach_labels->createOption( array(
'name' => ''.esc_html__( "Admin Bar Name", 'divi-machine' ).'',
'id' => 'dmach_admin_bar_name',
'type' => 'text',
'default' => '',
) );

$dmach_labels->createOption( array(
'name' => ''.esc_html__( "All Items", 'divi-machine' ).'',
'id' => 'dmach_all_items',
'type' => 'text',
'default' => '',
) );

$dmach_labels->createOption( array(
'name' => ''.esc_html__( "Add New Item", 'divi-machine' ).'',
'id' => 'dmach_add_new_item',
'type' => 'text',
'default' => '',
) );

$dmach_labels->createOption( array(
'name' => ''.esc_html__( "Add New", 'divi-machine' ).'',
'id' => 'dmach_add_new',
'type' => 'text',
'default' => 'Add New',
) );

$dmach_labels->createOption( array(
'name' => ''.esc_html__( "New Item", 'divi-machine' ).'',
'id' => 'dmach_new_item',
'type' => 'text',
'default' => '',
) );

$dmach_labels->createOption( array(
'name' => ''.esc_html__( "Edit Item", 'divi-machine' ).'',
'id' => 'dmach_edit_item',
'type' => 'text',
'default' => '',
) );

$dmach_labels->createOption( array(
'name' => ''.esc_html__( "Update Item", 'divi-machine' ).'',
'id' => 'dmach_update_item',
'type' => 'text',
'default' => '',
) );

$dmach_labels->createOption( array(
'name' => ''.esc_html__( "View Item", 'divi-machine' ).'',
'id' => 'dmach_view_item',
'type' => 'text',
'default' => '',
) );

$dmach_labels->createOption( array(
'name' => ''.esc_html__( "Search Item", 'divi-machine' ).'',
'id' => 'dmach_search_item',
'type' => 'text',
'default' => '',
) );



            

   $machine_settings_admin = $titan->createAdminPanel( array( 'name' => 'Machine Settings', 'id' => 'divi-machine-settings', 'parent' => 'divi-engine', 'position' => '1') );
   $settingstab = $machine_settings_admin->createTab( array('name' => ''.esc_html__( "General Settings", 'divi-machine' ).'',) );
   $modulestab = $machine_settings_admin->createTab( array('name' => ''.esc_html__( "Module Overrides", 'divi-machine' ).'',) );
   $licensestab = $machine_settings_admin->createTab( array('name' => ''.esc_html__( "License", 'divi-machine' ).'',) );
   $helptab = $machine_settings_admin->createTab( array('name' => ''.esc_html__( "Get Help", 'divi-machine' ).'',) );

   $helptab->createOption(array(
       'name' => 'Get Help',
       'type' => 'heading',
   ));

   $helptab->createOption( array(
   'type' => 'note',
   'desc' => ''.esc_html__( "Divi Machine is a very powerful plugin, but with great power comes.... complex settings :D At first it may seem overwhelming but we assure you that once you understand the concept, it will speed up your development time and save you so much money in the long run.", 'divi-machine' ).''
   ) );

   $helptab->createOption( array(
   'type' => 'note',
   'desc' => '<h4>We have video documentation where you can <a href="https://www.youtube.com/watch?v=532WFUX77qE&list=PL9My989q_-K_j-75tEPgclT9dIPyOZhie" target="_blank">watch us build a car listing website from scratch to finish here.</a><br>

   '
   ) );

   $helptab->createOption( array(
   'type' => 'note',
   'desc' => 'We also have <a href="https://help.diviengine.com/category/69-divi-machine" target="_blank">written documentation</a> which will be more up to date when we change and add more features so be sure to check this out. We are also available on support by emailing support@diviengine.com.<br>
   We do hope you love the plugin and that it helps you on your web design adventures.'
   ) );


   

   $acf_fields = DEDMACH_INIT::get_acf_fields();

   $modulestab->createOption(array(
    'name' => 'Map Module',
    'type' => 'heading',
));

   $modulestab->createOption( array(
    'name' => ''.esc_html__( 'Map Module Override: ACF Item', 'divi-machine' ).'',
    'id' => 'dmach_map_acf',
    'type' => 'select',
    'options'  => $acf_fields,
    'default' => 'none',
    'desc' => ''.esc_html__( 'If you want to have a pin on a map for the single page of your custom post, add the Google Map ACF item here. Now when you add a map module to your theme builder template with a map pin (dont worry about adding the exact pin location), we will change the pin location to what you specify in the ACF settings for each post.', 'divi-machine' ).'',
    ) );

    $modulestab->createOption( array(
     'name' => ''.esc_html__( 'Map Post Type', 'divi-machine' ).'',
     'id' => 'dmach_post_type',
     'type' => 'select',
     'options' => $option_args,
     'default' => 'none',
     'desc' => ''.esc_html__( 'Add the post type to make it work with the ACF override of Divi Modules', 'divi-machine' ).'',
     ) );

     $modulestab->createOption(array(
        'name' => 'Countdown Module',
        'type' => 'heading',
    ));
    
       $modulestab->createOption( array(
        'name' => ''.esc_html__( 'Countdown Module Override: ACF Item', 'divi-machine' ).'',
        'id' => 'dmach_countdown_acf',
        'type' => 'select',
        'options'  => $acf_fields,
        'default' => 'none',
        'desc' => ''.esc_html__( 'If Choose the ACF datepicker field that you are using to define the countdown for each post.', 'divi-machine' ).'',
        ) );
    
        $modulestab->createOption( array(
         'name' => ''.esc_html__( 'Countdown Post Type', 'divi-machine' ).'',
         'id' => 'dmach_countdown_post_type',
         'type' => 'select',
         'options' => $option_args,
         'default' => 'none',
         'desc' => ''.esc_html__( 'Add the post type to make it work with the ACF override of Divi Modules', 'divi-machine' ).'',
         ) );

     $modulestab->createOption( array(
        'type' => 'save',
        ) );

   /// GENERAL SETTINGS


   $settingstab->createOption( array(
   'type' => 'save',
   ) );

   $settingstab->createOption(array(
       'name' => 'General Settings',
       'type' => 'heading',
   ));

   $settingstab->createOption( array(
   'name' => ''.esc_html__( 'Enable Debug Mode?', 'divi-machine' ).'',
   'id' => 'enable_debug',
   'type' => 'enable',
   'default' => false,
   'enabled' => ''.esc_html__( 'YES', 'divi-machine' ).'',
   'disabled' => ''.esc_html__( 'NO', 'divi-machine' ).'',
   'desc' => ''.esc_html__( 'If support staff has asked you to enable debug, enable this.', 'divi-machine' ).'',
   ) );

   $settingstab->createOption( array(
   'name' => ''.esc_html__( "Add some custom CSS/JS (great when exporting/importing settings)", 'divi-machine' ).'',
   'type' => 'heading',
   ) );

   $settingstab->createOption( array(
       'name' => ''.esc_html__( "Custom CSS", 'divi-machine' ).'',
       'id' => 'machine_custom_css',
       'type' => 'code',
       'desc' => ''.esc_html__( "Put your custom CSS rules here. Do not put <style> tags as we will do this for you. We will automatically minify your CSS so no need to minify it here.", 'divi-machine' ).'',
       'lang' => 'css',
   ) );


   $settingstab->createOption( array(
       'name' => ''.esc_html__( "Custom JavaScript", 'divi-machine' ).'',
       'id' => 'machine_custom_js',
       'type' => 'code',
       'desc' => ''.esc_html__( "Put your additional javascript rules here. Dont forget to add your <script> tags as this allows you to add multiple for yourself.", 'divi-machine' ).'',
       'lang' => 'javascript',
   ) );

   $settingstab->createOption( array(
   'name' => ''.esc_html__( 'Import/Export Settings', 'divi-machine' ).'',
   'type' => 'heading',
   ) );

   $settingstab->createOption( array(
   'desc' => ''.esc_html__( 'Here you can export and import the settings of Divi Machine.', 'divi-machine' ).'',
   'type' => 'note',
   ) );


   $settingstab->createOption(array(
       'name' => 'Upload your setting file',
       'id' => 'setting_file_upload',
       'type' => 'file',
       'label' => 'Choose File',
       'placeholder' => 'Choose File'
   ));
   $settingstab->createOption(array(
       'id' => "import_setting_btn",
       'name' => 'Import Settings',
       'type' => 'ajax-button',
       'action' => 'divi_machine_import_action',
       'label' => __('Save Settings', 'default'),
       'data_filter_callback' => 'import_settings_data_filter_callback_machine',
       'success_callback' => 'import_settings_machine',
       'class' => array('button-primary', 'button-secondary')
   ));
   $settingstab->createOption(array(
       'name' => 'Export Settings',
       'type' => 'ajax-button',
       'action' => 'divi_machine_export_action',
       'label' => __('Download', 'default'),
       'success_callback' => 'export_settings_machine',
       'class' => array('button-primary', 'button-secondary')
   ));

   $settingstab->createOption( array(
   'type' => 'save',
   ) );

   


   /// LICENSE


   $licensestab->createOption(array(
       'name' => 'Divi Machine Software License',
       'type' => 'heading',
   ));


   if (isset($_GET['page']) && ($_GET['page'] == 'divi-machine-settings')) { // phpcs:ignore
   $de_dmach_option = new DE_DMACH_options_interface();

   $licensestab->createOption(array(
       'type' => 'custom',
       'custom' => $de_dmach_option->admin_menu(),
   ));

       add_action('init', array($de_dmach_option, 'options_update'), 1);
   }



}
add_action( 'tf_create_options', 'divi_machine_options' );

//Setting Code Start
add_action('wp_ajax_divi_machine_export_action', 'divi_machine_export_action');

function divi_machine_export_action() {
    // Do something
    $mydata = get_option('divi-machine_options');
    $mydata = unserialize($mydata);
    $export_data = [];
    foreach ($mydata as $key => $value) {
      $data_val = str_ireplace("\n", "<br/>", $value);
      $export_data[] = array($key, $data_val);
    }

    wp_send_json_success(__($export_data, 'default'));
}

add_action('wp_ajax_divi_machine_import_action', 'divi_machine_import_action');

function divi_machine_import_action() {
  $setting_csv = file_get_contents(wp_get_attachment_url($_REQUEST['file_id'])); // phpcs:ignore
  $lines = explode("\n", $setting_csv); // split data by new lines
  $linevalues = [];
  foreach ($lines as $i => $line) {

      $values = explode(',', $line);
      $key = $values[0];
      $value = $values[1];
      if (count($values) > 2) {
          unset($values[0]);
          $value = implode($values, ",");
          $value = trim( $value,'"');
      }
      $linevalues[$key] = str_ireplace( "<br/>", "\n", trim($value, '"') );
  }

  $linevalues = serialize($linevalues);

  update_option('divi-machine_options', $linevalues);
  wp_send_json_success(__('Successfully imported', 'default'));
}


