<?php
/**
 * @author      Elicus <hello@elicus.com>
 * @link        https://www.elicus.com/
 * @copyright   2021 Elicus Technologies Private Limited
 * @version     1.2.0
 */
class EL_DynamicMasonryGallery extends ET_Builder_Module {

	public $slug       = 'el_dynamic_masonry_gallery';
	public $vb_support = 'on';

	protected $module_credits = array(
		'module_uri' => 'https://diviextended.com/product/divi-masonry-gallery/',
		'author'     => 'Elicus',
		'author_uri' => 'https://elicus.com/',
	);

	public function init() {
		$this->name = esc_html__( 'Dynamic Masonry Gallery', 'divi-masonry-gallery' );
		$this->main_css_element = '%%order_class%%';
		add_filter( 'et_builder_processed_range_value', array( $this, 'el_builder_processed_range_value' ), 10, 3 );
	}

	public function get_settings_modal_toggles() {
		return array(
			'general'  => array(
				'toggles' => array(
					'main_content' => array(
						'title' => esc_html__( 'Configuration', 'divi-masonry-gallery' ),
					),
					'elements' => array(
						'title' => esc_html__( 'Elements', 'divi-masonry-gallery' ),
					),
					'lightbox' => array(
						'title' => esc_html__( 'Lightbox', 'divi-masonry-gallery' ),
					),
				),
			),
			'advanced'   => array(
				'toggles' => array(
					'image_text' => array(
						'title' => esc_html__( 'Text', 'divi-masonry-gallery' ),
                        'sub_toggles'   => array(
                            'title_text' => array(
                                'name' => 'Title',
                            ),
                            'caption_text' => array(
                                'name' => 'Caption',
                            ),
                        ),
                        'tabbed_subtoggles' => true,
					),
					'category' => array(
                        'title' => esc_html__( 'Category', 'divi-masonry-gallery' ),
                        'sub_toggles'   => array(
                                                'normal'  => array(
                                                    'name' => 'Normal',
                                                ),
                                                'active' => array(
                                                    'name' => 'Active',
                                                ),
                                            ),
                        'tabbed_subtoggles' => true,
                    ),
					'lightbox' => array(
						'title' => esc_html__( 'Lightbox', 'divi-masonry-gallery' ),
					),
					'overlay' => array(
						'title' => esc_html__( 'Overlay', 'divi-masonry-gallery' ),
					),
				),
			),
		);
	}

	public function get_advanced_fields_config() {
		return array(
			'fonts' => array(
				'title' => array(
					'label'          => esc_html__( 'Title', 'divi-masonry-gallery' ),
					'font_size'      => array(
						'default'        => '18px',
						'range_settings' => array(
							'min'  => '1',
							'max'  => '100',
							'step' => '1',
						),
						'validate_unit'  => true,
					),
					'line_height'    => array(
						'default'        => '1em',
						'range_settings' => array(
							'min'  => '0.1',
							'max'  => '10',
							'step' => '0.1',
						),
					),
					'letter_spacing' => array(
						'default'        => '0px',
						'range_settings' => array(
							'min'  => '0',
							'max'  => '10',
							'step' => '1',
						),
						'validate_unit'  => true,
					),
					'header_level'   => array(
						'default' => 'h4',
					),
					'text_align'     => array(
						'default' => 'left',
					),
					'css'            => array(
						'main'       => "{$this->main_css_element} .el_masonry_gallery_item_title, {$this->main_css_element}_lightbox .el_masonry_gallery_item_title",
					),
					'tab_slug'	=> 'advanced',
                    'toggle_slug' => 'image_text',
                    'sub_toggle' => 'title_text',
				),
				'caption'    => array(
					'label'          => esc_html__( 'Caption', 'divi-masonry-gallery' ),
					'font_size'      => array(
						'default'        => '14px',
						'range_settings' => array(
							'min'  => '1',
							'max'  => '100',
							'step' => '1',
						),
						'validate_unit'  => true,
					),
					'line_height'    => array(
						'default'        => '1em',
						'range_settings' => array(
							'min'  => '0.1',
							'max'  => '10',
							'step' => '0.1',
						),
					),
					'letter_spacing' => array(
						'default'        => '0px',
						'range_settings' => array(
							'min'  => '0',
							'max'  => '10',
							'step' => '1',
						),
						'validate_unit'  => true,
					),
					'text_align'     => array(
						'default' => 'left',
					),
					'css'            => array(
						'main'  => "{$this->main_css_element} .el_masonry_gallery_item_caption, {$this->main_css_element}_lightbox .el_masonry_gallery_item_caption",
					),
					'tab_slug'	=> 'advanced',
                    'toggle_slug' => 'image_text',
                    'sub_toggle' => 'caption_text',
				),
				'category' => array(
                    'label'     => esc_html__( 'Category', 'divi-masonry-gallery' ),
                    'font_size' => array(
                        'default'           => '16px',
                        'range_settings'    => array(
                            'min'   => '1',
                            'max'   => '100',
                            'step'  => '1',
                        ),
                        'validate_unit'     => true,
                    ),
                    'line_height' => array(
                        'default'           => '1.5em',
                        'range_settings'    => array(
                            'min'   => '0.1',
                            'max'   => '10',
                            'step'  => '0.1',
                        ),
                    ),
                    'letter_spacing' => array(
                        'default'           => '0px',
                        'range_settings'    => array(
                            'min'   => '0',
                            'max'   => '10',
                            'step'  => '1',
                        ),
                        'validate_unit' => true,
                    ),
                    'hide_text_align'   => true,
                    'css'       => array(
                        'main'          => "%%order_class%% .el_masonry_gallery_filter_categories li",
                    ),
                    'toggle_slug'   => 'category',
                    'sub_toggle'    => 'normal',
                    'tab_slug'      => 'advanced',
                    'depends_on'        => array( 'enable_filterable_gallery' ),
                    'depends_show_if'   => 'on',
                ),
                'active_category' => array(
                    'label'             => esc_html__( 'Active Category', 'divi-masonry-gallery' ),
                    'font_size'         => array(
                        'default'           => '16px',
                        'range_settings'    => array(
                            'min'   => '1',
                            'max'   => '100',
                            'step'  => '1',
                        ),
                        'validate_unit'     => true,
                    ),
                    'line_height'       => array(
                        'default'           => '1.5em',
                        'range_settings'    => array(
                            'min'   => '0.1',
                            'max'   => '10',
                            'step'  => '0.1',
                        ),
                    ),
                    'letter_spacing'    => array(
                        'default'           => '0px',
                        'range_settings'    => array(
                            'min'   => '0',
                            'max'   => '10',
                            'step'  => '1',
                        ),
                        'validate_unit' => true,
                    ),
                    'hide_text_align'   => true,
                    'css'               => array(
                        'main'      => "%%order_class%% .el_masonry_gallery_filter_categories li.el_masonry_gallery_active_category",
                    ),
                    'toggle_slug'       => 'category',
                    'sub_toggle'        => 'active',
                    'tab_slug'          => 'advanced',
                    'depends_on'        => array( 'enable_filterable_gallery' ),
                    'depends_show_if'   => 'on',
                ),
			),
			'margin_padding' => array(
				'css' => array(
					'main'      => '%%order_class%%',
					'important' => 'all',
				),
			),
			'max_width' => array(
				'css' => array(
					'main'             => '%%order_class%%',
					'module_alignment' => '%%order_class%%',
				),
			),
			'borders' => array(
				'image' => array(
					'label_prefix' => 'Image',
					'css'          => array(
						'main' => array(
							'border_radii'  => "{$this->main_css_element} .el_masonry_gallery_item img, {$this->main_css_element} .el_masonry_gallery_item .et_overlay",
							'border_styles' => "{$this->main_css_element} .el_masonry_gallery_item img",
						),
						'important' => 'all',
					),
					'tab_slug'     => 'advanced',
					'toggle_slug'  => 'border',
				),
				'default' => array(
					'css' => array(
						'main' => array(
							'border_styles' => '%%order_class%%',
							'border_radii'  => '%%order_class%%',
						),
					),
				),
			),
			'box_shadow' => array(
				'image' => array(
					'label'       => esc_html__( 'Image Box Shadow', 'divi-masonry-gallery' ),
					'css'         => array(
						'main' => "{$this->main_css_element} .el_masonry_gallery_item img",
						'important' => 'all',
					),
					'tab_slug'    => 'advanced',
					'toggle_slug' => 'box_shadow',
				),
				'default' => array(
					'css' => array(
						'main' => $this->main_css_element,
						'important' => 'all',
					),
				),
			),
			'background' => array(
				'use_background_video' => false,
				'options' => array(
					'parallax' => array( 'type' => 'skip' ),
				),
			),
			'text' => false,
			'filters' => false,
		);
	}

	public function get_fields() {
		return array_merge(
			array(
				'number_of_images' => array(
					'label'            => esc_html__( 'Number of Images', 'divi-masonry-gallery' ),
					'type'             => 'text',
					'option_category'  => 'configuration',
					'default'          => '20',
					'tab_slug'         => 'general',
					'toggle_slug'      => 'main_content',
					'description'      => esc_html__( 'Choose how many images you would like to display.', 'divi-masonry-gallery' ),
					'computed_affects' => array(
						'__gallery_data',
					)
				),
				'offset_number' => array(
	                'label'            => esc_html__( 'Offset Number', 'divi-masonry-gallery' ),
	                'type'             => 'text',
	                'option_category'  => 'configuration',
	                'default'          => 0,
	                'tab_slug'         => 'general',
	                'toggle_slug'      => 'main_content',
	                'description'      => esc_html__( 'Choose how many images you would like to skip. These images will not be shown in the gallery.', 'divi-masonry-gallery' ),
	                'computed_affects' => array(
	                    '__gallery_data',
	                ),
	            ),
	            'gallery_order_by' => array(
	                'label'            => esc_html__( 'Order by', 'divi-masonry-gallery' ),
	                'type'             => 'select',
	                'option_category'  => 'configuration',
	                'options'          => array(
	                    'date'      => esc_html__( 'Date', 'divi-masonry-gallery' ),
	                    'modified'  => esc_html__( 'Modified Date', 'divi-masonry-gallery' ),
	                    'title'     => esc_html__( 'Title', 'divi-masonry-gallery' ),
	                    'name'      => esc_html__( 'Slug', 'divi-masonry-gallery' ),
	                    'ID'        => esc_html__( 'ID', 'divi-masonry-gallery' ),
	                    'rand'      => esc_html__( 'Random', 'divi-masonry-gallery' ),
	                    'none'      => esc_html__( 'None', 'divi-masonry-gallery' ),
	                ),
	                'default'          => 'date',
	                'tab_slug'         => 'general',
	                'toggle_slug'      => 'main_content',
	                'description'      => esc_html__( 'Here you can choose the order type of your images.', 'divi-masonry-gallery' ),
	                'computed_affects' => array(
	                    '__gallery_data',
	                ),
	            ),
	            'gallery_order' => array(
	                'label'            => esc_html__( 'Order', 'divi-masonry-gallery' ),
	                'type'             => 'select',
	                'option_category'  => 'configuration',
	                'options'          => array(
	                    'DESC' => esc_html__( 'DESC', 'divi-masonry-gallery' ),
	                    'ASC'  => esc_html__( 'ASC', 'divi-masonry-gallery' ),
	                ),
	                'default'          => 'DESC',
	                'tab_slug'         => 'general',
	                'toggle_slug'      => 'main_content',
	                'description'      => esc_html__( 'Here you can choose the order of your images.', 'divi-masonry-gallery' ),
	                'computed_affects' => array(
	                    '__gallery_data',
	                ),
	            ),
	            'include_categories' => array(
	                'label'             => esc_html__( 'Select Categories', 'divi-masonry-gallery' ),
	                'type'              => 'categories',
	                'option_category'   => 'basic_option',
	                'renderer_options'  => array(
	                    'use_terms'  => true,
	                    'term_name'  => 'attachment-category',
	                    'field_name' => 'el_include_attachment_categories',
	                    ),
	                'tab_slug'          => 'general',
	                'toggle_slug'       => 'main_content',
	                'description'       => esc_html__( 'Here you can choose which category images you would like to display. If you want to display all categories images, then leave it unchecked.', 'divi-masonry-gallery' ),
	                'computed_affects'  => array(
	                    '__gallery_data'
	                )
	            ),
				'number_of_columns' => array(
	                'label'             => esc_html__( 'Number Of Columns', 'divi-masonry-gallery' ),
	                'type'              => 'select',
	                'option_category'   => 'configuration',
	                'options'           => array(
	                    '1'         => esc_html( '1' ),
	                    '2'         => esc_html( '2' ),
	                    '3'         => esc_html( '3' ),
	                    '4'         => esc_html( '4' ),
	                    '5'         => esc_html( '5' ),
	                    '6'         => esc_html( '6' ),
	                    '7'         => esc_html( '7' ),
	                    '8'         => esc_html( '8' ),
	                    '9'         => esc_html( '9' ),
	                    '10'        => esc_html( '10' ),
	                    '11'        => esc_html( '11' ),
	                    '12'        => esc_html( '12' ),
	                    '13'        => esc_html( '13' ),
	                    '14'        => esc_html( '14' ),
	                    '15'        => esc_html( '15' ),
	                ),
	                'default'           => '4',
	                'default_on_front'  => '4',
	                'mobile_options'    => true,
	                'tab_slug'          => 'general',
	                'toggle_slug'       => 'main_content',
	                'description'       => esc_html__( 'Here you can select the number of columns to display images.', 'divi-masonry-gallery' ),
	            ),
	            'column_spacing' => array(
	                'label'             => esc_html__( 'Column Spacing', 'divi-masonry-gallery' ),
					'type'              => 'range',
					'option_category'  	=> 'layout',
					'range_settings'    => array(
						'min'   => '0',
						'max'   => '100',
						'step'  => '1',
					),
					'fixed_unit'		=> 'px',
					'fixed_range'       => true,
					'validate_unit'		=> true,
					'mobile_options'    => true,
					'default'           => '15px',
					'default_on_front'  => '15px',
					'tab_slug'        	=> 'general',
					'toggle_slug'     	=> 'main_content',
					'description'       => esc_html__( 'Increase or decrease spacing between columns.', 'divi-masonry-gallery' ),
	            ),
	            'image_size' => array(
	                'label'             => esc_html__( 'Image Size', 'divi-masonry-gallery' ),
	                'type'              => 'select',
	                'option_category'   => 'configuration',
	                'options'           => array(
	                    'medium' 	=> esc_html__( 'Medium', 'divi-masonry-gallery' ),
	                    'large' 	=> esc_html__( 'Large', 'divi-masonry-gallery' ),
	                    'full' 		=> esc_html__( 'Full', 'divi-masonry-gallery' ),
	                ),
	                'default'           => 'medium',
	                'default_on_front'  => 'medium',
	                'tab_slug'          => 'general',
	                'toggle_slug'       => 'main_content',
	                'description'       => esc_html__( 'Here you can select the number of columns to display images.', 'divi-masonry-gallery' ),
	                'computed_affects' 	=> array(
						'__gallery_data',
					),
	            ),
	            'enable_filterable_gallery' => array(
					'label'            => esc_html__( 'Enable Filterable Gallery', 'divi-masonry-gallery' ),
					'type'             => 'yes_no_button',
					'option_category'  => 'configuration',
					'options'          => array(
						'on'  => esc_html__( 'Yes', 'divi-masonry-gallery' ),
						'off' => esc_html__( 'No', 'divi-masonry-gallery' ),
					),
					'default' 			=> 'off',
					'default_on_front' 	=> 'off',
					'tab_slug'          => 'general',
					'toggle_slug'      	=> 'elements',
					'description'      	=> esc_html__( 'Whether or not to show the filterable gallery. For filterable gallery, you must assign a category to the image. You can assign it either in the attachment page aur while selecting images.', 'divi-masonry-gallery' ),
					'computed_affects' 	=> array(
						'__gallery_data',
					),
				),
				'all_images_text' => array(
		            'label'             => esc_html__( 'All Images Text', 'divi-masonry-gallery' ),
		            'type'              => 'text',
		            'option_category'   => 'configuration',
		            'default'           => esc_html__( 'All', 'divi-masonry-gallery' ),
		            'show_if'           => array(
		                'enable_filterable_gallery' => 'on',
		            ),
		            'tab_slug'          => 'general',
		            'toggle_slug'       => 'elements',
		            'description'       => esc_html__( 'Here you can define the All images text you would like to display.', 'divi-masonry-gallery' ),
		            'computed_affects'  => array(
		                '__gallery_data'
		            )
		        ),
	            'show_title' => array(
					'label'            => esc_html__( 'Show Title', 'divi-masonry-gallery' ),
					'type'             => 'yes_no_button',
					'option_category'  => 'configuration',
					'options'          => array(
						'on'  => esc_html__( 'Yes', 'divi-masonry-gallery' ),
						'off' => esc_html__( 'No', 'divi-masonry-gallery' ),
					),
					'default' 			=> 'off',
					'default_on_front' 	=> 'off',
					'tab_slug'          => 'general',
					'toggle_slug'      	=> 'elements',
					'description'      	=> esc_html__( 'Whether or not to show the title for images (if available).', 'divi-masonry-gallery' ),
				),
				'title_area' => array(
					'label'             => esc_html__( 'Show Title in', 'divi-masonry-gallery' ),
	                'type'              => 'select',
	                'option_category'   => 'configuration',
	                'options'           => array(
	                    'lightbox'	=> esc_html__( 'Lightbox Only', 'divi-masonry-gallery' ),
	                    'gallery' 	=> esc_html__( 'Gallery Only', 'divi-masonry-gallery' ),
	                    'both'		=> esc_html__( 'Both', 'divi-masonry-gallery' ),
	                ),
	                'default'           => 'lightbox',
	                'default_on_front'  => 'lightbox',
	                'show_if'         	=> array(
	                    'show_title' => 'on',
	                ),
	                'tab_slug'          => 'general',
	                'toggle_slug'       => 'elements',
	                'description'       => esc_html__( 'Here you can select the area where you want to display title.', 'divi-masonry-gallery' ),
				),
				'show_caption' => array(
					'label'            => esc_html__( 'Show Caption', 'divi-masonry-gallery' ),
					'type'             => 'yes_no_button',
					'option_category'  => 'configuration',
					'options'          => array(
						'on'  => esc_html__( 'Yes', 'divi-masonry-gallery' ),
						'off' => esc_html__( 'No', 'divi-masonry-gallery' ),
					),
					'default' 			=> 'off',
					'default_on_front' 	=> 'off',
					'tab_slug'          => 'general',
					'toggle_slug'      	=> 'elements',
					'description'      	=> esc_html__( 'Whether or not to show the caption for images (if available).', 'divi-masonry-gallery' ),
				),
				'caption_area' => array(
					'label'             => esc_html__( 'Show Caption in', 'divi-masonry-gallery' ),
	                'type'              => 'select',
	                'option_category'   => 'configuration',
	                'options'           => array(
	                    'lightbox'	=> esc_html__( 'Lightbox Only', 'divi-masonry-gallery' ),
	                    'gallery' 	=> esc_html__( 'Gallery Only', 'divi-masonry-gallery' ),
	                    'both'		=> esc_html__( 'Both', 'divi-masonry-gallery' ),
	                ),
	                'default'           => 'lightbox',
	                'default_on_front'  => 'lightbox',
	                'show_if'         	=> array(
	                    'show_caption' => 'on',
	                ),
	                'tab_slug'          => 'general',
	                'toggle_slug'       => 'elements',
	                'description'       => esc_html__( 'Here you can select the area where you want to display caption.', 'divi-masonry-gallery' ),
				),
	            'enable_lightbox' => array(
					'label'             => esc_html__( 'OnClick Trigger', 'divi-masonry-gallery' ),
					'type'              => 'select',
	                'option_category'   => 'configuration',
	                'options'           => array(
	                	'off'	=> esc_html__( 'None', 'divi-masonry-gallery' ),
	                    'on'	=> esc_html__( 'Lightbox', 'divi-masonry-gallery' ),
	                    'link' 	=> esc_html__( 'Link', 'divi-masonry-gallery' ),
	                ),
					'default' 			=> 'off',
					'default_on_front' 	=> 'off',
					'tab_slug'          => 'general',
					'toggle_slug'      	=> 'main_content',
					'description'      	=> esc_html__( 'Choose an action to perform on clicking of an image.', 'divi-masonry-gallery' ),
				),
				'link_target'   => array(
					'label'            => esc_html__( 'Link Target', 'divi-masonry-gallery' ),
					'type'             => 'select',
					'option_category'  => 'configuration',
					'options'          => array(
						'off' => esc_html__( 'In The Same Window', 'divi-masonry-gallery' ),
						'on'  => esc_html__( 'In The New Tab', 'divi-masonry-gallery' ),
					),
					'show_if'          => array(
	                    'enable_lightbox' => 'link',
	                ),
					'default_on_front' => 'off',
					'tab_slug'         => 'general',
					'toggle_slug'      => 'main_content',
					'description'      => esc_html__( 'Here you can choose whether or not your link opens in a new window', 'divi-masonry-gallery' ),
				),
				'lightbox_effect' => array(
					'label'            => esc_html__( 'Lighbox Effect', 'divi-masonry-gallery' ),
					'type'             => 'select',
					'option_category'  => 'configuration',
					'options'          => array(
						'none' => esc_html__( 'None', 'divi-masonry-gallery' ),
						'zoom' => esc_html__( 'Zoom', 'divi-masonry-gallery' ),
						'fade' => esc_html__( 'Fade', 'divi-masonry-gallery' ),
					),
					'show_if'          => array(
	                    'enable_lightbox' => 'on',
	                ),
					'default_on_front' => 'none',
					'tab_slug'         => 'general',
					'toggle_slug'      => 'lightbox',
					'description'      => esc_html__( 'Here you can choose opening effect of lightbox.', 'divi-masonry-gallery' ),
				),
				'lightbox_transition_duration' => array(
	                'label'             => esc_html__( 'Transition Duration', 'divi-plus' ),
	                'type'              => 'range',
	                'option_category'	=> 'layout',
	                'range_settings'        => array(
	                    'min'  => '100',
	                    'max'  => '2000',
	                    'step' => '100',
	                ),
	                'show_if'          	=> array(
	                    'enable_lightbox' => 'on',
	                    'lightbox_effect' => array( 'zoom', 'fade' ),
	                ),
	                'unitless'          => true,
	                'default_on_front'  => '300',
	                'tab_slug'          => 'general',
	                'toggle_slug'       => 'lightbox',
	                'description'       => esc_html__( 'Here you can select the transition duration in miliseconds.', 'divi-plus' ),
	            ),
				'enable_navigation' => array(
					'label'             => esc_html__( 'Enable Navigation', 'divi-masonry-gallery' ),
					'type'              => 'yes_no_button',
					'option_category'   => 'configuration',
					'options'           => array(
						'on'  => esc_html__( 'Yes', 'divi-masonry-gallery' ),
						'off' => esc_html__( 'No', 'divi-masonry-gallery' ),
					),
					'show_if'         	=> array(
	                    'enable_lightbox' => 'on',
	                ),
					'default' 			=> 'on',
					'default_on_front' 	=> 'on',
					'tab_slug'          => 'general',
					'toggle_slug'      	=> 'lightbox',
					'description'      	=> esc_html__( 'Whether or not to enable navigation in lightbox.', 'divi-masonry-gallery' ),
				),
				'lightbox_title_and_caption_style' => array(
					'label'             => esc_html__( 'Title & Caption Style', 'divi-masonry-gallery' ),
	                'type'              => 'select',
	                'option_category'   => 'configuration',
	                'options'           => array(
	                    'image_overlay'	=> esc_html__( 'Image Overlay', 'divi-masonry-gallery' ),
	                    'below_image' 	=> esc_html__( 'Below Image', 'divi-masonry-gallery' ),
	                ),
	                'default'           => 'image_overlay',
	                'default_on_front'  => 'image_overlay',
	                'show_if'         	=> array(
	                    'enable_lightbox' => 'on',
	                ),
	                'tab_slug'          => 'general',
	                'toggle_slug'       => 'lightbox',
	                'description'       => esc_html__( 'Here you can select the display style of title and caption in lightbox.', 'divi-masonry-gallery' ),
				),
				'enable_overlay' => array(
					'label'            => esc_html__( 'Enable Image Overlay on Hover', 'divi-masonry-gallery' ),
					'type'             => 'yes_no_button',
					'option_category'  => 'configuration',
					'options'          => array(
						'on'  => esc_html__( 'Yes', 'divi-masonry-gallery' ),
						'off' => esc_html__( 'No', 'divi-masonry-gallery' ),
					),
					'default' 			=> 'off',
					'default_on_front' 	=> 'off',
					'tab_slug'          => 'general',
					'toggle_slug'      	=> 'elements',
					'description'      	=> esc_html__( 'Whether or not to show images in lightbox.', 'divi-masonry-gallery' ),
					'computed_affects'  => array(
	                    '__gallery_data'
	                )
				),
				'overlay_icon' => array(
					'label'           => esc_html__( 'Overlay Icon', 'divi-masonry-gallery' ),
					'type'            => 'select_icon',
					'option_category' => 'configuration',
					'class'           => array( 'et-pb-font-icon' ),
					'show_if'         => array(
	                    'enable_overlay' => 'on',
	                ),
					'tab_slug'        => 'general',
					'toggle_slug'     => 'elements',
					'description'     => esc_html__( 'Here you can define a custom icon for the overlay', 'divi-masonry-gallery' ),
					'computed_affects'  => array(
	                    '__gallery_data'
	                )
				),
				'meta_background_color' => array(
					'label'           	=> esc_html__( 'Title & Caption Background Color', 'divi-masonry-gallery' ),
					'type'            	=> 'color-alpha',
					'custom_color'    	=> true,
					'default'		  	=> 'rgba(0,0,0,0.6)',
					'default_on_front'	=> 'rgba(0,0,0,0.6)',
					'show_if'         	=> array(
	                    'enable_lightbox' => 'on',
	                ),
					'tab_slug'        	=> 'advanced',
					'toggle_slug'     	=> 'lightbox',
					'description'     	=> esc_html__( 'Here you can define a custom overlay color for the title and caption.', 'divi-masonry-gallery' ),
				),
				'lightbox_background_color' => array(
					'label'           	=> esc_html__( 'Lightbox Background Color', 'divi-masonry-gallery' ),
					'type'            	=> 'color-alpha',
					'custom_color'    	=> true,
					'default'		  	=> 'rgba(0,0,0,0.8)',
					'default_on_front'	=> 'rgba(0,0,0,0.8)',
					'show_if'         	=> array(
	                    'enable_lightbox' => 'on',
	                ),
					'tab_slug'        	=> 'advanced',
					'toggle_slug'     	=> 'lightbox',
					'description'     	=> esc_html__( 'Here you can define a custom background color for the lightbox.', 'divi-masonry-gallery' ),
				),
				'lightbox_close_icon_color' => array(
					'label'           	=> esc_html__( 'Close Icon Color', 'divi-masonry-gallery' ),
					'type'            	=> 'color-alpha',
					'custom_color'    	=> true,
					'default'		  	=> '#fff',
					'default_on_front'	=> '#fff',
					'show_if'         	=> array(
	                    'enable_lightbox' => 'on',
	                ),
					'tab_slug'        	=> 'advanced',
					'toggle_slug'     	=> 'lightbox',
					'description'     	=> esc_html__( 'Here you can define a custom color for the close icon.', 'divi-masonry-gallery' ),
				),
				'lightbox_arrows_color' => array(
					'label'           	=> esc_html__( 'Arrows Color', 'divi-masonry-gallery' ),
					'type'            	=> 'color-alpha',
					'custom_color'    	=> true,
					'default'		  	=> '#fff',
					'default_on_front'	=> '#fff',
					'show_if'         	=> array(
	                    'enable_lightbox' => 'on',
	                ),
					'tab_slug'        	=> 'advanced',
					'toggle_slug'     	=> 'lightbox',
					'description'     	=> esc_html__( 'Here you can define a custom color for the arrows.', 'divi-masonry-gallery' ),
				),
				'overlay_icon_size' => array(
	                'label'             => esc_html__( 'Icon Size', 'divi-masonry-gallery' ),
					'type'              => 'range',
					'option_category'  	=> 'layout',
					'range_settings'    => array(
						'min'   => '0',
						'max'   => '100',
						'step'  => '1',
					),
					'fixed_unit'		=> 'px',
					'fixed_range'       => true,
					'validate_unit'		=> true,
					'mobile_options'    => true,
					'default'           => '32px',
					'default_on_front'  => '32px',
					'show_if'         	=> array(
	                    'enable_overlay' => 'on',
	                ),
					'tab_slug'        	=> 'advanced',
					'toggle_slug'     	=> 'overlay',
					'description'       => esc_html__( 'Increase or decrease icon font size.', 'divi-masonry-gallery' ),
	            ),
				'overlay_icon_color' => array(
					'label'           => esc_html__( 'Overlay Icon Color', 'divi-masonry-gallery' ),
					'type'            => 'color-alpha',
					'custom_color'    => true,
					'show_if'         => array(
	                    'enable_overlay' => 'on',
	                ),
					'tab_slug'        => 'advanced',
					'toggle_slug'     => 'overlay',
					'description'     => esc_html__( 'Here you can define a custom color for the icon.', 'divi-masonry-gallery' ),
				),
				'overlay_color' => array(
					'label'           => esc_html__( 'Overlay Background Color', 'divi-masonry-gallery' ),
					'type'            => 'color-alpha',
					'custom_color'    => true,
					'show_if'         => array(
	                    'enable_overlay' => 'on',
	                ),
					'tab_slug'        => 'advanced',
					'toggle_slug'     => 'overlay',
					'description'     => esc_html__( 'Here you can define a custom color for the overlay', 'divi-masonry-gallery' ),
				),
				'category_bg_color' => array(
	                'label'             => esc_html__( 'Category Background', 'divi-masonry-gallery' ),
	                'type'              => 'background-field',
	                'base_name'         => 'category_bg',
	                'context'           => 'category_bg_color',
	                'option_category'   => 'button',
	                'custom_color'      => true,
	                'background_fields' => $this->generate_background_options( 'category_bg', 'button', 'advanced', 'category', 'category_bg_color' ),
	                'hover'             => 'tabs',
	                'show_if'           => array(
	                    'enable_filterable_gallery'  => 'on',
	                ),
	                'tab_slug'          => 'advanced',
	                'toggle_slug'       => 'category',
	                'sub_toggle'        => 'normal',
	                'description'       => esc_html__( 'Here you can adjust the background style of the category by customizing the background color, gradient, and image.', 'divi-masonry-gallery' ),
	            ),
	            'active_category_bg_color' => array(
	                'label'             => esc_html__( 'Active Category Background', 'divi-masonry-gallery' ),
	                'type'              => 'background-field',
	                'base_name'         => 'active_category_bg',
	                'context'           => 'active_category_bg_color',
	                'option_category'   => 'button',
	                'custom_color'      => true,
	                'background_fields' => $this->generate_background_options( 'active_category_bg', 'button', 'advanced', 'category', 'active_category_bg_color' ),
	                'hover'             => 'tabs',
	                'show_if'           => array(
	                    'enable_filterable_gallery'  => 'on',
	                ),
	                'tab_slug'          => 'advanced',
	                'toggle_slug'       => 'category',
	                'sub_toggle'        => 'active',
	                'description'       => esc_html__( 'Here you can adjust the background style of the active category by customizing the background color, gradient, and image.', 'divi-masonry-gallery' ),
	            ),
				'__gallery_data' => array(
					'type'                => 'computed',
					'computed_callback'   => array( 'EL_DynamicMasonryGallery', 'get_gallery' ),
					'computed_depends_on' => array(
						'number_of_images',
						'offset_number',
						'gallery_order_by',
						'gallery_order',
						'include_categories',
						'image_size',
						'enable_filterable_gallery',
						'all_images_text',
						'enable_overlay',
						'overlay_icon',
						'title_level',
					),
				),
			),
			$this->generate_background_options( 'category_bg', 'skip', 'advanced', 'category', 'category_bg_color' ),
			$this->generate_background_options( 'active_category_bg', 'skip', 'advanced', 'category', 'active_category_bg_color' )
		);

	}

	public static function get_gallery( $args = array(), $conditional_tags = array(), $current_page = array() ) {
		$defaults = array(
			'number_of_images' 			=> '20',
			'offset_number'		 		=> '0',
			'gallery_order_by' 			=> 'date',
			'gallery_order' 			=> 'DESC',
			'include_categories' 		=> '',
			'image_size' 				=> 'medium',
			'enable_filterable_gallery' => 'off',
			'all_images_text' 			=> 'All',
			'enable_overlay' 			=> 'off',
			'overlay_icon' 				=> '',
			'title_level' 				=> 'h4',
		);

		$args = wp_parse_args( $args, $defaults );

		foreach ( $defaults as $key => $default ) {
			${$key} = esc_html( et_()->array_get( $args, $key, $default ) );
		}

		$processed_title_level 	= et_pb_process_header_level( $title_level, 'h4' );
		$processed_title_level	= esc_html( $processed_title_level );

		$number_of_images = ( 0 === $number_of_images ) ? -1 : (int) $number_of_images;

		$args = array(
			'post_type'      => 'attachment',
			'posts_per_page' => $number_of_images,
			'post_status'	 => 'any',
			'orderby'        => sanitize_text_field( $gallery_order_by ),
			'order'          => sanitize_text_field( $gallery_order ),
			'offset'		 => intval( $offset_number ),
			'post_mime_type' => 'image/jpeg,image/gif,image/jpg,image/png',
		);

		if ( '' !== $include_categories ) {
			$args['tax_query'] = array(
				array(
					'taxonomy' => 'attachment-category',
					'field'    => 'term_id',
					'terms'    => array_map( 'intval', explode( ',', $include_categories ) ),
					'operator' => 'IN',
				),
			);
		}

		$query = new WP_Query( $args );

		if ( $query->have_posts() ) {
			$image_ids = wp_list_pluck( $query->posts, 'ID' );

			if ( 'on' === $enable_overlay && '' !== $overlay_icon ) {
				$overlay_output = sprintf(
					'<span class="et_overlay et_pb_inline_icon" data-icon="%1$s"></span>',
					esc_attr( et_pb_process_font_icon( $overlay_icon ) )
				);
			}

			$gallery_items = '';

			$attachment_categories = array();
			foreach( $image_ids as $image_id ) {
				if ( '' !== trim( wptexturize( get_the_title( $image_id ) ) ) ) {
					$title = sprintf(
						'<%1$s class="el_masonry_gallery_item_title">%2$s</%1$s>',
						$processed_title_level,
						esc_html( wptexturize( get_the_title( $image_id ) ) )
					);
				} else {
					$title = '';
				}

				if ( '' !== trim( wp_get_attachment_caption( $image_id ) ) ) {
					$caption = sprintf(
						'<p class="el_masonry_gallery_item_caption">%2$s</p>',
						$processed_title_level,
						esc_html( wp_get_attachment_caption( $image_id ) )
					);
				} else {
					$caption = '';
				}

				if ( '' !== $title || '' !== $caption ) {
					$title_and_caption = sprintf(
						'<div class="el_masonry_gallery_title_caption_wrapper">%1$s%2$s</div>',
						et_core_intentionally_unescaped( $title, 'html' ),
						et_core_intentionally_unescaped( $caption, 'html' )
					);
				} else {
					$title_and_caption = '';
				}

				$categories = get_the_terms( $image_id, 'attachment-category' );
				$image_atts = '';
				if ( $categories && ! is_wp_error( $categories ) && ! empty( $categories ) ) {
					foreach ( $categories as $category ) {
						$attachment_categories[$category->slug] = sprintf(
							esc_html__( '%1$s', 'divi-masonry-gallery' ),
							$category->name
						);
					}
					$categories 	= wp_list_pluck( $categories, 'slug' );
					$image_classes	= array_merge( 
						array( 
							"attachment-$image_size",
							"size-$image_size"
						),
						$categories
					);
					$image_classes 	= implode( ' ', $image_classes );
					$image_atts 	= array(
						'class' => $image_classes
					);
				}

				$gallery_items .= sprintf(
					'<div class="el_masonry_gallery_item">
						<div class="el_masonry_gallery_image_wrapper">%1$s%2$s</div>
						%3$s
					</div>',
					et_core_intentionally_unescaped( wp_get_attachment_image( intval( $image_id ), sanitize_text_field( $image_size ), false, $image_atts ), 'html' ),
					isset( $overlay_output ) ? $overlay_output : '',
					et_core_intentionally_unescaped( $title_and_caption, 'html' )
				);
			}

			if ( 'on' === $enable_filterable_gallery && ! empty ( $attachment_categories ) && '1' < count( $attachment_categories ) ) {
				$all_images_text = '' === $all_images_text ?
		            esc_html__( 'All', 'divi-masonry-gallery' ) :
		            sprintf(
		                esc_html__( '%s', 'divi-masonry-gallery' ),
		                esc_html( $all_images_text )
		            );
		            
				$categories = sprintf(
						'<li data-category="">%1$s</li>',
						esc_html( $all_images_text )
				);
				foreach ( $attachment_categories as $slug => $name ) {
					$categories .= sprintf(
						'<li data-category="%1$s">%2$s</li>',
						esc_attr( $slug ),
						esc_html( $name )
					);
				}
				$filter = sprintf(
					'<div class="el_masonry_gallery_filter_wrapper">
						<ul class="el_masonry_gallery_filter_categories">
						%1$s
						</ul>
					</div>',
					$categories
				);
			} else {
				$filter = '';
			}

			$output = sprintf(
				'%1$s
				<div class="el_masonry_gallery_wrapper">
					<div class="el_masonry_gallery_item_gutter"></div>
					%2$s%
				</div>',
				$filter,
				$gallery_items
			);
		} else {
			$output = '<div className="entry"><h4>No Gallery Found!!</h4></div>';
		}

		return $output;
	}

	public function render( $attrs, $content = null, $render_slug ) {
		$multi_view   						= et_pb_multi_view_options( $this );
		$number_of_images 					= $this->props['number_of_images'];
		$offset_number 						= $this->props['offset_number'];
		$gallery_order_by 					= $this->props['gallery_order_by'];
		$gallery_order 						= $this->props['gallery_order'];
		$include_categories 				= $this->props['include_categories'];
		$image_size   						= $this->props['image_size'];
		$enable_filterable_gallery			= $this->props['enable_filterable_gallery'];
		$all_images_text					= $this->props['all_images_text'];
		$enable_lightbox 					= $this->props['enable_lightbox'];
		$link_target						= $this->props['link_target'];
		$lightbox_effect					= $this->props['lightbox_effect'];
		$lightbox_transition_duration		= $this->props['lightbox_transition_duration'];
		$enable_navigation					= $this->props['enable_navigation'];
		$enable_overlay						= $this->props['enable_overlay'];
		$show_title 						= $this->props['show_title'];
		$title_area							= $this->props['title_area'];
		$show_caption 						= $this->props['show_caption'];
		$caption_area						= $this->props['caption_area'];
		$lightbox_title_and_caption_style 	= $this->props['lightbox_title_and_caption_style'];
		$number_of_columns					= $this->props['number_of_columns'];
		$column_spacing 					= $this->props['column_spacing'];
		$overlay_icon       				= $this->props['overlay_icon'];
		$overlay_icon_color					= $this->props['overlay_icon_color'];
		$overlay_color						= $this->props['overlay_color'];
		$meta_background_color				= $this->props['meta_background_color'];
		$lightbox_background_color 			= $this->props['lightbox_background_color'];
		$lightbox_close_icon_color			= $this->props['lightbox_close_icon_color'];
		$lightbox_arrows_color 				= $this->props['lightbox_arrows_color'];
		$title_level           				= $this->props['title_level'];
		$processed_title_level  			= et_pb_process_header_level( $title_level, 'h4' );
		$processed_title_level  			= esc_html( $processed_title_level );

		$number_of_images = ( 0 === $number_of_images ) ? -1 : (int) $number_of_images;

		$args = array(
			'post_type'      => 'attachment',
			'posts_per_page' => $number_of_images,
			'post_status'    => 'any',
			'orderby'        => sanitize_text_field( $gallery_order_by ),
			'order'          => sanitize_text_field( $gallery_order ),
			'offset'		 => intval( $offset_number ),
			'post_mime_type' => 'image/jpeg,image/gif,image/jpg,image/png',
		);

		if ( '' !== $include_categories ) {
			$args['tax_query'] = array(
				array(
					'taxonomy' => 'attachment-category',
					'field'    => 'term_id',
					'terms'    => array_map( 'intval', explode( ',', $include_categories ) ),
					'operator' => 'IN',
				),
			);
		}

		$query = new WP_Query( $args );

		if ( $query->have_posts() ) {
			$image_ids = wp_list_pluck( $query->posts, 'ID' );

			wp_enqueue_script( 'elicus-isotope-script' );
			wp_enqueue_script( 'elicus-images-loaded-script' );

			if ( 'on' === $enable_lightbox ) {
				wp_enqueue_script('magnific-popup');
				wp_enqueue_style('magnific-popup');
			}

			if ( 'on' === $enable_overlay && '' !== $overlay_icon ) {
				$overlay_output = ET_Builder_Module_Helper_Overlay::render(
					array(
						'icon' => $overlay_icon,
					)
				);
			}

			$gallery_items = '';

			$attachment_categories = array();
			foreach( $image_ids as $image_id ) {
				if ( 'on' === $show_title && '' !== trim( wptexturize( get_the_title( $image_id ) ) ) ) {
					$title = $multi_view->render_element(
						array(
							'tag'        => $processed_title_level,
							'content'    => esc_html( wptexturize( get_the_title( $image_id ) ) ),
							'attrs'      => array(
								'class' => 'el_masonry_gallery_item_title',
							),
						)
					);
				} else {
					$title = '';
				}

				if ( 'on' === $show_caption && '' !== trim( wp_get_attachment_caption( $image_id ) ) ) {
					$caption = $multi_view->render_element(
								array(
							'tag'        => 'p',
							'content'    => esc_html( wp_get_attachment_caption( $image_id ) ),
							'attrs'      => array(
								'class' => 'el_masonry_gallery_item_caption',
							),
						)
					);
				} else {
					$caption = '';
				}

				if ( '' !== $title || '' !== $caption ) {
					$title_and_caption = sprintf(
						'<div class="el_masonry_gallery_title_caption_wrapper">%1$s%2$s</div>',
						et_core_intentionally_unescaped( $title, 'html' ),
						et_core_intentionally_unescaped( $caption, 'html' )
					);
				} else {
					$title_and_caption = '';
				}

				$categories = get_the_terms( $image_id, 'attachment-category' );
				$image_atts = '';
				if ( $categories && ! is_wp_error( $categories ) && ! empty( $categories ) ) {
					foreach ( $categories as $category ) {
						$attachment_categories[$category->slug] = sprintf(
							esc_html__( '%1$s', 'divi-masonry-gallery' ),
							$category->name
						);
					}
					$categories 	= wp_list_pluck( $categories, 'slug' );
					$image_classes	= array_merge( 
						array( 
							"attachment-$image_size",
							"size-$image_size"
						),
						$categories
					);
					$image_classes 	= implode( ' ', $image_classes );
					$image_atts 	= array(
						'class' => $image_classes
					);
				}

				if ( 'on' === $enable_lightbox ) {
					$gallery_items .= sprintf(
						'<a href="%4$s" class="el_masonry_gallery_item" data-lightbox="on" data-effect="%5$s" data-navigation="%6$s" data-duration="%7$s">
							<div class="el_masonry_gallery_image_wrapper">%1$s%2$s</div>
							%3$s
						</a>',
						et_core_intentionally_unescaped( wp_get_attachment_image( intval( $image_id ), sanitize_text_field( $image_size ), false, $image_atts ), 'html' ),
						isset( $overlay_output ) ? $overlay_output : '',
						et_core_intentionally_unescaped( $title_and_caption, 'html' ),
						esc_url( wp_get_attachment_url( intval( $image_id ) ) ),
						esc_attr( $lightbox_effect ),
						esc_attr( $enable_navigation ),
						'none' !== $lightbox_effect ? absint( $lightbox_transition_duration ) : 0
					);
				} else if ( 'link' === $enable_lightbox ) {
					$link = get_post_meta( intval( $image_id ), 'dge_attachment_link', true );
					$gallery_items .= sprintf(
						'<%6$s %4$s %5$s class="el_masonry_gallery_item">
							<div class="el_masonry_gallery_image_wrapper">%1$s%2$s</div>
							%3$s
						</%6$s>',
						et_core_intentionally_unescaped( wp_get_attachment_image( intval( $image_id ), sanitize_text_field( $image_size ), false, $image_atts ), 'html' ),
						isset( $overlay_output ) ? $overlay_output : '',
						et_core_intentionally_unescaped( $title_and_caption, 'html' ),
						'' !== $link ? 'href="' . esc_url( $link ) .'"' : '',
						'' !== $link ? 'on' === $link_target ? 'target="_blank"' : '' : '',
						'' !== $link ? esc_html( 'a' ) : 'div'
					);
				} else {
					$gallery_items .= sprintf(
						'<div class="el_masonry_gallery_item">
							<div class="el_masonry_gallery_image_wrapper">%1$s%2$s</div>
							%3$s
						</div>',
						et_core_intentionally_unescaped( wp_get_attachment_image( intval( $image_id ), sanitize_text_field( $image_size ), false, $image_atts ), 'html' ),
						isset( $overlay_output ) ? $overlay_output : '',
						et_core_intentionally_unescaped( $title_and_caption, 'html' )
					);
				}
			}

			if ( 'on' === $enable_filterable_gallery && ! empty ( $attachment_categories ) && '1' < count( $attachment_categories ) ) {
				$all_images_text = '' === $all_images_text ?
		            esc_html__( 'All', 'divi-masonry-gallery' ) :
		            sprintf(
		                esc_html__( '%s', 'divi-masonry-gallery' ),
		                esc_html( $all_images_text )
		            );
		            
				$categories = sprintf(
						'<li class="el_masonry_gallery_active_category" data-category="">%1$s</li>',
						esc_html( $all_images_text )
				);
				foreach ( $attachment_categories as $slug => $name ) {
					$categories .= sprintf(
						'<li data-category="%1$s">%2$s</li>',
						esc_attr( $slug ),
						esc_html( $name )
					);
				}
				$filter = sprintf(
					'<div class="el_masonry_gallery_filter_wrapper">
						<ul class="el_masonry_gallery_filter_categories">
						%1$s
						</ul>
					</div>',
					$categories
				);
			} else {
				$filter = '';
			}

			$output = sprintf(
				'%1$s
				<div class="el_masonry_gallery_wrapper">
					<div class="el_masonry_gallery_item_gutter"></div>
					%2$s%
				</div>',
				$filter,
				$gallery_items
			);

			$number_of_columns 	= et_pb_responsive_options()->get_property_values( $this->props, 'number_of_columns' );
			$column_spacing 	= et_pb_responsive_options()->get_property_values( $this->props, 'column_spacing' );
			
			$number_of_columns['tablet'] = '' !== $number_of_columns['tablet'] ? $number_of_columns['tablet'] : $number_of_columns['desktop'];
			$number_of_columns['phone']  = '' !== $number_of_columns['phone'] ? $number_of_columns['phone'] : $number_of_columns['tablet'];

			$column_spacing['tablet'] = '' !== $column_spacing['tablet'] ? $column_spacing['tablet'] : $column_spacing['desktop'];
			$column_spacing['phone']  = '' !== $column_spacing['phone'] ? $column_spacing['phone'] : $column_spacing['tablet'];
			
			$breakpoints 	= array( 'desktop', 'tablet', 'phone' );
			$width 			= array();

			foreach ( $breakpoints as $breakpoint ) {
				if ( 1 === absint( $number_of_columns[$breakpoint] ) ) {
					$width[$breakpoint] = '100%';
				} else {
					$divided_width 	= 100 / absint( $number_of_columns[$breakpoint] );
					if ( 0.0 !== floatval( $column_spacing[$breakpoint] ) ) {
						$gutter = floatval( ( floatval( $column_spacing[$breakpoint] ) * ( absint( $number_of_columns[$breakpoint] ) - 1 ) ) / absint( $number_of_columns[$breakpoint] ) );
						$width[$breakpoint] = 'calc(' . $divided_width . '% - ' . $gutter . 'px)';
					} else {
						$width[$breakpoint] = $divided_width . '%';
					}
				}
			}

			et_pb_responsive_options()->generate_responsive_css( $width, '%%order_class%% .el_masonry_gallery_item', 'width', $render_slug, '', 'range' );
			et_pb_responsive_options()->generate_responsive_css( $column_spacing, '%%order_class%% .el_masonry_gallery_item', array( 'margin-bottom' ), $render_slug, '', 'range' );
			et_pb_responsive_options()->generate_responsive_css( $column_spacing, '%%order_class%% .el_masonry_gallery_item_gutter', 'width', $render_slug, '', 'range' );

			$overlay_icon_size 	= et_pb_responsive_options()->get_property_values( $this->props, 'overlay_icon_size' );
			et_pb_responsive_options()->generate_responsive_css( $overlay_icon_size, '%%order_class%% .el_masonry_gallery_item .et_overlay:before', 'font-size', $render_slug, '', 'range' );


			if ( ! in_array( $title_area, array( 'gallery', 'both' ), true ) && ! in_array( $caption_area, array( 'gallery', 'both' ), true ) ) {
				self::set_style( $render_slug, array(
	                'selector'    => '%%order_class%% .el_masonry_gallery_title_caption_wrapper',
	                'declaration' => 'display: none;',
	            ) );
			} else {
				if ( ! in_array( $title_area, array( 'gallery', 'both' ), true ) ) {
					self::set_style( $render_slug, array(
		                'selector'    => '%%order_class%% .el_masonry_gallery_item_title',
		                'declaration' => 'display: none;',
		            ) );
				}
				if ( ! in_array( $caption_area, array( 'gallery', 'both' ), true ) ) {
					self::set_style( $render_slug, array(
		                'selector'    => '%%order_class%% .el_masonry_gallery_item_caption',
		                'declaration' => 'display: none;',
		            ) );
				}
			}

			if ( ! in_array( $title_area, array( 'lightbox', 'both' ), true ) && ! in_array( $caption_area, array( 'lightbox', 'both' ), true ) ) {
				self::set_style( $render_slug, array(
	                'selector'    => '%%order_class%%_lightbox .mfp-bottom-bar',
	                'declaration' => 'display: none;',
	            ) );
			} else {
				if ( ! in_array( $title_area, array( 'lightbox', 'both' ), true ) ) {
					self::set_style( $render_slug, array(
		                'selector'    => '%%order_class%%_lightbox .el_masonry_gallery_item_title',
		                'declaration' => 'display: none;',
		            ) );
		            self::set_style( $render_slug, array(
		                'selector'    => '%%order_class%%_lightbox .el_masonry_gallery_item_title + .el_masonry_gallery_item_caption',
		                'declaration' => 'padding: 10px;',
		            ) );
				}
				if ( ! in_array( $caption_area, array( 'lightbox', 'both' ), true ) ) {
					self::set_style( $render_slug, array(
		                'selector'    => '%%order_class%%_lightbox .el_masonry_gallery_item_caption',
		                'declaration' => 'display: none;',
		            ) );
				}
			}

			if ( 'below_image' === $lightbox_title_and_caption_style ) {
				self::set_style( $render_slug, array(
	                'selector'    => '%%order_class%%_lightbox .mfp-bottom-bar, %%order_class%%_lightbox.mfp-img-mobile .mfp-bottom-bar',
	                'declaration' => 'bottom: auto; top: 100%;',
	            ) );
			}

			if ( '' !== $overlay_icon_color ) {
				self::set_style( $render_slug, array(
	                'selector'    => '%%order_class%% .el_masonry_gallery_item .et_overlay:before',
	                'declaration' => sprintf(
	                    'color: %1$s;',
	                    esc_attr( $overlay_icon_color )
	                )
	            ) );
			}

			if ( '' !== $overlay_color ) {
				self::set_style( $render_slug, array(
	                'selector'    => '%%order_class%% .el_masonry_gallery_item .et_overlay',
	                'declaration' => sprintf(
	                    'background-color: %1$s;',
	                    esc_attr( $overlay_color )
	                )
	            ) );
			}

			if ( '' !== $meta_background_color ) {
				self::set_style( $render_slug, array(
	                'selector'    => '%%order_class%%_lightbox .el_masonry_gallery_item_title, %%order_class%%_lightbox .el_masonry_gallery_item_caption',
	                'declaration' => sprintf(
	                    'background-color: %1$s;',
	                    esc_attr( $meta_background_color )
	                )
	            ) );
			}

			if ( 'on' === $enable_lightbox ) {
				if ( '' !== $lightbox_background_color ) {
					self::set_style( $render_slug, array(
		                'selector'    => '%%order_class%%_lightbox.mfp-bg',
		                'declaration' => sprintf(
		                    'background-color: %1$s;',
		                    esc_attr( $lightbox_background_color )
		                )
		            ) );
				}

				if ( '' !== $lightbox_close_icon_color ) {
					self::set_style( $render_slug, array(
		                'selector'    => '%%order_class%%_lightbox .mfp-close',
		                'declaration' => sprintf(
		                    'color: %1$s;',
		                    esc_attr( $lightbox_close_icon_color )
		                )
		            ) );
				}

				if ( '' !== $lightbox_arrows_color ) {
					self::set_style( $render_slug, array(
		                'selector'    => '%%order_class%%_lightbox .mfp-arrow:after',
		                'declaration' => sprintf(
		                    'color: %1$s;',
		                    esc_attr( $lightbox_arrows_color )
		                )
		            ) );
				}

				if ( 'none' !== $lightbox_effect ) {
					self::set_style( $render_slug, array(
		                'selector'    => '%%order_class%%_lightbox .mfp-container, %%order_class%%_lightbox.mfp-bg, %%order_class%%_lightbox.mfp-wrap .mfp-content',
		                'declaration' => sprintf(
		                    'transition-duration: %1$sms;',
		                    absint( $lightbox_transition_duration )
		                )
		            ) );
				}
			}

			$options = array(
	            'normal' => array(
	                'category_bg' => "{$this->main_css_element} .el_masonry_gallery_filter_categories li",
	                'active_category_bg' => "{$this->main_css_element} .el_masonry_gallery_filter_categories li.el_masonry_gallery_active_category",
	            ),
	            'hover' => array(
	                'category_bg' => "{$this->main_css_element} .el_masonry_gallery_filter_categories li:hover:not(.el_masonry_gallery_active_category):not(.el_masonry_gallery_disabled)",
	                'active_category_bg' => "{$this->main_css_element} .el_masonry_gallery_filter_categories li.el_masonry_gallery_active_category:hover:not(.el_masonry_gallery_disabled)",
	            ),
	        );
	        
	        $this->process_custom_background( $render_slug, $options );

			return et_core_intentionally_unescaped( $output, 'html' );
		}

		return '';

	}

	public function el_builder_processed_range_value( $result, $range, $range_string ) {
		if ( false !== strpos( $result, '0calc' ) ) {
			return $range;
		}
		return $result;
	}

	public function process_custom_background( $function_name, $options ) {

        $normal_fields = $options['normal'];
        
        foreach ( $normal_fields as $option_name => $element ) {
            
            $css_element           = $element;
            $css_element_processed = $element;

            if ( is_array( $element ) ) {
                $css_element_processed = implode( ', ', $element );
            }
            
            // Place to store processed background. It will be compared with the smaller device
            // background processed value to avoid rendering the same styles.
            $processed_background_color  = '';
            $processed_background_image  = '';
            $processed_background_blend  = '';
    
            // Store background images status because the process is extensive.
            $background_image_status = array(
                'desktop' => false,
                'tablet'  => false,
                'phone'   => false,
            );

            // Background Options Styling.
            foreach ( et_pb_responsive_options()->get_modes() as $device ) {
                $background_base_name = $option_name;
                $background_prefix    = "{$option_name}_";
                $background_style     = '';
                $is_desktop           = 'desktop' === $device;
                $suffix               = ! $is_desktop ? "_{$device}" : '';
    
                $background_color_style = '';
                $background_image_style = '';
                $background_images      = array();
    
                $has_background_color_gradient         = false;
                $has_background_image                  = false;
                $has_background_gradient_and_image     = false;
                $is_background_color_gradient_disabled = false;
                $is_background_image_disabled          = false;
    
                $background_color_gradient_overlays_image = 'off';
    
                // Ensure responsive is active.
                if ( ! $is_desktop && ! et_pb_responsive_options()->is_responsive_enabled( $this->props, "{$option_name}_color" ) ) {
                    continue;
                }

                // A. Background Gradient.
                $use_background_color_gradient = et_pb_responsive_options()->get_inheritance_background_value( $this->props, "{$background_prefix}use_color_gradient", $device, $background_base_name, $this->fields_unprocessed );
    
                if ( 'on' === $use_background_color_gradient ) {
                    $background_color_gradient_overlays_image = et_pb_responsive_options()->get_any_value( $this->props, "{$background_prefix}color_gradient_overlays_image{$suffix}", '', true );
    
                    $gradient_properties = array(
                        'type'             => et_pb_responsive_options()->get_any_value( $this->props, "{$background_prefix}color_gradient_type{$suffix}", '', true ),
                        'direction'        => et_pb_responsive_options()->get_any_value( $this->props, "{$background_prefix}color_gradient_direction{$suffix}", '', true ),
                        'radial_direction' => et_pb_responsive_options()->get_any_value( $this->props, "{$background_prefix}color_gradient_direction_radial{$suffix}", '', true ),
                        'color_start'      => et_pb_responsive_options()->get_any_value( $this->props, "{$background_prefix}color_gradient_start{$suffix}", '', true ),
                        'color_end'        => et_pb_responsive_options()->get_any_value( $this->props, "{$background_prefix}color_gradient_end{$suffix}", '', true ),
                        'start_position'   => et_pb_responsive_options()->get_any_value( $this->props, "{$background_prefix}color_gradient_start_position{$suffix}", '', true ),
                        'end_position'     => et_pb_responsive_options()->get_any_value( $this->props, "{$background_prefix}color_gradient_end_position{$suffix}", '', true ),
                    );
    
                    // Save background gradient into background images list.
                    $background_images[] = $this->get_gradient( $gradient_properties );
    
                    // Flag to inform BG Color if current module has Gradient.
                    $has_background_color_gradient = true;
                } else if ( 'off' === $use_background_color_gradient ) {
                    $is_background_color_gradient_disabled = true;
                }
    
                // B. Background Image.
                $background_image = et_pb_responsive_options()->get_inheritance_background_value( $this->props, "{$background_prefix}image", $device, $background_base_name, $this->fields_unprocessed );
                $parallax         = et_pb_responsive_options()->get_any_value( $this->props, "{$background_prefix}parallax{$suffix}", 'off' );
    
                // BG image and parallax status.
                $is_background_image_active         = '' !== $background_image && 'on' !== $parallax;
                $background_image_status[ $device ] = $is_background_image_active;
    
                if ( $is_background_image_active ) {
                    // Flag to inform BG Color if current module has Image.
                    $has_background_image = true;
    
                    // Check previous BG image status. Needed to get the correct value.
                    $is_prev_background_image_active = true;
                    if ( ! $is_desktop ) {
                        $is_prev_background_image_active = 'tablet' === $device ? $background_image_status['desktop'] : $background_image_status['tablet'];
                    }
    
                    // Size.
                    $background_size_default = ET_Builder_Element::$_->array_get( $this->fields_unprocessed, "{$background_prefix}size.default", '' );
                    $background_size         = et_pb_responsive_options()->get_any_value( $this->props, "{$background_prefix}size{$suffix}", $background_size_default, ! $is_prev_background_image_active );
            
                    if ( '' !== $background_size ) {
                        $background_style .= sprintf(
                            'background-size: %1$s; ',
                            esc_html( $background_size )
                        );
                    }
    
                    // Position.
                    $background_position_default = ET_Builder_Element::$_->array_get( $this->fields_unprocessed, "{$background_prefix}position.default", '' );
                    $background_position         = et_pb_responsive_options()->get_any_value( $this->props, "{$background_prefix}position{$suffix}", $background_position_default, ! $is_prev_background_image_active );
    
                    if ( '' !== $background_position ) {
                        $background_style .= sprintf(
                            'background-position: %1$s; ',
                            esc_html( str_replace( '_', ' ', $background_position ) )
                        );
                    }
    
                    // Repeat.
                    $background_repeat_default = ET_Builder_Element::$_->array_get( $this->fields_unprocessed, "{$background_prefix}repeat.default", '' );
                    $background_repeat         = et_pb_responsive_options()->get_any_value( $this->props, "{$background_prefix}repeat{$suffix}", $background_repeat_default, ! $is_prev_background_image_active );
    
                    if ( '' !== $background_repeat ) {
                        $background_style .= sprintf(
                            'background-repeat: %1$s; ',
                            esc_html( $background_repeat )
                        );
                    }
    
                    // Blend.
                    $background_blend_default = ET_Builder_Element::$_->array_get( $this->fields_unprocessed, "{$background_prefix}blend.default", '' );
                    $background_blend         = et_pb_responsive_options()->get_any_value( $this->props, "{$background_prefix}blend{$suffix}", $background_blend_default, ! $is_prev_background_image_active );
                    $background_blend_inherit = et_pb_responsive_options()->get_any_value( $this->props, "{$background_prefix}blend{$suffix}", '', true );
    
                    if ( '' !== $background_blend_inherit ) {
                        // Don't print the same image blend style.
                        if ( '' !== $background_blend ) {
                            $background_style .= sprintf(
                                'background-blend-mode: %1$s; ',
                                esc_html( $background_blend )
                            );
                        }
    
                        // Reset - If background has image and gradient, force background-color: initial.
                        if ( $has_background_color_gradient && $has_background_image && $background_blend_inherit !== $background_blend_default ) {
                            $has_background_gradient_and_image = true;
                            $background_color_style            = 'initial';
                            $background_style                  .= 'background-color: initial; ';
                        }
    
                        $processed_background_blend = $background_blend;
                    }
    
                    // Only append background image when the image is exist.
                    $background_images[] = sprintf( 'url(%1$s)', esc_html( $background_image ) );
                } else if ( '' === $background_image ) {
                    // Reset - If background image is disabled, ensure we reset prev background blend mode.
                    if ( '' !== $processed_background_blend ) {
                        $background_style .= 'background-blend-mode: normal; ';
                        $processed_background_blend = '';
                    }
    
                    $is_background_image_disabled = true;
                }
    
                if ( ! empty( $background_images ) ) {
                    // The browsers stack the images in the opposite order to what you'd expect.
                    if ( 'on' !== $background_color_gradient_overlays_image ) {
                        $background_images = array_reverse( $background_images );
                    }
    
                    // Set background image styles only it's different compared to the larger device.
                    $background_image_style = join( ', ', $background_images );
                    if ( $processed_background_image !== $background_image_style ) {
                        $background_style .= sprintf(
                            'background-image: %1$s !important;',
                            esc_html( $background_image_style )
                        );
                    }
                } else if ( ! $is_desktop && $is_background_color_gradient_disabled && $is_background_image_disabled ) {
                    // Reset - If background image and gradient are disabled, reset current background image.
                    $background_image_style = 'initial';
                    $background_style .= 'background-image: initial !important;';
                }
    
                // Save processed background images.
                $processed_background_image = $background_image_style;
    
                // C. Background Color.
                if ( ! $has_background_gradient_and_image ) {
                    // Background color `initial` was added by default to reset button background
                    // color when user disable it on mobile preview mode. However, it should
                    // be applied only when the background color is really disabled because user
                    // may use theme customizer to setup global button background color. We also
                    // need to ensure user still able to disable background color on mobile.
                    $background_color_enable  = ET_Builder_Element::$_->array_get( $this->props, "{$background_prefix}enable_color{$suffix}", '' );
                    $background_color_initial = 'off' === $background_color_enable && ! $is_desktop ? 'initial' : '';
    
                    $background_color       = et_pb_responsive_options()->get_inheritance_background_value( $this->props, "{$background_prefix}color", $device, $background_base_name, $this->fields_unprocessed );
                    $background_color       = '' !== $background_color ? $background_color : $background_color_initial;
                    $background_color_style = $background_color;
                    
                    if ( '' !== $background_color && $processed_background_color !== $background_color ) {
                        $background_style .= sprintf(
                            'background-color: %1$s; ',
                            esc_html( $background_color )
                        );
                    }
                }
    
                // Save processed background color.
                $processed_background_color = $background_color_style;
    
                // Print background gradient and image styles.
                if ( '' !== $background_style ) {
                    $background_style_attrs = array(
                        'selector'    => $css_element_processed,
                        'declaration' => rtrim( $background_style ),
                        'priority'    => $this->_style_priority,
                    );
    
                    // Add media query attribute to background style attrs.
                    if ( 'desktop' !== $device ) {
                        $current_media_query = 'tablet' === $device ? 'max_width_980' : 'max_width_767';
                        $background_style_attrs['media_query'] = ET_Builder_Element::get_media_query( $current_media_query );
                    }
    
                    ET_Builder_Element::set_style( $function_name, $background_style_attrs );
                }
            }
            
        }

        if ( isset( $options['hover'] ) ) {
            $hover_fields = $options['hover'];
        } else {
            $hover_fields = $options['normal'];
            foreach ( $hover_fields as &$value ) {
                $value = $value . ':hover';
            }
        }

        foreach ( $hover_fields as $option_name => $element ) {

            $css_element           = $element;
            $css_element_processed = $element;
            
            if ( is_array( $element ) ) {
                $css_element_processed = implode( ', ', $element );
            }

            // Background Hover.
            if ( et_builder_is_hover_enabled( "{$option_name}_color", $this->props ) ) {

                $background_base_name       = $option_name;
                $background_prefix          = "{$option_name}_";
                $background_images_hover    = array();
                $background_hover_style     = '';

                $has_background_color_gradient_hover         = false;
                $has_background_image_hover                  = false;
                $has_background_gradient_and_image_hover     = false;
                $is_background_color_gradient_hover_disabled = false;
                $is_background_image_hover_disabled          = false;

                $background_color_gradient_overlays_image_desktop = et_pb_responsive_options()->get_any_value( $this->props, "{$background_prefix}color_gradient_overlays_image", 'off', true );
    
                $gradient_properties_desktop = array(
                    'type'             => et_pb_responsive_options()->get_any_value( $this->props, "{$background_prefix}color_gradient_type", '', true ),
                    'direction'        => et_pb_responsive_options()->get_any_value( $this->props, "{$background_prefix}color_gradient_direction", '', true ),
                    'radial_direction' => et_pb_responsive_options()->get_any_value( $this->props, "{$background_prefix}color_gradient_direction_radial", '', true ),
                    'color_start'      => et_pb_responsive_options()->get_any_value( $this->props, "{$background_prefix}color_gradient_start", '', true ),
                    'color_end'        => et_pb_responsive_options()->get_any_value( $this->props, "{$background_prefix}color_gradient_end", '', true ),
                    'start_position'   => et_pb_responsive_options()->get_any_value( $this->props, "{$background_prefix}color_gradient_start_position", '', true ),
                    'end_position'     => et_pb_responsive_options()->get_any_value( $this->props, "{$background_prefix}color_gradient_end_position", '', true ),
                );

                $background_color_gradient_overlays_image_hover = 'off';

                // Background Gradient Hover.
                // This part is little bit different compared to other hover implementation. In
                // this case, hover is enabled on the background field, not on the each of those
                // fields. So, built in function get_value() doesn't work in this case.
                // Temporarily, we need to fetch the the value from get_raw_value().
                $use_background_color_gradient_hover = et_pb_responsive_options()->get_inheritance_background_value( $this->props, "{$background_prefix}use_color_gradient", 'hover', $background_base_name, $this->fields_unprocessed );

                if ( 'on' === $use_background_color_gradient_hover ) {
                    // Desktop value as default.
                    $background_color_gradient_type_desktop             = ET_Builder_Element::$_->array_get( $gradient_properties_desktop, 'type', '' );
                    $background_color_gradient_direction_desktop        = ET_Builder_Element::$_->array_get( $gradient_properties_desktop, 'direction', '' );
                    $background_color_gradient_radial_direction_desktop = ET_Builder_Element::$_->array_get( $gradient_properties_desktop, 'radial_direction', '' );
                    $background_color_gradient_color_start_desktop      = ET_Builder_Element::$_->array_get( $gradient_properties_desktop, 'color_start', '' );
                    $background_color_gradient_color_end_desktop        = ET_Builder_Element::$_->array_get( $gradient_properties_desktop, 'color_end', '' );
                    $background_color_gradient_start_position_desktop   = ET_Builder_Element::$_->array_get( $gradient_properties_desktop, 'start_position', '' );
                    $background_color_gradient_end_position_desktop     = ET_Builder_Element::$_->array_get( $gradient_properties_desktop, 'end_position', '' );

                    // Hover value.
                    $background_color_gradient_type_hover             = et_pb_hover_options()->get_raw_value( "{$background_prefix}color_gradient_type", $this->props, $background_color_gradient_type_desktop );
                    $background_color_gradient_direction_hover        = et_pb_hover_options()->get_raw_value( "{$background_prefix}color_gradient_direction", $this->props, $background_color_gradient_direction_desktop );
                    $background_color_gradient_direction_radial_hover = et_pb_hover_options()->get_raw_value( "{$background_prefix}color_gradient_direction_radial", $this->props, $background_color_gradient_radial_direction_desktop );
                    $background_color_gradient_start_hover            = et_pb_hover_options()->get_raw_value( "{$background_prefix}color_gradient_start", $this->props, $background_color_gradient_color_start_desktop );
                    $background_color_gradient_end_hover              = et_pb_hover_options()->get_raw_value( "{$background_prefix}color_gradient_end", $this->props, $background_color_gradient_color_end_desktop );
                    $background_color_gradient_start_position_hover   = et_pb_hover_options()->get_raw_value( "{$background_prefix}color_gradient_start_position", $this->props, $background_color_gradient_start_position_desktop );
                    $background_color_gradient_end_position_hover     = et_pb_hover_options()->get_raw_value( "{$background_prefix}color_gradient_end_position", $this->props, $background_color_gradient_end_position_desktop );
                    $background_color_gradient_overlays_image_hover   = et_pb_hover_options()->get_raw_value( "{$background_prefix}color_gradient_overlays_image", $this->props, $background_color_gradient_overlays_image_desktop );

                    $has_background_color_gradient_hover = true;

                    $gradient_values_hover = array(
                        'type'             => '' !== $background_color_gradient_type_hover ? $background_color_gradient_type_hover : $background_color_gradient_type_desktop,
                        'direction'        => '' !== $background_color_gradient_direction_hover ? $background_color_gradient_direction_hover : $background_color_gradient_direction_desktop,
                        'radial_direction' => '' !== $background_color_gradient_direction_radial_hover ? $background_color_gradient_direction_radial_hover : $background_color_gradient_radial_direction_desktop,
                        'color_start'      => '' !== $background_color_gradient_start_hover ? $background_color_gradient_start_hover : $background_color_gradient_color_start_desktop,
                        'color_end'        => '' !== $background_color_gradient_end_hover ? $background_color_gradient_end_hover : $background_color_gradient_color_end_desktop,
                        'start_position'   => '' !== $background_color_gradient_start_position_hover ? $background_color_gradient_start_position_hover : $background_color_gradient_start_position_desktop,
                        'end_position'     => '' !== $background_color_gradient_end_position_hover ? $background_color_gradient_end_position_hover : $background_color_gradient_end_position_desktop,
                    );

                    $background_images_hover[] = $this->get_gradient( $gradient_values_hover );
                } else if ( 'off' === $use_background_color_gradient_hover ) {
                    $is_background_color_gradient_hover_disabled = true;
                }

                // Background Image Hover.
                // This part is little bit different compared to other hover implementation. In
                // this case, hover is enabled on the background field, not on the each of those
                // fields. So, built in function get_value() doesn't work in this case.
                // Temporarily, we need to fetch the the value from get_raw_value().
                $background_image_hover = et_pb_responsive_options()->get_inheritance_background_value( $this->props, "{$background_prefix}image", 'hover', $background_base_name, $this->fields_unprocessed );
                $parallax_hover         = et_pb_hover_options()->get_raw_value( "{$background_prefix}parallax", $this->props );

                if ( '' !== $background_image_hover && null !== $background_image_hover && 'on' !== $parallax_hover ) {
                    // Flag to inform BG Color if current module has Image.
                    $has_background_image_hover = true;

                    // Size.
                    $background_size_hover   = et_pb_hover_options()->get_raw_value( "{$background_prefix}size", $this->props );
                    $background_size_desktop = ET_Builder_Element::$_->array_get( $this->props, "{$background_prefix}size", '' );
                    $is_same_background_size = $background_size_hover === $background_size_desktop;
                    if ( empty( $background_size_hover ) && ! empty( $background_size_desktop ) ) {
                        $background_size_hover = $background_size_desktop;
                    }

                    if ( ! empty( $background_size_hover ) && ! $is_same_background_size ) {
                        $background_hover_style .= sprintf(
                            'background-size: %1$s; ',
                            esc_html( $background_size_hover )
                        );
                    }

                    // Position.
                    $background_position_hover   = et_pb_hover_options()->get_raw_value( "{$background_prefix}position", $this->props );
                    $background_position_desktop = ET_Builder_Element::$_->array_get( $this->props, "{$background_prefix}position", '' );
                    $is_same_background_position = $background_position_hover === $background_position_desktop;
                    if ( empty( $background_position_hover ) && ! empty( $background_position_desktop ) ) {
                        $background_position_hover = $background_position_desktop;
                    }

                    if ( ! empty( $background_position_hover ) && ! $is_same_background_position ) {
                        $background_hover_style .= sprintf(
                            'background-position: %1$s; ',
                            esc_html( str_replace( '_', ' ', $background_position_hover ) )
                        );
                    }

                    // Repeat.
                    $background_repeat_hover   = et_pb_hover_options()->get_raw_value( "{$background_prefix}repeat", $this->props );
                    $background_repeat_desktop = ET_Builder_Element::$_->array_get( $this->props, "{$background_prefix}repeat", '' );
                    $is_same_background_repeat = $background_repeat_hover === $background_repeat_desktop;
                    if ( empty( $background_repeat_hover ) && ! empty( $background_repeat_desktop ) ) {
                        $background_repeat_hover = $background_repeat_desktop;
                    }

                    if ( ! empty( $background_repeat_hover ) && ! $is_same_background_repeat ) {
                        $background_hover_style .= sprintf(
                            'background-repeat: %1$s; ',
                            esc_html( $background_repeat_hover )
                        );
                    }

                    // Blend.
                    $background_blend_hover = et_pb_hover_options()->get_raw_value( "{$background_prefix}blend", $this->props );
                    $background_blend_default = ET_Builder_Element::$_->array_get( $this->fields_unprocessed, "{$background_prefix}blend.default", '' );
                    $background_blend_desktop = ET_Builder_Element::$_->array_get( $this->props, "{$background_prefix}blend", '' );
                    $is_same_background_blend = $background_blend_hover === $background_blend_desktop;
                    if ( empty( $background_blend_hover ) && ! empty( $background_blend_desktop ) ) {
                        $background_blend_hover = $background_blend_desktop;
                    }

                    if ( ! empty( $background_blend_hover ) ) {
                        if ( ! $is_same_background_blend ) {
                            $background_hover_style .= sprintf(
                                'background-blend-mode: %1$s; ',
                                esc_html( $background_blend_hover )
                            );
                        }

                        // Force background-color: initial;
                        if ( $has_background_color_gradient_hover && $has_background_image_hover && $background_blend_hover !== $background_blend_default ) {
                            $has_background_gradient_and_image_hover = true;
                            $background_hover_style .= 'background-color: initial !important;';
                        }
                    }

                    // Only append background image when the image exists.
                    $background_images_hover[] = sprintf( 'url(%1$s)', esc_html( $background_image_hover ) );
                } else if ( '' === $background_image_hover ) {
                    $is_background_image_hover_disabled = true;
                }

                if ( ! empty( $background_images_hover ) ) {
                    // The browsers stack the images in the opposite order to what you'd expect.
                    if ( 'on' !== $background_color_gradient_overlays_image_hover ) {
                        $background_images_hover = array_reverse( $background_images_hover );
                    }

                    $background_hover_style .= sprintf(
                        'background-image: %1$s !important;',
                        esc_html( join( ', ', $background_images_hover ) )
                    );
                } else if ( $is_background_color_gradient_hover_disabled && $is_background_image_hover_disabled ) {
                    $background_hover_style .= 'background-image: initial !important;';
                }

                // Background Color Hover.
                if ( ! $has_background_gradient_and_image_hover ) {
                    $background_color_hover = et_pb_responsive_options()->get_inheritance_background_value( $this->props, "{$background_prefix}color", 'hover', $background_base_name, $this->fields_unprocessed );
                    $background_color_hover = '' !== $background_color_hover ? $background_color_hover : 'transparent';

                    if ( '' !== $background_color_hover ) {
                        $background_hover_style .= sprintf(
                            'background-color: %1$s !important; ',
                            esc_html( $background_color_hover )
                        );
                    }
                }

                // Print background hover gradient and image styles.
                if ( '' !== $background_hover_style ) {
                    $background_hover_style_attrs = array(
                        'selector'    => $css_element_processed,
                        'declaration' => rtrim( $background_hover_style ),
                        'priority'    => $this->_style_priority,
                    );

                    ET_Builder_Element::set_style( $function_name, $background_hover_style_attrs );
                }
            }
        }
    }

}
new EL_DynamicMasonryGallery();