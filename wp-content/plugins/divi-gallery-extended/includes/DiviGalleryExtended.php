<?php

class EL_DiviGalleryExtended extends DiviExtension {

	/**
	 * The gettext domain for the extension's translations.
	 *
	 * @since 1.0.0
	 *
	 * @var string
	 */
	public $gettext_domain = 'divi-gallery-extended';

	/**
	 * The extension's WP Plugin name.
	 *
	 * @since 1.0.0
	 *
	 * @var string
	 */
	public $name = 'divi-gallery-extended';

	/**
	 * The extension's version
	 *
	 * @since 1.0.0
	 *
	 * @var string
	 */
	public $version = ELICUS_DIVI_GALLERY_EXTENDED_VERSION;

	/**
	 * DGE_DiviGalleryExtended constructor.
	 *
	 * @param string $name
	 * @param array  $args
	 */
	public function __construct( $name = 'divi-gallery-extended', $args = array() ) {
		$this->plugin_dir     = plugin_dir_path( __FILE__ );
		$this->plugin_dir_url = plugin_dir_url( $this->plugin_dir );

		add_action( 'wp_enqueue_scripts', array( $this, 'dge_register_scripts' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'dge_fb_enqueue_scripts' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'dge_admin_enqueue_scripts' ) );

		parent::__construct( $name, $args );

		add_filter( 'attachment_fields_to_edit', array( $this, 'dge_add_attachment_fields' ), 10, 2 );
		add_filter( 'attachment_fields_to_save', array( $this, 'dge_save_attachment_fields' ), 10, 2 );
		add_action( 'init', array( $this, 'dge_register_taxonomies' ) );
       
	}

	public function dge_register_scripts() {
        wp_register_script( 'elicus-isotope-script', "{$this->plugin_dir_url}scripts/isotope.pkgd.min.js", array('jquery'), '3.0.6', true );
        wp_register_script( 'elicus-images-loaded-script', "{$this->plugin_dir_url}scripts/imagesloaded.pkgd.min.js", array('jquery'), '4.1.4', true );
    }

    public function dge_fb_enqueue_scripts() {
		if ( et_core_is_fb_enabled() ) {
			wp_enqueue_script( 'elicus-isotope-script' );
			wp_enqueue_script( 'elicus-images-loaded-script' );
		}
	}

	public function dge_admin_enqueue_scripts() {
		wp_enqueue_style( 'dge-admin-style', "{$this->plugin_dir_url}styles/admin.min.css", array(), $this->version, false );
		wp_enqueue_script( 'dge-admin-script', "{$this->plugin_dir_url}scripts/admin.min.js", array('jquery'), $this->version, false );
	}

	public function dge_add_attachment_fields( $form_fields, $post ) {
		if ( preg_match( "/image/", $post->post_mime_type ) ) {
			$attachment_categories 	= wp_get_object_terms( $post->ID, 'attachment-category', array( 'fields' => 'ids' ) );
			$attachment_categories 	= sanitize_text_field( implode( ',', $attachment_categories ) );
			$categories_checklist	= wp_terms_checklist(
				$post->ID,
				array(
					'taxonomy' => 'attachment-category',
					'echo' => false,
				)
			);
			if ( $categories_checklist ) {
				$metafield = sprintf(
					'<div class="dge_attachment_metafield">
						<input type="hidden" class="dge_attachment_categories" name="attachments[%1$s][dge_attachment_categories]" value="%2$s" />
						<ul id="attachment-categorychecklist" data-wp-lists="list:attachment-category" class="categorychecklist form-no-clear">%3$s</ul>
					</div>',
					$post->ID,
					$attachment_categories,
					$categories_checklist
				);
			} else {
				$metafield = sprintf(
					'<div class="dge_attachment_metafield"><a href="%1$s">Create category</a></div>',
					esc_url( admin_url( 'edit-tags.php?taxonomy=attachment-category&post_type=attachment' ) )
				);
			}

			$form_fields['dge_attachment_categories'] = array(
		        'label'	=> esc_html__( 'Attachment Category', 'divi-gallery-extended' ),
		        'input' => 'html',
		        'html'	=> $metafield,
		        'show_in_edit' => false,
		    );

		    $attachment_link = esc_url_raw( get_post_meta( $post->ID, 'dge_attachment_link', true ) );

		    $form_fields['dge_attachment_link'] = array(
		        'label'	=> esc_html__( 'Custom Link', 'divi-gallery-extended' ),
		        'input' => 'text',
		        'value' => $attachment_link,
		        'show_in_edit' => true,
		    );
		}

	    return $form_fields;
	}

	public function dge_save_attachment_fields( $post, $attachment ) {
	   	if ( isset( $attachment['dge_attachment_categories'] ) ) {
	   		if ( taxonomy_exists( 'attachment-category' ) ) {
	   			wp_set_object_terms( $post['ID'], array_map( 'absint', preg_split( '/,+/', $attachment['dge_attachment_categories'] ) ), 'attachment-category', false );
	   		}
        }

        if ( isset( $attachment['dge_attachment_link'] ) ) {
        	update_post_meta( $post['ID'], 'dge_attachment_link', esc_url_raw( $attachment['dge_attachment_link'] ) );
        }

	    return $post;
	}

	public function dge_register_taxonomies() {

        $labels = array(
            'name'                       => esc_html_x( 'Attachment Categories', 'Taxonomy General Name', 'divi-gallery-extended' ),
            'singular_name'              => esc_html_x( 'Attachment Category', 'Taxonomy Singular Name', 'divi-gallery-extended' ),
            'menu_name'                  => esc_html__( 'Attachment Categories', 'divi-gallery-extended' ),
            'all_items'                  => esc_html__( 'All Attachment Categories', 'divi-gallery-extended' ),
            'parent_item'                => esc_html__( 'Parent Attachment Category', 'divi-gallery-extended' ),
            'parent_item_colon'          => esc_html__( 'Parent Attachment Category:', 'divi-gallery-extended' ),
            'new_item_name'              => esc_html__( 'New Attachment Category Name', 'divi-gallery-extended' ),
            'add_new_item'               => esc_html__( 'Add New Attachment Category', 'divi-gallery-extended' ),
            'edit_item'                  => esc_html__( 'Edit Attachment Category', 'divi-gallery-extended' ),
            'update_item'                => esc_html__( 'Update Attachment Category', 'divi-gallery-extended' ),
            'view_item'                  => esc_html__( 'View Attachment Category', 'divi-gallery-extended' ),
            'separate_items_with_commas' => esc_html__( 'Separate categories with commas', 'divi-gallery-extended' ),
            'add_or_remove_items'        => esc_html__( 'Add or remove categories', 'divi-gallery-extended' ),
            'choose_from_most_used'      => esc_html__( 'Choose from the most used', 'divi-gallery-extended' ),
            'popular_items'              => esc_html__( 'Popular Attachment Categories', 'divi-gallery-extended' ),
            'search_items'               => esc_html__( 'Search Attachment Categories', 'divi-gallery-extended' ),
            'not_found'                  => esc_html__( 'Not Found', 'divi-gallery-extended' ),
            'no_terms'                   => esc_html__( 'No Attachment Categories', 'divi-gallery-extended' ),
            'items_list'                 => esc_html__( 'Attachment Categories list', 'divi-gallery-extended' ),
            'items_list_navigation'      => esc_html__( 'Attachment Categories list navigation', 'divi-gallery-extended' ),
        );
        $args = array(
            'labels'                     => $labels,
            'hierarchical'               => true,
            'public'                     => false,
            'show_ui'                    => true,
            'show_admin_column'          => true,
            'show_in_nav_menus'          => true,
            'show_tagcloud'              => true,
        );

        register_taxonomy( 'attachment-category', array( 'attachment' ), $args );
    }
}

new EL_DiviGalleryExtended;
