<?php
/*
Plugin Name: Divi Gallery Extended
Plugin URI:  https://diviextended.com/product/divi-gallery-extended/
Description: Create stunning masonry galleries on your website easily.
Version:     1.2.0
Author:      Elicus
Author URI:  https://elicus.com
License:     GPL2
License URI: https://www.gnu.org/licenses/gpl-2.0.html
Text Domain: divi-gallery-extended
Domain Path: /languages
*/
defined( 'ABSPATH' ) || die( 'No script kiddies please!' );

define( 'ELICUS_DIVI_GALLERY_EXTENDED_VERSION', '1.2.0' );
define( 'ELICUS_DIVI_GALLERY_EXTENDED_OPTION', 'el-divi-gallery-extended' );
define( 'ELICUS_DIVI_GALLERY_EXTENDED_BASENAME', plugin_basename( __FILE__ ) );
define( 'ELICUS_DIVI_GALLERY_EXTENDED_PATH', plugin_dir_url( __FILE__ ) );

if ( ! function_exists( 'el_dge_initialize_extension' ) ) {
	/**
	 * Creates the extension's main class instance.
	 *
	 * @since 1.0.0
	 */
	function el_dge_initialize_extension() {
		require_once plugin_dir_path( __FILE__ ) . 'includes/DiviGalleryExtended.php';
	}
	add_action( 'divi_extensions_init', 'el_dge_initialize_extension' );
}