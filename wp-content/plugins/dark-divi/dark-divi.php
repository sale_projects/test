<?php
/*
Plugin Name: Dark Divi
Plugin URI: https://www.divi-community.fr
Description: Enable dark mode for the Divi Visual Builder.
Version: 1.1.3
Author: Pierre Bichet
Author URI: https://twitter.com/PixelsnDecibels
Icon1x: https://www.divi-community.fr/wp-content/uploads/2018/10/dark-divi-icon-128x128.png
Icon2x: https://www.divi-community.fr/wp-content/uploads/2018/10/dark-divi-icon-256x256.png
BannerHigh: https://www.divi-community.fr/wp-content/uploads/2018/10/dark-divi-banner-1544x500.png
BannerLow: https://www.divi-community.fr/wp-content/uploads/2018/10/dark-divi-banner-772x250.png
*/


// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

// Load styles in front
function pb_dd_load_dark_divi(){
	if( is_user_logged_in() ){
		wp_enqueue_style( 'dark-divi', plugins_url( 'assets/dark-divi.css', __FILE__ ) );
	}
}
add_action( 'wp_enqueue_scripts', 'pb_dd_load_dark_divi', 9999 );

// Load styles in admin
function pb_dd_admin_style() {
	wp_enqueue_style('dark-divi-admin', plugins_url( 'assets/dark-divi.css', __FILE__ ) );
}
add_action('admin_enqueue_scripts', 'pb_dd_admin_style', 9999);


// Updater
require_once plugin_dir_path( __FILE__ ) . 'lib/wp-package-updater/class-wp-package-updater.php';

$darkdivi_updater = new WP_Package_Updater(
	'https://divi-community.fr',
	wp_normalize_path( __FILE__ ),
	wp_normalize_path( plugin_dir_path( __FILE__ ) )
);