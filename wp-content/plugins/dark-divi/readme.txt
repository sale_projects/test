=== Dark Divi ===
Contributors: Pierre Bichet
Donate link: 
Tags: 
Requires at least: 4.9.8
Tested up to: 5.4.2
Requires PHP: 7.0
License: 
License URI: 

EN: Enable dark mode for the Divi Visual Builder.
FR : Active un mode "sombre" pour le Divi Visual Builder.


== Description ==

Enable dark mode for the Divi Visual Builder.

== Changelog ==

= 1.1.3 =
* Added: styles adjustments for Global Presets / Ajustements des styles pour les Global Presets
= 1.1.2 =
* Dark Divi was not loading if the site was using a template created with the Theme Builder in Divi 4.2.x / Dark Divi ne se chargeait pas si le site utilisait un template créé avec le Theme Builder dans Divi 4.2.x
* Fixed: font upload modal background is dark again / Le fond de la modale d'upload de police est de nouveau sombre
= 1.1.1 =
* Style fixes for Divi 4.x (mainly for modal backgrounds) / Corrections de styles pour Divi 4.x (surtout pour les fonds des modales)
= 1.1 =
* Compatibility with Divi 4 / Compatibilité avec Divi 4
* Dark Divi is now avalaible for modals of the New Divi Builder Experience and the Theme Builder / Dark Divi est désormais disponible pour les modales du nouveau Divi Builder en backend et pour le Theme Builder 
* Style adjusments for Firefox / Ajustements de styles pour Firefox
= 1.0.5 =
* Removed unwanted background on range sliders in Firefox / Suppression du fond inutile pour les curseurs de réglage dans Firefox
* Tested up to Divi 3.29.3 / Testé jusqu'à Divi 3.29.3
= 1.0.4 =
* Added: styles adjustments for Global Defaults Editor and history modal / Ajustements des styles pour le gestionnaire global de réglages par défaut et la modale d'historique
* Tested up to Divi 3.27.4 / Testé jusqu'à Divi 3.27.4
= 1.0.3 =
* Fixed: some background image previews wasn't displayed / Certains aperçus d'images de fond ne s'affichaient pas
* Fixed: missing styles for keyboard shortcuts text color / Les textes des raccourcis clavier n'étaient pas bien stylés 
* Added: styles for "Quick Actions" modal / Styles pour la modale des "Quick Actions"
= 1.0.2 =
* Compatibility with Divi 3.18 / Compatible avec Divi 3.18
= 1.0.1 =
* Added: missing styles to text settings / Ajout de styles manquants dans les paramètres de texte
* Added: styles for modal resize button and scrollbar / Ajout de styles pour le bouton de redimensionnement de la modale et la scrollbar
= 1.0.0 =
* Initial release / Version de base