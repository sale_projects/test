// See ../license.txt for copyright and licensing information applicable to Divi Ghoster
// This file was modified by Jonathan Hall and/or others; last modified 2020-12-31

( function( $ ) {
	wp.customize.bind( 'ready', function() {
		wp.customize.section( 'ghoster_custom_login', function( section ) {
			section.expanded.bind( function( isExpanding ) {
				if ( isExpanding ) {
					var current_url = wp.customize.previewer.previewUrl();
					current_url = current_url.includes( agsdg_login_customizer.page_url );
					if ( ! current_url ) {
						wp.customize.previewer.previewUrl.set( agsdg_login_customizer.page_url );
					}
				} else {
						wp.customize.previewer.previewUrl.set(wp.customize.settings.url.home)
				}
			} );
		} );
	} );
})( jQuery );
