// See ../license.txt for copyright and licensing information applicable to Divi Ghoster
// This file was modified by Jonathan Hall and/or others; last modified 2020-12-31

/*
 *     @since 3.0.1
 *		Create shortcut to open ghoster
 *
 */

function getGUrl() {
    var href = window.location.href;
    var index = href.indexOf('/wp-admin');
    var homeUrl = href.substring(0, index);
    var gUrk = homeUrl+"/wp-admin/admin.php?page=divi_ghoster";
    return gUrk;
}

jQuery(document).keydown(function(ev) {

    // f7
    if(ev.which === 118) {
        // your code
        window.location.replace( getGUrl()  );

        ev.preventDefault();
    }
});

