// See ../license.txt for copyright and licensing information applicable to Divi Ghoster

// Upload image button, color fields
jQuery(document).ready(function ($) {
    jQuery('.my-color-field').wpColorPicker({
        palettes: true,
        defaultColor: true,
        hide: true,
        target: true,
    });
});

jQuery(document).ready(function ($) {
	
	$('.ghoster-img-button-set').each(function() {
	var $button = $(this);
    var mediaUploader;
    $button.click(function (e) {
        e.preventDefault(); // If the uploader object has already been created, reopen the dialog
		
        if (mediaUploader) {
            mediaUploader.open();
            return;
        } // Extend the wp.media object
        mediaUploader = wp.media.frames.file_frame = wp.media({
			/*
            title: 'Choose Image',
            button: {
                text: 'Choose Image'
            },
			*/
            multiple: false
        }); // When a file is selected, grab the URL and set it as the text field's value and preview
        mediaUploader.on('select', function () {
            attachment = mediaUploader.state().get('selection').first().toJSON();
			console.log($button);
			$button.siblings('input:first').val(attachment.url).change();
			/*
            $('#custom-theme-image-preview').attr('src', attachment.url).fadeIn();
			*/
        }); // Open the uploader dialog
        mediaUploader.open();

    });
	});
	
});

(function ($) {
    $(window).on('hashchange', function (e) {
        if (location.hash === '#import-export') {
            $("#epanel-save-top").hide();
        } else {
            $("#epanel-save-top").show();
        }
    });
    if (location.hash !== '#import-export') {
        $("#epanel-save-top").show();
    } else {
        $("#epanel-save-top").hide();
    }
})(jQuery);

(function ($) {
    let enable_login_page = $("#divi-ghoster-enable-login");
    let ultimate_ghoster = $("#ultimate-ghoster");
    let login_page_button = $("#login-page");
    let ultimate_list = $("#ultimate-list");
    $(login_page_button).hide();
    $(ultimate_list).hide();

    $(ultimate_ghoster).change(function () {
        if ($(this).is(':checked')) {
            $(ultimate_list).show('slow')
        } else {
            $(ultimate_list).hide('slow')
        }
    });
	
	var loginCustomizerEnabled = enable_login_page.is(':checked');
	$('#divi-ghoster-enable-login-instructions').toggle( !loginCustomizerEnabled );
	login_page_button.toggle( loginCustomizerEnabled );
	login_page_button.find('.button:first').toggle( loginCustomizerEnabled );
	
	$('#divi-ghoster-enable-login').change(function() {
		login_page_button.toggle( $('#divi-ghoster-enable-login').is(':checked') );
	});
	
    if ($(ultimate_ghoster).is(':checked')) {
        $(ultimate_list).show();
    }
    // Delete custom theme image
    $('#custom-theme-image-remove, #custom-child-theme-image-remove, #branding-image-remove').click(function() {
       $(this).siblings('input:first').val('').change();
    });
    $('#custom-child-theme-image-set-same').click(function() {
       $(this).siblings('input:first').val('same').change();
    });
	
})(jQuery);

// Save settings without reloading page
(function ($) {
    let custom_colors = $('#divi-ghoster-enable-custom-colors');
    $('#ds-ghoster-settings-form-save').submit(function () {
		
        if (location.hash === '#custom-theme-colors') {
            $(this).ajaxSubmit({
                beforeSend: function () {
                    $('#loader').show();
					$('#divi-ghoster-enable-login').hide();
                },
                success: function () {
                    success_result();
                },
				error: function() {
					$('#divi-ghoster-enable-login').show();
				},
                timeout: 300000
            });
            if ($(custom_colors).is(':checked')) {
                $(this).ajaxSubmit({
                    type: "POST",
                    data: {
                        'action': 'add_custom_builder_css',
                        'type': 'build_css',
                        'divi_ghoster_nonce': divi_ghoster.divi_ghoster_nonce
                    },
                    url: divi_ghoster.ajax_url,
                    beforeSend: function () {
                        $('#loader').show();
                    },
                    success: function () {
                        success_result();
                    },
                });
            }
        } else {
            $(this).ajaxSubmit({
                beforeSend: function () {
                    $('#loader').show();
					$('#divi-ghoster-enable-login').hide();
                },
                success: function () {
                    success_result();

                },
				error: function() {
					$('#divi-ghoster-enable-login').show();
				},
                timeout: 300000
            });
        }

        return false;
    });

    function success_result() {
        if (location.hash !== '#import-export') {
            $('#loader').hide();
            $('#save-result').html("<div id='save-message' class='success-modal'></div>");
            $('#save-message').append($("<p class='message'>").text(divi_ghoster.success_message)).show();
			setTimeout(function () {
				$('#save-message').hide();
			}, 3000);
        }
		
		var loginCustomizerEnabled = $('#divi-ghoster-enable-login').is(':checked');
		$('#divi-ghoster-enable-login-instructions').toggle( !loginCustomizerEnabled );
		$('#login-page').toggle( loginCustomizerEnabled );
		$('#login-page .button:first').toggle( loginCustomizerEnabled );
		$('#divi-ghoster-enable-login').show();
    }

    if (location.hash === '#branding' || location.hash === '') {
        const btn = document.querySelector("#ds-ghoster-settings-form-save");
        btn.addEventListener("submit", function () {
            this.disabled = true;
            const time = setInterval(() => {
                $('#ds-ghoster-settings-form-save').submit();
                $('#loader').hide();
                clearInterval(time);
                this.disabled = false;
            }, 1000);
        });
    }

})(jQuery);

// Settings Tabs
var ds_ghoster_tabs_navigate = function () {
    jQuery('#ds-ghoster-settings-tabs-content > div, #ds-ghoster-settings-tabs > li').removeClass('ds-ghoster-settings-active');
    jQuery('#ds-ghoster-settings-' + location.hash.substr(1)).addClass('ds-ghoster-settings-active');
    jQuery('#ds-ghoster-settings-tabs > li:has(a[href="' + location.hash + '"])').addClass('ds-ghoster-settings-active');
};

if (location.hash) {
    ds_ghoster_tabs_navigate();
}

jQuery(window).on('hashchange', ds_ghoster_tabs_navigate);

/*
 * Select All
 */

jQuery('#agsdg-settings-hide-divi-page').click(function (event) {
    if (this.checked) {
        // Iterate each checkbox
        jQuery('.ds-checkboxes-wrap-1').each(function () {
            this.checked = true;
        });
    } else {
        jQuery('.ds-checkboxes-wrap-1').each(function () {
            this.checked = false;
        });
    }
});

jQuery('#agsdg-settings-hide-wp-page').click(function (event) {
    if (this.checked) {
        // Iterate each checkbox
        jQuery('.ds-checkboxes-wrap-2').each(function () {
            this.checked = true;
        });
    } else {
        jQuery('.ds-checkboxes-wrap-2').each(function () {
            this.checked = false;
        });
    }
});
jQuery('#agsdg-settings-all-ultimate-ghoster').click(function (event) {
    if (this.checked) {
        // Iterate each checkbox
        jQuery('.ds-checkboxes-wrap-ultimate').each(function () {
            this.checked = true;
        });
    } else {
        jQuery('.ds-checkboxes-wrap-ultimate').each(function () {
            this.checked = false;
        });
    }
});