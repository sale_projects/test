// See ../license.txt for copyright and licensing information applicable to Divi Ghoster

/*
This file includes code copied from the Divi theme, copyright Elegant Themes, released under the GNU General Public License
(GPL) version 2 or later, used here under GPL version 3.
*/

/** Hide Premade Layouts tab **/
jQuery(document).ready(function($) {
	var loadStage = -1, $iframe, firstTab, boundToWindow;
	
	function onCreateElementWithId(id, callback) {
      var MO = window.MutationObserver ? window.MutationObserver : window.WebkitMutationObserver;
      if (MO) {
          (new MO(function(events) {
              jQuery.each(events, function(i, event) {
                  if (event.addedNodes && event.addedNodes.length) {
                      jQuery.each(event.addedNodes, function(i, node) {
                          if ( node.id === id ) {
                              callback(node);
                          }
                      });
                  }
              });
          })).observe(document.body, {childList: true, subtree: true});
      }
  }
	function onCreateElementWithClass(className, callback) {
      var MO = window.MutationObserver ? window.MutationObserver : window.WebkitMutationObserver;
      if (MO) {
          (new MO(function(events) {
              jQuery.each(events, function(i, event) {
                  if (event.addedNodes && event.addedNodes.length) {
                      jQuery.each(event.addedNodes, function(i, node) {
                          if ( node.className && node.className.split && node.className.split(' ').indexOf(className) !== -1 ) {
                              callback(node);
                          }
                      });
                  }
              });
          })).observe(document.body, {childList: true, subtree: true});
      }
  }

  
	function handleLibraryDialog(elem) {
		
		var $elem = $(elem),
			$premadeLayoutsTab = $elem.find('.et-fb-settings-options_tab_modules_all:first, .et-common-tabs-navigation__button[data-key="modules_all"]:first');

		if ( $premadeLayoutsTab.length ) {
			
			var $firstTab = $premadeLayoutsTab.next();
			var $firstTabChildren = $firstTab.children();
			if ($firstTabChildren.length) {
				firstTab = $firstTabChildren[0];
			} else if ($firstTab.length) {
				firstTab = $firstTab[0];
			} else {
				firstTab = null;
			}
			
			$iframe = $elem.find('iframe').css('visibility', 'hidden');
			
			$premadeLayoutsTab.remove();
			
			var isTwoStage = $elem.hasClass('et-tb-divi-library-root');
			
			if (loadStage < 0) {
				loadStage = 1;
				var appFrame = document.getElementById('et-fb-app-frame');
				if (!appFrame) {
					appFrame = document.getElementById('et-bfb-app-frame');
				}
				
				var bindTo = appFrame && appFrame.contentWindow ? appFrame.contentWindow : window;
				
				if (bindTo === window) {
					if (boundToWindow) {
						return;
					}
					boundToWindow = true;
				}
				
				$( bindTo ).on('message', function(ev) {
				  
				  if (ev.originalEvent) {
					ev = ev.originalEvent;
				  }
				  
				  if ( ev.origin && ev.origin.indexOf('elegantthemes.com') != -1 && ev.data && ev.data.value ) {
					  
					  console.log(loadStage);
					  
					  switch (loadStage) {
						  case 1:
							 if (firstTab && ev.data.value.name === 'authentication_complete') {
								++loadStage;
								firstTab.click();
							 } else {
								loadStage = 0;
								firstTab = null;
								$iframe = null;
							 }
							 break;
						  case 3:
							 if (firstTab && ev.data.value.name === 'current_page_changed') {
								firstTab.click();
								firstTab = null;
							 }
						  case 4:
						  case 2:
							if ($iframe && $iframe.length && ev.data.value.name === 'current_page_changed') {
								++loadStage;
							} else {
								loadStage = 0;
								firstTab = null;
								$iframe = null;
							}
							if (!isTwoStage) { // short-circuit
								break;
							}
						  case 5:
							loadStage = 0;
							if ($iframe && $iframe.length && ev.data.value.name === 'current_page_changed') {
								$iframe.css('visibility', 'visible');
								$iframe = null;
							}
					  }
				  }
			  });
				
			} else {
				loadStage = 1;
			}
			
		}
	  }
	  
  onCreateElementWithId('et-fb-settings-column', handleLibraryDialog);
  onCreateElementWithClass('et-tb-divi-library-root', handleLibraryDialog);
}); // end document ready