// See ../license.txt for copyright and licensing information applicable to Divi Ghoster
// This file was modified by Jonathan Hall and/or others; last modified 2020-12-31

( function( $ ) {
	wp.customize.bind( 'preview-ready', function() {
		wp.customize.preview.bind( 'agsdg-open-preview', function( data ) {
			if ( true === data.expanded ) {
				wp.customize.preview.send( 'url', data.pageUrl );
			}
		} );

		wp.customize.preview.bind( 'agsdg-back-to-home', function( data ) {
			wp.customize.preview.send( 'url', data.home_url );
		} );

	} );
})( jQuery );
