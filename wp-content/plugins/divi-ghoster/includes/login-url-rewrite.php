<?php
/**
 * Class LoginUrlRewrite
 *
 * Manages custom login url
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( 'You are not allowed to be here!' );
}

class LoginUrlRewrite extends DiviGhosterRewrite {
	
	public function setup() {
		$this->login_url_slug = ( isset( DiviGhoster::$settings['login_url'] ) ? DiviGhoster::$settings['login_url'] : '' );
		
		if ( '' !== $this->login_url_slug ) {
			$this->actions();
		}
	}
	
	/**
	 * Custom login redirect actions
	 *
	 * @return void
	 */
	public function actions() {
		add_action( 'plugins_loaded', array( $this, 'handle_pagenow_to_custom_login_redirect' ), 999 );
		add_action( 'login_init', array( $this, 'handle_custom_logout' ) );
		add_action( 'template_redirect', array( $this, 'redirect_to_custom_login' ) );
		add_filter( 'login_url', array( $this, 'replace_wp_login_php_to_custom_url_slug' ), 2, 10 );
		add_filter( 'site_url', array( $this, 'replace_wp_login_php_to_custom_url_slug_in_url_query' ), 4, 10 );
		add_filter( 'wp_redirect', array( $this, 'replace_wp_login_php_after_logout' ) );
	}
	
	/**
	 * Check query string
	 *
	 * @param array $request
	 *
	 * @return bool
	 */
	private function check_query_string( $request ) {
		global $pagenow;
		return ( $pagenow === 'wp-login.php' && $request['path'] !== $this->handle_trailing_slashes_in_custom_query_string( $request['path'] ) && get_option( 'permalink_structure' ) );
	}
	
	/**
	 * Get redirect url
	 *
	 * @return string
	 */
	private function get_redirect_url() {
		// phpcs:ignore ET.Sniffs.ValidatedSanitizedInput.InputNotSanitized
		return ( $this->handle_trailing_slashes_in_custom_query_string( $this->new_login_url() ) . ( ! empty( esc_attr( $_SERVER['QUERY_STRING'] ) ) ? '?' . esc_attr( $_SERVER['QUERY_STRING'] ) : '' ) );
	}
	
	/**
	 * Rewrite $pagenow for custom url redirect handle
	 *
	 * @return void
	 * @see redirect_to_custom_login()
	 *
	 */
	public function handle_pagenow_to_custom_login_redirect() {
		global $pagenow;
		if ( isset( $_SERVER['REQUEST_URI'] ) ) {
			// phpcs:ignore ET.Sniffs.ValidatedSanitizedInput.InputNotValidated
			// phpcs:ignore ET.Sniffs.ValidatedSanitizedInput.InputNotSanitized
			$request = parse_url( rawurldecode( $_SERVER['REQUEST_URI'] ) );
			if ( $this->check_query_string( $request ) ) {
				wp_safe_redirect( $this->get_redirect_url() );
				die();
			}
			if ( $this->check_if_custom_query_string_is_set_in_request( $request ) ) {
				// phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited
				$pagenow = 'wp-login.php';
			}
		}
	}
	
	/**
	 * Check if custom url slug is in query string
	 *
	 * @param array $request
	 *
	 * @return bool
	 */
	private function check_if_custom_query_string_is_set_in_request( $request ) {
		return ( ( isset( $request['path'] ) && untrailingslashit( $request['path'] ) === home_url( $this->login_url_slug, 'relative' ) )
		         || ( ! get_option( 'permalink_structure' )
					// phpcs:ignore WordPress.Security.NonceVerification.Recommended -- Nonce verification is set by WordPress
		              && isset( $_GET[ $this->login_url_slug ] ) && empty( $_GET[ $this->login_url_slug ] ) ) );
	}
	
	/**
	 * New custom login url
	 *
	 * @param mixed $scheme
	 *
	 * @return string
	 */
	private function new_login_url( $scheme = null ) {
		$url = home_url( '/', $scheme );
		
		if ( get_option( 'permalink_structure' ) ) {
			return $this->handle_trailing_slashes_in_custom_query_string( $url . $this->login_url_slug );
		} else {
			return $url . '?' . $this->login_url_slug;
		}
	}
	
	/**
	 * Handle Custom Logout
	 *
	 * @return void
	 */
	public function handle_custom_logout() {
		if ( isset( $_SERVER['REQUEST_URI'] ) ) {
			// phpcs:ignore ET.Sniffs.ValidatedSanitizedInput.InputNotValidated
			// phpcs:ignore ET.Sniffs.ValidatedSanitizedInput.InputNotSanitized
			if ( strpos( $_SERVER['REQUEST_URI'], 'action=logout' ) !== false ) {
				check_admin_referer( 'log-out' );
				wp_logout();
				wp_safe_redirect( home_url(), 302 );
				die();
			}
			// phpcs:ignore ET.Sniffs.ValidatedSanitizedInput.InputNotValidated
			// phpcs:ignore ET.Sniffs.ValidatedSanitizedInput.InputNotSanitized
			if ( strpos( $_SERVER['REQUEST_URI'], 'wp-login.php' ) !== false ) {
				header( "HTTP/1.0 404 Not Found" );
				wp_safe_redirect( home_url( 'not-found' ), 302 );
				exit();
			}
		}
	}
	
	/**
	 * Redirect to custom login url
	 *
	 * @return void
	 */
	public function redirect_to_custom_login() {
		global $pagenow;
		if ( $pagenow === 'wp-login.php' ) {
			$redirect_to = admin_url();
			
			$requested_redirect_to = '';
			// phpcs:ignore WordPress.Security.NonceVerification.Recommended -- nonce verification is set by WordPress
			if ( isset( $_REQUEST['redirect_to'] ) ) {
				// phpcs:ignore WordPress.Security.NonceVerification.Recommended -- nonce verification is set by WordPress
				$requested_redirect_to = esc_url_raw( $_REQUEST['redirect_to'] );
			}
			
			if ( is_user_logged_in() ) {
				$user = wp_get_current_user();
				// phpcs:ignore WordPress.Security.NonceVerification.Recommended -- nonce verification is set by WordPress
				if ( ! isset( $_REQUEST['action'] ) ) {
					$logged_in_redirect = apply_filters( 'divi_ghoster_logged_in_redirect', $redirect_to, $requested_redirect_to, $user );
					wp_safe_redirect( esc_url( $logged_in_redirect ) );
					die();
				}
			}
			
			@require_once( ABSPATH . '/wp-login.php' );
			
			exit();
		}
	}
	
	/**
	 * Replace wp-login.php to custom url slug
	 *
	 * @param string $login_url
	 * @param string $redirect
	 *
	 * @return string
	 */
	public function replace_wp_login_php_to_custom_url_slug( $login_url, $redirect ) {
		return str_replace( 'wp-login.php', $this->login_url_slug, $login_url );
		
	}
	
	/**
	 * Replace wp-login.php in url query string to custom url slug
	 *
	 * @param string $url
	 * @param string $path
	 * @param string $orig_scheme
	 * @param int $blog_id
	 *
	 * @return string
	 */
	public function replace_wp_login_php_to_custom_url_slug_in_url_query( $url, $path, $orig_scheme, $blog_id ) {
		if ( 'login' == $orig_scheme || 'login_post' == $orig_scheme ) {
			$url = str_replace( 'wp-login.php', $this->login_url_slug, $url );
			
		}
		
		return $url;
		
	}
	
	/**
	 * Replace wp-login.php after logout
	 *
	 * @param string $location
	 *
	 * @return string
	 */
	public function replace_wp_login_php_after_logout( $location ) {
		return str_replace( 'wp-login.php', $this->login_url_slug, $location );
	}
}