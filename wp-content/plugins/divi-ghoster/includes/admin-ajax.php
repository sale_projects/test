<?php
// This file modified by Jonathan Hall and/or others; last modified 2021-03-02

/**
 * DiviAdminAjax class .
 *
 * Can handle ajax requests on Divi Ghoster settings page
 */

class DiviGhosterAdminAjax {
	/** @var string $custom_theme_image_dir */
	public static $custom_theme_image_dir;
	
	const THEME_TYPE_ALL = 1, THEME_TYPE_PARENT = 2, THEME_TYPE_CHILD = 3;
	
	public function setup() {
		self::$custom_theme_image_dir        = WP_CONTENT_DIR . '/uploads/ds-custom-theme-images/';
		add_action( 'admin_init', array( $this, 'admin_page_scripts' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'admin_ajax_scripts' ), 99 );
	}
	
	/**
	 * Save settings with ajax
	 *
	 * @return void
	 */
	public function admin_page_scripts() {
		// phpcs:ignore WordPress.Security.NonceVerification.Recommended
		if ( is_admin() && isset( $_GET['page'] ) && $_GET['page'] === 'divi_ghoster' ) {
			wp_enqueue_script( 'jquery-form' );
		}
	}
	
	/**
	 * Localize script to handle ajax requests
	 *
	 * @return void
	 */
	public function admin_ajax_scripts( $hook ) {
		if ( $hook === 'toplevel_page_divi_ghoster' ) {
			wp_localize_script( 'agsdg_admin_page', 'divi_ghoster', array(
				'success_message'       => __( 'Settings saved!', 'divi-ghoster' ),
				'ajax_url'              => admin_url( 'admin-ajax.php' ),
				'divi_ghoster_nonce'    => wp_create_nonce( DiviGhosterAdmin::$divi_ghoster_nonce ),
				'hide_nginx_nonce'      => wp_create_nonce( 'hide-nginx-notice_' . DiviGhoster::PLUGIN_SLUG ),
				'remove_image_message'  => __( 'Are you sure? The image will be removed!', 'divi-ghoster' ),
				'set_image_message'     => __( 'This will replace/set current image for theme', 'divi-ghoster' ),
				'nginx_dismiss_message' => __( 'If you dismiss this message, it wont appear again. (the features that will not work, will be highlighted though)', 'divi-ghoster' ),
				'custom_image_data'     => get_option( 'divi_ghoster_custom_theme_image' ),
				'default_logo'          => DiviGhoster::$pluginBaseUrl . 'assets/images/default-logo.png',
				'active_clone'          => DiviGhoster::$settings['theme_slug']
			) );
			
			wp_enqueue_script( 'agsdg_admin_page' );
		}
	}
	
	/**
	 * Add and save custom theme image
	 *
	 * @return void
	 * @throws Exception
	 */
	public static function process_branding_image($url, $isThemeImage, $forChildTheme=false) {
		DiviGhoster::addLogEntry(
			sprintf(
				__('Processing branding image: is theme image? %s; for child theme? %s', 'divi-ghoster'),
				$isThemeImage ? __('Yes', 'divi-ghoster') : __('No', 'divi-ghoster'),
				$forChildTheme ? __('Yes', 'divi-ghoster') : __('No', 'divi-ghoster')
			)
		);
	
	
		if ( ! current_user_can( 'manage_options' ) ) {
			DiviGhoster::addLogEntry( __('Permissions failure', 'divi-ghoster') );
			return __( 'Something is wrong and the action could not be proceed! Please try again later', 'divi-ghoster' );
		}
		
		// Disable SSL certificate verification since this may fail for the current site
		add_filter( 'http_request_args', array(__CLASS__, 'disableSslVerification') );
		
		$tmpFile = download_url($url);
		
		remove_filter( 'http_request_args', array(__CLASS__, 'disableSslVerification') );
	
		if ( is_wp_error($tmpFile) ) {
			DiviGhoster::addLogEntry(
				sprintf(
					__('Error while downloading the image: %s', 'divi-ghoster'),
					implode( '; ', $tmpFile->get_error_messages() )
				)
			);
			return __( 'Error while downloading the image.', 'divi-ghoster' );
		}
		
		$filename = $isThemeImage
						? (
							$forChildTheme
							? 'screenshot-child.'
							: 'screenshot.'
						)
						: 'branding.';
		
		switch ( exif_imagetype( $tmpFile ) ) {
			case IMAGETYPE_PNG:
				$deleteFilename = $filename.'jpg';
				$filename .= 'png';
				break;
			case IMAGETYPE_JPEG:
				$deleteFilename = $filename.'png';
				$filename .= 'jpg';
				break;
			default:
				DiviGhoster::addLogEntry( __('Invalid uploaded image type', 'divi-ghoster') );
				return __( 'Please upload a valid JPEG or PNG image.', 'divi-ghoster' );
		}
		
		if ( ! self::move_uploaded_file_to_custom_directory( $filename, $tmpFile ) ) {
			DiviGhoster::addLogEntry( __('Error while moving uploaded file to custom directory', 'divi-ghoster') );
			return __( 'Error while saving image', 'divi-ghoster' );
		}
		
		if ( file_exists(self::$custom_theme_image_dir.$deleteFilename) && !unlink(self::$custom_theme_image_dir.$deleteFilename) ) {
			DiviGhoster::addLogEntry( __('Error while deleting previous image', 'divi-ghoster') );
			return __( 'Error while deleting previous image', 'divi-ghoster' );
		}
		
		if ( $isThemeImage ) {
			if (!$forChildTheme) {
				try {
					self::setCustomThemeImage(self::THEME_TYPE_PARENT);
				} catch (Exception $ex) {
					// return $ex->getMessage();
					DiviGhoster::addLogEntry(
						sprintf(
							__('Exception while setting custom parent theme image: %s', 'divi-ghoster'),
							$ex->getMessage()
						)
					);
				}
			}
			if ($forChildTheme || DiviGhoster::$settings['child_theme_image_url'] === 'same') {
				try {
					self::setCustomThemeImage(self::THEME_TYPE_CHILD);
				} catch (Exception $ex) {
					DiviGhoster::addLogEntry(
						sprintf(
							__('Exception while setting custom child theme image: %s', 'divi-ghoster'),
							$ex->getMessage()
						)
					);
					// return $ex->getMessage();
				}
			}
		}
		
		return true;
				
	}
	
	public static function disableSslVerification($httpRequestArgs) {
		$httpRequestArgs['sslverify'] = false;
		return $httpRequestArgs;
	}
	
	public static function getThemesForImageChange($themeType=self::THEME_TYPE_ALL) {
		
		$themes = array();
		$themesToMatch = array( DiviGhoster::$targetTheme );
		
		// divi-ghoster/includes/ultimate.php
		if ( ! empty( DiviGhoster::$settings['theme_slug'] ) ) {
			$themesToMatch[] = DiviGhoster::$settings['theme_slug'];
		}
		
		$includeChildThemes = $themeType == self::THEME_TYPE_ALL || $themeType == self::THEME_TYPE_CHILD;
		$includeParentThemes = $themeType == self::THEME_TYPE_ALL || $themeType == self::THEME_TYPE_PARENT;
		
		
		foreach ( wp_get_themes() as $theme ) {
			if (
				( $includeParentThemes && in_array($theme->stylesheet, $themesToMatch) )
				|| ( $includeChildThemes && $theme->parent() && in_array($theme->parent()->get_stylesheet(), $themesToMatch) )
			) {
				$themes[] = $theme->stylesheet_dir.'/';
			}
		}
		
		return $themes;
	}
	
	public static function setCustomThemeImage($themeType) { // does not support self::THEME_TYPE_ALL
		
		$success = true;
			
		$screenshotName = $themeType === self::THEME_TYPE_PARENT || !isset(DiviGhoster::$settings['child_theme_image_url']) || DiviGhoster::$settings['child_theme_image_url'] === 'same' ? 'screenshot' : 'screenshot-child';
		
		$themesForChange = self::getThemesForImageChange($themeType);
		
		DiviGhoster::addLogEntry(
			sprintf(
				__('Setting custom theme image in: %s', 'divi-ghoster'),
				implode(', ', $themesForChange)
			)
		);
		
		foreach ( $themesForChange as $themeRoot ) {
			
			// divi-ghoster/includes/ultimate.php
			$success = (
				( file_exists( $themeRoot . 'screenshot.jpg' ) && ( file_exists( $themeRoot . '_screenshot.jpg' ) || rename( $themeRoot . 'screenshot.jpg', $themeRoot . '_screenshot.jpg' ) ) ) ||
				( file_exists( $themeRoot . 'screenshot.png' ) && ( file_exists( $themeRoot . '_screenshot.png' ) || rename( $themeRoot . 'screenshot.png', $themeRoot . '_screenshot.png' ) ) )
			) && $success;
			
			$success = (
				( file_exists( self::$custom_theme_image_dir . $screenshotName.'.jpg' ) && copy( self::$custom_theme_image_dir . $screenshotName.'.jpg', $themeRoot . 'screenshot.jpg' ) ) ||
				( file_exists( self::$custom_theme_image_dir . $screenshotName.'.png' ) && copy( self::$custom_theme_image_dir . $screenshotName.'.png', $themeRoot . 'screenshot.png' ) )
			) && $success;
		}
		
		if (!$success) {
			throw new Exception( __( 'Unable to set theme screenshot.', 'divi-ghoster' ) );
		}
	
	}
	
	public static function restoreThemeImage($deleteCustom=true, $themeType=self::THEME_TYPE_ALL) {
		
		$success = true;
		
		if ($deleteCustom) {
			if ( $themeType == self::THEME_TYPE_ALL || $themeType == self::THEME_TYPE_PARENT ) {
				DiviGhoster::addLogEntry( __('Deleting custom theme image for parent theme', 'divi-ghoster') );
				
				if ( file_exists( self::$custom_theme_image_dir . 'screenshot.jpg' ) ) {
					unlink( self::$custom_theme_image_dir . 'screenshot.jpg' );
				}
				
				if ( file_exists( self::$custom_theme_image_dir . 'screenshot.png' ) ) {
					unlink( self::$custom_theme_image_dir . 'screenshot.png' );
				}
			}
			
			if ( ($themeType == self::THEME_TYPE_ALL || $themeType == self::THEME_TYPE_CHILD) && isset(DiviGhoster::$settings['child_theme_image_url']) && DiviGhoster::$settings['child_theme_image_url'] !== 'same' ) {
				DiviGhoster::addLogEntry( __('Deleting custom theme image for child theme', 'divi-ghoster') );
				
				if ( file_exists( self::$custom_theme_image_dir . 'screenshot-child.jpg' ) ) {
					unlink( self::$custom_theme_image_dir . 'screenshot-child.jpg' );
				}
				
				if ( file_exists( self::$custom_theme_image_dir . 'screenshot-child.png' ) ) {
					unlink( self::$custom_theme_image_dir . 'screenshot-child.png' );
				}
			}
			
		}
		
		if ( $themeType == self::THEME_TYPE_PARENT && ( !isset(DiviGhoster::$settings['child_theme_image_url']) || DiviGhoster::$settings['child_theme_image_url'] === 'same' ) ) {
			$themeType = self::THEME_TYPE_ALL;
		}
		
		$themesForChange = self::getThemesForImageChange($themeType);
		
		DiviGhoster::addLogEntry(
			sprintf(
				__('Restoring theme image in: %s', 'divi-ghoster'),
				implode(', ', $themesForChange)
			)
		);
		
		foreach ($themesForChange as $themeRoot) {
			
			// divi-ghoster/includes/ultimate.php
			$success = (
				(
					file_exists( $themeRoot . '_screenshot.jpg' ) && rename( $themeRoot . '_screenshot.jpg', $themeRoot . 'screenshot.jpg' )
					&& ( !file_exists( $themeRoot . '_screenshot.png' ) || unlink( $themeRoot . '_screenshot.png' ) )
				) ||
				(
					file_exists( $themeRoot . '_screenshot.png' ) && rename( $themeRoot . '_screenshot.png', $themeRoot . 'screenshot.png' )
					&& ( !file_exists( $themeRoot . '_screenshot.jpg' ) || unlink( $themeRoot . '_screenshot.jpg' ) )
					
				)
			) && $success;
			
		}
		
		if (!$success) {
			throw new Exception( esc_html__( 'Unable to restore theme screenshot.', 'divi-ghoster' ) );
		}
	
	}
	
	
	public static function removeBrandingImage() {
		
		DiviGhoster::addLogEntry( __('Deleting branding image', 'divi-ghoster') );
		
		$success = true;
		
		if ( file_exists( self::$custom_theme_image_dir . 'branding.jpg' ) ) {
			unlink( self::$custom_theme_image_dir . 'branding.jpg' );
		}
		
		if ( file_exists( self::$custom_theme_image_dir . 'branding.png' ) ) {
			unlink( self::$custom_theme_image_dir . 'branding.png' );
		}
		
		
		if (!$success) {
			throw new Exception( esc_html__( 'Unable to remove branding image.', 'divi-ghoster' ) );
		}
	
	}
	
	/**
	 * Move uploaded file to custom directory
	 *
	 * @param string $file_name
	 * @param string $temporary_file
	 *
	 * @return array
	 */
	private static function move_uploaded_file_to_custom_directory( $file_name, $temporary_file ) {
		
		if ( ! is_dir( self::$custom_theme_image_dir ) ) {
			mkdir( self::$custom_theme_image_dir );
		}
		if ( is_file( $temporary_file ) ) {
			return rename( $temporary_file, self::$custom_theme_image_dir . $file_name );
		}
		
		return false;
		
	}
	
}