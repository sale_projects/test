<?php
/*
 * This file modified by Terry Hale, Coulter Peterson, Anna Kurowska, Vasili Guruli, Jonathan Hall and/or others; last modified 2020-12-21
 */

/**
 * Class DiviGhosterCustomBuilder
 * Manages the Divi Builder customization
 *
 * @since 3.0.0
 *
 * 2019-11-12   Modified $wp_customizer->add_setting(). (Commented out sanitizer filter)
 *              Divi does not sanitize colors, since they have an rgba picker.
 *              The sanitize_hex_color WP filter fails with "Invalid value." if an rgba value is passed.
 */
class DiviGhosterCustomBuilder {
	
	/**
	 * @var string option name where customizations are stored
	 */
	private static $builder_custom_opt_name = 'agsdg_settings';
	
	/**
	 * @var string internationalization slug
	 */
	private static $i18l_namespace = 'ags-ghoster';
	
	
	/**
	 * @var string base url for the script directory
	 */
	private static $builder_script_base_url;
	
	/**
	 * @var string base url for the style directory
	 */
	private static $builder_style_base_url;
	
	/**@var string Custom Builder CSS File */
	public static $custom_css_file, $custom_css_url;
	
	/**
	 * kick things off.
	 *
	 * @return void
	 * @since: 3.0.0
	 *
	 */
	
	public static function setup() {
		$uploadDir = wp_get_upload_dir();
		self::$custom_css_file = $uploadDir['basedir'] . '/ds-custom-builder-css/et-custom-builder.css';
		self::$custom_css_url = $uploadDir['baseurl'] . '/ds-custom-builder-css/et-custom-builder.css';
		// add inline builder custom styles
		add_action( 'wp_ajax_add_custom_builder_css', array( __CLASS__, 'add_custom_builder_css' ), 10 );
		if ( ! empty( DiviGhoster::$settings['enable_custom_colors'] ) && (string) DiviGhoster::$settings['enable_custom_colors'] == '1' ) {
			add_action( 'wp_enqueue_scripts', array( __CLASS__, 'create_builder_custom_inline_styles' ), 99 );
			add_action( 'admin_enqueue_scripts', array( __CLASS__, 'create_builder_custom_inline_styles' ), 99 );
		}
	}
	
	/**
	 * return the default builder colors
	 *
	 * @return array
	 * @since: 3.0.0
	 *
	 */
	private static function get_default_builder_colors() {
		return array(
			'primary_accent_color'   => '#8F42ED',
			'primary_accent_color_2' => '#7e3bd0',
			'primary_accent_color_3' => '#6c2eb9',
			'primary_accent_color_4' => '#7435C1',
			'secondary_accent_color' => '#2b87da',
			'3rd_accent_color'       => '#00C3AA',
			'4rd_accent_color'       => '#FF5600',
			'5th_accent_color'       => '#97d000',
			'6th_accent_color'       => '#4c5866',
			'text_color'             => '#FFFFFF',
		);
	}
	
	/**
	 * make the customized builder styles available to the page
	 *
	 * @return void
	 * @since: 3.0.0
	 *
	 */
	public static function create_builder_custom_inline_styles() {
		if ( file_exists( self::$custom_css_file ) ) {
			wp_enqueue_style(
				'ds-custom-builder',
				self::$custom_css_url
			);
		}
	}
	
	/**
	 * Create custom builder css file
	 *
	 * @return array
	 */
	public static function add_custom_builder_css() {
		if ( ! current_user_can( 'manage_options' ) ) {
			die( 'You are not allowed to be here!' );
		}
		if ( isset( $_POST['type'] ) && $_POST['type'] === 'build_css' ) {
			if ( isset( $_POST['divi_ghoster_nonce'] ) ) {
				if ( ! wp_verify_nonce( sanitize_key( wp_unslash( $_POST['divi_ghoster_nonce'] ) ), DiviGhosterAdmin::$divi_ghoster_nonce ) ) {
					die( 'You are not allowed to be here!' );
				}
			}
			if ( isset( $_POST['agsdg_settings'] ) ) {
				// phpcs:ignore ET.Sniffs.ValidatedSanitizedInput.InputNotSanitized, ET.Sniffs.ValidatedSanitizedInput.InputNotValidated -- $_POST['agsdg_settings'] is sanitized in filter_var_array function
				$custom_builder_options = filter_var_array( $_POST['agsdg_settings'], FILTER_SANITIZE_STRING );
				$custom_builder_options = filter_var_array( $custom_builder_options, FILTER_SANITIZE_STRING );
				
				if ( empty( $custom_builder_options ) ) {
					return;
				}
				
				$custom_builder_options = wp_parse_args( $custom_builder_options, self::get_default_builder_colors() );
				
				$custom_css = self::build_custom_css_output( $custom_builder_options );
				
				if (!file_exists( dirname(self::$custom_css_file) ) ) {
					mkdir( dirname(self::$custom_css_file) );
				}
				
				if ( file_put_contents( self::$custom_css_file, $custom_css ) ) {
					return array(
						'status' => 'success'
					);
				} else {
					return array(
						'status' => 'error'
					);
				}
			}
		}
		
		exit();
	}
	
	
	/**
	 * build the css output for the builder customizations
	 *
	 * @param $custom_colors
	 *
	 * @return string
	 * @since: 3.0.0
	 *
	 */
	private static function build_custom_css_output( $custom_colors ) {
		$css_output = array();
		foreach ( $custom_colors as $option_name => $color ) {
			$selectors = self::get_css_selectors( $option_name );
			
			if ( empty( $selectors ) || empty( $selectors[0]['selector'] ) ) {
				continue;
			}
			
			foreach ( $selectors as $selector_props ) {
				
				if ( isset( $selector_props['important'] ) && $selector_props['important'] ) {
					$is_important = ' !important';
				} else {
					$is_important = '';
				}
				
				$css_output[] = sprintf( '%s { %s: %s %s; }', $selector_props['selector'], $selector_props['property'], $color, $is_important );
			}
		}
		
		$css_output = implode( "\n", $css_output );
		
		
		$preloader_colors = '@keyframes et-bfb-loader {  0% {  box-shadow: 0 -17px ' . $custom_colors['primary_accent_color_2'] . ', 17px 0 ' . $custom_colors['3rd_accent_color'] . ', 0 17px ' . $custom_colors['primary_accent_color_2'] . ', -17px 0 ' . $custom_colors['3rd_accent_color'] . ';}
				25% { box-shadow: 17px 0 ' . $custom_colors['secondary_accent_color'] . ', 0 17px ' . $custom_colors['3rd_accent_color'] . ', -17px 0 ' . $custom_colors['secondary_accent_color'] . ', 0 -17px ' . $custom_colors['3rd_accent_color'] . ';  }
				50% { box-shadow: 0 17px ' . $custom_colors['secondary_accent_color'] . ', -17px 0 ' . $custom_colors['4rd_accent_color'] . ', 0 -17px ' . $custom_colors['secondary_accent_color'] . ', 17px 0 ' . $custom_colors['4rd_accent_color'] . '; }
				75% { box-shadow: -17px 0 ' . $custom_colors['primary_accent_color_2'] . ', 0 -17px ' . $custom_colors['4rd_accent_color'] . ', 17px 0 ' . $custom_colors['primary_accent_color_2'] . ', 0 17px ' . $custom_colors['4rd_accent_color'] . '; }
				100% { box-shadow: 0 -17px ' . $custom_colors['primary_accent_color_2'] . ', 17px 0 ' . $custom_colors['3rd_accent_color'] . ', 0 17px ' . $custom_colors['primary_accent_color_2'] . ', -17px 0 ' . $custom_colors['3rd_accent_color'] . ';}}' .
		                    '@keyframes et-fb-loader {  0% {  box-shadow: 0 -17px ' . $custom_colors['primary_accent_color_2'] . ', 17px 0 ' . $custom_colors['3rd_accent_color'] . ', 0 17px ' . $custom_colors['primary_accent_color_2'] . ', -17px 0 ' . $custom_colors['3rd_accent_color'] . ';}
				25% { box-shadow: 17px 0 ' . $custom_colors['secondary_accent_color'] . ', 0 17px ' . $custom_colors['3rd_accent_color'] . ', -17px 0 ' . $custom_colors['secondary_accent_color'] . ', 0 -17px ' . $custom_colors['3rd_accent_color'] . ';  }
				50% { box-shadow: 0 17px ' . $custom_colors['secondary_accent_color'] . ', -17px 0 ' . $custom_colors['4rd_accent_color'] . ', 0 -17px ' . $custom_colors['secondary_accent_color'] . ', 17px 0 ' . $custom_colors['4rd_accent_color'] . '; }
				75% { box-shadow: -17px 0 ' . $custom_colors['primary_accent_color_2'] . ', 0 -17px ' . $custom_colors['4rd_accent_color'] . ', 17px 0 ' . $custom_colors['primary_accent_color_2'] . ', 0 17px ' . $custom_colors['4rd_accent_color'] . '; }
				100% { box-shadow: 0 -17px ' . $custom_colors['primary_accent_color_2'] . ', 17px 0 ' . $custom_colors['3rd_accent_color'] . ', 0 17px ' . $custom_colors['primary_accent_color_2'] . ', -17px 0 ' . $custom_colors['3rd_accent_color'] . ';}}';
		$css_output       = $css_output . $preloader_colors;
		
		
		return $css_output;
		
	}
	
	private static function get_css_selectors( $option ) {
		switch ( $option ) {
			
			case "primary_accent_color":
				return array(
					array(
						'selector'  => '#epanel-mainmenu li.ui-state-active a, #epanel-header .et-defaults-button:hover, .et-core-tabs li.ui-state-active a, #et_pb_layout_controls .et-pb-layout-buttons,  .et-tb-branded-modal__actions  .et-common-button--tertiary, .et-tb-branded-modal__header, .et-common-tabs-navigation__button--active, .et-common-tabs-navigation__button--active:hover, .et_pb_layout_controls .et_pb_roles_active_menu, .et_pb_roles_active_menu, .et-db #et-boc .et-fb-modal .et-fb-tabs__item--active, .et-db #et-boc .et-fb-modal .et-fb-tabs__item--active:hover, .et-db #et-boc .et-l .et-fb-tabs__item--active:hover, .et-core-tabs li.ui-state-active a, .et-core-tabs a:hover, .et-pb-options-tabs-links li.et-pb-options-tabs-links-active a, .et-pb-options-tabs-links li a:hover, .et-db #et-boc .et-fb-modal-add-module-container .et-fb-settings-tabs-nav-item--active a, .et-db #et-boc .et-fb-settings-tabs-nav-item:hover a',
						'property'  => 'background-color',
						'important' => true,
					),
				);
				break;
			
			case "primary_accent_color_2":
				return array(
					array(
						'selector'  => '.block-editor__container #et-switch-to-divipan',
						'property'  => 'border-color',
						'important' => false,
					),
					
					array(
						'selector'  => '#epanel-mainmenu, .et-core-tabs, .et-core-tabs.ui-widget-header, #et_pb_layout_controls, #et_pb_layout_controls, #epanel-header .et-defaults-button, .et-common-tabs-navigation, .block-editor__container #et-switch-to-divi, .et-common-tabs-navigation__button, .et-db #et-boc .et-fb-modal .et-fb-tabs__list, .et-db #et-boc .et-fb-modal .et-fb-tabs__list:hover, .et-db #et-boc .et-l .et-fb-tabs__item:hover, .et-pb-options-tabs-links, .et-pb-options-tabs-links li a:hover, .et-pb-options-tabs-links, .et-pb-options-tabs-links li a:hover, .et-core-tabs, .et-core-tabs.ui-widget-header, .et-db #et-boc .et-fb-modal-add-module-container .et-fb-settings-tabs-nav, .et-db #et-boc #et-fb-app .et-fb-modal .et-fb-modal__footer .et-fb-button--primary-alt, a.et-pb-modal-save-template, a.et-pb-modal-save-template:hover',
						'property'  => 'background-color',
						'important' => true,
					),
					
					array(
						'selector'  => '.et-db #et-boc .et-l .et-fb-modal-add-module-container.et_fb_add_section_modal .et-fb-settings-options-tab-modules_all li.et_fb_fullwidth',
						'property'  => 'background',
						'important' => true,
					)
				);
				break;
			
			case "primary_accent_color_3":
				return array(
					array(
						'selector' => '.et-db #et-boc .et-fb-modal-add-module-container.et_fb_add_section_modal .et-fb-settings-options-tab-modules_all li.et_fb_fullwidth, .et-db #et-boc .et-fb-modal .et-fb-modal__header, .et-db #et-boc #et-fb-app .et-fb-modal .et-fb-modal__header .et-fb-button, .et-core-modal-header, .et-pb-settings-heading, .et_pb_prompt_modal h3, .et-db #et-boc .et-fb-modal-add-module-container .et-fb-settings-heading, .et-db #et-boc #et-fb-app .et-fb-modal-add-module-container .et-fb-settings-heading .et-fb-button, .et-builder-exit-modal .et-core-modal-header, .et_pb_toggle_builder_wrapper #et_pb_toggle_builder, #epanel-header, et_pb_roles_title, h1#epanel-title, .reset-popup-header, .et-box-desc-top, .reset-popup-header, .et-tb-admin-container-header, .et_pb_roles_title, .et_pb_prompt_modal h3, .et-core-modal-header, .et-common-prompt__header, #et_pb_layout .hndle, .et-db #et-boc #et_pb_layout .hndle .et-fb-button, .et-db #et-boc #et_pb_layout .hndle .et-fb-button:hover, .et-db #et-boc #et-fb-app .et-fb-page-settings-bar > .et-fb-button, .et-db #et-boc #et-fb-app .et-fb-page-settings-bar > .et-fb-button:hover, .et-db #et-boc #et-fb-app .et-fb-page-settings-bar .et-fb-page-settings-bar__column--main .et-fb-button, #et_pb_layout_controls, #et_pb_layout_controls .et-pb-layout-buttons, #et_pb_layout_controls .et-pb-layout-buttons:hover',
						'property' => 'background-color',
					),
					
					array(
						'selector'  => '.et-db #et-boc .et-l #et-fb-app .et-fb-button--primary.et-fb-page-settings-bar__toggle-button, .et-db #et-boc .et-l #et-fb-app .et-fb-page-settings-bar__toggle-button   .et-fb-button--primary:hover, .et_pb_toggle_builder_wrapper .et_pb_ready#et_pb_fb_cta, .et-db #et-boc .et-l .et-fb-modal-add-module-container.et-fb-modal-settings--inversed:after,  .et-db #et-boc .et-l #et-fb-app .et-fb-buttonbutton--primary, .et-db #et-boc .et-l #et-fb-app .et-fb-buttonbutton--primary:hover',
						'property'  => 'background-color',
						'important' => true,
					),
					array(
						'selector'  => '.et-db #et-boc .et-l #et-fb-app .et-fb-button--primary, .et-db #et-boc .et-l #et-fb-app .et-fb-button--primary:hover, .et-db #et-boc .et-l .et-fb-skeleton--fullwidth>.et-fb-skeleton__header, .et-db #et-boc .et-l .et_pb_section.et_pb_fullwidth_section>.et-fb-skeleton>.et-fb-skeleton__content>.et-fb-section-button-wrap--add .et-fb-button, .et-db #et-boc .et-l #et-fb-app .et-fb-page-settings-bar__column.et-fb-page-settings-bar__column--main .et-fb-button--primary, .et-db #et-boc .et-l #et-fb-app  .et-fb-page-settings-bar__column.et-fb-page-settings-bar__column--main .et-fb-button--primary:hover',
						'property'  => 'background',
						'important' => true,
					)
				);
				break;
			
			case "primary_accent_color_4":
				return array(
					array(
						'selector'  => '#epanel-mainmenu a:hover, #et_pb_layout_controls .et-pb-layout-buttons:hover, .et-core-tabs a:hover, .et-common-tabs-navigation__button:hover',
						'property'  => 'background-color',
						'important' => true,
					)
				);
				break;
			
			case "text_color":
				return array(
					array(
						'selector' => '.et-db #et-boc .et-l #et_pb_toggle_builder.et_pb_builder_is_used, .et-db #et-boc .et-l #et_pb_fb_cta, .et-db #et-boc .et-fb-modal .et-fb-modal__content .et-fb-tabs__item, .et-db #et-boc .et-fb-modal .et-fb-modal__header, .et-db #et-boc #et-fb-app .et-fb-modal.et-fb-modal--app .et-fb-modal__header, .et-db #et-boc .et-fb-modal .et-fb-tabs__item--active, .et-db #et-boc .et-fb-modal-add-module-container .et-fb-settings-heading, .et-db #et-boc .et-fb-modal-add-module-container .et-fb-settings-tabs-nav-item a, .et-db #et-boc .et-fb-modal-add-module-container .et-fb-settings-tabs-nav .et-fb-settings-tabs-nav-item--active a, .et-builder-exit-modal .et-core-modal-title, .et-builder-exit-modal .et-core-modal-close, .et-builder-exit-modal .et-core-modal-close:hover, .et-builder-exit-modal .et-core-modal-close:before, .et-builder-exit-modal .et_pb_prompt_buttons .et-core-modal-action, .et-db #et-boc .et-l #et-fb-app .et-fb-button.et-fb-button--elevate.et-fb-button--info.et-fb-button--save-draft, .et-db #et-boc .et_pb_root--vb .et-fb-page-settings-bar .et-fb-button--save-draft, .et-db #et-boc .et-fb-button-group .et-fb-button--save-draft, .et-db #et-boc .et-fb-button-group .et-fb-button--publish, .et-db #et-boc .et-fb-button-group .et-fb-button--publish, .et-db #et-boc .et-l #et-fb-app .et-fb-button.et-fb-button--elevate.et-fb-button--success.et-fb-button--publish, .et-db #et-boc .et_pb_root--vb .et-fb-page-settings-bar .et-fb-button--publish, .et-db #et-boc .et-fb-button-group .et-fb-button--publish, .et-db #et-boc .et-fb-skeleton--row > .et-fb-skeleton__header, .et-db #et-boc .et-fb-skeleton--module > .et-fb-skeleton__header, .et-db #et-boc .et-fb-skeleton--section > .et-fb-skeleton__header, #panel-wrap #epanel-mainmenu a, #panel-wrap .et-save-button, #et-theme-builder .et-tb-admin-save-button, .et-tb-admin-container-header, h1#epanel-title, #et_pb_layout .hndle, .et-db #et-boc .et-l .et-fb-modal__quick-actions input.et-fb-settings-option-input',
						'property' => 'color'
					),
					array(
						'selector' => '.et-fb-item-addable-button .et-fb-icon.et-fb-icon--add svg, .et-db #et-boc #et-fb-app .et-fb-modal .et-fb-modal__header .et-fb-button svg, .et-db #et-boc #et-fb-app .et-fb-modal.et-fb-modal--app .et-fb-modal__header .et-fb-button svg, .et-db #et-boc .et-fb-button-group--responsive-mode .et-fb-icon svg, .et-db #et-boc .et-fb-button-group--builder-mode .et-fb-icon svg, .et-db #et-boc .et-fb-button-group--responsive-mode .et-fb-button--app-modal path, .et-db #et-boc .et-fb-modal .et-fb-modal__footer .et-fb-icon svg, .et-db #et-boc .et-fb-modal-add-module-container .et-fb-settings-heading .et-fb-icon svg, .et-db #et-boc .et-fb-skeleton--row > .et-fb-skeleton__header .et-fb-icon svg, .et-db #et-boc .et-fb-row-button-wrap--add .et-fb-icon svg, .et-db #et-boc #et-fb-app .et-fb-component-settings--row > .et-fb-button-group .et-fb-icon svg, .et-db #et-boc .et-fb-skeleton--module > .et-fb-skeleton__header .et-fb-icon svg, .et-db #et-boc .et_pb_module .et-fb-icon svg, .et-db #et-boc .et_pb_row > .et_pb_column_empty .et-fb-icon svg, .et-db #et-boc .et-fb-skeleton--row .et_pb_column_empty .et-fb-icon svg, .et-db #et-boc .et-fb-section-button-wrap--add .et-fb-icon svg, .et-db #et-boc .et-fb-skeleton--section > .et-fb-skeleton__header .et-fb-icon svg, .et-db #et-boc .et-fb-component-settings--section .et-fb-button-group .et-fb-icon svg, .et-db #et-boc .et-fb-page-settings-bar__toggle-button .et-fb-icon svg, .et-db #et-boc .et-fb-page-settings-bar__toggle-button ~ .et-fb-button-group .et-fb-icon svg, .et-db #et-boc #et_pb_layout .et-fb-page-settings-bar .et-fb-button .et-fb-icon svg',
						'property' => 'fill'
					),
					array(
						'selector' => '.et-db #et-boc .et-fb-page-settings-bar__toggle-button .et-fb-icon__child, .et-db #et-boc .et-fb-page-settings-bar__toggle-button .et-fb-icon__child:before, .et-db #et-boc .et-fb-page-settings-bar__toggle-button .et-fb-icon__child:after, #et_support_center .et_documentation_videos_list .active .dashicons-arrow-right',
						'property' => 'background'
					),
					
					array(
						'selector' => '.et-db #et-boc .et-l .et-fb-video-list__item--active .et-fb-video-list__icon',
						'property' => 'background-color'
					),
					
					array(
						'selector'  => '.et-db #et-boc .et-l #et-fb-app .et-fb-button--succes',
						'property'  => 'background',
						'important' => true
					),
				);
				break;
			
			// blue
			case "secondary_accent_color":
				return array(
					
					array(
						'selector'  => '.et-db #et-boc .et-l .et_select_animation .et_animation_button a.et_active_animation .et_animation_button_icon svg g, .et-db #et-boc .et-l .et_select_animation .et_animation_button a:hover .et_animation_button_icon svg g, .et-fb-settings-options-wrap .et-fb-icon.et-fb-icon--help-circle, .et-fb-icon.et-fb-icon--next svg, .et-fb-icon.et-fb-icon--app-setting,
						.et-fb-tabs__panel .et-fb-form__toggle .et-fb-settings-background-tab-nav--color.et-fb-settings-background-tab-nav--active  .et-fb-icon svg,
						.et-fb-tabs__panel .et-fb-form__toggle .et-fb-settings-background-tab-nav--video.et-fb-settings-background-tab-nav--active .et-fb-icon svg,
						.et-fb-tabs__panel .et-fb-form__toggle .et-fb-settings-background-tab-nav.et-fb-settings-background-tab-nav--image.et-fb-settings-background-tab-nav--active .et-fb-icon svg,
						.et-fb-tabs__panel .et-fb-form__toggle .et-fb-settings-background-tab-nav--gradient.et-fb-settings-background-tab-nav--active  .et-fb-icon svg, .et-fb-icon.et-fb-icon--help-circle path, .et-fb-icon.et-fb-icon--help-circle svg, .et-fb-icon.et-fb-icon--help-circle g',
						'property'  => 'fill',
						'important' => true
					),
					array(
						'selector'  => '.et-db #et-boc .et-l #et-fb-app .et-fb-section-button-wrap--add button.et-fb-button.et-fb-button--info, .et-db #et-boc .et-l .et-fb-skeleton--section>.et-fb-skeleton__header, input[type="range"]::-webkit-slider-thumb, input[type="range"]::-moz-range-thumb, input[type="range"]::-ms-thumb, input[type="range"]::-webkit-slider-thumb, .et-db #et-boc .et-fb-skeleton--section > .et-fb-skeleton__header, .et-db #et-boc .et-l .et-fb-video-list__icon',
						'property'  => 'background-color',
						'important' => true
					),
					array(
						'selector' => '.et-db #et-boc .et-l #et-fb-app .et-fb-button.et-fb-button--info, .et-db #et-boc .et-l #et-fb-app .et-fb-button--info, .et-db #et-boc .et-l #et-fb-app .et-fb-section-button-wrap--add .et-fb-button--info, .et-db #et-boc .et-l #et-fb-app .et-fb-skeleton--fullwidth .et-fb-skeleton__header .et-fb-button--primary, .et-db #et-boc .et-l .et-fb-skeleton--section>.et-fb-skeleton__header',
						'property' => 'background'
					),
					array(
						'selector' => '.et-db #et-boc .et-l .et-fb-modal-add-module-container.et_fb_add_section_modal .et-fb-main-settings--add_new_module .et-fb-settings-options-wrap .et-fb-settings-options.et-fb-modules-list li.et_fb_regular, .et-db #et-boc .et-l .et-fb-modal-add-module-container.et_fb_add_section_modal .et-fb-main-settings--add_new_module .et-fb-settings-options-wrap .et-fb-settings-options.et-fb-modules-list li.et_fb_regular::after , .et-db #et-boc .et-l .et-fb-settings-custom-select-wrapper.et-fb-settings-option-select-active .select-option-item-hovered, .et-db #et-boc .et-l .et-fb-form__help--active, .et-db #et-boc .et-l .et-fb-form__hover--active, .et-db #et-boc .et-l .et-fb-form__menu--active, .et-db #et-boc .et-l .et-fb-form__reset--active, .et-db #et-boc .et-l .et-fb-form__responsive--active, .et-db #et-boc .et-l .et-fb-video-list__item--active, .et-db #et-boc .et-l #et-fb-app .et-fb-button--info, .et-db #et-boc .et-l .et-fb-modal-add-module-container.et_fb_add_section_modal .et-fb-settings-options-tab-modules_all li:after, #panel-wrap .et_pb_yes_no_button.et_pb_on_state, #epanel .et_pb_yes_no_button.et_pb_on_state, .et-admin-page  .reset-popup-overlay .no, .et-admin-page .et-tb-branded-modal__actions  .et-common-button--primary, .et-common-prompt__modal .et-common-button--primary, .et-tb-branded-modal  .et-common-button--primary, .et_pb_prompt_buttons a.et_pb_prompt_proceed, .et_pb_prompt_modal .et_pb_prompt_buttons input.et_pb_prompt_proceed, .et-core-modal-action,  #et-boc #et-fb-app .et-fb-skeleton--section > .et-fb-skeleton__header .et-fb-button, #et-boc #et-fb-app .et_pb_section .et-fb-section-button-wrap--add .et-fb-button, #et-boc #et-fb-app .et-fb-component-settings--section .et-fb-button, #et-boc #et-fb-app .et-fb-component-settings--section .et-fb-button:hover, #et-boc #et-fb-app .et-fb-component-settings--section .et-fb-button-group, .et-db #et-boc .et-fb-outline--section, .et-db #et-boc .et-core-control-toggle--on, .et-db #et-boc .et-fb-settings-custom-select-wrapper.et-fb-settings-option-select-active .select-option-item-hovered, .et-db #et-boc #et-fb-app .et-fb-modal .et-fb-modal__footer .et-fb-button--info, a.et-pb-modal-preview-template, a.et-pb-modal-preview-template:hover, .et-db #et-boc .et-l #et-fb-app .et-fb-button.et-fb-button--elevate.et-fb-button--info.et-fb-button--save-draft, .et-db #et-boc .et_pb_root--vb .et-fb-page-settings-bar .et-fb-button--save-draft, .et-db #et-boc .et-fb-button-group .et-fb-button--save-draft, .et-builder-exit-modal .et_pb_prompt_buttons .et-core-modal-action:not(.et-core-modal-action-secondary), .et-db #et-boc .et-l .et-fb-button-group--info, .et-fb-button--info, .et-core-modal-action:hover, .et-core-modal-action:focus, .et-core-modal-action:active, .et-common-vertical-menu__option-button:focus, .et-common-vertical-menu__option-button:hover, .et_pb_yes_no_button.et_pb_on_state, #et_support_center .et_documentation_videos_list .active, #et_support_center .et_documentation_videos_list .dashicons-arrow-right, .et-db #et-boc .et-l #et-fb-app .et-fb-button--info:hover, .et-db #et-boc .et-l #et-fb-app .et-fb-buttonbutton--info, .et-db #et-boc .et-l #et-fb-app .et-fb-buttonbutton--info:hover',
						'property' => 'background-color'
					),
					array(
						'selector'  => '.et-db #et-boc .et-l .et-fb-settings-tab-title-active, .et-db #et-boc .et-l .et-fb-settings-tab-title:hover, .et-db #et-boc .et-l .et-fb-form__toggle.et-fb-form__toggle-opened h3, #panel-wrap #epanel p.postinfo .jquery-checkbox-checked .mark:before, .et-db #et-boc .et-l .et-fb-modules-list ul>li:before, #et_support_center .et-support-center-article a:before, #et_support_center .et_card_cta a, #et_support_center .et_documentation_videos_list .active .dashicons-arrow-right',
						'property'  => 'color',
						'important' => true
					),
					array(
						'selector' => '.et-db #et-boc .et-l .et-fb-preset-container .et-fb-preset.et-fb-preset--active, .et-db #et-boc .et-l .et-fb-preset-container .et-fb-preset:hover , .et-db #et-boc .et-l .et-fb-shortcut-subtitle, .et-db #et-boc .et-l .et-fb-settings-background-tab-nav--active, .et-db #et-boc .et-l .et-fb-settings-color-manager__switch--active, .et-db #et-boc .et-l .et-fb-settings-color-manager__switch--active',
						'property' => 'color'
					),
					array(
						'selector'  => '.et-db #et-boc .et-l .et-fb-settings-border-radius-preview',
						'property'  => 'border-color',
						'important' => true
					),
					array(
						'selector'  => '.et-db #et-boc .et-l #et-fb-app .et-fb-button--info, .et-db #et-boc .et-l #et-fb-app .et-fb-button--info:hover',
						'property'  => 'background',
						'important' => true
					)
				);
				break;
			
			// green - 3rd accent
			case "3rd_accent_color":
				return array(
					array(
						'selector' => '#panel-wrap .et-save-button, .et-admin-page  .reset-popup-overlay #epanel-reset, #et-theme-builder .et-tb-admin-save-button, .et-common-prompt__modal .et-common-button--secondary, .et-tb-branded-modal  .et-common-button--secondary, #et_pb_save_roles:hover, #et_pb_save_roles, .et-db #et-boc .et-l .et-fb-modal-add-module-container div.et_pb_variation, .et-db #et-boc .et-l #et-fb-app .et-fb-button--success, .et-db #et-boc .et-l #et-fb-app .et-fb-button--success:hover, .et-db #et-boc .et-l #et-fb-app .et-fb-buttonbutton--success, .et-db #et-boc .et-l #et-fb-app .et-fb-buttonbutton--success:hover, .et-db #et-boc .et-l .et-fb-tooltip-modal--progress-chart',
						'property' => 'background-color',
					),
					
					array(
						'selector' => '.et-tb-admin-modals-portal .et-tb-branded-modal .et-common-checkbox__input[type=checkbox]:checked:before, .et-tb-template-settings-sublist .et-common-checkbox__input[type=checkbox]:checked:before, #panel-wrap #epanel p.inputs .jquery-checkbox .mark, .et-db #et-boc .et-l .et-fb-checkboxes-category-wrap input[type=checkbox]:checked:before',
						'property' => 'color',
					),
					
					// orange
					array(
						'selector' => '.et-db #et-boc .et-l .et-fb-component-error__button.et-fb-button:first-child',
						'property' => 'box-shadow'
					),
					array(
						'selector' => '.et-db #et-boc .et-l .et-fb-component-error__heading, .et-db #et-boc .et-l .et-fb-component-error',
						'property' => 'color'
					),
					array(
						'selector' => '.et-db #et-boc .et-l .et-fb-component-error, .et-db #et-boc .et-l .et-fb-wireframe-sortable-placeholder--row,.et-db #et-boc .et-l .et-fb-wireframe-sortable-placeholder--row-inner',
						'property' => 'border-color'
					),
					array(
						'selector' => '.et-db #et-boc #et-fb-app .et-fb-skeleton--row .et_pb_column_empty .et-fb-button, .et-fb-button.et-fb-help-button .et-fb-icon.et-fb-icon--help-circle',
						'property' => 'fill'
					),
					array(
						'selector'  => '.et-db #et-boc .et-l #et-fb-app button.et-fb-button.et-fb-button--success, .et-db #et-boc .et-fb-button-group .et-fb-button--publish, .et-db #et-boc .et-l #et-fb-app .et-fb-button.et-fb-button--elevate.et-fb-button--success.et-fb-button--publish, .et-db #et-boc .et_pb_root--vb .et-fb-page-settings-bar .et-fb-button--publish, .et-db #et-boc .et-fb-button-group .et-fb-button--publish, .et-db #et-boc .et-l .et-fb-component-error__button.et-fb-button, .et-builder-exit-modal .et_pb_prompt_buttons .et-core-modal-action-secondary, .et-db #et-boc .et-l #et-fb-app .et-fb-button--success, .et-db #et-boc .et-l #et-fb-app .et-fb-button--success:hover, .et-db #et-boc .et-l #et-fb-app .et-fb-buttonbutton--success, .et-db #et-boc .et-l #et-fb-app .et-fb-buttonbutton--success:hover .et-db #et-boc #et-fb-app .et-fb-modal .et-fb-modal__footer .et-fb-button--block.et-fb-button--success, .et-db #et-boc .et_fb_save_module_modal a.et-fb-save-library-button, a.et-pb-modal-save, a.et-pb-modal-save:hover, .et_pb_prompt_modal .et_pb_prompt_buttons input.et_pb_prompt_proceed, .et-db #et-boc #et-fb-app .et-fb-button--block.et-fb-button--success, .et-db #et-boc .et-l .et-fb-modal__quick-actions .et-fb-quick-actions--item--hovered div, .et-db #et-boc .et-l .et-core-control-toggle--on, .et-core-modal-action-secondary:hover, .et-core-modal-action-secondary:focus, .et-core-modal-action-secondary:active, .et-db #et-boc .et-fb-skeleton--row > .et-fb-skeleton__header, .et-db #et-boc .et-fb-outline--row, .et-db #et-boc #et-fb-app .et-fb-row-button-wrap--add .et-fb-button, .et-db #et-boc #et-fb-app .et-fb-component-settings--row .et-fb-button, .et-db #et-boc #et-fb-app .et-fb-component-settings--row .et-fb-button-group, .et_pb_row .et-pb-controls,  .et-db #et-boc .et-fb-columns-layout li .column-block-wrap .column-block,  .et-pb-column-layouts li:hover .et_pb_layout_column',
						'property'  => 'background-color',
						'important' => true
					),
					
					array(
						'selector' => '.et-db #et-boc .et-l .et-fb-modal-settings--modules_all.et-fb-modal-add-module-container.et-fb-modal-settings--blue-bottom:after',
						'property' => 'background'
					),
				);
				break;
			
			//orange
			case "4rd_accent_color":
				return array(
					array(
						'selector' => '#panel-wrap #epanel p.inputs:not(.different) .jquery-checkbox-checked .mark:before, #epanel p.inputs.different .jquery-checkbox:not(.jquery-checkbox-checked) .mark:before',
						'property' => 'color'
					),
					array(
						'selector' => '.et-db #et-boc .et-l .et-fb-modal-add-module-container.et-fb-modal-settings--modules_all li.et_fb_specialty_layout_item.et-fb-selected-item .et_pb_specialty_column, .et-db #et-boc .et-l .et-fb-modal-settings--modules_all.et-fb-modal-add-module-container li.et_fb_specialty_layout_item:hover .et_pb_specialty_column, .et-db #et-boc .et-l .et-fb-modal-add-module-container.et_fb_add_section_modal .et-fb-settings-options.et-fb-settings-options-tab-modules_all li.et_fb_specialty:after, .et-db #et-boc .et-l .et-fb-modal-add-module-container.et_fb_add_section_modal .et-fb-settings-options.et-fb-settings-options-tab-modules_all li.et_fb_specialty, .et-db #et-boc #et-fb-app .et-fb-modal .et-fb-modal__footer .et-fb-button--danger, .et-db #et-boc .et-l .et-fb-modal-add-module-container.et_fb_add_section_modal .et-fb-settings-options-tab-modules_all li.et_fb_specialty, .et-db #et-boc .et-l .et-fb-modal-add-module-container.et_fb_add_section_modal .et-fb-settings-options-tab-modules_all li.et_fb_specialty:after, .et-db #et-boc .et-l .et-fb-modal-add-module-container.et-fb-modal-settings--modules_all .et_pb_specialty_column',
						'property' => 'background-color'
					)
				);
				break;
			
			case "5th_accent_color":
				return array(
					array(
						'selector'  => '.et-db #et-boc .et-l #et-fb-app .et-fb-button--globalitem, .et-db #et-boc .et-l #et-fb-app .et-fb-button--globalitem:hover, .et-db #et-boc .et-l #et-fb-app .et-fb-buttonbutton--globalitem, .et-db #et-boc .et-l #et-fb-app .et-fb-buttonbutton--globalitem:hover, .et-db #et-boc .et-l #et-fb-app .et-fb-button--globalitem, .et-db #et-boc .et-l #et-fb-app .et-fb-button--globalitem:hover, .et-db #et-boc .et-l #et-fb-app .et-fb-buttonbutton--globalitem, .et-db #et-boc .et-l #et-fb-app .et-fb-buttonbutton--globalitem:hover, .et-db #et-boc .et-l .et-fb-skeleton--global>.et-fb-skeleton__header, #et-theme-builder .et-tb-layout--global, .et-tb-branded-modal  .et-tb-history-point--active, .et-db #et-boc .et-l #et-fb-app .et-fb-buttonbutton--inverse, .et-db #et-boc .et-l #et-fb-app .et-fb-buttonbutton--inverse:hover, .et-fb-modal-history .et-fb-modal__content button.et-fb-button.et-fb-button--inverse',
						'property'  => 'background-color',
						'important' => true,
					),
					array(
						'selector' => '.et-db #et-boc .et-l .et-fb-modal-add-module-container .et-fb-main-settings .et-fb-settings-options-tab .et_fb_global',
						'property' => 'background-color',
					)
				);
				break;
			
			case "6th_accent_color":
				return array(
					array(
						'selector'  => '#et-theme-builder .et-tb-layout--custom, .et-core-button, body button.et-core-button, .et-db #et-boc .et-fb-skeleton--module > .et-fb-skeleton__header, .et-db #et-boc #et-fb-app .et-fb-component-settings--module .et-fb-button, .et-db #et-boc #et-fb-app .et-fb-component-settings--module .et-fb-button:hover, .et-db #et-boc #et-fb-app .et-fb-module-button-wrap--add .et-fb-button, .et-db #et-boc #et-fb-app .et_pb_row > .et_pb_column_empty .et-fb-button, .et_pb_module_block, .et-db #et-boc .et-pb-draggable-spacing__sizing, .et-db #et-boc #et-fb-app .et-fb-modal.et-fb-modal--app .et-fb-modal__header, .et-db #et-boc #et-fb-app .et-fb-modal.et-fb-modal--app .et-fb-modal__header .et-fb-button, .et-db #et-boc .et-fb-button-group--responsive-mode, .et-db #et-boc .et-fb-button-group--builder-mode, .et-db #et-boc #et-fb-app .et-fb-button-group--responsive-mode .et-fb-button, .et-db #et-boc #et-fb-app .et-fb-button-group--builder-mode .et-fb-button, .et-db #et-boc #et-fb-app .et-fb-button-group--responsive-mode .et-fb-button:hover, .et-db #et-boc #et-fb-app .et-fb-button-group--builder-mode .et-fb-button:hover, .et-db #et-boc .et-l .et-fb-button-group--inverse, .et-db #et-boc .et-l #et-fb-app .et-fb-button--inverse, .et-db #et-boc .et-l #et-fb-app .et-fb-button--inverse:hover ',
						'property'  => 'background-color',
						'important' => true,
					)
				);
				break;
			
			default:
				return array();
				break;
		}
	}
	
	
}

add_action( 'init', array( 'DiviGhosterCustomBuilder', 'setup' ) );
