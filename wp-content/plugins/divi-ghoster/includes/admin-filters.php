<?php
/*
This file includes code copied from and/or based on parts of the Divi theme
and/or the Divi Builder by Elegant Themes, released under GPLv2+, used under GPLv3 (see ../license.txt file for license text).
*/

class DiviGhosterAdminFilters {
	
	private static $updatesPageScreenhotsMatch;
	
	public static function setup() {
		add_filter( 'show_admin_bar', array( 'DiviGhosterAdminFilters', 'hideAdminBar' ), 10 );
		add_action( 'admin_bar_menu', array( 'DiviGhosterAdminFilters', 'filterAdminBarMenu' ), 9999 );
		add_action( 'et_fb_enqueue_assets', array( 'DiviGhosterAdminFilters', 'fbAssets' ), 9 );
		
		add_filter( 'option_et_bfb_settings', array( 'DiviGhosterAdminFilters', 'filterWpOption' ), 10, 2 );
		add_action( 'wp_before_admin_bar_render', array( 'DiviGhosterAdminFilters', 'modifyAdminBar' ), 0 );
		
		if ( isset($_GET['welcome_page']) && $_GET['welcome_page'] == 'custom_dashboard' ) {
			add_action( 'parse_query', array( 'DiviGhosterAdminFilters', 'filterDashboardWidgetQuery' ), 10, 999999 );
		}
		
		if ( DiviGhoster::$settings['ultimate_ghoster'] === 'yes' && '1' === (string) DiviGhoster::$settings['hide_product_tour'] ) {
			add_action( 'wp_head', array( 'DiviGhosterAdminFilters', 'turnOffProductTour' ), 9 );
			add_action( 'et_fb_enqueue_assets', array( 'DiviGhosterAdminFilters', 'turnOffProductTour' ), 9 );
			if (isset($_GET['et_fb'])) {
				add_action( 'wp_head', array( 'DiviGhosterAdminFilters', 'hideProductTourCss' ) );
			}
			add_action( 'admin_head', array( 'DiviGhosterAdminFilters', 'hideProductTourCss' ) );
			add_action( 'wp_footer', array( 'DiviGhosterAdminFilters', 'hideProductTourInDiviFrontBuilder' ) );
		}
		if ( is_admin() ) {
			if ( !wp_doing_ajax() ) {
				add_action( 'admin_init', array( 'DiviGhosterAdminFilters', 'modifyAdminThemePage' ) );
				add_action( 'admin_menu', array( 'DiviGhosterAdminFilters', 'modifyAdminMenu' ), 9999 );
				add_action( 'admin_head', array( 'DiviGhosterAdminFilters', 'adminCssJs' ) );
				add_action( 'wp_dashboard_setup', array( 'DiviGhosterAdminFilters', 'modifyWidgets' ) );
				add_action( 'et_pb_before_page_builder', array( 'DiviGhosterAdminFilters', 'builderScript' ) );
				add_filter( 'display_post_states', array( 'DiviGhosterAdminFilters', 'filterPostStates' ), 9999 );
				add_filter( 'et_builder_settings_definitions', array(
					'DiviGhosterAdminFilters',
					'filterThemeOptionsDefinitionsBuilder'
				) );
				
				add_filter( 'wp_prepare_themes_for_js', array( __CLASS__, 'addCacheBustingToJsThemes' ) );
				
				global $pagenow;
				if ( $pagenow === 'update-core.php') {
					self::$updatesPageScreenhotsMatch = [];
					foreach ( DiviGhosterAdminAjax::getThemesForImageChange() as $themePath ) {
						$matchEscaped = esc_url( basename(dirname($themePath)).'/'.basename($themePath).'/screenshot.' );
						$protocolEnd = strpos($matchEscaped, '://');
						self::$updatesPageScreenhotsMatch[] = $protocolEnd === false ? $matchEscaped : substr($matchEscaped, $protocolEnd + 3);
					}
					
					add_filter( 'clean_url', array( __CLASS__, 'addCacheBustingToUpdatesTheme' ) );
				}
				
			}
			
			self::addTranslateFilters();
			
			
			global $pagenow;
			
			if ( $pagenow == 'index.php' && ( ! empty( DiviGhoster::$settings['widget_layout'] ) ) ) {
				add_filter( 'et_builder_should_load_framework', array(
					'DiviGhosterAdminFilters',
					'loadDiviIfDashboard'
				) );
				
                remove_action( 'welcome_panel', 'wp_welcome_panel' );
				add_action( 'welcome_panel', array('DiviGhosterAdminFilters', 'customWidget'));
				add_action( 'admin_notices', array('DiviGhosterAdminFilters', 'maybeShowNonAdminWelcomePanel'));
			}
			
			// Remove the translate filters before the plugins list (will be restored after the plugins list)
			if ( ( isset( $pagenow ) && $pagenow == 'plugins.php' ) ) {
				add_action( 'load-plugins.php', array( 'DiviGhosterAdminFilters', 'removeTranslateFilters' ) );
			}
			// Show portability button on Theme Options page
            // phpcs:ignore WordPress.Security.NonceVerification.Recommended
			if ( isset( $_GET['page'] ) && $_GET['page'] == 'et_' . DiviGhoster::$settings['theme_slug'] . '_options' ) {
				add_filter( 'et_core_portability_args_epanel', array(
					'DiviGhosterAdminFilters',
					'filterPortabilityArgs'
				) );
			}

			if ( DiviGhoster::$settings['hide_wp_footer'] == '1' ) {
				add_filter( 'admin_footer_text', array( 'DiviGhosterAdminFilters', 'modifyAdminFooter' ), 9999 );
			}
			
			
			// Disable product tour (help videos) on 1st launch of new page/post.
			// Also hide this option in the Builder/Advanced tab on Theme Options page.
			$product_tour_status_override = apply_filters( 'et_builder_product_tour_status_override', 'on' );
		}
		
		add_filter( 'admin_body_class', array( 'DiviGhosterAdminFilters', 'filterAdminBodyClasses' ), 99 );
	}
	

	
	static function addCacheBustingToJsThemes($jsThemes) {
		
		$themes = array_map( 'basename', DiviGhosterAdminAjax::getThemesForImageChange() );
		
		foreach ( $themes as $theme ) {
			if ( isset($jsThemes[$theme]['screenshot']) && is_array($jsThemes[$theme]['screenshot']) ) {
				$jsThemes[$theme]['screenshot'] = array_map( array(__CLASS__, 'addCacheBustingToUrl'), $jsThemes[$theme]['screenshot'] );
			}
		}
		return $jsThemes;
	}
	
	static function addCacheBustingToUpdatesTheme($url) {
		
		foreach ( self::$updatesPageScreenhotsMatch as $matchStr ) {
			if ( strpos($url, $matchStr) ) {
				return self::addCacheBustingToUrl($url);
			}
		}
		return $url;
	}
	
	static function addCacheBustingToUrl($url, $escaped=true) {
		return $url.( strpos($url, '?') === false ? '?' : ( $escaped ? '&amp;' : '&' ) ).'t='.time();
	}
	
	static function filterAdminBodyClasses($existingClasses) {
		global $hook_suffix;
		
		if ( isset($hook_suffix) ) {
			$pagePos = strpos($hook_suffix, '_page_');
			if ($pagePos) {
				if (substr($hook_suffix, 0, 9) == 'toplevel_') {
					$newClass = $hook_suffix;
				} else {
						$newClass = DiviGhoster::$targetThemeSlug.substr($hook_suffix, $pagePos);
						$pagePos = strlen(DiviGhoster::$targetThemeSlug);
				}
			}
			if (isset($newClass)) {
				if ( substr( $newClass, $pagePos + 6, 3 + strlen( DiviGhoster::$settings['theme_slug'] ) ) == 'et_'.DiviGhoster::$settings['theme_slug'] ) {
					$newClass = substr($newClass, 0, $pagePos + 9)
									. DiviGhoster::$targetThemeSlug
									. substr( $newClass, $pagePos + 9 + strlen( DiviGhoster::$settings['theme_slug'] ) );
				}
				$existingClasses .= ' '.$newClass;
			}
		}
		
		return $existingClasses;
	}
	
	public static function fbAssets() {
		// "Translate" frontend builder strings
		if ( has_action( 'et_fb_enqueue_assets', 'et_fb_backend_helpers' ) && function_exists( 'et_fb_backend_helpers' ) ) {
			remove_action( 'et_fb_enqueue_assets', 'et_fb_backend_helpers' );
			add_filter( 'gettext', array( 'DiviGhosterAdminFilters', 'filterTranslatedTextDiviOrExtra' ) );
			add_filter( 'ngettext', array( 'DiviGhosterAdminFilters', 'filterTranslatedTextDiviOrExtra' ) );
			et_fb_backend_helpers();
			remove_filter( 'gettext', array( 'DiviGhosterAdminFilters', 'filterTranslatedTextDiviOrExtra' ) );
			remove_filter( 'ngettext', array( 'DiviGhosterAdminFilters', 'filterTranslatedTextDiviOrExtra' ) );
		}
		add_action( 'wp_footer', array( 'DiviGhosterAdminFilters', 'builderScript' ), 999 );
		
	}
	
	public static function builderScript() {
		// Script to reset the export file name
		?>
        <script type="text/javascript">
            jQuery(document).ready(function ($) {
                // Following code from WP and Divi Icons Pro (fb.js) - modified

                var MO = window.MutationObserver ? window.MutationObserver : window.WebkitMutationObserver;
                var fbApp = document.getElementById('et-fb-app');
                if (!fbApp) {
                    fbApp = document.body;
                }
                if (MO && fbApp) {
                    (new MO(function (events) {
						<?php if ( DiviGhoster::$settings['ultimate_ghoster'] === 'yes' && (string) DiviGhoster::$settings['hide_premade_layouts'] === '1' ) : ?>
                        //
						<?php endif; ?>
                        $.each(events, function (i, event) {
                            if (event.addedNodes && event.addedNodes.length) {
                                $.each(event.addedNodes, function (i, node) {
                                    var $exportFileNameField = $(node).find('#et-fb-exportFileName');
                                    if ($exportFileNameField.length) {
                                        $exportFileNameField.val($exportFileNameField.val().replace('<?php echo( esc_attr( addslashes(  DiviGhoster::$targetTheme ) ) ); ?>', '<?php echo esc_attr( addslashes( DiviGhoster::$settings['branding_name'] ) ); ?>'));
                                    }
                                });
                            }
                        });
                    })).observe(fbApp, {childList: true, subtree: true});
                }

                // End code from WP and Divi Icons Pro
            });
        </script>
		<?php
	}
	
	public static function filterAdminBarMenu( $admin_bar ) {
		$visualBuilderNode = $admin_bar->get_node( 'et-use-visual-builder' );
		if ( ! empty( $visualBuilderNode ) ) {
			$admin_bar->remove_node( 'et-use-visual-builder' );
			$visualBuilderNode       = get_object_vars( $visualBuilderNode );
			$visualBuilderNode['id'] = 'agsdg-et-use-visual-builder';
			$admin_bar->add_node( $visualBuilderNode );
		}
	}
	
	public static function filterPortabilityArgs( $args ) {
		
		$args->view = true;
		
		return $args;
	}
	
	
	public static function addTranslateFilters() {
		add_filter( 'gettext', array( 'DiviGhosterAdminFilters', 'filterTranslatedText' ) );
		add_filter( 'ngettext', array( 'DiviGhosterAdminFilters', 'filterTranslatedText' ) );
		
		add_filter( 'load_script_translations', array( 'DiviGhosterAdminFilters', 'filterScriptTranslations' ), 10, 2 );
	}
	
	public static function removeTranslateFilters() {
		remove_filter( 'gettext', array( 'DiviGhosterAdminFilters', 'filterTranslatedText' ) );
		remove_filter( 'ngettext', array( 'DiviGhosterAdminFilters', 'filterTranslatedText' ) );
	}
	
	public static function filterScriptTranslations($jsonStrings, $translationsFile) {
		if ( strpos($translationsFile, '/'.DiviGhoster::$targetTheme.'/') !== false ) {
			$jsonStringsDecoded = json_decode($jsonStrings, true);
			if ( isset($jsonStringsDecoded['locale_data']) ) {
				foreach ( $jsonStringsDecoded['locale_data'] as $textDomain => &$translations ) {
					foreach ($translations as $original => &$translated) {
						if ( isset($translated[0]) ) {
							foreach ($translated as &$translation) {
								$filtered = self::filterTranslatedText($translation ? $translation : $original);
								if ( $filtered != ($translation ? $translation : $original) ) {
									$translation = $filtered;
								}
							}
						}
					}
				}
				$jsonStrings = wp_json_encode( $jsonStringsDecoded );
			}
		}
		return $jsonStrings;
	}
	
	public static function modifyAdminThemePage() {
		global $pagenow, $plugin_page;
		//  remove Divi Page if option is set
		if ( DiviGhoster::$settings['hide_divi_page'] == 1 ) {
			if ( ( $pagenow != 'admin.php' || $plugin_page != 'wtfdivi_settings' ) || ( $pagenow != 'admin.php' || $plugin_page != 'wp-and-divi-icons-pro' ) || ( $pagenow != 'admin.php' || $plugin_page != 'et_support_center_divi' ) || ( $pagenow != 'customize.php' || $plugin_page != 'et_customizer_option_set=theme' ) || ( $pagenow != 'admin.php' || $plugin_page != 'et_theme_builder' ) || ( $pagenow != 'admin.php' || $plugin_page != 'et_' . DiviGhoster::$settings['theme_slug'] . '_role_editor' ) || ( $pagenow != 'admin.php' || $plugin_page != 'ds-icon-expansion' ) || ( $pagenow != 'admin.php' || $plugin_page != 'et_divi_options' ) || ( $pagenow != 'edit.php' || $plugin_page != 'et_pb_layout' ) || ( $pagenow != 'admin.php' || $plugin_page != 'aspen-footer-editor' ) || ( $pagenow != 'admin.php' || $plugin_page != 'ds-dbreadcrumbs' ) || ( $pagenow != 'admin.php' || $plugin_page != 'ds-icon-party' ) ) {
				remove_menu_page( 'et_' . DiviGhoster::$settings['theme_slug'] . '_options' );
			}
		}
		
	}
	
	public static function modifyAdminMenu() {
		
		global $menu, $submenu, $pagenow, $plugin_page;
		
		// Add Ghoster menu item
		if ( DiviGhoster::$settings['ultimate_ghoster'] != 'yes' || ( $pagenow == 'admin.php' && $plugin_page == 'divi_ghoster' ) ) {
			add_menu_page( __( 'Divi', 'divi-ghoster' ) . ' Ghoster', __( 'Divi', 'divi-ghoster' ) . ' Ghoster', 'manage_options', 'divi_ghoster', array(
				'DiviGhosterAdmin',
				'menuPage'
			) );
		}
		// TODO fix issue with Extra theme. DiviGhoster::$targetThemeSlug is always -> divi. It should be extra when theme is switched
		if ( DiviGhoster::$settings['ultimate_ghoster'] === 'yes' ) {
			if ( function_exists( 'wp_get_theme' ) ) {
				$theme = strtolower( wp_get_theme()->get_template() );
				
				if ( 'extra' === $theme ) {
					DiviGhoster::$targetThemeSlug = $theme;
				}
			}
		}
		
		// Loop through and find the Divi or Extra admin menu
		foreach ( $menu as $menuItem ) {
			if ( $menuItem[2] == 'et_' . DiviGhoster::$targetThemeSlug . '_options' ) {
				$menuExists = true;
				break;
			}
		}
		
		// If we didn't find a target theme submenu then return (neither theme active)
		if ( empty( $menuExists ) ) {
			return;
		}
		
		
		// Following code copied from the Divi theme and modified - Divi/functions.php
		// Add Theme Options menu only if it's enabled for current user
		if ( et_pb_is_allowed( 'theme_options' ) ) {

			if ( isset( $_GET['page'] ) && 'et_' . DiviGhoster::$settings['theme_slug'] . '_options' === $_GET['page'] && isset( $_POST['action'] ) ) {
				if (
					// phpcs:ignore ET.Sniffs.ValidatedSanitizedInput.InputNotSanitized -- not sanitizing nonces since they are verified here anyways
					( isset( $_POST['_wpnonce'] ) && wp_verify_nonce( $_POST['_wpnonce'], 'epanel_nonce' ) )
					||
					// phpcs:ignore ET.Sniffs.ValidatedSanitizedInput.InputNotSanitized -- not sanitizing nonces since they are verified here anyways
					( 'reset' === $_POST['action'] && isset( $_POST['_wpnonce_reset'] ) && wp_verify_nonce( $_POST['_wpnonce_reset'], 'et-nojs-reset_epanel' ) )
				) {
					epanel_save_data( 'js_disabled' ); //saves data when javascript is disabled
				}
			}
		}
		// End code from Divi/functions.php
		
		
		global $_wp_submenu_nopriv, $_parent_pages, $_registered_pages, $wp_filter, $wp, $post_type;
		
		
		// Remove certain menu items if Ultimate Ghoster is enabled
		if ( DiviGhoster::$settings['ultimate_ghoster'] == 'yes' ) {
			
			switch ( DiviGhoster::$targetTheme ) {
				
				
				case 'Divi':
				case 'Extra':
					
					// Remove Divi Booster
					if ( $pagenow != 'admin.php' || $plugin_page != 'wtfdivi_settings' ) {
						remove_submenu_page( 'et_divi_options', 'wtfdivi_settings' );
					}
					// Remove Aspen Footer Editor
					if ( $pagenow != 'admin.php' || $plugin_page != 'aspen-footer-editor' ) {
						remove_submenu_page( 'et_divi_options', 'aspen-footer-editor' );
					}
					// Remove Divi Breadcrumbs
					if ( $pagenow != 'admin.php' || $plugin_page != 'ds-dbreadcrumbs' ) {
						remove_submenu_page( 'et_divi_options', 'ds-dbreadcrumbs' );
					}
					// Remove Divi Extras
					if ( $pagenow != 'admin.php' || $plugin_page != 'ds-divi-extras' ) {
						remove_submenu_page( 'et_divi_options', 'ds-divi-extras' );
					}
					// Remove Divi Icon Party
					if ( $pagenow != 'admin.php' || $plugin_page != 'ds-icon-party' ) {
						remove_submenu_page( 'et_divi_options', 'ds-icon-party' );
					}
					// Remove WP and Divi Icons Pro
					if ( $pagenow != 'admin.php' || $plugin_page != 'wp-and-divi-icons-pro' ) {
						remove_submenu_page( 'et_divi_options', 'wp-and-divi-icons-pro' );
					}
					// Remove WP & Divi Icons
					if ( $pagenow != 'admin.php' || $plugin_page != 'ds-icon-expansion' ) {
						remove_submenu_page( 'et_divi_options', 'ds-icon-expansion' );
					}
					
					// Remove Divi Switch
					if ( $pagenow != 'admin.php' || $plugin_page != 'divi_switch' ) {
						remove_submenu_page( 'et_divi_options', 'admin.php?page=et_divi_options#wrap-swtch_tab' );
					}
					
					// Remove Divi Rocket
					if ( $pagenow != 'admin.php' || $plugin_page != 'divi-rocket-settings' ) {
						remove_submenu_page( 'et_divi_options', '   divi-rocket-settings' );
					}
					
					// Remove Page Builder Everywhere
					if ( $pagenow != 'admin.php' || $plugin_page != 'ds-page-builder-everywhere' ) {
						remove_submenu_page( 'et_divi_options', 'ds-page-builder-everywhere' );
					}
					
					// Remove Divi Overlays
					if ( $pagenow != 'edit.php' || $plugin_page != 'divi_overlay' ) {
						remove_menu_page( 'edit.php?post_type=divi_overlay' );
					}
					
					
					//                   // Remove Divi Overlays
					//                    if ($pagenow != '$admin.php' ||  $plugin_page != 'divi_overlay') {
					//                        remove_menu_page( 'edit.php?post_type=divi_overlay');
					//                    }
					
					break;
				//              case 'Extra':
				//                  // Remove Divi Switch
				//                  if ($pagenow != 'admin.php' || $plugin_page != 'divi-switch-settings') {
				//                      remove_submenu_page('et_divi_options', 'admin.php?page=divi-switch-settings');
				//                  }
				//                  // Remove Divi Booster
				//                  if ($pagenow != 'admin.php' || $plugin_page != 'wtfdivi_settings') {
				//                      remove_submenu_page('et_divi_options', 'wtfdivi_settings');
				//                  }
				//                  // Remove Aspen Footer Editor
				//                  if ($pagenow != 'admin.php' || $plugin_page != 'aspen-footer-editor') {
				//                      remove_submenu_page('et_divi_options', 'aspen-footer-editor');
				//                  }
				//                    // Remove role editor if not on that page
				//                  if ($pagenow != 'admin.php' || $plugin_page != 'et_' . DiviGhoster::$settings['theme_slug'] . '_role_editor') {
				//                        remove_submenu_page('et_divi_options', 'et_divi_role_editor');
				//                  }
				//                    // Remove Support Center if not on that page
				//                  if ($pagenow != 'admin.php' || $plugin_page != 'et_support_center_' . DiviGhoster::$settings['theme_slug'] ) {
				//                        remove_submenu_page('et_divi_options', 'et_support_center_divi');
				//                  }
				//                  break;
			}
			
		}
		
		
		// Remove Divi Options Page
		if ( DiviGhoster::$settings['hide_divi_options_page'] == '1' && ( $pagenow != 'admin.php' || $plugin_page != 'et_divi_options' ) ) {
			remove_submenu_page( 'et_divi_options', 'et_divi_options' );
		}
		
		// Remove Divi Library Page
		if ( DiviGhoster::$settings['hide_library_page'] == '1' && ( $pagenow != 'edit.php' || $plugin_page != 'et_pb_layout' ) ) {
			remove_submenu_page( 'et_divi_options', 'edit.php?post_type=et_pb_layout' );
		}
		
		// Remove Theme Builder Page
		if ( DiviGhoster::$settings['hide_theme_page'] == '1' && ( $pagenow != 'admin.php' || $plugin_page != 'et_theme_builder' ) ) {
			remove_submenu_page( 'et_divi_options', 'et_theme_builder' );
		}
		
		// Remove role editor if not on that page
		if ( DiviGhoster::$settings['hide_editor_page'] == '1' && ( $pagenow != 'admin.php' || $plugin_page != 'et_' . DiviGhoster::$settings['theme_slug'] . '_role_editor' ) ) {
			remove_submenu_page( 'et_divi_options', 'et_divi_role_editor' );
		}
		
		// Remove Customizer Page
		if ( DiviGhoster::$settings['hide_customizer_page'] == '1' && ( $pagenow != 'customize.php' || $plugin_page != 'et_customizer_option_set=theme' ) ) {
			remove_submenu_page( 'et_divi_options', 'customize.php?et_customizer_option_set=theme' );
		}
		
		// Remove Settings page
		if ( DiviGhoster::$settings['hide_settings_page'] == '1' && ( $pagenow != 'options-general.php' ) ) {
			remove_menu_page( 'options-general.php' );
		}
		
		// Remove Appearance Page
		if ( DiviGhoster::$settings['hide_appearance_page'] == '1' && ( $pagenow != 'themes.php' ) ) {
			remove_menu_page( 'themes.php' );
		}
		// Remove Tools Page
		if ( DiviGhoster::$settings['hide_tools_page'] == '1' && ( $pagenow != 'tools.php' ) ) {
			remove_menu_page( 'tools.php' );
		}
		
		// Remove Projects Page
		if ( DiviGhoster::$settings['hide_projects_page'] == '1' && ( $pagenow != 'edit.php?post_type=project' ) ) {
			remove_menu_page( 'edit.php?post_type=project' );
		}
		
		// Remove Plugins Page
		if ( DiviGhoster::$settings['hide_plugins_page'] == '1' && ( $pagenow != 'plugins.php' ) ) {
			remove_menu_page( 'plugins.php' );
		}
		
		// Remove Support Center if not on that page
		if ( DiviGhoster::$settings['hide_support_page'] == '1' && ( $pagenow != 'admin.php' || $plugin_page != 'et_support_center_divi' ) ) {
			remove_submenu_page( 'et_divi_options', 'et_support_center_divi' );
		}
		
		// Remove Updates Page if not on that page
		if ( DiviGhoster::$settings['hide_updates_page'] == '1' && $plugin_page != 'update-core.php' ) {
			remove_submenu_page( 'index.php', 'update-core.php' );
		}
		
		
		if ( DiviGhoster::$settings['hide_ct_page'] == '1' ) {
			$ctSlugs = array(
				'AGS_child_theme',
				'ags_divitherapist',
				'diviecommerce-options',
				'ags_photography',
				'ags_divipodcast',
				'divibusinesspro-options',
				'ags_divimedical',
				'ags_divicontractor',
				'ags_diviresume',
				'ags_divifitness',
				'ags_dnonprofit'
			);
			foreach ( $ctSlugs as &$slug ) {
				if ( $pagenow != 'admin.php' || $plugin_page != $slug ) {
					remove_menu_page( $slug );
				}
			}
		}
		
		
		// Loop through the main admin menu. Look for 'Divi' or 'Extra' and replace with branding name.
		foreach ( $menu as $i => $menuItem ) {
			if ( $menuItem[2] == 'et_divi_100_options' ) {
				// Change Divi 100 items
                // phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited -- this plugin is designed to override certain things
				$menu[ $i ][0] = DiviGhoster::$settings['branding_name_esc'] . ' Addons';
                // phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited -- this plugin is designed to override certain things
				$menu[ $i ][3] = DiviGhoster::$settings['branding_name_esc'] . ' Addons';
				break;
			}
			
			// Take care of Divi Overlays (only menu/submenu names - slugs not handled at this point)
			if ( class_exists( 'DiviOverlays' ) && $menuItem[2] == 'edit.php?post_type=divi_overlay' ) {
				remove_submenu_page( 'edit.php?post_type=divi_overlay', 'edit.php?post_type=divi_overlay' );
                // phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited -- this plugin is designed to override certain things
				$menu[ $i ][0] = DiviGhoster::$settings['branding_name_esc'] . ' Overlays';
				
				add_submenu_page( 'edit.php?post_type=divi_overlay', DiviGhoster::$settings['branding_name_esc'] . ' Overlays', DiviGhoster::$settings['branding_name_esc'] . ' Overlays', 'edit_posts', 'edit.php?post_type=divi_overlay', '', 0 );
				
			}
		}
		
		// Now, loop through the theme (Divi or Extra) submenu and make substitutions.
		$menuItems = array(
			'et_' . DiviGhoster::$targetThemeSlug . '_options' => array(
				'slug' => 'et_' . DiviGhoster::$settings['theme_slug'] . '_options',
				'name' => DiviGhoster::$settings['branding_name_esc']
			
			)
		);
		
		
		foreach ( $menuItems as $oldSlug => $params ) {
			
			// Add top-level theme pages and copy hooked functions from the old page hooks to the new ones
			$hookName    = add_menu_page( $params['name'], $params['name'], 'edit_theme_options', $params['slug'] );
			$oldHookName = get_plugin_page_hookname( $oldSlug, '' );
			
			foreach ( array( '', 'load-', 'admin_print_scripts-', 'admin_head-' ) as $prefix ) {
				if ( ! empty( $wp_filter[ $prefix . $oldHookName ] ) ) {
					foreach ( $wp_filter[ $prefix . $oldHookName ] as $priority => $hooks ) {
						foreach ( $hooks as $hook ) {
							add_action( $prefix . $hookName, $hook['function'], $priority, $hook['accepted_args'] );
						}
					}
				}
			}
			
			// Copy submenu items
			$newSlug             = plugin_basename( $params['slug'] );
			
			if (isset($submenu[ $oldSlug ])) {
				// phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited -- this plugin is designed to override certain things
				$submenu[ $newSlug ] = $submenu[ $oldSlug ];
				foreach ( $submenu[ $newSlug ] as $i => $subMenuItem ) {
					
					$oldSubmenuSlug = $submenu[ $newSlug ][ $i ][2];
					
					if ( $oldSubmenuSlug == $oldSlug ) {
						// replace theme options link
						// phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited -- this plugin is designed to override certain things
						$submenu[ $newSlug ][ $i ][2] = $params['slug'];
					} else if ( $oldSubmenuSlug == 'et_' . DiviGhoster::$targetThemeSlug . '_role_editor' ) {
						// replace role editor link
						// phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited -- this plugin is designed to override certain things
						$submenu[ $newSlug ][ $i ][2] = 'et_' . DiviGhoster::$settings['theme_slug'] . '_role_editor';
					} else if ( $oldSubmenuSlug == 'admin.php?page=et_' . DiviGhoster::$targetThemeSlug . '_options#wrap-swtch_tab' ) {
						// replace switch link
						// phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited -- this plugin is designed to override certain things
						$submenu[ $newSlug ][ $i ][2] = 'admin.php?page=et_' . DiviGhoster::$settings['theme_slug'] . '_options#wrap-swtch_tab';
					} else if ( $oldSubmenuSlug == 'ds-divi-extras' ) {
						// replace Divi Extras link
						// phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited -- this plugin is designed to override certain things
						$submenu[ $newSlug ][ $i ][2] = 'ds-' . DiviGhoster::$settings['theme_slug'] . '-extras';
					}
					
					// Replace current theme names or slugs with ghosted name/slug for any other plugin menu item within the theme admin submenu
					// Special case for 'Divi Extras' (don't replace 'Extra')
					
					// phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited -- this plugin is designed to override certain things
					if ( $submenu[ $newSlug ][ $i ][0] == 'Divi Extras' || $submenu[ $newSlug ][ $i ][0] == DiviGhoster::$settings['theme_slug'] ) {
						// phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited -- this plugin is designed to override certain things
						$submenu[ $newSlug ][ $i ][0] = self::filterTranslatedTextDiviOnly( $submenu[ $newSlug ][ $i ][0] );
						// phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited -- this plugin is designed to override certain things
						$submenu[ $newSlug ][ $i ][3] = self::filterTranslatedTextDiviOnly( $submenu[ $newSlug ][ $i ][3] );
					} else {
						// phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited -- this plugin is designed to override certain things
						$submenu[ $newSlug ][ $i ][0] = self::filterTranslatedTextDiviOrExtra( $submenu[ $newSlug ][ $i ][0] );
						// phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited -- this plugin is designed to override certain things
						$submenu[ $newSlug ][ $i ][3] = self::filterTranslatedTextDiviOrExtra( $submenu[ $newSlug ][ $i ][3] );
					}
					// phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited -- this plugin is designed to override certain things
					$submenu[ $newSlug ][ $i ][2] = self::filterTranslatedSlugActiveTheme( $submenu[ $newSlug ][ $i ][2] );
					
					// Copy hooked functions from the old submenu page hooks to the new ones
					$oldHookName = get_plugin_page_hookname( $oldSubmenuSlug, $oldSlug );
					$hookName    = get_plugin_page_hookname( $submenu[ $newSlug ][ $i ][2], $params['slug'] );
					foreach ( array( '', 'load-', 'admin_print_scripts-', 'admin_head-' ) as $prefix ) {
						if ( ! empty( $wp_filter[ $prefix . $oldHookName ] ) ) {
							foreach ( $wp_filter[ $prefix . $oldHookName ] as $priority => $hooks ) {
								foreach ( $hooks as $hook ) {
									add_action( $prefix . $hookName, $hook['function'], $priority, $hook['accepted_args'] );
								}
							}
						}
					}
					
					unset( $_registered_pages[ $oldHookName ] );
					// phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited -- this plugin is designed to override certain things
					$_registered_pages[ $hookName ] = 1;
					// phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited -- this plugin is designed to override certain things
					$_parent_pages[ $submenu[ $newSlug ][ $i ][2] ] = $newSlug;
					
				}
			}
			
           
			
			// phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited -- this plugin is designed to override certain things
			if ( isset( $_wp_submenu_nopriv[ $oldSlug ] ) ) {
				// phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited -- this plugin is designed to override certain things
				$_wp_submenu_nopriv[ $newSlug ] = $_wp_submenu_nopriv[ $oldSlug ];
			}
			
			remove_menu_page( $oldSlug );
			unset( $submenu[ $oldSlug ] );
			
		}
		
	}
	
	public static function modifyAdminBar() {
		// hides wordpress icon from wordpress
		if ( DiviGhoster::$settings['hide_wp_icon'] == '1' ) {
			global $wp_admin_bar;
			$wp_admin_bar->remove_menu( 'wp-logo' );
		}
	}
	
	public static function modifyAdminFooter() {
		// hides wordpress footer
		if ( DiviGhoster::$settings['hide_wp_footer'] == '1' ) {
			return ' ';
		}
	}
	
	public static function filterTranslatedText( $translated ) {
		return empty( DiviGhoster::$targetTheme ) ? $translated : preg_replace( ( DiviGhoster::$targetTheme == 'Divi' ? '/(Divi\b)/' : '/(' . DiviGhoster::$targetTheme . '\b|Divi\b)/' ), DiviGhoster::$settings['branding_name'], $translated );
	}
	
	// Translate only Divi
	public static function filterTranslatedTextDiviOnly( $text ) {
		return str_replace( 'Divi', DiviGhoster::$settings['branding_name'], $text );
	}
	
	// Translate Divi, Extra or Aspen
	public static function filterTranslatedTextDiviOrExtra( $text ) {
		$text = str_replace( 'Divi', DiviGhoster::$settings['branding_name'], $text );
		// Do not replace 'Extras' (like in Divi Extras)
		if ( ! strpos( $text, 'Extras' ) ) {
			$text = str_replace( 'Extra', DiviGhoster::$settings['branding_name'], $text );
		}
		$text = str_replace( 'Aspen', DiviGhoster::$settings['branding_name'], $text );
		
		// remove duplicate string (Somehow WHen BRanding Name contains Divi string is duplicated in theme options dropdown
		$text = implode(' ',array_unique(explode(' ', $text)));
		
		return $text;
	}
	
	// Translate slug based on theme name (and include aspen)
	public static function filterTranslatedSlugActiveTheme( $text ) {
		// replace current theme slug (based on active theme stylesheet directory name) with preferred ghoster theme slug
		//  (allow for capitalized theme directory)
		$translatedSlug = str_replace( strtolower( get_option( 'stylesheet' ) ), DiviGhoster::$settings['theme_slug'], $text );
		$translatedSlug = str_replace( 'aspen', DiviGhoster::$settings['theme_slug'], $text );
		
		return $translatedSlug;
		
	}
	
	public static function adminCssJs() {
	   // Do not output the admin CSS if the currently active theme is not the one that Ghoster has been applied to
	   $currentTemplate = wp_get_theme()->get_template();
	   if ($currentTemplate != DiviGhoster::$targetTheme && $currentTemplate != DiviGhoster::$settings['theme_slug']) {
		   return;
	   }
	   
		if ( file_exists( DiviGhosterAdminAjax::$custom_theme_image_dir . 'branding.jpg' ) ) {
			$brandingImage = esc_url( WP_CONTENT_URL . '/uploads/ds-custom-theme-images/branding.jpg?t='.time() );
			
			
		} else if ( file_exists( DiviGhosterAdminAjax::$custom_theme_image_dir . 'branding.png' ) ) {
			$brandingImage = esc_url( WP_CONTENT_URL . '/uploads/ds-custom-theme-images/branding.png?t='.time() );
		} else if ( !empty( DiviGhoster::$settings['branding_image'] ) ) {
			$brandingImage = esc_url( DiviGhoster::$settings['branding_image'] );
		}
	   

	   echo('<style>');

	   if (!empty($brandingImage)) {
		
		
		
		
	   ?>

		   #adminmenu #toplevel_page_et_<?php echo esc_html(DiviGhoster::$settings['theme_slug']); ?>_options div.wp-menu-image::before,
		   #adminmenu #toplevel_page_et_divi_100_options div.wp-menu-image::before {
			   background: url(<?php echo $brandingImage; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped -- escaped previously ?>) no-repeat !important;
			   content:'' !important;
			   margin-top: 6px !important;
			   max-width:22px !important;
			   max-height:22px !important;
			   width: 100%;
			   background-size: contain!important;
		   }
		   #et_pb_layout .hndle:before, #et_pb_toggle_builder:before,
		   .block-editor__container .editor-post-switch-to-divi:before, .gutenberg__editor .editor-post-switch-to-divi:before {
			   color:transparent !important;
			   background: url(<?php echo $brandingImage; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped -- escaped previously ?>) no-repeat  !important;
			   background-size: contain!important;
			   max-height: 33px;
			   max-width: 36px;
			   width: 100%;
		   }

		   #et_pb_layout h3:before
		   {
			   background-image: url(<?php echo $brandingImage; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped -- escaped previously ?>) no-repeat  !important;
		   }
		   #et_settings_meta_box .hndle.ui-sortable-handle::before {
			   color:transparent !important;
			   background: url(<?php echo $brandingImage; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped -- escaped previously ?>) no-repeat !important;
			   max-height: 26px;
			   max-width: 26px;
			   margin: 9px 0px 0px 0px;
			   background-size: contain!important;
		   }
		   #et_settings_meta_box .hndle:before
		   {
			   color:transparent !important;
			   background: url(<?php echo $brandingImage; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped -- escaped previously ?>) no-repeat !important;
			   height:36px;
			   width:36px;
			   margin:6px 0px 0px 0px;
		   }
		   #epanel-logo{
			   content: url(<?php echo $brandingImage; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped -- escaped previously ?>) !important;
			   width:143px;
			   height:65px;
		   }
		   #epanel-title {
			   background-color: transparent !important;
		   }
		   #epanel-title:before {
			   display: none !important;
		   }

		   #epanel-header:before, .et-tb-admin-container-header:before  {
			   display: block;
			   float: left;
			   vertical-align: top;
			   background: url(<?php echo $brandingImage; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped -- escaped previously ?>) no-repeat !important;
			   content: '' !important;
			   width: 32px !important;
			   height: 32px !important;
			   margin-top: -4px;
			   margin-right: 10px;
			   background-size: contain !important;
			   background-position: left 0px center !important;
		   }
		   .divi-ghoster-placeholder-block-icon {
			   background: url(<?php echo $brandingImage; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped -- escaped previously ?>) no-repeat;
			   background-size: contain;
			   background-position: left 0px center;
		   }
		   .wp-block-divi-placeholder .et-icon:before {
			   background: url(<?php echo $brandingImage; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped -- escaped previously ?>) no-repeat;
			   content: '' !important;
			   width: 50px;
			   height: 50px;
			   margin-left: auto;
			   margin-right:auto;
			   background-size: contain;
			   background-position: left 0px center;
		   }
		   .editor-post-switch-to-divi:after {
			   background: url(<?php echo $brandingImage; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped -- escaped previously ?>) no-repeat;
			   content: '' !important;
			   width: 32px;
			   height: 32px;
			   margin-top: -4px;
			   margin-left: -5px;
			   background-size: contain;
			   background-position: left 0px center;
		   }
		   .et_pb_roles_title:before {
			   background: url(<?php echo $brandingImage; // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped -- escaped previously ?>) no-repeat !important;
			   content: '' !important;
			   width: 32px !important;
			   height: 32px !important;
			   background-size: contain !important;
			   background-position: left 0px center !important;
		   }
	   <?php } // /!empty(DiviGhoster::$settings['branding_image'])
	   if (DiviGhoster::$settings['ultimate_ghoster'] == 'yes') {
		   // Output CSS to hide the duplicate theme in the theme editor dropdown ?>
		   body.theme-editor-php #theme option[value="<?php echo esc_attr( DiviGhoster::$settings['theme_slug'] ); ?>"],.et-bfb-optin-cta {
			   display: none;
		   }
		   #wrap-swtch_tab #swtch_tab-11,
		   #wrap-swtch_tab li[aria-controls="swtch_tab-11"] {
			   display: none !important;
		   }
	   <?php } // /ultimate_ghoster == 'yes' ?>
	   </style>

	   <?php
		   if ( 'post.php' == $GLOBALS['pagenow'] || 'post-new.php' == $GLOBALS['pagenow'] ) {
	   ?>
	   <script type="text/javascript">

		   /*
		   This script includes code copied from and/or based on parts of the Divi
		   theme, Copyright Elegant Themes, used under the GNU General Public License version 3 (see
		   <?php echo(esc_url(DiviGhoster::$pluginBaseUrl)); ?>license.txt file for license text).
		   */

		   jQuery(document).ready(function($) {
			   // Following code from WP and Divi Icons Pro (fb.js) - modified

			   var MO = window.MutationObserver ? window.MutationObserver : window.WebkitMutationObserver;
			   var editor = document.getElementById('editor');
			   if (MO && editor) {
				   (new MO(function(events) {
					   $.each(events, function(i, event) {
						   if (event.addedNodes && event.addedNodes.length) {
							   var $newElements = $(event.addedNodes);
							   $newElements.find('.editor-post-switch-to-divi').addBack('.editor-post-switch-to-divi').each(function() {
								   var $button = $(this);
								   $button.html($button.html().replace('Divi', <?php echo(wp_json_encode(DiviGhoster::$settings['branding_name_esc'])); ?>));
							   });
							   $newElements.find('.dashicons-format-image').addBack('.dashicons-format-image').each(function() {
								   var $icon = $(this);
								   /* Parts of the following line were copied from Divi by Elegant Themes and modified */
								   if ($icon.has('path[d^="M7.5,6H7v4h0.5c2.125"]')) {
									   $icon.addClass('divi-ghoster-placeholder-block-icon').empty();
								   }
							   });

						   }
					   });
				   })).observe(editor, {childList: true, subtree: true});
			   }

			   // End code from WP and Divi Icons Pro
		   });
	   </script>
	   <?php } ?>

	   <?php
   }
	
	public static function filterPostStates( $states ) {
		$diviIndex = array_search( 'Divi', $states );
		if ( $diviIndex !== false ) {
			$states[ $diviIndex ] = DiviGhoster::$settings['branding_name_esc'];
		}
		
		return $states;
	}
	
	public static function filterWpOption( $optionValue, $optionName ) {
		switch ( $optionName ) {
			case 'et_bfb_settings':
				if ( DiviGhoster::$settings['ultimate_ghoster'] == 'yes' ) {
					$optionValue['enable_bfb'] = 'on';
				}
				break;
		}
		
		return $optionValue;
	}
	
	public static function filterThemeOptionsDefinitionsBuilder( $options ) {
		if ( DiviGhoster::$settings['ultimate_ghoster'] == 'yes' ) {
			unset( $options['et_enable_bfb'] );
		}
		
		return $options;
	}
	
	public static function filterBuilderHelpVideos() {
		return array();
	}
	
	public static function turnOffProductTour() {
		// Disable product tour
		et_fb_disable_product_tour();
	}
	
	public static function hideProductTourCss() {
		// Hide product tour and help links
		echo '<style>
                #builder-2 .et-epanel-box.et-epanel-box-small-2:nth-of-type(3) {
                        display: none;
                }
                .et-fb-tooltip--product-tour--centered{
                    display: none;
                }
                .et-fb-product-tour-overlay, .et-fb-help-button {
                    display: none !important;
                }
                #et-fb-tooltip-helper-container-custom {
                        display: none;
                }
                </style>';
	}
	
	public static function hideProductTourInDiviFrontBuilder() {
		// Hide product tour
?>
		<script type="text/javascript">
                (function($){
                    $(".et-fb-tooltip--product-tour--centered").remove();
                    $(".et-fb-product-tour-overlay").remove();
                    $("#et-fb-tooltip-helper-container-custom").remove();
					
					$('head:first').append('<style>.et-db #et-boc .et-l .et-fb-modal__tab-footer .et-fb-help-button, .et-db #et-boc .et-l .et_pb_root--vb .et-fb-page-settings-bar .et-fb-button--help, .et-db #et-boc .et-l .et-fb-modal-history .et-fb-help-button { display: none !important; }</style>');
                })(jQuery);
                </script>
<?php
	}
	
	public static function modifyWidgets() {
		global $wp_meta_boxes;
		if ( DiviGhoster::$settings['hide_wp_widgets'] == '1' ) {
			
			remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );
			remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
			remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
			remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );
			remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
			remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );
			remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );
			remove_meta_box( 'dashboard_secondary', 'dashboard', 'side' );
			remove_meta_box( 'dashboard_activity', 'dashboard', 'side' );
			remove_meta_box( 'dashboard_welcome', 'dashboard', 'side' );
			remove_meta_box( 'dashboard_activity', 'dashboard', 'normal' );
			remove_action( 'welcome_panel', 'wp_welcome_panel' );
		}
		
	}
	
	public static function childThemesChecker() {
		// Check if child theme menu is registered
		$ctSlugs = array(
			'AGS_child_theme',
			'ags_divitherapist',
			'diviecommerce-options',
			'ags_photography',
			'ags_divipodcast',
			'divibusinesspro-options',
			'ags_divimedical',
			'ags_divicontractor',
			'ags_diviresume',
			'ags_divifitness',
			'ags_dnonprofit'
		);
		foreach ( $ctSlugs as &$slug ) {
			if ( ! empty( $GLOBALS['admin_page_hooks'][ $slug ] ) ) {
				return true;
			}
		}
	}
	
	public static function hideAdminBar( $show_admin_bar ) {
		// phpcs:ignore WordPress.Security.NonceVerification.Recommended -- just checking a variable
		if ( isset( $_GET['welcome_page'] ) && $_GET['welcome_page'] === 'custom_dashboard' ) {
			return false;
		} else {
			return $show_admin_bar;
		}
	}
	
	public static function customWidget() {
		$postid = DiviGhoster::$settings['widget_layout'];
		$html   = '';
		$html   .= '<iframe id="divi-ghoster-iframe" src="' . esc_url( add_query_arg( [
			'page_id' => (int) $postid,
			'welcome_page' => 'custom_dashboard',
			'welcome_nonce' => wp_create_nonce('divi_ghoster_view_welcome_widget')
		], home_url() ) ) . '" width="100%" height="800px"></iframe>';
		// phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
		echo  $html;
	}
	
	public static function loadDiviIfDashboard() {
		return $should_load = true;
	}
	
	public static function maybeShowNonAdminWelcomePanel() {
		if ( !current_user_can('edit_theme_options') && !empty(DiviGhoster::$settings['welcome_widget_all']) ) {
			
			// Following code copied from wp-admin/index.php and modified
			$classes = 'welcome-panel';
			
			/*
			$option = (int) get_user_meta( get_current_user_id(), 'show_welcome_panel', true );
			// 0 = hide, 1 = toggled to show or single site creator, 2 = multisite site owner.
			$hide = ( 0 === $option );
			if ( $hide ) {
				$classes .= ' hidden';
			}
			*/
			
			?>
			<div class="wrap">
				<div id="welcome-panel" class="<?php echo esc_attr( $classes ); ?>">
					<?php self::customWidget(); ?>
				</div>
			</div>
			<?php
		}
		
	}
	
	public static function filterDashboardWidgetQuery($query) {
		
		if ( $query->is_main_query()
				&& is_user_logged_in()
				&& ( current_user_can('edit_theme_options') || !empty(DiviGhoster::$settings['welcome_widget_all']) )
				&& $query->is_page()
				&& ( empty( $query->get_queried_object() ) || $query->get_queried_object()->ID == DiviGhoster::$settings['widget_layout'] )
				&& isset($_GET['welcome_nonce'])
				&& wp_verify_nonce( $_GET['welcome_nonce'], 'divi_ghoster_view_welcome_widget' )
		) {
			
			// Safeguard to protect against other private posts being viewed
			$notAllowed = [
				'p', 'name', 'pagename', 'post_parent', 'post_parent__in', 'post_parent__not_in',  'post__in',  'post__not_in',  'post_name__in', 
			];
			
			$query->query_vars = array_merge(
				array_diff_key($query->query_vars, $notAllowed),
				[
					'page_id' => (int) DiviGhoster::$settings['widget_layout'],
					'post_status' => 'private'
				]
			);
			
		}
		
	}
	
	
}

DiviGhosterAdminFilters::setup();