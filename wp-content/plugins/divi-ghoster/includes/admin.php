<?php

class DiviGhosterAdmin {
	
	public static $ghosterUrl, $customizer_url, $divi_ghoster_nonce, $divi_ghoster_nginx_slug;
	private static $export_nonce;
	
	public static function setup() {
		
		self::$ghosterUrl              = admin_url( 'admin.php?page=divi_ghoster' );
		self::$customizer_url          = admin_url( 'customize.php' );
		self::$divi_ghoster_nonce      = wp_unique_id( DiviGhoster::PLUGIN_SLUG );
		self::$divi_ghoster_nginx_slug = 'nginx-';
		
		add_action( 'admin_init', array( 'DiviGhosterAdmin', 'onAdminInit' ) );
		add_action( 'admin_enqueue_scripts', array( 'DiviGhosterAdmin', 'adminScripts' ) );
		
		// This should probably be moved to ultimate.php
		if ( DiviGhoster::$settings['ultimate_ghoster'] === 'yes' && (string) DiviGhoster::$settings['hide_premade_layouts'] === '1' ) {
			add_action( 'admin_enqueue_scripts', array( __CLASS__, 'hidePremadeLayoutsScript' ) );
			if (!empty($_GET['et_fb']) ) {
				add_action( 'wp_enqueue_scripts', array( __CLASS__, 'hidePremadeLayoutsScript' ) );
			}
		}
		
		add_filter( 'upgrader_post_install', array( __CLASS__, 'onUpdateInstalled' ), 10, 2 );
	}
	
	// divi-ghoster/includes/ultimate.php
	public static function onUpdateInstalled( $return, $args ) {
		if ( empty( $args['theme'] ) ) {
			return;
		}
		
		DiviGhoster::addLogEntry(
			sprintf(
				__('Update installed for %s', 'divi-ghoster'),
				$args['theme']
			)
		);
		
		if ( strcasecmp( $args['theme'], DiviGhoster::$targetTheme ) == 0 ) {
			
			try {
				DiviGhosterAdminAjax::setCustomThemeImage(DiviGhosterAdminAjax::THEME_TYPE_PARENT);
			} catch ( Exception $ex ) {
				DiviGhoster::addLogEntry(
					sprintf(
						__('Exception while setting custom theme image: %s', 'divi-ghoster'),
						$ex->getMessage()
					)
				);
				$error = true;
			}
			
			if (DiviGhoster::$settings['ultimate_ghoster'] === 'yes') {
				try {
					DiviGhosterUltimate::onUpdateInstalled( false );
				} catch ( Exception $ex ) {
					DiviGhoster::addLogEntry(
						sprintf(
							__('Exception while running the Ultimate Ghoster update handler: %s', 'divi-ghoster'),
							$ex->getMessage()
						)
					);
					$error = true;
				}
			}
			
		} else {
			$updatedTheme = wp_get_theme( $args['theme'] );
			if ( (string) $updatedTheme->parent() == DiviGhoster::$settings['theme_name'] || strcasecmp( (string) $updatedTheme->parent(), DiviGhoster::$targetTheme ) == 0 ) {
				
				try {
					DiviGhosterAdminAjax::setCustomThemeImage(DiviGhosterAdminAjax::THEME_TYPE_CHILD);
				} catch ( Exception $ex ) {
					$error = true;
				}
				
				if (DiviGhoster::$settings['ultimate_ghoster'] === 'yes') {
					try {
						DiviGhosterUltimate::onUpdateInstalled( $updatedTheme );
					} catch ( Exception $ex ) {
						$error = true;
					}
				}
				
			}
		}
		
		if ( ! empty( $error ) ) {
			return new WP_Error( 'AGSDG_ERROR' );
		}
		
	}
    
    public static function ghosterLicenseSection()
    {
        echo '<span class="agsdg_settings_section_heading">' . esc_html__('Ghoster License', 'divi-ghoster') . '</span>';
    }
    

	/**
	 * Display helper text in tooltip if server is Nginx
	 *
	 * @return string
	 */
	private static function tooltip_helper_text_if_server_is_nginx() {
		$tooltip = '';
		if ( self::check_if_server_is_nginx() ) {
			$tooltip = sprintf(
			/* translators: 1) <div class="ds-ghoster-settings-notification"> 2) <h4> 3) <h4 /> 4) <strong> 5) </strong> 6) </div> */
				__( '%1$s%2$sNginx Server:%3$sIn case if you can not make changes in Nginx configuration file, %4$sleave empty New Theme Path and New Plugin Path!%5$s
	                        Otherwise your Theme or Plugin styles may be broken %6$s', 'divi-ghoster' ), '<div class="ds-ghoster-settings-notification notification-warning">', '<h4>', '</h4>', '<strong>', '</strong>', '</div>'
			);
		}
		
		return $tooltip;
	}
	
	/**
	 * Check if server is Nginx
	 *
	 * @return bool
	 */
	public static function check_if_server_is_nginx() {
		// phpcs:ignore ET.Sniffs.ValidatedSanitizedInput.InputNotValidated, ET.Sniffs.ValidatedSanitizedInput.InputNotSanitized
		return stristr( $_SERVER['SERVER_SOFTWARE'], 'nginx' ) || stristr( $_SERVER['SERVER_SOFTWARE'], 'wpengine' );
	}
	
	public static function onAdminInit() {
		self::$export_nonce = wp_create_nonce( 'export_settings' );
		register_setting( 'agsdg_pluginPage', 'agsdg_settings', array( 'DiviGhosterAdmin', 'sanitizeSettings' ) );
		
		if ( isset($_GET['ds-ghoster-clear-log']) && wp_verify_nonce($_GET['ds-ghoster-clear-log'], 'ds-ghoster-clear-log') ) {
			DiviGhoster::clearLog();
			wp_safe_redirect( remove_query_arg('ds-ghoster-clear-log').'#log' );
		}
	}
	
	public static function menuPage() {
        
	    if (AGS_GHOSTER_has_license_key()) {
        
             self::adminPage();

        
	    }  else {
             AGS_GHOSTER_activate_page();
         }
        
	}
	
	public static function adminScripts( $hook ) {
		if ( $hook == 'toplevel_page_divi_ghoster' ) {
			wp_enqueue_media();
			wp_enqueue_script( 'agsdg_sweet_alert_js', 'https://cdn.jsdelivr.net/npm/sweetalert2@9' );
			wp_enqueue_script( 'agsdg_admin_page', DiviGhoster::$pluginBaseUrl . 'js/admin-page.js', array(
				'jquery',
				'wp-i18n'
			), DiviGhoster::PLUGIN_VERSION, true );
			wp_enqueue_script( 'color-picker-iris', DiviGhoster::$pluginBaseUrl . 'js/iris.min.js', array( 'jquery' ), DiviGhoster::PLUGIN_VERSION, true );
			wp_enqueue_style( 'wp-color-picker' );
			wp_enqueue_script( 'wp-color-picker' );
		}
		wp_enqueue_style( 'dg_admin', DiviGhoster::$pluginBaseUrl . 'css/admin.css' );
		
		if ( DiviGhoster::$settings['wp_shortcut'] == '1' ) {
			wp_enqueue_script( 'shortcut-something', DiviGhoster::$pluginBaseUrl . 'js/shortcut.js', array( 'jquery' ), DiviGhoster::PLUGIN_VERSION, true );
		}
	}
	
	public static function hidePremadeLayoutsScript() {
		wp_enqueue_script( 'divi-ghoster-hide-premade-layouts', DiviGhoster::$pluginBaseUrl . 'js/ghoster-hide-premade-layouts.js', array( 'jquery' ), DiviGhoster::PLUGIN_VERSION, true );
	}
	
	public static function sanitizeSettings( $settings ) {
		DiviGhoster::addLogEntry( __('Saving settings', 'divi-ghoster') );
		
		include_once( __DIR__ . '/ultimate.php' );
		
		// Make sure Branding Name is set
		if ( empty( $settings['branding_name'] ) ) {
			add_settings_error( 'branding_name', 'branding_name_empty', __( 'The Branding Name field may not be empty.', 'divi-ghoster' ) );
			$settings['branding_name'] = ( empty( DiviGhoster::$settings['branding_name'] ) ? DiviGhoster::$targetTheme : DiviGhoster::$settings['branding_name'] );
		}
		$settings['branding_name_esc'] = esc_html( $settings['branding_name'] );
		
		/*
		if ( isset( $settings['new_theme_path'] ) ) {
			DiviGhoster::$settings['new_theme_path'] = esc_attr( $settings['new_theme_path'] );
		} else {
			DiviGhoster::$settings['new_theme_path'] = '';
		}
		
		if ( isset( $settings['new_plugin_path'] ) ) {
			DiviGhoster::$settings['new_plugin_path'] = esc_attr( $settings['new_plugin_path'] );
		} else {
			DiviGhoster::$settings['new_plugin_path'] = '';
		}
		
		
		if ( isset( $settings['login_url'] ) ) {
			DiviGhoster::$settings['login_url'] = esc_attr( $settings['login_url'] );
		} else {
			DiviGhoster::$settings['login_url'] = '';
		}
		
		if ( isset( $settings['enable_login_page'] ) ) {
			DiviGhoster::$settings['enable_login_page'] = esc_attr( $settings['enable_login_page'] );
		} else {
			DiviGhoster::$settings['enable_login_page'] = '';
		}
		
		if ( isset( $settings['theme_name'] ) ) {
			DiviGhoster::$settings['theme_name'] = esc_attr( $settings['theme_name'] );
		} else {
			DiviGhoster::$settings['theme_name'] = '';
		}
		
		if ( isset( $settings['author_name'] ) ) {
			DiviGhoster::$settings['author_name'] = esc_attr( $settings['author_name'] );
		} else {
			DiviGhoster::$settings['author_name'] = '';
		}
		*/
		
		if ( ! empty( $settings['branding_image_url'] ) ) {
			$settings['branding_image_url'] = esc_url_raw( $settings['branding_image_url'] );
			if ( empty( DiviGhoster::$settings['branding_image_url'] ) || $settings['branding_image_url'] != DiviGhoster::$settings['branding_image_url'] ) {
				$result = DiviGhosterAdminAjax::process_branding_image( $settings['branding_image_url'], false );
			}
		} else {
			$settings['branding_image_url'] = '';
			
			if ( ! empty( DiviGhoster::$settings['branding_image_url'] ) ) {
				$result = DiviGhosterAdminAjax::removeBrandingImage();
			}
			
		}
		
		DiviGhoster::addLogEntry(
			sprintf(
				__('Theme image values: old - %s, new - %s', 'divi-ghoster'),
				empty(DiviGhoster::$settings['theme_image_url']) ? __('[empty]', 'divi-ghoster') : DiviGhoster::$settings['theme_image_url'],
				empty($settings['theme_image_url']) ? __('[empty]', 'divi-ghoster') : $settings['theme_image_url']
			)
		);
		
		if ( ! empty( $settings['theme_image_url'] ) ) {
			$settings['theme_image_url'] = esc_url_raw( $settings['theme_image_url'] );
			if ( empty( DiviGhoster::$settings['theme_image_url'] ) || $settings['theme_image_url'] != DiviGhoster::$settings['theme_image_url'] ) {
				$result = DiviGhosterAdminAjax::process_branding_image( $settings['theme_image_url'], true );
			}
		} else {
			$settings['theme_image_url'] = '';
			if ( ! empty( DiviGhoster::$settings['theme_image_url'] ) ) {
				try {
					$result = DiviGhosterAdminAjax::restoreThemeImage(true, DiviGhosterAdminAjax::THEME_TYPE_PARENT);
				} catch (Exception $ex) {
					DiviGhoster::addLogEntry(
						sprintf(
							__('Exception while restoring theme image for parent theme: %s', 'divi-ghoster'),
							$ex->getMessage()
						)
					);
				}
			}
		}
		
		// Need to make sure DiviGhoster::$settings is up to date before processing the child theme image
		
		DiviGhoster::$settings['theme_image_url'] = $settings['theme_image_url'];

		$oldChildThemeImageUrl = empty( DiviGhoster::$settings['child_theme_image_url'] ) ? null : DiviGhoster::$settings['child_theme_image_url'];
		$settings['child_theme_image_url'] = empty( $settings['child_theme_image_url'] ) ? '' : ( $settings['child_theme_image_url'] === 'same' ? 'same' : esc_url_raw( $settings['child_theme_image_url'] ) );
		
		DiviGhoster::addLogEntry(
			sprintf(
				__('Child theme image values: old - %s, new - %s', 'divi-ghoster'),
				empty($oldChildThemeImageUrl) ? __('[empty]', 'divi-ghoster') : DiviGhoster::$settings['theme_image_url'],
				empty($settings['child_theme_image_url']) ? __('[empty]', 'divi-ghoster') : $settings['theme_image_url']
			)
		);
		
		
		if ( ! empty( $settings['child_theme_image_url'] ) ) {
			
			if ( empty( $oldChildThemeImageUrl ) || $settings['child_theme_image_url'] != $oldChildThemeImageUrl ) {
				
				if ( $settings['child_theme_image_url'] === 'same' ) {
					if ( !empty( $oldChildThemeImageUrl ) ) {
						try {
							$result = DiviGhosterAdminAjax::restoreThemeImage(true, DiviGhosterAdminAjax::THEME_TYPE_CHILD);
						} catch (Exception $ex) {
							DiviGhoster::addLogEntry(
								sprintf(
									__('Exception while restoring theme image for child theme: %s', 'divi-ghoster'),
									$ex->getMessage()
								)
							);
						}
					}
					DiviGhoster::$settings['child_theme_image_url'] = $settings['child_theme_image_url'];
					try {
						DiviGhosterAdminAjax::setCustomThemeImage(DiviGhosterAdminAjax::THEME_TYPE_CHILD);
					} catch (Exception $ex) {
						DiviGhoster::addLogEntry(
							sprintf(
								__('Exception while setting theme image for child theme: %s', 'divi-ghoster'),
								$ex->getMessage()
							)
						);
					}
				} else {
					DiviGhoster::$settings['child_theme_image_url'] = $settings['child_theme_image_url'];
					$result = DiviGhosterAdminAjax::process_branding_image( $settings['child_theme_image_url'], true, true );
				}
				
			}
		} else {
			if ( ! empty( $oldChildThemeImageUrl ) ) {
				try {
					DiviGhoster::$settings['child_theme_image_url'] = $settings['child_theme_image_url'];
					$result = DiviGhosterAdminAjax::restoreThemeImage(true, DiviGhosterAdminAjax::THEME_TYPE_CHILD);
				} catch (Exception $ex) {
					DiviGhoster::addLogEntry(
						sprintf(
							__('Exception while restoring theme image for child theme: %s', 'divi-ghoster'),
							$ex->getMessage()
						)
					);
				}
			}
			
		}
		
		
		
		if ( empty( $settings['enable_login_page'] ) ) {
			$settings['enable_login_page'] = '';
		}
		
		$ultimateGhosterEnabled = ! empty( $settings['ultimate_ghoster'] ) && $settings['ultimate_ghoster'] != 'no' && $settings['ultimate_ghoster'] != 'off';
		
		// Make sure Theme Slug is set, is not the theme default, and does not contain invalid characters
		if ( empty( $settings['theme_slug'] ) ) {
			add_settings_error( 'theme_slug', 'theme_slug_empty', __( 'The Theme Slug field may not be empty.', 'divi-ghoster' ) );
			$settings['theme_slug'] = ( empty( DiviGhoster::$settings['theme_slug'] ) ? 'ghost_' . DiviGhoster::$targetThemeSlug : DiviGhoster::$settings['theme_slug'] );
		} else if ( strcasecmp( $settings['theme_slug'], DiviGhoster::$targetThemeSlug ) == 0 ) {
			add_settings_error( 'theme_slug', 'theme_slug_empty', __( 'The Theme Slug must be something other than the default', 'divi-ghoster' ) . ' &quot;' . DiviGhoster::$targetThemeSlug . '&quot;.' );
			$settings['theme_slug'] = ( empty( DiviGhoster::$settings['theme_slug'] ) ? 'ghost_' . DiviGhoster::$targetThemeSlug : DiviGhoster::$settings['theme_slug'] );
		} else {
			//..$newSlug = preg_replace('/[^A-Za-z0-9_\-]+/', '', $settings['theme_slug']);
			$newSlug = preg_replace( '/[^A-Za-z0-9_]+/', '', $settings['theme_slug'] );
			if ( $newSlug != $settings['theme_slug'] ) {
				add_settings_error( 'theme_slug', 'theme_slug_invalid_chars', __( 'The theme slug may only contain letters, numbers and underscores.', 'divi-ghoster' ) );
				$settings['theme_slug'] = $newSlug;
			}
		}
		
		DiviGhoster::addLogEntry(
			sprintf(
				__('Ultimate Ghoster enabled: %s', 'divi-ghoster'),
				$ultimateGhosterEnabled ? __('Yes', 'divi-ghoster') : __('No', 'divi-ghoster')
			)
		);
		
		if ( $ultimateGhosterEnabled ) {
			$settings['ultimate_ghoster'] = 'yes';
		
			if ( function_exists( 'wp_get_theme' ) ) {
				$theme = wp_get_theme()->get_template();
				
				if ( 'Extra' === $theme ) {
					DiviGhoster::$targetTheme = $theme;
				} else {
					DiviGhoster::$targetTheme = 'Divi';
				}
			}
			
			
			// Handle Ultimate Ghoster setting
			if ( get_option( 'permalink_structure', '' ) == '' ) {
				//add_settings_error( 'ultimate_ghoster', 'ultimate_ghoster_plain_permalinks', __( 'Ultimate Ghoster cannot be enabled while your permalink structure is set to Plain. Please change your permalink structure in Settings &gt; Permalinks.', 'divi-ghoster' ) );
			} else if ( ( $settings['theme_slug'] != DiviGhoster::$settings['theme_slug'] || DiviGhoster::$settings['ultimate_ghoster'] != 'yes' ) ) {
				try {
					DiviGhosterUltimate::disable( DiviGhoster::$settings['theme_slug'] );
				} catch ( Exception $ex ) {
					DiviGhoster::addLogEntry(
						sprintf(
							__('Exception while disabling Ultimate Ghoster: %s', 'divi-ghoster'),
							$ex->getMessage()
						)
					);
				}
				try {
					DiviGhosterUltimate::enable( $settings['theme_slug'], $settings['branding_name'] );
					update_option( 'adsdg_ultimate_theme', DiviGhoster::$targetTheme );
					add_settings_error( 'ultimate_ghoster', 'ultimate_ghoster_enabled', __( 'Settings saved. Ultimate Ghoster is enabled.', 'divi-ghoster' ), 'updated' );
				} catch ( Exception $ex ) {
					DiviGhoster::addLogEntry(
						sprintf(
							__('Exception while enabling Ultimate Ghoster: %s', 'divi-ghoster'),
							$ex->getMessage()
						)
					);
					add_settings_error( 'ultimate_ghoster', 'ultimate_ghoster_enable_error', __( 'An error occurred while enabling Ultimate Ghoster; please try again. If the problem persists, you may need to re-install your theme. ' . $ex->getMessage(), 'divi-ghoster' ) );
				}
			}
			
		} // end Ultimate Ghoster check
		
		
		if ( empty( $settings['ultimate_ghoster'] ) || $settings['ultimate_ghoster'] == 'no' ) {
			$settings['ultimate_ghoster'] = 'no';
			delete_option( 'adsdg_ultimate_theme' );
			
			if ( DiviGhoster::$settings['ultimate_ghoster'] === 'yes' ) {
				try {
					DiviGhosterUltimate::disable( DiviGhoster::$settings['theme_slug'] );
					add_settings_error( 'ultimate_ghoster', 'ultimate_ghoster_disabled', __( 'Ultimate Ghoster has been disabled.', 'divi-ghoster' ), 'updated' );
				} catch ( Exception $ex ) {
					DiviGhoster::addLogEntry(
						sprintf(
							__('Exception while disabling Ultimate Ghoster: %s', 'divi-ghoster'),
							$ex->getMessage()
						)
					);
					add_settings_error( 'ultimate_ghoster', 'ultimate_ghoster_disable_error', __( 'An error occurred while disabling Ultimate Ghoster. Please try enabling and disabling it again.', 'divi-ghoster' ) );
				}
			}
		}
		
		if ( ! empty( $settings['login_url'] ) ) {
			$pattern               = array( '_', ' ' );
			$settings['login_url'] = sanitize_text_field( $settings['login_url'] );
			$settings['login_url'] = trim( str_replace( $pattern, '-', $settings['login_url'] ) );
		}
		if ( ! empty( $settings['new_theme_path'] ) ) {
			$settings['new_theme_path'] = trim( str_replace( ' ', '-', $settings['new_theme_path'] ) );
		}
		
		if ( empty( $settings['widget_layout'] ) ) {
			$settings['widget_layout'] = '0';
		}
		
		// Make sure Options are set
		$hidePageSettings = array(
			'hide_divi_page',
			'hide_divi_options_page',
			'hide_library_page',
			'hide_theme_page',
			'hide_divi_related_plugins',
			'hide_divi_source',
			'hide_divi_ghoster',
			'hide_premade_layouts',
			'hide_theme_update_info',
			'hide_customizer_page',
			'hide_appearance_page',
			'hide_edit_page',
			'hide_settings_page',
			'hide_tools_page',
			'hide_projects_page',
			'hide_product_tour',
			'hide_plugins_page',
			'hide_ct_page',
			'hide_editor_page',
			'hide_support_page',
			'hide_updates_page',
			'hide_wp_page',
			'hide_wp_icon',
			'hide_wp_footer',
			'hide_wp_widgets',
			'wp_shortcut',
			'enable_custom_colors',
		
		);
		
		foreach ( $hidePageSettings as &$option ) {
			$settings[ $option ] = ! empty( $settings[ $option ] ) ? 1 : 0;
		}
		
		return $settings;
	}
	
	
	public static function adminPage() {
		
		?>
        <!-- START OUTPUT -->
        <div id="save-result"></div>
        <div id="custom-theme-image"></div>
        <div id="ds-ghoster-settings-container" xmlns="http://www.w3.org/1999/html">
			<?php settings_errors(); ?>
            <div id="ds-ghoster-settings">

                <div id="ds-ghoster-settings-header">
                    <div class="ds-ghoster-settings-logo">
                        <img alt="logo"
                             src="<?php echo( esc_url( DiviGhoster::$pluginBaseUrl . 'assets/images/logo.png' ) ); ?>">
                        <h1><?php esc_html_e( 'Divi Ghoster', 'divi-ghoster' ) ?></h1>
                    </div>
                    <div id="ds-ghoster-settings-header-links">
                        <!--                <a id="ds-ghoster-settings-header-link-settings" href=""><?php //esc_html_e('Settings', 'divi-ghoster');
						?></a>-->
                        <a id="ds-ghoster-export-import" class="ds-ghoster-export-import"
                           href="#import-export"><?php esc_html_e( 'Import/Export', 'divi-ghoster' ); ?> </a>
                        <a id="ds-ghoster-settings-header-link-support"
                           href="https://support.aspengrovestudios.com/article/20-divi-ghoster-plugin"
                           target="_blank"><?php esc_html_e( 'Documentation', 'divi-ghoster' ); ?></a>
                    </div>
                </div>

                <ul id="ds-ghoster-settings-tabs">
                    <li class="ds-ghoster-settings-active"><a
                                href="#branding"><?php esc_html_e( 'Branding', 'divi-ghoster' ); ?></a></li>
                    <li><a href="#ultimate-ghoster"><?php esc_html_e( 'Ultimate Ghoster', 'divi-ghoster' ); ?></a></li>
                    <li><a href="#customization"><?php esc_html_e( 'Customization', 'divi-ghoster' ); ?></a></li>
                    <li>
                        <a href="#custom-theme-colors"><?php esc_html_e( 'Custom Builder Colors', 'divi-ghoster' ); ?></a>
                    </li>
                    <li><a class="ds-ghoster-export-import"
                           href="#import-export"><?php esc_html_e( 'Import/Export', 'divi-ghoster' ); ?> </a></li>
                    <li><a class="ds-ghoster-log"
                           href="#log"><?php esc_html_e( 'Log', 'divi-ghoster' ); ?> </a></li>
                    
                    <li><a href="#license"><?php esc_html_e('License', 'divi-ghoster'); ?></a></li>
                    
                </ul>

                <form action='options.php' method='post'
                      id="ds-ghoster-settings-form-save">
					
					<?php settings_fields( 'agsdg_pluginPage' );
					do_settings_sections( 'agsdg_pluginPage' ); ?>
                    <div id="ds-ghoster-settings-tabs-content">

                        <!-- BRANDING TAB -->
                        <div id="ds-ghoster-settings-branding" class="ds-ghoster-settings-active">

                            <!--BRANDING NAME-->
                            <div class="ds-ghoster-settings-box">
                                <label>
                                    <span>
                                        <h3><?php esc_html_e( 'Branding Name', 'divi-ghoster' ); ?></h3>
                                        <?php esc_html_e( '(example: "Acme Web Design")', 'divi-ghoster' ); ?>
                                    </span>
                                    <input type="text" name="agsdg_settings[branding_name]"
                                           value="<?php echo esc_attr( DiviGhoster::$settings['branding_name'] ); ?>"
                                           class="agsdg_settings_field">
                                </label>
                                <div class="ds-ghoster-settings-tooltip">
                                    <div class="ds-ghoster-settings-tooltiptext">
                                        <span><?php esc_html_e( 'Help', 'divi-ghoster' ); ?></span>
										<?php esc_html_e( '(Example: "Acme Web Design"): This will replace all instances in the dashboard, page builder, etc. where the name ‘Divi’ appears. Please note: Certain characters may cause HTML entity code to appear in some places, such as the title bar of the main theme builder page. We recommend avoiding using such characters in the branding name. These characters include: & " < >', 'divi-ghoster' ); ?>
                                    </div>
                                </div>
                            </div>

                            <!--THEME NAME -->
                            <div class="ds-ghoster-settings-box">
                                <label>
                                    <span>
                                        <h3><?php esc_html_e( 'Theme Name', 'divi-ghoster' ); ?></h3>
                                        <?php esc_html_e( '(example: "Acme Web Design")', 'divi-ghoster' ); ?>
                                    </span>
                                    <input type="text" name="agsdg_settings[theme_name]"
                                           value="<?php echo esc_attr( DiviGhoster::$settings['theme_name'] ); ?>"
                                           class="agsdg_settings_field">
                                </label>
                                <div class="ds-ghoster-settings-tooltip">
                                    <div class="ds-ghoster-settings-tooltiptext">
                                        <span><?php esc_html_e( 'Help', 'divi-ghoster' ); ?></span>
										<?php esc_html_e( '(Example: "Acme Web Design"): This will replace Theme Name', 'divi-ghoster' ); ?>
                                    </div>
                                </div>
                            </div>

                            <!--AUTHOR NAME-->
                            <div class="ds-ghoster-settings-box">
                                <label>
                                    <span>
                                        <h3><?php esc_html_e( 'Author Name', 'divi-ghoster' ); ?></h3>
                                        <?php esc_html_e( '(example: "Acme Web Design")', 'divi-ghoster' ); ?>
                                    </span>
                                    <input type="text" name="agsdg_settings[author_name]"
                                           value="<?php echo esc_attr( DiviGhoster::$settings['author_name'] ); ?>"
                                           class="agsdg_settings_field">
                                </label>
                                <div class="ds-ghoster-settings-tooltip">
                                    <div class="ds-ghoster-settings-tooltiptext">
                                        <span><?php esc_html_e( 'Help', 'divi-ghoster' ); ?></span>
										<?php esc_html_e( '(Example: "Acme Web Design"): This will replace Theme Author Name', 'divi-ghoster' ); ?>
                                    </div>
                                </div>
                            </div>

                            <!--Branding Image-->
                            <div class="ds-ghoster-settings-box">
                                <label>
                                    <span>
                                        <h3><?php esc_html_e( 'Branding Image', 'divi-ghoster' ); ?></h3>
                                        <?php esc_html_e( '(minimum 50px by 50px)', 'divi-ghoster' ); ?>
                                    </span>
									
									<?php /*
                                    <img class="ghoster-branding-image" id="image-preview"
                                         src="<?php echo empty(DiviGhoster::$settings['branding_image']) ? DiviGhoster::$pluginBaseUrl . 'assets/images/default-logo.png' : DiviGhoster::$settings['branding_image']; ?>"
                                         alt="logo">
									*/ ?>
                                    <input class="agsdg_settings_field ghoster-img-input" id="image-url" type="text"
                                           name="agsdg_settings[branding_image_url]"
                                           placeholder="<?php esc_html_e( 'Paste the URL, choose image from media library or leave blank', 'divi-ghoster' ) ?>"
                                           value="<?php
										   if ( isset( DiviGhoster::$settings['branding_image_url'] ) ) {
										       echo esc_attr( DiviGhoster::$settings['branding_image_url'] );
									       } else if ( isset( DiviGhoster::$settings['branding_image'] ) ) {
										       echo esc_attr( DiviGhoster::$settings['branding_image'] );
									       }
										   ?>"/>

                                    <button id="branding-image-set" type="button"
                                            class="button dg-button ghoster-img-button ghoster-img-button-set">
										<?php esc_html_e( 'Select/Upload Image', 'divi-ghoster' ); ?>
                                    </button>

                                    <button id="branding-image-remove" type="button"
                                            class="button dg-button ghoster-img-button">
										<?php esc_html_e( 'Restore Default Image', 'divi-ghoster' ) ?>
                                    </button>
                                </label>

                                <div class="ds-ghoster-settings-tooltip">
                                    <div class="ds-ghoster-settings-tooltiptext">
                                        <span><?php esc_html_e( 'Help', 'divi-ghoster' ); ?></span>
										<?php esc_html_e( 'Upload your desired Branding Image (50px x 50px): This will replace all instances where the Divi icon appears in the dashboard, page builder, etc. If you upload a larger image of the same scale, it will reduce, but we encourage not using large file sizes.', 'divi-ghoster' ); ?>
                                    </div>
                                </div>
                            </div>

                            <!--Custom Theme Image-->
                            <div class="ds-ghoster-settings-box" id="custom-theme-image-container">
                                <label>
                                    <span>
                                        <h3><?php esc_html_e( 'Custom Theme Image', 'divi-ghoster' ); ?></h3>
                                    </span>

                                    <input class="agsdg_settings_field ghoster-img-input" id="custom-theme-image-url"
                                           type="text"
                                           name="agsdg_settings[theme_image_url]"
                                           placeholder="<?php esc_html_e( 'Paste the URL, choose image from media library or leave blank', 'divi-ghoster' ) ?>"
                                           value="<?php if ( ! empty( DiviGhoster::$settings['theme_image_url'] ) ) {
										       echo esc_attr( DiviGhoster::$settings['theme_image_url'] );
									       } ?>"/>

                                    <button id="custom-theme-image-set" type="button"
                                            class="button dg-button ghoster-img-button ghoster-img-button-set">
										<?php esc_html_e( 'Select/Upload Image', 'divi-ghoster' ) ?>
                                    </button>

                                    <button id="custom-theme-image-remove" type="button"
                                            class="button dg-button ghoster-img-button">
										<?php esc_html_e( 'Restore Default Image', 'divi-ghoster' ) ?>
                                    </button>
                                </label>

                                <div class="ds-ghoster-settings-tooltip">
                                    <div class="ds-ghoster-settings-tooltiptext">
                                        <span><?php esc_html_e( 'Help', 'divi-ghoster' ); ?></span>
										<?php esc_html_e( 'Upload your desired Theme Image: This will replace theme screenshot in the Dashboard Appearance Themes Screen. We recommend to use 880px x 660px dimensions for best results.', 'divi-ghoster' ); ?>
                                    </div>
                                </div>
                            </div>

                            <!--Custom Child Theme Image-->
                            <div class="ds-ghoster-settings-box" id="custom-child-theme-image-container">
                                <label>
                                    <span>
                                        <h3><?php esc_html_e( 'Custom Child Theme Image', 'divi-ghoster' ); ?></h3>
                                    </span>

                                    <input class="agsdg_settings_field ghoster-img-input" id="custom-child-theme-image-url"
                                           type="text"
                                           name="agsdg_settings[child_theme_image_url]"
                                           placeholder="<?php esc_html_e( 'Paste the URL, choose image from media library or leave blank', 'divi-ghoster' ) ?>"
                                           value="<?php 
										       echo isset(DiviGhoster::$settings['child_theme_image_url']) ? esc_attr( DiviGhoster::$settings['child_theme_image_url'] ) : ( empty(DiviGhoster::$settings['theme_image_url'])  ? '' : 'same' );
									       ?>"/>

                                    <button id="custom-child-theme-image-set-same" type="button"
                                            class="button dg-button ghoster-img-button">
										<?php esc_html_e( 'Same as Custom Theme Image', 'divi-ghoster' ) ?>
                                    </button>

                                    <button id="custom-child-theme-image-set" type="button"
                                            class="button dg-button ghoster-img-button ghoster-img-button-set">
										<?php esc_html_e( 'Select/Upload Image', 'divi-ghoster' ) ?>
                                    </button>

                                    <button id="custom-child-theme-image-remove" type="button"
                                            class="button dg-button ghoster-img-button">
										<?php esc_html_e( 'Restore Default Image', 'divi-ghoster' ) ?>
                                    </button>
                                </label>

                                <div class="ds-ghoster-settings-tooltip">
                                    <div class="ds-ghoster-settings-tooltiptext">
                                        <span><?php esc_html_e( 'Help', 'divi-ghoster' ); ?></span>
										<?php esc_html_e( 'Upload your desired Theme Image: This will replace theme screenshot in the Dashboard Appearance Themes Screen for any child themes of the ghosted theme. We recommend to use 880px x 660px dimensions for best results. Enter "same" if you want to use the same image as the Custom Theme Image.', 'divi-ghoster' ); ?>
                                    </div>
                                </div>
                            </div>

                            <!--Theme URL Slug-->
                            <div class="ds-ghoster-settings-box">
                                <label>
                                    <span>
                                        <h3><?php esc_html_e( 'Theme URL Slug', 'divi-ghoster' ); ?></h3>
                                        <?php esc_html_e( '(example: acme)', 'divi-ghoster' ); ?>
                                    </span>
                                    <input type="text" size="45" name="agsdg_settings[theme_slug]" value="<?php
									echo esc_attr( DiviGhoster::$settings['theme_slug'] );
									?>" class="agsdg_settings_field">
                                </label>
                                <div class="ds-ghoster-settings-tooltip">
                                    <div class="ds-ghoster-settings-tooltiptext">
                                        <span><?php esc_html_e( 'Help', 'divi-ghoster' ); ?></span>
										<?php esc_html_e( 'Enter your Slug Text (example: acme_web_designs): This will change the theme name that is used in URLs and the wp-content/themes directory. Examples:
                                                            http://yourwebsiteurl.com/wp-content/themes/acme_web_designs/style.css and http://yourwebsiteurl.com/wp-admin/admin.php?page=et_acme-web-designs.', 'divi-ghoster' ); ?>
                                    </div>
                                </div>
								<p class="ds-ghoster-settings-warning"><?php esc_html_e( 'Warning: Any existing theme in your themes directory with this slug may be overwritten.', 'divi-ghoster' ); ?></p>
                            </div>

                            <!--Enable Login Page Customizer-->
                            <div class="ds-ghoster-settings-box">
                                <label>
                                    <span>
                                        <h3><?php esc_html_e( 'Enable Login Page Customizer', 'divi-ghoster' ); ?></h3>
                                    </span>
                                    <input name="agsdg_settings[enable_login_page]"
                                           id="divi-ghoster-enable-login" type="checkbox"
                                           value="1" <?php checked( '1', DiviGhoster::$settings['enable_login_page'] ); ?>/>
                                </label>
                                <div class="ds-ghoster-settings-tooltip">
                                    <div class="ds-ghoster-settings-tooltiptext">
                                        <span><?php esc_html_e( 'Help', 'divi-ghoster' ); ?></span>
										<?php esc_html_e( 'Enable login page customizer to create your own branded login page', 'divi-ghoster' ); ?>
                                    </div>
                                </div>
                            </div>
							
                            <!--Login Page-->
                            <div class="ds-ghoster-settings-box" id="login-page">
                                <label>
                                    <span>
                                        <h3><?php esc_html_e( 'Login Page', 'divi-ghoster' ); ?></h3>
                                        <?php esc_html_e( '(create branded login page)', 'divi-ghoster' ); ?>
                                    </span>
									
									<span id="divi-ghoster-enable-login-instructions"><?php esc_html_e( 'Save changes to access the login customizer', 'divi-ghoster' ); ?></span>
									
									<?php
									$login_customizer_url = add_query_arg( array(
										'return'             => self::$ghosterUrl,
										'autofocus[section]' => 'ghoster_custom_login'
									), self::$customizer_url );
									?><a class="button button-primary" href="<?php
									echo esc_url( $login_customizer_url );
									?>"><?php esc_html_e( 'Login Customizer', 'divi-ghoster' ); ?></a>

                                </label>
                                <div class="ds-ghoster-settings-tooltip">
                                    <div class="ds-ghoster-settings-tooltiptext">
                                        <span><?php esc_html_e( 'Help', 'divi-ghoster' ); ?></span>
										<?php esc_html_e( 'Customize the standard WordPress login page with your own logo, background, and colors with an easy to use interface', 'divi-ghoster' ); ?>
                                    </div>
                                </div>
                            </div>

                            <!-- New login url -->
                            <div class="ds-ghoster-settings-box">
                                <label>
                                    <span>
                                        <h3><?php esc_html_e( 'Login Url slug', 'divi-ghoster' ); ?></h3>
                                        <?php esc_html_e( '(example: "mylogin")', 'divi-ghoster' ); ?>
                                    </span>
                                    <input type="text" name="agsdg_settings[login_url]" placeholder="Login Url Slug"
                                           value="<?php echo esc_html( DiviGhoster::$settings['login_url'] ); ?>"
                                           class="agsdg_settings_field">
                                </label>
                                <div class="ds-ghoster-settings-tooltip">
                                    <div class="ds-ghoster-settings-tooltiptext">
                                        <span><?php esc_html_e( 'Help', 'divi-ghoster' ); ?></span>
										<?php esc_html_e( '(Example: "mylogin"): This will replace standard http://your-site.com/wp-login.php with http://your-site.com/mylogin', 'divi-ghoster' ); ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- ULTIMATE GHOSTER TAB -->
                        <div id="ds-ghoster-settings-ultimate-ghoster">

                            <!-- Enable Ultimate Ghoster -->
                            <div class="ds-ghoster-settings-box list-checkboxes">
                                <div class="ds-settings-heading">
                                    <span><h3><?php esc_html_e( 'Ultimate Divi Ghoster', 'divi-ghoster' ); ?></h3></span>
                                </div>
                                <div class="list-checkboxes-content">
									<?php if ( false === HIDE_ULTIMATE_CHECKBOX ) : ?>
                                        <label>
                                            <span><b><?php esc_html_e( 'Enable Ultimate Divi Ghoster:', 'divi-ghoster' ); ?></b></span>
                                            <input name="agsdg_settings[ultimate_ghoster]" id="ultimate-ghoster"
                                                   type="checkbox" <?php echo( DiviGhoster::$settings['ultimate_ghoster'] == 'yes' ? ' checked="checked"' : '' ); ?> >

                                        </label>
									
									<?php endif; ?>
                                    <div id="ultimate-list">
                                        <label>
                                            <span><?php esc_html_e( 'Hide Divi Related Plugins', 'divi-ghoster' ); ?></span>
                                            <input name="agsdg_settings[hide_divi_related_plugins]"
                                                   class="ds-checkboxes-wrap-ultimate" type="checkbox"
                                                   value="1" <?php checked( DiviGhoster::$settings['hide_divi_related_plugins'], '1' ); ?> >
                                        </label>
                                        <label>
                                            <span><?php esc_html_e( 'Hide Divi Ghoster Plugin', 'divi-ghoster' ); ?></span>
                                            <input name="agsdg_settings[hide_divi_ghoster]"
                                                   class="ds-checkboxes-wrap-ultimate" type="checkbox"
                                                   value="1" <?php checked( DiviGhoster::$settings['hide_divi_ghoster'], '1' ); ?> >
                                        </label>
                                        <label>
                                            <span><?php esc_html_e( 'Hide Traces of Divi', 'divi-ghoster' ); ?></span>
                                            <input name="agsdg_settings[hide_divi_source]"
                                                   class="ds-checkboxes-wrap-ultimate" type="checkbox"
                                                   value="1" <?php checked( DiviGhoster::$settings['hide_divi_source'], '1' ); ?> >
                                        </label>
                                        <label>
                                            <span><?php esc_html_e( 'Hide Premade Layouts', 'divi-ghoster' ); ?></span>
                                            <input name="agsdg_settings[hide_premade_layouts]" type="checkbox"
                                                   value="1" <?php checked( DiviGhoster::$settings['hide_premade_layouts'], '1' ); ?>
                                                   class="ds-checkboxes-wrap-ultimate"/>
                                        </label>
                                        <label>
                                            <span><?php esc_html_e( 'Hide Theme Update Info', 'divi-ghoster' ); ?></span>
                                            <input name="agsdg_settings[hide_theme_update_info]" type="checkbox"
                                                   value="1" <?php checked( DiviGhoster::$settings['hide_theme_update_info'], '1' ); ?>
                                                   class="ds-checkboxes-wrap-ultimate"/>
                                        </label>
                                        <label>
                                            <span><?php esc_html_e( 'Hide Product Tour and Help Links', 'divi-ghoster' ); ?></span>
                                            <input name="agsdg_settings[hide_product_tour]" type="checkbox"
                                                   value="1" <?php checked( DiviGhoster::$settings['hide_product_tour'], '1' ); ?>
                                                   class="ds-checkboxes-wrap-ultimate"/>
                                        </label>
                                        <div class="ds-settings-summary">
                                            <button type="button" onclick="jQuery(this).parent().siblings().find(':checkbox').prop('checked', true);" class="button dg-button">
												<?php esc_html_e( 'Check All', 'divi-ghoster' ); ?>
											</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="ds-ghoster-settings-tooltip">
                                    <div class="ds-ghoster-settings-tooltiptext">
                                        <span><?php esc_html_e( 'Help', 'divi-ghoster' ); ?></span>
										<?php esc_html_e( 'Once “Ultimate Ghoster” is enabled and saved, the following functions will take effect:', 'divi-ghoster' ); ?>
                                        <ul>
                                            <li>
                                                - <?php esc_html_e( 'The ‘Divi Ghoster’ link in the dashboard will disappear', 'divi-ghoster' ); ?></li>
                                            <li>
                                                - <?php esc_html_e( 'Divi Ghoster and other popular Divi-related plugins will not be visible in the plugins dashboard', 'divi-ghoster' ); ?></li>
                                            <li>
                                                - <?php esc_html_e( 'The Divi theme will be hidden from the source code and theme detector websites and replaced with your selected branding name', 'divi-ghoster' ); ?></li>
                                            <li>
                                                - <?php esc_html_e( 'The theme name under Appearance > Themes will be branded with your name.', 'divi-ghoster' ); ?></li>
                                            <li>
                                                - <?php esc_html_e( 'The “Role Editor” will be “Ghosted”', 'divi-ghoster' ); ?></li>
                                            <li>
                                                - <?php esc_html_e( 'The Divi Logo will be replaced with your custom logo.', 'divi-ghoster' ); ?></li>
                                            <li>
                                                - <?php esc_html_e( 'Enabling Ultimate Ghoster will hide the Divi Ghoster plugin.', 'divi-ghoster' ); ?></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                            <!-- Hide Divi Pages -->
                            <div class="ds-ghoster-settings-box list-checkboxes">
                                <div class="ds-settings-heading">
                                    <span><h3><?php esc_html_e( 'Hide Divi Admin Pages', 'divi-ghoster' ); ?></h3></span>
                                </div>
                                <div class="list-checkboxes-content">
                                    <label>
                                        <span><?php esc_html_e( 'Theme Options Page', 'divi-ghoster' ); ?></span>
                                        <input name="agsdg_settings[hide_divi_options_page]" type="checkbox"
                                               value="1" <?php checked( DiviGhoster::$settings['hide_divi_options_page'], '1' ); ?>
                                               class="ds-checkboxes-wrap-1"/>
                                    </label>
                                    <label>
                                        <span><?php esc_html_e( 'Theme Builder Page', 'divi-ghoster' ); ?></span>
                                        <input name="agsdg_settings[hide_theme_page]" type="checkbox"
                                               value="1" <?php checked( DiviGhoster::$settings['hide_theme_page'], '1' ); ?>
                                               class="ds-checkboxes-wrap-1"/>
                                    </label>
                                    <label>
                                        <span><?php esc_html_e( 'Theme Customizer Page', 'divi-ghoster' ); ?></span>
                                        <input name="agsdg_settings[hide_customizer_page]" type="checkbox"
                                               value="1" <?php checked( DiviGhoster::$settings['hide_customizer_page'], '1' ); ?>
                                               class="ds-checkboxes-wrap-1"/>
                                    </label>
                                    <label>
                                        <span><?php esc_html_e( 'Role Editor Page', 'divi-ghoster' ); ?></span>
                                        <input name="agsdg_settings[hide_editor_page]" type="checkbox"
                                               value="1" <?php checked( DiviGhoster::$settings['hide_editor_page'], '1' ); ?>
                                               class="ds-checkboxes-wrap-1"/>
                                    </label>
                                    <label>
                                        <span><?php esc_html_e( 'Theme Library Page', 'divi-ghoster' ); ?></span>
                                        <input name="agsdg_settings[hide_library_page]" type="checkbox"
                                               value="1" <?php checked( DiviGhoster::$settings['hide_library_page'], '1' ); ?>
                                               class="ds-checkboxes-wrap-1"/>
                                    </label>
                                    <label>
                                        <span><?php esc_html_e( 'Support Center Page', 'divi-ghoster' ); ?></span>
                                        <input name="agsdg_settings[hide_support_page]" type="checkbox"
                                               value="1" <?php checked( DiviGhoster::$settings['hide_support_page'], '1' ); ?>
                                               class="ds-checkboxes-wrap-1"/>
                                    </label>
                                    <label class="ds-settings-summary">
                                        <span><?php esc_html_e( 'Hide Main Divi Menu Item', 'divi-ghoster' ); ?></span>
                                        <input name="agsdg_settings[hide_divi_page]" id="agsdg-settings-hide-divi-page"
                                               type="checkbox"
                                               value="1" <?php checked( DiviGhoster::$settings['hide_divi_page'], '1' ); ?> />
                                    </label>
                                </div>

                                <div class="ds-ghoster-settings-tooltip">
                                    <div class="ds-ghoster-settings-tooltiptext">
                                        <span><?php esc_html_e( 'Help', 'divi-ghoster' ); ?></span>
										<?php esc_html_e( 'Hides the page from the WordPress Dashboard.', 'divi-ghoster' ); ?>
                                    </div>
                                </div>

                            </div>

                            <!-- Shortcut -->
                            <div class="ds-ghoster-settings-box">
                                <label>
                                <span>
                                    <h3><?php esc_html_e( 'Enable Ultimate Ghoster Shortcut', 'divi-ghoster' ); ?></h3>
                                    <?php esc_html_e( 'click F7 to access Ghoster', 'divi-ghoster' ); ?>
                                </span>
                                    <input name="agsdg_settings[wp_shortcut]" type="checkbox"
                                           value="1" <?php checked( DiviGhoster::$settings['wp_shortcut'], '1' ); ?> />
                                </label>
                                <div class="ds-ghoster-settings-tooltip">
                                    <div class="ds-ghoster-settings-tooltiptext">
                                        <span><?php esc_html_e( 'Help', 'divi-ghoster' ); ?></span>
										<?php esc_html_e( 'Enable “Ultimate Ghoster Shortcut” feature to access Divi Ghoster Settings by clicking F7 from Admin Dashboard', 'divi-ghoster' ); ?>
                                    </div>
                                </div>
                            </div>

                            <!-- Notices -->
                            <div class="ds-ghoster-settings-box">
                                <ul class="ghoster-ultimate-notice">
									<?php $ghosterUrlEsc = esc_url( self::$ghosterUrl );
									$ghosterLink         = '<a class="ghoster-link" href=" ' . $ghosterUrlEsc . ' "> ' . $ghosterUrlEsc . '</a>'; ?>
                                    <li>
                                        1.<?php echo sprintf( esc_html__( 'If you are using a caching plugin, %s be sure to clear its cache %s after enabling or disabling Ultimate Ghoster!', 'divi-ghoster' ), '<strong>', '</strong>' ); ?> </li>
                                    <li>
                                        2. <?php esc_html_e( 'Ultimate Ghoster will not work if the permalink structure is set to Plain in Settings > Permalinks.', 'divi-ghoster' ); ?></li>
                                    <li>
                                        3. <?php echo sprintf( esc_html__( '%sEnabling Ultimate Ghoster will hide the %s Ghoster plugin. Please copy this URL and save it to disable this feature later: %s %s or enable "Ultimate Ghoster Shortcut" feature to access Divi Ghoster Settings by clicking F7 from Admin Dashboard', 'divi-ghoster' ), '<strong>', esc_html( DiviGhoster::$targetTheme ), '</strong>', et_core_intentionally_unescaped( $ghosterLink, 'html' ) ); ?>
                                    </li>
                                    <li>
                                        4. <?php esc_html_e( 'Enabling Ultimate Ghoster will hide Divi Switch, Page Builder Everywhere, Divi Rocket, Divi Booster, Divi Icon Party, WP and Divi Icons Pro, and Aspen Footer Editor from the Divi menu and the Plugins list.', 'divi-ghoster' ); ?></li>
                                    <li>
                                        5. <?php esc_html_e( 'If installed, they can be accessed directly at any time by visiting:', 'divi-ghoster' ); ?>
                                    <li>
                                        <ul>
                                            <li>
                                                - <?php echo sprintf( esc_html__( 'For %sDivi Switch%s:', 'divi-ghoster' ), '<strong>','</strong>' ); ?>
                                                <a href="<?php echo esc_url( admin_url( 'admin.php?page=et_' . DiviGhoster::$settings['theme_slug'] . '_options#wrap-swtch_tab' ) ); ?> "><?php echo esc_url( admin_url( 'admin.php?page=et_' . DiviGhoster::$settings['theme_slug'] . '_options#wrap-swtch_tab' ) ); ?></a>
                                            </li>
                                            <li>
                                                - <?php echo sprintf( esc_html__( 'For %sPage Builder Everywhere%s:', 'divi-ghoster' ), '<strong>','</strong>' ); ?>
                                                <a href="<?php echo esc_url( admin_url( 'admin.php?page=ds-page-builder-everywhere' ) ); ?> "><?php echo esc_url( admin_url( 'admin.php?page=ds-page-builder-everywhere' ) ); ?></a>
                                            </li>
                                            <li>
                                                - <?php echo sprintf( esc_html__( 'For %sDivi Rocket%s:', 'divi-ghoster' ), '<strong>','</strong>' ); ?>
                                                <a href="<?php echo esc_url( admin_url( 'admin.php?page=divi-rocket-settings' ) ); ?> "><?php echo esc_url( admin_url( 'admin.php?page=divi-rocket-settings' ) ); ?></a>
                                            </li>
                                            <li>
                                                - <?php echo sprintf( esc_html__( 'For %sDivi Booster%s:', 'divi-ghoster' ), '<strong>','</strong>' ); ?>
                                                <a href="<?php echo esc_url( admin_url( 'admin.php?page=wtfdivi_settings' ) ); ?> "><?php echo esc_url( admin_url( 'admin.php?page=wtfdivi_settings' ) ); ?></a>
                                            </li>
                                            <li>
                                                - <?php echo sprintf( esc_html__( 'For %sAspen Footer Editor%s:', 'divi-ghoster' ), '<strong>','</strong>' ); ?>
                                                <a href="<?php echo esc_url( admin_url( 'admin.php?page=' . DiviGhoster::$settings['theme_slug'] . '-footer-editor' ) ); ?> "><?php echo esc_url( admin_url( 'admin.php?page=' . DiviGhoster::$settings['theme_slug'] . '-footer-editor' ) ); ?></a>
                                            </li>
                                            <li>
                                                - <?php echo sprintf( esc_html__( 'For %sDivi Icon Party%s:', 'divi-ghoster' ), '<strong>','</strong>' ); ?>
                                                <a href="<?php echo esc_url( admin_url( 'admin.php?page=ds-icon-party' ) ); ?> "><?php echo esc_url( admin_url( 'admin.php?page=ds-icon-party' ) ); ?></a>
                                            </li>
                                            <li>
                                                - <?php echo sprintf( esc_html__( 'For %sWP and Divi Icon Pro%s:', 'divi-ghoster' ), '<strong>','</strong>' ); ?>
                                                <a href="<?php echo esc_url( admin_url( 'admin.php?page=ds-icon-expansion' ) ); ?> "><?php echo esc_url( admin_url( 'admin.php?page=ds-icon-expansion' ) ); ?></a>
                                            </li>
                                            <li>
                                                - <?php echo sprintf( esc_html__( 'For %sDivi Overlays%s:', 'divi-ghoster' ), '<strong>','</strong>' ); ?>
                                                <a href="<?php echo esc_url( admin_url( 'edit.php?post_type=divi_overlay' ) ); ?> "><?php echo esc_url( admin_url( 'edit.php?post_type=divi_overlay' ) ); ?></a>
                                            </li>
                                        </ul>
                                </ul>
                            </div>
                        </div>

                        <!-- CUSTOMIZATION TAB -->
                        <div id="ds-ghoster-settings-customization">

                            <!-- Hide WordPress Tabs -->
                            <div class="ds-ghoster-settings-box list-checkboxes">
                                <label class="ds-settings-heading">
                                    <span><h3><?php esc_html_e( 'Hide WordPress Tabs', 'divi-ghoster' ); ?></h3></span>
                                </label>
                                <div class="list-checkboxes-content">
                                    <label>
                                        <span><?php esc_html_e( 'Appearance', 'divi-ghoster' ); ?></span>
                                        <input name="agsdg_settings[hide_appearance_page]" type="checkbox"
                                               value="1" <?php checked( DiviGhoster::$settings['hide_appearance_page'], '1' ); ?>
                                               class="ds-checkboxes-wrap-2"/>
                                    </label>

                                    <label>
                                        <span><?php esc_html_e( 'Settings', 'divi-ghoster' ); ?></span>
                                        <input name="agsdg_settings[hide_settings_page]" type="checkbox"
                                               value="1" <?php checked( DiviGhoster::$settings['hide_settings_page'], '1' ); ?>
                                               class="ds-checkboxes-wrap-2"/>
                                    </label>

                                    <label>
                                        <span><?php esc_html_e( 'Tools', 'divi-ghoster' ); ?></span>
                                        <input name="agsdg_settings[hide_tools_page]" type="checkbox"
                                               value="1" <?php checked( DiviGhoster::$settings['hide_tools_page'], '1' ); ?>
                                               class="ds-checkboxes-wrap-2"/>
                                    </label>

                                    <label>
                                        <span><?php esc_html_e( 'Projects', 'divi-ghoster' ); ?></span>
                                        <input name="agsdg_settings[hide_projects_page]" type="checkbox"
                                               value="1" <?php checked( DiviGhoster::$settings['hide_projects_page'], '1' ); ?>
                                               class="ds-checkboxes-wrap-2"/>
                                    </label>

                                    <label>
                                        <span><?php esc_html_e( 'Plugins', 'divi-ghoster' ); ?> </span>
                                        <input name="agsdg_settings[hide_plugins_page]" type="checkbox"
                                               value="1" <?php checked( DiviGhoster::$settings['hide_plugins_page'], '1' ); ?>
                                               class="ds-checkboxes-wrap-2"/>
                                    </label>

                                    <label>
                                        <span><?php esc_html_e( 'Updates', 'divi-ghoster' ); ?></span>
                                        <input name="agsdg_settings[hide_updates_page]" type="checkbox"
                                               value="1" <?php checked( DiviGhoster::$settings['hide_updates_page'], '1' ); ?>
                                               class="ds-checkboxes-wrap-2"/>
                                    </label>
									
									<?php //display this setting only if child theme is active...
									if ( DiviGhosterAdminFilters::childThemesChecker() ) {
										?>
                                        <label>
                                            <span><?php esc_html_e( 'Child Theme', 'divi-ghoster' ); ?></span>
                                            <input name="agsdg_settings[hide_ct_page]" type="checkbox"
                                                   value="1" <?php checked( DiviGhoster::$settings['hide_ct_page'], '1' ); ?>
                                                   class="ds-checkboxes-wrap-2"/>
                                        </label>
										<?php
									} else {
										DiviGhoster::$settings['hide_ct_page'] = '0';
									}
									?>

                                    <label class="ds-settings-summary">
                                        <span><?php esc_html_e( 'Hide All', 'divi-ghoster' ); ?></span>
                                        <input name="agsdg_settings[hide_wp_page]" id="agsdg-settings-hide-wp-page"
                                               type="checkbox"
                                               value="1" <?php checked( DiviGhoster::$settings['hide_wp_page'], '1' ); ?> />
                                    </label>

                                    <div class="ds-ghoster-settings-tooltip">
                                        <div class="ds-ghoster-settings-tooltiptext">
                                            <span><?php esc_html_e( 'Help', 'divi-ghoster' ); ?></span>
											<?php esc_html_e( 'Hides the page from the WordPress Dashboard.', 'divi-ghoster' ); ?>
                                        </div>
                                    </div>
                                </div>

                            </div>

                            <!-- WordPress Customizations -->
                            <div class="ds-ghoster-settings-box list-checkboxes">
                                <label class="ds-settings-heading">
                                    <span><h3><?php esc_html_e( 'WordPress Customizations', 'divi-ghoster' ); ?></h3></span>
                                </label>
                                <div class="list-checkboxes-content">
                                    <label>
                                        <span><?php esc_html_e( 'Hide WordPress Icon', 'divi-ghoster' ); ?></span>
                                        <input name="agsdg_settings[hide_wp_icon]" type="checkbox"
                                               value="1" <?php checked( DiviGhoster::$settings['hide_wp_icon'], '1' ); ?> />
                                    </label>

                                    <label>
                                        <span><?php esc_html_e( 'Hide WordPress Footer', 'divi-ghoster' ); ?></span>
                                        <input name="agsdg_settings[hide_wp_footer]" type="checkbox"
                                               value="1" <?php checked( DiviGhoster::$settings['hide_wp_footer'], '1' ); ?> />
                                    </label>

                                    <label>
                                        <span><?php esc_html_e( 'Hide WordPress Default Widgets', 'divi-ghoster' ); ?></span>
                                        <input name="agsdg_settings[hide_wp_widgets]" type="checkbox"
                                               value="1" <?php checked( DiviGhoster::$settings['hide_wp_widgets'], '1' ); ?> />
                                    </label>

                                    <div class="ds-ghoster-settings-tooltip">
                                        <div class="ds-ghoster-settings-tooltiptext">
                                            <span><?php esc_html_e( 'Help', 'divi-ghoster' ); ?></span>
                                            <ul>
                                                <li><?php esc_html_e( '"Hide WordPress Icon" - Hides the icon from Dashboard', 'divi-ghoster' ); ?></li>
                                                <li><?php esc_html_e( '"Hide WordPress Footer" - Hides "Thank you for creating with WordPress', 'divi-ghoster' ); ?></li>
                                                <li><?php esc_html_e( '"Hide WordPress Default Widgets" - Hides widgets/meta boxes from Dashboard', 'divi-ghoster' ); ?></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!-- Widget Library Layout -->
                            <div class="ds-ghoster-settings-box">
                                <label>
                                    <span>
                                       <h3><?php esc_html_e( 'Admin Dashboard Welcome Page', 'divi-ghoster' ); ?></h3>
                                    </span>
									<?php
									$posts = get_posts( array(
										'post_type'        => 'page',
										'post_status'      => 'private',
										'suppress_filters' => false,
										'posts_per_page'   => - 1
									) ); ?>
                                    <select name="agsdg_settings[widget_layout]"
                                            id="agsdg_settings[widget_layout]">'; <?php
										echo '<option value = "" >' . esc_html__( 'Choose Layout', 'divi-ghoster' ) . '</option>';
										foreach ( $posts as $post ) {
											echo '<option value="', (int) $post->ID, '"', ( DiviGhoster::$settings['widget_layout'] == $post->ID ? ' selected="selected"' : '' ), '>', esc_html( $post->post_title ), '</option>';
										} ?>
                                    </select>

                                </label>
								
                                <p class="ds-ghoster-settings-info"><?php esc_html_e( 'Replaces welcome widget on WordPress admin dashboard with the selected page (only pages with "Private" visibility are shown in the list). Note that selecting a page here will allow anyone who can see the welcome widget to view the selected page, regardless of whether they usually have access to others\' private pages.', 'divi-ghoster' ); ?></p>
                            </div>
							
							
                            <!--Enable Login Page Customizer-->
                            <div class="ds-ghoster-settings-box">
                                <label>
                                    <span>
                                        <h3><?php esc_html_e( 'Show Welcome Page for All Users', 'divi-ghoster' ); ?></h3>
                                    </span>
                                    <input name="agsdg_settings[welcome_widget_all]"
                                           id="divi-ghoster-welcome-widget-all" type="checkbox"
                                           value="1" <?php checked( '1', isset(DiviGhoster::$settings['welcome_widget_all']) ? DiviGhoster::$settings['welcome_widget_all'] : false ); ?>/>
                                </label>
                                <div class="ds-ghoster-settings-tooltip">
                                    <div class="ds-ghoster-settings-tooltiptext">
                                        <span><?php esc_html_e( 'Help', 'divi-ghoster' ); ?></span>
										<?php esc_html_e( 'By default, the welcome panel in the dashboard is only visible to users with the edit_theme_options capability. Checking this box will make it visible to all users. Note that users without the edit_theme_options capability will not be able to hide the welcome panel.', 'divi-ghoster' ); ?>
                                    </div>
                                </div>
                            </div>

                            <!-- Notices -->
                            <div class="ds-ghoster-settings-box">
                                <ul class="ghoster-ultimate-notice">
                                    <li><?php esc_html_e( 'If hiding page is enabled, page can be accessed directly at any time by visiting:', 'divi-ghoster' ); ?>
                                    <li>
                                        <ul>
                                            <li>
                                                - <?php echo sprintf( esc_html__( 'For %sTheme Options Page%s:', 'divi-ghoster' ), '<strong>', '</strong>' ); ?>
                                                <a href="<?php echo esc_url( admin_url( 'admin.php?page=et_' . DiviGhoster::$settings['theme_slug'] . '_options' ) ); ?> "><?php echo esc_url( admin_url( 'admin.php?page=et_' . DiviGhoster::$settings['theme_slug'] . '_options' ) ); ?></a>
                                            </li>
                                            <li>
                                                - <?php echo sprintf( esc_html__( 'For %sTheme Library Page%s:', 'divi-ghoster' ), '<strong>', '</strong>' ); ?>
                                                <a href="<?php echo esc_url( admin_url( 'edit.php?post_type=et_pb_layout' ) ); ?> "><?php echo esc_url( admin_url( 'edit.php?post_type=et_pb_layout' ) ); ?></a>
                                            </li>
                                            <li>
                                                - <?php echo sprintf( esc_html__( 'For %sTheme Builder Page%s:', 'divi-ghoster' ), '<strong>', '</strong>' ); ?>
                                                <a href="<?php echo esc_url( admin_url( 'admin.php?page=et_theme_builder' ) ); ?> "><?php echo esc_url( admin_url( 'admin.php?page=et_theme_builder' ) ); ?></a>
                                            </li>
                                            <li>
                                                - <?php echo sprintf( esc_html__( 'For %sTheme Customizer Page%s:', 'divi-ghoster' ), '<strong>', '</strong>' ); ?>
                                                <a href="<?php echo esc_url( admin_url( 'customize.php?et_customizer_option_set=theme' ) ); ?> "><?php echo esc_url( admin_url( 'customize.php?et_customizer_option_set=theme' ) ); ?></a>
                                            </li>
                                            <li>
                                                - <?php echo sprintf( esc_html__( 'For %sTheme Roles Editor Page%s:', 'divi-ghoster' ), '<strong>', '</strong>' ); ?>
                                                <a href="<?php echo esc_url( admin_url( 'admin.php?page=et_' . DiviGhoster::$settings['theme_slug'] . '_role_editor' ) ); ?> "><?php echo esc_url( admin_url( 'admin.php?page=et_' . DiviGhoster::$settings['theme_slug'] . '_role_editor' ) ); ?></a>
                                            </li>
                                            <li>
                                                - <?php echo sprintf( esc_html__( 'For %sTheme Support Page%s:', 'divi-ghoster' ), '<strong>', '</strong>' ); ?>
                                                <a href="<?php echo esc_url( admin_url( 'admin.php?page=et_support_center_divi' ) ); ?> "><?php echo esc_url( admin_url( 'admin.php?page=et_support_center_divi' ) ); ?></a>
                                            </li>
                                            <li>
                                                - <?php echo sprintf( esc_html__( 'For %sAppearance Page%s:', 'divi-ghoster' ), '<strong>', '</strong>' ); ?>
                                                <a href="<?php echo esc_url( admin_url( 'themes.php' ) ); ?> "><?php echo esc_url( admin_url( 'themes.php' ) ); ?></a>
                                            </li>
                                            <li>
                                                - <?php echo sprintf( esc_html__( 'For %sSettings Page%s:', 'divi-ghoster' ), '<strong>', '</strong>' ); ?>
                                                <a href="<?php echo esc_url( admin_url( 'options-general.php' ) ); ?> "><?php echo esc_url( admin_url( 'options-general.php' ) ); ?></a>
                                            </li>
                                            <li>
                                                - <?php echo sprintf( esc_html__( 'For %sTools Page%s:', 'divi-ghoster' ), '<strong>', '</strong>' ); ?>
                                                <a href="<?php echo esc_url( admin_url( 'tools.php' ) ); ?> "><?php echo esc_url( admin_url( 'tools.php' ) ); ?></a>
                                            </li>
                                            <li>
                                                - <?php echo sprintf( esc_html__( 'For %sProjects Page%s:', 'divi-ghoster' ), '<strong>', '</strong>' ); ?>
                                                <a href="<?php echo esc_url( admin_url( 'tools.php' ) ); ?> "><?php echo esc_url( admin_url( 'tools.php' ) ); ?></a>
                                            </li>
                                            <li>
                                                - <?php echo sprintf( esc_html__( 'For %sPlugins Page%s:', 'divi-ghoster' ), '<strong>', '</strong>' ); ?>
                                                <a href="<?php echo esc_url( admin_url( 'plugins.php' ) ); ?> "><?php echo esc_url( admin_url( 'plugins.php' ) ); ?></a>
                                            </li>
                                        </ul>
                                </ul>
                            </div>
                        </div>

                        <!-- CUSTOM COLORS TAB -->
                        <div id="ds-ghoster-settings-custom-theme-colors">

                            <!-- Enable Custom Colors -->
                            <div class="ds-ghoster-settings-box ghoster-color-picker-box">
                                <div class="title">
                                    <h3><?php esc_html_e( 'Enable Custom Colors', 'divi-ghoster' ); ?></h3>
									<?php esc_html_e( 'customize the backend and visual editor color scheme', 'divi-ghoster' ); ?>
                                </div>
                                <input name="agsdg_settings[enable_custom_colors]"
                                       id="divi-ghoster-enable-custom-colors" type="checkbox"
                                       value="1" <?php checked( '1', DiviGhoster::$settings['enable_custom_colors'] ); ?> />
                                <div class="ds-ghoster-settings-tooltip">
                                    <div class="ds-ghoster-settings-tooltiptext">
                                        <span><?php esc_html_e( 'Help', 'divi-ghoster' ); ?></span>
                                        <p><?php esc_html_e( 'Customize the Divi Builder, Theme Options Page, Theme Builder Page, Role Editor Page with your unique color scheme to help white label the interface even further.', 'divi-ghoster' ); ?></p>
                                    </div>
                                </div>
                            </div>

                            <!-- Primary Accent Color -->
                            <div class="ds-ghoster-settings-box ghoster-color-picker-box">
                                <label class="title">
                                    <h3><?php esc_html_e( 'Primary Accent Color', 'divi-ghoster' ); ?></h3>
									<?php esc_html_e( 'primary color for header and tabs', 'divi-ghoster' ); ?>
                                </label>
                                <input type="text" name="agsdg_settings[primary_accent_color]"
                                       value="<?php echo esc_attr( ( ! isset( DiviGhoster::$settings['primary_accent_color'] ) ? '#8F42ED' : DiviGhoster::$settings['primary_accent_color'] ) ); ?>"
                                       class="my-color-field" data-default-color="#8F42ED"/>
                            </div>

                            <div class="ds-ghoster-settings-box ghoster-color-picker-box">
                                <label class="title">
                                    <h3><?php esc_html_e( 'Primary Accent Color', 'divi-ghoster' ); ?></h3>
									<?php esc_html_e( 'primary color for header and tabs', 'divi-ghoster' ); ?>
                                </label>
                                <input type="text" name="agsdg_settings[primary_accent_color_2]"
                                       value="<?php echo esc_attr( ( ! isset( DiviGhoster::$settings['primary_accent_color_2'] ) ? '#7e3bd0' : DiviGhoster::$settings['primary_accent_color_2'] ) ); ?>"
                                       class="my-color-field" data-default-color="#7e3bd0"/>
                            </div>

                            <div class="ds-ghoster-settings-box ghoster-color-picker-box">
                                <label class="title">
                                    <h3><?php esc_html_e( 'Primary Accent Color', 'divi-ghoster' ); ?></h3>
									<?php esc_html_e( 'primary color for header and tabs', 'divi-ghoster' ); ?>
                                </label>
                                <input type="text" name="agsdg_settings[primary_accent_color_4]"
                                       value="<?php echo esc_attr( ( ! isset( DiviGhoster::$settings['primary_accent_color_4'] ) ? '#7435C1' : DiviGhoster::$settings['primary_accent_color_4'] ) ); ?>"
                                       class="my-color-field" data-default-color="#7435C1"/>
                            </div>

                            <div class="ds-ghoster-settings-box ghoster-color-picker-box">
                                <label class="title">
                                    <h3><?php esc_html_e( 'Primary Accent Color', 'divi-ghoster' ); ?></h3>
									<?php esc_html_e( 'primary color for header and tabs', 'divi-ghoster' ); ?>
                                </label>
                                <input type="text" name="agsdg_settings[primary_accent_color_3]"
                                       value="<?php echo esc_attr( ( ! isset( DiviGhoster::$settings['primary_accent_color_3'] ) ? '#6c2eb9' : DiviGhoster::$settings['primary_accent_color_3'] ) ); ?>"
                                       class="my-color-field" data-default-color="#6c2eb9"/>
                            </div>

                            <!-- Secondary Accent Color -->
                            <div class="ds-ghoster-settings-box ghoster-color-picker-box">
                                <label class="title">
                                    <h3><?php esc_html_e( 'Secondary Accent Color', 'divi-ghoster' ); ?></h3>
									<?php esc_html_e( 'sections, icons, marks, buttons', 'divi-ghoster' ); ?>
                                </label>
                                <input type="text" name="agsdg_settings[secondary_accent_color]"
                                       value="<?php echo esc_attr( ( ! isset( DiviGhoster::$settings['secondary_accent_color'] ) ? '#2b87da' : DiviGhoster::$settings['secondary_accent_color'] ) ); ?>"
                                       class="my-color-field" data-default-color="#2b87da"/>
                            </div>

                            <!-- 3rd Accent Color -->
                            <div class="ds-ghoster-settings-box ghoster-color-picker-box">
                                <label class="title">
                                    <h3><?php esc_html_e( '3rd Accent Color', 'divi-ghoster' ); ?></h3>
									<?php esc_html_e( 'rows, icons, marks, buttons', 'divi-ghoster' ); ?>
                                </label>
                                <input type="text" name="agsdg_settings[3rd_accent_color]"
                                       value="<?php echo esc_attr( ( ! isset( DiviGhoster::$settings['3rd_accent_color'] ) ? '#00C3AA' : DiviGhoster::$settings['3rd_accent_color'] ) ); ?>"
                                       class="my-color-field" data-default-color="#00C3AA"/>
                            </div>

                            <!-- 4th Accent Color -->
                            <div class="ds-ghoster-settings-box ghoster-color-picker-box">
                                <label class="title">
                                    <h3><?php esc_html_e( '4th Accent Color', 'divi-ghoster' ); ?></h3>
									<?php esc_html_e( 'mark disabled color', 'divi-ghoster' ); ?>
                                </label>
                                <input type="text" name="agsdg_settings[4rd_accent_color]"
                                       value="<?php echo esc_attr( ( ! isset( DiviGhoster::$settings['4rd_accent_color'] ) ? '#FF5600' : DiviGhoster::$settings['4rd_accent_color'] ) ); ?>"
                                       class="my-color-field" data-default-color="#FF5600"/>
                            </div>

                            <!-- 5th Accent Color -->
                            <div class="ds-ghoster-settings-box ghoster-color-picker-box">
                                <label class="title">
                                    <h3><?php esc_html_e( '5th Accent Color', 'divi-ghoster' ); ?></h3>
									<?php esc_html_e( 'global layout, buttons', 'divi-ghoster' ); ?>
                                </label>
                                <input type="text" name="agsdg_settings[5th_accent_color]"
                                       value="<?php echo esc_attr( ( ! isset( DiviGhoster::$settings['5th_accent_color'] ) ? '#97d000' : DiviGhoster::$settings['5th_accent_color'] ) ); ?>"
                                       class="my-color-field" data-default-color="#97d000"/>
                            </div>

                            <!-- 6th Accent Color -->
                            <div class="ds-ghoster-settings-box ghoster-color-picker-box">
                                <label class="title">
                                    <h3><?php esc_html_e( '6th Accent Color', 'divi-ghoster' ); ?></h3>
									<?php esc_html_e( 'custom layout, module color', 'divi-ghoster' ); ?>
                                </label>
                                <input type="text" name="agsdg_settings[6th_accent_color]"
                                       value="<?php echo esc_attr( ( ! isset( DiviGhoster::$settings['6th_accent_color'] ) ? '#4c5866' : DiviGhoster::$settings['6th_accent_color'] ) ); ?>"
                                       class="my-color-field" data-default-color="#4c5866"/>
                            </div>

                            <!-- Text Color -->
                            <div class="ds-ghoster-settings-box ghoster-color-picker-box">
                                <label class="title">
                                    <h3><?php esc_html_e( 'Text Color', 'divi-ghoster' ); ?></h3>
									<?php esc_html_e( 'header and buttons', 'divi-ghoster' ); ?>
                                </label>
                                <input type="text" name="agsdg_settings[text_color]"
                                       value="<?php echo esc_attr( ( ! isset( DiviGhoster::$settings['text_color'] ) ? '#FFFFFF' : DiviGhoster::$settings['text_color'] ) ); ?>"
                                       class="my-color-field" data-default-color="#FFFFFF"/>
                            </div>
                        </div>
                </form>
                <!-- CLOSE TAB -->

                <!-- IMPORT/EXPORT TAB -->
                <div id="ds-ghoster-settings-import-export">

                    <!-- Import Settings -->
                    <div class="ds-ghoster-settings-box" id="ghoster-export-import">
                        <label>
                            <span><h3><?php esc_html_e( 'Import Ghoster Settings', 'divi-ghoster' ); ?></h3></span>
                            <form method="post" enctype="multipart/form-data">
                                <p><input type="file" name="ghoster_settings_import"/></p>
                                <p>
                                    <input type="hidden" name="divi_ghoster_import_action"
                                           value="divi_ghoster_import_settings"/>
									<?php wp_nonce_field( 'divi_ghoster_import_nonce', 'divi_ghoster_import_nonce' ); ?>
									<?php submit_button( __( 'Import Settings', 'divi-ghoster' ), 'primary', 'submit', false ); ?>
                                </p>
                            </form>
                        </label>
                    </div>

                    <!-- Export Settings -->
                    <div class="ds-ghoster-settings-box">
                        <label>
                            <span><h3><?php esc_html_e( 'Export Ghoster Settings', 'divi-ghoster' ); ?></h3></span>
                            <a class="button button-primary"
                               href="<?php echo esc_url( admin_url( 'admin.php?page=divi-ghoster&_export_ghoster_settings=' . DiviGhosterAdmin::$export_nonce ) ) ?>">
								<?php esc_html_e( 'Export', 'divi-ghoster' ); ?>
                            </a>
                        </label>
                    </div>

                </div>
                <!-- CLOSE TAB -->
				
				
                <!-- LOG TAB -->
                <div id="ds-ghoster-settings-log">
					
					<p>
						<?php
						esc_html_e(
							'Divi Ghoster logs up to 100 events in this log to assist in troubleshooting any problems that arise with the Divi Ghoster functionality. On this page, you can view and clear this logging data (please ensure you have saved any settings changes before clicking the Clear Log link). Old log entries will automatically be removed as needed to limit the maximum number of entries to 100.',
							'divi-ghoster'
						);
						?>
					</p>
					
					<?php
						$log = get_option('divi_ghoster_log');
						if ($log) {
							$log = json_decode($log);
						}
						
						if (empty($log)) {
					?>
					<p><?php esc_html_e( 'The log is empty.', 'divi-ghoster' ); ?></p>
					
					<?php } else { ?>
					
					<p>
						<a href="<?php echo( esc_url( add_query_arg('ds-ghoster-clear-log', wp_create_nonce('ds-ghoster-clear-log') ) . '#log' ) ); ?>">
							<?php esc_html_e( 'Clear Log', 'divi-ghoster' ); ?>
						</a>
					</p>
					
					<table id="ds-ghoster-log-table">
						<thead>
							<tr>
								<th><?php esc_html_e( 'Date/Time', 'divi-ghoster' ); ?></th>
								<th><?php esc_html_e( 'Message', 'divi-ghoster' ); ?></th>
							</tr>
						</thead>
						<tbody>
						<?php foreach ($log as $logEntry) { ?>
							<tr>
								<td><?php echo( esc_html( $logEntry[0] ) ); ?></td>
								<td><?php echo( esc_html( $logEntry[1] ) ); ?></td>
							</tr>
						<?php } ?>
						</tbody>
					</table>
					
					<?php } ?>

                </div>
                <!-- CLOSE TAB -->
            </div>

            <div id="loader"></div>
            <div id="ds-ghoster-settings-tabs-content">
                
                <!-- LICENSE TAB -->
                <div id="ds-ghoster-settings-license">
                    <?php AGS_GHOSTER_license_key_box(); ?>
                </div>
                
                <button id='epanel-save-top' class='save-button button dg-button' name="btnSubmit"
                        form="ds-ghoster-settings-form-save">
					<?php esc_html_e( 'Save Changes', 'divi-ghoster' ); ?>
                </button>
            </div> <!-- close ds-ghoster-settings-tabs-content -->
        </div>   <!-- SETTINGS TAB -->

        </div> <!-- close ds-ghoster-settings-container -->
		
		<?php
	}
}

DiviGhosterAdmin::setup();