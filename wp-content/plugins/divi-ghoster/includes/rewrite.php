<?php
/**
 * DiviGhosterRewrite class .
 *
 * Can handle rewrite rules
 */

if ( ! defined( 'ABSPATH' ) ) {
	die( 'Directly access not allowed here' );
}

abstract class DiviGhosterRewrite {
	/** @var array $current_theme */
	protected $current_theme;
	/** @var string $current_theme_name */
	protected $current_theme_name;
	/** private string $theme_stylesheet_directory */
	protected $current_theme_path;
	/** @var string $new_theme_path */
	protected $new_theme_path;
	/** @var string $plugin_dir_path */
	protected $plugin_dir_path;
	/** @var array $Plugin_data */
	protected $plugin_data = array();
	/** @var string $New_plugin_path */
	protected $new_plugin_path;
	/** @var string $login_url_slug */
	protected $login_url_slug;
	/** @var string $page_now */
	protected $page_now;
	/** @var string $url_path */
	protected $url_path;
	/** @var string $current_path */
	protected $current_path;
	/** @var string $child_theme_path */
	protected $child_theme_path;
	/** @var string $key */
	protected $key;
	/**@var array $new_theme_path_rule */
	protected $new_path_rule;
	/**@var string $rewrite */
	protected $rewrite = false;
	
	const PLUGIN_URL = WP_PLUGIN_URL;
	
	/**
	 * Rewrite rules in .htaccess
	 *
	 * @return void
	 */
	public function set_flush_rewrite_rules() {
		flush_rewrite_rules();
	}
	
	/**
	 * Remove rules in .htaccess
	 *
	 * @return void
	 */
	public function remove_rewrite_rules() {
		flush_rewrite_rules( true );
	}
	
	/**
	 * Rename and switch theme
	 *
	 * @param string $new_path
	 * @param string $current_path
	 *
	 * @return bool
	 */
	protected function rename_path( $current_path, $new_path ) {
		if ( $new_path ) {
			@rename( $current_path, $new_path );
		}
		
		return false;
	}
	
	/**
	 * Check if trailing slash exists in permalink structure
	 *
	 * @return bool
	 */
	protected function check_if_trailing_slash_exists_in_permalink_structure() {
		return ( '/' === substr( get_option( 'permalink_structure' ), - 1, 1 ) );
	}
	
	/**
	 * Remove/Use Trailing slash if exists
	 *
	 * @param string $query_string
	 *
	 * @return string
	 */
	protected function handle_trailing_slashes_in_custom_query_string( $query_string ) {
		return $this->check_if_trailing_slash_exists_in_permalink_structure() ? trailingslashit( $query_string ) : untrailingslashit( $query_string );
	}
	
	/**
	 * Get current path for theme/plugin
	 *
	 * @param string path
	 *
	 * @return string
	 */
	public static function get_current_path( $path = 'theme' ) {
		if ( $path === 'plugin' ) {
			return trim( str_replace( site_url(), '', self::PLUGIN_URL ), '/' );
		}
		
		return trim( str_replace( site_url(), '', get_template_directory_uri() ), '/' );
	}
	
	/**
	 * Get child theme path
	 *
	 * @return string
	 */
	public static function get_child_theme_path() {
		return trim( str_replace( site_url(), '', get_stylesheet_directory_uri() ), '/' );
	}
	
	/**
	 * Get theme and plugin paths if set
	 *
	 * @return string
	 */
	protected function get_new_path() {
		if ( '' !== $this->url_path ) {
			return trim( $this->url_path, '/' );
		}
		
		return '';
	}
	
	/**
	 * Actions for rewrite rules
	 *
	 * @param string $path
	 *
	 * @return void
	 */
	protected function rewrite_actions( $path ) {
		if ( '' !== $path && $path ) {
			global $pagenow;
			// phpcs:ignore WordPress.Security.NonceVerification.Missing
			if ( $pagenow !== 'options.php' || empty( $_POST['option_page'] ) || $_POST['option_page'] !== 'agsdg_pluginPage' ) {
				add_action( 'admin_init', array( $this, 'set_flush_rewrite_rules' ) );
			}
		} else {
			add_action( 'admin_init', array( $this, 'remove_rewrite_rules' ) );
		}
	}
	
	/**
	 * Add rewrite rule for child theme
	 *
	 * @param bool bool $is_child_theme
	 *
	 * @return void
	 */
	private function rewrite_rule_for_child_theme( $is_child_theme = false ) {
		if ( $is_child_theme ) {
		
		}
	}
}