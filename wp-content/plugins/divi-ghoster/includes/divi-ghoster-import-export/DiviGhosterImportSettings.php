<?php
/**
 * DiviGhosterExportSettings class .
 *
 * Can handle Settings Export
 */
 
// This file was modified by Jonathan Hall and/or others.
// Last modified 2020-11-02

class DiviGhosterImportSettings extends DIviGhosterImport {
	
	private $ultimate_ghoster_settings_compatibility = array(
		'hide_divi_related_plugins' => 1,
		'hide_divi_ghoster'         => 1,
		'hide_divi_source'          => 1,
		'hide_product_tour'         => 1,
		'hide_divi_page'            => 1,
		'hide_theme_update_info'    => 1,
		'hide_premade_layouts'      => 1,
	);
	
	/**
	 * Setup import settings
	 *
	 * @return void
	 */
	public function setup() {
		add_action( 'admin_notices', array( $this, 'import_notices' ) );
		
		if ( !empty( $_POST['divi_ghoster_import_action'] ) && 'divi_ghoster_import_settings' === $_POST['divi_ghoster_import_action'] ) {
			add_action('admin_init', array($this, 'doImport'));
		}
	}
	
	public function doImport() {
		if ( ! current_user_can( 'manage_options' ) ) {
			wp_die( sprintf(
			/* Translators: 1) <a> 2) </a>*/
			// phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
				__( 'You are not allowed to be here! %1$sGo back%2$s', 'divi-ghoster' ), '<a href="' . esc_url( home_url() ) . '">', '</a>'
			), 'Not Found' );
		}
		
		if ( isset( $_FILES['ghoster_settings_import'] ) ) {
			// phpcs:ignore ET.Sniffs.ValidatedSanitizedInput.InputNotValidated
			// phpcs:ignore ET.Sniffs.ValidatedSanitizedInput.InputNotSanitized
			if ( isset( $_POST['divi_ghoster_import_nonce'] ) ) {
				if ( ! wp_verify_nonce( sanitize_key( wp_unslash( $_POST['divi_ghoster_import_nonce'] ) ), 'divi_ghoster_import_nonce' ) ) {
					//wp_die( 'You are not allowed to this action!' );
					$this->type    = 'error';
					$this->message = __( 'Something went wrong and settings could not be imported. Please try again later.', 'divi-ghoster' );
					delete_option( 'divi_ghoster_success_import' );
					
					return;
				}
			}
			$allowed_extension   = array( 'json' );
			// phpcs:ignore ET.Sniffs.ValidatedSanitizedInput.InputNotValidated
			// phpcs:ignore ET.Sniffs.ValidatedSanitizedInput.InputNotSanitized
			// $_FILES superglobal is checked above, line 40
			if ( isset( $_FILES['ghoster_settings_import']['name'] ) && isset($_FILES['ghoster_settings_import']['tmp_name']) ) {
				$file                = sanitize_file_name( $_FILES['ghoster_settings_import']['name'] );
				$json_file_extension = strtolower( @end( explode( '.', $file ) ) );
				// phpcs:ignore ET.Sniffs.ValidatedSanitizedInput.InputNotValidated
				// phpcs:ignore ET.Sniffs.ValidatedSanitizedInput.InputNotSanitized
				$json_file_name      = esc_attr( $_FILES['ghoster_settings_import']['tmp_name'] );
				if ( '' !== $this->check_uploaded_file( $json_file_name, $json_file_extension, $allowed_extension ) ) {
					$this->type    = 'error';
					$this->message = $this->check_uploaded_file( $json_file_name, $json_file_extension, $allowed_extension );
					delete_option( 'divi_ghoster_success_import' );
					
					return;
				}
				$import = new DiviGhosterImportSettings();
				$import->get_exported_ghoster_settings( $json_file_name );
				$ultimate_mode = DiviGhoster::$settings['ultimate_ghoster'];
				
				if ( $import->import_settings( 'import', $ultimate_mode ) ) {
					$this->message = __( 'Import complete.', 'divi-ghoster' );
					update_option( 'divi_ghoster_success_import', $this->message );
					wp_safe_redirect( admin_url( 'admin.php?page=divi_ghoster#branding' ) );
					exit();
				} else {
					$this->type    = 'error';
					$this->message = __( 'Something went wrong and settings could not be imported. Please try again later.', 'divi-ghoster' );
					delete_option( 'divi_ghoster_success_import' );
				}
			}
		}
	}
	
	/**
	 * Check if uploaded file is not empty or allowed extension
	 *
	 * @param string $json_file_name
	 * @param string $json_file_extension
	 * @param array $allowed_extension
	 *
	 * @return string
	 */
	private function check_uploaded_file( $json_file_name, $json_file_extension, $allowed_extension ) {
		if ( empty( $json_file_name ) ) {
			return __( 'Please upload a file to import', 'divi-ghoster' );
		}
		
		if ( ! in_array( $json_file_extension, $allowed_extension ) ) {
			return __( 'Unknown file extension! Allowed file types : ' . implode( '|', $allowed_extension ), 'divi-ghoster' );
		}
		
		return '';
	}
	
	/**
	 * Display import notices
	 *
	 * @retrn void
	 */
	public function import_notices() {
		$message = ( ! empty( get_option( 'divi_ghoster_success_import' ) ) ? esc_attr( get_option( 'divi_ghoster_success_import' ) ) : $this->message );
		
		if ( '' !== $message && $message ) {
			add_settings_error( $this->settings_error_notice, $this->settings_error_notice, $message, $this->type );
			delete_option( 'divi_ghoster_success_import' );
		}
	}
	
	/**
	 * @inheritDoc
	 */
	public function divi_ghoster_settings( $settings ) {
	}
	
	/**
	 * Import Divi Ghoster Settings
	 *
	 * @param string $action
	 * @param string $ultimate_mode
	 *
	 * @return bool
	 */
	public function import_settings( $action = 'import', $ultimate_mode = 'no' ) {
		if ( null !== $this->divi_ghoster_options ) {
			if ( isset( $this->divi_ghoster_options['ghoster_options'] ) ) {
				if ( is_admin() && current_user_can( 'manage_options' ) ) {
					if ( 'yes' === $ultimate_mode ) {
						foreach ( $this->ultimate_ghoster_settings_compatibility as $compatibility => $setting ) {
							if ( $this->divi_ghoster_options['ghoster_options'][ $compatibility ] === 1 ) {
								continue;
							}
							$ultimate_options_data = filter_var_array( $this->ultimate_ghoster_settings_compatibility, FILTER_VALIDATE_INT );
							$ghoster_settings      = filter_var_array( $this->divi_ghoster_options['ghoster_options'], FILTER_SANITIZE_STRING );
							$ultimate_options      = array_merge( $ghoster_settings, $ultimate_options_data );
							update_option( 'agsdg_settings', $ultimate_options );
							return true;
						}
					}
					$import_data = filter_var_array( $this->divi_ghoster_options['ghoster_options'], FILTER_SANITIZE_STRING );
					update_option( 'agsdg_settings', $import_data );
					return true;
				}
			}
		}
		
		return false;
	}
	
	/**
	 * Get imported Ghoster Settings
	 *
	 * @param string $imported_settings
	 *
	 * @return array
	 */
	public function get_exported_ghoster_settings( $imported_settings ) {
		if ( ! empty( $imported_settings ) ) {
			$this->divi_ghoster_options = json_decode( file_get_contents( $imported_settings ), true );
		}
		
		return $this->divi_ghoster_options;
	}
}