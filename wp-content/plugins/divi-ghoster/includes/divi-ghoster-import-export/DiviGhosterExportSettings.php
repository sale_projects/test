<?php
// This file modified by Vasili Guruli, Jonathan Hall, and/or others; last modified 2020-08-06

/**
 * DiviGhosterExportSettings class .
 *
 * Can handle Settings Export
 */

class DiviGhosterExportSettings extends DiviGhosterExport {
	
	public function setup() {
		$this->divi_ghoster_settings = DiviGhoster::$settings;
		$this->file_name             = 'divi_ghoster_settings-' . gmdate( "Y-m-d" ) . '.json';
		
		$this->run_export();
		
		if ( $this->start ) {
			$this->export_settings( 'export' );
		}
	}
	
	/**
	 * Run settings export
	 *
	 * @return void
	 */
	private function run_export() {
		// phpcs:ignore WordPress.Security.NonceVerification.Recommended
		if ( isset( $_GET['_export_ghoster_settings'] ) ) {
			require_once( ABSPATH . 'wp-includes/pluggable.php' );
			// phpcs:ignore ET.Sniffs.ValidatedSanitizedInput.InputNotValidated
			// phpcs:ignore ET.Sniffs.ValidatedSanitizedInput.InputNotSanitized
			if ( ! wp_verify_nonce( sanitize_key( wp_unslash( $_GET['_export_ghoster_settings'] ) ), 'export_settings' ) ) {
				die( 'You are not allowed to export settings' );
			} else{
				$this->start = true;
			}
		}
	}
	
	/**
	 * Divi Ghoster settings
	 *
	 * @param array|string $settings
	 *
	 * @return string
	 */
	public function divi_ghoster_settings( $settings ) {
		if ( ! empty( $settings ) ) {
			foreach ( $settings as $setting_key => $setting_value ) {
				if ( ! isset( $this->divi_ghoster_options['ghoster_options'] ) ) {
					$this->divi_ghoster_options['ghoster_options'] = array();
				}
				
				$this->divi_ghoster_options['ghoster_options'][ $setting_key ] = $setting_value;
			}
		}
		
		return json_encode( $this->divi_ghoster_options, JSON_UNESCAPED_SLASHES );
	}
	
	
	/**
	 * Export settings
	 *
	 * @param string $action
	 */
	public function export_settings( $action = 'export' ) {
		$export_settings = $this->divi_ghoster_settings( $this->divi_ghoster_settings );
		
		if ( ! empty( $export_settings ) ) {
			header( 'Content-Type: application/json' );
			header( "Content-Disposition: attachment; filename={$this->file_name}" );
			// phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
			echo $export_settings;
		}
		
		exit();
	}
	
	/**
	 * @inheritDoc
	 */
	public function get_exported_ghoster_settings( $imported_settings ) {}
}