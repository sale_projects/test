<?php
/**
 * Export class .
 *
 * Can handle Settings Export
 */

abstract class DiviGhosterExport extends DiviGhosterSettings {
	
	/**
	 * Export settings
	 *
	 * @param string $action
	 */
	abstract public function export_settings( $action = 'export' );
}