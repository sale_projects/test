<?php
/**
 * DiviGhosterSettings class .
 *
 * Can handle Ghoster Settings
 */

abstract class DiviGhosterSettings {
	
	/** @var array $divi_ghoster_settings */
	protected $divi_ghoster_settings;
	/** @var array $divi_ghoster_options */
	protected $divi_ghoster_options;
	/** @var string $file_name */
	protected $file_name;
	/** @var bool $start */
	protected $start = false;
	/**
	 * Divi Ghoster Settings
	 *
	 * @param array|string $settings
	 *
	 * @return void
	 */
	abstract public function divi_ghoster_settings( $settings );
	
	/**
	 * Get Exported Ghoster Settings
	 *
	 * @param string $imported_settings
	 *
	 * @return void
	 */
	abstract public function get_exported_ghoster_settings( $imported_settings );
}