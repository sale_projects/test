<?php
/**
 * Import class .
 *
 * Can handle Settings Import
 */

abstract class DiviGhosterImport extends DiviGhosterSettings {
	/** @var string $type */
	protected $type = 'success';
	/** @var string $message */
	protected $message;
	/** @var string $settings_error */
	protected $settings_error_notice = 'divi_ghoster_import_notice';
	
	/**
	 * Import Divi Ghoster Settings
	 *
	 * @param string $action
	 *
	 * @return void
	 */
	abstract public function import_settings( $action = 'import' );
}