<?php

class DiviGhosterUltimate {
	private static $themeDirectoryUri, $pluginLoadOrder;
	
	public static function run() {
		
		self::$pluginLoadOrder = array_map('basename', array_map('dirname', wp_get_active_and_valid_plugins()));
	
		// Make sure all Ultimate Ghoster features are set in settings
		$missingSettings = array_diff_key( array(
			'hide_divi_related_plugins' => 1,
			'hide_divi_ghoster' => 1,
			'hide_divi_source' => 1,
			'hide_premade_layouts' => 1,
			'hide_theme_update_info' => 1,
			'hide_product_tour' => 1
		), DiviGhoster::$settings);
		if ($missingSettings) {
			DiviGhoster::$settings = array_merge(DiviGhoster::$settings, $missingSettings);
			DiviGhoster::saveSettings();
		}
	
		add_action('plugin_loaded', ['DiviGhosterUltimate', 'onPluginLoaded']);
		add_action('ds_ghoster_before_plugin_load_divi-mega-pro', function() {
			add_filter('active_plugins', ['DiviGhosterUltimate', 'fakeDiviBuilderPlugin']);
		});
		add_action('ds_ghoster_after_plugin_load_divi-mega-pro', function() {
			remove_filter('active_plugins', ['DiviGhosterUltimate', 'fakeDiviBuilderPlugin']);
		});
	
		add_action( 'permalink_structure_changed', array(
			'DiviGhosterUltimate',
			'onPermalinkStructureChange'
		), 10, 2 );
		add_action( 'template_redirect', array( 'DiviGhosterUltimate', 'templateRedirect' ) );
		add_action( 'after_setup_theme', array( 'DiviGhosterUltimate', 'onThemeSetUp' ), 9999 );
		if ( '1' === (string) DiviGhoster::$settings['hide_divi_source'] ) {
			add_filter( 'template_directory_uri', array( 'DiviGhosterUltimate', 'filterThemeDirectoryUri' ), 10, 3 );
			add_filter( 'stylesheet_directory_uri', array( 'DiviGhosterUltimate', 'filterThemeDirectoryUri' ), 10, 3 );
		}
		
		add_filter( 'script_loader_tag', array( 'DiviGhosterUltimate', 'filterScriptTag' ), 10, 2 );
		add_filter( 'style_loader_tag', array( 'DiviGhosterUltimate', 'filterScriptTag' ), 10, 2 );
		add_filter( 'plugins_api_result', array( 'DiviGhosterUltimate', 'filterPluginsApiResult' ) );
		add_filter( 'wp_prepare_themes_for_js', array( 'DiviGhosterUltimate', 'filterThemesList' ) );
		
		if ( '1' === (string) DiviGhoster::$settings['hide_divi_related_plugins'] || '1' === (string) DiviGhoster::$settings['hide_divi_ghoster'] ) {
			add_filter( 'all_plugins', array( 'DiviGhosterUltimate', 'filterPluginsList' ) );
		}
		if ( '1' === (string) DiviGhoster::$settings['hide_theme_update_info'] ) {
			add_filter( 'site_transient_update_themes', array(
				'DiviGhosterUltimate',
				'disableThemeUpdateInfo'
			), 9999 );
		}
		
		global $pagenow;
		// phpcs:ignore WordPress.Security.NonceVerification.Recommended
		// phpcs:ignore WordPress.Security.NonceVerification.Missing
		if ( $pagenow != 'options.php' || empty( $_POST['option_page'] ) || $_POST['option_page'] != 'agsdg_pluginPage' ) {
			// Add the Ultimate Ghoster rewrite rule, except when we're saving plugin options
			add_action( 'init', array( 'DiviGhosterUltimate', 'rewriteRule' ) );
		}
		
		if ( is_admin() ) {
			if ( $pagenow == 'plugin-editor.php' ) {
				add_action( 'init', array( 'DiviGhosterUltimate', 'modifyCachedPluginsInfoForPluginsPage' ) );
			} else if ( $pagenow == 'update-core.php' ) {
				add_action( 'init', array( 'DiviGhosterUltimate', 'modifyCachedPluginsInfoForUpdatesPage' ) );
			// phpcs:ignore WordPress.Security.NonceVerification.Recommended -- just checking the action variable so that we can add a filter
			} else if ( $pagenow == 'update.php' && isset( $_REQUEST['action'] ) && ( $_REQUEST['action'] == 'update-selected' || $_REQUEST['action'] == 'upgrade-plugin' ) ) {
				ob_start( array( 'DiviGhosterUltimate', 'processPluginUpdaterOutput' ), 2, false );
				set_error_handler( array( 'DiviGhosterUltimate', 'processPluginUpdaterOutputErrors' ), E_NOTICE );
			}
			
			// Set up theme global variable based on Ultimate Ghoster theme (copied from DiviGhoster::run function)
			DiviGhoster::$targetTheme = get_option( 'adsdg_ultimate_theme' );
			if ( false === DiviGhoster::$targetTheme ) {
				DiviGhoster::$targetTheme = 'Divi';
			}
			DiviGhoster::$targetThemeSlug = strtolower( DiviGhoster::$targetTheme );
		}
		
		
		// Enable Divi 100
		// TODO: Fix this (was uncommented in last release version)
		/*function et_divi_100_is_active() {
			global DiviGhoster::$targetTheme;
			return (DiviGhoster::$targetTheme == 'Divi');
		}*/
		
		if ( ! is_admin() ) {
			include_once( __DIR__ . '/antibot.php' );
		}
		
	}
	
	/**
	 * Remove symbolic link for Divi theme if is not in use
	 *
	 * @param string $themesRoot
	 * @param string $ghostSlug
	 *
	 * @return void
	 */
	public static function removeUnnecessarySymbolicLinksForDiviTheme( $themesRoot, $ghostSlug ) {
		$dirs = array_diff( scandir( $themesRoot ), [ '..', '.' ] );
		
		foreach ( $dirs as $dir ) {
			if ( is_link( $themesRoot . '/' . $dir ) ) {
				if ( $ghostSlug !== $dir ) {
					DiviGhoster::addLogEntry( sprintf( __('Removing an unnecessary theme symbolic link: %s', 'divi-ghoster'), $dir ) );
					unlink( $themesRoot . '/' . $dir );
				}
			}
		}
	}
	
	public static function enable( $ghostSlug, $ghostName ) {
		DiviGhoster::addLogEntry( __('Enabling Ultimate Ghoster', 'divi-ghoster') );
		
		if ( self::isEnabled() ) {
			// jh-todo could you check if translations are ok?
			// TODO issue with Extra theme, needs to fix Extra theme compatibility, now works but
			// it's not complete,
			//throw new Exception( esc_html__( 'There is an existing Ultimate Ghoster instance.', 'divi-ghoster' ) );
			
			DiviGhoster::addLogEntry( __('Ultimate Ghoster was already enabled', 'divi-ghoster') );
		}
		
		self::changeAllChildThemes( $ghostSlug );
		self::changeThemeMeta( $ghostName );
		self::rewriteRule( $ghostSlug );
		
		// Make symlink - will fail silently if it already exists
		$themesRoot = get_theme_root();
		
		@symlink( $themesRoot . '/' . DiviGhoster::$targetTheme, $themesRoot . '/' . $ghostSlug );
		self::removeUnnecessarySymbolicLinksForDiviTheme( $themesRoot, $ghostSlug );
		// Implement fallback if needed
		self::maybeAddFallback( $ghostSlug );
		
		if ( ! DiviGhosterAdmin::check_if_server_is_nginx() ) {
			flush_rewrite_rules();
		}
	}
	
	public static function disable( $ghostSlug ) {
		DiviGhoster::addLogEntry( __('Disabling Ultimate Ghoster', 'divi-ghoster') );
		
		self::restoreAllChildThemes();
		self::restoreThemeMeta();
		self::removeFallback();
		
		self::removeThemeSymlink( $ghostSlug );
		
		flush_rewrite_rules();
		
		if ( self::isEnabled() ) {
			throw new Exception( __('isEnabled is still true after disabling Ultimate Ghoster', 'divi-ghoster') );
		}
	}
	
	// Disable theme update info
	public static function disableThemeUpdateInfo( $value ) {
		if ( isset( $value ) && is_object( $value ) ) {
			unset( $value->response[ DiviGhoster::$targetTheme ] );
		}
	}
	
	// Checks whether Ultimate appears to be enabled on the site, regardless of the setting value
	public static function isEnabled() {
		return file_exists( get_theme_root() . '/' . DiviGhoster::$targetTheme . '/style.pre_agsdg.css' );
	}
	
	// Remove specific listings from plugins page
	public static function filterPluginsList( $plugins, $invert = false ) {
		foreach ( $plugins as $plugin => $v ) {
			
			$pluginLc = strtolower( $plugin );
			// substr($pluginLc, 0, 5) == 'divi-' handles Divi Ghoster
			// strpos($pluginLc, '/divi-') handles Aspen Footer Editor, Divi Overlays, Divi Switch
			// '/ds-divi-' handles Divi Breadrumbs, Divi Extras, Divi Icon Party
			// '/wp-and-divi' handles WP and Divi Icons
			// TODO add PBE!
			if ( substr( $pluginLc, 0, 12 ) === 'divi-ghoster' ) {
				$divi_ghoster = $plugin;
				if ( 1 === DiviGhoster::$settings['hide_divi_ghoster'] ) {
					unset( $plugins[ $divi_ghoster ] );
				} else {
					continue;
				}
			}
			if ( 1 === DiviGhoster::$settings['hide_divi_related_plugins'] ) {
				if ( ( substr( $pluginLc, 0, 5 ) == 'divi-' || strpos( $pluginLc, '/divi-' ) !== false || strpos( $pluginLc, '/ds-divi-' ) !== false || strpos( $pluginLc, '/wp-and-divi' ) !== false || strpos( $pluginLc, '/pbe' ) !== false || strpos( $pluginLc, '/divi-' ) !== false ) xor $invert ) {
					unset( $plugins[ $plugin ] );
				}
			}
			
		}
		
		return $plugins;
	}
	
	public static function rewriteRule( $slug = null ) {
		/*
		if ( ! DiviGhosterAdmin::check_if_server_is_nginx() ) {
			if ( empty( $slug ) ) {
				add_rewrite_rule( 'wp-content/themes/' . DiviGhoster::$settings['theme_slug'] . '/(.*)$', 'wp-content/themes/' . DiviGhoster::$targetTheme . '/$1' );
			} else {
				add_rewrite_rule( 'wp-content/themes/' . $slug . '/(.*)$', 'wp-content/themes/' . DiviGhoster::$targetTheme . '/$1' );
			}
		}
		*/
	}
	
	// If $childTheme, then $newThemeName is the theme slug, not the branding name
	public static function changeThemeMeta( $newThemeName, $childTheme = false ) {
		DiviGhoster::addLogEntry(
			sprintf(
				__('Changing meta for theme %s (is child theme? - %s)', 'divi-ghoster'),
				$newThemeName,
				$childTheme ? __('Yes', 'divi-ghoster') : __('No', 'divi-ghoster')
			)
		);
		
		$themeRoot      = ( empty( $childTheme ) ? get_theme_root() . '/' . DiviGhoster::$targetTheme . '/' : $childTheme->get_stylesheet_directory() . '/' );
		$stylesheetFile = $themeRoot . 'style.css';
		// Backup original stylesheet
		//!file_exists($themeRoot.'style.pre_agsdg.css');
		if ( ! file_exists( $themeRoot . 'style.pre_agsdg.css' ) ) {
			if ( ! @copy( $stylesheetFile, $themeRoot . 'style.pre_agsdg.css' ) ) {
				throw new Exception( esc_html__( 'Unable to back up original theme stylesheet.', 'divi-ghoster' ) );
			}
		}
		
		if (!$childTheme) {
			$originalHeader = get_file_data( $themeRoot . 'style.pre_agsdg.css', ['ThemeName' => 'Theme Name'], 'theme' );
			if ( empty( $originalHeader['ThemeName'] ) ) {
				throw new Exception( esc_html__( 'Unable to read theme name from original stylesheet header.', 'divi-ghoster' ) );
			}
			update_option('agsdg_ghosted_theme', $originalHeader['ThemeName']);
			DiviGhoster::addLogEntry(
				sprintf(
					__('Detected ghosted theme: %s', 'divi-ghoster'),
					$originalHeader['ThemeName']
				)
			);
		}
		
		$stylesheetContents = @file_get_contents( $themeRoot . 'style.pre_agsdg.css' );
		if ( $stylesheetContents === false ) {
			throw new Exception( esc_html__( 'Unable to retrieve theme stylesheet contents.', 'divi-ghoster' ) );
		}
		
		// Normalize line endings in stylesheet
		$stylesheetContents = str_replace( array( "\r\n", "\r" ), "\n", $stylesheetContents );
		
		$commentStartPos = strpos( $stylesheetContents, '/*' );
		$commentEndPos   = strpos( $stylesheetContents, '*/' );
		if ( $commentStartPos === false || $commentEndPos === false ) {
			throw new Exception( esc_html__( 'Theme stylesheet header seems to be missing.', 'divi-ghoster' ) );
		}
		
		$comment     = trim( substr( $stylesheetContents, $commentStartPos + 2, ( $commentEndPos - $commentStartPos ) ) );
		$newComment  = '';
		$themeName   = '';
		$authorName  = '';
		$parentTheme = DiviGhoster::$targetTheme;
		//$newComment2 = '';
		if ( isset( DiviGhoster::$settings['theme_name'] ) ) {
			$themeName = DiviGhoster::$settings['theme_name'];
		}
		if ( isset( DiviGhoster::$settings['author_name'] ) ) {
			$authorName = DiviGhoster::$settings['author_name'];
		}
		
		foreach ( explode( "\n", $comment ) as $commentLine ) {
			$commentLine = trim( $commentLine );
			if ( empty( $commentLine ) ) {
				continue;
			}
			if ( $commentLine[0] == '*' || $commentLine[0] == '#' ) {
				$commentLine = trim( substr( $commentLine, 1 ) );
			}
			
			$colonPos = strpos( $commentLine, ':' );
			if ( $colonPos !== false ) {
				$beforeColon = substr( $commentLine, 0, $colonPos );
				if ( $beforeColon == 'Theme Name' ) {
					$newComment .= 'Theme Name: ' . $themeName . "\n";
				}
				if ( $beforeColon === 'Author' ) {
					$newComment .= 'Author: ' . $authorName . "\n";
					
				} else if ( $beforeColon == 'Version' || $beforeColon == 'License' || $beforeColon == 'License URI' ) {
					$newComment .= $commentLine . "\n";
				} /*else if ($beforeColon != 'Description' && $beforeColon != 'Tags') {
						$newComment2 .= 'Original '.$beforeColon.' - '.substr($commentLine, $colonPos + 1)."\n";
					}*/
				if ( $beforeColon === 'Template' ) {
					$newComment .= 'Template: ' . $newThemeName . "\n";
				}
			}
		}
		
		$stylesheetContents =
			( $commentStartPos > 0 ? substr( $stylesheetContents, 0, $commentStartPos ) : '' )
			. "/*\n" . $newComment . "*/\n"
			. ( empty( $childTheme )
				// Leaving this text in English (no translation) since the licensing text is also English
				? '/* For copyright information, see the ./LICENSE.md file (in this directory). This file was modified ' . gmdate( 'Y-m-d' ) . ' by Aspen Grove Studios to customize metadata in header comment and add this line. */'
				: '/* This file was modified ' . gmdate( 'Y-m-d' ) . ' by Aspen Grove Studios to customize metadata in header comment */' )
			. substr( $stylesheetContents, $commentEndPos + 2 );
		//.(empty($newComment2) ? '' : "\n/*\n".$newComment2.'*/');
		
		// Add new header, save stylesheet
		if ( ! file_put_contents( $stylesheetFile, $stylesheetContents ) ) {
			throw new Exception( esc_html__( 'Unable to save updated theme stylesheet.', 'divi-ghoster' ) );
		}
	}
	
	public static function changeAllChildThemes( $newThemeSlug ) {
		
		if ( ! empty( DiviGhoster::$settings['theme_name'] ) ) {
			$targetTheme = DiviGhoster::$settings['theme_name'];
		}
		
		foreach ( wp_get_themes() as $theme ) {
			if ( false === $theme->parent() ) {
				continue;
			}
			if ( $theme->parent() == DiviGhoster::$targetTheme ) {
				self::changeThemeMeta( $newThemeSlug, $theme );
			} else {
				if ( $theme->parent() == $targetTheme ) {
					self::changeThemeMeta( $newThemeSlug, $theme );
				}
			}
		}
	}
	
	public static function restoreAllChildThemes() {
		if ( ! empty( DiviGhoster::$settings['theme_name'] ) ) {
			$targetTheme = DiviGhoster::$settings['theme_name'];
		}
		foreach ( wp_get_themes() as $theme ) {
			if ( false === $theme->parent() ) {
				continue;
			}
			if ( $theme->parent() === DiviGhoster::$settings['theme_name'] ) {
				self::restoreThemeMeta( $theme );
			}
			if ( $theme->parent() == DiviGhoster::$targetTheme ) {
				self::restoreThemeMeta( $theme );
			} else {
				if ( $theme->parent() == $targetTheme ) {
					self::restoreThemeMeta( $theme );
				}
			}
		}
	}
	
	private static function deleteDiviGhosterTraces( $childThemeRoot, $parentThemeRoot, $tracesFor = 'parent' ) {
		if ( 'child' === $tracesFor ) {
			@rename( $childThemeRoot . 'style.pre_agsdg.css', $childThemeRoot . 'style.css' );
		}
		
		@rename( $parentThemeRoot . 'style.pre_agsdg.css', $parentThemeRoot . 'style.css' );
	}
	
	public static function restoreThemeMeta( $childTheme = null ) {
		DiviGhoster::addLogEntry(
			sprintf(
				__('Restoring meta for theme %s (is child theme? - %s)', 'divi-ghoster'),
				$newThemeName,
				$childTheme ? __('Yes', 'divi-ghoster') : __('No', 'divi-ghoster')
			)
		);
		
		$themeRoot       = ( empty( $childTheme ) ? get_theme_root() . '/' . DiviGhoster::$targetTheme . '/' : $childTheme->get_stylesheet_directory() . '/' );
		$parentThemeRoot = get_theme_root() . '/' . DiviGhoster::$targetTheme . '/';
		
		self::deleteDiviGhosterTraces( $themeRoot, $parentThemeRoot, 'child' );
		self::deleteDiviGhosterTraces( $themeRoot, $parentThemeRoot );
		
		
		if (!$childTheme) {
			delete_option('agsdg_ghosted_theme');
		}
	}
	
	public static function onUpdateInstalled( $childTheme ) {
			
			if ( $childTheme ) {
				if ( empty( DiviGhoster::$settings['theme_slug'] ) ) {
					throw new Exception();
				}
				self::changeThemeMeta( DiviGhoster::$settings['theme_slug'], $childTheme );
			} else {
				if ( empty( DiviGhoster::$settings['branding_name'] ) ) {
					throw new Exception();
				}
				self::changeThemeMeta( DiviGhoster::$settings['branding_name'], false );
			}
			
			self::removeFallback();
			self::maybeAddFallback();
		
	}
	
	public static function filterThemeDirectoryUri( $uri, $template, $themeRootUri ) {
		if ( strcasecmp( $template, DiviGhoster::$targetTheme ) == 0 ) {
			if ( ! isset( self::$themeDirectoryUri ) ) {
				self::$themeDirectoryUri = $themeRootUri . '/' . DiviGhoster::$settings['theme_slug'];
			}
			
			return self::$themeDirectoryUri;
		}
		
		return $uri;
	}
	
	public static function onPermalinkStructureChange( $oldStructure, $newStructure ) {
		DiviGhoster::addLogEntry(
			sprintf(
				__('Permalink structure changed to: %s', 'divi-ghoster'),
				$newStructure
			)
		);
		
		
		// Prevent the user from changing the permalink structure to Plain while Ultimate Ghoster is enabled
		if ( empty( $newStructure ) ) {
			global $wp_rewrite;
			$wp_rewrite->set_permalink_structure( $oldStructure );
		}
	}
	
	public static function removeThemeSymlink( $slug ) {
		$themesRoot = get_theme_root();
		$linkTarget = @readlink( $themesRoot . '/' . $slug );
		
		return ( ! ( $linkTarget && realpath( $linkTarget ) == realpath( $themesRoot . '/' . DiviGhoster::$targetTheme ) ) || @unlink( $themesRoot . '/' . $slug ) );
	}
	
	public static function templateRedirect() {
		// Intercept 404 and determine whether it is a theme file
		if ( is_404() ) {
			$themeRoot    = parse_url( get_template_directory_uri(), PHP_URL_PATH );
			$themeRootLen = strlen( $themeRoot );
			if ( isset( $_SERVER['REQUEST_URI'] ) ) {
				if ( strlen( esc_url_raw($_SERVER['REQUEST_URI']) ) >= $themeRootLen
				     && substr( esc_url_raw($_SERVER['REQUEST_URI']), 0, $themeRootLen ) == $themeRoot
				     && strpos( esc_url_raw($_SERVER['REQUEST_URI']), '..' ) === false ) {
					
					$qPos      = strpos( esc_url_raw($_SERVER['REQUEST_URI']), '?' );
					$themeFile = get_template_directory() . ( $qPos === false ? substr( esc_url_raw($_SERVER['REQUEST_URI']), $themeRootLen ) : substr( esc_url_raw($_SERVER['REQUEST_URI']), $themeRootLen, $qPos - $themeRootLen ) );
					
					if ( ! file_exists( $themeFile ) ) {
						return;
					}
					
					if ( substr( $themeFile, - 4 ) != '.php' ) {
						/*
						if ( function_exists( 'mime_content_type' ) ) {
							$mimeType = mime_content_type( $themeFile );
						}
						*/
						if ( empty( $mimeType ) ) {
							$dotPos = strrpos( $themeFile, '.' );
							if ( $dotPos === false ) {
								$mimeType = 'application/octet-stream';
							} else {
								switch ( strtolower( substr( $themeFile, $dotPos + 1 ) ) ) {
									case 'htm':
									case 'html':
										$mimeType = 'text/html';
										break;
									case 'txt':
										$mimeType = 'text/plain';
										break;
									case 'js':
										$mimeType = 'application/javascript';
										break;
									case 'css':
										$mimeType = 'text/css';
										break;
									case 'xml':
										$mimeType = 'text/xml';
										break;
									case 'png':
										$mimeType = 'image/png';
										break;
									case 'jpg':
									case 'jpeg':
										$mimeType = 'image/jpeg';
										break;
									case 'gif':
										$mimeType = 'image/gif';
										break;
									default:
										$mimeType = 'application/octet-stream';
								}
							}
						}
						
						header( 'HTTP/1.0 200 OK' );
						header( 'Content-Type: ' . $mimeType );
						readfile( $themeFile );
						exit;
					}
				}
			}
		}
	}
	
	public static function maybeAddFallback( $themeSlug = null ) {
		if ( $themeSlug === null ) {
			$themeSlug = DiviGhoster::$settings['theme_slug'];
		}
		
		// Check if style.css can be retrieved successfully using the new theme slug
		$result = @file_get_contents( get_theme_root_uri() . '/' . $themeSlug . '/style.css', false, null, 0, 1 );
		if ( $result !== false ) {
			DiviGhoster::addLogEntry( __('Fallback is not needed', 'divi-ghoster') );
			return;
		}
		
		DiviGhoster::addLogEntry( __('Implementing fallback', 'divi-ghoster') );
		
		// Fetching the stylesheet failed, so we need to implement the fallback
		WP_Filesystem();
		$newDir = get_theme_root() . DIRECTORY_SEPARATOR . $themeSlug;
		if ( is_link( $newDir ) ) {
			unlink( $newDir );
		}
		if ( ! @mkdir( $newDir ) || !( self::directCopy( get_template_directory(), $newDir ) || copy_dir( get_template_directory(), $newDir ) ) ) {
			throw new Exception( esc_html__( 'Unable to create copy of theme.', 'divi-ghoster' ) );
		}
	}
	
	/* Remove fallback if it was implemented */
	public static function removeFallback() {
		global $wp_filesystem;
		
		$newDir = get_theme_root() . DIRECTORY_SEPARATOR . DiviGhoster::$settings['theme_slug'];
		WP_Filesystem();
		if ( is_dir($newDir) && !( self::directDelete($newDir) || $wp_filesystem->rmdir( $newDir, true ) ) ) {
			throw new Exception( esc_html__( 'Unable to remove copy of theme.', 'divi-ghoster' ) );
		}
	}
	
	private static function directCopy($source, $target) {
		exec( PHP_SHLIB_SUFFIX == 'dll' ? 'copy /Y "'.$source.'" "'.$target.'"' : 'cp -rf "'.$source.'" "'.$target.'"', $output, $result );
		DiviGhoster::addLogEntry( sprintf( __('Direct copy result: %s', 'divi-ghoster'), $result ) );
		return !$result;
	}
	
	private static function directDelete($target) {
		exec( PHP_SHLIB_SUFFIX == 'dll' ? 'del /F /Q "'.$target.'"' : 'rm -rf "'.$target.'"', $output, $result );
		DiviGhoster::addLogEntry( sprintf( __('Direct delete result: %s', 'divi-ghoster'), $result ) );
		return !$result;
	}
	
	// Hide custom slug from themes list
	public static function filterThemesList( $themes ) {
		if ( isset( $themes[ DiviGhoster::$settings['theme_slug'] ] ) ) {
			unset( $themes[ DiviGhoster::$settings['theme_slug'] ] );
		}
		
		return $themes;
	}
	
	public static function modifyCachedPluginsInfoForPluginsPage() {
		// Remove theme-related plugins from the cache so they don't show up in the plugin editor list
		get_plugins();
		$plugins     = wp_cache_get( 'plugins', 'plugins' );
		$plugins[''] = self::filterPluginsList( $plugins[''] );
		wp_cache_set( 'plugins', $plugins, 'plugins' );
	}
	
	public static function modifyCachedPluginsInfoForUpdatesPage() {
		// Rebrand plugins in updates page
		get_plugins();
		$plugins = wp_cache_get( 'plugins', 'plugins' );
		
		foreach ( self::filterPluginsList( $plugins[''], true ) as $pluginFile => $pluginData ) {
			if ( $pluginData['Name'] == 'Divi Ghoster' ) {
				$newName = DiviGhoster::$settings['branding_name'] . ' Addon';
			} else {
				$newName = preg_replace( '/(Divi' . ( DiviGhoster::$targetTheme == 'Divi' ? '' : '|' . preg_quote( DiviGhoster::$targetTheme, '/' ) ) . ')\b/i', DiviGhoster::$settings['branding_name'], $pluginData['Name'] );
			}
			if ( $newName != $plugins[''][ $pluginFile ]['Name'] ) {
				$plugins[''][ $pluginFile ]['Name'] = $newName;
			}
		}
		wp_cache_set( 'plugins', $plugins, 'plugins' );
	}
	
	public static function filterPluginsApiResult( $result ) {
		/*$pluginDir = $result->slug.'/';
		$pluginDirLen = strlen($pluginDir);
		$pluginFile = $result->slug.'.php';

		foreach (agsdg_hidden_plugins() as $hiddenPlugin => $t) {
			if ($hiddenPlugin == $pluginFile || substr($hiddenPlugin, 0, $pluginDirLen) == $pluginDir) {
				$isHidden = true;
				break;
			}
		}*/
		
		// TODO: Following line throws a warning on add plugin page
		
		$pluginLc = strtolower( $result->slug );
		if ( substr( $pluginLc, 0, 5 ) == 'divi-' || strpos( $pluginLc, '/divi-' ) !== false ) { // Plugin is hidden
			if ( $result->name == 'Divi Ghoster' ) {
				$result->name = DiviGhoster::$settings['branding_name'] . ' Addon';
			} else {
				$result->name = preg_replace( '/(Divi' . ( DiviGhoster::$targetTheme == 'Divi' ? '' : '|' . preg_quote( DiviGhoster::$targetTheme, '/' ) ) . ')\b/i', DiviGhoster::$settings['branding_name'], $result->name );
			}
			$result->author         = '';
			$result->author_profile = '';
			$result->donate_link    = '';
			$result->banners        = array();
			$result->homepage       = '';
			$result->contributors   = array();
			$result->sections       = array( 'description' => esc_html__( 'See version and compatibility details to the right, if applicable.', 'divi-ghoster' ) );
		}
		
		return $result;
	}
	
	public static function processPluginUpdaterOutput( $outputBuffer ) {
		if ( substr( trim( $outputBuffer ), 0, 8 ) == '<iframe ' ) {
			return $outputBuffer;
		}
		$originalText = esc_html__('Downloading update from', 'divi-ghoster');
		if ( strpos( $outputBuffer, $originalText ) !== false ) {
			$outputBuffer = preg_replace( '#'.preg_quote($originalText).'(.*)&#8230;#U', esc_html__('Downloading update', 'divi-ghoster').'&#8230;', $outputBuffer );
		}
		
		return preg_replace( '/(Divi' . ( DiviGhoster::$targetTheme == 'Divi' ? '' : '|' . preg_quote( DiviGhoster::$targetTheme, '/' ) ) . ')\b/i', DiviGhoster::$settings['branding_name_esc'], str_replace( DiviGhoster::$settings['branding_name_esc'] . ' Ghoster', DiviGhoster::$settings['branding_name_esc'] . ' Addon', $outputBuffer ) );
	}
	
	public static function processPluginUpdaterOutputErrors( $level, $message ) {
		// Suppress notices related to output buffering
		if ( strpos( $message, 'processPluginUpdaterOutput' ) === false ) {
			return false;
		}
	}
	
	public static function onThemeSetUp() {
		global $l10n;
		// Copy the theme's text domain to the new theme name
		if ( ! empty( $l10n[ DiviGhoster::$targetTheme ] ) ) {
			// phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited
			$l10n[ DiviGhoster::$settings['branding_name'] ] = &$l10n[ DiviGhoster::$targetTheme ];
		}
	}
	
	public static function filterScriptTag( $tag, $handle ) {
		if ( stripos( $handle, DiviGhoster::$targetThemeSlug ) !== false ) {
			$newHandle = str_ireplace( array(
				DiviGhoster::$targetThemeSlug . '-',
				'-' . DiviGhoster::$targetThemeSlug
			), array(
				DiviGhoster::$settings['theme_slug'] . '-',
				'-' . DiviGhoster::$settings['theme_slug']
			), $handle );
			if ( $newHandle != $handle ) {
				$tag = str_ireplace( array( 'id=\'' . $handle, 'id="' . $handle ), array(
					'id=\'' . $newHandle,
					'id="' . $newHandle
				), $tag );
			}
		}
		
		return $tag;
	}
	
	static function onPluginLoaded($plugin) {
		// Not sure why, but apparently $plugin can be an object, maybe it gets overwritten by certain plugins?
		if (is_string($plugin)) {
			$plugin = basename( dirname($plugin) );
			do_action('ds_ghoster_after_plugin_load_'.$plugin);
			
			$pos = array_search( $plugin, self::$pluginLoadOrder );
			if (++$pos < count(self::$pluginLoadOrder)) {
				do_action('ds_ghoster_before_plugin_load_'.self::$pluginLoadOrder[$pos]);
			}
		}
	}

	static function fakeDiviBuilderPlugin($plugins) {
		$plugins[] = 'divi-builder/divi-builder.php';
		return $plugins;
	}
}
