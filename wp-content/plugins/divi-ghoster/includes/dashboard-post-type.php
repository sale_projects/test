<?php

/**
 * DiviGhosterDashboardPostType class .
 *
 * Can handle custom post type creation for Dashboard
 */
class DiviGhosterDashboardPostType {
	
	public function setup() {
		add_action( 'init', array( $this, 'register_divi_ghoster_dashboard_layout_post_type' ), 99 );
	}
	
	/**
	 * Register Dashboard Layout post type
	 *
	 * @return void
	 */
	public function register_divi_ghoster_dashboard_layout_post_type() {
		$args = array(
			'label'               => __( 'dashboard_layout', DiviGhoster::PLUGIN_SLUG ),
			'description'         => __( 'Custom dashboard layouts', DiviGhoster::PLUGIN_SLUG ),
			'labels'              => $this->car_post_type_labels(),
			'supports'            => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'revisions', 'custom-fields', ),
			'taxonomies'          => array( 'dashboard-layout' ),
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_nav_menus'   => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => 5,
			'can_export'          => true,
			'has_archive'         => true,
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
			'capability_type'     => 'post',
			'show_in_rest' => true,
		);
		
		register_post_type( 'dashboard_layout', $args );
	}
	
	/**
	 * Dashboard Layout post type labels
	 *
	 * @return array
	 */
	private function car_post_type_labels() {
		return array(
			'name'                => _x( 'Dashboard Layouts', DiviGhoster::PLUGIN_SLUG ),
			'singular_name'       => _x( 'Dashboard Layout', DiviGhoster::PLUGIN_SLUG ),
			'menu_name'           => __( 'Dashboard Layouts', DiviGhoster::PLUGIN_SLUG ),
			'parent_item_colon'   => __( 'Parent Dashboard Layout', DiviGhoster::PLUGIN_SLUG ),
			'all_items'           => __( 'All Dashboard Layouts', DiviGhoster::PLUGIN_SLUG ),
			'view_item'           => __( 'View Dashboard Layout', DiviGhoster::PLUGIN_SLUG ),
			'add_new_item'        => __( 'Add New Dashboard Layout', DiviGhoster::PLUGIN_SLUG ),
			'add_new'             => __( 'Add New', DiviGhoster::PLUGIN_SLUG ),
			'edit_item'           => __( 'Edit Dashboard Layout', DiviGhoster::PLUGIN_SLUG ),
			'update_item'         => __( 'Update Dashboard Layout', DiviGhoster::PLUGIN_SLUG ),
			'search_items'        => __( 'Search Dashboard Layout', DiviGhoster::PLUGIN_SLUG ),
			'not_found'           => __( 'Not Found', DiviGhoster::PLUGIN_SLUG ),
			'not_found_in_trash'  => __( 'Not found in Trash', DiviGhoster::PLUGIN_SLUG ),
		);
	}
}