<?php

class DiviGhosterCustomizerBase {
	
	/**
	 * @var string custom post type slug
	 */
	private static $customizer_template_post_type = 'agsdb_custom_temp';
	
	/**
	 * @var string custom post type rewrite slug
	 */
	private static $hidden_cpt_rewrite_slug = 'agsdg_cpt';
	
	public static function setup() {
		self::create_template_cpt();
		if (is_admin()) {
			self::maybe_reset_rewrites();
		}
		self::add_custom_login_options();
	}
	
	/**
	 * Add custom options for custom login
	 *
	 * @return void
	 */
	private static function add_custom_login_options() {
		if ( false === get_option( 'divi_ghoster_custom_login_options' ) ) {
			add_option( 'divi_ghoster_custom_login_options', [] );
		}
	}
	
	/**
	 * make sure the cpt used for customizer templates does not have rewrite entries
	 * force them to be rewritten if not
	 * this is to hide the CPT that was visible in prior versions of Divi Ghoster
	 * tested to confirm this is needed even after disabling CPT rewrite
	 *
	 * @return void
	 * @since: 3.0.0
	 *
	 */
	private static function maybe_reset_rewrites() {
		$rewrites = get_option( 'rewrite_rules' );
		if ( ! empty( $rewrites ) ) {  //prevent array_keys() warning
			$matches = preg_grep( '/^' . self::$hidden_cpt_rewrite_slug . '/i', array_keys( $rewrites ) );
			if ( !empty( $matches ) && function_exists('flush_rewrite_rules') ) {
				flush_rewrite_rules();
			}
		}
	}
	
	/**
	 * register the custom post type we use for keeping our customizer pages safe
	 *
	 * @return void
	 * @since: 3.0.0
	 *
	 */
	private static function create_template_cpt() {
		
		$public = isset($_GET['agsdg_nonce']) && wp_verify_nonce($_GET['agsdg_nonce'], 'agsdg-customize-login');
		
		register_post_type(
			self::$customizer_template_post_type,
			array(
				// it seems that labels don't need to be escaped (e.g. esc_attr is called before they are used in WP admin menu)
				'label'               => sprintf( __( '%s Custom Login', 'divi-ghoster' ), DiviGhoster::$settings['branding_name'] ),
				'description'         => esc_html__( 'a hidden cpt for assigning page templates to special customizer pages', 'divi-ghoster' ),
				'public'              => $public,
				'exclude_from_search' => true,
				'publicly_queryable'  => $public,
				'show_ui'             => false,
				'show_in_nav_menus'   => false,
				'show_in_rest'        => false,
				'can_export'          => false,
				'capability_type'     => 'page',
				'rewrite'             => false,
			)
		);
	}
	
	/**
	 * get the page ID that will be used for customizing the builder
	 *
	 * @return int|WP_Error
	 * @since: 3.0.0
	 *
	 */
	public static function get_customizer_template_page_id( $template_page_title, $template_context ) {
		$template_page = get_posts( array(
			'numberposts' => 1,
			'meta_key'    => '_wp_page_template',
			'meta_value'  => $template_page_title,
			'post_type'   => self::$customizer_template_post_type
		) );
		
		if ( empty( $template_page ) ) {
			return self::create_builder_customizer_post( $template_page_title, $template_context );
		}
		
		return $template_page[0]->ID;
	}
	
	/**
	 * create a page that will use our template within the customizer
	 *
	 * @return int|WP_Error
	 * @since: 3.0.0
	 *
	 */
	private static function create_builder_customizer_post( $template_page_title, $template_context ) {
		add_filter( 'pre_wp_unique_post_slug', array( __CLASS__, 'override_slug' ), 10, 5 );
		$builder_template_post = wp_insert_post(
			array(
				'post_author'  => 0,
				'post_status'  => 'publish',
				'post_type'    => self::$customizer_template_post_type,
				'post_title'   => sprintf( esc_html__( 'Divi Ghoster Custom  %s Template', 'divi-ghoster' ), $template_context ),
				'post_content' => esc_html__( 'This is a post for Login Customizer. Do not remove it! it will be not available for your users.', 'divi-ghoster' ),
			)
		);
		update_post_meta( $builder_template_post, '_wp_page_template', $template_page_title );
		
		remove_filter( 'pre_wp_unique_post_slug', array( __CLASS__, 'override_slug' ) );
		
		return $builder_template_post;
	}
	
	/**
	 * specify our own slug for the new page
	 *
	 * @param $default_slug
	 * @param $slug
	 * @param $post_ID
	 * @param $post_status
	 * @param $post_type
	 *
	 * @return string
	 * @since: 3.0.0
	 *
	 */
	public static function override_slug( $default_slug, $slug, $post_ID, $post_status, $post_type ) {
		if ( self::$customizer_template_post_type !== $post_type ) {
			return $default_slug;
		}
		
		return $slug;
	}
}

add_action( 'after_setup_theme', array( 'DiviGhosterCustomizerBase', 'setup' ) );

