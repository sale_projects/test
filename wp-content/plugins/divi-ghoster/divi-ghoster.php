<?php
/*
 * Plugin Name:       Divi Ghoster
 * Plugin URI:        https://aspengrovestudios.com/product/divi-ghoster/
 * Description:       White label Divi and Extra with your own brand.
 * Version:           5.0.26
 * Author:            Aspen Grove Studios
 * Author URI:        https://aspengrovestudios.com/
 * Text Domain:       divi-ghoster
 * License:           GPLv3
 * License URI:       https://www.gnu.org/licenses/gpl-3.0.en.html
 * GitLab Plugin URI: https://gitlab.com/aspengrovestudios/divi-ghoster/
 * AGS Info: ids.aspengrove 2948 ids.divispace 2948 legacy.key AGS_GHOSTER_license_key legacy.status AGS_GHOSTER_license_status adminPage admin.php?page=divi_ghoster docs https://support.aspengrovestudios.com/article/406-divi-ghoster
 */

update_option( 'AGS_GHOSTER_license_status', 'valid' );
update_option('AGS_GHOSTER_license_key', '*********');
/*
Despite the following, this project is licensed exclusively
under GNU General Public License (GPL) version 3 (no future versions).
This statement modifies the following text.

Divi Ghoster plugin
Copyright (C) 2020 Aspen Grove Studios

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.


This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

========

Credits:

This plugin includes code based on and/or copied from parts of the Divi theme, copyright Elegant Themes,
released under the GNU General Public License (GPL) version 2 and/or "2 or later", used in this project under GPL
version 3 (see ./license.txt file).


This plugin includes code based on and/or copied from parts of WordPress
by Automattic, released under the GNU General Public License (GPL) version 2 or later,
licensed under GPL version 3 (see ./license.txt file).


=======

Note:

Divi is a registered trademark of Elegant Themes, Inc. This product is
not affiliated with nor endorsed by Elegant Themes.

*/


if ( ! defined( 'ABSPATH' ) ) {
	die();
}

//require dirname(__FILE__).'/includes/functions.php';


define('TEST_CHILD_THEME', false);
define('HIDE_ULTIMATE_CHECKBOX', false);

if (!is_multisite()) {

//class AGSLayouts {
	class DiviGhoster {
		const
            PLUGIN_VERSION = '5.0.26',
            PLUGIN_SLUG = 'divi-ghoster',
PLUGIN_AUTHOR_URL = 'https://divi.space/',
            PAGE_SLUG = 'divi_ghoster';

		public static $pluginFile, $pluginBaseUrl, $pluginDirectory, $settings, $supportedThemes, $targetTheme, $actualTargetTheme, $targetThemeSlug;
		
		private static $incompatiblePlugins = array();
		private static $notices = array();
		private static $classes = array();

		public static function run() {
			self::$pluginFile = __FILE__;
			self::$pluginBaseUrl = plugin_dir_url(__FILE__);
			self::$pluginDirectory = __DIR__.'/';
			self::$notices = array(
				'theme_not_activate' => __('Theme could not be activated due to plugin(s) incompatibility!', 'divi-ghoster'),
				'maybe_plugin_incompatibility' => __('The following plugin(s) may use Divi Or Divi Child Theme functions:')
			);
			self::$incompatiblePlugins = array(//'divi-switch' => 'divi-switch/functions.php',
			);
			// Classes without method constructor or args in setup method
			self::$classes = array(
				'admin_ajax' => 'DiviGhosterAdminAjax',
				'new_theme_url' => 'DiviGhosterNewThemeUrl',
				'new_plugin_url' => 'DiviGhosterNewPluginUrl',
				//'custom_post_type' => 'DiviGhosterDashboardPostType',
				'export_settings' => 'DiviGhosterExportSettings',
				'import_settings' => 'DiviGhosterImportSettings',
				'login_url_rewrite' => 'LoginUrlRewrite',
			);
			
			add_action('activated_plugin', array('DiviGhoster', 'redirectToSettingsPage') );
			// TODO Divi Switch plugin is not compatible with some Divi Child themes (Divi Business Pro) fatal error of `undefined et_get_option function`
			// TODO switchBackToDiviTheme function was preventing to activate theme(s) in case if fatal error appears.
			// TODO Fix this issue on Divi Switch
			//add_action('switch_theme', array('DiviGhoster', 'switchBackToDiviTheme'), 99, 3);
			//add_action('admin_notices', array('DiviGhoster', 'admin_notices_error'));
			add_filter('et_pb_load_roles_admin_hook', array('DiviGhoster', 'filterRoleEditorHook'), 9999);
			add_filter('et_divi_role_editor_page', array('DiviGhoster', 'filterRoleEditorPage'), 9999);

			add_action('after_setup_theme', array('DiviGhoster', 'onThemeSetUp'), 9999);
            add_action('wp_head', array('DiviGhoster', 'maybeHideBuilderSupportIcons'), 9999);
            add_filter('et_fb_app_preferences', array('DiviGhoster', 'setPageFlowDefault'));

            add_action('in_admin_header', array('DiviGhoster', 'inAdminHeader'));
            add_action('admin_notices', array('DiviGhoster', 'checkPermalinkStructure'));
			
			add_action('switch_theme', [__CLASS__, 'onThemeSwitched'], 10, 3);
			
			add_filter('divi_ghoster_ghosted_theme', [__CLASS__, 'getGhostedThemeName'], 10, 3);
		
			include_once(ABSPATH . '/wp-admin/includes/plugin.php');

			self::$supportedThemes = array('Divi', 'Extra');
			self::$targetTheme = wp_get_theme()->get_template();
			
			if (in_array(self::$targetTheme, self::$supportedThemes)) {
				self::$actualTargetTheme = self::$targetTheme;
			} else {
				self::$targetTheme = get_option('agsdg_ghosted_theme');
				self::$actualTargetTheme = self::$targetTheme;
				
				if (!in_array(self::$targetTheme, self::$supportedThemes)) {
					// Fall back to Divi if the theme is unknown or not supported
					self::$targetTheme = 'Divi';
				}
			}
			
			self::$targetThemeSlug = strtolower(self::$targetTheme);

			self::$settings = get_option('agsdg_settings');
			if (self::$settings == false) {
				self::initializeSettings();
			}
			if (!isset(self::$settings['branding_name_esc'])) { // backwards compat
				self::$settings['branding_name_esc'] = esc_html( self::$settings['branding_name'] );
				self::saveSettings();
			}
			
			include_once(__DIR__.'/includes/customizer-base.php');
			if (!empty(self::$settings['enable_login_page'])) {
				include_once(__DIR__.'/includes/custom-login.php');
			}
			include_once(__DIR__.'/includes/custom-builder.php');
			include (__DIR__ . '/includes/rewrite.php');
			if (is_admin()) {
				//include_once(__DIR__.'/includes/dashboard-post-type.php');
				include_once(__DIR__.'/includes/divi-ghoster-import-export/DiviGhosterSettings.php');
				include_once(__DIR__.'/includes/divi-ghoster-import-export/DiviGhosterExport.php');
				include_once(__DIR__.'/includes/divi-ghoster-import-export/DiviGhosterImport.php');
				include_once(__DIR__.'/includes/divi-ghoster-import-export/DiviGhosterExportSettings.php');
				include_once(__DIR__.'/includes/divi-ghoster-import-export/DiviGhosterImportSettings.php');
				include (__DIR__ . '/includes/admin-ajax.php');

			}
			include_once(__DIR__.'/includes/login-url-rewrite.php');
			include_once(__DIR__.'/includes/admin.php');

			include_once(__DIR__.'/includes/admin-filters.php');
		
			if (self::$settings['ultimate_ghoster'] === 'yes') {
				include_once(__DIR__.'/includes/ultimate.php');
				DiviGhosterUltimate::run();
			}
			
			if ( self::$targetTheme == 'Divi' && is_admin() ) {
			
				// Divi\epanel\core_functions.php
				add_filter( 'et_epanel_is_divi', '__return_true' );
		
			}

			foreach (self::$classes as $class) {
				if ( class_exists($class) ) {
					/*if ( ( new ReflectionClass( $class ) )->getConstructor() ) {
						//TODO Add args handle for class::__constructor()
						continue;
					}*/
					$call_class = new $class();
					if ( method_exists( $call_class, 'setup' ) ) {
						//TODO setup method should run args if present
						$call_class->setup();
					}
				}
			}
			
		}
		
		public static function checkPermalinkStructure(){
			if ( get_option( 'permalink_structure', '' ) == '' ) {
				echo '<div class="error notice"><p>' . sprintf(__('Ultimate Ghoster cannot work correctly while your permalink structure is set to Plain. Please change your permalink structure in Settings &gt; <a href="%s">Permalinks.</a>', 'divi-ghoster'), esc_url(admin_url('options-permalink.php'))) . '</p></div>';
			}
		}

		// Remove warning notices in theme page
		public static function inAdminHeader(){
			global $pagenow;

			if ( is_admin() ) {
				if ('themes.php' === $pagenow) {
					echo '<style>.notice-warning { display: none; }</style>';
				}
			}
		}

		// This will hide the 'help' and 'support' icons next to the save button on the front end builder
        //  if the Support role "Divi Documentation & Help" has been turned off in the role editor.
        public static function maybeHideBuilderSupportIcons() {
            if ( ! current_user_can( 'et_support_center_documentation' ) ) {
                echo '<style type="text/css">
                .et-fb-button-group.et-fb-button-group--save-changes .et-fb-button--quick-actions,
                .et-fb-button-group.et-fb-button-group--save-changes .et-fb-button--help {
                display: none !important;
                }
                </style>';
            }
        }
		
		public static function admin_notices_error(){
			$html = '';
			if ( 'yes' === get_option('divi_incompatible_theme') ) {
				$html .= '<div class="error notice is-dismissible"><p>' . self::$notices['theme_not_activate'] . '</p></div>';
			}
			$i = 1;
			if ( ! empty( get_option('divi_incompatible_plugins') ) ) {
				$html .= '<div id="setting-error-tgmpa" class="notice notice-warning settings-error is-dismissible"> ';
				$html .= '<p><strong>';
				$html .= '<span style="display: block; margin: 0.5em 0.5em 0 0; clear: both;">' . self::$notices['maybe_plugin_incompatibility'] . '</span>';
				foreach (get_option('divi_incompatible_plugins') as $incompatiblePlugin){
					$html .= '<span style="display: block; margin: 0.5em 0.5em 0 0; clear: both;"><i>' . $i . '</i> - ' . $incompatiblePlugin['name'] . '</span>';
					$i++;
				}
				$html .= '<span style="display: block; margin: 0.5em 0.5em 0 0; clear: both;">' . __('Please deactivate the incompatible plugin(s) to avoid any errors', 'divi-ghoster') . '</span>';
				$html .= '</strong></p>';
				$html .= '</div>';
			}

			echo esc_attr( $html );
		}

		public static function initializeSettings() {
			$oldOptions = get_option('dwl_settings');
			self::$settings = array(
				'branding_name' => (empty($oldOptions['dwl_text_field_0']) ? self::$targetTheme : $oldOptions['dwl_text_field_0']),
				'branding_image_url' => get_option('dash_icon_path', ''),
				'theme_slug' => get_option('curr_page', 'ghost_divi'),
				'theme_name' => get_option('theme_name', ''),
				'author_name' => get_option('author_name', ''),
				'custom_theme_image' => get_option('custom_theme_image', ''),
				'theme_image_url' => get_option('theme_image_url', ''),
				'login_url' => get_option('login_url', ''),
				'new_theme_path' => get_option('new_theme_path', ''),
				'new_plugin_path' => get_option('new_plugin_path', ''),
				'enable_login_page' => get_option('enable_login_page', 'no'),
				'ultimate_ghoster' => get_option('hidden_stat', 'no'),
				'hide_divi_related_plugins' => get_option('hidden_stat', 'no'),
				'hide_divi_ghoster' => get_option('hidden_stat', 'no'),
				'hide_divi_source' => get_option('hidden_stat', 'no'),
				'hide_product_tour' => get_option('hidden_stat', 'no'),
				'hide_divi_page'=> get_option('hide_divi_page', 'no'),
				'hide_divi_options_page' => get_option('hide_divi_options_page', 'no'),
				'hide_theme_update_info' => get_option('hide_divi_options_page', 'no'),
				'hide_premade_layouts' => get_option('hide_divi_options_page', 'no'),
				'hide_library_page'=> get_option('hide_library_page', 'no'),
				'hide_theme_page' => get_option('hide_theme_page', 'no'),
				'hide_customizer_page' => get_option('hide_customizer_page', 'no'),
				'hide_appearance_page' => get_option('hide_appearance_page', 'no'),
				'hide_edit_page' => get_option('hide_edit_page', 'no'),
				'hide_settings_page' => get_option('hide_settings_page', 'no'),
				'hide_tools_page' => get_option('hide_tools_page', 'no'),
				'hide_projects_page' => get_option('hide_projects_page', 'no'),
				'hide_plugins_page' => get_option('hide_plugins_page', 'no'),
				'hide_ct_page' => get_option('hide_ct_page', 'no'),
				'hide_editor_page' => get_option('hide_editor_page', 'no'),
				'hide_support_page' => get_option('hide_support_page', 'no'),
				'hide_updates_page' => get_option('hide_updates_page', 'no'),
				'hide_wp_page' => get_option('hide_wp_page', 'no'),
				'hide_wp_icon' => get_option('hide_wp_icon', 'no'),
				'hide_wp_footer' => get_option('hide_wp_footer', 'no'),
				'hide_wp_widgets' => get_option('hide_wp_widgets', 'no'),
				'wp_shortcut' => get_option('wp_shortcut', 'no'),
				'enable_custom_colors' => get_option('enable_custom_colors', 'no'),
				'widget_layout' => get_option('widget_layout', '0'),

			);
			self::saveSettings();
		}

		public static function saveSettings() {
			update_option('agsdg_settings', self::$settings);
		}

		public static function filterRoleEditorHook($hook) {
			return get_plugin_page_hookname('et_' . self::$settings['theme_slug'] . '_role_editor', 'et_' . self::$settings['theme_slug'] . '_options');
		}

		public static function filterRoleEditorPage($page) {
			return 'et_' . self::$settings['theme_slug'] . '_role_editor';
		}
		
		public static function onPluginActivate() {
			self::onPluginIsActiveChange(true);
		}
		
		public static function onPluginDeactivate() {
			self::onPluginIsActiveChange(false);
		}

		public static function onPluginIsActiveChange($isActive) {
			self::addLogEntry(
				sprintf(
					__('Divi Ghoster plugin status changed to: %s (version: %s)', 'divi-ghoster'),
					$childTheme ? __('Active', 'divi-ghoster') : __('Inactive', 'divi-ghoster'),
					self::PLUGIN_VERSION
				)
			);
			
			// Update settings
			self::$settings['ultimate_ghoster'] = 'no';
			self::saveSettings();
			delete_option('adsdg_ultimate_theme');
			delete_option('nginx-nginx-notice');

			try {
				include_once(__DIR__.'/includes/ultimate.php');
				DiviGhosterUltimate::disable(self::$settings['theme_slug']);
			} catch ( Exception $ex ) {
				self::addLogEntry(
					sprintf(
						__('Exception while disabling Ultimate Ghoster: %s', 'divi-ghoster'),
						$ex->getMessage()
					)
				);
			}
			
			if (!empty(self::$settings['theme_image_url'])) {
				try {
					if ($isActive) {
						DiviGhosterAdminAjax::setCustomThemeImage(DiviGhosterAdminAjax::THEME_TYPE_PARENT);
					} else {
						DiviGhosterAdminAjax::restoreThemeImage(false, DiviGhosterAdminAjax::THEME_TYPE_PARENT);
					}
				} catch ( Exception $ex ) {
					self::addLogEntry(
						sprintf(
							__('Exception while setting or restoring theme image for parent theme: %s', 'divi-ghoster'),
							$ex->getMessage()
						)
					);
				}
			}
			
			if (!empty(self::$settings['child_theme_image_url'])) {
				try {
					if ($isActive) {
						DiviGhosterAdminAjax::setCustomThemeImage(DiviGhosterAdminAjax::THEME_TYPE_CHILD);
					} else if (self::$settings['child_theme_image_url'] !== 'same') {
						DiviGhosterAdminAjax::restoreThemeImage(false, DiviGhosterAdminAjax::THEME_TYPE_CHILD);
					}
				} catch ( Exception $ex ) {
					self::addLogEntry(
						sprintf(
							__('Exception while setting or restoring theme image for child theme: %s', 'divi-ghoster'),
							$ex->getMessage()
						)
					);
				}
			}
			
			flush_rewrite_rules();
		}

		public static function onThemeSetUp() {
			global $themename;
			$themename = DiviGhoster::$settings['branding_name_esc'];
		}

		public static function setPageFlowDefault($allPrefs) {
			$allPrefs['page_creation_flow']['value'] = 'build_from_scratch';
			
			return $allPrefs;
		}

		public static function redirectToSettingsPage($plugin) {
			if ($plugin === plugin_basename(DiviGhoster::$pluginFile)) {
				wp_safe_redirect( esc_url( admin_url( 'admin.php?page=divi_ghoster' ) ) );
				exit();
			}
		}
		
		public static function addLogEntry($message) {
			$log = get_option('divi_ghoster_log');
			if ($log) {
				$log = json_decode($log);
			}
			if (!is_array($log)) {
				$log = array();
			}
			while (count($log) > 99) {
				array_pop($log);
			}
			array_unshift($log, [
				current_time('mysql'),
				$message
			]);
			update_option('divi_ghoster_log', wp_json_encode($log), false);
		}
		
		public static function clearLog() {
			delete_option('divi_ghoster_log');
		}
		
		public static function onThemeSwitched($themeName, $newTheme, $oldTheme) {
			self::addLogEntry(
				sprintf(
					__('Active theme changed: old theme: %s (parent: %s); new theme: %s (parent: %s)', 'divi-ghoster'),
					$oldTheme->name,
					empty($oldTheme->parent_theme) ? __('None', 'divi-ghoster') : $oldTheme->parent_theme,
					$newTheme->name,
					empty($newTheme->parent_theme) ? __('None', 'divi-ghoster') : $newTheme->parent_theme
				)
			);
		}
		
		public static function getGhostedThemeName() {
			if (self::$settings['ultimate_ghoster'] === 'yes') {
				return self::$actualTargetTheme;
			}
		}
		
	}
    
	require dirname(__FILE__).'/updater/updater.php';
    
	DiviGhoster::run();
	
// Disable Ultimate Ghoster on plugin activation/deactivation
	register_activation_hook(__FILE__, array('DiviGhoster', 'onPluginActivate'));
	register_deactivation_hook(__FILE__, array('DiviGhoster', 'onPluginDeactivate'));
}
