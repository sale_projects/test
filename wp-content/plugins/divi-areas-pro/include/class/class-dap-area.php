<?php
/**
 * Defines the DAP_Area class, which represents a single Divi Area.
 *
 * @package Divi_Areas_Pro
 */

/**
 * Represents a single Divi Area.
 *
 * @since 2.0.0
 */
class DAP_Area {
	/**
	 * Cache of all DAP_Area instances that were instantiated.
	 * Used by DAP_Area::get_area()
	 *
	 * @since 2.0.0
	 * @var array
	 */
	private static $_areas = [];

	/**
	 * A flag that is true while generating an Areas HTML code. This flag is used in
	 * the class `DAP_Front` via `DAP_Area::is_doing_area()` to avoid injection of
	 * Areas into other Areas.
	 *
	 * @since 1.0.0
	 * @var bool
	 */
	private static $_doing_area = false;

	/**
	 * The Area ID, which is always identical to the areas post ID.
	 *
	 * @since 2.0.0
	 * @var int
	 */
	public $ID = 0;

	/**
	 * Holds the post-status of the current Area.
	 *
	 * @since 2.0.0
	 * @var string
	 */
	public $post_status = 'draft';

	/**
	 * Holds the post-name (slug) of the Area.
	 *
	 * @since 2.0.0
	 * @var string
	 */
	public $post_name = '';

	/**
	 * Holds the unparsed post-content; i.e., all the shortcodes that were generated
	 * by Divis Visual Builder.
	 *
	 * @since 2.0.0
	 * @var string
	 */
	public $post_content = '';

	/**
	 * The prefixed Area ID, i.e. "prefix-post_id"
	 *
	 * @since 2.0.0
	 * @var string
	 */
	public $area_id = '';

	/**
	 * The prefixed Area slug, i.e. "prefix-post_name".
	 *
	 * @since 2.0.0
	 * @var string
	 */
	public $area_slug = '';

	/**
	 * A modified version of the Area ID that is used by the JS API hooks,
	 * i.e. "prefix_post_name".
	 *
	 * @since 2.0.0
	 * @var string
	 */
	public $area_hook = '';

	/**
	 * Area configuration after parsing the database params.
	 *
	 * @since 2.0.0
	 * @var array
	 */
	public $config = [];

	/**
	 * A counter that is used by the output_code() logic to avoid CSS class
	 * collisions between Areas and other content.
	 *
	 * @since 2.0.0
	 * @var int
	 */
	private $module_index = - 1;

	/**
	 * List of JavaScript variables that are used by the get_js_code() method.
	 *
	 * @since 2.0.0
	 * @var array
	 */
	private $js_vars = [];

	/**
	 * Counts the JS variables with trigger rules, to give each rule a different name.
	 *
	 * @since 2.0.0
	 * @var int
	 */
	private $trigger_rule_groups = 0;

	/**
	 * The default area configuration, used for new Areas or missing/new options.
	 *
	 * @since 2.0.0
	 * @var array
	 */
	private static $default_config = [
		// Area configuration version.
		'version'           => '',

		// Custom strings.
		'layout_type'       => 'popup',
		'inline_location'   => 'css',
		'location_position' => 'before',
		'location_selector' => '',
		'close_trigger'     => 'hover',
		'position'          => 'center_center',
		'overflow'          => 'clip',
		'size'              => 'auto',

		// Numbers or numeric strings.
		'close_delay'       => 'instant',
		'close_for'         => '',
		'min_width'         => 'auto',
		'width'             => 'auto',
		'max_width'         => 'auto',
		'min_height'        => 'auto',
		'height'            => 'auto',
		'max_height'        => 'auto',
		'z_index'           => 'auto',

		// On/Off toggles.
		'role_limitation'   => false,
		'page_limitation'   => false,
		'show_close'        => true,
		'not_modal'         => true,
		'not_blocking'      => true,
		'singleton'         => false,
		'keep_closed'       => false,
		'box_shadow'        => false,
		'with_loader'       => false,
		'dark_close'        => false,
		'alt_close'         => false,
		'is_static'         => false,
		'push_content'      => false,

		// Complex values, lists.
		'limit_role'        => [],
		'postlist'          => [],
		'triggers'          => [],
		'triggers_hover'    => [],
		'ajax_conditions'   => [],
	];


	/* ------------------------ Static methods: Accessors ----------------------- */


	/**
	 * Returns a DAP_Area instance for the given post_id.
	 * When the specified post_id is no valid Divi Area, the method returns false.
	 *
	 * @since 2.0.0
	 *
	 * @param int|WP_Post $post_id The source post to load.
	 *
	 * @return DAP_Area|false The DAP_Area instance or false.
	 */
	public static function get_area( $post_id ) {
		$result = false;
		$post   = get_post( $post_id, OBJECT );

		// Also treat saved layouts as valid Areas.
		if ( defined( 'ET_BUILDER_LAYOUT_POST_TYPE' ) ) {
			$layout_posttype = ET_BUILDER_LAYOUT_POST_TYPE;
		} else {
			$layout_posttype = 'et_pb_layout';
		}

		/*
		 * Validate the post: It has to be (1) a valid WP_Post object that is either
		 * (2) a Divi Area post, or (3) a saved template library post which (4) has
		 * divi-area metadata.
		 */
		if (
			$post // (1)
			&& is_a( $post, 'WP_Post' ) // (1)
			&& (
				DIVI_AREAS_CPT === $post->post_type // (2)
				||
				(
					$layout_posttype === $post->post_type // (3)
					&& (
						get_post_meta( $post->ID, '_da_area', true ) // (4)
						|| get_post_meta( $post->ID, '_divi_area', true ) // (4)
					)
				)
			)
		) {
			if ( ! isset( self::$_areas[ $post->ID ] ) ) {
				self::$_areas[ $post->ID ] = new DAP_Area( $post->ID );
			}

			$result = self::$_areas[ $post->ID ];
		}

		return $result;
	}

	/**
	 * Tests, whether the given area is visible for the current user.
	 * This method caches the visibility result for up to 60 minutes before
	 * calculating it again.
	 *
	 * @since  2.0.0
	 *
	 * @param int $post_id The area ID.
	 *
	 * @return bool Whether the area is visible.
	 */
	public static function is_area_visible_for_current_user( $post_id ) {
		$group  = get_current_user_id();
		$cached = self::cache_get( 'user', $group );

		if ( ! $cached ) {
			$cached = [];
		}

		if ( ! isset( $cached[ $post_id ] ) ) {
			$area               = DAP_Area::get_area( $post_id );
			$cached[ $post_id ] = $area->is_visible_for_current_user();

			self::cache_set( 'user', $cached, $group );
		}

		/**
		 * Allow modifying the area visibility bypassing the Divi Area cache.
		 *
		 * @since 2.3.0
		 * @var   bool $visible The auto-determined visibility.
		 * @var   int  $area_id The area ID.
		 */
		return apply_filters(
			'divi_areas_is_area_visible_for_current_user',
			$cached[ $post_id ],
			$post_id
		);
	}

	/**
	 * Tests if the given Area is visible on the current page (in terms of "request"
	 * not "post-type").
	 * This method caches the visibility result for up to 60 minutes before
	 * calculating it again.
	 *
	 * @since 2.0.0
	 *
	 * @param int $post_id The area ID.
	 *
	 * @return bool Whether the area is visible.
	 */
	public static function is_area_visible_on_current_page( $post_id ) {
		$request = DAP_Front::get_request_info();
		$group   = $request['key'];
		$cached  = self::cache_get( 'page', $group );

		if ( ! $cached ) {
			$cached = [];
		}

		if ( ! isset( $cached[ $post_id ] ) ) {
			$area               = DAP_Area::get_area( $post_id );
			$cached[ $post_id ] = $area->is_visible_on_current_page();

			self::cache_set( 'page', $cached, $group );
		}

		/**
		 * Allow modifying the area visibility bypassing the Divi Area cache.
		 *
		 * @since 2.3.0
		 * @var   bool $visible The auto-determined visibility.
		 * @var   int  $area_id The area ID.
		 */
		return apply_filters(
			'divi_areas_is_area_visible_on_current_page',
			$cached[ $post_id ],
			$post_id
		);
	}

	/**
	 * Returns true while an Areas' HTML code is generated.
	 *
	 * @since 2.0.0
	 * @return bool
	 */
	public static function is_doing_area() {
		return self::$_doing_area;
	}




	/* ------------------------- Static methods: Caching ------------------------ */


	/**
	 * Invalidates all DAP_Area caches. This function will not _delete_ the cached
	 * values; it only expires the values so they are re-calculated on next usage.
	 *
	 * @since 0.2.0
	 */
	public static function invalidate_cache() {
		update_option( DIVI_AREAS_CACHE_GROUP . '_version', time(), true );
	}

	/**
	 * Returns a cached value.
	 *
	 * @since 2.0.0
	 *
	 * @param string $key   Name of the cache key.
	 * @param string $group Optional. The cache group. Defaults to 'default'.
	 *
	 * @return mixed Cached value.
	 */
	public static function cache_get( $key, $group = 'default' ) {
		$result = null;

		if ( divi_areas_pro()::flag_use_cache() ) {
			$cache_version = (int) get_option( DIVI_AREAS_CACHE_GROUP . '_version' );
			$cache_key     = sprintf(
				'%s_%s_%s',
				DIVI_AREAS_CACHE_GROUP,
				$key,
				$group
			);

			$cached = get_option( $cache_key );

			if (
				$cached
				&& isset( $cached['v'] )
				&& isset( $cached['d'] )
				&& $cached['v'] === $cache_version
			) {
				$result = $cached['d'];
			}
		}

		return $result;
	}

	/**
	 * Returns a cached value.
	 *
	 * @since 2.0.0
	 *
	 * @param string $key   Name of the cache key.
	 * @param mixed  $data  The value to cache.
	 * @param string $group Optional. The cache group. Defaults to 'default'.
	 *
	 * @return void
	 */
	public static function cache_set( $key, $data, $group = 'default' ) {
		if ( divi_areas_pro()::flag_use_cache() ) {
			$cache_version = (int) get_option( DIVI_AREAS_CACHE_GROUP . '_version' );
			$cache_key     = sprintf(
				'%s_%s_%s',
				DIVI_AREAS_CACHE_GROUP,
				$key,
				$group
			);

			$cached = [
				'v' => $cache_version,
				'd' => $data,
			];

			update_option( $cache_key, $cached, false );
		}
	}




	/* ---------------------------- Instance methods ---------------------------- */


	/**
	 * Prepares a new DAP_Area instance.
	 *
	 * @since 2.0.0
	 *
	 * @param int $post_id Optional. The post ID to use to initialize the object.
	 *                     When no post_id is specified, the DAP_Area object is not
	 *                     initialized.
	 */
	public function __construct( $post_id = 0 ) {
		$this->reset_config();

		if ( $post_id ) {
			$post = get_post( $post_id );

			$this->ID           = $post_id;
			$this->post_status  = $post->post_status;
			$this->post_name    = $post->post_name;
			$this->post_content = $post->post_content;

			$this->area_id   = DIVI_AREAS_ID_PREFIX . $this->ID;
			$this->area_slug = DIVI_AREAS_ID_PREFIX . $this->post_name;
			$this->area_hook = str_replace( '-', '_', $this->area_id );

			$this->parse_config();
		}
	}

	/**
	 * Returns the default options for a new Area.
	 *
	 * @since 2.3.0
	 * @return array The default Area configuration.
	 */
	public function get_default_config() {
		return apply_filters(
			'divi_areas_default_config',
			self::$default_config
		);
	}

	/**
	 * Resets all config options to their default value, ensures that all options are
	 * present.
	 *
	 * @since 2.0.0
	 * @return void
	 */
	public function reset_config() {
		if ( ! is_array( $this->config ) ) {
			$this->config = [];
		}

		foreach ( $this->get_default_config() as $key => $value ) {
			$this->config[ $key ] = $value;
		}

		// Dynamic values.
		$this->config['version']     = DIVI_AREAS_VERSION;
		$this->config['triggers_by'] = [];
	}

	/**
	 * Sanitizes the specified config so it is ready to be saved.
	 *
	 * @since 2.3.0
	 *
	 * @param array $config Area configuration to sanitize.
	 *
	 * @return array The sanitized configuration.
	 */
	public static function sanitize_config( $config ) {
		$result = [];

		// Set the Area version.
		$result['version'] = isset( $config['version'] ) ? $config['version'] : '';

		// Custom strings.
		$result['layout_type']       = $config['layout_type'];
		$result['inline_location']   = $config['inline_location'];
		$result['location_position'] = $config['location_position'];
		$result['location_selector'] = $config['location_selector'];
		$result['close_trigger']     = $config['close_trigger'];
		$result['position']          = $config['position'];
		$result['overflow']          = $config['overflow'];
		$result['size']              = $config['size'];

		// Numbers or numeric strings.
		$result['close_delay'] = $config['close_delay'];
		$result['close_for']   = $config['close_for'];
		$result['min_width']   = divi_areas_serialize_responsive( $config['min_width'] );
		$result['width']       = divi_areas_serialize_responsive( $config['width'] );
		$result['max_width']   = divi_areas_serialize_responsive( $config['max_width'] );
		$result['min_height']  = divi_areas_serialize_responsive( $config['min_height'] );
		$result['height']      = divi_areas_serialize_responsive( $config['height'] );
		$result['max_height']  = divi_areas_serialize_responsive( $config['max_height'] );
		$result['z_index']     = $config['z_index'];

		// On/Off toggles.
		$result['role_limitation'] = divi_areas_serialize_bool( $config['role_limitation'] );
		$result['page_limitation'] = divi_areas_serialize_bool( $config['page_limitation'] );
		$result['show_close']      = divi_areas_serialize_bool( $config['show_close'] );
		$result['not_modal']       = divi_areas_serialize_bool( $config['not_modal'] );
		$result['not_blocking']    = divi_areas_serialize_bool( $config['not_blocking'] );
		$result['singleton']       = divi_areas_serialize_bool( $config['singleton'] );
		$result['keep_closed']     = divi_areas_serialize_bool( $config['keep_closed'] );
		$result['box_shadow']      = divi_areas_serialize_bool( $config['box_shadow'] );
		$result['with_loader']     = divi_areas_serialize_bool( $config['with_loader'] );
		$result['dark_close']      = divi_areas_serialize_bool( $config['dark_close'] );
		$result['alt_close']       = divi_areas_serialize_bool( $config['alt_close'] );
		$result['is_static']       = divi_areas_serialize_bool( $config['is_static'] );
		$result['push_content']    = divi_areas_serialize_bool( $config['push_content'] );

		// Complex values, lists.
		$result['limit_role']     = $config['limit_role'];
		$result['postlist']       = $config['postlist'];
		$result['triggers']       = [];
		$result['triggers_hover'] = [];

		self::serialize_condition( $result, $config );

		foreach ( $config['triggers'] as $trigger ) {
			$item = [
				'trigger_type'        => $trigger['trigger_type'],
				'trigger_scroll_type' => $trigger['trigger_scroll_type'],
				'trigger_selector'    => $trigger['trigger_selector'],
				'trigger_delay'       => $trigger['trigger_delay'],
				'trigger_distance'    => $trigger['trigger_distance'],
				'trigger_label'       => $trigger['trigger_label'],
			];
			self::serialize_condition( $item, $trigger, 'area' );

			$result['triggers'][] = $item;
		}

		foreach ( $config['triggers_hover'] as $trigger ) {
			$item = [
				'trigger_type_hover'     => $trigger['trigger_type'],
				'trigger_selector_hover' => $trigger['trigger_selector'],
				'trigger_label_hover'    => $trigger['trigger_label'],
			];
			self::serialize_condition( $item, $trigger, 'hover' );

			$result['triggers_hover'][] = $item;
		}

		/**
		 * Filter the sanitized configuration.
		 *
		 * @since 2.3.0
		 *
		 * @param array $sanitized The sanitized (serialized) configuration.
		 * @param array $config    The original, deserialized configuration.
		 */
		return apply_filters(
			'divi_areas_sanitize_config',
			$result,
			$config
		);
	}

	/**
	 * Saves the current area configuration to the post-meta table.
	 *
	 * @since 2.0.0
	 * @return void
	 */
	public function save_config() {
		if ( ! is_array( $this->config ) || ! $this->ID ) {
			return;
		}

		$config_arr = self::sanitize_config( $this->config );

		/**
		 * Filters the sanitized Area configuration before they are saved to the DB.
		 *
		 * @since 2.0.0
		 *
		 * @param array    $config The sanitized Area configuration.
		 * @param DAP_Area $this   The current area instance (passed by reference).
		 */
		$config_arr = apply_filters( 'divi_areas_save_config', $config_arr, $this );

		update_post_meta( $this->ID, '_da_area', $config_arr );
		update_post_meta( $this->ID, '_da_version', $config_arr['version'] );
	}

	/**
	 * Searches the post-meta table for area configuration and updates the $config
	 * array of the DAP_Area instance.
	 * Only sets/modifies options that are used by the plugin, custom config values
	 * are not changed and can be initialized via `divi_areas_parse_config`.
	 *
	 * @since 2.0.0
	 *
	 * @param array|null $meta Optional. The Area configuration to parse. When omitted,
	 *                         the function reads the configuration from the post-meta
	 *                         field `_da_area`.
	 *
	 * @return void
	 */
	public function parse_config( $meta = null ) {
		if ( ! $meta || ! is_array( $meta ) ) {
			/**
			 * This method parses entries in the post-meta table to initialize the
			 * instance, so we need an existing post ID to continue.
			 */
			if ( $this->ID ) {
				$meta = get_post_meta( $this->ID, '_da_area', true );
			}
		}

		if ( ! $meta || ! is_array( $meta ) ) {
			// Try to update the Area from a previous version.
			$meta = DAP_Updater::update_area( $this->ID, [] );
		}

		if ( ! $meta || ! is_array( $meta ) ) {
			/*
			 * No meta data provided to the function and the post does not have any
			 * meta information either. We cannot parse anything, so abort here.
			 */
			$this->reset_config();

			return;
		}

		if ( ! is_array( $this->config ) ) {
			$this->config = [];
		}

		// Found valid meta details? Check if they are up-to-date.
		if (
			empty( $meta['version'] )
			|| - 1 === version_compare( $meta['version'], DIVI_AREAS_VERSION )
		) {
			$meta = DAP_Updater::update_area( $this->ID, $meta );
		}

		$meta = array_merge( $this->get_default_config(), $meta );

		$this->config['version'] = $meta['version'];

		// Assign string values.
		$this->config['layout_type']       = $meta['layout_type'];
		$this->config['inline_location']   = $meta['inline_location'];
		$this->config['location_position'] = $meta['location_position'];
		$this->config['location_selector'] = $meta['location_selector'];
		$this->config['close_trigger']     = $meta['close_trigger'];
		$this->config['position']          = $meta['position'];
		$this->config['overflow']          = $meta['overflow'];
		$this->config['size']              = $meta['size'];

		// Numbers or numeric strings.
		$this->config['close_delay'] = $meta['close_delay'];
		$this->config['close_for']   = $meta['close_for'];
		$this->config['min_width']   = divi_areas_deserialize_responsive( $meta['min_width'] );
		$this->config['width']       = divi_areas_deserialize_responsive( $meta['width'] );
		$this->config['max_width']   = divi_areas_deserialize_responsive( $meta['max_width'] );
		$this->config['min_height']  = divi_areas_deserialize_responsive( $meta['min_height'] );
		$this->config['height']      = divi_areas_deserialize_responsive( $meta['height'] );
		$this->config['max_height']  = divi_areas_deserialize_responsive( $meta['max_height'] );
		$this->config['z_index']     = $meta['z_index'];

		// Convert boolean values.
		$this->config['role_limitation'] = divi_areas_deserialize_bool( $meta['role_limitation'] );
		$this->config['page_limitation'] = divi_areas_deserialize_bool( $meta['page_limitation'] );
		$this->config['show_close']      = divi_areas_deserialize_bool( $meta['show_close'] );
		$this->config['not_modal']       = divi_areas_deserialize_bool( $meta['not_modal'] );
		$this->config['not_blocking']    = divi_areas_deserialize_bool( $meta['not_blocking'] );
		$this->config['singleton']       = divi_areas_deserialize_bool( $meta['singleton'] );
		$this->config['keep_closed']     = divi_areas_deserialize_bool( $meta['keep_closed'] );
		$this->config['box_shadow']      = divi_areas_deserialize_bool( $meta['box_shadow'] );
		$this->config['with_loader']     = divi_areas_deserialize_bool( $meta['with_loader'] );
		$this->config['dark_close']      = divi_areas_deserialize_bool( $meta['dark_close'] );
		$this->config['alt_close']       = divi_areas_deserialize_bool( $meta['alt_close'] );
		$this->config['is_static']       = divi_areas_deserialize_bool( $meta['is_static'] );
		$this->config['push_content']    = divi_areas_deserialize_bool( $meta['push_content'] );

		// Load complex values, lists.
		$this->config['limit_role'] = (array) $meta['limit_role'];
		$this->config['postlist']   = (array) $meta['postlist'];

		$this->config['condition']      = $this->parse_condition( $meta );
		$this->config['triggers']       = $this->parse_trigger( $meta['triggers'], 'area' );
		$this->config['triggers_hover'] = $this->parse_trigger( $meta['triggers_hover'], 'hover' );
		$this->refresh_trigger_mapping();

		/* -- Dynamic config that is not saved -- */

		/*
		 * A static Inline  Area is instantly visible on page load and cannot be
		 * closed or triggered.
		 */
		if ( 'inline' === $this->config['layout_type'] && $this->config['is_static'] ) {
			$this->config['show_close']  = false;
			$this->config['triggers']    = [];
			$this->config['triggers_by'] = [];
		}

		/**
		 * Filter the parsed configuration of the Area.
		 *
		 * @since 2.3.0
		 *
		 * @param array $config The parsed (deserialized) configuration.
		 * @param array $meta   The original, serialized configuration.
		 */
		$this->config = apply_filters( 'divi_areas_parse_config', $this->config, $meta );

		/**
		 * Fires after the area configuration was fully parsed.
		 *
		 * @since 2.0.0
		 *
		 * @param DAP_Area $this The current area instance (passed by reference).
		 * @param array    $meta The original, serialized configuration.
		 */
		do_action_ref_array( 'divi_areas_parsed_config', [ &$this, $meta ] );
	}

	/**
	 * Parses the specified trigger list and adds the trigger details to the current
	 * area configuration.
	 *
	 * @since 2.0.0
	 *
	 * @param array  $triggers List of triggers.
	 * @param string $group    Optional. Defines the trigger-type, can be either
	 *                         'auto', 'hover' or 'area'.
	 *
	 * @return array The parsed triggers.
	 */
	public function parse_trigger( $triggers, $group = 'auto' ) {
		$result = [];

		if (
			is_object( $triggers )
			|| ( is_array( $triggers ) && isset( $triggers['trigger_type'] ) )
		) {
			$triggers = [ $triggers ];
		}

		if ( ! $triggers || ! is_array( $triggers ) ) {
			return $result;
		}

		$defaults = [
			'trigger_selector'    => '',
			'trigger_scroll_type' => 'distance',
			'trigger_delay'       => '',
			'trigger_distance'    => '',
			'trigger_label'       => '',
		];

		$hover_mapping = [
			'trigger_type_hover'     => 'trigger_type',
			'trigger_selector_hover' => 'trigger_selector',
			'trigger_label_hover'    => 'trigger_label',
		];

		foreach ( $triggers as $config ) {
			if ( empty( $config ) || ! is_array( $config ) ) {
				continue;
			}

			if ( 'auto' === $group ) {
				$is_hover = ! empty( $config['trigger_type_hover'] );
			} else {
				$is_hover = 'hover' === $group;
			}

			// Translate hover-settings to default settings.
			if ( $is_hover ) {
				foreach ( $hover_mapping as $hover_key => $default_key ) {
					if ( isset( $config[ $hover_key ] ) ) {
						$config[ $default_key ] = $config[ $hover_key ];
						unset( $config[ $hover_key ] );
					}
				}
			}

			if ( empty( $config['trigger_type'] ) ) {
				continue;
			}

			$config = array_merge( $defaults, $config );

			$result[] = [
				'trigger_type'        => strtolower( $config['trigger_type'] ),
				'trigger_scroll_type' => strtolower( $config['trigger_scroll_type'] ),
				'trigger_selector'    => $config['trigger_selector'],
				'trigger_delay'       => $config['trigger_delay'],
				'trigger_distance'    => $config['trigger_distance'],
				'trigger_label'       => $config['trigger_label'],
				'condition'           => $this->parse_condition( $config ),
			];
		}

		return $result;
	}

	/**
	 * Needs to be called after modifying a config['triggers'] or
	 * config['triggers_hover'] value to refresh the internal trigger-mapping.
	 *
	 * @since 2.3.0
	 * @return void
	 */
	public function refresh_trigger_mapping() {
		/**
		 * After parsing the triggers we refresh the "triggers_by" mapping. This
		 * mapping is effectively used to generate JS scripts and therefore it only
		 * contains triggers that are relevant for the areas layout_type.
		 */
		$mapping = [];
		if ( 'hover' === $this->get( 'layout_type' ) ) {
			$source = $this->config['triggers_hover'];
		} else {
			$source = $this->config['triggers'];
		}

		foreach ( $source as $trigger ) {
			if ( ! isset( $mapping[ $trigger['trigger_type'] ] ) ) {
				$mapping[ $trigger['trigger_type'] ] = [];
			}
			$mapping[ $trigger['trigger_type'] ] [] = $trigger;
		}
		$this->config['triggers_by'] = $mapping;
	}

	/**
	 * Parses the Area or trigger config and returns a single array with all conditions.
	 *
	 * @since 2.2.0
	 *
	 * @param array $config The Area-meta or trigger-config array.
	 *
	 * @return array An array with all conditions of the Area or trigger.
	 */
	protected function parse_condition( $config ) {
		$result = [];

		$suffix = '';
		if ( isset( $config['cond_custom_area'] ) ) {
			$suffix = '_area';
		} elseif ( isset( $config['cond_custom_hover'] ) ) {
			$suffix = '_hover';
		}

		$defaults = [
			"cond_custom$suffix"          => 'off',
			"cond_device$suffix"          => [ 'desktop', 'tablet', 'phone' ],
			"cond_use_url_param$suffix"   => 'off', // Either: off, present, absent.
			"cond_url_param$suffix"       => '',
			"cond_use_referrer$suffix"    => 'off', // Either: off, present, absent.
			"cond_referrer$suffix"        => [], // Checkbox list.
			"cond_custom_referrer$suffix" => '',
			"cond_schedule$suffix"        => 'off', // Either: off, on_date, off_date, weekly.
			"cond_schedule_dates$suffix"  => [], // List of dates.
			"cond_schedule_weekly$suffix" => [], // List of weekly rules.
		];

		$defaults_date = [
			"schedule_range$suffix" => '',
			"schedule_start$suffix" => '',
			"schedule_end$suffix"   => '',
			"schedule_label$suffix" => '',
		];

		$defaults_weekly = [
			"weekly_days$suffix"  => [], // Checkbox list (Mo, Tu, We, ...).
			"weekly_start$suffix" => '',
			"weekly_end$suffix"   => '',
			"weekly_label$suffix" => '',
		];

		$config = array_merge( $defaults, $config );

		if ( $suffix ) {
			$result['enabled'] = 'on' === $config[ "cond_custom$suffix" ];
		} else {
			$result['enabled'] = true;
		}

		$result['device']          = $config[ "cond_device$suffix" ];
		$result['use_url_param']   = $config[ "cond_use_url_param$suffix" ];
		$result['use_referrer']    = $config[ "cond_use_referrer$suffix" ];
		$result['url_param']       = explode( "\n", $config[ "cond_url_param$suffix" ] );
		$result['referrer']        = $config[ "cond_referrer$suffix" ];
		$result['custom_referrer'] = explode( "\n", $config[ "cond_custom_referrer$suffix" ] );
		$result['schedule']        = $config[ "cond_schedule$suffix" ];
		$result['schedule_dates']  = [];
		$result['schedule_weekly'] = [];

		$result['url_param']       = array_map( 'trim', $result['url_param'] );
		$result['url_param']       = array_filter( $result['url_param'] );
		$result['custom_referrer'] = array_map( 'trim', $result['custom_referrer'] );
		$result['custom_referrer'] = array_filter( $result['custom_referrer'] );

		foreach ( $config[ "cond_schedule_dates$suffix" ] as $item ) {
			$item = array_merge( $defaults_date, $item );

			// Convert the range strings to two timestamps.
			$range = explode( ' - ', $item[ "schedule_range$suffix" ] );
			if ( 1 === count( $range ) ) {
				$range[] = $range[0];
			}
			$range = array_map( 'trim', $range );

			$start = sprintf(
				'%s %s:00',
				$range[0],
				substr( $item[ "schedule_start$suffix" ] . '00:00', 0, 5 )
			);
			$end   = sprintf(
				'%s %s:59',
				$range[1],
				substr( $item[ "schedule_end$suffix" ] . '00:00', 0, 5 )
			);

			$result['schedule_dates'] [] = [
				'start' => strtotime( $start ),
				'end'   => strtotime( $end ),
				'label' => $item[ "schedule_label$suffix" ],
			];
		}

		foreach ( $config[ "cond_schedule_weekly$suffix" ] as $item ) {
			$item = array_merge( $defaults_weekly, $item );
			$days = str_replace(
				[ 'Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa' ],
				[ '0', '1', '2', '3', '4', '5', '6' ],
				implode( ',', $item[ "weekly_days$suffix" ] )
			);

			$result['schedule_weekly'] [] = [
				'days'  => array_map( 'intval', explode( ',', $days ) ),
				'start' => substr( $item[ "weekly_start$suffix" ] . '00:00', 0, 5 ),
				'end'   => substr( $item[ "weekly_end$suffix" ] . '00:00', 0, 5 ),
				'label' => $item[ "weekly_label$suffix" ],
			];
		}

		/**
		 * Store ajax details in duplicate config-array with a unique hash. The JS
		 * snippet only includes the ajax_key, which is used to query the server and
		 * ask if that specific condition is currently met.
		 */
		if ( $result['enabled'] ) {
			$for_ajax           = [
				'schedule'        => $result['schedule'],
				'schedule_dates'  => $result['schedule_dates'],
				'schedule_weekly' => $result['schedule_weekly'],
			];
			$ajax_key           = $suffix ? md5( wp_json_encode( $for_ajax ) ) : 'default';
			$result['ajax_key'] = $ajax_key;

			$this->config['ajax_conditions'][ $ajax_key ] = $for_ajax;
		}

		return $result;
	}

	/**
	 * Serializes the conditions specified in the config array and merges them into
	 * the item array.
	 *
	 * @since 2.2.0
	 *
	 * @param array  $item   The serialized item array (Area or trigger).
	 * @param array  $config The parsed (deserialized) item array.
	 * @param string $group  Trigger group ("area" or "hover").
	 *
	 * @return void
	 */
	protected static function serialize_condition( &$item, $config, $group = '' ) {
		$suffix = '';
		if ( $group ) {
			$suffix = '_' . strtolower( $group );
		}

		$defaults = [
			'enabled'         => false,
			'devices'         => [ 'desktop', 'tablet', 'phone' ],
			'use_url_param'   => 'off',
			'use_referrer'    => 'off',
			'url_param'       => [],
			'referrer'        => [],
			'custom_referrer' => [],
			'schedule'        => 'off',
			'schedule_dates'  => [],
			'schedule_weekly' => [],
		];

		$defaults_date = [
			'start' => '',
			'end'   => '',
			'label' => '',
		];

		$defaults_weekly = [
			'days'  => [], // Checkbox list (Mo, Tu, We, ...).
			'start' => '',
			'end'   => '',
			'label' => '',
		];

		if ( is_array( $config['condition'] ) ) {
			$condition = array_merge( $defaults, $config['condition'] );
		} else {
			$condition = $defaults;
		}

		if ( $suffix ) {
			$item[ "cond_custom$suffix" ] = $condition['enabled'] ? 'on' : 'off';
		}

		$item[ "cond_device$suffix" ]          = $condition['device'];
		$item[ "cond_use_url_param$suffix" ]   = $condition['use_url_param'];
		$item[ "cond_use_referrer$suffix" ]    = $condition['use_referrer'];
		$item[ "cond_url_param$suffix" ]       = implode( "\n", $condition['url_param'] );
		$item[ "cond_referrer$suffix" ]        = $condition['referrer'];
		$item[ "cond_custom_referrer$suffix" ] = implode( "\n", $condition['custom_referrer'] );
		$item[ "cond_schedule$suffix" ]        = $condition['schedule'];

		$item[ "cond_schedule_dates$suffix" ]  = [];
		$item[ "cond_schedule_weekly$suffix" ] = [];

		foreach ( $condition['schedule_dates'] as $rule ) {
			$rule = array_merge( $defaults_date, $rule );

			$item[ "cond_schedule_dates$suffix" ][] = [
				"schedule_range$suffix" => sprintf(
					'%s - %s',
					gmdate( 'Y-m-d', $rule['start'] ),
					gmdate( 'Y-m-d', $rule['end'] )
				),
				"schedule_start$suffix" => gmdate( 'H:i', $rule['start'] ),
				"schedule_end$suffix"   => gmdate( 'H:i', $rule['end'] ),
				"schedule_label$suffix" => $rule['label'],
			];
		}

		foreach ( $condition['schedule_weekly'] as $rule ) {
			$rule = array_merge( $defaults_weekly, $rule );
			$days = str_replace(
				[ '0', '1', '2', '3', '4', '5', '6' ],
				[ 'Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa' ],
				implode( ',', $rule['days'] )
			);

			$item[ "cond_schedule_dates$suffix" ][] = [
				"weekly_days$suffix"  => explode( ',', $days ),
				"weekly_start$suffix" => $rule['start'],
				"weekly_end$suffix"   => $rule['end'],
				"weekly_label$suffix" => $rule['label'],
			];
		}
	}

	/**
	 * Retrieve area configuration variable.
	 *
	 * @since 2.0.0
	 *
	 * @param string $key      Configuration variable key.
	 * @param mixed  $default  Optional. Value to return if the area config variable
	 *                         is not set. Default empty string.
	 *
	 * @return mixed Contents of the area configuration variable.
	 */
	public function get( $key, $default = '' ) {
		$result = $default;

		if ( isset( $this->config[ $key ] ) ) {
			$result = $this->config[ $key ];
		}

		/**
		 * Filter the Area flag.
		 *
		 * @since 2.1.0
		 *
		 * @param mixed    $result The flag value.
		 * @param string   $key    The flag name.
		 * @param DAP_Area $area   The Divi Area instance.
		 */
		return apply_filters( 'divi_areas_get_area_flag', $result, $key, $this );
	}

	/**
	 * Set area configuration variable.
	 *
	 * @since 2.0.0
	 *
	 * @param string $key   Configuration variable key.
	 * @param mixed  $value The new configuration value.
	 */
	public function set( $key, $value ) {
		if ( 'trigger' === $key ) {
			// Add a single trigger.
			foreach ( $this->parse_trigger( $value, 'auto' ) as $trigger ) {
				$this->config['triggers'][] = $trigger;
			}
			$this->refresh_trigger_mapping();
		} elseif ( 'triggers' === $key || 'triggers_hover' === $key ) {
			// Replace all triggers.
			$group = 'triggers' === $key ? 'area' : 'hover';

			$this->config[ $key ] = $this->parse_trigger( $value, $group );
			$this->refresh_trigger_mapping();
		} else {
			$this->config[ $key ] = $value;
		}
	}

	/**
	 * Generates a list of flags (based on the Area configuration) that will be passed
	 * to DiviArea.register().
	 * This function will only return the flags that are relevant for the layout_type,
	 * to keep the resulting code small and not confuse the JS API.
	 *
	 * @see   register-end.php
	 * @since 2.0.0
	 * @return array List of flags.
	 */
	public function get_js_flags() {
		$result = [];

		$triggers_by = $this->get( 'triggers_by' );
		if ( is_array( $triggers_by ) && isset( $triggers_by['exit'] ) ) {
			$result['trigger-exit'] = 1;
		}

		if ( $this->get( 'show_close' ) ) {
			// ---- Area WITH Close Button.
			if ( $this->get( 'dark_close' ) ) {
				$result['darkMode'] = 1;
			}
			if ( $this->get( 'alt_close' ) ) {
				$result['closeAlt'] = 1;
			}
		} else {
			// ---- Area with NO Close Button.
			$result['hideClose'] = 1;
		}

		// ---- Layout-type: ALL.
		if ( ! $this->get( 'box_shadow' ) ) {
			$result['shadow'] = 0;
		}
		if ( $this->get( 'with_loader' ) ) {
			$result['withloader'] = 1;
		}
		if ( is_numeric( $this->get( 'z_index' ) ) ) {
			$result['fixZIndex'] = (int) $this->get( 'z_index' );
		}

		if ( 'popup' === $this->get( 'layout_type' ) ) {
			// ---- Layout-type: Only POPUP.
			if ( ! $this->get( 'not_modal' ) ) {
				$result['isModal'] = 1;
			}
			if ( ! $this->get( 'not_blocking' ) ) {
				$result['isBlocking'] = 1;
			}
		}

		if ( 'inline' === $this->get( 'layout_type' ) ) {
			// ---- Layout-type: Only INLINE.
			if ( $this->get( 'is_static' ) ) {
				$result['static'] = 1;
			} elseif ( 'css' === $this->get( 'inline_location' ) ) {
				$result['areaHook']  = $this->get( 'location_selector' );
				$result['areaPlace'] = $this->get( 'location_position' );
			}
			$result['hookId'] = $this->ID;
		} else {
			// ---- Layout-type: Not INLINE.
			$result['position'] = $this->get( 'position' );

			if ( $this->get( 'singleton' ) ) {
				$result['singleton'] = 1;
			}

			if ( 'clip' !== $this->get( 'overflow' ) ) {
				$result['overflow'] = $this->get( 'overflow' );
			}

			if ( 'auto' !== $this->get( 'size' ) ) {
				$result['size'] = $this->get( 'size' );
			}
		}

		if ( 'hover' === $this->get( 'layout_type' ) ) {
			// ---- Layout-type: Only HOVER.
			$delay = divi_areas_parse_number( $this->get( 'close_delay' ) );

			if ( $this->get( 'close_trigger' ) ) {
				$result['closeTrigger'] = $this->get( 'close_trigger' );
			}
			if ( $delay['value_float'] > 0 ) {
				$result['closeDelay'] = number_format( $delay['value'], 1, '.', '' );
			}
		}

		if ( 'flyin' === $this->get( 'layout_type' ) ) {
			// ---- Layout-type: Only FLY-IN.
			if (
				$this->get( 'push_content' )
				&& 'auto' !== $this->get( 'size' )
			) {
				$result['pushContent'] = 1;
			}
		}

		if ( $this->area_slug !== $this->area_id ) {
			$result['ids'] = $this->area_slug;
		}

		/**
		 * Filter the default JS flags of the Area.
		 *
		 * @since 0.2.0
		 *
		 * @param array    $js_flags Flags that will be passed to DiviArea.register().
		 * @param DAP_Area $area     The current Area object.
		 */
		return apply_filters(
			'divi_areas_get_js_flags',
			$result,
			$this
		);
	}

	/**
	 * Generates a CSS declaration for the given attribute, based on the area
	 * configuration. The return value is always a string, but can be empty, when no
	 * rules are defined for the requested attribute or device.
	 * Attributes with dashes should be written without dash in all lower case. The
	 * attribute must not be prefixed.
	 * For example, use "maxwidth", and not "max-width" or "maxWidth"!
	 *
	 * @since 2.0.0
	 *
	 * @param string $attribute The attribute name, e.g. "width" or "maxwidth".
	 * @param string $device    A device identifier (desktop, tablet, phone).
	 *
	 * @return string
	 */
	protected function get_style_declaration( $attribute, $device ) {
		$result = '';

		$devices   = [ 'desktop', 'tablet', 'phone' ];
		$attribute = strtolower( str_replace( [ '_', '-' ], '', $attribute ) );
		$device_id = array_search( $device, $devices, true );

		// TODO - add responsive.

		if ( false !== $device_id ) {
			$rules    = [];
			$defaults = [];
			$styles   = [];

			// Find the config value for the attribute and prepare the rules.
			switch ( $attribute ) {
				case 'wrapper.width':
					$rules['min-width']    = $this->get( 'min_width' );
					$rules['width']        = $this->get( 'width' );
					$rules['max-width']    = $this->get( 'max_width' );
					$defaults['min-width'] = [ 'none', 'auto' ];
					$defaults['width']     = [ 'auto' ];
					$defaults['max-width'] = [ 'auto', 'none' ];
					break;
				case 'wrapper.height':
					$rules['min-height']    = $this->get( 'min_height' );
					$rules['height']        = $this->get( 'height' );
					$rules['max-height']    = $this->get( 'max_height' );
					$defaults['min-height'] = [ 'none', 'auto' ];
					$defaults['height']     = [ 'auto' ];
					$defaults['max-height'] = [ 'none', 'auto' ];
					break;
				case 'area.height':
				case 'section.height':
					$rules['min-height'] = [ '0', '0', '0' ];
					$rules['height']     = [ '100%', '100%', '100%' ];
					$rules['max-height'] = [ 'none', 'none', 'none' ];
					break;
				case 'section.width':
					$rules['min-width'] = [ '0', '0', '0' ];
					$rules['width']     = [ '100%', '100%', '100%' ];
					$rules['max-width'] = [ 'none', 'none', 'none' ];
					break;
			}

			// Parse the config value and find the responsive rules for the style.
			foreach ( $rules as $name => $rule ) {
				if ( is_string( $rule ) ) {
					$rule = divi_areas_deserialize_responsive( $rule );

					if ( ! is_array( $rule ) && 'desktop' !== $device ) {
						continue;
					}
				}

				if ( is_array( $rule ) ) {
					if ( isset( $rule[ $device_id ] ) ) {
						$rule = $rule[ $device_id ];
					} else {
						continue;
					}
				}

				// Only add the rule when it's not empty and not equal to the default value.
				if (
					$rule
					&& is_string( $rule )
					&& ( ! isset( $defaults[ $name ] ) || ! in_array( $rule, $defaults[ $name ], true ) )
				) {
					$styles[] = "{$name}:{$rule};";
				}
			}

			$result = implode( '', $styles );
		}

		/**
		 * Filter the default JS flags of the Area.
		 *
		 * @since 0.2.0
		 *
		 * @param string   $styles    The css declaration string.
		 * @param string   $attribute Name of the requested attribute.
		 * @param string   $device    Device type (desktop, tablet, phone).
		 * @param DAP_Area $area      The current Area object.
		 */
		return apply_filters(
			'divi_areas_get_style_declaration',
			$result,
			$attribute,
			$device,
			$this
		);
	}

	/**
	 * Adds custom CSS rules to the area output.
	 *
	 * @since 2.0.0
	 * @return void
	 */
	public function add_area_styles() {
		$wrapper_class   = ".area-outer-wrap[data-da-area='{$this->area_id}']";
		$area_class      = "{$wrapper_class}>[data-da-area]";
		$section_classes = "{$wrapper_class} .et_pb_section";

		$applied_to = $this->add_style( $wrapper_class, 'wrapper.width' );
		if ( count( $applied_to ) > 0 ) {
			$this->add_style( $section_classes, 'section.width', $applied_to );
		}

		$applied_to = $this->add_style( $wrapper_class, 'wrapper.height' );
		if ( count( $applied_to ) > 0 ) {
			$this->add_style( $area_class, 'area.height', $applied_to );
			$this->add_style( $section_classes, 'section.height', $applied_to );
		}

		/**
		 * Fired, after the custom CSS rules were registered with Divi
		 *
		 * @since 2.0.0
		 *
		 * @param string   $wrapper_class CSS selector for the Divi Area wrapper.
		 * @param DAP_Area $area          The current Area object.
		 */
		do_action( 'divi_areas_custom_styles_added', $wrapper_class, $this );
	}

	/**
	 * Tests, whether the area is visible for the current user.
	 *
	 * @since 2.0.0
	 * @return bool True, when the current user is allowed to see the Area.
	 */
	public function is_visible_for_current_user() {
		$visible = false;
		$log     = [ $this->ID, 'For User?' ];

		// Check the Area status.
		if (
			'publish' === $this->post_status
			|| ( 'private' === $this->post_status && is_user_logged_in() )
		) {
			$visible = true;
		} else {
			$log[] = 'Not published';
		}

		// When status is okay, check the user role. At least ONE role must match.
		if ( $visible && $this->get( 'role_limitation' ) ) {
			$log[] = 'User ' . get_current_user_id();
			$log[] = 'Require ' . implode( ', ', $this->get( 'limit_role' ) );

			if ( ! is_user_logged_in() ) {
				if ( ! in_array( ':guest', $this->get( 'limit_role' ), true ) ) {
					$log[]   = 'Hide for guest';
					$visible = false;
				}
			} else {
				if ( ! in_array( ':loggedin', $this->get( 'limit_role' ), true ) ) {
					$user    = wp_get_current_user();
					$roles   = (array) $user->roles;
					$visible = false;
					$log[]   = 'User roles ' . implode( ', ', $roles );

					foreach ( $roles as $role ) {
						if ( in_array( $role, $this->get( 'limit_role' ), true ) ) {
							$visible = true;
							break;
						}
					}
				}
			}
		} elseif ( $visible ) {
			$log[] = 'No user-role limitation';
		}

		$log[] = '→ ' . ( $visible ? 'SHOW' : 'HIDE' );
		DAP_Component::logger()->debug( ...$log );

		/**
		 * Allow modifying the Area visibility. The result of this filter is stored in
		 * the Divi Area cache.
		 *
		 * @since 2.0.0
		 * @var   bool     $visible The auto-determined visibility.
		 * @var   DAP_Area $area    The area instance.
		 */
		return apply_filters( 'divi_areas_is_visible_for_current_user', $visible, $this );
	}

	/**
	 * Undocumented function
	 *
	 * @since 1.0.0
	 * @return bool
	 */
	public function is_visible_on_current_page() {
		$visible = true;

		if ( divi_area_vb_version( '4.0.0' ) ) {
			$log = [ $this->ID, 'On Page?' ];

			if ( $this->get( 'page_limitation' ) ) {
				$visible          = false;
				$highest_priority = - 1;

				foreach ( $this->get( 'postlist' ) as $setting => $rule ) {
					$log_rules = [ "{$rule} {$setting}" ];

					$template_config = DAP_Front::get_template_setting_ancestor( $setting );

					if ( empty( $template_config ) ) {
						// When the setting is not valid (anymore) skip this rule.
						$log_rules[] = 'Miss (no settings found)';
					} elseif ( $highest_priority > $template_config['priority'] ) {
						// Continue, when a higher-prioritized rule already defined visibility.
						$log_rules[] = 'Miss (priority too low)';
					} elseif (
						$highest_priority === $template_config['priority']
						&& 'use_on' === $rule
					) {
						// 'Use-On' rules must have a higher priority to have any effect.
						$log_rules[] = 'Miss (prefer earlier match)';
					} elseif ( DAP_Front::fulfills_template_setting( $setting, $template_config ) ) {
						// When the rule matches the current request, the visibility is changed.
						$highest_priority = $template_config['priority'];

						$visible = 'use_on' === $rule;

						$log_rules[] = sprintf(
							'Match: %s/%d',
							$visible ? 'show' : 'hide',
							$template_config['priority']
						);
					}

					$log[] = implode( ' - ', $log_rules );
				}
			} else {
				$log[] = 'No page limitation';
			}
		}

		$log[] = '→ ' . ( $visible ? 'SHOW' : 'HIDE' );
		DAP_Component::logger()->debug( ...$log );

		/**
		 * Allow modifying the Area visibility. The result of this filter is stored in
		 * the Divi Area cache.
		 *
		 * @since 2.0.0
		 * @var   bool     $visible The auto-determined visibility.
		 * @var   DAP_Area $area    The area instance.
		 */
		return apply_filters( 'divi_areas_is_visible_on_current_page', $visible, $this );
	}




	/* -------------------------- Rendering and output -------------------------- */


	/**
	 * Outputs the HTML code and JavaScript logic of the Area.
	 *
	 * @since 2.0.0
	 * @return void
	 */
	public function full_code() {
		/*
		 * This output is intentionally unescaped, because it contains generated
		 * JavaScript code which would be stripped by any kses or sanitation logic.
		 *
		 * The contents are generated by this plugin and Divi.
		 */
		echo et_core_intentionally_unescaped( $this->get_full_code(), 'html' );
	}

	/**
	 * Returns a string with the full HTML code of the Area and a script tag with the
	 * JS API logic.
	 *
	 * @since 2.0.0
	 * @return string The full Area code.
	 */
	public function get_full_code() {
		/** @noinspection PhpFormatFunctionParametersMismatchInspection */
		return sprintf(
			"\n\n<!-- Area #%3\$s -->\n%1\$s\n%2\$s\n<!-- /Area #%3\$s -->\n",
			$this->get_html_code(),
			$this->get_js_code(),
			esc_attr( $this->area_id )
		);
	}

	/**
	 * Returns the fully parsed and wrapped Area HTML code.
	 * This function will effectively render the Divi layout and turn it into plain
	 * HTML. During this process, Divi will also enqueue the custom CSS styles for
	 * this Area.
	 *
	 * @see   ET_Builder_Element::_render()
	 * @since 2.0.0
	 * @return string
	 */
	public function get_html_code() {
		global $wp_embed;

		self::$_doing_area = true;
		$this->start_module_index_override();

		$area_content      = $this->post_content;
		$wp_embed->post_ID = $this->ID;

		// [embed] shortcode.
		$wp_embed->run_shortcode( $area_content );

		// Detect plain embed links on their own line.
		$wp_embed->autoembed( $area_content );

		// Do the Divi magic: Generate the HTML and prepare the CSS.
		$content = do_shortcode( $area_content );

		// Add custom styles to the Area output.
		$this->add_area_styles();

		$this->end_module_index_override();
		self::$_doing_area = false;

		return $this->wrap_area_output( $content );
	}

	/**
	 * Returns a single script tag with inline JavaScript that registers the Area and
	 * sets up all triggers.
	 *
	 * @since 2.0.0
	 * @return string
	 */
	public function get_js_code() {
		$result = '';
		$code   = [];

		/**
		 * Fires before the JS snippet for the current Area is generated.
		 *
		 * @since 2.1.0
		 *
		 * @param DAP_Area $area The processed Divi Area object.
		 */
		do_action( 'divi_areas_prepare_area_js_code', $this );

		// Static Areas do not have any JS logic.
		if ( ! $this->get( 'is_static' ) ) {
			$triggers_by = $this->get( 'triggers_by' );

			/**
			 * Filters the Area triggers before creating the final JS snippet. This
			 * filter can inject or modify dynamic triggers to the Area.
			 *
			 * @since 2.1.0
			 *
			 * @param array    $triggers Default triggers (defined via UI).
			 * @param DAP_Area $area     The processed Divi Area object.
			 */
			$triggers_by = apply_filters(
				'divi_areas_prepare_area_triggers',
				$triggers_by,
				$this
			);

			$minify = ! divi_areas_pro()->flag_unminified_js( $this );
			$vars   = [ 'area' => $this ];

			$code[] = divi_areas_parse_template( 'js-part/register-start', $vars );

			// Process all triggers.
			foreach ( $triggers_by as $type => $triggers ) {
				$vars['triggers'] = $triggers;

				$code[] = divi_areas_parse_template( 'js-part/trigger-' . $type, $vars );
			}
			unset( $vars['triggers'] );

			// Add the required openers/closers.
			$code[] = divi_areas_parse_template( 'js-part/action-open', $vars );
			$code[] = divi_areas_parse_template( 'js-part/action-close', $vars );

			$code[] = divi_areas_parse_template( 'js-part/register-end', $vars );

			// Either minify the script or prepend the debug header.
			if ( ! $minify ) {
				$code = array_map(
					function ( $segment ) {
						if ( ! trim( $segment ) ) {
							return '';
						}

						return "\n" . trim( $segment, "\r\n" ) . "\n";
					},
					$code
				);

				$code = array_filter( $code );
			}

			/**
			 * Filters the Area JS snippet parts before building the full snippet.
			 *
			 * @since 2.1.0
			 *
			 * @param array    $code The JS code parts.
			 * @param DAP_Area $area The processed Divi Area object.
			 */
			$code = apply_filters(
				'divi_areas_prepare_area_js_parts',
				$code,
				$this
			);

			// Insert the variables and their values into the script.
			$js_vars = $this->get_js_vars();
			$jq_vars = '';

			if ( $js_vars['jquery'] ) {
				$jq_vars = "var\n\t" . implode( ",\n\t", $js_vars['jquery'] ) . ";\n";
			}

			$full_code = sprintf(
				"(function(%2\$s){\n%4\$s%1\$s\n})(\n\t%3\$s\n)",
				trim( implode( '', $code ), ';' ),
				implode( ', ', $js_vars['names'] ),
				implode( ",\n\t", $js_vars['values'] ),
				$jq_vars
			);

			if ( $minify ) {
				$symbols = array_keys( $this->js_vars );
				$result  = sprintf(
					'<script>%s</script>',
					divi_areas_minify_js( $full_code, $symbols )
				);
			} elseif ( defined( 'DIVIMODE_DEBUG' ) && DIVIMODE_DEBUG ) {
				$symbols       = array_keys( $this->js_vars );
				$minified_code = divi_areas_minify_js( $full_code, $symbols );
				$ratio         = 100 / strlen( $full_code ) * strlen( $minified_code );

				$result = sprintf(
					"<script>%s\n/*\n%s\n\n(Reduced by %s%%)\n*/\n</script>",
					$full_code,
					$minified_code,
					number_format( 100 - $ratio, 2, '.', '' )
				);
			} else {
				$result = sprintf( '<script>%s</script>', $full_code );
			}
		}

		return $result;
	}

	/**
	 * Registers a JavaScript variable that will be declared at the beginning of the
	 * generated js_code and is recognized by the minification routine.
	 * When a variable with the given name was already registered earlier, that
	 * variable will be overwritten with the new value.
	 *
	 * @since 2.0.0
	 *
	 * @param string $name      The variable name.
	 * @param string $value     Optional. The variable value. If left empty, the
	 *                          variable will be minified but not declared.
	 * @param string $data_type Optional. Defines the data type for sanitation.
	 *                          Supported types: string, number, bool, json, or
	 *                          auto (default).
	 *
	 * @return void
	 */
	public function add_js_var( $name, $value = null, $data_type = 'auto' ) {
		if ( null !== $value ) {
			$value = $this->serialize_js_var( $value, $data_type );
		}

		$this->js_vars[ $name ] = $value;
	}

	/**
	 * Checks, if a specified JS variable has been registered.
	 *
	 * @since 2.0.0
	 *
	 * @param string $name The variable name to check.
	 *
	 * @return bool
	 */
	public function has_js_var( $name ) {
		return array_key_exists( $name, $this->js_vars );
	}

	/**
	 * Searches for a variable value and returns the variable name, when found.
	 *
	 * @since 2.0.0
	 *
	 * @param string $value     See add_js_var().
	 * @param string $data_type See add_js_var().
	 *
	 * @return string The variable name, or an empty string.
	 */
	public function find_js_var( $value, $data_type = 'auto' ) {
		$result   = '';
		$expected = $this->serialize_js_var( $value, $data_type );

		foreach ( $this->js_vars as $var_name => $var_value ) {
			if ( $expected === $var_value ) {
				$result = $var_name;
			}
		}

		return $result;
	}

	/**
	 * Converts the specified value into a string that can be output in the dynamic js script.
	 *
	 * @since 2.0.0
	 *
	 * @param string $value     See add_js_var().
	 * @param string $data_type See add_js_var().
	 *
	 * @return string The serialized value.
	 */
	private function serialize_js_var( $value, $data_type ) {
		$result = $value;

		if ( 'auto' === $data_type ) {
			if ( is_bool( $value ) ) {
				$data_type = 'bool';
			} elseif ( is_numeric( $value ) ) {
				$data_type = 'number';
			} elseif ( is_array( $value ) || is_object( $value ) ) {
				$data_type = 'json';
			}
		}

		if ( 'string' === $data_type ) {
			$result = '"' . trim( $value, '"\'' ) . '"';
		} elseif ( 'number' === $data_type ) {
			if ( is_float( $value ) ) {
				$result = number_format( $value, 2, '.', '' );
			} else {
				$result = (string) (int) $value;
			}
		} elseif ( 'bool' === $data_type ) {
			$result = $value ? 'true' : 'false';
		} elseif ( 'json' === $data_type ) {
			if ( is_array( $value ) ) {
				$value = array_filter( $value, [ $this, 'is_not_null' ] );
			}

			$result = preg_replace(
				'/"([a-zA-Z0-9_]+)"\s*:/',
				'$1:',
				str_replace(
					"\n",
					"\n\t",
					wp_json_encode(
						$value,
						JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES
					)
				)
			);
		}

		return $result;
	}

	/**
	 * Returns a JS variable declaration for all registered JS vars.
	 *
	 * @since 2.0.0
	 * @return array {
	 *     The variable names and their initial value.
	 * @type array names .. The list of variable names.
	 * @type array values .. List of corresponding values.
	 * @type array jquery .. List of variables that need jQuery for declaration.
	 * }
	 */
	private function get_js_vars() {
		$names  = [];
		$values = [];
		$jquery = [];

		foreach ( $this->js_vars as $name => $value ) {
			if ( null === $value ) {
				continue;
			}

			if (
				false !== strpos( $value, '$(' )
				|| false !== strpos( $value, 'jQuery(' )
			) {
				$jquery[] = esc_attr( $name ) . ' = ' . $value;

				if ( ! in_array( 'jQuery', $values, true ) ) {
					$names[]  = '$';
					$values[] = 'jQuery';
				}
			} else {
				$names[]  = esc_attr( $name );
				$values[] = $value;
			}
		}

		return [
			'names'  => $names,
			'values' => $values,
			'jquery' => $jquery,
		];
	}

	/**
	 * Parses the given trigger definition and returns JS code.
	 *
	 * @since 2.0.0
	 *
	 * @param array  $trigger The trigger details.
	 * @param string $prepend Optional. JS code to prepend to the variable name. For
	 *                        example ',' or '&&'.
	 * @param string $append  Optional. JS code to append after the variable name.
	 *
	 * @return string|null The name of the js variable or null (no variable prepared).
	 */
	public function prepare_js_trigger_conditions( $trigger, $prepend = null, $append = null ) {
		$result     = null;
		$config     = ( $trigger && $trigger['condition'] ) ? $trigger['condition'] : false;
		$conditions = [];

		// When trigger does not have custom conditions, fall back to the Area defaults.
		if (
			! is_array( $config )
			|| empty( $config['enabled'] )
		) {
			$config = $this->config['condition'];
		}

		// ----- Parse the conditions into an intermediate array.

		// dev .. device list.
		if (
			isset( $config['device'] )
			&& is_array( $config['device'] )
			&& 3 !== count( $config['device'] )
		) {
			$devices = [
				in_array( 'desktop', $config['device'], true ) ? 1 : 0,
				in_array( 'tablet', $config['device'], true ) ? 1 : 0,
				in_array( 'phone', $config['device'], true ) ? 1 : 0,
			];

			$conditions['dev'] = $devices;
		}

		// pa1 / pa0 .. with param / without param.
		if ( 'present' === $config['use_url_param'] ) {
			$conditions['pa1'] = $config['url_param'];
		} elseif ( 'absent' === $config['use_url_param'] ) {
			$conditions['pa0'] = $config['url_param'];
		}

		// re1 / re0 .. with referrer / without referrer.
		if ( 'present' === $config['use_referrer'] ) {
			$conditions['re1'] = $config['referrer'];
		} elseif ( 'absent' === $config['use_referrer'] ) {
			$conditions['re0'] = $config['referrer'];
		}

		// rc1 / rc0 .. with custom referrer / without custom referrer.
		if ( 'present' === $config['use_referrer'] ) {
			$conditions['rc1'] = $config['custom_referrer'];
		} elseif ( 'absent' === $config['use_referrer'] ) {
			$conditions['rc0'] = $config['custom_referrer'];
		}

		// cav .. check area visibility via ajax.
		if ( 'on_date' === $config['schedule'] || 'not_date' === $config['schedule'] ) {
			$conditions['cav'] = 'default' === $config['ajax_key'] ? 1 : $config['ajax_key'];
		}

		// cav .. check area visibility via ajax.
		if ( 'weekly' === $config['schedule'] ) {
			$conditions['cav'] = 'default' === $config['ajax_key'] ? 1 : $config['ajax_key'];
		}

		/**
		 * Filter the Area conditions before sending them to the browser.
		 *
		 * @since 2.2.0
		 *
		 * @param array    $conditions The Area conditions.
		 * @param DAP_Area $area       The processed Area instance.
		 */
		$conditions = apply_filters( 'divi_areas_area_js_conditions', $conditions, $this );

		// ----- Build the JS code for all conditions.
		if ( $conditions ) {
			$rule_name = $this->find_js_var( $conditions, 'json' );

			if ( ! $rule_name ) {
				$this->trigger_rule_groups ++;
				$rule_name = 'trigger_rules_' . $this->trigger_rule_groups;
				$this->add_js_var( $rule_name, $conditions, 'json' );
			}

			$result = sprintf(
				'%s%s%s',
				is_null( $prepend ) ? '' : $prepend,
				$rule_name,
				is_null( $append ) ? '' : $append
			);
		}

		return $result;
	}

	/**
	 * Returns a sanitized array of all CSS selectors of the given trigger.
	 *
	 * @since 1.2.0
	 *
	 * @param array $trigger The trigger definition.
	 *
	 * @return array An array of unique strings.
	 */
	public function parse_trigger_selectors( $trigger ) {
		$result = [];

		if ( ! empty( $trigger['trigger_selector'] ) ) {
			$selectors = $trigger['trigger_selector'];
			$selectors = str_replace( "\n", ',', $selectors );

			$result = explode( ',', $selectors );
			$result = array_map( 'trim', $result );
			$result = array_filter( $result );
			$result = array_unique( $result );
		}

		return $result;
	}

	/**
	 * Adds the wrapper div to the this HTML output.
	 *
	 * @since 2.0.0
	 *
	 * @param string $content The rendered post_content.
	 *
	 * @return string $content with a wrapper div and attributes (class, id, ...).
	 */
	private function wrap_area_output( $content ) {
		$attrs = [
			'id'    => $this->area_id,
			'class' => [
				'divi-area-wrap',
				'divi-area-wrap-' . $this->ID,
				'divi-area-wrap-' . $this->post_name,
				'divi-area-type-' . $this->get( 'layout_type' ),
				$this->area_slug,
			],
			'style' => [],
		];

		/**
		 * Static Areas and Areas in Preview-Mode are instantly visible. All other
		 * Areas (i.e., non Inline Areas or dynamic Inline Areas) are initially hidden
		 * and displayed via a the JS trigger or the API later.
		 */
		if (
			$this->get( 'is_static' )
			|| ( is_singular( DIVI_AREAS_CPT ) && get_the_ID() === $this->ID )
		) {
			$attrs['style'][] = 'display:block;';
		} else {
			$attrs['style'][] = 'display:none;';
		}

		/**
		 * Filter the area attributes.
		 *
		 * @since 0.1.0
		 * @var   array   $attrs List of area attributes.
		 * @var   WP_Post $area  The area post-object.
		 */
		$attrs = apply_filters(
			'divi_areas_render_attrs',
			$attrs,
			$this
		);

		// Escape all attributes and values.
		$attributes = [];
		foreach ( $attrs as $name => $value ) {
			if ( is_array( $value ) ) {
				$value = implode( ' ', $value );
			}

			$attributes[] = esc_html( $name ) . '="' . esc_attr( $value ) . '"';
		}

		// Return the wrapped content.
		return sprintf(
			'<div %2$s>%1$s</div>',
			$content,
			implode( ' ', $attributes )
		);
	}

	/**
	 * Adds a hook that is invoked before a Divi element is rendered. Our hook handler
	 * will manually override the module-index, so Divi gives all elements a truly
	 * unique CSS ID.
	 *
	 * @since 2.0.0
	 */
	private function start_module_index_override() {
		ET_Builder_Element::begin_theme_builder_layout( $this->ID );

		add_filter(
			'et_pb_module_shortcode_attributes',
			[ $this, 'do_module_index_override' ]
		);
	}

	/**
	 * Removes our custom hook function again so Divi uses its internal logic to
	 * define the module-index for the next element.
	 *
	 * @since 2.0.0
	 * @return void
	 */
	private function end_module_index_override() {
		ET_Builder_Element::end_theme_builder_layout();

		global $et_pb_predefined_module_index;

		unset( $et_pb_predefined_module_index );
	}

	/**
	 * This dummy-filter does not actually filter anything, but only updates a global
	 * flag that is recognized by Divi. This allows us to generate site-wide unique
	 * CSS selectors, that will never conflict or overlap with anything else on the
	 * page.
	 *
	 * @see    ET_Builder_Element::get_module_order_class()
	 * @filter et_pb_module_shortcode_attributes
	 * @global       $et_pb_predefined_module_index
	 * @since  1.0.0
	 *
	 * @param string $not_used Not used but required for return value.
	 *
	 * @return string The unmodified input parameter.
	 */
	public function do_module_index_override( $not_used = '' ) {
		global $et_pb_predefined_module_index;

		$this->module_index ++;
		$et_pb_predefined_module_index = sprintf(
			'dap_%1$s_%2$s',
			$this->ID,
			$this->module_index
		);

		return $not_used;
	}

	/**
	 * Adds custom CSS rules for desktop, tablet and phone viewports in a
	 * desktop-first approach.
	 * Based on logic found in ET_Builder_Element::process_advanced_fonts_options().
	 * {@see class-et-builder-element.php} for more references and samples.
	 * Note: ET_Builder_Module is an alias for ET_Builder_Element. Both classes are
	 * defined in the class file mentioned above.
	 *
	 * @since 2.0.0
	 *
	 * @param string|array $selectors The CSS selector (or selectors).
	 * @param string       $attribute Name of the CSS attribute to set.
	 * @param array|null   $devices   Optional. List of devices to style. Defaults to
	 *                                all three devices: desktop, tablet, phone.
	 *
	 * @return array List of devices that were styled.
	 */
	private function add_style( $selectors, $attribute, $devices = null ) {
		$result          = [];
		$selectors       = (array) $selectors;
		$function_name   = 'dap_area';
		$prev_device_css = '';
		$media_queries   = [
			'desktop' => false,
			'tablet'  => 'max_width_980',
			'phone'   => 'max_width_767',
		];

		if ( is_null( $devices ) ) {
			$devices = [ 'desktop', 'tablet', 'phone' ];
		}

		foreach ( $devices as $device ) {
			// See if there is a CSS declaration of the attribute and device.
			$css = $this->get_style_declaration( $attribute, $device );

			// Skip the rule when it's empty or identical with the previous device.
			if ( ! $css || $css === $prev_device_css ) {
				continue;
			}
			$prev_device_css = $css;

			$style = [
				'selector'    => implode( ', ', array_unique( $selectors ) ),
				'declaration' => esc_html( $css ),
			];

			// Add a media-query for non-desktop rules.
			if ( $media_queries[ $device ] ) {
				$style['media_query'] = ET_Builder_Module::get_media_query(
					$media_queries[ $device ]
				);
			}

			$result[] = $device;
			ET_Builder_Module::set_style( $function_name, $style );
		}

		return $result;
	}

	/**
	 * Tests if a given value equals to null.
	 *
	 * @since 2.2.0
	 *
	 * @param mixed $value The value to test.
	 *
	 * @return bool True, if the given value is not null.
	 */
	public function is_not_null( $value ) {
		return ! is_null( $value );
	}
}
