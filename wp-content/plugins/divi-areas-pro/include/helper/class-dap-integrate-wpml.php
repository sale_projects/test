<?php
/**
 * Extends Divi Areas to add WPML-specific features.
 *
 * @package Divi_Areas_Pro
 */

defined( 'ABSPATH' ) || die();

/**
 * The integration module.
 *
 * @since 2.3.0
 */
class DAP_Integrate_Wpml {
	/**
	 * Singleton getter for the integration module.
	 *
	 * @snce 2.3.0
	 * @return DAP_Integrate_Wpml The singleton instance.
	 */
	public static function inst() {
		static $inst = null;

		if ( ! $inst ) {
			$inst = new DAP_Integrate_Wpml();
		}

		return $inst;
	}

	/**
	 * DAP_Integrate_Wpml constructor.
	 *
	 * @since 2.3.0
	 */
	private function __construct() {
		$this->setup();
	}

	/**
	 * Hooks up the module with Divi Areas Pro.
	 *
	 * @since 2.3.0
	 */
	public function setup() {
		add_filter(
			'divi_areas_metabox_area_settings_after_page_rules',
			[ $this, 'extend_area_settings' ]
		);

		add_filter(
			'divi_areas_active_area_state',
			[ $this, 'filter_area_state' ],
			10,
			2
		);

		add_filter(
			'divi_areas_default_config',
			[ $this, 'filter_default_config' ]
		);

		add_filter(
			'divi_areas_sanitize_config',
			[ $this, 'filter_sanitize_config' ],
			10,
			2
		);

		add_filter(
			'divi_areas_parse_config',
			[ $this, 'filter_parse_config' ],
			10,
			2
		);
	}

	/**
	 * Injects WPML language selector into the Area display settings tab.
	 *
	 * @since 2.3.0
	 *
	 * @param array $form The settings form.
	 *
	 * @return array The modified settings form.
	 */
	public function extend_area_settings( $form ) {
		global $sitepress;

		$languages = [];
		foreach ( $sitepress->get_active_languages() as $lang ) {
			$languages[ $lang['code'] ] = $lang['display_name'];
		}

		$form['wpml'] = [
			'label' => __( 'Language', 'divi_areas' ),
			'type'  => 'section',
			'items' => [
				'use_wpml'  => [
					'label'       => __( 'Language Limitation', 'divi_areas' ),
					'description' => __( 'To display this Area only for specific translations of your page, enable this flag.', 'divi_areas' ),
					'type'        => 'yes_no_button',
					'default'     => 'off',
					'options'     => [
						'on'  => __( 'Yes', 'divi_areas' ),
						'off' => __( 'No', 'divi_areas' ),
					],
					'hints'       => [
						'on'  => __( 'Specific languages', 'divi_areas' ),
						'off' => __( 'Any language', 'divi_areas' ),
					],
				],
				'wpml_lang' => [
					'label'       => __( 'For Language', 'divi_areas' ),
					'description' => __( 'Choose, which translations should display this Area.', 'divi_areas' ),
					'type'        => 'checkbox',
					'options'     => $languages,
					'condition'   => [ 'use_wpml', '=', 'on' ],
				],
			],
		];

		return $form;
	}

	/**
	 * Filters the default options for a new Area.
	 *
	 * @since 2.3.0
	 *
	 * @param array $defaults Initial default options.
	 *
	 * @return array The default Area options.
	 */
	public function filter_default_config( $defaults ) {
		$defaults['use_wpml']  = false;
		$defaults['wpml_lang'] = [];

		return $defaults;
	}

	/**
	 * Serialize the custom configuration.
	 *
	 * @since 2.3.0
	 *
	 * @param array $sanitized The sanitized (serialized) configuration.
	 * @param array $config    The original, deserialized configuration.
	 *
	 * @return array The final sanitized configuration.
	 */
	public function filter_sanitize_config( $sanitized, $config ) {
		$sanitized['use_wpml']  = divi_areas_serialize_bool( $config['use_wpml'] );
		$sanitized['wpml_lang'] = $config['wpml_lang'];

		return $sanitized;
	}

	/**
	 * Deserialize the custom configuration.
	 *
	 * @since 2.3.0
	 *
	 * @param array $parsed The parsed (deserialized) configuration.
	 * @param array $config    The original, serialized configuration.
	 *
	 * @return array The final sanitized configuration.
	 */
	public function filter_parse_config( $parsed, $config ) {
		$parsed['use_wpml']  = divi_areas_deserialize_bool( $config['use_wpml'] );
		$parsed['wpml_lang'] = (array) $config['wpml_lang'];

		return $parsed;
	}

	/**
	 * Filters the initial state of the Area. This filter sets the Area to
	 * "hidden" when the current request does not match the Areas language flag.
	 *
	 * @since 2.3.0
	 *
	 * @param string $state   Either 'show' or 'hide'.
	 * @param int    $area_id The Area that is filtered.
	 *
	 * @return string The new Area state ('show' or anything else).
	 */
	public function filter_area_state( $state, $area_id ) {
		$area = DAP_Area::get_area( $area_id );

		if ( $area->get( 'use_wpml' ) && defined( 'ICL_LANGUAGE_CODE' ) ) {
			$allowed_languages = $area->get( 'wpml_lang' );

			if ( ! in_array( ICL_LANGUAGE_CODE, $allowed_languages, true ) ) {
				$state = 'hide_for_language';
			}
		}

		return $state;
	}
}

/**
 * Initialize the WPML integration
 *
 * @since 2.3.0
 */
function divi_areas_setup_wpml() {
	DAP_Integrate_Wpml::inst();
}

add_action( 'wpml_loaded', 'divi_areas_setup_wpml' );
