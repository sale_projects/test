<?php
/**
 * Divi Areas Pro template functions.
 *
 * Template functions are intended for use by theme or plugin developers. They must be
 * wrapped in a function_exists() check, so they can be overwritten by other plugins.
 *
 * @package Divi_Areas_Pro
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;


if ( ! function_exists( 'is_divi_area' ) ) :
	/**
	 * Checks, if the given post is a valid Divi Area.
	 *
	 * @since  1.0.0
	 * @param  mixed $post_id Either a post ID, a WP_Post object or false.
	 * @return bool True, when the specified post is a Divi Area.
	 */
	function is_divi_area( $post_id = false ) {
		$post = get_post( $post_id );

		if ( ! $post ) {
			$post_id = divi_areas_the_post_id();
			$post    = get_post( $post_id );
		}

		if ( ! $post ) {
			return false;
		}

		// The case is obvious when the post-type is "divi-area".
		if ( DIVI_AREAS_CPT === $post->post_type ) {
			return true;
		}

		// For saved Divi templates, we check the post-meta data to be sure.
		if (
			! defined( 'ET_BUILDER_LAYOUT_POST_TYPE' ) ||
			ET_BUILDER_LAYOUT_POST_TYPE !== $post->post_type
		) {
			return false;
		}

		// The post is a saved Divi Template, but could also be a row/module/section.
		$layout_type = wp_get_post_terms(
			$post->ID,
			'layout_type',
			[ 'fields' => 'names' ]
		);

		if ( ! in_array( 'layout', $layout_type, true ) ) {
			return false;
		}

		// Final check: We have a saved Divi Layout. Does it have Divi Area meta data?
		$config = get_post_meta( $post->ID, '_da_area', true );

		if ( $config && is_array( $config ) && ! empty( $config['layout_type'] ) ) {
			return true;
		}

		return false;
	}
endif;


if ( ! function_exists( 'et_core_intentionally_unescaped' ) ) :
	/**
	 * Pass thru semantical intentionally unescaped acknowledgement.
	 *
	 * Taken from Divi/core/components/data/init.php
	 *
	 * @since 1.4.3
	 *
	 * @param string $passthru Value that is passed through.
	 * @param string $excuse   Why the value should be allowed to stay unescaped.
	 * @return string
	 */
	function et_core_intentionally_unescaped( $passthru, $excuse ) {
		// Add valid excuses as they arise.
		$valid_excuses = [
			'cap_based_sanitized',
			'fixed_string',
			'react_jsx',
			'html',
			'underscore_template',
		];

		if ( ! in_array( $excuse, $valid_excuses, true ) ) {
			_doing_it_wrong(
				__FUNCTION__,
				esc_html__( 'This is not a valid excuse to not escape the passed value.', 'divi_areas' ),
				null
			);
		}

		return $passthru;
	}
endif;

if ( ! function_exists( 'divi_area_vb_version' ) ) {
	/**
	 * Returns the currently installed version of the Visual Builder Builder.
	 *
	 * When no Visual Builder plugin/theme is active, the function returns an empty
	 * string. When a `$min_version` param is defined, and the Visual Builder version
	 * is lower than this min-version, the function also returns an empty string.
	 *
	 * @since 2.0.1
	 * @param string $min_version Optional. The minimum required version.
	 * @return string The visual builder version, or an empty string.
	 */
	function divi_area_vb_version( $min_version = '' ) {
		global $dap_et_builder_product_version; // This is used for unit-testing.
		static $_dap_version_cache = [];

		$result = '';
		if ( isset( $dap_et_builder_product_version ) ) {
			unset( $_dap_version_cache[ $min_version ] );
		}

		if ( ! isset( $_dap_version_cache[ $min_version ] ) ) {
			if ( isset( $dap_et_builder_product_version ) ) {
				$vb_version = $dap_et_builder_product_version;
			} elseif ( defined( 'ET_BUILDER_PRODUCT_VERSION' ) ) {
				$vb_version = ET_BUILDER_PRODUCT_VERSION;
			} else {
				$vb_version = '';
			}

			if ( ! did_action( 'init' ) && ! $vb_version ) {
				_doing_it_wrong(
					'divi_area_vb_version',
					'Function called too early - reliable results are returned after the `init` action',
					'divi-areas-pro-2.0.1'
				);
			} else {
				if ( ! $vb_version ) {
					$_dap_version_cache[ $min_version ] = '';
				} elseif (
					! $min_version
					|| version_compare( $vb_version, $min_version, 'ge' )
				) {
					$_dap_version_cache[ $min_version ] = $vb_version;
				}
			}
		}

		if ( isset( $_dap_version_cache[ $min_version ] ) ) {
			$result = $_dap_version_cache[ $min_version ];
		}

		return $result;
	}
}
