<?php
/**
 * Divi Areas Pro helper functions. Those functions are intended for internal use or
 * by other plugin developers.
 *
 * Functions that are used by themes are located in the template-functions.php file!
 *
 * @package Divi_Areas_Pro
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

/**
 * Returns the current post ID.
 *
 * Logic is taken from Divi´s method `ET_Builder_Element::get_current_post_id()`.
 *
 * Retrieve Post ID from 1 of 4 sources depending on which exists:
 * - $_POST['current_page']['id']
 * - $_POST['et_post_id']
 * - $_GET['post']
 * - get_the_ID()
 *
 * @since 1.2.0
 * @return int The current post ID.
 */
function divi_areas_the_post_id() {
	// phpcs:disable

	// Getting correct post id in computed_callback request.
	if ( wp_doing_ajax() ) {
		if ( isset( $_POST['current_page'] ) && ! empty( $_POST['current_page']['id'] ) ) {
			return absint( $_POST['current_page']['id'] ); // input var okay.
		}
		if ( isset( $_POST['et_post_id'] ) ) {
			return absint( $_POST['et_post_id'] ); // input var okay.
		}
	}

	if ( isset( $_REQUEST['post'] ) ) {
		return absint( $_REQUEST['post'] ); // input var okay.
	}

	// phpcs:enable

	return get_the_ID();
}

/**
 * Takes a boolean string (on/off) and returns a real boolean value.
 *
 * When the responsive option is supported AND a responsive value is detected, then
 * the return value is an array with the attributes 'value', 'value_tablet', 'value_phone'.
 *
 * @since 2.0.0
 * @uses divi_areas_deserialize_responsive()
 *
 * @param string $value         The serialized value.
 * @param bool   $is_responsive Whether the value can contain responsive data.
 * @return array The deserialized data.
 */
function divi_areas_deserialize_bool( $value, $is_responsive = false ) {
	$result = false;

	if ( $is_responsive ) {
		$value = divi_areas_deserialize_responsive( $value );
	} elseif ( is_bool( $value ) ) {
		$result = $value;
	} else {
		$value = (string) $value;
	}

	if ( is_string( $value ) ) {
		$result = 'on' === strtolower( $value );
	} elseif ( is_array( $value ) ) {
		$result = [
			'value'        => 'on' === strtolower( $value[0] ),
			'value_tablet' => 'on' === strtolower( $value[1] ),
			'value_phone'  => 'on' === strtolower( $value[2] ),
		];
	}

	return $result;
}

/**
 * Takes a real boolean (or an array of booleans) and turns it into an on/off string.
 *
 * This is the reverse of divi_areas_deserialize_bool()
 *
 * @since 2.0.0
 * @uses divi_areas_serialize_responsive()
 *
 * @param string $value         The deserialized value.
 * @param bool   $is_responsive Whether the value can contain responsive data.
 * @return array The serialized data.
 */
function divi_areas_serialize_bool( $value, $is_responsive = false ) {
	if ( $is_responsive && is_array( $value ) ) {
		$value = [
			'value'        => empty( $value[0] ) ? 'off' : 'on',
			'value_tablet' => empty( $value[1] ) ? 'off' : 'on',
			'value_phone'  => empty( $value[2] ) ? 'off' : 'on',
		];
	} else {
		$value = empty( $value ) ? 'off' : 'on';
	}

	return divi_areas_serialize_responsive( $value );
}

/**
 * Parses the input string and splits it into an array with three elements, if a
 * responsive value is detected. The returned array is properly decoded and contains
 * values in the order desktop (0), tablet (1), phone (2).
 *
 * This function guarantees to return either a string or an array with three items.
 *
 * @since 2.0.0
 * @param string $value The serialized string value.
 * @return array|string Either the responsive array, or the input string.
 */
function divi_areas_deserialize_responsive( $value ) {
	if (
		$value
		&& is_string( $value )
		&& 2 === substr_count( $value, '|' )
	) {
		$value = array_map( 'urldecode', explode( '|', $value ) );
	} else {
		$value = (string) $value;
	}

	return $value;
}

/**
 * Takes a string (non-responsive) or an array with three device-specific values (responsive) and turns it into a single string that can be saved to the post-meta table.
 *
 * This is the reverse of divi_areas_deserialize_responsive()
 *
 * @since 2.0.0
 * @param array|string $value The actual value.
 * @return string A serialized string value.
 */
function divi_areas_serialize_responsive( $value ) {
	if ( is_array( $value ) ) {
		$value = implode( '|', array_map( 'urldecode', $value ) );
	} else {
		$value = (string) $value;
	}

	return $value;
}

/**
 * Takes a numeric string and returns an array that contains a value and unit element.
 *
 * Attributes in the response array:
 *
 * @type {mixed}  value       Either an int (7, -24), a float (12.34, -9.2) or a
 *                            string ("auto", "").
 * @type {float}  value_float Guaranteed to be a valid float. When value contains a
 *                            number, this is the float representation of that number.
 *                            Otherwise value_float is 0.
 * @type {string} unit        The unit ("%", "px", etc.) or empty, when value is a
 *                            string.
 *
 * @since 2.0.0
 * @param string $value The number string.
 * @return array An array with the attributes value, value_float, unit.
 */
function divi_areas_parse_number( $value ) {
	$parsed = [
		'value'       => '',
		'value_float' => (float) 0,
		'unit'        => '',
	];

	if ( is_string( $value ) ) {
		preg_match( '/^([+\-]?[\d\.]+)?\s*(.*)$/', $value, $res );

		if ( strlen( $res[1] ) ) {
			if ( false !== strpos( $res[1], '.' ) ) {
				$parsed['value'] = floatval( ltrim( $res[1], '+' ) );
			} else {
				$parsed['value'] = intval( ltrim( $res[1], '+' ) );
			}
			$parsed['value_float'] = (float) $parsed['value'];
			$parsed['unit']        = trim( $res[2] );
		} else {
			$parsed['value'] = $res[2];
		}
	}

	return $parsed;
}


/**
 * Minifies the JS code in all elements of the provided array.
 *
 * This logic is not bulletproof and only tested with Divi Areas "js-part" scripts.
 * Possibly this function will correctly minify many other scripts, but the logic is
 * not very smart - it basically replaces a set of predefined strings.
 *
 * @since 2.0.0
 * @param string|array $code   JS code parts.
 * @param array        $uglify List of variable names that should be uglified.
 * @return string|array Minified version of the input $code.
 */
function divi_areas_minify_js( $code, $uglify = [] ) {
	$minified      = [];
	$short_vars    = [];
	$return_string = false;

	if ( is_string( $code ) ) {
		$return_string = true;
		$code          = [ $code ];
	}

	if ( ! $code || ! is_array( $code ) ) {
		return $return_string ? '' : [];
	}

	// Prepare regex to uglify symbol names.
	foreach ( $uglify as $index => $var_name ) {
		$short  = '';
		$number = $index;

		for ( $chr = 1; $number >= 0 && $chr < 10; $chr++ ) {
			$short   = chr( 97 + ( $number % pow( 26, $chr ) / pow( 26, $chr - 1 ) ) ) . $short;
			$number -= pow( 26, $chr );
		}

		$regex = sprintf(
			'/(^|>=|<=|:|[%1$s])%2$s([%1$s]|$)/',
			'\\s' . preg_quote( '\+-*&|!;,.()[]{}<>=', '/' ),
			preg_quote( $var_name, '/' )
		);

		$short_vars[ $regex ] = $short;
	}

	// General minification patterns.
	$minify_terms = [
		'true'  => '!0',
		'false' => '!1',
	];

	// Removes unnecessary spaces around operators.
	$regex_spaces = '/\s*(var |[,:;{}()<>+\-\*\/=&\|\[\]!])\s*/';

	// Minify each element of the code-array.
	foreach ( $code as $segment ) {
		$parts   = [];
		$segment = str_replace( [ "\r", "\n", "\t" ], ' ', $segment );

		/**
		 * Split the code-segment into code and string token. Everything between
		 * double quotes is a string and will not be minified.
		 *
		 * Regex based on the following StackOverflow answer:
		 *
		 * @see https://stackoverflow.com/a/2202489/313501
		 */
		$regex = '/"(?:\\\\.|[^\\\\"])*"|[^;"]*;?/';
		preg_match_all( $regex, $segment, $all_token );

		if ( $all_token ) {
			foreach ( $all_token[0] as $token ) {
				$token = trim( $token );

				if ( false !== strpos( $token, 'areaOpen' ) ) {
					$token = $token;
				}

				// Do not modify strings and continue with next token.
				if ( $token && '"' === $token[0] ) {
					$parts[] = $token;
					continue;
				}

				// Remove inline comments.
				$token = preg_replace( '/\s*\/\/.*$/', '', $token );

				if ( $token ) {
					$token = preg_replace( $regex_spaces, '$1', $token );

					// Shorten general terms, remove unnecessary spaces.
					$token = str_replace(
						array_keys( $minify_terms ),
						array_values( $minify_terms ),
						$token
					);

					// Convert ".00" numbers to integers.
					$token = preg_replace( '/(\D\d+)\.0+/', '$1', $token );

					// Uglify variables and function names.
					foreach ( $short_vars as $regex => $short ) {
						$token = preg_replace( $regex, "\$1$short\$2", $token );
					}

					$parts[] = trim( $token );
				}
			}
		}

		$minified_part = implode( '', $parts );
		$minified_part = str_replace( ';}', '}', $minified_part );

		// Remove block comments.
		$minified_part = preg_replace( '!/\*.+?\*/!', '', $minified_part );

		$minified[] = $minified_part;
	}

	// Return the minified code in same format as the input parameter.
	return $return_string ? implode( '', $minified ) : $minified;
}
