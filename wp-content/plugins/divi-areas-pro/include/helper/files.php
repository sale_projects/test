<?php
/**
 * Divi Areas Pro helper functions. Those functions are intended for internal use or
 * by other plugin developers.
 *
 * @package Divi_Areas_Pro
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

/**
 * Loads the specified template.
 *
 * This function uses extract() to import variables from the $_variables array into
 * the current symbol table. It is safe, because it runs inside a function scope and
 * does not overwrite already defined symbols (EXTR_SKIP). We also prefix the
 * parameters, so there's less likelihood of actually needing this flag.
 *
 * @since 2.0.0
 * @param string $__path      The template path, relative to the template folder.
 * @param array  $__variables List of variables to expand and pass to the template.
 * @return void
 */
function divi_areas_include_template( $__path, $__variables = [] ) {
	if ( false === strpos( $__path, '.php', 1 ) ) {
		$__path .= '.php';
	}

	$__full_path = DIVI_AREAS_PATH . 'template/' . $__path;

	if ( file_exists( $__full_path ) ) {

		// phpcs:ignore WordPress.PHP.DontExtract.extract_extract
		extract( $__variables, EXTR_SKIP );

		include $__full_path;
	}
}

/**
 * Loads the specified template and returns the output of the template.
 *
 * @since 2.0.0
 * @param string $__path      The template path, relative to the template folder.
 * @param array  $__variables List of variables to expand and pass to the template.
 * @return string The template output.
 */
function divi_areas_parse_template( $__path, $__variables = [] ) {
	ob_start();
	divi_areas_include_template( $__path, $__variables );
	return ob_get_clean();
}

/**
 * Reads the specified data file (from the uploads folder) and returns the
 * deserialized value.
 *
 * @since 2.1.0
 * @param string $file   Name of the data file inside the given folder. The '.json'
 *                       extension will be automatically added when needed.
 * @return mixed The deserialized value, or null on failure.
 */
function divi_areas_read_data( $file = 'data.json' ) {
	$result = null;

	if ( divi_areas_has_data( $file ) ) {
		$fs        = divi_areas_get_fs();
		$file_path = divi_areas_get_data_path( $file );

		$serialized = $fs->get_contents( $file_path );
		if ( ! $serialized ) {
			$serialized = '';
		}

		try {
			$result = json_decode( $serialized, true );
		} catch ( Exception $ex ) {
			$result = null;
		}
	}

	return $result;
}

/**
 * Writes the given object into the specified data file (inside the uploads folder).
 *
 * @since 2.1.0
 * @param string $file   Name of the data file inside the given folder. The '.json'
 *                       extension will be automatically added when needed.
 * @param mixed  $value  The value to save. It will be json-encoded before writing it
 *                       to the filesystem.
 * @return bool True on success.
 */
function divi_areas_write_data( $file, $value ) {
	$fs       = divi_areas_get_fs();
	$base_dir = divi_areas_get_data_path( '' );
	wp_mkdir_p( $base_dir );

	$file_path = divi_areas_get_data_path( $file );

	if ( ! is_string( $value ) ) {
		$value = wp_json_encode( $value );
	}

	$result = $fs->put_contents( $file_path, $value );

	return $result;
}

/**
 * Deletes a single data file from the local data directory.
 *
 * @since 2.1.0
 * @param string $file The file to delete. The '.json' ending will be added when it's
 *                     missing.
 * @return void
 */
function divi_areas_remove_data( $file ) {
	if ( divi_areas_has_data( $file ) ) {
		$file_path = divi_areas_get_data_path( $file );
		$fs        = divi_areas_get_fs();

		$fs->delete( $file_path, false, 'f' );
	}
}

/**
 * Checks, if the given file exists inside the data folder.
 *
 * @since 2.1.0
 * @param string $file Name of the file (relative to the data folder).
 * @return bool True, if the file exists.
 */
function divi_areas_has_data( $file ) {
	$result = false;
	$fs     = divi_areas_get_fs();

	if ( $fs ) {
		$file_path = divi_areas_get_data_path( $file );
		$result    = $fs->exists( $file_path );
	}

	return $result;
}

/**
 * Returns a list of all .json files form the data folder.
 *
 * @since 2.1.0
 * @return array
 */
function divi_areas_list_data() {
	$fs       = divi_areas_get_fs();
	$data_dir = divi_areas_get_data_path( '' );
	$files    = $fs->dirlist( $data_dir, false, false );
	$result   = [];

	if ( is_array( $files ) ) {
		foreach ( $files as $infos ) {
			if ( strpos( $infos['name'], '.json' ) > 1 ) {
				$result[] = $infos['name'];
			}
		}
	}

	return $result;
}

/**
 * Returns the absolute path to the specified file in the plugins data folder.
 *
 * This method does not validate if the file exists (or the data-folder), it only
 * builds the expected path to the file.
 *
 * When no filename is specified, the directory path is returned (without a trailing
 * slash!). When a file is specified, it will be part of the resulting path - if
 * needed, the extension ".json" is added to the filename.
 *
 * @since 2.1.0
 * @param string $file The filename, inside the data folder.
 * @return string The absolute path to the file.
 */
function divi_areas_get_data_path( $file = '' ) {
	$upload_dir = wp_upload_dir();
	$folder     = 'divi-areas-layouts';
	$result     = $upload_dir['basedir'] . DIRECTORY_SEPARATOR . $folder;
	$file       = trim( $file, '\\/' );

	if ( $file ) {
		if ( false === strpos( $file, '.json', 1 ) ) {
			$file .= '.json';
		}

		$result .= DIRECTORY_SEPARATOR . $file;
	}

	return $result;
}

if ( ! function_exists( 'divi_areas_get_fs' ) ) :
	/**
	 * Returns a WP Filesystem API instance.
	 *
	 * Logic of this method is based on code from ET_Core_Cache_Directory.
	 *
	 * @since 2.1.0
	 * @return WP_Filesystem_Base
	 */
	function divi_areas_get_fs() {
		static $da_fs = null;

		if ( null === $da_fs ) {
			require_once ABSPATH . 'wp-admin/includes/file.php';

			// phpcs:ignore WordPress.PHP.NoSilencedErrors.Discouraged
			if ( defined( 'DIVI_AREAS_PATH' ) && @WP_Filesystem( [], DIVI_AREAS_PATH, true ) ) {
				// We can write to the plugins base directory.
				$da_fs = $GLOBALS['wp_filesystem'];
			}

			// phpcs:ignore WordPress.PHP.NoSilencedErrors.Discouraged
			if ( ! $da_fs && @WP_Filesystem( [], false, true ) ) {
				// We can write to WP_CONTENT_DIR.
				$da_fs = $GLOBALS['wp_filesystem'];
			}

			if ( ! $da_fs ) {
				$uploads_dir = (object) wp_get_upload_dir();

				// phpcs:ignore WordPress.PHP.NoSilencedErrors.Discouraged
				if ( @WP_Filesystem( [], $uploads_dir->basedir, true ) ) {
					// We can write to the uploads directory.
					$da_fs = $GLOBALS['wp_filesystem'];
				}
			}

			if ( ! $da_fs ) {
				// We aren't able to write to the filesystem so let's just make sure
				// to return an instance of the filesystem base class so that calling
				// it won't cause errors.
				require_once ABSPATH . 'wp-admin/includes/class-wp-filesystem-base.php';

				// Write notice to log when WP_DEBUG is enabled.
				$nl   = PHP_EOL;
				$msg  = 'Unable to write to filesystem. Please ensure that PHP has write access to one of ';
				$msg .= "the following directories:{$nl}{$nl}\t- WP_CONTENT_DIR{$nl}\t- wp_upload_dir(){$nl}\t- DIVI_AREAS_PATH.";

				if ( function_exists( 'et_debug' ) ) {
					et_debug( $msg );
				}

				$da_fs = new WP_Filesystem_Base();
			}
		}

		return $da_fs;
	}
endif;
