<?php
/**
 * Handles the on-site logic for plugin updates, such as displaying the about-page or
 * updating the database.
 *
 * @package Divi_Areas_Pro
 */

/**
 * On-Site update logic.
 *
 * @since 2.0.0
 */
class DAP_Updater
	extends DAP_Component {

	/**
	 * List of all update routines that must be called in order.
	 *
	 * When a new "update_to_x_x_x()" method is created, that version must be added to
	 * this update path list.
	 *
	 * This list ensures that all updates run in the correct order, even when users
	 * skip a version.
	 *
	 * @since 2.0.1
	 * @var array
	 */
	private $update_path = [
		'2.0.0',
		'2.0.1',
		'2.2.0',
		'2.3.0',
	];

	/**
	 * The slug for our welcome page. It will be added to the `/wp-admin/index.php`
	 * page using the `?page=SLUG` parameter.
	 *
	 * @since 2.0.0
	 * @var string
	 */
	const WELCOME_PAGE_SLUG = 'about-divi-areas-pro';

	/**
	 * Holds the menu-hook of the hidden welcome page menu item.
	 *
	 * @since 2.0.0
	 * @var string
	 */
	private $menu_hook = '';

	/**
	 * Flag that tells us, that some plugin details changed which require a page
	 * reload to take effect.
	 *
	 * @since 2.0.0
	 * @var bool
	 */
	private $reload_page = false;

	/**
	 * Flag that tells us, that the updater needs more time and will continue on the
	 * next page load.
	 *
	 * @since 2.0.0
	 * @var bool
	 */
	private $completed = true;

	/**
	 * Whether the current request is a qualified request on a wp-admin page. False
	 * means, that the request is either on the front-end, or it is an Ajax/REST
	 * request, etc.
	 *
	 * Set and used by `DAP_Updater::is_valid_request()`
	 *
	 * @since 2.0.0
	 * @var bool
	 */
	private $user_request = null;

	/**
	 * Number of Areas that are updated in a single request.
	 *
	 * @since 2.0.1
	 * @var int
	 */
	private $batch_size = 50;

	/**
	 * The version to which we update.
	 *
	 * During update process from 2.0.0 to 2.0.1 this will be "2.0.1".
	 *
	 * @since 2.0.1
	 * @var string
	 */
	private $version_to = '';

	/**
	 * List of possible error messages to display. Initialized by `check_for_update()`.
	 *
	 * @since 2.0.1
	 * @var array
	 */
	private $messages = [];

	/**
	 * Called during the "plugins_loaded" action. Hooks up the admin menu, etc.
	 *
	 * @since  2.0.0
	 * @return void
	 */
	public function setup() {
		if ( ! is_admin() ) {
			return;
		}

		add_action(
			'admin_init',
			[ $this, 'check_for_update' ]
		);

		add_action(
			'admin_menu',
			[ $this, 'admin_menu' ]
		);

		add_action(
			'divimode_activation_redirect_divi-areas',
			[ $this, 'redirect_to_welcome_page' ]
		);

		add_action(
			'admin_enqueue_scripts',
			[ $this->module( 'asset' ), 'enqueue_styles_about_page' ]
		);
	}

	/**
	 * Returns the `$user_request` member; when the member has no value, it is
	 * initialized first.
	 *
	 * @since 2.0.0
	 * @return bool
	 */
	public function is_valid_request() {
		if ( null === $this->user_request ) {
			$this->user_request = false;

			if (
				is_admin() // Only handle admin-page requests.
				&& is_user_logged_in() // Make sure the user is already logged in.
				&& $this->settings_library()->has_permission() // User can access Divi Areas Pro.
				&& ( // The request must not be Ajax or REST.
					( ! defined( 'DOING_AJAX' ) || ! DOING_AJAX )
					&& ( ! defined( 'REST_REQUEST' ) || ! REST_REQUEST )
				)
			) {
				$this->user_request = true;
			}
		}

		return $this->user_request;
	}

	/**
	 * Checks, whether the plugin version changed since the last request.
	 *
	 * @since  2.0.0
	 * @return void
	 */
	public function check_for_update() {
		if ( self::settings_library()->has_plugin_version_changed() ) {
			$this->messages = [
				'areas' => [
					'one'  => __( '1 Divi Area still needs to be updated', 'divi_areas' ),
					// translators: The placeholder is the number of pending migrations.
					'many' => __( '%d Divi Areas still need to be updated', 'divi_areas' ),
				],
			];

			$next_version = $this->get_next_update_version();

			/*
			 * This state of the updater does not perform checks yet, as the update
			 * process is pre-defined and cannot be influenced by an user.
			 *
			 * This part might run during a heartbeat Ajax request and update the DB
			 * in the background.
			 */
			$update_handler = 'update_to_' . str_replace( '.', '_', $next_version );

			if ( $next_version && method_exists( $this, $update_handler ) ) {
				$this->version_to = $next_version;

				$this->{$update_handler}();
			}

			// Complete: Mark the migration as done, flush caches, display the about page.
			if ( $this->completed ) {
				// Update the DB to the new version.
				self::settings_library()->set_db_version( $this->version_to );

				// And check, if another update is pending.
				$next_version = $this->get_next_update_version();

				if ( ! $next_version ) {
					// No other update available. Finish the update process.
					self::settings_library()->plugin_update_done();

					$this->flush_all_caches();
				} else {
					// There's another update pending that needs to be installed.
					$this->is_incomplete(
						sprintf(
						// translators: The placeholder contains a version number, e.g., "2.0.1".
							__( 'Unprocessed update found for version %s', 'divi_areas' ),
							$next_version
						)
					);
				}
			}

			/*
			 * Incomplete: Maybe reload the current page after a short delay. When the
			 * request is not "valid" (e.g., an Ajax request) then simply do nothing.
			 * The next valid request, or the next Ajax request will pick up where we
			 * have stopped now and continue the upgrade process.
			 */
			if ( ! $this->completed && $this->is_valid_request() ) {
				echo '<script>setTimeout(function(){location.reload(true);},300)</script>';
				exit;
			}

			// phpcs:ignore WordPress.Security.NonceVerification.Recommended
			if ( ! is_network_admin() && empty( $_GET['activate-multi'] ) ) {
				// Redirect to Welcome Page.
				$this->redirect_to_welcome_page();
			}

			if ( $this->reload_page && $this->is_valid_request() ) {
				// Reload the current page.
				$redirect_to = add_query_arg( [] );
				wp_safe_redirect( $redirect_to );
				exit;
			}
		} elseif (
			$this->is_valid_request()
			&& '1' === get_transient( '_da__show_about_page' )
		) {
			/*
			 * The update was handled by an un-qualified request (possibly heartbeat).
			 * Redirect the user to the welcome page when he can actually see that
			 * page.
			 */
			$this->redirect_to_welcome_page();
		}
	}

	/**
	 * Clears the cache after updating the plugin.
	 * This method attempts to clear any available cache that might have
	 * minified JS/CSS code of our plugin.
	 *
	 * @since 2.3.0
	 * @return void
	 */
	public function flush_all_caches() {
		try {
			// W3 Total Cache.
			if ( function_exists( 'w3tc_minify_flush' ) ) {
				w3tc_minify_flush();
			}
			if ( function_exists( 'w3tc_flush_all' ) ) {
				w3tc_flush_all();
			}

			// WPMU DEV Hummingbird.
			if ( class_exists( 'WP_Hummingbird' ) ) {
				/** @noinspection PhpUndefinedClassInspection */
				WP_Hummingbird::flush_cache( false, false );
			}

			// WP Rocket.
			if ( function_exists( 'rocket_clean_domain' ) ) {
				rocket_clean_domain();
			}

			// WP Fastest Cache.
			if (
				isset( $GLOBALS['wp_fastest_cache'] )
				&& method_exists( $GLOBALS['wp_fastest_cache'], 'deleteCache' )
			) {
				$GLOBALS['wp_fastest_cache']->deleteCache( true );
			}

			// Litespeed Cache.
			do_action( 'litespeed_purge_all', 'Divi Areas Pro updated' );

			// Comet Cache.
			if ( is_callable( 'comet_cache::wipe' ) ) {
				/** @noinspection PhpUndefinedClassInspection */
				comet_cache::wipe();
			}

			// Host: SG Optimizer.
			if ( isset( $GLOBALS['sg_cachepress_supercacher'] ) ) {
				global $sg_cachepress_supercacher;
				$sg_cachepress_supercacher->purge_everything();
				$sg_cachepress_supercacher->delete_assets();
			}

			// Host: Pantheon Advanced Page Cache.
			if ( function_exists( 'pantheon_wp_clear_edge_all' ) ) {
				pantheon_wp_clear_edge_all();
			}

			// Host: WPEngine.
			if ( class_exists( 'WpeCommon' ) ) {
				if ( is_callable( 'WpeCommon::purge_memcached' ) ) {
					/** @noinspection PhpUndefinedClassInspection */
					WpeCommon::purge_memcached();
				}

				if ( is_callable( 'WpeCommon::clear_maxcdn_cache' ) ) {
					/** @noinspection PhpUndefinedClassInspection */
					WpeCommon::clear_maxcdn_cache();
				}

				if ( is_callable( 'WpeCommon::purge_varnish_cache' ) ) {
					/** @noinspection PhpUndefinedClassInspection */
					WpeCommon::purge_varnish_cache();
				}
			}

			// Host: Kinsta.
			if (
				class_exists( '\Kinsta\Cache' )
				&& isset( $GLOBALS['kinsta_cache'] )
				&& is_object( $GLOBALS['kinsta _cache'] )
			) {
				global $kinsta_cache;

				if (
					isset( $kinsta_cache->kinsta_cache_purge )
					&& method_exists( $kinsta_cache->kinsta_cache_purge, 'purge_complete_caches' )
				) {
					$kinsta_cache->kinsta_cache_purge->purge_complete_caches();
				}
			}

			// WordPress "native" cache.
			wp_cache_flush();
		} catch ( Exception $exception ) {
			// phpcs:ignore WordPress.PHP.DevelopmentFunctions.error_log_error_log
			error_log( 'Divi Areas Pro could not clear the website cache after update: ' . $exception->getMessage() );
		}

		// Clear front end caches.
		$this->module( 'front' )->clear_all_caches();

		// Clear all admin-side caches.
		$this->module( 'admin' )->clear_all_caches();
	}

	/**
	 * Returns the version number of the oldest missing update.
	 *
	 * @since 2.0.1
	 * @return string A version number (e.g., "2.0.1"), or an empty string when all
	 *                updates were processed.
	 */
	private function get_next_update_version() {
		$result = '';

		$old_version = self::settings_library()->get_db_version();

		/*
		 * Loop entire list of historic plugin versions and find the first (= oldest)
		 * version that was not processed yet.
		 */
		foreach ( $this->update_path as $next_version ) {
			if ( version_compare( $old_version, $next_version, 'lt' ) ) {
				$result = $next_version;
				break;
			}
		}

		return $result;
	}

	/**
	 * Returns the menu hook of the welcome page menu item.
	 *
	 * @since 2.0.0
	 * @return string The menu hook.
	 */
	public function get_menu_hook() {
		return $this->menu_hook;
	}

	/**
	 * Returns the URL to the welcome page tab.
	 *
	 * @since 2.0.0
	 * @return string The full URL.
	 */
	public function get_welcome_page_url() {
		$current_url = add_query_arg( [] );

		return add_query_arg(
			[
				'page' => self::WELCOME_PAGE_SLUG,
				'from' => rawurlencode( $current_url ),
			],
			admin_url( 'index.php' )
		);
	}

	/**
	 * Add an invisible entry to the admin menu for the welcome page.
	 *
	 * @since  2.0.0
	 * @return void
	 */
	public function admin_menu() {
		$this->menu_hook = add_submenu_page(
			null, // No parent: This will hide the item from the menu, while redirects will still work.
			__( 'Divi Areas Pro', 'divi_areas' ),
			'', // No menu name.
			'read', // Everybody can access the page.
			self::WELCOME_PAGE_SLUG,
			[ $this, 'show_welcome_page' ]
		);
	}

	/**
	 * Redirect the current request to the welcome page.
	 *
	 * @since 2.0.0
	 * @return void
	 */
	public function redirect_to_welcome_page() {
		/*
		 * When we somehow ended up in an Ajax request (such as heartbeat) or even
		 * REST we do not redirect the ajax request. Instead, we set a transient to
		 * redirect the user on the next non-ajax request to the about page.
		 */
		if ( ! $this->is_valid_request() ) {
			set_transient( '_da__show_about_page', '1', 900 );
		} else {
			delete_transient( '_da__show_about_page' );

			$redirect_to = $this->get_welcome_page_url();

			wp_safe_redirect( $redirect_to );
			exit;
		}
	}

	/**
	 * Displays the welcome page contents.
	 *
	 * @since 2.0.0
	 * @return void
	 */
	public function show_welcome_page() {
		divi_areas_include_template( 'about' );
	}

	/**
	 * Instructs the Updater to reload the current page in the end, or to display the
	 * about page.
	 *
	 * @since 2.0.0
	 * @return void
	 */
	protected function need_page_reload() {
		$this->reload_page = true;
	}

	/**
	 * Marks the migration as "incomplete" - the page is reloaded without marking the
	 * update as "done". As a result, the updater will continue the process on the
	 * next page load.
	 *
	 * @since 2.0.0
	 *
	 * @param string $reason A message that will be displayed to the user.
	 *
	 * @return void
	 */
	protected function is_incomplete( $reason ) {
		$this->completed = false;

		// Only display status infos to qualified users.
		if ( $this->is_valid_request() ) {
			printf(
				'<!doctype html><html>
				<head><title>Divi Areas Pro</title></head>
				<body><div class="wrapper">
				<h1>%1$s</h1>
				<p class="reason">%2$s</p>
				<p class="notice">%3$s</p>
				</div></body>
				<style>body{
					background: #f1f1f1;
					color: #444;
					font-family: -apple-system,BlinkMacSystemFont,"Segoe UI",Roboto,Oxygen-Sans,Ubuntu,Cantarell,"Helvetica Neue",sans-serif;
					font-size: 13px;
					padding: 0;
				}
				.wrapper{
					max-width: 800px;
					text-align: center;
					padding: 30px;
					border: 1px solid #e5e5e5;
					background: #fff;
					margin: 50px auto 0;
					line-height: 1.4;
				}
				h1 {
					margin: 0 0 25px;
				}
				.reason {
					font-size: 16px;
					color: #222;
					margin: 20px 0;
				}
				.notice{
					margin: 20px 0 0;
				}</style>',
				esc_html__( 'We\'re still updating your website &hellip;', 'divi_areas' ),
				esc_html( $reason ),
				esc_html__( '(You do not need to do anything)', 'divi_areas' )
			);
		}
	}

	/**
	 * Sets the correct "update-incomplete" message depending on the $done and $total
	 * params.
	 *
	 * @since 2.0.1
	 *
	 * @param int   $done     Number of migrated items.
	 * @param int   $total    Number of total items.
	 * @param array $messages Array with two elements: "one" and "many"".
	 *
	 * @return bool True, when migration is incomplete.
	 */
	private function handle_processed_data( $done, $total, $messages ) {
		if ( $done < $total ) {
			$pending = $total - $done;

			if ( 1 === $pending ) {
				$message = $messages['one'];
			} else {
				$message = sprintf( $messages['many'], $pending );
			}

			$this->is_incomplete( $message );

			return true;
		}

		return false;
	}

	/**
	 * Returns an array of all Divi Areas (including Areas in the template library.)
	 *
	 * @since 2.0.1
	 *
	 * @param string $minimum_version Optional. Only Areas with a lower version than
	 *                                this version are returned. Defaults to
	 *                                `$this->version_to` when omitted.
	 *
	 * @return array List of Areas that do not have the requested version.
	 */
	private function get_all_area_ids( $minimum_version = '' ) {
		global $wpdb;

		if ( ! $minimum_version ) {
			$minimum_version = $this->version_to;
		}

		/*
		 * Split the version string into an array that consists of at least 4 integer
		 * segments. For example "2.1" is split into [2,1,0,0].
		 */
		$ver = explode( '.', trim( $minimum_version . '.0.0.0.0', '.' ), 4 );
		$ver = array_map( 'intval', $ver );

		/*
		 * The following SQL query is optimized for performance and intentionally not
		 * cached, because it is only used when the plugin version changes. It is
		 * vital that this query returns uncached results from the DB!
		 *
		 * The query finds all Areas that have a LOWER version number than the
		 * specified minimum_version parameter: When the minimum version is "2.2.0"
		 * then only Areas with version "2.1.9.9" and lower are returned.
		 *
		 * @see https://stackoverflow.com/a/64892177/313501
		 */

		// phpcs:disable WordPress.DB.PreparedSQL.InterpolatedNotPrepared
		$query = $wpdb->prepare(
			"SELECT
				area_id
			FROM (
			    SELECT
					area_id,
					area_ver,
					SUBSTRING_INDEX(area_ver, '.', 1) + 0 AS ver1,
					SUBSTRING_INDEX(SUBSTRING_INDEX(area_ver, '.', 2), '.', -1) + 0 AS ver2,
					SUBSTRING_INDEX(SUBSTRING_INDEX(area_ver, '.', 3), '.', -1) + 0 AS ver3,
					SUBSTRING_INDEX(SUBSTRING_INDEX(area_ver, '.', 4), '.', -1) + 0 AS ver4
				FROM (
					SELECT
						area_id,
						CONCAT(area_ver, REPEAT('.0', 3 - CHAR_LENGTH(area_ver) + CHAR_LENGTH(REPLACE(area_ver, '.', '')))) AS area_ver
					FROM (
						SELECT DISTINCT
							post.ID AS area_id,
							COALESCE(meta_ver.meta_value, '1') AS area_ver
						FROM {$wpdb->posts} AS post
						INNER JOIN {$wpdb->postmeta} AS meta_data
							ON meta_data.post_id = post.ID AND (meta_data.meta_key = '_divi_area' OR meta_data.meta_key = '_da_area')
						LEFT JOIN {$wpdb->postmeta} AS meta_ver
							ON meta_ver.post_id = post.ID AND meta_ver.meta_key = '_da_version'
						WHERE
							post.post_type IN (%s, 'et_pb_layout')
					) AS raw_data
				) AS sane_data
			) AS final_data
			WHERE
				ver1 <= {$ver[0]}
				AND (ver2 <= {$ver[1]} OR ver1 < {$ver[0]})
				AND (ver3 <= {$ver[2]} OR ver2 < {$ver[1]} OR ver1 < {$ver[0]})
				AND (ver4 < {$ver[3]} OR ver3 < {$ver[2]} OR ver2 < {$ver[1]} OR ver1 < {$ver[0]})
			;",
			DIVI_AREAS_CPT
		);

		// phpcs:disable WordPress.DB.PreparedSQL.NotPrepared
		// phpcs:disable WordPress.DB.DirectDatabaseQuery.DirectQuery
		// phpcs:disable WordPress.DB.DirectDatabaseQuery.NoCaching
		$post_ids = $wpdb->get_col( $query, 0 );
		// phpcs:enable

		if ( is_wp_error( $post_ids ) ) {
			return [];
		} else {
			return array_map( 'intval', $post_ids );
		}
	}

	/**
	 * Loop all Areas and save each Area once without any changes.
	 *
	 * When calling the $area->save_config() method, the plugin will check if any Area
	 * configuration details need to be migrated or added. After this function is
	 * done, all Areas are compatible with the latest Area options. At the end of
	 * save_config(), the Area version is saved to the post-meta field `_da_version`.
	 *
	 * @since 2.3.0
	 * @return void
	 */
	private function update_all_areas() {
		/*
		 * Update details of relevant posts:
		 * All Divi Areas and saved Areas in the Template Library.
		 *
		 * We get all unprocessed Area IDs but only process 25 posts at once.
		 */
		$area_ids = $this->get_all_area_ids();

		if ( $area_ids ) {
			$processed = 0;

			foreach ( $area_ids as $post_id ) {
				$area = DAP_Area::get_area( $post_id );
				$area->save_config();
				$processed ++;

				if ( $processed >= $this->batch_size ) {
					break;
				}
			}

			$this->handle_processed_data(
				$processed,
				count( $area_ids ),
				$this->messages['areas']
			);
		}
	}

	/**
	 * Updates the Area configuration of a single Area to the latest plugin standards.
	 *
	 * Note, that this function access the $area->config array directly, which is
	 * discouraged in third party integrations. It's better to use $area->get() and
	 * $area->set() in most cases.
	 *
	 * @since 2.3.0
	 *
	 * @param int   $area_id The post ID of the Area.
	 * @param array $config  The most recent area configuration.
	 *
	 * @return array The updated and sanitized area meta data.
	 */
	public static function update_area( $area_id, $config ) {
		$v1_meta = false;

		if ( ! is_array( $config ) ) {
			$config = [];
		}

		// Try to determine the Area version based on available post-meta data.
		if ( empty( $config['version'] ) ) {
			$v1_meta    = get_post_meta( $area_id, '_divi_area', true );
			$v2_meta    = get_post_meta( $area_id, '_da_area', true );
			$v2_version = get_post_meta( $area_id, '_da_version', true );

			if ( $v2_meta && is_array( $v2_meta ) ) {
				if ( $v2_version ) {
					$config['version'] = substr( $v2_version, 0, 3 ) . '.0';
				} elseif ( isset( $v2_meta['condition'] ) ) {
					$config['version'] = '2.2.0';
				} elseif ( isset( $v2_meta['overflow'] ) ) {
					$config['version'] = '2.1.0';
				} else {
					$config['version'] = '2.0.0';
				}
			} elseif ( $v1_meta && is_array( $v1_meta ) ) {
				$config['version'] = '1.x';
			}
		} elseif ( DIVI_AREAS_VERSION === $config['version'] ) {
			// Calling this function on an already updated Area has no effect.
			return $config;
		}

		/*
		 *
		 * Update from 1.x to 2.2.0
		 *
		 * Sample 1.1.2 Area (old):
		 * {"triggers":[{"type":"on-scroll","selector":"","delay":"0","offset":"10","offsetType":"percent"}],"locations":[],"limit_role":[],"exclude_posts":[],"include_posts":[49082],"disable_on_mobile":false,"disable_on_tablet":false,"disable_on_desktop":false,"hide_close":false,"is_modal":false,"keep_closed":false,"dark_close":false,"singleton":false,"box_shadow":false,"close_for":1,"theme_location":"none","theme_location_pos":"before","layout_type":"popup","which_page":"none","close_span":"day","box_width":""}
		 *
		 * Note: This code updates the Area to 2.2.0 directly!
		 *
		 *
		 */
		if ( '1.x' === $config['version'] ) {
			$config['layout_type']       = $v1_meta['layout_type'];
			$config['show_close']        = ! $v1_meta['hide_close'];
			$config['not_modal']         = ! $v1_meta['is_modal'];
			$config['singleton']         = $v1_meta['singleton'];
			$config['keep_closed']       = $v1_meta['keep_closed'];
			$config['box_shadow']        = $v1_meta['box_shadow'];
			$config['dark_close']        = $v1_meta['dark_close'];
			$config['close_trigger']     = $v1_meta['close_trigger'];
			$config['close_delay']       = number_format( (float) $v1_meta['close_delay'], 2 ) . 'sec';
			$config['close_for']         = $v1_meta['close_for'] . $v1_meta['close_span'];
			$config['z_index']           = $v1_meta['zindex'];
			$config['max_width']         = $v1_meta['box_width'];
			$config['max_height']        = $v1_meta['box_height'];
			$config['role_limitation']   = false;
			$config['limit_role']        = [];
			$config['page_limitation']   = false;
			$config['postlist']          = [];
			$config['inline_location']   = '';
			$config['location_position'] = '';
			$config['location_selector'] = '';
			$config['triggers']          = [];
			$config['triggers_hover']    = [];
			$config['condition']         = [
				'cond_device' => [],
			];

			if ( 'inline' !== $config['layout_type'] ) {
				$config[ 'position_' . $config['layout_type'] ] = str_replace( '-', '_', $v1_meta['position'] );
			}

			if ( is_array( $v1_meta['limit_role'] ) && count( $v1_meta['limit_role'] ) ) {
				$config['role_limitation'] = true;
				$config['limit_role']      = (array) $v1_meta['limit_role'];
			}

			$post_ids = [];
			if ( 'all' === $v1_meta['which_page'] ) {
				if ( is_array( $v1_meta['exclude_posts'] ) && count( $v1_meta['exclude_posts'] ) ) {
					$config['postlist']['singular:post_type:page:all'] = 'use_on';

					foreach ( $v1_meta['exclude_posts'] as $post_id ) {
						$post_ids[ $post_id ] = 'exclude_from';
					}
				}
			} else {
				if ( is_array( $v1_meta['include_posts'] ) && count( $v1_meta['include_posts'] ) ) {
					foreach ( $v1_meta['include_posts'] as $post_id ) {
						$post_ids[ $post_id ] = 'use_on';
					}
				}
			}

			if ( $post_ids ) {
				$config['page_limitation'] = true;

				foreach ( $post_ids as $id => $mode ) {
					$post_type = get_post_type( $id );
					$key       = "singular:post_type:{$post_type}:id:{$id}";

					$config['postlist'][ $key ] = $mode;
				}
			}

			if ( 'none' === $v1_meta['theme_location'] ) {
				if ( count( $v1_meta['locations'] ) > 0 ) {
					$config['inline_location']   = 'css';
					$config['location_position'] = $v1_meta['locations'][0]->position;
					$config['location_selector'] = $v1_meta['locations'][0]->selector;
				}
			} else {
				$config['inline_location']   = $v1_meta['theme_location'];
				$config['location_position'] = $v1_meta['theme_location_pos'];
				$config['location_selector'] = '';
			}

			// Convert triggers.
			foreach ( $v1_meta['triggers'] as $v1_trigger ) {
				$trigger_action_map = [
					'on-click'  => 'click',
					'on-hover'  => 'hover',
					'on-timer'  => 'time',
					'on-scroll' => 'scroll',
					'on-exit'   => 'exit',
				];

				$v2_trigger = [
					'trigger_type'        => $trigger_action_map[ strtolower( $v1_trigger->type ) ],
					'trigger_scroll_type' => 'distance',
					'trigger_selector'    => $v1_trigger->selector,
					'trigger_delay'       => $v1_trigger->delay,
					// phpcs:ignore WordPress.NamingConventions.ValidVariableName.UsedPropertyNotSnakeCase
					'trigger_distance'    => $v1_trigger->offset . ( 'percent' === $v1_trigger->offsetType ? '%' : 'px' ),
					'trigger_label'       => ucfirst( $trigger_action_map[ $v1_trigger->type ] ) . ' Trigger',
					'condition'           => [],
				];

				if ( 'hover' === $config['layout_type'] ) {
					$config['triggers_hover'][] = $v2_trigger;
				} else {
					$config['triggers'][] = $v2_trigger;
				}
			}

			// Convert conditions.
			if ( empty( $v1_meta['disable_on_desktop'] ) ) {
				$config['condition']['cond_device'][] = 'desktop';
			}
			if ( empty( $v1_meta['disable_on_tablet'] ) ) {
				$config['condition']['cond_device'][] = 'tablet';
			}
			if ( empty( $v1_meta['disable_on_mobile'] ) ) {
				$config['condition']['cond_device'][] = 'phone';
			}

			$config['version'] = '2.2.0';
		}

		/*
		 *
		 * Update from 2.0.0 to 2.1.0
		 *
		 * Sample 2.0.0 Area (old):
		 * {"layout_type":"popup","is_static":"off","role_limitation":"on","limit_role":["administrator","editor"],"page_limitation":"on","postlist":{"singular:post_type:page:id:49082":"use_on"},"inline_location":"css","location_position":"before","location_selector":"","triggers":[{"label":"Scroll Trigger","trigger_type":"scroll","trigger_selector":"","trigger_delay":0,"trigger_distance":"15%","trigger_param":"","trigger_device":["desktop","tablet"]}],"triggers_hover":[],"close_trigger":null,"close_delay":0,"show_close":"on","not_modal":"on","not_blocking":"on","singleton":"off","keep_closed":"on","close_for":"3day","closed_flag":"close","min_width":"none","width":"auto","max_width":"none","min_height":"auto","height":"auto","max_height":null,"position_popup":"center_center","position_flyin":"bottom_right","position_hover":"bottom_center","z_index":null,"box_shadow":"off","with_loader":"off","dark_close":"off","alt_close":"on"}
		 *
		 * Changes for 2.1.0:
		 * - Convert the trigger_distance (bug in 1.x importer set it to "0.15" instead of "15").
		 * - Add new "overflow" config.
		 *
		 */
		if ( - 1 === version_compare( $config['version'], '2.1.0' ) ) {
			// Migrate the "0.25%" scroll distance to "25%".
			foreach ( [ 'triggers', 'triggers_hover' ] as $field ) {
				if ( empty( $config[ $field ] ) || ! is_array( $config[ $field ] ) ) {
					$config[ $field ] = [];
					continue;
				}

				foreach ( $config[ $field ] as $ind => $trigger ) {
					if ( 'scroll' === $trigger['trigger_type'] ) {
						$distance = divi_areas_parse_number( $trigger['trigger_distance'] );

						if (
							'%' === $distance['unit']
							&& $distance['value_float'] <= 1
							&& $distance['value_float'] > 0
						) {
							$config[ $field ][ $ind ]['trigger_distance'] = 100 * $distance['value_float'] . '%';
						}
					}
				}
			}

			$config['overflow'] = 'clip';

			$config['version'] = '2.1.0';
		}

		/*
		 *
		 * Update from 2.1.0 to 2.2.0
		 *
		 * Sample 2.1.0 Area (old):
		 * {"layout_type":"popup","is_static":"off","role_limitation":"on","limit_role":["administrator","editor"],"page_limitation":"on","postlist":{"singular:post_type:page:id:49082":"use_on"},"inline_location":"css","location_position":"before","location_selector":"","triggers":[{"label":"Scroll Trigger","trigger_type":"scroll","trigger_selector":"","trigger_delay":0,"trigger_distance":"15%","trigger_param":"","trigger_device":["desktop","tablet"]}],"triggers_hover":[],"close_trigger":null,"close_delay":0,"show_close":"on","not_modal":"on","not_blocking":"on","singleton":"off","keep_closed":"on","close_for":"3day","closed_flag":"close","min_width":"none","width":"auto","max_width":"none","min_height":"auto","height":"auto","max_height":null,"overflow":"clip","position_popup":"center_center","position_flyin":"bottom_right","position_hover":"bottom_center","z_index":null,"box_shadow":"off","with_loader":"off","dark_close":"off","alt_close":"on"}
		 *
		 * Changes for 2.2.0:
		 * - Add new default conditions.
		 * - Add new flag to each trigger: "cond_custom_area" or "cond_custom_hover".
		 * - Convert trigger devices to trigger conditions.
		 * - Convert "On Param" trigger to "On Delay" with condition.
		 *
		 */
		if ( - 1 === version_compare( $config['version'], '2.2.0' ) ) {
			$config['condition'] = [];

			$suffixes = [
				'triggers'       => '_area',
				'triggers_hover' => '_hover',
			];

			foreach ( [ 'triggers', 'triggers_hover' ] as $field ) {
				$suffix = $suffixes[ $field ];

				foreach ( $config[ $field ] as $ind => $trigger ) {
					// Convert device list to custom condition.
					$devices = $trigger['trigger_device'];

					if (
						! is_array( $devices )
						|| 3 === count( $devices )
					) {
						$trigger[ "cond_custom$suffix" ] = 'off';
					} else {
						$trigger[ "cond_custom$suffix" ] = 'on';
						$trigger[ "cond_device$suffix" ] = $devices;
					}

					// 2.2.0: Convert the trigger-type "URL param" to a condition.
					if ( 'param' === $trigger['trigger_type'] ) {
						$trigger['trigger_type']                = 'time';
						$trigger[ "cond_custom_$suffix" ]       = 'on';
						$trigger[ "cond_use_url_param$suffix" ] = 'present';
						$trigger[ "cond_url_param$suffix" ]     = $trigger['trigger_param'];
					}

					unset( $trigger['trigger_device'] );
					unset( $trigger['trigger_param'] );

					$config[ $field ][ $ind ] = $trigger;
				}
			}

			$config['version'] = '2.2.0';
		}

		/*
		 *
		 * Update from 2.2.0 to 2.3.0
		 *
		 * Sample 2.2.0 Area (old):
		 * {"layout_type":"popup","is_static":"off","role_limitation":"on","limit_role":["administrator","editor"],"page_limitation":"on","postlist":{"singular:post_type:page:id:49082":"use_on"},"inline_location":"css","location_position":"before","location_selector":"","triggers":[{"trigger_type":"scroll","trigger_scroll_type":"distance","trigger_selector":"","trigger_delay":0,"trigger_distance":"15%","trigger_label":"On Scroll Trigger","cond_custom_area":"on","cond_device_area":["desktop","tablet"],"cond_use_url_param_area":"off","cond_use_referrer_area":"off","cond_url_param_area":"","cond_referrer_area":[],"cond_custom_referrer_area":"","cond_schedule_area":"weekly","cond_schedule_dates_area":[],"cond_schedule_weekly_area":[{"weekly_days_area":["Mo","Tu"],"weekly_start_area":"01:00:0","weekly_end_area":"23:59:59","weekly_label_area":"Mo, Tu"},{"weekly_days_area":["Fr"],"weekly_start_area":"10:00:0","weekly_end_area":"14:00:0","weekly_label_area":"Fr"}]}],"triggers_hover":[],"close_trigger":null,"close_delay":0,"cond_device":["desktop","phone"],"cond_use_url_param":"off","cond_url_param":"","cond_use_referrer":"present","cond_referrer":["none","search"],"cond_custom_referrer":"*elegantthemes.com\/*\n*divimode.com\/*","cond_schedule":"not_date","cond_schedule_dates":[{"schedule_range":"2020-11-15 - 2020-11-21","schedule_start":"00:00:00","schedule_end":"23:59:59","schedule_label":"2020-11-15 - 2020-11-21"},{"schedule_range":"2020-11-29 - 2020-11-30","schedule_start":"00:00:00","schedule_end":"23:59:59","schedule_label":"2020-11-29 - 2020-11-30"}],"cond_schedule_weekly":[],"show_close":"on","not_modal":"on","not_blocking":"on","singleton":"off","keep_closed":"on","close_for":"3day","closed_flag":"close","min_width":"none","width":"auto","max_width":"none","min_height":"auto","height":"auto","max_height":"none","overflow":"clip","position_popup":"center_center","position_flyin":"bottom_right","position_hover":"bottom_center","z_index":"auto","box_shadow":"off","with_loader":"off","dark_close":"off","alt_close":"on"}
		 *
		 * Changes for 2.3.0:
		 * - Add new "size" flag.
		 * - Add new "push_content" flag.
		 * - Convert the "position" flag (from "position_<type>").
		 *
		 */
		if ( - 1 === version_compare( $config['version'], '2.3.0' ) ) {
			$config['size']         = 'auto';
			$config['push_content'] = false;

			if ( isset( $config[ "position_{$config['layout_type']}" ] ) ) {
				$config['position'] = $config[ "position_{$config['layout_type']}" ];
			} else {
				$config['position'] = '';
			}

			unset( $config['position_popup'] );
			unset( $config['position_flyin'] );
			unset( $config['position_hover'] );

			$config['version'] = '2.3.0';
		}

		// Finally, update the Area version.
		$config['version'] = DIVI_AREAS_VERSION;

		/**
		 * Fires after the area configuration was updated to the latest version.
		 *
		 * @since 2.3.0
		 *
		 * @param array $config  The updated config array.
		 * @param int   $area_id The Area ID.
		 */
		return apply_filters(
			'divi_areas_updated_config',
			$config,
			$area_id
		);
	}




	/*
	*
	*
	* -------------------------------------------------------------------------
	* -------------------------- INDIVIDUAL UPDATES ---------------------------
	* -------------------------------------------------------------------------
	*
	*
	*/


	/**
	 * Updates the DB from 1.x to version 2.0.0
	 *
	 * @since        2.0.0
	 * @noinspection PhpUnused
	 */
	protected function update_to_2_0_0() {
		// Migrate the options from 1.x to 2.0.0.
		$old_config = get_option( 'divi_areas' );
		$da_setting = get_option( '_da_setting' );
		$da_license = get_option( '_da_setting_license' );

		if ( ! $da_setting && is_array( $old_config ) ) {
			$da_setting = [
				'start_in_preview' => isset( $old_config['start_in_preview'] ) ? $old_config['start_in_preview'] : '',
				'init_divi_editor' => isset( $old_config['init_divi_editor'] ) ? $old_config['init_divi_editor'] : '',
				'debug_mode'       => isset( $old_config['debug_mode'] ) ? $old_config['debug_mode'] : '',
				'script_debug'     => isset( $old_config['script_debug'] ) ? $old_config['script_debug'] : '',
			];

			// Options are only used in Divi Area editor, no autoload.
			update_option( '_da_setting', $da_setting, false );

			$this->need_page_reload();
		}

		if ( ! $da_license && is_array( $old_config ) ) {
			$da_license = [
				'license_validated' => isset( $old_config['license_validated'] ) ? $old_config['license_validated'] : '',
				'license_status'    => isset( $old_config['license_status'] ) ? $old_config['license_status'] : '',
				'license_key'       => isset( $old_config['license_key'] ) ? $old_config['license_key'] : '',
			];

			// Options are only used in wp-admin dashboard, no autoload.
			update_option( '_da_setting_license', $da_license, false );

			$this->need_page_reload();
		}

		$this->update_all_areas();
	}

	/**
	 * Updates the DB from 2.0.0 to version 2.0.1
	 *
	 * Important: Add the new version to the "update_path" variable at the top!
	 *
	 * @since        2.0.1
	 * @noinspection PhpUnused
	 */
	protected function update_to_2_0_1() {
		$this->update_all_areas();
	}

	/**
	 * Updates the DB from 2.1.x to version 2.2.0
	 * Important: Add the new version to the "update_path" variable at the top!
	 *
	 * @since        2.2.0
	 * @noinspection PhpUnused
	 */
	protected function update_to_2_2_0() {
		try {
			// Start the Layout-Library cron task, if it did not start yet.
			$this->module( 'layouts' )->setup_schedule();
			// phpcs:ignore Generic.CodeAnalysis.EmptyStatement.DetectedCatch
		} catch ( Exception $exception ) {
			// Layout library was not found? That's bad, but don't interrupt the update process!
		}

		$this->update_all_areas();
	}

	/**
	 * Updates the DB from 2.2.x to version 2.3.0
	 * Important: Add the new version to the "update_path" variable at the top!
	 *
	 * @since        2.3.0
	 * @noinspection PhpUnused
	 */
	protected function update_to_2_3_0() {
		$this->update_all_areas();
	}
}
