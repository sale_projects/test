<?php
/**
 * Integrates a template library of pre-built Area layouts.
 *
 * @package Divi_Areas_Pro
 */

/**
 * Divi Library pre-built layouts.
 */
class DAP_Layouts extends DAP_Component {
	/**
	 * Called during the "plugins_loaded" action. Hooks up the admin menu, etc.
	 *
	 * @since  1.0.0
	 * @return void
	 */
	public function setup() {
		add_action( 'init', [ $this, 'register_nonces' ] );
		add_action( 'init', [ $this, 'prepare_post_type' ] );
		add_action( 'divimode_ajax_reset_layouts', [ $this, 'process_sync_templates' ] );
		add_action( 'divi_areas_sync_layouts', [ $this, 'process_sync_templates' ] );
		add_action( 'divi_areas_layouts_process', [ $this, 'process_queue' ] );

		add_filter(
			'et_builder_library_modal_custom_tabs',
			[ $this, 'register_library_tab' ],
			10,
			2
		);

		add_filter(
			'et_builder_library_custom_layouts',
			[ $this, 'generate_library_data' ],
			10,
			2
		);

		add_action(
			'divimode_saved_options_divi-areas_setting',
			[ $this, 'check_layouts_flag' ],
			100,
			2
		);

		add_action( 'divimode_load_settings_divi-areas', [ $this, 'setup_schedule' ] );

		// Process the Divi "Get Layout" request before it's handled by Divi.
		add_action(
			'wp_ajax_et_builder_library_get_layout',
			[ $this, 'wp_ajax_et_builder_library_get_layout' ],
			1
		);
	}

	/**
	 * Register nonces for custom layout library functionality.
	 *
	 * @since 2.1.0
	 * @return void
	 */
	public function register_nonces() {
		self::settings_library()->add_nonce( 'reset_layouts' );
	}

	/**
	 * Fetch the latest Layout templates from the divimode.com Layouts Library.
	 *
	 * This function is called via an scheduled event that runs once per day, OR when
	 * the user resets the layouts library.
	 *
	 * @since 2.1.0
	 * @return void
	 */
	public function process_sync_templates() {
		if ( 'on' === self::get_option( 'enable_layouts' ) ) {
			// Force full re-check with remote server.
			divi_areas_remove_data( 'data.json' );

			$this->clear_queue();
			$this->add_queue( 'sync' );
			$this->add_queue( 'import' );
			$this->process_queue();
		}
	}

	/**
	 * Action handler that is called after the Divi Areas settings were updated.
	 *
	 * We check, if the "enable_layouts" flag has changed. If it changed, we either
	 * remove all layouts from the DB or import them to the DB.
	 *
	 * @since 2.1.0
	 * @param array $data     The new plugin settings.
	 * @param array $old_data The previous new plugin settings.
	 * @return void
	 */
	public function check_layouts_flag( $data, $old_data ) {
		if ( ! $old_data || ! isset( $old_data['enable_layouts'] ) ) {
			$changed = true;
		} else {
			$changed = $old_data['enable_layouts'] !== $data['enable_layouts'];
		}

		if ( $changed ) {
			if ( 'on' === $data['enable_layouts'] ) {
				$this->add_queue( 'import' );
			} else {
				$this->clear_queue();
				$this->add_queue( 'delete' );
			}

			$this->process_queue();
		}
	}

	/**
	 * Register the custom CPT and taxonomies for the layout library.
	 *
	 * @since 2.1.0
	 * @param bool $ignore_settings When true, the post type is prepared independent
	 *                              of the current `enable_layouts` setting.
	 * @return void
	 */
	public function prepare_post_type( $ignore_settings = false ) {
		if ( ! $ignore_settings && 'on' !== self::get_option( 'enable_layouts' ) ) {
			return;
		}

		$labels = [
			'name'                       => _x( 'Layout Categories', 'Taxonomy General Name', 'divi_areas' ),
			'singular_name'              => _x( 'Layout Category', 'Taxonomy Singular Name', 'divi_areas' ),
			'menu_name'                  => __( 'Category', 'divi_areas' ),
			'all_items'                  => __( 'All Categories', 'divi_areas' ),
			'parent_item'                => __( 'Parent Category', 'divi_areas' ),
			'parent_item_colon'          => __( 'Parent Category:', 'divi_areas' ),
			'new_item_name'              => __( 'New Category Name', 'divi_areas' ),
			'add_new_item'               => __( 'Add New Category', 'divi_areas' ),
			'edit_item'                  => __( 'Edit Category', 'divi_areas' ),
			'update_item'                => __( 'Update Category', 'divi_areas' ),
			'view_item'                  => __( 'View Category', 'divi_areas' ),
			'separate_items_with_commas' => __( 'Separate categories with commas', 'divi_areas' ),
			'add_or_remove_items'        => __( 'Add or remove categories', 'divi_areas' ),
			'choose_from_most_used'      => __( 'Choose from the most used', 'divi_areas' ),
			'popular_items'              => __( 'Popular Categories', 'divi_areas' ),
			'search_items'               => __( 'Search Categories', 'divi_areas' ),
			'not_found'                  => __( 'Not Found', 'divi_areas' ),
			'no_terms'                   => __( 'No Categories', 'divi_areas' ),
			'items_list'                 => __( 'Categories list', 'divi_areas' ),
			'items_list_navigation'      => __( 'Categories list navigation', 'divi_areas' ),
		];
		$args   = [
			'labels'            => $labels,
			'hierarchical'      => false,
			'public'            => false,
			'show_ui'           => false,
			'show_admin_column' => false,
			'show_in_nav_menus' => false,
			'show_tagcloud'     => false,
			'rewrite'           => false,
			'show_in_rest'      => false,
		];

		/**
		 * Filter the taxonomy args before registering the layout category taxonomy.
		 *
		 * @since 2.1.0
		 * @param array  $args The taxonomy args.
		 */
		$args = apply_filters(
			'divi_areas_register_taxonomy_layout_category',
			$args
		);

		register_taxonomy( DIVI_AREAS_LAYOUT_TAX_CATEGORY, [ DIVI_AREAS_LAYOUT_CPT ], $args );

		$labels = [
			'name'                  => _x( 'Divi Areas Layouts', 'Post Type General Name', 'divi_areas' ),
			'singular_name'         => _x( 'Divi Areas Layout', 'Post Type Singular Name', 'divi_areas' ),
			'menu_name'             => __( 'Divi Areas Layout', 'divi_areas' ),
			'name_admin_bar'        => __( 'Divi Areas Layout', 'divi_areas' ),
			'archives'              => __( 'Layout Archives', 'divi_areas' ),
			'attributes'            => __( 'Layout Attributes', 'divi_areas' ),
			'parent_item_colon'     => __( 'Parent Item:', 'divi_areas' ),
			'all_items'             => __( 'All Layouts', 'divi_areas' ),
			'add_new_item'          => __( 'Add New Layout', 'divi_areas' ),
			'add_new'               => __( 'Add New', 'divi_areas' ),
			'new_item'              => __( 'New Layout', 'divi_areas' ),
			'edit_item'             => __( 'Edit Layout', 'divi_areas' ),
			'update_item'           => __( 'Update Layout', 'divi_areas' ),
			'view_item'             => __( 'View Layout', 'divi_areas' ),
			'view_items'            => __( 'View Layouts', 'divi_areas' ),
			'search_items'          => __( 'Search Layout', 'divi_areas' ),
			'not_found'             => __( 'Not found', 'divi_areas' ),
			'not_found_in_trash'    => __( 'Not found in Trash', 'divi_areas' ),
			'featured_image'        => __( 'Featured Image', 'divi_areas' ),
			'set_featured_image'    => __( 'Set featured image', 'divi_areas' ),
			'remove_featured_image' => __( 'Remove featured image', 'divi_areas' ),
			'use_featured_image'    => __( 'Use as featured image', 'divi_areas' ),
			'insert_into_item'      => __( 'Insert into Layout', 'divi_areas' ),
			'uploaded_to_this_item' => __( 'Uploaded to this Layout', 'divi_areas' ),
			'items_list'            => __( 'Layouts list', 'divi_areas' ),
			'items_list_navigation' => __( 'Layouts list navigation', 'divi_areas' ),
			'filter_items_list'     => __( 'Filter Layouts list', 'divi_areas' ),
		];

		$args = [
			'label'               => __( 'Divi Areas Layout', 'divi_areas' ),
			'description'         => __( 'Pre-Built Layouts for Divi Areas Pro', 'divi_areas' ),
			'labels'              => $labels,
			'supports'            => [ 'title', 'editor' ],
			'taxonomies'          => [ DIVI_AREAS_LAYOUT_TAX_CATEGORY ],
			'hierarchical'        => false,
			'public'              => false,
			'show_ui'             => false,
			'show_in_menu'        => false,
			'menu_position'       => 5,
			'show_in_admin_bar'   => false,
			'show_in_nav_menus'   => false,
			'can_export'          => false,
			'has_archive'         => false,
			'exclude_from_search' => true,
			'publicly_queryable'  => false,
			'rewrite'             => false,
			'capability_type'     => 'page',
			'show_in_rest'        => false,
		];

		/**
		 * Filter the post type args before registering the layout post type.
		 *
		 * @since 2.1.0
		 * @param array  $args The post-type args.
		 */
		$args = apply_filters(
			'divi_areas_register_post_type_layout',
			$args
		);

		register_post_type( DIVI_AREAS_LAYOUT_CPT, $args );
	}

	/**
	 * Clears the background-tasks queue and stops the queue-processor.
	 *
	 * @since 2.1.0
	 * @return void
	 */
	protected function clear_queue() {
		$scheduled_time = wp_next_scheduled( 'divi_areas_layouts_process' );
		if ( $scheduled_time ) {
			wp_unschedule_event( $scheduled_time, 'divi_areas_layouts_process' );
		}

		delete_option( 'divi_areas_layouts_queue' );
	}

	/**
	 * Adds a specific task to the queue.
	 *
	 * @since 2.1.0
	 * @param string $type Defines the task to process (sync|import|delete).
	 * @return array The final queue, which was saved to the DB.
	 */
	protected function add_queue( $type ) {
		$queue = get_option( 'divi_areas_layouts_queue' );
		if ( ! is_array( $queue ) ) {
			$queue = [];
		}

		// The cron task might be called before the CPT was registered.
		if ( ! taxonomy_exists( DIVI_AREAS_LAYOUT_TAX_CATEGORY ) ) {
			$this->prepare_post_type( true );
		}

		if ( 'sync' === $type ) {
			$queue[] = [
				'method' => 'process_sync_layout_index',
			];
		} elseif ( 'import' === $type ) {
			if ( ! divi_areas_has_data( 'data.json' ) ) {
				$sync_queued = false;

				foreach ( $queue as $task ) {
					if ( 'process_sync_layout_index' === $task['method'] ) {
						$sync_queued = true;
					}
				}

				if ( ! $sync_queued ) {
					$queue = $this->add_queue( 'sync' );
				}
			}

			$queue[] = [
				'method' => 'process_import_layout_templates',
			];
		} elseif ( 'delete' === $type ) {
			$queue[] = [
				'method' => 'process_delete_layouts',
			];
		}

		update_option( 'divi_areas_layouts_queue', $queue );
		return $queue;
	}

	/**
	 * Asynchronously processes the first task in the queue and schedules the next
	 * process action, if there are pending tasks. When no more tasks are present, the
	 * cron-job will automatically end.
	 *
	 * @since 2.1.0
	 * @return void
	 */
	public function process_queue() {
		/*
		 * Re-Schedule this task. This event is removed at the end of this function
		 * when the queue is empty.
		 */
		wp_schedule_single_event( time(), 'divi_areas_layouts_process' );

		if ( ! defined( 'DOING_CRON' ) || ! DOING_CRON ) {
			spawn_cron();
			return;
		}

		$queue = (array) get_option( 'divi_areas_layouts_queue' );

		// The cron task might be called before the CPT was registered.
		if ( ! taxonomy_exists( DIVI_AREAS_LAYOUT_TAX_CATEGORY ) ) {
			$this->prepare_post_type( true );
		}

		/*
		 * Start to process the queue, until it's either empty or a timeout interrups
		 * us. In case of a timeout, the
		 */
		while ( $queue ) {
			// Shift the first task off the queue and process it.
			$task = array_shift( $queue );

			if ( ! isset( $task['method'] ) ) {
				continue;
			}

			$method = [ $this, $task['method'] ];

			if ( isset( $task['param'] ) ) {
				$method( $queue, $task['param'] );
			} else {
				$method( $queue );
			}

			/*
			 * Update the queue in the DB, so we can directly continue in the next
			 * request in case we hit a the php max execution timeout.
			 */
			update_option( 'divi_areas_layouts_queue', $queue );
		}

		// When the queue is empty, stop the cron-task marathon.
		if ( ! $queue ) {
			$this->clear_queue();
		}
	}

	/**
	 * Returns true while the Layout-Sync is in progress.
	 *
	 * @since 2.1.0
	 * @return bool
	 */
	public static function sync_busy() {
		$scheduled_time = wp_next_scheduled( 'divi_areas_layouts_process' );
		if ( $scheduled_time ) {
			return true;
		}

		$queue = get_option( 'divi_areas_layouts_queue' );
		if ( $queue ) {
			return true;
		}

		return false;
	}

	/**
	 * Retches a list of all remote layout files from the divimode.com server and adds
	 * a download-task for each file to the beginning of the queue.
	 *
	 * @since 2.1.0
	 * @param array $queue The currently processed task queue.
	 * @return void
	 */
	protected function process_sync_layout_index( &$queue ) {
		// Download the data.json file.
		$index_url = 'https://divimode.com/wp-content/uploads/area-layouts/data.json';
		$this->process_replace_data_file( $queue, $index_url );

		// Read all lines of the downloaded data.json file.
		$templates = divi_areas_read_data( 'data.json' );

		// Enqueue all items from that file for download (at the start of the queue).
		if ( is_array( $templates ) ) {
			foreach ( $templates as $template_url => $file_hash ) {
				$file_name     = basename( $template_url );
				$template_path = divi_areas_get_data_path( $file_name );

				// Do not re-download if the local template exists and has the
				// current hash value.
				if (
					divi_areas_has_data( $file_name ) &&
					true === verify_file_md5( $template_path, $file_hash )
				) {
					continue;
				}

				array_unshift(
					$queue,
					[
						'method' => 'process_replace_data_file',
						'param'  => $template_url,
					]
				);
			}
		}
	}

	/**
	 * Downloads the specified remote file to the wp-uploads folder. Existing files
	 * will be overwritten. The filename of the local file is identical with the
	 * remote file.
	 *
	 * @since 2.1.0
	 * @param array  $queue     The currently processed task queue.
	 * @param string $url       The absolute URL to the remote file.
	 * @param string $file_name Optional. Name of the local target file. Uses the
	 *                          remote file name when omitted.
	 * @return void
	 */
	protected function process_replace_data_file( &$queue, $url, $file_name = false ) {
		if ( ! $file_name ) {
			$file_name = basename( $url );
		}

		$request = wp_remote_get( $url );

		if ( is_array( $request ) && ! is_wp_error( $request ) && isset( $request['body'] ) ) {
			$serialized = $request['body'];

			try {
				$json = json_decode( $serialized, true );

				// Enqueue media files.
				if ( $json && is_array( $json ) && ! empty( $json['images'] ) ) {
					foreach ( $json['images'] as $url => $file_hash ) {
						$media_name = basename( $url );
						$media_path = divi_areas_get_data_path( $media_name );

						// Do not re-download if the local template exists and has
						// the current hash value.
						if (
							divi_areas_has_data( $media_name ) &&
							true === verify_file_md5( $media_path, $file_hash )
						) {
							continue;
						}

						array_unshift(
							$queue,
							[
								'method' => 'process_replace_data_file',
								'param'  => $url,
							]
						);
					}
				}
			} catch ( Exception $ex ) {
				// Data could not be decoded, try to save the original string instead.
				$json = $serialized;
			}

			divi_areas_write_data( $file_name, $json );
		}
	}

	/**
	 * Find all template files and queue each file for import into the database (at
	 * the beginning of the queue).
	 *
	 * @since 2.1.0
	 * @param array $queue The currently processed task queue.
	 * @return void
	 */
	protected function process_import_layout_templates( &$queue ) {
		$files = divi_areas_list_data();

		foreach ( $files as $filename ) {
			if ( 'data.json' === $filename ) {
				continue;
			}

			array_unshift(
				$queue,
				[
					'method' => 'process_import_layout_file',
					'param'  => $filename,
				]
			);
		}
	}

	/**
	 * Import a single layout template into the database.
	 *
	 * @since 2.1.0
	 * @param array  $queue    The currently processed task queue.
	 * @param string $filename The local data file to import.
	 * @return void
	 */
	protected function process_import_layout_file( &$queue, $filename ) {
		$data = divi_areas_read_data( $filename );
		$this->import_layout( $data );
	}

	/**
	 * Removes all traces the layout templates from the WP database and from the
	 * uploads folder.
	 *
	 * @since 2.1.0
	 * @param array $queue    The currently processed task queue.
	 * @return void
	 */
	protected function process_delete_layouts( &$queue ) {
		// First delete all layout categories.
		$all_categories = get_terms(
			[
				'taxonomy'   => DIVI_AREAS_LAYOUT_TAX_CATEGORY,
				'hide_empty' => false,
			]
		);

		if ( ! is_wp_error( $all_categories ) ) {
			foreach ( $all_categories as $term ) {
				wp_delete_term( $term->term_id, DIVI_AREAS_LAYOUT_TAX_CATEGORY );
			}
		}

		// Second, delete all layout posts with a layout_id.
		$all_layouts = get_posts(
			[
				// phpcs:ignore WordPress.DB.SlowDBQuery.slow_db_query_meta_key
				'meta_key'    => 'layout_id',
				'post_type'   => DIVI_AREAS_LAYOUT_CPT,
				'post_status' => 'any',
				'numberposts' => -1,
			]
		);

		foreach ( $all_layouts as $post ) {
			wp_delete_post( $post->ID, true );
		}

		// Third, remove all layout sources from the uploads folder.
		$files = divi_areas_list_data();
		foreach ( $files as $filename ) {
			divi_areas_remove_data( $filename );
		}
	}

	/**
	 * Schedule the remote sync to load the latest Layout Templates from the
	 * divimode.com website.
	 *
	 * @since 2.1.0
	 */
	public function setup_schedule() {
		if ( ! wp_next_scheduled( 'divi_areas_sync_layouts' ) ) {
			wp_schedule_event( time(), 'daily', 'divi_areas_sync_layouts' );
			spawn_cron();
		}
	}

	/**
	 * Creates (or updates) a single layout template.
	 *
	 * This method can create a new post with post-meta values, and also might create
	 * a new term which is linked to the post.
	 *
	 * @since 2.1.0
	 * @param array $data {
	 *     The layout data.
	 *
	 *     @type int      $id         Global layout ID, defined by divimode.com.
	 *     @type string   $title      Displayed layout name.
	 *     @type string   $content    Post-content (i.e. Divi shortcodes).
	 *     @type string[] $category   Category names to link to the layout.
	 *     @type array    $config     Divi Area configuration details.
	 *     @type string   $thumbnail  URL to the layout thumbnail.
	 * }
	 * @return int The post-ID of the layout item, or 0 on failure.
	 */
	public function import_layout( $data ) {
		$result = 0;

		if (
			! $data
			|| ! is_array( $data )
			|| ! isset( $data['title'] )
			|| ! isset( $data['content'] )
			|| ! isset( $data['category'] )
			|| ! isset( $data['config'] )
		) {
			return 0;
		}

		// Get the term_ids for all layout categories (insert missing terms).
		$term_ids = $this->import_layout_categories( $data['category'] );

		// See, if this layout exists and should be updated.
		$post_id = 0;
		if ( ! empty( $data['id'] ) ) {
			$post_id = $this->get_post_id_by_layout_id( $data['id'] );
		}

		$post_data = [
			'post_title'   => $data['title'],
			'post_content' => $data['content'],
			'post_type'    => DIVI_AREAS_LAYOUT_CPT,
			'post_status'  => 'draft',
			'tax_input'    => [
				DIVI_AREAS_LAYOUT_TAX_CATEGORY => $term_ids,
			],
		];

		// Update the existing layout?
		if ( $post_id ) {
			$post_data['ID'] = $post_id;
		}

		/*
		 * As we run this task in the background without a user-session, WP always
		 * tries to sanitize the post_content via wp_kses_post(), which strips styles
		 * from eventual code modules.
		 *
		 * That's why we disable kses-checks globally and re-enable them after the
		 * post was inserted/updated.
		 *
		 * @see https://wordpress.stackexchange.com/a/255867/31140
		 */
		kses_remove_filters();

		$result = wp_insert_post( $post_data );

		if ( ! current_user_can( 'unfiltered_html' ) ) {
			kses_init_filters();
		}

		// Add the Divi Area configuration.
		if ( $result ) {
			// Import the area configuration.
			if ( $data['config'] && is_array( $data['config'] ) ) {
				update_post_meta( $result, '_da_area', $data['config'] );
			}

			// Import the thumbnail URL.
			if ( ! empty( $data['thumbnail'] ) ) {
				update_post_meta( $result, 'thumbnail_url', $data['thumbnail'] );
			}

			// Associate the layout ID with the post.
			if ( ! empty( $data['id'] ) ) {
				update_post_meta( $result, 'layout_id', $data['id'] );
			}
		}

		return $result;
	}

	/**
	 * Takes an array of category names and returns an array of term_ids that match
	 * those categories (the display name is matched). When a category does not exist
	 * in the DB, it will be created by this function.
	 *
	 * @since 2.1.0
	 * @param string[] $categories Display names of layout categories.
	 * @return int[] List of term_ids for all specified category names.
	 */
	public function import_layout_categories( $categories = [] ) {
		$result = [];

		if ( ! is_array( $categories ) ) {
			return $result;
		}

		foreach ( $categories as $term_name ) {
			$term_name = trim( $term_name );
			if ( ! $term_name ) {
				continue;
			}

			$term = get_term_by( 'name', $term_name, DIVI_AREAS_LAYOUT_TAX_CATEGORY );

			if ( $term ) {
				// The term exists in the DB, so we can directly return its term_id.
				$result[] = $term->term_id;
			} else {
				// The term does not exist. Try to insert it and use the new term_id.
				$term = wp_insert_term( $term_name, DIVI_AREAS_LAYOUT_TAX_CATEGORY );
				if ( $term ) {
					$result[] = $term['term_id'];
				}
			}
		}

		return $result;
	}

	/**
	 * Imports the given image data into the media library. If the image already
	 * exists in the media library, it will not be imported again.
	 *
	 * @since 2.1.0
	 * @param array $data {
	 *     Image data.
	 *
	 *     @type string $hash    The image hash ID.
	 *     @type string $name    The file name.
	 *     @type string $type    The file extension, e.g. "jpg".
	 *     @type string $mime    The mime type, e.g. "image/jpeg".
	 *     @type string $content The base64 encoded image.
	 * }
	 * @param int   $post_id Optional. The parent post ID.
	 * @return string The URL to the image file.
	 */
	public function import_image_data( $data, $post_id = 0 ) {
		$result = '';

		if (
			! is_array( $data )
			|| empty( $data['id'] )
			|| empty( $data['name'] )
			|| empty( $data['content'] )
		) {
			return $result;
		}

		$basename = sanitize_file_name( wp_basename( $data['name'] ) );

		// Test, if the image already exists in the media library.
		$media_id = $this->get_media_id_by_file( $basename, 0 );

		if ( ! $media_id ) {
			$filesystem = divi_areas_get_fs();
			$temp_file  = wp_tempnam();

			// Valid use of base64 decode: We load image data from a JSON file.
			// phpcs:ignore: WordPress.PHP.DiscouragedPHPFunctions.obfuscation_base64_decode
			$filesystem->put_contents( $temp_file, base64_decode( $data['content'] ) );

			$file     = [
				'name'     => $basename,
				'tmp_name' => $temp_file,
			];
			$media_id = media_handle_sideload( $file, $post_id );

			if ( is_wp_error( $media_id ) ) {
				// Make sure the temporary file is removed if media_handle_sideload didn't take care of it.
				$filesystem->delete( $temp_file );
				$media_id = 0;
			}
		}

		// Set the replacement as an id if the original image was set as an id (for gallery).
		if ( $media_id ) {
			$result = wp_get_attachment_url( $media_id );
		}

		return $result;
	}

	/**
	 * Tests, if the given attachment file exists in the media library and either
	 * returns the attachment ID or false.
	 *
	 * @since 2.1.0
	 * @param string $filename  The image file name.
	 * @param int    $parent_id The parent ID (attached to).
	 * @return int|false
	 */
	public function get_media_id_by_file( $filename, $parent_id = 0 ) {
		global $wpdb;

		// phpcs:ignore WordPress.DB.DirectDatabaseQuery.DirectQuery, WordPress.DB.DirectDatabaseQuery.NoCaching
		$res = $wpdb->get_var(
			$wpdb->prepare(
				"SELECT
					m.post_id
				FROM {$wpdb->postmeta} AS m
				INNER JOIN {$wpdb->posts} AS p
					ON p.ID = m.post_id
				WHERE
					m.meta_key = '_wp_attached_file'
					AND m.meta_value LIKE %s
					AND p.post_type = 'attachment'
					AND (%d = 0 OR p.post_parent = %d);
				",
				'%/' . $wpdb->esc_like( $filename ),
				(int) $parent_id,
				(int) $parent_id
			)
		);

		if ( $res ) {
			return (int) $res;
		}

		return false;
	}

	/**
	 * Returns the post-id of the local layout template based on the global layout-id.
	 *
	 * @since 2.1.0
	 * @param int $layout_id The global layout-id.
	 * @return int The local post-id, or 0 if the layout is not present in the DB.
	 */
	public function get_post_id_by_layout_id( $layout_id ) {
		$result = 0;

		/**
		 * Try to locate the post based on the layout ID. We ignore the slow-query
		 * warnings, because this method is only used in the admin area while
		 * importing new layouts. Data import by itself is a process where performance
		 * is secondary.
		 */
		$args = [
			'post_type'   => DIVI_AREAS_LAYOUT_CPT,
			'post_status' => 'any',
			'meta_key'    => 'layout_id', // phpcs:ignore WordPress.DB.SlowDBQuery.slow_db_query_meta_key
			'meta_value'  => (int) $layout_id, // phpcs:ignore WordPress.DB.SlowDBQuery.slow_db_query_meta_value
		];
		$res  = get_posts( $args );

		// When we got a valid result, we use the first post ID that we found.
		if ( $res && is_array( $res ) ) {
			$result = $res[0]->ID;
		}

		return $result;
	}

	/*
	 * ------------------------------------------------------------------------
	 * -                      Visual Builder Integration                      -
	 * ------------------------------------------------------------------------
	 */

	/**
	 * AJAX Callback: Gets a layout by ID.
	 *
	 * Handle Divis get-layout request to import images into the media library before
	 * importing a Divi Area Layout into the current post.
	 *
	 * @global $_POST['id']     The id of the desired layout.
	 * @global $_POST ['nonce'] Nonce: 'et_builder_library_get_layout'.
	 *
	 * @since 2.1.0
	 * @return void
	 */
	public function wp_ajax_et_builder_library_get_layout() {
		et_core_security_check( 'edit_posts', 'et_builder_library_get_layout', 'nonce' );

		// phpcs:ignore WordPress.Security.NonceVerification.Missing
		$id = isset( $_POST['id'] ) ? (int) $_POST['id'] : 0;

		if ( ! $id ) {
			return;
		}

		// This flag is injected by builder.js when editing a Divi Area post.
		// phpcs:ignore WordPress.Security.NonceVerification.Missing
		if ( empty( $_POST['post_type'] ) || DIVI_AREAS_CPT !== $_POST['post_type'] ) {
			return;
		}

		/**
		 * Action fires when we detect a valid request to load a template library
		 * object into a Divi Area post.
		 *
		 * @since 2.2.0
		 */
		do_action( 'divi_areas_pre_library_get_layout' );

		$post = get_post( $id );

		if ( DIVI_AREAS_LAYOUT_CPT !== $post->post_type ) {
			return;
		}

		if ( ! preg_match_all( '/%IMG:(.*?)%/', $post->post_content, $matches ) ) {
			return;
		}

		// Import all images into the media library.
		$hashes = array_unique( $matches[1] );
		foreach ( $hashes as $img_hash ) {
			$data    = divi_areas_read_data( 'image-' . $img_hash );
			$img_url = $this->import_image_data( $data, $post->ID );

			$post->post_content = str_replace(
				"%IMG:$img_hash%",
				$img_url,
				$post->post_content
			);
		}

		wp_update_post( $post );
	}

	/**
	 * Registers the Divi Area template library.
	 *
	 * @since 2.1.0
	 * @param array  $custom_tabs List of custom tabs (id + label).
	 * @param string $post_type   The current post type.
	 * @return array The modified tab details.
	 */
	public function register_library_tab( $custom_tabs, $post_type ) {
		// When creating a new post inside BFB, the post type is always set to 'post',
		// regardless of the actual post type that's created.
		if ( 'post' === $post_type ) {
			$referer    = wp_get_raw_referer();
			$parsed_url = [];
			$request    = [];

			if ( $referer ) {
				$parsed_url = wp_parse_url( $referer );
			}

			if ( is_array( $parsed_url ) && isset( $parsed_url['query'] ) ) {
				wp_parse_str( $parsed_url['query'], $request );
			}

			if ( DAP_Post_Type::is_new_divi_area( $request ) ) {
				$post_type = DIVI_AREAS_CPT;
			}
		}

		if ( DIVI_AREAS_CPT === $post_type && 'on' === self::get_option( 'enable_layouts' ) ) {
			$custom_tabs['divi_areas'] = 'Divi Areas';
		}

		return $custom_tabs;
	}

	/**
	 * Filters custom tabs layout data for the library modal. Custom tabs must be
	 * registered via the {@see 'et_builder_library_modal_custom_tabs'} filter.
	 *
	 * @see 'et_builder_library_saved_layouts' for structure of the
	 *      $custom_layouts_data array.
	 *
	 * @since 2.1.0
	 * @param array[] $custom_layouts_data Custom layouts data, organized by tab.
	 * @param array[] $saved_layouts_data  Saved layouts data.
	 */
	public function generate_library_data( $custom_layouts_data, $saved_layouts_data ) {
		if ( 'on' === self::get_option( 'enable_layouts' ) ) {
			$categories = $this->get_library_categories();
			$packs      = $this->get_library_packs();
			$layouts    = $this->get_library_layouts( $categories );
			$sorted     = $this->get_library_sorted( $categories, $packs );
			$options    = [
				'content' => [
					'title' => [
						// translators: Placeholder is the number of available templates.
						_x( '%d Areas', 'Layout Library', 'divi_areas' ),
						// translators: Placeholder is the number of available templates.
						_x( '%d Area', 'Layout Library', 'divi_areas' ),
					],
				],
				'sidebar' => [
					'title' => __( 'Find An Area', 'divi_areas' ),
				],
				'list'    => [
					'columns' => [],
				],
			];

			$custom_layouts_data['divi_areas'] = [
				'categories' => $categories,
				'packs'      => $packs,
				'layouts'    => $layouts,
				'options'    => $options,
				'sorted'     => $sorted,
			];
		}

		return $custom_layouts_data;
	}

	/**
	 * Generates a list of layout categories.
	 *
	 * @type object category_item {
	 *     The category object.
	 *
	 *     @type int    $id      The category ID.
	 *     @type string $slug    Category slug.
	 *     @type string $name    Category name.
	 *     @type int[]  $layouts List of layout IDs in that category.
	 * }
	 *
	 * @since 2.1.0
	 * @return array List of category_item.
	 */
	protected function get_library_categories() {
		$result         = [];
		$all_categories = get_terms(
			[
				'taxonomy'   => DIVI_AREAS_LAYOUT_TAX_CATEGORY,
				'hide_empty' => false,
			]
		);

		foreach ( $all_categories as $term ) {
			$result[ (string) $term->term_id ] = (object) [
				'id'      => $term->term_id,
				'slug'    => $term->slug,
				'name'    => trim( $term->name, ': ' ),
				'layouts' => [],
			];
		}

		return $result;
	}

	/**
	 * Generate a list of layout packs.
	 *
	 * Note: "Packs" seem to have no effect on our custom layouts; they can be used
	 * by elegant themes "Premade Layouts". That's why we return an empty array here.
	 *
	 * @type array pack_item {
	 *     The pack item.
	 *
	 *     @type int    $id           The pack ID.
	 *     @type string $slug         Pack slug.
	 *     @type string $name         Pack name.
	 *     @type string $date         Creation date, "Y-m-d H:i:s".
	 *     @type int[]  $layouts      List of layout IDs in that pack.
	 *     @type array  $categories   Always an empty array.
	 *     @type int[]  $category_ids List of category IDs in that pack.
	 *     @type string $screenshot   Screenshot URL.
	 *     @type string $description  Pack description.
	 *     @type string $thumbnail    Thumbnail URL.
	 * }
	 *
	 * @since 2.1.0
	 * @return array List of pack_items.
	 */
	protected function get_library_packs() {
		return [];
	}

	/**
	 * Generate a list of layouts.
	 *
	 * @type object layout_item {
	 *     The layout object.
	 *
	 *     @type int      $id              Post-ID
	 *     @type string[] $categories      Always empty array.
	 *     @type int[]    $category_ids    Category IDs.
	 *     @type string   $category_slug   Post-type (not a category!).
	 *     @type string   $type            Post-type label.
	 *     @type int      $date            Publish date.
	 *     @type string   $description     Layout description.
	 *     @type int      $index           Position inside pack (?).
	 *     @type bool     $is_global       Whether the layout is a global template
	 *                                     library item.
	 *     @type bool     $is_landing      True indicates, that this layout should be
	 *                                     used as initial layout of  pack.
	 *     @type string   $name            Post name.
	 *     @type string   $short_name      Post name.
	 *     @type string   $screenshot      URL to screenshot.
	 *     @type string   $slug            Post slug.
	 *     @type string   $icon            Always 'layout'.
	 *     @type string   $thumbnail       Thumbnail URL.
	 *     @type string   $thumbnail_small Thumbnail URL.
	 *     @type string   $url             Permalink of the post.
	 *     @type string   $status          Post status.
	 *     @type int      $pack_id         The parent pack.
	 * }
	 *
	 * @since 2.1.0
	 * @param array $categories The category_items list. This function will update
	 *                          the layout-references for relevant categories.
	 * @return array List of layout_items.
	 */
	protected function get_library_layouts( $categories ) {
		$result = [];

		$default_img = 'https://divimode.com/wp-content/uploads/area-layouts/default-image.jpg';
		$all_layouts = get_posts(
			[
				'post_type'   => DIVI_AREAS_LAYOUT_CPT,
				'numberposts' => -1,
				'post_status' => 'any',
			]
		);

		foreach ( $all_layouts as $post ) {
			$term_ids = wp_get_post_terms(
				$post->ID,
				DIVI_AREAS_LAYOUT_TAX_CATEGORY,
				[ 'fields' => 'ids' ]
			);

			$thumbnail_url = get_post_meta( $post->ID, 'thumbnail_url', true );
			if ( ! $thumbnail_url ) {
				$thumbnail_url = $default_img;
			}

			$result[] = (object) [
				'id'              => (int) $post->ID,
				'categories'      => [],
				'category_ids'    => $term_ids,
				'category_slug'   => $post->post_type,
				'type'            => __( 'Divi Area Layout', 'divi_areas' ),
				'date'            => $post->post_date,
				'description'     => $post->post_excerpt,
				'index'           => 0,
				'is_global'       => false,
				'is_landing'      => false,
				'name'            => $post->post_title,
				'short_name'      => $post->post_title,
				'screenshot'      => '',
				'slug'            => $post->post_name,
				'icon'            => 'layout',
				'thumbnail'       => $thumbnail_url,
				'thumbnail_small' => '',
				'url'             => '',
				'pack_id'         => 100,
				'status'          => $post->post_status,
			];

			foreach ( $term_ids as $term_id ) {
				if ( isset( $categories[ $term_id ] ) ) {
					$categories[ $term_id ]->layouts[] = $post->ID;
				}
			}
		}

		return $result;
	}

	/**
	 * Generates a list of sorted category/pack IDs.
	 *
	 * @since 2.1.0
	 * @param array $categories A list of category_items.
	 * @param array $packs      A list of pack_items.
	 * @return array  {
	 *     The sorted packs/categories.
	 *
	 *     @type int[] categories Sorted category IDs.
	 *     @type int[] packs      Sorted pack IDs.
	 * }
	 */
	protected function get_library_sorted( $categories, $packs ) {
		$result     = [];
		$categories = array_values( $categories );
		$packs      = array_values( $packs );

		foreach ( [ 'categories', 'packs' ] as $taxonomy ) {
			$result[ $taxonomy ] = [];

			foreach ( $$taxonomy as $term ) {
				if ( is_array( $term ) ) {
					$result[ $taxonomy ][] = $term['id'];
				} elseif ( is_object( $term ) ) {
					$result[ $taxonomy ][] = $term->id;
				}
			}
		}

		return $result;
	}
}
