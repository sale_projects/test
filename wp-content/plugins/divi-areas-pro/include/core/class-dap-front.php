<?php
/**
 * Injects the Divi Areas in the front end.
 *
 * @package Divi_Areas_Pro
 */

/**
 * The application entry class.
 */
class DAP_Front
	extends DAP_Component {

	/**
	 * Stores details about the current request and is used to determine, whether an
	 * Area is visible on the current page or not.
	 * Used in combination with self::$_postlist_settings.
	 *
	 * @since 2.0.0
	 * @var array
	 */
	private static $_request = null;

	/**
	 * Caches the flat template settings array generated by Divi.
	 * Used in combination with self::$_request.
	 *
	 * @see   et_theme_builder_get_flat_template_settings_options()
	 * @since 2.0.0
	 * @var array
	 */
	private static $_postlist_settings = null;

	/**
	 * List of all Areas that will be injected on the current page.
	 * Populated during the `wp` action. This array contains a list of Area IDs that
	 * should be injected into the current page - both, dynamic and static ones. This
	 * list ist processed during the page rendering and also at the end of the
	 * `wp_footer` action.
	 *
	 * @var array
	 */
	protected $include_areas = [];

	/**
	 * Counter for the currently rendered Divi sections (default page content only).
	 * Used to select the nth section as Inline Area position.
	 *
	 * @var int
	 */
	protected $section_num = 0;

	/**
	 * Whether the output buffering for Inline Areas in `replace_in_footer()` was
	 * already started.
	 *
	 * @since 2.0.0
	 * @var bool
	 */
	protected $inline_buffer = false;

	/**
	 * A list of placeholder strings and the corresponding Area-ID; also used by the
	 * `replace_in_footer()` method.
	 *
	 * @since 2.0.0
	 * @var array
	 */
	protected $inline_placeholders = [];

	/**
	 * A flag that indicates, whether the current page requires the JS API library.
	 *
	 * @since 2.2.0
	 * @var bool
	 */
	protected $load_api = false;

	/**
	 * Inject the Divi Areas logic into the front end of the website.
	 *
	 * @since  0.1.0
	 * @return void
	 */
	public function setup() {
		/**
		 * Assume, that we need the JS API on this page, if the request is not an
		 * Ajax/Cron request.
		 */
		if (
			( ! defined( 'DOING_AJAX' ) || ! DOING_AJAX )
			&& ( ! defined( 'DOING_CRON' ) || ! DOING_CRON )
		) {
			$this->load_api = true;
		}

		if ( is_admin() ) {
			// Ajax handler to check Area conditions.
			add_action( 'wp_ajax_dap_cav', [ $this, 'ajax_check_area_visibility' ] );
			add_action( 'wp_ajax_nopriv_dap_cav', [ $this, 'ajax_check_area_visibility' ] );

			return;
		}

		add_action( 'template_redirect', [ $this, 'find_areas_for_page' ] );
		add_filter( 'divi_areas_js_data', [ $this, 'apply_settings' ] );

		add_action(
			'wp_enqueue_scripts',
			[ $this->module( 'asset' ), 'enqueue_js_library' ]
		);
	}

	/**
	 * Checks, whether the current request displays Divi´s Visual Builder.
	 *
	 * @since  0.4.0
	 * @return bool
	 */
	public function is_editor() {
		static $is_editor_res = null;

		if ( null === $is_editor_res ) {
			$is_editor_res = false;

			// Is either the Divi theme or the plugin enabled?
			if ( function_exists( 'et_fb_is_enabled' ) ) {
				$is_editor_res = false;

				if ( et_fb_is_enabled() ) {
					$is_editor_res = true;
				} elseif (
					// phpcs:ignore WordPress.Security.NonceVerification.Recommended
					! empty( $_GET['et_fb'] ) &&
					current_user_can( 'edit_posts' )
				) {
					// This condition is true in the admin integration of the Divi Builder.
					$is_editor_res = et_pb_is_allowed( 'use_visual_builder' );
				}
			}
		}

		return $is_editor_res;
	}

	/**
	 * A singular Divi Area can only be viewed by an editor/admin who can also edit
	 * the Area. This is always a preview.
	 *
	 * @since 2.0.0
	 * @return bool True, when the current page displays a single Divi Area.
	 */
	public function is_preview() {
		return is_singular( DIVI_AREAS_CPT );
	}

	/**
	 * Determines whether the current request tries to load a missing page resource,
	 * such as a .js or .map file.
	 * When such a request is detected, the JS API is not initialized, so we do not
	 * return JS code for the resource. So far we know that this fixes issues where
	 * .svg files are used that do not exist: WP will return the 404 page result which
	 * is parsed by the parent document. During that process the JS API can spill into
	 * the parent document and interfere with the Visual Builder.
	 *
	 * @since 2.2.0
	 * @return bool True, when we suspect that the 404 request is accessing a missing
	 *              page resource.
	 */
	public function is_missing_file() {
		static $is_missing_file = null;

		if ( null === $is_missing_file ) {
			$is_missing_file = false;

			if ( is_404() ) {
				if ( isset( $_SERVER['HTTP_SEC_FETCH_DEST'] ) ) {
					/*
					 * When a Sec-Fetch-Dest header is present, we use that
					 * information to determine how this resource should be used. Only
					 * documents and embeds should load JS sources.
					 */
					$is_missing_file = ! in_array(
						$_SERVER['HTTP_SEC_FETCH_DEST'],
						[ 'document', 'embed', 'nested-document' ],
						true
					);
				} elseif ( ! empty( $_SERVER['REQUEST_URI'] ) && ! wp_get_raw_referer() ) {
					/*
					 * If no Sec-Fetch-Dest header is present, we evaluate the
					 * requested URI to determine, if it looks like a page resource.
					 * Of course, we only do this when no referer is present, as a
					 * referer always indicates a top-level document.
					 */
					$requested_url = esc_url_raw( wp_unslash( $_SERVER['REQUEST_URI'] ) );
					$questionmark  = strpos( $requested_url, '?' );

					if ( $questionmark > 1 ) {
						$requested_url = substr( $requested_url, 0, $questionmark );
					}

					/**
					 * If the requested URL starts with "/wp-content/", or if it ends
					 * with a dot followed by 2-4 letters (like .js, .map, .json,
					 * .svg) then we're pretty sure that the request tries to find a
					 * missing page resource.
					 */
					if ( preg_match( '!(^/wp-content/|\.[a-z]{2,4}$)!i', $requested_url ) ) {
						$is_missing_file = true;
					}
				}
			}
		}

		return $is_missing_file;
	}

	/**
	 * Returns true, when the current request should load the JS API.
	 *
	 * @since 2.2.0
	 * @return bool
	 */
	public function need_js_api() {
		return $this->load_api;
	}

	/**
	 * Finds all areas that need to be rendered on the current webpage and populates
	 * the $areas member of this object.
	 *
	 * @action template_redirect
	 * @since  0.1.0
	 * @return void
	 */
	public function find_areas_for_page() {
		// We do not inject any Divi Area into the Visual Builder.
		if (
			$this->need_js_api()
			&& ( $this->is_editor() || $this->is_preview() || $this->is_missing_file() )
		) {
			$this->load_api = false;

			return;
		}

		// Get a cached list of all Area IDs.
		$area_ids = $this->get_all_area_ids();

		/*
		 * Find reduce the list to Area IDs that are used on the current page and
		 * visible for the current user.
		 */
		foreach ( $area_ids as $area_id => $state ) {
			if ( '' === $state ) {
				if ( ! DAP_Area::is_area_visible_for_current_user( $area_id ) ) {
					$area_ids[ $area_id ] = 'hide_for_user';
					continue;
				}
				if ( ! DAP_Area::is_area_visible_on_current_page( $area_id ) ) {
					$area_ids[ $area_id ] = 'hide_on_page';
					continue;
				}

				/**
				 * Filters the initial state of the Area.
				 * When this filter is called, the Area is available for the
				 * current user and enabled for the current page/post.
				 *
				 * @since 2.3.0
				 *
				 * @param string $state   Either 'show' or 'hide'.
				 * @param int    $area_id The Area that is filtered.
				 */
				$area_ids[ $area_id ] = apply_filters(
					'divi_areas_active_area_state',
					'show',
					$area_id
				);
			}

			if ( 'show' === $area_ids[ $area_id ] ) {
				$this->include_areas[ $area_id ] = 'show';
			}
		}

		/**
		 * Allow modification of the dynamic areas that will be injected later.
		 *
		 * @since 0.1.0
		 * @var   array List of dynamic Areas, that will be displayed.
		 * @var   array List of all available areas (id => visibility).
		 */
		$this->include_areas = apply_filters(
			'divi_areas_find_areas_for_page',
			$this->include_areas,
			$area_ids
		);

		DAP_Component::logger()->debug( 'Area visibility', $area_ids );

		if ( $this->include_areas ) {
			// Add hooks to output the Area HTML/JS code at the appropriate positions.
			$this->prepare_area_injection_hooks();
		}
	}

	/**
	 * Hook up all visible Areas for display inside the page.
	 * Static Inline Areas are injected directly into the relevant place in the DOM
	 * tree and not positioned by JS (like other area types).
	 * Dynamic Inline Areas generate a "hint" in the HTML code that marks their
	 * intended position, while they are displayed via the JS API.
	 * Non-Inline Areas are injected into the page footer.
	 *
	 * @since  2.0.0
	 * @return void
	 */
	private function prepare_area_injection_hooks() {
		foreach ( $this->include_areas as $area_id => $state ) {
			if ( 'show' === $state ) {
				$area = DAP_Area::get_area( $area_id );

				if ( $area ) {
					if ( 'inline' === $area->get( 'layout_type' ) ) {
						$this->inject_area_to_theme( $area );
					} else {
						$this->inject_hook_footer( $area );
					}

					$this->include_areas[ $area_id ] = 'hooked';
				} else {
					$this->include_areas[ $area_id ] = 'failed';
				}
			}
		}
	}

	/**
	 * Injects the specified Area into the page footer.
	 *
	 * @since  2.0.0
	 *
	 * @param DAP_Area $area The Area to inject.
	 *
	 * @return void
	 */
	public function inject_hook_footer( DAP_Area $area ) {

		/**
		 * Divi enqueues footer styles at priority 20. That means, we have to render
		 * all Divi Areas before that, to ensure their styles are registered with
		 * Divi��s PageResource Manager.
		 *
		 * @see ET_Core_PageResource::__register_callbacks()
		 * However, animation data is output with priority 10. Because of that, we
		 * need to have our Divi Areas ready on priority 9!
		 * @see et_builder_get_modules_js_data() in framework.php
		 */
		add_action( 'wp_footer', [ $area, 'full_code' ], 9 );
	}

	/**
	 * Injects the specified static Inline Area into the page footer when it was not
	 * processed yet.
	 *
	 * @see    DAP_Front::inject_hook_footer() for more details.
	 * @since  2.0.0
	 *
	 * @param DAP_Area $area The Area to inject.
	 *
	 * @return void
	 */
	public function inject_hook_footer_static_inline_area( DAP_Area $area ) {
		add_action(
			'wp_footer',
			function () use ( $area ) {
				/**
				 * Output the Area code in the footer when the Area was not injected
				 * earlier. This happens, when the Inline Area uses some CSS selectors
				 * that are not recognized (or too complex) for the inline injection
				 * logic.
				 */
				if (
					'inline' === $area->get( 'layout_type' )
					&& true !== $area->get( 'injected' )
				) {
					$static_trigger = [
						'trigger_type'   => 'time',
						'trigger_delay'  => 'instant',
						'trigger_device' => [ 'desktop', 'tablet', 'phone' ],
					];

					$area->set( 'is_static', false );
					$area->set( 'trigger', $static_trigger );

					$area->full_code();
				}
			},
			9
		);
	}

	/**
	 * Hooks a single Inline Area into the page output.
	 *
	 * @since  0.4.0
	 *
	 * @param DAP_Area $area The area to inject.
	 *
	 * @return void
	 */
	private function inject_area_to_theme( DAP_Area $area ) {
		switch ( $area->get( 'inline_location' ) ) {
			// -- Location: Main Menu (Navigation):
			case 'navigation':
				$et_slide_header = (
					'slide' === et_get_option( 'header_style' )
					|| 'fullscreen' === et_get_option( 'header_style' )
				);

				if ( $et_slide_header ) {
					$filter = 'et_html_slide_header';
					$regex  = '(.*<div.*?class="et_slide_in_menu_container[^>]*?>)(.*)(</div>.*)';
				} else {
					$filter = 'et_html_main_header';
					$regex  = '(.*<div.*?id="et-top-navigation[^>]*?>)(.*)(</div>.*?#et-top-navigation.*)';
				}

				$this->filter_for_inline_area(
					$filter,
					$area,
					$regex
				);
				break;
			// /Location: Navigation.

			// -- Location: Header:
			case 'header':
				if (
					function_exists( 'et_theme_builder_overrides_layout' )
					&& et_theme_builder_overrides_layout( ET_THEME_BUILDER_HEADER_LAYOUT_POST_TYPE )
				) {
					// Custom header designed in Divi´s Theme Builder.
					$this->capture_for_inline_area(
						'et_theme_builder_template_before_header',
						'et_theme_builder_template_after_header',
						$area
					);
				} else {
					// Default header.
					$this->filter_for_inline_area(
						'et_html_main_header',
						$area,
						'(<header[^>]*?>)(.*)(</header>)'
					);
				}
				break;
			// /Location: Header.

			// -- Location: Footer:
			case 'footer':
				if (
					function_exists( 'et_theme_builder_overrides_layout' )
					&& et_theme_builder_overrides_layout( ET_THEME_BUILDER_FOOTER_LAYOUT_POST_TYPE )
				) {
					// Custom footer designed in Divi´s Theme Builder.
					$this->capture_for_inline_area(
						'et_theme_builder_template_before_footer',
						'et_theme_builder_template_after_footer',
						$area
					);
				} else {
					// Default footer.
					$this->capture_for_inline_area(
						'et_after_main_content',
						'wp_footer',
						$area
					);
				}
				break;
			// /Location: Footer.

			// -- Location: Post Content:
			case 'post':
				if (
					function_exists( 'et_theme_builder_overrides_layout' )
					&& et_theme_builder_overrides_layout( ET_THEME_BUILDER_BODY_LAYOUT_POST_TYPE )
				) {
					// Custom post content designed in Divi´s Theme Builder.
					$this->capture_for_inline_area(
						'et_theme_builder_template_before_body',
						'et_theme_builder_template_after_body',
						$area
					);
				} else {
					$this->filter_for_inline_area(
						'the_content',
						$area,
						'',
						function () {
							return is_singular() && is_main_query() && in_the_loop();
						}
					);
				}
				break;
			// /Location: Post Content.

			// -- Location: Comment Form:
			case 'comment':
				$this->capture_for_inline_area(
					'comment_form_before',
					'comment_form_after',
					$area
				);
				break;
			// /Location: Comment Form.

			// -- Location: CSS Selector.
			case 'css':
				/*
				 * Static Areas with CSS selector can be injected during page
				 * rendering. When the Area uses a CSS selector and is _not_ static,
				 * then the Area will always be injected via JS.
				 */
				if ( $area->get( 'is_static' ) ) {
					$this->monitor_shortcodes_for_inline_area( $area );
					$this->inject_hook_footer_static_inline_area( $area );
				} else {
					$this->inject_hook_footer( $area );
				}
				break;
			// /Location: CSS Selector.

			default:
				/**
				 * We did not recognize the location of the Inline Area. Maybe another
				 * plugin/an extension set that location and needs to hook up the
				 * Area?
				 *
				 * @since 1.4.5
				 *
				 * @param DAP_Area $area The Area object.
				 */
				do_action( 'divi_areas_prepare_inline_area_unknown_location', $area );
				break;
		}
	}

	/**
	 * Clears all caches that are related to Divi Areas front end.
	 *
	 * @since 2.0.0
	 *
	 * @param bool $only_plugin_cache When true, only plugin-specific caches are
	 *                                cleared. Otherwise, any display-related cache
	 *                                is cleared, including Divis static file cache.
	 *
	 * @return void
	 */
	public static function clear_all_caches( $only_plugin_cache = false ) {
		/**
		 * Clear the list of _all_ Divi Area IDs.
		 *
		 * @see ::get_all_area_ids() below.
		 */
		wp_cache_delete( 'ids_for_private', DIVI_AREAS_CACHE_GROUP );
		wp_cache_delete( 'ids_for_public', DIVI_AREAS_CACHE_GROUP );

		// Invalidates the DAP_Area cache, so it will be recalculated on next use.
		DAP_Area::invalidate_cache();

		/**
		 * Clear the Divi cache when an Area is changed.
		 */
		if (
			! $only_plugin_cache
			&& function_exists( 'et_get_option' )
			&& class_exists( 'ET_Core_PageResource' )
			&& 'on' === et_get_option( 'et_pb_static_css_file', 'on' )
		) {
			ET_Core_PageResource::remove_static_resources( 'all', 'all' );
		}
	}

	/**
	 * Returns a list of all visible area IDs.
	 *
	 * @since  0.1.0
	 * @return array
	 */
	protected function get_all_area_ids() {
		global $wpdb;

		$post_type = DIVI_AREAS_CPT;
		$use_cache = $this->app()->flag_use_cache();
		$list      = false;

		if ( $use_cache ) {
			if ( is_user_logged_in() ) {
				$cache_key = 'ids_for_private';
			} else {
				$cache_key = 'ids_for_public';
			}

			$list = wp_cache_get( $cache_key, DIVI_AREAS_CACHE_GROUP );
		}

		if ( ! $list || ! is_array( $list ) ) {
			/**
			 * Filters the list of available Divi Areas before the DB is queried.
			 * When an array is returned, the default DB query is bypassed an the
			 * filter value is used instead.
			 *
			 * @since 2.1.0
			 *
			 * @param false|array $area_ids False to use default logic, or a list of
			 *                              post-IDs (must be Divi Area post type).
			 */
			$list = apply_filters( 'divi_areas_pre_get_all_areas', false );

			if ( false === $list || ! is_array( $list ) ) {
				$list = $wpdb->get_col( // phpcs:ignore
					$wpdb->prepare(
						"SELECT `ID`
						FROM `{$wpdb->posts}`
						WHERE
							`post_type` = %s
							AND (
								`post_status` = 'publish'
								OR (%d = 1 AND `post_status` = 'private')
							)
						;",
						$post_type,
						(int) is_user_logged_in()
					)
				);

				if ( ! is_array( $list ) ) {
					$list = [];
				}
			}

			$list = array_flip( $list );
			$list = array_fill_keys( array_keys( $list ), '' );

			if ( $use_cache ) {
				wp_cache_set( $cache_key, $list, DIVI_AREAS_CACHE_GROUP, 3600 );
			}
		}

		/**
		 * Filter the list of all Divi Areas.
		 * The array key is the area ID, the value can be either:
		 * - An empty value '' means, that the plugin determines the visibility based
		 *   on the Area configuration.
		 * - 'show' will include the Area on the current page.
		 * - Anything else will hide the Area.
		 *
		 * @since 0.1.0
		 * @var   array List of Divi Area IDs and visibility flag.
		 */
		return apply_filters( 'divi_areas_get_all_areas', $list );
	}

	/**
	 * Apply configuration from settings screen to the JS object.
	 *
	 * @filter divi_areas_js_data
	 * @since  0.5.0
	 *
	 * @param array $js_data The default settings object.
	 *
	 * @return array Updated settings object.
	 */
	public function apply_settings( $js_data ) {
		$debug_mode = self::get_option( 'debug_mode' );

		if ( $debug_mode ) {
			$js_data['debug'] = 'on' === $debug_mode;
		}

		return $js_data;
	}

	/**
	 * Captures all code that is output between the $start and $end hooks via output
	 * buffering. At the $end hook, the given Inline Area will be injected at the
	 * specified position.
	 *
	 * @since 1.4.3
	 *
	 * @param string   $start Hook that starts the output buffering.
	 * @param string   $end   Hook that ends the buffering and injects the area.
	 * @param DAP_Area $area  The Area.
	 *
	 * @return void
	 */
	protected function capture_for_inline_area( $start, $end, DAP_Area $area ) {
		/**
		 * This function is called when the output buffer is flushed.
		 * It receives the contents of the output buffer as parameter and is expected
		 * to return the new/final contents that are sent to the browser.
		 *
		 * @since 1.4.3
		 *
		 * @param string $code The contents of the output buffer.
		 *
		 * @return string The final contents to send to the browser.
		 */
		$process_code = function ( $code ) use ( $area ) {
			$code = $this->inject_inline_area( $code, $area );

			// phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
			echo et_core_intentionally_unescaped( $code, 'html' );
		};

		add_action(
			$start,
			function () use ( $end, $process_code ) {
				ob_start();

				add_action(
					$end,
					function () use ( $process_code ) {
						$code = ob_get_clean();
						$process_code( $code );
					},
					1
				);
			},
			1000
		);
	}

	/**
	 * Scans each Divi Module for the area CSS selector and injects the Area when a
	 * match is found.
	 *
	 * @since 2.0.0
	 *
	 * @param DAP_Area $area The Area to inject.
	 *
	 * @return void
	 */
	protected function monitor_shortcodes_for_inline_area( DAP_Area $area ) {
		$selectors     = [];
		$all_selectors = explode(
			"\n",
			str_replace( ',', "\n", $area->get( 'location_selector' ) )
		);
		$all_selectors = array_map( 'trim', $all_selectors );
		$all_selectors = array_unique( $all_selectors );

		// Find those selectors that can be parsed by Divi Areas Pro.
		foreach ( $all_selectors as $selector ) {
			if (
				strlen( $selector ) > 1
				&& false === stripos( $selector, ' ' )
				&& ( '.' === $selector[0] || '#' === $selector[0] )
				&& false === strpos( $selector, '.', 1 )
				&& false === strpos( $selector, '#', 1 )
				&& false === strpos( $selector, '[' )
				&& false === strpos( $selector, '=' )
			) {
				$selectors[] = $selector;
			}
		}

		/**
		 * The filter handler which processes each shortcode.
		 *
		 * @since 2.0.0
		 *
		 * @param string       $code The shortcode result (HTML code).
		 * @param string       $tag  The shortcode tag, e.g. "et_pb_section".
		 * @param array|string $args Shortcode attributes array or empty string.
		 *
		 * @return string Filtered $code.
		 */
		$process_shortcode = function ( $code, $tag, $args ) use ( $area, $selectors ) {
			if (
				true === $area->get( 'injected' )
				|| DAP_Area::is_doing_area()
				|| empty( $args )
				|| empty( $args['module_id'] )
			) {
				return $code;
			}

			if ( $this->check_inline_selectors( $args, $selectors ) ) {
				$code = $this->inject_inline_area( $code, $area );
			}

			return $code;
		};

		if ( count( $selectors ) ) {
			add_filter( 'do_shortcode_tag', $process_shortcode, 1, 3 );
		}
	}

	/**
	 * Returns true, when the shortcode args match any location selector.
	 *
	 * @since  2.0.0
	 *
	 * @param array $args      Shortcode args.
	 * @param array $selectors List of area selectors.
	 *
	 * @return bool Whether the args matches any selector.
	 */
	protected function check_inline_selectors( $args, $selectors ) {
		$match = false;

		foreach ( $selectors as $selector ) {
			if ( $match ) {
				break;
			}

			if ( '#' === $selector[0] ) {
				// ID selector; must match exactly.
				if ( ! empty( $args['module_id'] ) ) {
					$match = substr( $selector, 1 ) === $args['module_id'];
				}
			} elseif ( '.' === $selector[0] ) {
				// Class selector; must match any class.
				if ( ! empty( $args['module_class'] ) ) {
					$classes = explode( ' ', $args['module_class'] );

					$match = in_array( substr( $selector, 1 ), $classes, true );
				}
			}
		}

		return $match;
	}

	/**
	 * Injects an Inline Area into the return value of the given $hook.
	 * When a $regex is supplied, the Inline Area will be added relative to that
	 * Regex. This regex must always contain 3 bracket parts, as seen here:
	 * >     (before the area)(.*)(after the area)
	 * The Regex must be unescaped and without delimiters or modifiers.
	 * >     /(<before>)(.*)(<\/after>)/i  <-- Wrong!
	 * >     (<before>)(.*)(</after>)      <-- Correct
	 * The Inline Area will either be prepended/appended to the middle group, or
	 * replace this group. Code that is in the first and third groups will stay
	 * unchanged.
	 * When no $regex is supplied, the entire contents will be modified.
	 *
	 * @since 1.4.3
	 *
	 * @param string   $filter    The filter to hook into. The first parameter of that
	 *                            filter is expected to contain the code to modify.
	 * @param DAP_Area $area      The Area.
	 * @param string   $regex     Optional. An Regex to find the relevant code part.
	 *                            The regex must have NO delimiters or modifiers.
	 * @param callback $condition Optional. Callback function that returns true or
	 *                            false, depending on whether the Area should be
	 *                            injected at this place.
	 *
	 * @return void
	 */
	protected function filter_for_inline_area( $filter, DAP_Area $area, $regex = '', $condition = null ) {
		if ( $regex ) {
			$process_code = function ( $code ) use ( $area, $regex, $condition ) {
				if (
					$condition
					&& is_callable( $condition )
					&& false === $condition( $code, $area )
				) {
					return $code;
				}

				preg_match( '!' . $regex . '!si', $code, $res );

				if ( 4 === count( $res ) ) {
					array_shift( $res );

					$res[1] = $this->inject_inline_area( $res[1], $area );

					$code = implode( '', $res );
				}

				return $code;
			};
		} else {
			$process_code = function ( $code ) use ( $area, $condition ) {
				if (
					$condition
					&& is_callable( $condition )
					&& false === $condition( $code, $area )
				) {
					return $code;
				}

				return $this->inject_inline_area( $code, $area );
			};
		}

		add_filter(
			$filter,
			function ( $code ) use ( $process_code ) {
				return $process_code( $code );
			},
			1
		);
	}

	/**
	 * Injects an Inline Area into the given code snippet.
	 *
	 * @since  0.4.0
	 *
	 * @param string   $code Original HTML code.
	 * @param DAP_Area $area The Area.
	 *
	 * @return string The modified HTML code.
	 */
	protected function inject_inline_area( $code, DAP_Area $area ) {
		$position = $area->get( 'location_position' );

		if ( true === $area->get( 'injected' ) ) {
			return $code;
		}

		$area->set( 'injected', true );

		if ( $area->get( 'is_static' ) ) {
			/**
			 * Insert a placeholder into the content. That placeholder is replaced
			 * later (see next function call).
			 * Reason for this two-step injection is the wpautop logic, which adds
			 * many <p>/<br> tags into our Area when it's injected before the_content
			 * filter is finished.
			 */
			$placeholder = sprintf(
				'<DiviAreasPro %d/>',
				$area->ID
			);

			$this->replace_in_footer( $placeholder, $area );

			if ( 'before' === $position ) {
				$code = $placeholder . $code;
			} elseif ( 'after' === $position ) {
				$code .= $placeholder;
			} elseif ( 'replace' === $position ) {
				$code = $placeholder;
			}
		} else {
			/*
			Non-Static Inline Areas (i.e., dynamic Inline Areas) either have a
			trigger or a Close Button. That means, they need to be controlled by
			the JS API and injected by the browser.
			*/

			/**
			 * The $hook variable defines a hidden "hook"-tag that informs the JS API
			 * where the dynamic Inline Area should be displayed.
			 *
			 * @since 1.4.4
			 * @var string
			 */
			$hook = sprintf(
				'<span data-area-hook="%s" data-area-place="%s"></span>',
				esc_attr( $area->ID ),
				esc_attr( $position )
			);

			$code = $hook . $code;

			// Also inject the full area HTML/JS, so the hook is actually used.
			$this->inject_hook_footer( $area );
		}

		return $code;
	}

	/**
	 * Replaces the specified placeholder with the full Area code at the end of the
	 * wp_footer action.
	 *
	 * @since 2.0.0
	 *
	 * @param string   $placeholder The placeholder string to search for.
	 * @param DAP_Area $area        The Area to inject.
	 *
	 * @return void
	 */
	protected function replace_in_footer( $placeholder, DAP_Area $area ) {
		$this->inline_placeholders[ $placeholder ] = $area->ID;

		if ( ! $this->inline_buffer ) {
			$this->inline_buffer = ob_start();

			/**
			 * Priority 9 is chosen intentionally and should not be changed.
			 *
			 * @see inject_hook_footer() for more details on that priority.
			 */
			add_action( 'wp_footer', [ $this, 'do_replace_in_footer' ], 9 );
		}
	}

	/**
	 * Replaces all Inline Areas from the `inline_placeholders` list and then outputs
	 * the buffered HTML code.
	 *
	 * @action wp_footer [9]
	 * @since  2.0.0
	 * @return void
	 */
	public function do_replace_in_footer() {
		if ( $this->inline_buffer ) {
			$this->inline_buffer = false;

			$output = ob_get_clean();

			foreach ( $this->inline_placeholders as $placeholder => $area_id ) {
				$area = DAP_Area::get_area( $area_id );

				if ( $area ) {
					$area_code = $area->get_full_code();
					$output    = str_replace( $placeholder, $area_code, $output );
				}
			}

			// The code comes form the output buffer and was already escaped earlier.
			echo et_core_intentionally_unescaped( $output, 'html' );
		}
	}

	/**
	 * Ajax handler that checks if a given Area is visible right now.
	 * This method uses neither a nonce nor a capability check, as it's intended to be
	 * public accessible. The JS API even tries to omit session details to make this
	 * Ajax request run faster.
	 * phpcs:disable WordPress.Security.NonceVerification.Recommended
	 *
	 * @since 2.2.0
	 * @return void
	 */
	public function ajax_check_area_visibility() {
		if ( ! isset( $_GET['a'] ) || ! is_numeric( $_GET['a'] ) ) {
			wp_send_json_error( '500' );
		}

		$area_id = (int) $_GET['a'];
		$area    = DAP_Area::get_area( $area_id );

		if ( ! $area ) {
			wp_send_json_error( '404' );
		}

		if ( ! empty( $_GET['c'] ) ) {
			$cond_key = sanitize_key( $_GET['c'] );
		} else {
			$cond_key = 'default';
		}

		if ( ! $area->config['ajax_conditions'] || ! isset( $area->config['ajax_conditions'][ $cond_key ] ) ) {
			wp_send_json_error( '416' );
		}

		$condition = $area->config['ajax_conditions'][ $cond_key ];
		$now       = time();
		$in_range  = false;

		// Check the date schedule.
		if ( 'on_date' === $condition['schedule'] || 'not_date' === $condition['schedule'] ) {
			foreach ( $condition['schedule_dates'] as $range ) {
				if ( $now >= $range['start'] && $now <= $range['end'] ) {
					$in_range = true;
					break;
				}
			}
			if ( 'not_date' === $condition['schedule'] ) {
				$in_range = ! $in_range;
			}
		}

		// Check the weekly schedule.
		if ( 'weekly' === $condition['schedule'] ) {
			$day_of_week  = (int) current_time( 'w' );
			$current_time = current_time( 'H:i' );

			foreach ( $condition['schedule_weekly'] as $range ) {
				if ( ! in_array( $day_of_week, $range['days'], true ) ) {
					continue;
				}
				if ( $current_time >= $range['start'] && $current_time <= $range['end'] ) {
					$in_range = true;
					break;
				}
			}
		}

		wp_send_json_success( $in_range );
	}

	/**
	 * Parses the current wp_query and returns an array that describes the aspects
	 * type, subtype, id. Those details are used by the `is_visible_on_current_page()`
	 * method to evaluate postlist settings.
	 *
	 * @since 2.0.0
	 * @since 2.3.0 Moved from DAP_Area to DAP_Front
	 * @return array Relevant details about the current request.
	 */
	public static function get_request_info() {
		if ( ! is_array( self::$_request ) ) {
			self::$_request = [
				'type'    => '',
				'subtype' => '',
				'id'      => 0,
				'key'     => 'unknown',
			];

			if ( divi_area_vb_version( '4.0.0' ) ) {
				$is_extra_layout_home = 'layout' === get_option( 'show_on_front' ) && is_home();

				if ( $is_extra_layout_home || is_front_page() ) {
					self::$_request['type'] = ET_Theme_Builder_Request::TYPE_FRONT_PAGE;
					self::$_request['id']   = get_queried_object_id();
				} elseif ( is_404() ) {
					self::$_request['type'] = ET_Theme_Builder_Request::TYPE_404;
				} elseif ( is_search() ) {
					self::$_request['type'] = ET_Theme_Builder_Request::TYPE_SEARCH;
				} else {
					$id             = get_queried_object_id();
					$object         = get_queried_object();
					$page_for_posts = (int) get_option( 'page_for_posts' );
					$is_blog_page   = 0 !== $page_for_posts && is_page( $page_for_posts );

					if ( is_singular() ) {
						self::$_request['type']    = ET_Theme_Builder_Request::TYPE_SINGULAR;
						self::$_request['subtype'] = get_post_type( $id );
						self::$_request['id']      = $id;
					} elseif ( $is_blog_page || is_home() ) {
						self::$_request['type']    = ET_Theme_Builder_Request::TYPE_POST_TYPE_ARCHIVE;
						self::$_request['subtype'] = 'post';
						self::$_request['id']      = $id;
					} elseif ( is_category() || is_tag() || is_tax() ) {
						self::$_request['type']    = ET_Theme_Builder_Request::TYPE_TERM;
						self::$_request['subtype'] = $object->taxonomy;
						self::$_request['id']      = $id;
					} elseif ( is_post_type_archive() ) {
						self::$_request['type']    = ET_Theme_Builder_Request::TYPE_POST_TYPE_ARCHIVE;
						self::$_request['subtype'] = $object->name;
						self::$_request['id']      = $id;
					} elseif ( is_author() ) {
						self::$_request['type'] = ET_Theme_Builder_Request::TYPE_AUTHOR;
						self::$_request['id']   = $id;
					} elseif ( is_date() ) {
						self::$_request['type'] = ET_Theme_Builder_Request::TYPE_DATE;
					}
				}

				// Generate a key that contains all request details in a fixed order.
				$key_parts = [
					self::$_request['type'],
					self::$_request['subtype'],
					self::$_request['id'],
				];

				self::$_request['key'] = sanitize_key(
					implode( '_', array_filter( $key_parts ) )
				);

				DAP_Component::logger()->debug(
					'Request',
					'Id: ' . self::$_request['id'],
					'Type: ' . self::$_request['type'],
					'Subtype: ' . self::$_request['subtype'],
					'Key: ' . self::$_request['key']
				);
			}
		}

		return self::$_request;
	}

	/**
	 * Check if this request fulfills a template setting.
	 *
	 * @since 2.0.0
	 * @since 2.3.0 Moved from DAP_Area to DAP_Front
	 *
	 * @param string $setting         The template setting string.
	 * @param array  $template_config The template configuration, returned by
	 *                                self::get_template_setting_ancestor().
	 *
	 * @return bool True, when current page matches the defined template.
	 */
	public static function fulfills_template_setting( $setting, $template_config ) {
		$fulfilled = false;

		if (
			$template_config
			&& is_array( $template_config )
			&& isset( $template_config['validate'] )
			&& is_callable( $template_config['validate'] )
		) {
			$req = self::get_request_info();

			// @phpcs:ignore Generic.PHP.ForbiddenFunctions.Found
			$fulfilled = call_user_func(
				$template_config['validate'],
				$req['type'],
				$req['subtype'],
				$req['id'],
				explode( ET_THEME_BUILDER_SETTING_SEPARATOR, $setting )
			);
		}

		return $fulfilled;
	}

	/**
	 * Get the top ancestor of a setting based on its id. Takes the setting itself if
	 * it has no ancestors. Returns an empty array if the setting is not found.
	 * Samples:
	 * 'singular:post_type:page:all' has no ancestors.
	 * 'singular:post_type:page:id:1200' uses ancestor 'singular:post_type:page:id:'
	 *
	 * @since 2.0.0
	 * @since 2.3.0 Moved from DAP_Area to DAP_Front
	 *
	 * @param string $setting The template setting string to parse.
	 *
	 * @return array The (ancestor) template configuration item, or an empty array.
	 */
	public static function get_template_setting_ancestor( $setting ) {
		if ( ! self::$_postlist_settings ) {
			self::$_postlist_settings = et_theme_builder_get_flat_template_settings_options();
		}

		if ( ! defined( 'ET_THEME_BUILDER_SETTING_SEPARATOR' ) ) {
			return [];
		}

		if ( ! isset( self::$_postlist_settings[ $setting ] ) ) {
			// If the setting is not found, check if a valid parent exists.
			$setting = substr( $setting, 0, strrpos( $setting, ET_THEME_BUILDER_SETTING_SEPARATOR ) + 1 );
		}

		if ( ! isset( self::$_postlist_settings[ $setting ] ) ) {
			// The setting is still not found - bail.
			return [];
		}

		return self::$_postlist_settings[ $setting ];
	}
}
