<?php
/**
 * Provides the custom post type for the Divi Areas.
 *
 * @package Divi_Areas_Pro
 */

/**
 * Custom Post Type logic.
 */
class DAP_Post_Type
	extends DAP_Component {
	/**
	 * Called during the "plugins_loaded" action. Hooks up the admin menu, etc.
	 *
	 * @since  0.1.0
	 * @return void
	 */
	public function setup() {
		add_action(
			'init',
			[ $this, 'register_cpt' ]
		);

		add_action(
			'divimode_saved_options_divi-areas_setting',
			[ $this, 'update_cpt' ]
		);

		add_action(
			'divimode_save_post_data_divi-areas_area',
			[ $this, 'save_da_area' ],
			10,
			2
		);

		add_filter(
			'et_fb_is_enabled',
			[ $this, 'et_fb_is_new_divi_area' ],
			10,
			2
		);
	}

	/**
	 * Register the custom Post Type that holds predefined Divi Areas.
	 *
	 * @since  0.1.0
	 * @return void
	 */
	public function register_cpt() {
		// phpcs:ignore WordPress.WP.AlternativeFunctions.file_get_contents_file_get_contents
		$icon = file_get_contents( DIVI_AREAS_PATH . 'img/plugin-icon.svg' );

		// phpcs:ignore WordPress.PHP.DiscouragedPHPFunctions.obfuscation_base64_encode
		$encoded_icon = base64_encode( $icon );

		$labels = [
			'name'               => _x( 'Divi Areas', 'Custom Post Type', 'divi_areas' ),
			'singular_name'      => _x( 'Divi Areas', 'Custom Post Type', 'divi_areas' ),
			'add_new'            => _x( 'Add New', 'Custom Post Type', 'divi_areas' ),
			'add_new_item'       => _x( 'Add New Divi Area', 'Custom Post Type', 'divi_areas' ),
			'edit_item'          => _x( 'Edit Divi Area', 'Custom Post Type', 'divi_areas' ),
			'new_item'           => _x( 'New Divi Area', 'Custom Post Type', 'divi_areas' ),
			'view_item'          => _x( 'View Divi Area', 'Custom Post Type', 'divi_areas' ),
			'search_items'       => _x( 'Search Divi Areas', 'Custom Post Type', 'divi_areas' ),
			'not_found'          => _x( 'No Divi Areas found', 'Custom Post Type', 'divi_areas' ),
			'not_found_in_trash' => _x( 'No Divi Areas found in Trash', 'Custom Post Type', 'divi_areas' ),
			'parent_item_colon'  => _x( 'Parent Divi Area:', 'Custom Post Type', 'divi_areas' ),
			'menu_name'          => _x( 'Divi Areas', 'Custom Post Type', 'divi_areas' ),
		];

		$menu_pos = (int) self::get_option( 'menu_pos' );

		$args = [
			'labels'              => $labels,
			'hierarchical'        => false,
			'supports'            => [ 'title', 'editor', 'author', 'revisions' ],
			'public'              => true, // We want to enable the slugs and preview.
			'publicly_queryable'  => true, // Needed for the BFB Visual Builder!
			'show_ui'             => true, // The heart of our admin UI.
			'show_in_menu'        => true, // true = generate top-level menu.
			'menu_position'       => $menu_pos,
			'show_in_nav_menus'   => false, // Do not show Areas in the menu builder.
			'show_in_admin_bar'   => true,
			'show_in_rest'        => false,
			'exclude_from_search' => true,  // Never show Areas in the front end search results.
			'has_archive'         => false, // Do not create a front end archives endpoint.
			'query_var'           => true,  // Allow `?divi-area=slug` URLs?
			'can_export'          => true,
			'rewrite'             => true,  // Needed for our preview feature.
			'capability_type'     => 'post',
			'menu_icon'           => 'data:image/svg+xml;base64,' . $encoded_icon,
		];

		/**
		 * Filter the Post Type args, in case someone wants to modify REST export
		 * UI preferences, etc.
		 *
		 * @since 0.1.0
		 * @var array $args The options passed to register_post_type() later.
		 */
		$args = apply_filters( 'divi_areas_register_cpt', $args );

		$cpt = register_post_type( DIVI_AREAS_CPT, $args );

		// Customize the Post Type, when registration was successful.
		if ( $cpt && is_a( $cpt, 'WP_Post_Type' ) ) {
			$this->hook_save();

			add_filter(
				'et_builder_post_types',
				[ $this, 'et_builder_post_types' ]
			);

			add_filter(
				"manage_edit-{$cpt->name}_columns",
				[ $this, 'setup_admin_columns' ]
			);

			add_action(
				"manage_{$cpt->name}_posts_custom_column",
				[ $this, 'manage_admin_columns' ],
				10,
				2
			);

			add_action(
				"add_meta_boxes_{$cpt->name}",
				[ $this, 'add_cpt_metabox' ],
				99
			);

			// Modify the "Permalink" form. We call it "Custom trigger".
			add_filter(
				'get_sample_permalink_html',
				[ $this, 'sample_permalink' ],
				10,
				5
			);

			// Define a custom page template for Divi Area previews.
			add_filter(
				'single_template',
				[ $this, 'template_for_preview' ],
				20
			);

			/**
			 * Notify other plugin parts that the custom post type is ready.
			 *
			 * @since 0.1.0
			 * @var   string $post_type The post type slug.
			 */
			do_action( 'divi_areas_setup_cpt', $cpt->name );
		}
	}

	/**
	 * Updates the Divi-Areas CPT to match the plugin settings.
	 * This function is called when the settings page is saved to instantly reflect
	 * the changes.
	 *
	 * @since 2.0.0
	 */
	public function update_cpt() {
		// Re-load the options from DB.
		$this->settings_library()->load_options();

		$menu_pos = (int) self::get_option( 'menu_pos' );

		$cpt_obj                = get_post_type_object( DIVI_AREAS_CPT );
		$cpt_obj->menu_position = $menu_pos;

		register_post_type( DIVI_AREAS_CPT, $cpt_obj );
	}

	/**
	 * Filters the Area-details before they are saved to the post meta table.
	 *
	 * @since 2.3.0
	 *
	 * @param array $data    The form data that will be saved to the DB.
	 * @param int   $post_id The target post.
	 *
	 * @return array
	 */
	public function save_da_area( $data, $post_id ) {
		$area = DAP_Area::get_area( $post_id );
		$area->parse_config( $data );
		$area->save_config();

		return $data;
	}

	/**
	 * Always enable the Divi Visual Builder for our custom post type.
	 *
	 * @since  0.1.0
	 *
	 * @param array $post_types List of post types that can use the Divi VB.
	 *
	 * @return array Modified list of post types.
	 */
	public function et_builder_post_types( $post_types ) {
		$post_types[] = DIVI_AREAS_CPT;

		return $post_types;
	}

	/**
	 * Remove the custom Divi metabox from our post type.
	 *
	 * @since  0.1.0
	 *
	 * @param WP_Post $post The displayed post.
	 *
	 * @return void
	 */
	public function add_cpt_metabox( $post ) {
		$post_type = $post->post_type;

		// The original Divi-Metabox is not relevant for Divi Areas.
		remove_meta_box( 'et_settings_meta_box', $post_type, 'side' );

		$box = $this->divimode_library()->new_metabox();
		$box->init(
			'area',
			__( 'Divi Area', 'divi_areas' ),
			$post_type,
			DIVI_AREAS_PATH . 'template',
			DIVI_AREAS_PATH . 'img/plugin-icon.svg'
		);

		$box->add_tab( 'settings', __( 'Settings', 'divi_areas' ) );
		$box->add_tab( 'triggers', __( 'Trigger', 'divi_areas' ) );
		$box->add_tab( 'behavior', __( 'Behavior', 'divi_areas' ) );
		$box->add_tab( 'layout', __( 'Layout', 'divi_areas' ) );
	}

	/**
	 * Adds the WP save-post hook for our custom post type.
	 *
	 * @since  2.0.0
	 * @return void
	 */
	protected function hook_save() {
		$post_type = DIVI_AREAS_CPT;

		add_action(
			"save_post_{$post_type}",
			[ $this, 'save_divi_area' ],
			10,
			2
		);

		/*
		 * Filter is called before the admin editor is displayed. We use this
		 * opportunity to apply the "init_divi_editor" flag.
		 */
		add_filter(
			'replace_editor',
			[ $this, 'prepare_divi_area_for_editor' ],
			10,
			2
		);

		/**
		 * Indicates, that Divi Area´s save_post hook was added.
		 *
		 * @since 1.0.0
		 */
		do_action( 'divi_areas_add_save_hook' );
	}

	/**
	 * Adds the WP save-post hook for our custom post type.
	 *
	 * @since  2.0.0
	 * @return void
	 */
	protected function unhook_save() {
		$post_type = DIVI_AREAS_CPT;

		remove_action(
			"save_post_{$post_type}",
			[ $this, 'save_post' ],
			10
		);

		remove_action(
			'replace_editor',
			[ $this, 'prepare_divi_area_for_editor' ],
			10
		);

		/**
		 * Indicates, that Divi Area´s save_post hook was removed.
		 *
		 * @since 1.0.0
		 */
		do_action( 'divi_areas_remove_save_hook' );
	}

	/**
	 * Automatically enable the Divi page builder for the new post.
	 *
	 * @since  0.4.0
	 *
	 * @param int     $post_id The post ID.
	 * @param WP_Post $post    The post object.
	 *
	 * @return int The post ID.
	 */
	public function save_divi_area( $post_id, $post ) {
		// Always update the "_et_pb_use_builder" post-meta flag, even on autosave.
		if ( 'on' === self::get_option( 'init_divi_editor' ) ) {
			$_REQUEST['et_pb_use_builder'] = 'on';
			$_POST['et_pb_use_builder']    = 'on';

			update_post_meta( $post_id, '_et_pb_use_builder', 'on' );
		}

		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return $post_id;
		}

		$post_type = get_post_type_object( $post->post_type );
		if ( ! current_user_can( $post_type->cap->edit_post, $post_id ) ) {
			return $post_id;
		}

		// Prevent recursion when saving an Area.
		$this->unhook_save();

		/*
		 * Render the post contents after saving all details. The reason is, that Divi
		 * will update certain post-meta fields when a post is rendered (like the
		 * "_et_builder_use_spam_service" flag).
		 *
		 * When we do not generate those flags now, Divi will insert them into the
		 * wrong post (i.e. into the displayed post, instead of the Divi Area post).
		 *
		 * Divi will insert those post-meta values for the post that's defined in
		 * $_POST['post']. That param is set by the Visual Builder and typically is
		 * empty when processing the WordPress post form. So we implement a small hack
		 * to temporarily set $_POST['post'] to our Divi Area post-ID, let Divi do
		 * it's work, and then revert the $_POST object back to the original state.
		 */

		// phpcs:disable WordPress.Security.NonceVerification.Missing
		// phpcs:disable WordPress.Security.ValidatedSanitizedInput.MissingUnslash
		// phpcs:disable WordPress.Security.ValidatedSanitizedInput.InputNotSanitized
		// phpcs:disable ET.Sniffs.ValidatedSanitizedInput.InputNotSanitized
		if ( isset( $_POST['post'] ) ) {
			$orig_post = $_POST['post'];
		} else {
			$orig_post = false;
		}
		$_POST['post'] = $post_id;

		/*
		 * This is the crucial function which might update/insert data in the
		 * post-meta table for the post_id in $_POST['post']. We do not need the
		 * resulting HTML, so we do not assign the return value.
		 */
		apply_filters( 'et_builder_render_layout', $post->post_content );

		if ( $orig_post ) {
			$_POST['post'] = $orig_post;
		} else {
			unset( $_POST['post'] );
		}
		// phpcs:enable

		$this->module( 'front' )->clear_all_caches();

		// Restore the save-hook.
		$this->hook_save();
	}

	/**
	 * Filter that is called before the post editor is displayed.
	 *
	 * We use it to apply the "Always use Divi Editor" flag and return the unmodified
	 * filter result. This logic is required to handle the case, when a user creates a
	 * Divi Area while "Always use Divi Editor" is disabled, then enables the flag and
	 * tries to edit the existing Divi Area. Without this code, the editor would not
	 * load, until the Divi Area was saved at least once.
	 *
	 * @since 2.0.0
	 *
	 * @param bool    $result Ignored.
	 * @param WP_Post $post   The post that is loaded into the editor.
	 *
	 * @return bool The unmodified result parameter.
	 */
	public function prepare_divi_area_for_editor( $result, $post ) {
		if (
			is_a( $post, 'WP_Post' )
			&& $post->ID
			&& DIVI_AREAS_CPT === $post->post_type
			&& 'on' === self::get_option( 'init_divi_editor' )
		) {
			update_post_meta( $post->ID, '_et_pb_use_builder', 'on' );
		}

		return $result;
	}

	/**
	 * Ensures that the Visual Builder loads correctly when creating a new Divi Area
	 * with the Editor Setting "Always use Divi Editor" is enabled.
	 *
	 * @filter et_fb_is_enabled
	 * @since  2.0.0
	 *
	 * @param bool|null $result  The result of the filter.
	 * @param int       $post_id The post that is edited.
	 *
	 * @return bool|null The filter result.
	 */
	public function et_fb_is_new_divi_area( $result, $post_id ) {
		// phpcs:ignore WordPress.Security.NonceVerification.Recommended
		if ( current_user_can( 'edit_posts' ) && self::is_new_divi_area( $_GET ) ) {
			$result = true;
		}

		return $result;
	}

	/**
	 * Tests, whether the given request creates a new Divi Area post.
	 * Divis' internal check contains the condition "is_singular()" which will return
	 * false when the WordPress homepage is set to "latest posts", so we implement
	 * this logic to always enable the Visual Builder for BFB requests that create a
	 * new Divi Area.
	 * When creating a brand new post in BFB, the iframe URL does not
	 * contain a post-type hint and Divis' code-base assumes that all posts that are
	 * generated via BFB are 'post' types.
	 *
	 * @since 2.1.1
	 *
	 * @param array $request The GET/POST parameter array.
	 *
	 * @return bool True, when the request params indicate a new Divi Area is created.
	 */
	public static function is_new_divi_area( $request ) {
		$result = false;

		if (
			! empty( $request['et_fb'] )
			&& ! empty( $request['et_bfb'] )
			&& ! empty( $request['is_new_page'] )
			&& ! empty( $request['custom_page_id'] )
			&& DIVI_AREAS_CPT === get_post_type( (int) $request['custom_page_id'] )
		) {
			$result = true;
		}

		return $result;
	}

	/**
	 * Register custom columns for our Post TType.
	 *
	 * @since  0.1.0
	 *
	 * @param array $columns List of column keys and titles.
	 *
	 * @return array Modified column list.
	 */
	public function setup_admin_columns( $columns ) {
		return [
			'cb'                                  => '<input type="checkbox" />',
			'title'                               => __( 'Title', 'divi_areas' ),
			'trigger'                             => __( 'CSS trigger', 'divi_areas' ),
			'type'                                => __( 'Area Type', 'divi_areas' ),
			'taxonomy-' . DIVI_AREAS_TAX_CATEGORY => __( 'Category', 'divi_areas' ),
			'author'                              => __( 'Author', 'divi_areas' ),
			'date'                                => __( 'Date', 'divi_areas' ),
		];
	}

	/**
	 * Output the column value for our custom post type columns.
	 *
	 * @since  0.1.0
	 *
	 * @param string $column  The column ID.
	 * @param int    $post_id The currently displayed post.
	 *
	 * @return void
	 */
	public function manage_admin_columns( $column, $post_id ) {
		switch ( $column ) {
			case 'trigger':
				$post = get_post( $post_id );
				if ( $post->post_name ) {
					printf(
						'<p><code>%1$s</code></p><p><code>%2$s</code></p>',
						esc_html( DIVI_AREAS_ID_PREFIX . $post->post_name ),
						esc_html( DIVI_AREAS_ID_PREFIX . $post_id )
					);
				} else {
					printf(
						'<p><code>%s</code></p>',
						esc_html( DIVI_AREAS_ID_PREFIX . $post_id )
					);
				}
				break;

			case 'type':
				$layout_types = [
					'popup'  => __( 'Popup', 'divi_areas' ),
					'inline' => __( 'Inline', 'divi_areas' ),
					'flyin'  => __( 'Fly-in', 'divi_areas' ),
					'hover'  => __( 'Hover', 'divi_areas' ),
				];

				$area = DAP_Area::get_area( $post_id );
				echo esc_html( $layout_types[ $area->get( 'layout_type' ) ] );
				break;

			default:
				break;
		}
	}

	/**
	 * Generate a sample permalink that shows the Divi Area trigger.
	 *
	 * @since 0.4.0
	 *
	 * @param string  $return    Sample permalink HTML markup.
	 * @param int     $post_id   Post ID.
	 * @param string  $new_title New sample permalink title.
	 * @param string  $new_slug  New sample permalink slug.
	 * @param WP_Post $post      Post object.
	 *
	 * @return string The new permalink HTML code.
	 */
	public function sample_permalink( $return, $post_id, $new_title, $new_slug, $post ) {
		if ( DIVI_AREAS_CPT !== $post->post_type ) {
			return $return;
		}

		list( , $post_name ) = get_sample_permalink( $post_id, $new_title, $new_slug );

		$return = [];

		$return[] = '<strong>' . __( 'Custom trigger:', 'divi_areas' ) . "</strong>\n";
		$return[] = '<span id="sample-permalink"><code>#' . DIVI_AREAS_ID_PREFIX . '<span id="editable-post-name">' . esc_html( $post_name ) . '</span></code> </span> ';
		$return[] = '&lrm;'; // Fix bi-directional text display defect in RTL languages.
		$return[] = sprintf(
			'<span id="edit-slug-buttons"><button type="button" class="edit-slug button button-small hide-if-no-js" aria-label="%s">%s</button></span>',
			__( 'Edit trigger', 'divi_areas' ),
			__( 'Edit' ) // phpcs:ignore WordPress.WP.I18n.MissingArgDomain
		);
		$return[] = '<span id="editable-post-name-full">' . esc_html( $post_name ) . "</span>\n";

		return implode( '', $return );
	}

	/**
	 * Serve a custom page template to display the Divi Area previews.
	 * This filter is called for every `single` page, so we need to make sure to only
	 * modify the template for Divi Areas.
	 *
	 * @filter single_template
	 * @since  2.0.0
	 *
	 * @param string $template Path to the template. See locate_template().
	 *
	 * @return string The path to our custom page template.
	 */
	public function template_for_preview( $template ) {
		if (
			is_singular( DIVI_AREAS_CPT )
			&& ! $this->module( 'front' )->is_editor() // Do not change the Visual Builder.
		) {
			$post_id   = get_the_ID();
			$post_type = get_post_type_object( DIVI_AREAS_CPT );

			/*
			 * Only users who can edit the Area can also preview them:
			 *
			 * An Area can contain internal navigation details, coupons, and other
			 * information that should not be disclosed to any user. This condition
			 * ensures, that only valid editors and admins can preview an Area and see
			 * all those details.
			 */
			$can_preview_area = current_user_can( $post_type->cap->edit_post, $post_id );

			/**
			 * Filter the preview capability.
			 *
			 * @since 2.1.0
			 *
			 * @param bool $can_preview Whether the preview should be displayed.
			 * @param int  $post_id     ID of the area.
			 */
			$can_preview_area = apply_filters(
				'divi_areas_can_preview_area',
				$can_preview_area,
				$post_id
			);

			if ( ! $can_preview_area ) {
				/**
				 * Filters the URL to which users are redirected that are not allowed
				 * to preview a Divi Area.
				 *
				 * @since 2.1.0
				 *
				 * @param string $redirect_to The redirect URL. Default is the
				 *                            websites home URL.
				 * @param int    $post_id     The area ID.
				 */
				$redirect_to = apply_filters(
					'divi_areas_preview_redirect_url',
					site_url( '/' ),
					$post_id
				);

				wp_safe_redirect( $redirect_to );
				exit;
			}

			/**
			 * Fires when a Divi Area preview is displayed.
			 *
			 * @since 2.1.0
			 *
			 * @param int $post_id ID of the Area in the preview.
			 */
			do_action( 'divi_areas_show_preview', $post_id );

			add_action(
				'wp_enqueue_scripts',
				[ $this->module( 'asset' ), 'enqueue_styles_preview' ]
			);

			$template = DIVI_AREAS_PATH . 'template/single-divi-area.php';
		}

		return $template;
	}
}
