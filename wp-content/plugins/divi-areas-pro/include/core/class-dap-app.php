<?php
/**
 * The main application class.
 *
 * @package Divi_Areas_Pro
 */

defined( 'ABSPATH' ) || die();

use DiviAreasPro\DM_Settings;
use DiviAreasPro\DM_Library;

/**
 * The application entry class.
 */
class DAP_App
	extends DAP_Component {

	/**
	 * Name of the plugin option name.
	 *
	 * @var string
	 */
	private static $config_slug = '_da_setting';

	/**
	 * Prepares the plugin for first use when activated.
	 *
	 * @since 2.0.0
	 */
	public static function plugin_activated() {
		self::divimode_library()->install_plugin( self::$config_slug );
		DAP_Admin::clear_all_caches();
	}

	/**
	 * Initialize plugin configuration.
	 *
	 * @since  0.1.0
	 * @since  1.2.3 function renamed from init_vars() to setup_instance().
	 * @return void
	 */
	protected function setup_instance() {
		$this->init_constants();
	}

	/**
	 * Called during the "plugins_loaded" action. Hooks up the admin menu, etc.
	 *
	 * @since 0.1.0
	 * @since 1.2.3 function renamed from __construct() to setup().
	 */
	public function setup() {
		$this->logger()->set_subdir( 'divi-areas-logs' );

		add_action( 'init', [ $this, 'init_divimode_library' ], 1 );
		add_action( 'divimode_register_components', [ $this, 'register_components' ] );
		add_action( 'divi_areas_localize_admin_js', [ $this, 'localize_admin_js' ] );

		// Add dependencies.
		$this->add_module( 'asset', 'DAP_Asset' )->setup_on( 'divi_areas_loaded' );
		$this->add_module( 'cpt', 'DAP_Post_Type' )->setup_on( 'divi_areas_loaded' );
		$this->add_module( 'category', 'DAP_Category' )->setup_on( 'divi_areas_loaded' );
		$this->add_module( 'layouts', 'DAP_Layouts' )->setup_on( 'divi_areas_loaded' );
		$this->add_module( 'library', 'DAP_Library' )->setup_on( 'divi_areas_loaded' );
		$this->add_module( 'editor', 'DAP_Editor' )->setup_on( 'divi_areas_loaded' );
		$this->add_module( 'admin', 'DAP_Admin' )->setup_on( 'divi_areas_loaded' );
		$this->add_module( 'front', 'DAP_Front' )->setup_on( 'divi_areas_loaded' );
		$this->add_module( 'updater', 'DAP_Updater' )->setup_on( 'divi_areas_loaded' );

		/**
		 * Initialize dependencies.
		 *
		 * @since 1.2.3
		 */
		do_action( 'divi_areas_loaded' );
	}

	/**
	 * Defines missing constants with default values.
	 *
	 * @since 2.0.0
	 * @return void
	 */
	private function init_constants() {
		if ( ! defined( 'DIVI_AREAS_CPT' ) ) {
			define( 'DIVI_AREAS_CPT', 'divi-area' );
		}

		if ( ! defined( 'DIVI_AREAS_LAYOUT_CPT' ) ) {
			define( 'DIVI_AREAS_LAYOUT_CPT', 'divi-area-layout' );
		}

		if ( ! defined( 'DIVI_AREAS_TAX_CATEGORY' ) ) {
			define( 'DIVI_AREAS_TAX_CATEGORY', 'divi-area-category' );
		}

		if ( ! defined( 'DIVI_AREAS_LAYOUT_TAX_CATEGORY' ) ) {
			define( 'DIVI_AREAS_LAYOUT_TAX_CATEGORY', 'divi-area-layout-category' );
		}

		if ( ! defined( 'DIVI_AREAS_CACHE_GROUP' ) ) {
			define( 'DIVI_AREAS_CACHE_GROUP', 'dap_cache' );
		}

		if ( ! defined( 'DIVI_AREAS_ID_PREFIX' ) ) {
			define( 'DIVI_AREAS_ID_PREFIX', 'divi-area-' );
		}

		if ( ! defined( 'DIVI_AREAS_ADMIN_CAPABILITY' ) ) {
			define( 'DIVI_AREAS_ADMIN_CAPABILITY', 'manage_options' );
		}

		// Define the origins for the GreenSock Animation Platform (comma separated).
		if ( ! defined( 'DIVI_AREAS_GSAP_URLS' ) ) {
			define( 'DIVI_AREAS_GSAP_URLS', '//cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/TimelineMax.min.js,//cdnjs.cloudflare.com/ajax/libs/gsap/2.1.3/TweenMax.min.js' );
		}

		if ( ! defined( 'DIVIMODE_DEBUG' ) ) {
			define( 'DIVIMODE_DEBUG', false );
		}
	}

	/**
	 * Initialize the shared Divimode UI library which handles common tasks
	 *
	 * @since 1.1.0
	 * @since 2.2.0 fires during `init` action instead of `plugins_loaded`.
	 * @return void
	 */
	public function init_divimode_library() {
		$db_prefix     = '_da_';
		$shared_slug   = 'divi-areas';
		$settings_slug = $db_prefix . 'setting';

		// Translate the plugin.
		load_plugin_textdomain(
			'divi_areas',
			false,
			dirname( DIVI_AREAS_PLUGIN ) . '/lang/'
		);

		$this->divimode_library()->config(
			[
				'version' => DIVI_AREAS_VERSION,
				'lib_url' => DIVI_AREAS_URL . 'shared/divimode',
			]
		);

		add_filter(
			"divimode_load_options_{$settings_slug}",
			[ $this, 'init_options' ]
		);

		// Initialize the settings library.
		$this->settings_library()->configure(
			$db_prefix,
			$shared_slug,
			'Divi Areas Pro',
			__( 'Settings', 'divi_areas' ),
			DIVI_AREAS_CPT,
			DIVI_AREAS_PLUGIN,
			DIVI_AREAS_VERSION,
			DIVI_AREAS_STORE_URL,
			DIVI_AREAS_ADMIN_CAPABILITY
		);

		// Initialize the plugin updater (EDD). Not used for the Divi Marketplace.
		if ( false === strpos( DIVI_AREAS_STORE_URL, 'elegantthemes' ) ) {
			$this->settings_library()->license(
				DIVI_AREAS_SL_STORE_URL,
				DIVI_AREAS_SL_ITEM_ID,
				DIVI_AREAS_PLUGIN_FILE,
				DIVI_AREAS_VERSION,
				'Philipp Stracker'
			);
		}

		do_action( 'divi_areas_localize_admin_js', $this->settings_library() );
	}

	/**
	 * Adds translations to the plugin localization object.
	 *
	 * New translations can be added by calling the localize() function of the
	 * settings-library instance. The localization result is available via the
	 * JavaScript variable `window.dm_config.i18n`.
	 *
	 * Note: Loading the plugin translations should not be done during plugins_loaded
	 * action since that is too early and prevent other language related plugins from
	 * correctly hooking up with load_textdomain() function. Calling
	 * load_plugin_textdomain() should be delayed until init action.
	 *
	 * @since 2.1.0
	 *
	 * @param DM_Settings $library The settings library instance.
	 *
	 * @return void
	 */
	public function localize_admin_js( DM_Settings $library ) {
		$library->localize(
			[
				'settings_link'    => __( 'Settings', 'divi_areas' ),
				'goto_settings'    => __( 'Go to plugin settings', 'divi_areas' ),
				'save_changes'     => __( 'Save Changes', 'divi_areas' ),
				'unknown_error'    => __( 'An error occurred, please try again.', 'divi_areas' ),
				'no_permission'    => __( 'You do not have permission to access this page.', 'divi_areas' ),
				'license_active'   => __( 'Active License', 'divi_areas' ),
				'license_inactive' => __( 'Inactive License', 'divi_areas' ),
				'no_license'       => __( 'No License', 'divi_areas' ),
				// translators: Placeholder shows the expiration date.
				'status_expired'   => __( 'Your license key expired on %s.', 'divi_areas' ),
				'status_disabled'  => __( 'Your license key has been disabled.', 'divi_areas' ),
				'status_missing'   => __( 'Invalid license key.', 'divi_areas' ),
				'status_invalid'   => __( 'Your license is not active for this URL.', 'divi_areas' ),
				// translators: Placeholder displays the plugin name.
				'status_mismatch'  => __( 'This is no valid license key for %s.', 'divi_areas' ),
				'status_limit'     => __( 'Your license key has reached its activation limit.', 'divi_areas' ),
				// translators: Placeholder displays the plugin name.
				'status_okay'      => __( 'Thank you for purchasing %s!', 'divi_areas' ),
				// translators: Placeholder displays the plugin name.
				'unlicensed_title' => __( '%s is unlicensed!', 'divi_areas' ),
				'unlicensed_text'  => __( 'Please enter your license key to enable automatic updates.', 'divi_areas' ),
				'license_remind'   => __( 'Divi Areas Pro is unlicensed. Please remember to enter your license key in the plugin settings to enable automatic updates.', 'divi_areas' ),
				'read_more'        => __( 'Read more...', 'divi_areas' ),

				// Itemlist component.
				'use_on'           => __( 'Use On', 'divi_areas' ),
				'exclude_from'     => __( 'Exclude From', 'divi_areas' ),
				'no_results'       => __( 'No results found.', 'divi_areas' ),
				'add_new'          => __( 'Add New', 'divi_areas' ),
				'remove_this_item' => __( 'Remove this item?', 'divi_areas' ),
				'remove_item'      => __( 'Remove', 'divi_areas' ),
				'cancel_remove'    => __( 'Cancel', 'divi_areas' ),
			]
		);
	}

	/**
	 * Registers custom UI components that are used inside the admin metabox. This
	 * action hook is called by the divimode library.
	 *
	 * ```
	 * $library->register_component( $component_slug, $source_path, $source_url );
	 * ```
	 *
	 * @action divimode_register_components
	 *
	 * @since  2.0.0
	 *
	 * @param DM_Library $library The DM_Library instance which triggered the action.
	 *
	 * @return void
	 */
	public function register_components( DM_Library $library ) {
		// No custom components yet.
	}

	/**
	 * Initialize missing plugin options (e.g. on first installation).
	 *
	 * @since  1.2.1
	 *
	 * @param array $options Plugin options loaded from DB.
	 *
	 * @return array Initialized plugin options.
	 */
	public function init_options( $options ) {
		$defaults = [
			'start_in_preview' => 'on',
			'init_divi_editor' => 'on',
			'enable_support'   => 'on',
			'menu_pos'         => '5',
			'enable_layouts'   => 'on',
			'debug_mode'       => 'auto',
			'script_debug'     => 'auto',
			'use_cache'        => 'on',
		];

		return wp_parse_args( $options, $defaults );
	}

	/**
	 * Returns the state of the "script_debug" flag.
	 *
	 * @since 2.0.0
	 *
	 * @param mixed $context Optional. Some variable that is passed to the filter
	 *                       `divi_areas_output_unminified_js`.
	 *
	 * @return bool True, when the JS should be unminified. The production setting
	 *              should return false.
	 */
	public static function flag_unminified_js( $context ) {
		$script_debug = self::get_option( 'script_debug' );
		if ( '' === $script_debug || 'auto' === $script_debug ) {
			$script_debug = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG;
		} else {
			$script_debug = 'on' === $script_debug;
		}

		/**
		 * Determine, if the JS snippet should be minified.
		 *
		 * @since 1.0.0
		 *
		 * @param bool  $script_debug True means, the snippet will stay un-minified.
		 * @param mixed $context      Optional argument to add some context.
		 */
		return apply_filters(
			'divi_areas_output_unminified_js',
			$script_debug,
			$context
		);
	}

	/**
	 * Returns the caching state (true/false).
	 *
	 * @since 2.0.1
	 * @return bool True when caching is enabled.
	 */
	public static function flag_use_cache() {
		$result = 'on' === self::get_option( 'use_cache' );

		/**
		 * Filter the final flag. When the filter returns a truthy value, caching is
		 * enabled and several Area-states are saved in the sites option table.
		 *
		 * @since 2.0.2
		 *
		 * @param bool $use_cache Whether to enable the plugins caching mechanism.
		 */
		return apply_filters( 'divi_areas_use_cache', $result );
	}
}
