<?php
/**
 * Adds Divi Areas support to the Divi Template Library.
 *
 * @package Divi_Areas_Pro
 */

/**
 * Divi Library support.
 */
class DAP_Library extends DAP_Component {
	/**
	 * Called during the "plugins_loaded" action. Hooks up the admin menu, etc.
	 *
	 * @since  1.0.0
	 * @return void
	 */
	public function setup() {
		add_action( 'init', [ $this, 'register_nonces' ] );
		add_action( 'admin_init', [ $this, 'admin_init' ] );
		add_action( 'admin_menu', [ $this, 'admin_menu' ] );
		add_action( 'divi_areas_localize_admin_js', [ $this, 'localize_admin_js' ] );

		add_action(
			'et_fb_enqueue_assets',
			[ $this->module( 'asset' ), 'js_fb_config' ]
		);

		// Customize the "Saved Layouts" response to mark Divi Areas.
		add_filter(
			'et_builder_library_saved_layouts',
			[ $this, 'customize_divi_area_layouts' ]
		);
	}

	/**
	 * Adds translations to the plugin localization object.
	 *
	 * New translations can be added by calling the localize() function of the
	 * settings-library instance. The localization result is available via the
	 * JavaScript variable `window.dm_config.i18n`.
	 *
	 * @since 2.1.0
	 * @param \DiviAreasPro\DM_Settings $library The settings library instance.
	 * @return void
	 */
	public function localize_admin_js( \DiviAreasPro\DM_Settings $library ) {
		$library->localize(
			[
				'import_scope_question'  => __( 'Do you want to replace the Divi Area configuration with the settings from the import file, or only import the Area layout?', 'divi_areas' ),
				'divi_area_config_found' => __( 'Divi Area Configuration Found', 'divi_areas' ),
				'divi_area_config'       => __( 'Divi Area Configuration', 'divi_areas' ),
				'replace_configuration'  => __( 'Replace Configuration', 'divi_areas' ),
				'only_import_content'    => __( 'Only Import Content', 'divi_areas' ),

			]
		);
	}

	/**
	 * Register nonces for custom template library functionality.
	 *
	 * @since 2.1.0
	 * @return void
	 */
	public function register_nonces() {
		self::settings_library()->add_nonce( 'dap_save_to_library' );
		self::settings_library()->add_nonce( 'ajax_load_from_library' );
	}

	/**
	 * Integrate Divi Areas into the Divi Template Library.
	 *
	 * @since  1.0.0
	 * @return void
	 */
	public function admin_init() {
		if ( ! defined( 'ET_BUILDER_LAYOUT_POST_TYPE' ) ) {
			return;
		}

		$post_type = ET_BUILDER_LAYOUT_POST_TYPE;

		add_filter(
			'et_pb_submit_layout_args',
			[ $this, 'maybe_save_area_to_library' ]
		);

		add_action(
			"add_meta_boxes_{$post_type}",
			[ $this, 'add_meta_boxes' ],
			99
		);

		self::divimode_library()->ajax_add_handler(
			'dap_load_from_library',
			[ $this, 'ajax_load_from_library' ]
		);

		$this->add_layout_pack();
	}

	/**
	 * This filter is called by the Divi Library before anything is saved to the DB.
	 * We check the template type and hook up other functions, so we can save details
	 * from the Divi Area metabox to the template library.
	 *
	 * @param array $args Layout arguments.
	 * @return array Layout arguments.
	 */
	public function maybe_save_area_to_library( $args ) {
		if ( 'layout' === $args['layout_type'] ) {
			$post_type = ET_BUILDER_LAYOUT_POST_TYPE;

			add_action(
				"save_post_{$post_type}",
				[ $this, 'save_to_library' ],
				10,
				3
			);
		}

		return $args;
	}

	/**
	 * Create the layout pack "Divi Areas" when it does not exist.
	 *
	 * @since  1.0.0
	 * @return void
	 */
	private function add_layout_pack() {
		$term = get_term_by( 'slug', 'divi_area', 'layout_pack' );

		if ( ! $term ) {
			$args = [
				'slug'   => 'divi_area',
				'parent' => 0,
			];

			wp_insert_term( 'Divi Areas', 'layout_pack', $args );
		}
	}

	/**
	 * Custom action when Divi Area entry is saved to the Divi Library.
	 *
	 * @since 1.0.0
	 * @param int     $post_id The newly created post_id.
	 * @param WP_Post $post The post object.
	 * @param bool    $update Whether the library entry was created or updated.
	 */
	public function save_to_library( $post_id, $post, $update ) {
		// Add the new Divi Area template to the "Divi Area" layout pack.
		wp_add_object_terms( $post_id, 'divi_area', 'layout_pack' );

		// We use our own nonce-verification, which is not recognized by phpcs.
		// phpcs:disable WordPress.Security.NonceVerification.Missing
		if (
			isset( $_POST['_da_nonce'] )
			&& isset( $_POST['dm_area_data'] )
			&& self::settings_library()->verify_nonce( sanitize_key( $_POST['_da_nonce'] ), 'dap_save_to_library' )
		) {
			// phpcs:disable WordPress.Security.ValidatedSanitizedInput.InputNotSanitized
			// phpcs:disable ET.Sniffs.ValidatedSanitizedInput.InputNotSanitized
			// phpcs:disable WordPress.Security.ValidatedSanitizedInput.MissingUnslash

			/*
			 * The post-data is not sanitized, because the save_metabox_data method
			 * treats it as a JSON string that is parsed with json_decode. Sanitation
			 * is not possible at this point.
			 */
			self::divimode_library()->save_metabox_data(
				'area',
				$_POST['dm_area_data'],
				'post',
				$post_id
			);
			// phpcs:enable
		}
		// phpcs:enable

		/**
		 * After the template was fully saved.
		 *
		 * @since 1.0.0
		 * @param int $post_id The newly created template ID.
		 */
		do_action( 'divi_areas_saved_to_library', $post_id );
	}

	/**
	 * Show the Divi Areas meta boxes when editing a Divi Area inside the
	 * Divi template library.
	 *
	 * @since  1.0.0
	 * @param  WP_Post $post The displayed post.
	 * @return void
	 */
	public function add_meta_boxes( $post ) {
		if ( is_divi_area( $post ) ) {
			$this->module( 'cpt' )->add_cpt_metabox( $post );
		}
	}

	/**
	 * Add a link to the Divi Area templates in the Divi Areas main menu.
	 *
	 * @since  1.0.0
	 * @return void
	 */
	public function admin_menu() {
		$post_type = DIVI_AREAS_CPT;

		/*
		We cannot use the check "post_type_exists()" here, because the
		layout post-type is registered in a later hook.
		The best thing we can do, is to check for the constant.
		*/
		if ( ! defined( 'ET_BUILDER_LAYOUT_POST_TYPE' ) ) {
			return;
		}

		$template_url = admin_url(
			sprintf(
				'/edit.php?post_type=%s&layout_pack=divi_area',
				ET_BUILDER_LAYOUT_POST_TYPE
			)
		);

		// Add the Templates menu-item.
		add_submenu_page(
			"edit.php?post_type={$post_type}",
			__( 'Saved templates', 'divi_areas' ),
			__( 'Saved templates', 'divi_areas' ),
			DIVI_AREAS_ADMIN_CAPABILITY,
			$template_url
		);
	}

	/**
	 * Filter for the "Saved Layouts" ajax response. We check for Divi Areas in the
	 * layout list and give them a custom thumbnail to make it easier for users to
	 * recognize Areas from other layouts.
	 *
	 * @filter et_builder_library_saved_layouts
	 * @see ET_Builder_Library::builder_library_layouts_data()
	 *
	 * @since 2.0.0
	 * @param array $layout_data The Ajax response that was prepared by Divi.
	 * @return array Modified ajax responce data.
	 */
	public function customize_divi_area_layouts( $layout_data ) {
		foreach ( $layout_data['layouts'] as $id => $item ) {
			if ( is_divi_area( $item->id ) ) {
				/*
				 * We cannot change the thumbnail icon of the list item, so we prefix
				 * the Post-Type to the layout name.
				 *
				 * Elegant Themes support did not help, and there is no developer
				 * documentation of the template library for reference..
				 */
				$layout_data['layouts'][ $id ]->name = sprintf(
					'Divi Area | %s',
					$layout_data['layouts'][ $id ]->name
				);
			}
		}

		return $layout_data;
	}

	/**
	 * Ajax handler that returns post-meta data for the given template library item.
	 *
	 * Nonce was already validated by `ajax_add_handler()` when this method is called.
	 *
	 * @since  1.0.0
	 * @return void
	 */
	public function ajax_load_from_library() {
		$response = [];
		$id = isset( $_POST['id'] ) ? (int) $_POST['id'] : 0; // phpcs:ignore

		$response['id'] = $id;

		if ( $id ) {
			$response['area'] = get_post_meta( $id, '_da_area', true );
		}

		wp_send_json_success( $response );
	}
}
