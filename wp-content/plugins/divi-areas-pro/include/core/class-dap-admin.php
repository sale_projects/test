<?php
/**
 * Provides the admin UI and logic to edit divi areas in the wp-admin.
 *
 * @package Divi_Areas_Pro
 */

/**
 * Admin UI.
 */
class DAP_Admin
	extends DAP_Component {
	/**
	 * Called during the "plugins_loaded" action. Hooks up the admin menu, etc.
	 *
	 * @since  0.1.0
	 * @return void
	 */
	public function setup() {
		if ( ! is_admin() ) {
			return;
		}

		add_action( 'init', [ $this, 'register_nonces' ] );
		add_filter( 'admin_body_class', [ $this, 'admin_body_class' ] );
		add_action( 'divimode_ajax_flush_cache', [ $this, 'flush_frontend_cache' ] );

		add_action(
			'admin_enqueue_scripts',
			[ $this, 'enqueue_admin_scripts' ]
		);

		add_action(
			'divimode_settings_admin_footer_divi-areas',
			[ $this->module( 'asset' ), 'js_fb_config' ],
			99999
		);

		add_action(
			'divimode_add_settings_meta_boxes_divi-areas',
			[ $this, 'add_settings_metabox' ],
			99
		);

		add_action(
			'divimode_saved_options_divi-areas_setting',
			[ $this, 'plugin_settings_changed' ]
		);
	}

	/**
	 * Register nonces for custom front-end logic.
	 *
	 * @since 2.1.0
	 * @return void
	 */
	public function register_nonces() {
		self::settings_library()->add_nonce( 'flush_cache' );
	}

	/**
	 * Flushes the front-end cache (e.g. when Caching is disabled in the settings).
	 *
	 * @since 2.1.0
	 * @return void
	 */
	public function flush_frontend_cache() {
		$this->module( 'front' )::clear_all_caches( true );
	}

	/**
	 * Enqueue our custom plugin JS and CSS assets for the admin editor.
	 *
	 * @since  0.1.0
	 *
	 * @param string $hook The current admin page hook name.
	 *
	 * @return void
	 */
	public function enqueue_admin_scripts( $hook ) {
		if ( ! is_divi_area() ) {
			return;
		}

		if ( 'edit.php' === $hook ) {
			wp_enqueue_style(
				'divi-areas',
				DIVI_AREAS_URL . 'css/admin.css',
				[],
				DIVI_AREAS_VERSION
			);
		} elseif ( 'post.php' === $hook || 'post-new.php' === $hook ) {
			/*
			 * The admin-module which interacts with the custom metabox settings and
			 * provides template library integration, etc.
			 */
			wp_enqueue_script(
				'divi-areas',
				DIVI_AREAS_URL . 'js/admin.js',
				[ 'jquery' ],
				DIVI_AREAS_VERSION,
				true
			);

			wp_enqueue_style(
				'divi-areas',
				DIVI_AREAS_URL . 'css/admin.css',
				[],
				DIVI_AREAS_VERSION
			);

			// Loads the shared divimode library for our custom metaboxes.
			$this->divimode_library()->enqueue_lib_assets();

			/*
			 * We need the JS Library in the Admin page because this module
			 * initializes the DiviArea.Hook module for our actions/filters
			 */
			$this->module( 'asset' )->enqueue_js_library();

			// Output the plugin settings (like "start in preview mode").
			$this->module( 'asset' )->js_fb_config();

			$this->maybe_enable_support();
		}
	}

	/**
	 * Registers the custom metabox for the Divi Areas Settings page.
	 *
	 * @since 2.0.0
	 * @return void
	 */
	public function add_settings_metabox() {
		$box = $this->divimode_library()->new_metabox();
		$box
			->set_collapsable( false )
			->set_draggable( false )
			->init(
				'setting',
				__( 'Divi Areas Pro - Settings', 'divi_areas' ),
				'divimode_settings',
				DIVI_AREAS_PATH . 'template',
				DIVI_AREAS_PATH . 'img/plugin-icon.svg'
			);

		$box->add_tool(
			'&#x70;',
			$this->module( 'updater' )->get_welcome_page_url(),
			__( 'About Divi Areas Pro', 'divi_areas' ),
			'small'
		);

		if ( false === strpos( DIVI_AREAS_STORE_URL, 'elegantthemes' ) ) {
			// Check if a valid license is present.
			$license       = self::get_option( 'license_status' );
			$valid_license = isset( $license['license'] ) && 'valid' === $license['license'];

			// When no license/expired license exists, the "Update" tab is the first tab.
			if ( ! $valid_license ) {
				$box->add_tab( 'updates', __( 'Updates', 'divi_areas' ) );
			}

			$box->add_tab( 'editor', __( 'Editor', 'divi_areas' ) );
			$box->add_tab( 'layouts', __( 'Layout Library', 'divi_areas' ) );
			$box->add_tab( 'frontend', __( 'Front-End', 'divi_areas' ) );

			if ( $valid_license ) {
				$box->add_tab( 'updates', __( 'Updates', 'divi_areas' ) );
			}
		} else {
			$box->add_tab( 'editor', __( 'Editor', 'divi_areas' ) );
			$box->add_tab( 'frontend', __( 'Front-End', 'divi_areas' ) );
		}

		$this->maybe_enable_support();
	}

	/**
	 * Add a divi-area class to the admin body when editing a divi area post.
	 *
	 * @since  0.1.0
	 *
	 * @param string $classes Admin body classes.
	 *
	 * @return string
	 */
	public function admin_body_class( $classes ) {
		if ( is_divi_area() ) {
			$classes .= ' post-type-divi-area ';
		}

		return $classes;
	}

	/**
	 * The plugin settings were changed in the admin dashboard.
	 *
	 * In case Caching was disabled we want to clear the plugin cache now.
	 *
	 * @since 2.0.1
	 * @return void
	 */
	public function plugin_settings_changed() {
		if ( 'off' === self::get_option( 'use_cache' ) ) {
			$this->flush_frontend_cache();
		}
	}


	/**
	 * Clears all caches that are related to Divi Areas admin UI.
	 *
	 * @since 2.0.0
	 * @return void
	 */
	public static function clear_all_caches() {
		/**
		 * Clear plugin settings.
		 */
		wp_cache_delete( '_da_setting', 'site-options' );
		wp_cache_delete( '_da_setting', 'options' );
		wp_cache_delete( '_da_setting_license', 'site-options' );
		wp_cache_delete( '_da_setting_license', 'options' );

		// Clear the help-url states.
		delete_transient( 'divimode_url_states' );
	}

	/**
	 * Adds the support beacon to the current admin page when the `enable_support`
	 * flag is enabled in the settings and a valid license exists.
	 *
	 * @since 2.3.1
	 * @return void
	 */
	protected function maybe_enable_support() {
		if ( 'on' !== self::get_option( 'enable_support' ) ) {
			return;
		}

		$identity     = [
			'email'     => '',
			'license'   => false,
			'signature' => '',
		];
		$license_info = [];

		if ( false === strpos( DIVI_AREAS_STORE_URL, 'elegantthemes' ) ) {
			$license             = self::get_option( 'license_status' );
			$identity['license'] = isset( $license['license'] ) && 'valid' === $license['license'];
			$identity['email']   = $license['customer_email'];

			if ( ! empty( $license['license'] ) ) {
				$license_info[] = $license['license'];
			}
			if ( ! empty( $license['expires'] ) ) {
				$license_info[] = substr( $license['expires'], 0, 10 );
			}
			if ( ! empty( $license['payment_id'] ) ) {
				$license_info[] = $license['payment_id'];
			}
		} else {
			$identity['license'] = true;
		}

		$this->divimode_library()->load_helpscout(
			DIVI_AREAS_HS_BEACON,
			$identity,
			[
				'License'        => implode( ' | ', $license_info ),
				'Divi Areas Pro' => DIVI_AREAS_VERSION,
			]
		);
	}
}
