<?php
/**
 * Provides the custom category taxonomy for the Divi Areas.
 *
 * @package Divi_Areas_Pro
 */

/**
 * Custom "category" taxonomy logic.
 *
 * @since 2.0.0
 */
class DAP_Category extends DAP_Component {
	/**
	 * Called during the "plugins_loaded" action. Hooks up the admin menu, etc.
	 *
	 * @since  2.0.0
	 * @return void
	 */
	public function setup() {
		add_action(
			'divi_areas_setup_cpt',
			[ $this, 'register_taxonomy' ],
			1
		);

		add_action(
			'restrict_manage_posts',
			[ $this, 'display_category_filter' ]
		);
	}

	/**
	 * Register the custom "category" taxonomy.
	 *
	 * @since  2.0.0
	 * @param string $post_type The post-type slug of Divi Areas.
	 * @return void
	 */
	public function register_taxonomy( $post_type ) {
		$labels = [
			'name'                  => _x( 'Categories', 'Category Taxonomy', 'divi_areas' ),
			'singular_name'         => _x( 'Category', 'Category Taxonomy', 'divi_areas' ),
			'search_items'          => _x( 'Search Categories', 'Category Taxonomy', 'divi_areas' ),
			'all_items'             => _x( 'All Categories', 'Category Taxonomy', 'divi_areas' ),
			'parent_item'           => _x( 'Parent Category', 'Category Taxonomy', 'divi_areas' ),
			'parent_item_colon'     => _x( 'Parent Category:', 'Category Taxonomy', 'divi_areas' ),
			'edit_item'             => _x( 'Edit Category', 'Category Taxonomy', 'divi_areas' ),
			'view_item'             => _x( 'View Category', 'Category Taxonomy', 'divi_areas' ),
			'update_item'           => _x( 'Update Category', 'Category Taxonomy', 'divi_areas' ),
			'add_new_item'          => _x( 'Add New Category', 'Category Taxonomy', 'divi_areas' ),
			'new_item_name'         => _x( 'New Category Name', 'Category Taxonomy', 'divi_areas' ),
			'not_found'             => _x( 'No categories found', 'Category Taxonomy', 'divi_areas' ),
			'no_terms'              => _x( 'No categories', 'Category Taxonomy', 'divi_areas' ),
			'items_list_navigation' => _x( 'Categories', 'Category Taxonomy', 'divi_areas' ),
			'items_list'            => _x( 'Categories', 'Category Taxonomy', 'divi_areas' ),
			'back_to_items'         => _x( 'Back to All Categories', 'Category Taxonomy', 'divi_areas' ),
		];

		$args = [
			'labels'             => $labels,
			'description'        => _x( 'Categories for Divi Areas', 'Category Taxonomy', 'divi_areas' ),
			'public'             => false,
			'publicly_queryable' => false,
			'hierarchical'       => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'show_in_nav_menus'  => false,
			'show_tagcloud'      => false,
			'show_in_quick_edit' => true,
			'show_admin_column'  => true,
		];

		/**
		 * Filter the taxonomy args, in case someone wants to modify it.
		 *
		 * @since 2.0.0
		 * @var array $args The options passed to register_taxonomy() later.
		 */
		$args = apply_filters( 'divi_areas_register_taxonomy', $args );

		register_taxonomy( DIVI_AREAS_TAX_CATEGORY, $post_type, $args );
	}

	/**
	 * Allow filtering of the Divi Areas list by a given category.
	 *
	 * @since 2.0.0
	 */
	public function display_category_filter() {
		global $typenow;

		if ( DIVI_AREAS_CPT !== $typenow ) {
			return;
		}

		$taxonomy = DIVI_AREAS_TAX_CATEGORY;
		$selected = '';

		if (
			isset( $_GET['_da_tax_nonce'] )
			&& wp_verify_nonce( sanitize_key( $_GET['_da_tax_nonce'] ), 'filter_category' )
			&& isset( $_GET[ $taxonomy ] )
		) {
			/*
			 * Note that sanitize_key() will transform the value to lowercase.
			 * This is okay, because the slug is always lowercase
			 */
			$selected = sanitize_key( $_GET[ $taxonomy ] );
		}

		wp_nonce_field( 'filter_category', '_da_tax_nonce' );
		wp_dropdown_categories(
			[
				'show_option_all' => __( 'Show all Categories', 'divi_areas' ),
				'taxonomy'        => $taxonomy,
				'name'            => $taxonomy,
				'orderby'         => 'name',
				'value_field'     => 'slug',
				'selected'        => $selected,
				'hierarchical'    => true,
				'show_count'      => true,
				'hide_empty'      => true,
			]
		);
	}
}
