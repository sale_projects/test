<?php
/**
 * The contents of the welcome page that is displayed after plugin updates.
 *
 * Page structure and classes borrows code from the wp-admin/about.php template.
 *
 * @since 2.0.0
 * @package Divi_Areas_Pro
 */

$license     = DAP_App::get_option( 'license_status' );
$has_license = isset( $license['license'] ) && 'valid' === $license['license'];
$ver_full    = explode( '.', DIVI_AREAS_VERSION );
$ver_major   = array_shift( $ver_full );
$ver_minor   = array_slice( $ver_full, 0, 4 );
$changelog   = false;

// Read the latest changes from the changelog.md file.
$fs             = divi_areas_get_fs();
$changelog_path = DIVI_AREAS_PATH . 'changelog.txt';
$full_changes   = $fs->get_contents( $changelog_path );

if ( $full_changes ) {
	$version_changes = preg_split( '/^=\s*Version/m', $full_changes, 6 );

	// Remove the first line with the full changelog URL.
	array_shift( $version_changes );

	// Remove the oldest entries.
	array_pop( $version_changes );

	// Convert all version changes into an array.
	foreach ( $version_changes as $ind => $changes ) {
		$changes = str_replace( "\r", "\n", trim( $changes ) );
		$changes = str_replace( "\n\n", "\n", $changes );
		$changes = str_replace( "\n* ", "\n", $changes );

		// Convert the string into an array (each line is one item).
		$changes = explode( "\n", $changes );
		$changes = array_map( 'trim', $changes );
		$changes = array_filter( $changes );

		// Remove the first line with the version number.
		array_shift( $changes );

		$version_changes[ $ind ] = $changes;
	}

	// Assign the prepared changelog to the dedicated variables.
	$changelog     = array_shift( $version_changes );
	$changelog_old = $version_changes;
}

?>
<div class="wrap about-divi-areas-pro about__container">
	<div class="about__dap_header about__group">
		<div class="about__header-title">
			<div class="about__header-logo"></div>
			<p>
				<span class="title"><?php esc_html_e( 'Divi Areas Pro', 'divi_areas' ); ?></span>
				<span class="version major"><?php echo esc_html( $ver_major ); ?></span>
				<span class="version minor">.<?php echo esc_html( implode( '.', $ver_minor ) ); ?></span>
			</p>
			<span class="store-name">
			<?php if ( strpos( DIVI_AREAS_STORE_URL, 'elegantthemes' ) ) : ?>
				<svg viewBox="0 0 28 28">
				<path class="transparent" d="M20.9,6H7.1A1.1,1.1,0,0,0,6,7.1V21.9A1.1,1.1,0,0,0,7.1,23H20.9A1.1,1.1,0,0,0,22,21.9V7.1A1.1,1.1,0,0,0,20.9,6ZM20,21H16V18a2,2,0,0,0-2-2h0a2,2,0,0,0-2,2v3H8V9H20Z"/>
				<path d="M6.67,5a2,2,0,0,0-2,1.64L4,10.5a2.5,2.5,0,0,0,5,0,2.5,2.5,0,0,0,5,0,2.5,2.5,0,0,0,5,0,2.5,2.5,0,0,0,5,0l-.7-3.86a2,2,0,0,0-2-1.64Z"/>
				</svg>
				DIVI MARKETPLACE
			<?php else : ?>
				<svg xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" stroke-linejoin="round" stroke-miterlimit="2" clip-rule="evenodd" viewBox="0 0 571 120"><defs/><path fill="#fff" d="M51.7 52.5h18.1c10 0 18.2 8.1 18.2 18.1 0 10-8.2 18.1-18.2 18.1H54.7a3 3 0 01-3-3V52.5zm18.1-31.3H56.7a5 5 0 00-5 5v26.3H25.5a5 5 0 00-5 5V115a5 5 0 005 5h44.3a49.5 49.5 0 0049.4-49.4 49.5 49.5 0 00-49.4-49.4zM0 26.3a5 5 0 005 5h21.3a5 5 0 005-5V5a5 5 0 00-5-5H5a5 5 0 00-5 5v21.3z"/><g fill="#e6e6e6" fill-rule="nonzero"><path d="M208.6 95.2c0 1.6.4 2 2 2h7.8c1.5 0 2-.4 2-2V46c0-1.6-.5-2-2-2h-7.8c-1.6 0-2 .4-2 2v49zM258.7 96.8c.4.7.7 1.2 1.4 1.2h.4c.7 0 1-.5 1.4-1.2L287.3 46c.6-1.1.2-2-.9-2h-9.2c-1.7 0-2.5.5-3.2 2L260.4 74h-.2l-13.6-28c-.7-1.4-1.5-2-3.2-2h-9.2c-1.1 0-1.5 1-1 2l25.5 50.8zM300.2 95.2c0 1.6.5 2 2 2h7.8c1.6 0 2-.4 2-2V46c0-1.6-.4-2-2-2h-7.8c-1.5 0-2 .4-2 2v49zM342.5 73.2l15 23.1c1 1.3 1.3 1.7 2.2 1.7h.2c.9 0 1.3-.4 2.2-1.7l15-23.1h.2l2.6 22c.2 1.6.5 2 2 2h7.9c1.5 0 2.1-.5 1.9-2l-6.1-50.9c-.1-.7-.5-1-1.2-1h-.2c-.7 0-1.2.3-1.7 1l-22.6 34h-.1l-22.6-34c-.5-.7-1-1-1.7-1h-.3c-.7 0-1 .3-1.1 1l-6.2 50.9c-.2 1.5.5 2 2 2h7.9c1.5 0 1.7-.4 2-2l2.6-22zM405.1 70.6c0 15.5 11.4 27.6 26 27.6s26-12 26-27.6c0-15.5-11.4-27.6-26-27.6s-26 12.1-26 27.6zm12 0c0-8.7 4.9-16.5 14-16.5 9.2 0 14.1 7.8 14.1 16.5s-5 16.5-14 16.5c-9.2 0-14.2-7.8-14.2-16.5zM533 66.6c0-.8.7-1.5 1.6-1.5h32c1.5 0 2 .4 2 2V74c0 1.6-.5 2-2 2h-21.8v10.4h23.4c1.5 0 2 .5 2 2.1v6.8c0 1.6-.5 2-2 2H535c-1.5 0-2-.4-2-2V66.6zm0-20.5c0-1.6.5-2 2-2h32.8c1.5 0 2 .4 2 2v6.7c0 1.6-.5 2-2 2h-23l-10.2.1a1.5 1.5 0 01-1.5-1.5v-7.3zM472 46c0-1.5.3-2 1.9-2h18.4c14.6 0 26 11.1 26 26.6a25.6 25.6 0 01-26 26.6H474c-1.6 0-2-.4-2-2V66.6c0-.8.7-1.5 1.5-1.5h10.3v21.2h8.6c9.2 0 14-7 14-15.7s-4.8-15.7-14-15.7H472v-8.8zM147.4 46c0-1.5.5-2 2-2h18.5c14.6 0 25.9 11.1 25.9 26.6a25.6 25.6 0 01-26 26.6h-18.4c-1.5 0-2-.4-2-2V66.6c0-.8.7-1.5 1.5-1.5h10.3v21.2h8.7c9.1 0 14-7 14-15.7S177 55 167.9 55h-20.5v-8.8z"/></g></svg>
			<?php endif; ?>
			</span>
		</div>
		<div class="about__header-text">
			<p><?php esc_html_e( 'Create Popups, Fly-Ins, Mega Menus, conditional content and more, using a beautiful and flexible UI.', 'divi_areas' ); ?></p>
		</div>
	</div>

	<!-- TODO the intro-video will be added soon
	<div class="about__group">
		<div class="about__section">
			<div class="column">
				<h2 class="is-section-header"><?php esc_html_e( 'Overview of Divi Areas Pro', 'divi_areas' ); ?></h2>
			</div>
		</div>

		<div class="about__section">
			<div class="column is-edge-to-edge">
				<div class="headline-feature feature-video">
					<div class='embed-container'>
						<iframe src='...' frameborder='0'
							allowfullscreen></iframe>
					</div>
				</div>
			</div>
		</div>
	</div>
	--->

	<?php if ( false === strpos( DIVI_AREAS_STORE_URL, 'elegantthemes' ) && ! $has_license ) : ?>
	<div class="about__group">
		<div class="about__section is-alternate-color is-action-row">
			<div class="column license-form">
				<h2 class="is-section-header"><?php esc_html_e( 'Activate Your Plugin', 'divi_areas' ); ?></h2>
				<div class="input-row">
					<input type="text" spellcheck="false" id="license-key" placeholder="<?php esc_attr_e( 'Enter Your License Key', 'divi_areas' ); ?>" />
					<button type="button" class="btn-activate" disabled ><?php esc_html_e( 'Activate', 'divi_areas' ); ?></button>
				</div>
				<p class="form-infos">
					<?php esc_html_e( 'Activate your plugin now to enable automatic updates on this website.', 'divi_areas' ); ?>
				</p>
			</div>
		</div>
	</div>
	<?php endif; ?>

	<div class="about__group">
		<div class="about__section has-2-columns">
			<div class="column is-edge-to-edge">
				<div class="about__image aligncenter">
				<img src="<?php echo esc_url( DIVI_AREAS_URL . '/img/about-admin-menu.png' ); ?>" title="<?php esc_html_e( 'Divi Areas admin menu', 'divi_areas' ); ?>" />
				</div>
			</div>
			<div class="column is-vertically-aligned-center">
				<h3><?php esc_html_e( 'Start Here', 'divi_areas' ); ?></h3>
				<p><?php esc_html_e( 'Create new Divi Areas, group them in categories or change the plugins settings - you find everything you need inside the Divi Areas menu item.', 'divi_areas' ); ?></p>
				<p>
					<a href="<?php echo esc_url( admin_url( '/post-new.php?post_type=' . DIVI_AREAS_CPT ) ); ?>"><?php esc_html_e( 'Create a New Area', 'divi_areas' ); ?></a> |
					<a href="<?php echo esc_url( admin_url( '/edit-tags.php?taxonomy=' . DIVI_AREAS_TAX_CATEGORY . '&post_type=' . DIVI_AREAS_CPT ) ); ?>"><?php esc_html_e( 'Manage Area Categories', 'divi_areas' ); ?></a> |
					<a href="<?php echo esc_url( admin_url( '/edit.php?page=divi-areas-settings&post_type=' . DIVI_AREAS_CPT ) ); ?>"><?php esc_html_e( 'Edit the Settings', 'divi_areas' ); ?></a>
				</p>
			</div>
		</div>
		</div>

	<div class="about__group">
		<div class="about__section">
			<div class="column">
				<?php if ( $changelog ) : ?>
					<h2 class="is-section-header">
						<?php
						printf(
							// translators: The placeholder displays the current plugin version.
							esc_html__( 'What\'s new in version %s?', 'divi_areas' ),
							esc_html( DIVI_AREAS_VERSION )
						);
						?>
					</h2>
					<ul>
						<?php foreach ( $changelog as $item ) : ?>
						<li><?php echo wp_kses_post( $item ); ?></li>
					<?php endforeach ?>
					</ul>

					<?php if ( $changelog_old && isset( $changelog_old[0] ) ) : ?>
						<strong><?php esc_html_e( 'Previous changes', 'divi_areas' ); ?></strong>
						<ul class="changelog-prev">
							<?php $num_prev = 0; ?>
							<?php foreach ( $changelog_old as $level => $items ) : ?>
								<?php foreach ( $items as $item ) : ?>
								<li class="prev-<?php echo (int) $num_prev; ?>"><?php echo wp_kses_post( $item ); ?></li>
									<?php
									if ( ++$num_prev >= 10 ) {
										break 2;
									}
									?>
							<?php endforeach ?>
						<?php endforeach ?>
						</ul>
					<?php endif; ?>
					<a href="https://divimode.com/divi-areas-pro/changelog/" target="_blank"><?php esc_html_e( 'Full Changelog &rarr;', 'divi_areas' ); ?></a>
				<?php else : ?>
					<p style="text-align:center">
						<a href="https://divimode.com/divi-areas-pro/changelog/" target="_blank">
						<?php
						printf(
							// translators: The placeholder displays the current plugin version.
							esc_html__( "See what's new in %s &rarr;", 'divi_areas' ),
							esc_html( DIVI_AREAS_VERSION )
						);
						?>
						</a>
					</p>
				<?php endif; ?>
			</div>
		</div>
	</div>

	<div class="about__group">
		<div class="about__section">
			<div class="column no-bottom-gap">
				<h2 class="is-section-header"><?php esc_html_e( 'Some Feature Highlights', 'divi_areas' ); ?></h2>
			</div>
		</div>

		<div class="about__section has-2-columns">
			<div class="column is-edge-to-edge">
				<div class="about__image aligncenter">
					<img src="<?php echo esc_url( DIVI_AREAS_URL . '/img/about-triggers.png?v2' ); ?>" title="<?php esc_html_e( 'List of available Area Triggers', 'divi_areas' ); ?>" />
				</div>
			</div>
			<div class="column is-vertically-aligned-center">
				<h3><?php esc_html_e( 'Seven Unique Triggers', 'divi_areas' ); ?></h3>
				<p><?php esc_html_e( 'Display an Area at the right time, using any of the built-in triggers.', 'divi_areas' ); ?></p>
				<p><?php esc_html_e( 'It is possible to configure an entire Popup-flow inside the Divi Area editor: Design the contents, choose the pages that will show the Area and assign triggers to show your creation to the world. All on the same page.', 'divi_areas' ); ?></p>
			</div>
		</div>

		<div class="about__section has-2-columns">
			<div class="column is-vertically-aligned-center">
				<h3><?php esc_html_e( 'Conditional logic', 'divi_areas' ); ?></h3>
				<p><?php esc_html_e( 'Define a list of flexible conditions to create powerful, laser targeted campaigns.', 'divi_areas' ); ?></p>
				<p><?php esc_html_e( 'You can check for the current viewport size, URL parameters, define a custom schedule and check which page the visitor came from.', 'divi_areas' ); ?></p>
				<p><?php esc_html_e( 'Additionally, all conditions and triggers are evaluated in the browser or via Ajax. This makes even your most complex Areas 100% compatible with any caching plugin.', 'divi_areas' ); ?></p>
			</div>
			<div class="column is-edge-to-edge">
				<div class="about__image aligncenter">
					<img src="<?php echo esc_url( DIVI_AREAS_URL . '/img/about-conditions.png' ); ?>" title="<?php esc_html_e( 'Use conditions to laser target your Areas.', 'divi_areas' ); ?>" />
				</div>
			</div>
		</div>

		<div class="about__section has-2-columns">
			<div class="column is-edge-to-edge">
				<div class="about__image aligncenter">
					<img src="<?php echo esc_url( DIVI_AREAS_URL . '/img/about-layouts.png' ); ?>" title="<?php esc_html_e( 'Use a pre-built Area layout', 'divi_areas' ); ?>" />
				</div>
			</div>
			<div class="column is-vertically-aligned-center">
				<h3><?php esc_html_e( 'Layout Library', 'divi_areas' ); ?></h3>
				<p><?php esc_html_e( 'Get started quickly by importing any of the pre-built and configured Area layouts.', 'divi_areas' ); ?></p>
				<p><?php esc_html_e( 'We are constantly releasing new layouts for all kind of scenarios to help you get up to speed when designing your website.', 'divi_areas' ); ?></p>
			</div>
		</div>

		<div class="about__section has-2-columns">
			<div class="column is-vertically-aligned-center">
				<h3><?php esc_html_e( 'Seamless Documentation', 'divi_areas' ); ?></h3>
				<p><?php esc_html_e( 'Every control or setting that you find in Divi Areas Pro comes with an inline description to give you additional context and tips. ', 'divi_areas' ); ?></p>
				<p><?php esc_html_e( 'Many controls also offer links to the online Knowledge Base or related tutorials, to provide you with additional guidance for your next project!', 'divi_areas' ); ?></p>
			</div>
			<div class="column is-edge-to-edge">
				<div class="about__image aligncenter">
				<img src="<?php echo esc_url( DIVI_AREAS_URL . '/img/about-inline-help.png' ); ?>" title="<?php esc_html_e( 'Every component is well documented', 'divi_areas' ); ?>" />
				</div>
			</div>
		</div>

		<div class="about__section has-2-columns">
			<div class="column is-edge-to-edge">
				<div class="about__image aligncenter">
				<img src="<?php echo esc_url( DIVI_AREAS_URL . '/img/about-post-selector.png' ); ?>" title="<?php esc_html_e( 'Fine tune your Areas with the new Post Selector', 'divi_areas' ); ?>" />
				</div>
			</div>
			<div class="column is-vertically-aligned-center">
				<h3><?php esc_html_e( 'Advanced Post Selector', 'divi_areas' ); ?></h3>
				<p><?php esc_html_e( 'Precisely control, which pages will load an Area and which don\'t. We have not reinvented the wheel, but use Divis internal functions to generate the Post Selector and to apply the conditions. This makes Divi Areas a perfect match for the new Theme Builder.', 'divi_areas' ); ?></p>
			</div>
		</div>

		<div class="about__section has-2-columns">
			<div class="column is-vertically-aligned-center">
				<h3><?php esc_html_e( 'From Categories to Live Preview', 'divi_areas' ); ?></h3>
				<p><?php esc_html_e( 'Many features that are available in Divi Areas Pro were suggested and motivated by our loyal customers. We listen to user requests and constantly keep improving the plugin.', 'divi_areas' ); ?>
				<p><?php esc_html_e( 'For example the option to create custom categories to organize your Divi Areas. You can filter the admin list by any category, and even categorize Areas using the WordPress quick-edit form.', 'divi_areas' ); ?>
				<p><?php esc_html_e( 'If you have an idea for a new feature, please reach out to us!', 'divi_areas' ); ?>
				</p>
			</div>
			<div class="column is-edge-to-edge">
				<div class="about__image aligncenter">
				<img src="<?php echo esc_url( DIVI_AREAS_URL . '/img/about-categories.png' ); ?>" title="<?php esc_html_e( 'Categorize and filter your Divi Areas.', 'divi_areas' ); ?>" />
				</div>
			</div>
		</div>

		<div class="about__section has-2-columns">
			<div class="column is-edge-to-edge">
				<div class="about__image aligncenter">
					<img src="<?php echo esc_url( DIVI_AREAS_URL . '/img/about-unit-selector.png' ); ?>" title="<?php esc_html_e( 'Quickly switch between units using the new Unit Selector', 'divi_areas' ); ?>" />
				</div>
			</div>
			<div class="column is-vertically-aligned-center">
				<h3><?php esc_html_e( 'An elegant UI and Smart Controls', 'divi_areas' ); ?></h3>
				<p><?php esc_html_e( 'The new user interface comes packed with exciting and intuitive controls. It has never been easier or more fun to set up a Popup. While developing the user interface we did not settle for anything less than perfect.', 'divi_areas' ); ?></p>
				<p><?php esc_html_e( 'Using Divi Areas Pro is fun and intuitive. It feels like Divi and provides controls and elements that you are used to.', 'divi_areas' ); ?></p>
			</div>
		</div>
	</div>

	<div class="about__group">
		<div class="about__section">
			<div class="column no-bottom-gap">
				<h2 class="is-section-header"><?php esc_html_e( 'Developer Friendly', 'divi_areas' ); ?></h2>
			</div>
		</div>
		<div class="about__section">
			<div class="column">
				<p><?php esc_html_e( 'Divi Areas Pro comes with a powerful JavaScript API that allows you to take full control over any aspect of your Popups or other Areas. The API is fully documented in our online Knowledge Base, which also contains many Code samples.', 'divi_areas' ); ?></p>
				<p><?php esc_html_e( 'It is even possible to extend the admin UI by adding new fields to the Divi Area metabox via some filters and even define complete custom controls. Divi Areas Pro is a high-end plugin by developers for developers.', 'divi_areas' ); ?></p>
				<p><a href="https://divimode.com/knowledge-base/" target="_blank"><?php esc_html_e( 'Knowledge Base &rarr;', 'divi_areas' ); ?></a></p>
			</div>
		</div>
	</div>

	<p class="about__bottom_line">
	<?php if ( false === strpos( DIVI_AREAS_STORE_URL, 'elegantthemes' ) ) : ?>
		<a href="https://divimode.com/account/" target="_blank"><?php esc_html_e( 'Account & Live Chat Support &rarr;', 'divi_areas' ); ?></a> |
		<a href="https://divimode.com/get-support/" target="_blank"><?php esc_html_e( 'New Support Ticket &rarr;', 'divi_areas' ); ?></a>
	<?php else : ?>
		<a href="<?php echo esc_url( DIVI_AREAS_STORE_URL ); ?>" target="_blank"><?php esc_html_e( 'Help and Documentation &rarr;', 'divi_areas' ); ?></a>
	<?php endif; ?>
	</p>
</div>

<?php if ( false === strpos( DIVI_AREAS_STORE_URL, 'elegantthemes' ) && ! $has_license ) : ?>
	<script>(function($) {
		var $form = $('.license-form');
		var $h2 = $form.find('h2');
		var $key = $form.find('#license-key');
		var $btn = $form.find('.btn-activate');
		var $info = $form.find('.form-infos');
		var origInfo = $info.html();
		var tmrRevert = 0;
		$key.on('input', function() {$btn.prop('disabled', !$key.val().trim().length)});
		$btn.on('click', function() {
			if ($btn.hasClass('-working')) {
				return false;
			}
			$btn.addClass('-working');
			$key.prop('disabled', true);
			showMsg('...');

			$.ajax({
				method: 'POST',
				url: '<?php echo esc_url( admin_url( 'admin-ajax.php' ) ); ?>',
				data: {
					action: 'divimode_set_license_divi-areas',
					_wpnonce: '<?php echo esc_js( DAP_App::settings_library()->get_nonce( 'divimode_set_license_divi-areas' ) ); ?>',
					license_key: $key.val(),
				}
			}).done(function( response ) {
				if (
					response
					&& response.success
					&& response.data
					&& response.data.msg
				) {
					showMsg(response.data.msg);
					if('valid' === response.data.license) {
						$form.addClass('-is-valid');
						$key.add($btn).add($h2).animate({opacity: 0, marginTop: -40}, 300, function() {
							$h2.hide();
							$key.hide();
							$btn.hide();
						});
					}
				} else {
					showMsg('<?php esc_attr_e( 'Something went wrong. Please reload the page and try again.', 'divi_areas' ); ?>');
				}
				$btn.removeClass('-working');
				$key.prop('disabled', false);
			});
		});
		function showMsg(msg, revert) {
			if (tmrRevert) {
				clearTimeout(tmrRevert);
				tmrRevert = 0;
			}
			$info.animate({opacity:0}, 200, function() {
				$info.html(msg).animate({opacity: 1}, 200, function() {
					if (!revert) {
						tmrRevert = setTimeout(function() {
							if (!$form.hasClass('-is-valid')) {
								showMsg(origInfo, 1);
							}
						}, 3000);
					}
				});
			});
		}
	})(jQuery)</script>
<?php endif; ?>
