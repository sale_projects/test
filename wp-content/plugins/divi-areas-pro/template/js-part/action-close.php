<?php
/**
 * JS template part.
 *
 * Generates required close-functions or listeners.
 *
 * @var DAP_Area $area   The calling area object.
 *
 * @since   2.0.0
 * @package Divi_Areas_Pro
 */

$time_spans = [
	'm' => 1, // Minutes.
	'h' => 60, // Hours.
	'd' => 1440, // Days.
	'w' => 10080, // Weeks.
];

if ( $area->get( 'keep_closed' ) ) {
	$area->add_js_var( 'areaHook', $area->area_hook, 'string' );

	$close_for = divi_areas_parse_number( $area->get( 'close_for' ) );

	/**
	 * The unit-type defines, how the $span value should be interpreted. The following
	 * types are recognized: m (minutes), h (hours), d (days), w (weeks).
	 *
	 * An invalid unit, or a missing unit, will be treated as "minutes".
	 *
	 * @var string
	 */
	$span_type = $close_for['unit'] ? $close_for['unit'][0] : 'm';

	/**
	 * The "close for" duration in minutes. It's calculated as a float value, but the
	 * final output in the JS is always an integer, because the JS API expects full
	 * minutes.
	 *
	 * @var float
	 */
	$span = max( 0, $close_for['value_float'] );

	if ( isset( $time_spans[ $span_type ] ) ) {
		$span *= $time_spans[ $span_type ];
	}

	/**
	 * The "Start Closed Timer" flag defines the JS event that marks the area as
	 * closed. It can be either "on show" which marks the Area as "closed" instantly
	 * upon displaying the Area. Or it is "on hide" (which is the default) that marks
	 * an Area as "closed" when the user actually closes the Area - it does not
	 * matter, how the Area is closed (close button, overlay click, JS API, ...)
	 */
	if ( 'open' === $area->get( 'closed_flag' ) ) {
		$js_filter = 'before_show_area_';
	} else {
		$js_filter = 'before_hide_area_';
	}
	?>

	divi_area_api.addAction('<?php echo esc_html( $js_filter ); ?>' + areaHook, function() {
		divi_area_api.markClosed(area_id, <?php echo (int) $span; ?>);
	});
	<?php
}
