<?php
/**
 * JS template part.
 *
 * Adds the "on tab focus" trigger logic.
 *
 * @var DAP_Area $area    The calling area object.
 * @var array    $triggers {
 *     Array of trigger details. Each array item has the folling attributes:
 *
 *     @type string trigger_type
 *     @type string trigger_selector
 *     @type string trigger_delay
 *     @type string trigger_distance
 *     @type string trigger_param
 *     @type string label
 *     @type string trigger_device
 * }
 *
 * @since   2.0.0
 * @package Divi_Areas_Pro
 */

$callers = [];

foreach ( $triggers as $i => $trigger ) {
	$cond = $area->prepare_js_trigger_conditions( $trigger, ', ' );
	$area->add_js_var(
		"focus_trigger_$i",
		[
			'l' => 8,
		]
	);

	$callers[] = "\t\tthe_area[register_trigger]( focus_trigger_$i $cond );";
}

echo et_core_intentionally_unescaped( implode( "\n", $callers ), 'react_jsx' );
