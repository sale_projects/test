<?php
/**
 * JS template part.
 *
 * Adds the "on scroll distance" trigger logic.
 *
 * @var DAP_Area $area    The calling area object.
 * @var array    $triggers {
 *     Array of trigger details. Each array item has the folling attributes:
 *
 *     @type string trigger_type
 *     @type string trigger_selector
 *     @type string trigger_delay
 *     @type string trigger_distance
 *     @type string trigger_param
 *     @type string label
 *     @type string trigger_device
 * }
 *
 * @since   2.0.0
 * @package Divi_Areas_Pro
 */

$callers = [];

foreach ( $triggers as $i => $trigger ) {
	$cond = $area->prepare_js_trigger_conditions( $trigger, ', ' );

	if ( 'element' === $trigger['trigger_scroll_type'] ) {
		$selectors = $area->parse_trigger_selectors( $trigger );

		foreach ( $selectors as $selector ) {
			$area->add_js_var(
				"scroll_trigger_$i",
				[
					'l' => 6,
					's' => $selector,
				]
			);

			$callers[] = "\t\tthe_area[register_trigger]( scroll_trigger_$i $cond );";
		}
	} else {
		$distance = divi_areas_parse_number( $trigger['trigger_distance'] );

		if ( '%' === $distance['unit'] ) {
			$area->add_js_var(
				"scroll_trigger_$i",
				[
					'l' => 4,
					'd' => number_format( $distance['value_float'] / 100, 4, '.', '' ),
				]
			);

			$callers[] = "\t\tthe_area[register_trigger]( scroll_trigger_$i $cond );";
		} elseif ( $distance['unit'] ) {
			$area->add_js_var(
				"scroll_trigger_$i",
				[
					'l' => 5,
					'd' => number_format( $distance['value_float'], 2, '.', '' ),
					'u' => ( 'px' === $distance['unit'] ? null : $distance['unit'] ),
				]
			);

			$callers[] = "\t\tthe_area[register_trigger]( scroll_trigger_$i $cond );";
		}
	}
}

echo et_core_intentionally_unescaped( implode( "\n\t", $callers ), 'react_jsx' );
