<?php
/**
 * JS template part.
 *
 * Beginning of the "initializeArea" function that registers the Area.
 *
 * @var DAP_Area $area   The calling area object.
 *
 * @since   2.0.0
 * @package Divi_Areas_Pro
 */

$json_config = $area->get_js_flags();

$area->add_js_var( 'area_id', $area->area_id, 'string' );
$area->add_js_var( 'area_config', (object) $json_config, 'json' );
$area->add_js_var( 'register_trigger', 'registerTrigger', 'string' );

$area->add_js_var( 'divi_area_api', 'DiviArea' );
$area->add_js_var( 'the_area' );
$area->add_js_var( 'post_id' );
$area->add_js_var( 'ids' );

?>

divi_area_api.addAction("ready", function() {
	divi_area_api.register( area_id, area_config, function(the_area) {
