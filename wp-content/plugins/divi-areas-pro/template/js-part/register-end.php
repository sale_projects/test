<?php
/**
 * JS template part.
 *
 * End of the "initializeArea" function that registers the Area.
 *
 * @var DAP_Area $area   The calling area object.
 *
 * @since   2.0.0
 * @package Divi_Areas_Pro
 */

$cond = $area->prepare_js_trigger_conditions( false, ', ' );
$area->add_js_var( 'hash_trigger', [ 'l' => 9 ] );

?>
		the_area[register_trigger]( hash_trigger <?php echo esc_js( $cond ); ?> );

	});
}, 2);
