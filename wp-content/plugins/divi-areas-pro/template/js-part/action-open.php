<?php
/**
 * JS template part.
 *
 * Generates required opener functions or listeners.
 *
 * @var DAP_Area $area   The calling area object.
 *
 * @since   2.0.0
 * @package Divi_Areas_Pro
 */

// One-Time opener (e.g. timed, or on scroll).
if ( $area->has_js_var( 'areaOpenOnce' ) ) {
	$area->add_js_var( 'stayClosed', 0, 'number' );
	$area->add_js_var( 'areaOpen' );
	?>

	function areaOpenOnce(openRules) {
		return ! stayClosed && areaOpen(0,openRules)
	}
	<?php
}

// Generic opener (e.g. on click).
if ( $area->has_js_var( 'areaOpen' ) ) {
	$area->add_js_var( 'openEvent' );
	$area->add_js_var( 'openRules' );

	$open_actions = [];

	if ( $area->has_js_var( 'viewportCondition' ) ) {
		$open_actions[] = '(!openRules || divi_area_api.Utils.showOnViewport(openRules.devices))';
	}

	// Add a condition BEFORE calling divi_area_api.show().
	if ( $area->get( 'keep_closed' ) ) {
		$open_actions[] = '! DiviArea.isClosed(area_id)';
	}

	// Make the Area visible.
	$open_actions[] = 'divi_area_api.show(area_id, openEvent)';

	// Do stuff AFTER an Area was opened.
	if ( $area->has_js_var( 'areaOpenOnce' ) ) {
		$open_actions[] = '(stayClosed = 1)';
	}

	// In the end, always return false.
	$open_actions[] = 'false';

	$opener = implode( ' && ', $open_actions );
	?>

	function areaOpen(openEvent, openRules) {
		return <?php echo et_core_intentionally_unescaped( $opener, 'fixed_string' ); ?>;
	}
	<?php
}
