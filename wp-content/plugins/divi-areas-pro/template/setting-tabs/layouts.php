<?php
/**
 * Content of the "Editor" tab on the settings page.
 *
 * @since   0.2.0
 * @package Divi_Areas_Pro
 */

$is_busy       = DAP_Layouts::sync_busy();
$post_count    = wp_count_posts( DIVI_AREAS_LAYOUT_CPT );
$count_layouts = array_sum( (array) $post_count );

$form = [
	'section_layouts' => [
		'label' => false,
		'type'  => 'section',
		'items' => [
			'layouts_intro'      => [
				'label' => __( 'What is the layout library?', 'divi_areas' ),
				'type'  => 'html',
				'html'  => sprintf(
					'<p>%s</p><p>%s</p><p>%s</p>',
					__( 'The Layout Library provides you with a set of pre-built Divi Areas for different purposes. Those layouts can be imported via the Template Library in Divi, and are saved in your WordPress database.', 'divi_areas' ),
					__( 'If you do not want to import the Layout Library into your database, or simply do not want to use those templates any longer, then you can disable this feature. When disabled, any existing templates are removed from your database (but your Divi Areas stay unchanged!). Also, the plugin does not attempt to import new templates.', 'divi_areas' ),
					__( 'This Layout Library is only available when editing a Divi Area, it is not displayed when editing pages or posts.', 'divi_areas' )
				),
			],
			'enable_layouts'     => [
				'label'       => __( 'Enable the Layout Libray', 'divi_areas' ),
				'description' => [
					__( 'When enabled, you will see a new tab called "Divi Areas" in the Template Library. All available layout templates are imported into your database.', 'divi_areas' ),
					__( 'When disabled, any imported layout templates are removed again from your database and the "Divi Areas" tab disappears from the Template Library.', 'divi_areas' ),
				],
				'type'        => 'yes_no_button',
				'default'     => 'on',
				'options'     => [
					'on'  => __( 'Yes', 'divi_areas' ),
					'off' => __( 'No', 'divi_areas' ),
				],
				'hints'       => [
					'on'  => __( 'Import templates and show the "Divi Areas" tab', 'divi_areas' ),
					'off' => __( 'Remove all imported layout templates, do not show the "Divi Areas" tab', 'divi_areas' ),
				],
			],
			'refresh_layouts'    => [
				'label'       => __( 'Refresh Layout Library', 'divi_areas' ),
				'type'        => 'html',
				'description' => __( 'When you have to restore your WordPress database from a backup, or made manual changes in the database, your Layout Library might get outdated or corrupted. In that case, click the button below to re-import the latest Layout templates to this website.', 'divi_areas' ),
				'html'        => '',
				'condition'   => [ 'enable_layouts', '=', 'on' ],
			],
			'layout_preview_on'  => [
				'label'     => __( 'Preview', 'divi_areas' ),
				'type'      => 'html',
				'html'      => sprintf(
					'<img src="%s" width="800" />',
					esc_url( DIVI_AREAS_URL . '/img/layout-library-tab-on.png' )
				),
				'condition' => [ 'enable_layouts', '=', 'on' ],
			],
			'layout_preview_off' => [
				'label'     => __( 'Preview', 'divi_areas' ),
				'type'      => 'html',
				'html'      => sprintf(
					'<img src="%s" width="800" />',
					esc_url( DIVI_AREAS_URL . '/img/layout-library-tab-off.png' )
				),
				'condition' => [ 'enable_layouts', '=', 'off' ],
			],
		],
	],
];

if ( 'on' !== DAP_App::get_option( 'enable_layouts' ) ) {
	unset( $form['section_layouts']['items']['refresh_layouts'] );
} else {
	if ( $is_busy ) {
		$refresh_html = sprintf(
			'<em>%s</em>',
			__( 'Layouts are currently refreshing ...', 'divi_areas' )
		);
	} else {
		$refresh_html = sprintf(
			'<p class="layout-count-notice">%s</p><a href="%s" class="dm-button">%s</a>',
			1 === $count_layouts
			? __( '<strong>1 Layout</strong> available.', 'divi_areas' )
			: sprintf(
				// translators: Placeholder is the number of available templates.
				__( '<strong>%d Layouts</strong> available.', 'divi_areas' ),
				$count_layouts
			),
			DAP_App::settings_library()->nonce_url( 'reset_layouts' ),
			esc_html__( 'Refresh Layouts', 'divi_areas' )
		);
	}

	$form['section_layouts']['items']['refresh_layouts']['html'] = $refresh_html;
}

return $form;
