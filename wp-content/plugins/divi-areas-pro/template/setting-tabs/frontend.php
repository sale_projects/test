<?php
/**
 * Content of the "Front-End" tab on the settings page.
 *
 * @since   0.2.0
 * @package Divi_Areas_Pro
 */

return [
	'section_frontend' => [
		'label' => false,
		'type'  => 'section',
		'items' => [
			'debug_mode'   => [
				'label'       => __( 'Enable Debug output', 'divi_areas' ),
				'description' => [
					__( 'When debug mode is enabled, you will find a lot of helpful information in your browser console.', 'divi_areas' ),
					__( 'Those details help you to understand why a particular area is displayed as it is or behaves in a certain way.', 'divi_areas' ),
					__( 'When you use the "WP_DEBUG" option, then the Divi Areas Pro debugger is automatically enabled, when WP_DEBUG is enabled.', 'divi_areas' ),
				],
				'read_more'   => [
					'https://codex.wordpress.org/WP_DEBUG' => __( 'About WP_DEBUG (on wordpress.org)', 'divi_areas' ),
				],
				'type'        => 'select',
				'default'     => 'auto',
				'options'     => [
					'auto' => sprintf(
						'<span>%s</span><span class="__info">%s</span>',
						__( 'Auto', 'divi_areas' ),
						__( 'Use the WP_DEBUG flag', 'divi_areas' )
					),
					'on'   => sprintf(
						'<span>%s</span><span class="__info">%s</span>',
						__( 'Debugging', 'divi_areas' ),
						__( 'Enable debug mode', 'divi_areas' )
					),
					'off'  => sprintf(
						'<span>%s</span><span class="__info">%s</span>',
						__( 'Production', 'divi_areas' ),
						__( 'Disable debug mode', 'divi_areas' )
					),
				],
			],
			'script_debug' => [
				'label'       => __( 'Output un-minified JS', 'divi_areas' ),
				'description' => [
					__( 'Divi Areas Pro outputs a small JS snippet for every Area that is injected on your page.', 'divi_areas' ),
					__( 'If you enable this option, the plugin will inject the un-minified JS code. This should be enabled only for debugging purposes.', 'divi_areas' ),
					__( 'When you use the "SCRIPT_DEBUG" option, then the Divi Areas Pro debugger is automatically enabled, when SCRIPT_DEBUG is enabled.', 'divi_areas' ),
				],
				'type'        => 'select',
				'default'     => 'auto',
				'options'     => [
					'auto' => sprintf(
						'<span>%s</span><span class="__info">%s</span>',
						__( 'Auto', 'divi_areas' ),
						__( 'Use the SCRIPT_DEBUG flag', 'divi_areas' )
					),
					'on'   => sprintf(
						'<span>%s</span><span class="__info">%s</span>',
						__( 'Debugging', 'divi_areas' ),
						__( 'Output un-minified code', 'divi_areas' )
					),
					'off'  => sprintf(
						'<span>%s</span><span class="__info">%s</span>',
						__( 'Production', 'divi_areas' ),
						__( 'Output minified code', 'divi_areas' )
					),
				],
			],
			'use_cache'    => [
				'label'       => __( 'Divi Area Cache', 'divi_areas' ),
				'description' => [
					__( 'Divi Areas is designed to run super fast. To speed up your page loading times, the plugin caches display states of every Area, i.e., which Area is visible on each page. The caching mechanism automatically refreshes the state when an Area is modified, so usually you do not need to make any changes here.', 'divi_areas' ),
					__( 'However, after you made a direct change in the database, or restored a backup, you might need to manually flush the cache.', 'divi_areas' ),
				],
				'type'        => 'yes_no_button',
				'default'     => 'on',
				'options'     => [
					'on'  => __( 'Yes', 'divi_areas' ),
					'off' => __( 'No', 'divi_areas' ),
				],
				'hints'       => [
					'on'  => __( 'Enable caching for faster page loading', 'divi_areas' ),
					'off' => __( 'Disable caching, flush existing cache', 'divi_areas' ),
				],
			],
			'flush_cache'  => [
				'label'       => __( 'Flush Cache', 'divi_areas' ),
				'type'        => 'html',
				'description' => __( 'When you have to restore your WordPress database from a backup, or made manual changes in the database, the Divi Areas Cache might get outdated and needs to be refreshed. In that case, click the button below to clear and regenerate the cache.', 'divi_areas' ),
				'html'        => sprintf(
					'<a href="%s" class="dm-button">%s</a>',
					DAP_App::settings_library()->nonce_url( 'flush_cache' ),
					esc_html__( 'Clear Cache', 'divi_areas' )
				),
				'condition'   => [ 'use_cache', '=', 'on' ],
			],
		],
	],
];
