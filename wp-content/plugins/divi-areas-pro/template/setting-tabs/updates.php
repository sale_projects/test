<?php
/**
 * Content of the "Updates" tab on the settings page.
 *
 * @since   0.2.0
 * @package Divi_Areas_Pro
 */

$date_format = get_option( 'date_format' );
$time_format = get_option( 'time_format' );
$update_data = DAP_App::settings_library()->get_available_update_infos();

$license_key         = DAP_App::get_option( 'license_key' );
$license_status      = DAP_App::get_option( 'license_status' );
$license_key_present = DAP_App::get_option( 'has_license_key' );
$license_masked      = '';
$html_license_key    = [];
$html_info_updates   = [];
$html_update_actions = [];

$unlicense_url = DAP_App::settings_library()->nonce_url( 'remove_license' );

if ( empty( $license_status['license'] ) || 'valid' !== $license_status['license'] ) {
	$license_is_valid = false;
} else {
	$license_is_valid = true;
}

if ( ! empty( $license_key ) ) {
	$license_masked = sprintf(
		'<span class="license-mask-start">%s</span><span class="license-mask-middle">%s</span><span class="license-mask-end">%s</span>',
		substr( $license_key, 0, 4 ),
		str_repeat( '•', max( 4, strlen( $license_key ) - 8 ) ),
		substr( $license_key, -4 )
	);
}

$dummy_license_key = $license_key_present ? '********' : '';

/* -------------------------------------------------------------------------- */

if ( $license_is_valid ) {
	if ( 'lifetime' === $license_status['expires'] ) {
		$license_valid_until = __( 'This license does not expire.', 'divi_areas' );
	} else {
		$license_valid_until = sprintf(
			// Translators: %s is the localized expiration date.
			esc_html__( 'License is valid until %s', 'divi_areas' ),
			esc_html(
				date_i18n(
					$date_format,
					strtotime( $license_status['expires'] )
				)
			)
		);
	}

	$html_license_key[] = sprintf(
		'<div class="dm-panel-content-text dm-license-info dm-license-valid">
			<i class="dashicons dashicons-yes"></i>
			<strong>%s</strong>
			<div class="dm-license-key">%s</div>
			<div class="dm-license-expiration">%s</div>
		</div>',
		esc_html( $license_status['msg'] ),
		$license_masked,
		$license_valid_until
	);

	$html_license_key[] = sprintf(
		'<a href="%s" class="dm-button dm-unlicense">%s</a>',
		esc_url( $unlicense_url ),
		esc_html__( 'Remove license', 'divi_areas' )
	);
} else {
	$html_license_key[] = '<div>';

	$html_license_key[] = DAP_App::settings_library()->nonce_fields( 'set_license' );

	$html_license_key[] = sprintf(
		'<input type="password" name="license_key" placeholder="%s" value="%s" style="width:320px;display:inline-block" /> ',
		__( 'Enter your license key...', 'divi_areas' ),
		$dummy_license_key
	);

	if ( $license_status && ! empty( $license_status['success'] ) ) {
		$html_license_key[] = '<i class="dashicons dashicons-yes" style="line-height:30px"></i>';
	} elseif ( $license_status && ! empty( $license_status['error'] ) ) {
		$html_license_key[] = '<i class="dashicons dashicons-warning" style="line-height:30px"></i>';
	}
	$html_license_key[] = '</div>';

	if ( $license_status && ! empty( $license_status['msg'] ) ) {
		$html_license_key[] = sprintf(
			'<div class="dm-panel-infos">%s</div>',
			esc_html( $license_status['msg'] )
		);
	}
}

/* -------------------------------------------------------------------------- */

// Display update-available notice, when new version is detected.
$html_info_updates[] = '<div>';

if ( $update_data['available'] ) {
	$html_info_updates[] = '<i class="dashicons dashicons-update"></i> ';
	$html_info_updates[] = sprintf(
	// translators: 1 is the current version, 2 is the latest version number.
		__( 'You have version %1$s installed. Update to %2$s is available', 'divi_areas' ),
		'<strong>' . DIVI_AREAS_VERSION . '</strong>',
		'<strong>' . $update_data['new_version'] . '</strong>'
	);

	if ( current_user_can( 'update_plugins' ) ) {
		if ( $license_is_valid ) {
			$html_update_actions[] = sprintf(
				'<a href="%s" class="dm-button">%s</a>',
				esc_url( $update_data['upgrade_link'] ),
				esc_html__( 'Update now', 'divi_areas' )
			);
		} else {
			$html_info_updates[] = sprintf(
				' | <strong>%s</strong>',
				__( 'Enter a valid license key to install the update', 'divi_areas' )
			);
		}
	}
} else {
	// Display last license check timestamp.
	$html_info_updates[] = '<i class="dashicons dashicons-yes"></i> ';
	$html_info_updates[] = sprintf(
		// translators: placeholder is the latest plugin version.
		__( 'You are using latest version of the Divi Areas Pro: %s', 'divi_areas' ),
		'<strong>' . DIVI_AREAS_VERSION . '</strong>'
	);
}

$html_info_updates[] = '</div>';

// Display last license check timestamp.
$html_info_updates[] = '<div>';
$html_info_updates[] = sprintf(
	// translators: 1 is the date and 2 the time of the last update check.
	__( 'Last checked on %1$s at %2$s', 'divi_areas' ),
	'<strong>' . date_i18n( $date_format, $update_data['last_checked'] ) . '</strong>',
	'<strong>' . date_i18n( $time_format, $update_data['last_checked'] ) . '</strong>'
);

if ( current_user_can( 'update_plugins' ) ) {
	$html_update_actions[] = sprintf(
		'<a href="%s" class="dm-button">%s</a>',
		esc_url( $update_data['recheck_link'] ),
		esc_html__( 'Check again', 'divi_areas' )
	);
}
$html_info_updates[] = '</div>';
$html_info_updates[] = '<div style="margin-top:7px">' . implode( '', $html_update_actions ) . '</div>';

/* -------------------------------------------------------------------------- */

$content = [
	'section_license' => [
		'label'       => __( 'License', 'divi_areas' ),
		'type'        => 'section',
		'collapsable' => false,
		'items'       => [
			'license_key'  => [
				'label'       => __( 'License Key', 'divi_areas' ),
				'description' => [
					__( 'You find the license key in your account on divimode.com', 'divi_areas' ),
					__( 'A valid license key is required to receive plugin updates and priority support.', 'divi_areas' ),
				],
				'read_more'   => [
					'https://divimode.com/account/' => __( 'Your user account', 'divi_areas' ),
					'https://divimode.com/knowledge-base/plugin-update-cannot-be-installed/' => __( 'Plugin updates cannot be installed', 'divi_areas' ),
				],
				'type'        => 'html',
				'html'        => implode( '', $html_license_key ),
			],
			'info_updates' => [
				'label'       => __( 'Updates', 'divi_areas' ),
				'description' => [
					__( 'Here you can see if an update is available and when the plugin did check for updates the last time.', 'divi_areas' ),
					__( 'Use the "Check again now" function to instantly detect new updates.', 'divi_areas' ),
				],
				'type'        => 'html',
				'html'        => implode( '', $html_info_updates ),
			],
		],
	],
];

if ( $update_data['available'] && ! empty( $update_data['changelog'] ) ) {
	$content['section_changelog'] = [
		'label' => __( 'Changelog', 'divi_areas' ),
		'type'  => 'section',
		'items' => [],
	];

	$is_first = true;

	foreach ( $update_data['changelog'] as $entry ) {
		$item = [
			'label' => sprintf(
				// translators: Placeholder is the latest available version number.
				__( "What's new in %s", 'divi_areas' ),
				$entry['version']
			),
			'type'  => 'html',
			'html'  => sprintf(
				'<div class="changelog">%s</div>',
				$entry['text']
			),
		];

		if ( $is_first ) {
			$item['description'] = __( 'This is an overview of all changes that you will get when you update to the latest version.', 'divi_areas' );
			$item['html']       .= sprintf(
				'<div><a href="%s" target="_blank" class="dm-button">%s &rarr;</a></div>',
				'https://divimode.com/divi-areas-pro/changelog/',
				esc_html__( 'Full changelog', 'divi_areas' )
			);
		}

		$is_first                                = false;
		$content['section_changelog']['items'][] = $item;
	}
}

return $content;
