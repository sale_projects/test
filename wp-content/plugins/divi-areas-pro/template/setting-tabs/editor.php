<?php
/**
 * Content of the "Editor" tab on the settings page.
 *
 * @since   0.2.0
 * @package Divi_Areas_Pro
 */

return [
	'section_editor' => [
		'label' => false,
		'type'  => 'section',
		'items' => [
			'start_in_preview' => [
				'label'       => __( 'Start in Preview Mode', 'divi_areas' ),
				'description' => [
					__( 'When enabled, the editor enters in the preview mode for "Desktop View" and moves the Divi Area settings below the editor.', 'divi_areas' ),
					__( 'When disabled, the editor loads in the "Wireframe view" with the Divi Area settings in the right sidebar.', 'divi_areas' ),
				],
				'type'        => 'yes_no_button',
				'default'     => 'on',
				'options'     => [
					'on'  => __( 'Yes', 'divi_areas' ),
					'off' => __( 'No', 'divi_areas' ),
				],
				'hints'       => [
					'on'  => __( 'Automatically enter "Desktop Preview" mode when editing a Divi Area', 'divi_areas' ),
					'off' => __( 'Default WordPress behavior', 'divi_areas' ),
				],
			],
			'init_divi_editor' => [
				'label'       => __( 'Always use Divi Editor', 'divi_areas' ),
				'description' => [
					__( 'Automatically enable the Divi Builder when editing a Divi Areas.', 'divi_areas' ),
					__( 'When disabled, you need to click the "Use The Divi Builder" button every time you create a new Divi Area.', 'divi_areas' ),
				],
				'type'        => 'yes_no_button',
				'default'     => 'on',
				'options'     => [
					'on'  => __( 'Yes', 'divi_areas' ),
					'off' => __( 'No', 'divi_areas' ),
				],
				'hints'       => [
					'on'  => __( 'Automatically enable the Divi Visual Builder for all Divi Areas', 'divi_areas' ),
					'off' => __( 'Default WordPress behavior', 'divi_areas' ),
				],
			],
			'menu_pos'         => [
				'label'             => __( 'Menu Position', 'divi_areas' ),
				'description'       => [
					__( 'Change the position of the Divi Areas menu item. The position is represented by a number which should be greater than 0 (zero). A higher number moves the menu items further down in the admin menu.', 'divi_areas' ),
					__( 'The default position is 5.', 'divi_areas' ),
				],
				'read_more'         => [
					'https://developer.wordpress.org/reference/functions/register_post_type/#menu_position' => __( 'List of default menu positions', 'divi_areas' ),
				],
				'type'              => 'text',
				'default'           => 5,
				'number_validation' => false,
				'multiline'         => false,
			],
			'enable_support'   => [
				'label'       => __( 'Enable Support', 'divi_areas' ),
				'description' => [
					__( 'Enable support to directly access the plugin documentation and support from this website.', 'divi_areas' ),
					__( 'While enabled, you will see a "Help" button in the Divi Area admin menus. That button provides documentation and allows you to contact support directly from your website.', 'divi_areas' ),
					__( 'Privacy note: When you contact support via this website, the following details might be shared with our support team: (1) The current URL you are on. (2) Your WordPress users first name and email address. (3) Details about your Divi Areas Pro license. (4) Current version numbers of WordPress, Divi, Divi Areas Pro.', 'divi_areas' ),
					__( 'You can always ask for support via the "Get Support" page on divimode.com', 'divi_areas' ),
				],
				'read_more'   => [
					'https://divimode.com/get-support/'    => __( 'Get Support on divimode.com', 'divi_areas' ),
					'https://divimode.com/knowledge-base/' => __( 'Knowledge Base on divimode.com', 'divi_areas' ),
				],
				'type'        => 'yes_no_button',
				'default'     => 'on',
				'options'     => [
					'on'  => __( 'Yes', 'divi_areas' ),
					'off' => __( 'No', 'divi_areas' ),
				],
				'hints'       => [
					'on'  => __( 'Load the Helpscout integration for direct support contact', 'divi_areas' ),
					'off' => __( 'Do not load scripts from Helpscout.', 'divi_areas' ),
				],
			],
		],
	],
];
