<?php
/**
 * WordPress template to preview a single Divi Area.
 *
 * This template is used by `DAP_Post_Type::template_for_preview()` via filter
 * `single_template`. It should display a single Area without any other content (no
 * menus, headers, footers, ...).
 *
 * Also, the preview is not 100% accurate, because most Area dimensions are dependant
 * on the CSS of the surrounding page (e.g. "width:auto" or "width:100%" is relative
 * to the parent DOM layout).
 *
 * @since 2.0.0
 * @package Divi_Areas_Pro
 */

$area = DAP_Area::get_area( get_the_ID() );

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<?php
	elegant_description();
	elegant_keywords();
	elegant_canonical();

	/**
	 * Fires in the head, before {@see wp_head()} is called. This action can be used to
	 * insert elements into the beginning of the head before any styles or scripts.
	 *
	 * @since 1.0
	 */
	do_action( 'et_head_meta' );

	$template_directory_uri = get_template_directory_uri();
	?>

	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

	<script type="text/javascript">
		document.documentElement.className = 'js';

	</script>

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
	<div id="page-container">
		<section id="et-boc">
			<div id="et-main-area">

				<article id="post-<?php echo esc_attr( $area->ID ); ?>">
					<div class="entry-content">
						<div class="et-l et-l-post">
						<div id="et-boc">
							<div class="et_builder_inner_content et_pb_gutters3">
								<div
									class="area-outer-wrap popup_outer_wrap et-l entry-content"
									data-da-area="divi-area-<?php echo (int) $area->ID; ?>"
									data-da-registered="1"
								>
								<?php echo et_core_intentionally_unescaped( $area->get_html_code(), 'html' ); ?>
								</div> <!-- .area-outer-wrap -->
							</div> <!-- .et_builder_inner_content -->
						</div> <!-- #et-boc -->
						</div> <!-- .et-l -->
					</div> <!-- .entry-content -->
				</article> <!-- .et_pb_post -->

			</div> <!-- #et-main-area -->
		</section> <!-- #et-boc -->
	</div> <!-- #page-container -->

	<?php wp_footer(); ?>
</body>

</html>
