<?php
/**
 * Content of the "Trigger" metabox for the Divi Areas post type.
 *
 * @since   0.1.0
 * @package Divi_Areas_Pro
 */

$trigger_types = [
	'click'    => __( 'On Click', 'divi_areas' ),
	'hover'    => __( 'On Hover', 'divi_areas' ),
	'time'     => __( 'After Delay', 'divi_areas' ),
	'scroll'   => __( 'On Scroll', 'divi_areas' ),
	'exit'     => __( 'Exit Intent', 'divi_areas' ),
	'inactive' => __( 'On Inactivity', 'divi_areas' ),
	'focus'    => __( 'On Browser Focus', 'divi_areas' ),
];

/**
 * Filter the available trigger types.
 *
 * @since 2.0.0
 * @var   array $trigger_types List of area triggers.
 */
$trigger_types = apply_filters( 'divi_areas_trigger_types', $trigger_types );

$hover_triggers = [
	'click' => __( 'On Click', 'divi_areas' ),
	'hover' => __( 'On Hover', 'divi_areas' ),
];

/**
 * Filter the available hover-trigger types.
 *
 * @since 2.0.0
 * @var   array $trigger_types List of hover triggers.
 */
$hover_triggers = apply_filters( 'divi_areas_hover_trigger_types', $hover_triggers );

/**
 * Returns an array with fields that show all trigger conditions.
 *
 * @since 2.2.0
 *
 * @param string The condition-parent/position.
 *
 * @return array List of trigger conditions.
 */
$generate_conditions = function ( $type ) {
	if ( 'default' === $type ) {
		$suffix           = '';
		$customize_toggle = false;
		$customize_cond   = [];
	} else {
		$suffix           = "_$type";
		$customize_toggle = "cond_custom$suffix";
		$customize_cond   = [ $customize_toggle, '=', 'on' ];
	}

	$result = [
		$customize_toggle             => ( $customize_toggle
			? [
				'label'   => __( 'Use Custom Conditions', 'divi_areas' ),
				'type'    => 'yes_no_button',
				'default' => 'off',
				'options' => [
					'off' => __( 'No', 'divi_areas' ),
					'on'  => __( 'yes', 'divi_areas' ),
				],
			] : null
		),
		"cond_device$suffix"          => [
			'label'       => __( 'On Device', 'divi_areas' ),
			'description' => __( 'This trigger is enabled on the selected device-types.', 'divi_areas' ),
			'type'        => 'switcher',
			'multiple'    => true,
			'default'     => [
				'desktop',
				'tablet',
				'phone',
			],
			'options'     => [
				'desktop' => __( 'Desktop', 'divi_areas' ),
				'tablet'  => __( 'Tablet', 'divi_areas' ),
				'phone'   => __( 'Phone', 'divi_areas' ),
			],
			'condition'   => $customize_cond,
		],
		"cond_use_url_param$suffix"   => [
			'label'       => __( 'By URL Param', 'divi_areas' ),
			'description' => __( 'This trigger is enabled when a certain URL param is present or absent.', 'divi_areas' ),
			'type'        => 'select',
			'default'     => 'off',
			'options'     => [
				'off'     => sprintf(
					'<span>%s</span><span class="__detail">%s</span>',
					__( 'Off', 'divi_areas' ),
					__( 'Do not check the URL Param', 'divi_areas' )
				),
				'present' => sprintf(
					'<span>%s</span><span class="__detail">%s</span>',
					__( 'Show', 'divi_areas' ),
					__( 'Only show with the following URL params', 'divi_areas' )
				),
				'absent'  => sprintf(
					'<span>%s</span><span class="__detail">%s</span>',
					__( 'Hide', 'divi_areas' ),
					__( 'Always hide with the following URL params', 'divi_areas' )
				),
			],
			'condition'   => $customize_cond,
		],
		"cond_url_param$suffix"       => [
			'label'       => __( 'URL Params', 'divi_areas' ),
			'description' => __( 'Define the URL param, which triggers the Area. You can define a param name, such as <code>my-param</code> to trigger the Area when the param is set (regardless of the value). Or you can trigger the Area on a specific param value by defining <code>my-param=value</code>. Separate multiple param rules with a newline.', 'divi_areas' ),
			'type'        => 'text',
			'multiline'   => true,
			'default'     => '',
			'condition'   => [
				[ "cond_use_url_param$suffix", '=', 'present' ],
				[ "cond_use_url_param$suffix", '=', 'absent' ],
			],
		],
		"cond_use_referrer$suffix"    => [
			'label'       => __( 'Arrived from URL', 'divi_areas' ),
			'description' => __( 'Trigger this Area when the visitor arrived at the current page from a specific URL - the so-called "Referrer".', 'divi_areas' ),
			'type'        => 'select',
			'default'     => 'off',
			'options'     => [
				'off'     => sprintf(
					'<span>%s</span><span class="__detail">%s</span>',
					__( 'Off', 'divi_areas' ),
					__( 'Do not check the Referrer ', 'divi_areas' )
				),
				'present' => sprintf(
					'<span>%s</span><span class="__detail">%s</span>',
					__( 'Show', 'divi_areas' ),
					__( 'Only show for the following referrers', 'divi_areas' )
				),
				'absent'  => sprintf(
					'<span>%s</span><span class="__detail">%s</span>',
					__( 'Hide', 'divi_areas' ),
					__( 'Always hide for the following referrers', 'divi_areas' )
				),
			],
			'condition'   => $customize_cond,
		],
		"cond_referrer$suffix"        => [
			'label'       => __( 'Referrer Rules', 'divi_areas' ),
			'description' => __( 'Choose the referrer-rules that should be checked for.', 'divi_areas' ),
			'type'        => 'checkbox',
			'options'     => [
				'none'     => __( 'No Referrer (e.g. direct access, bookmark, email)', 'divi_areas' ),
				'internal' => [
					'label'    => __( 'Any internal page', 'divi_areas' ),
					'disables' => [ 'external' ],
				],
				'external' => [
					'label'    => __( 'Any external page', 'divi_areas' ),
					'disables' => [ 'internal', 'search' ],
				],
				'search'   => [
					'label'    => __( 'A Search Engine', 'divi_areas' ),
					'disables' => [ 'external' ],
				],
				'social'   => [
					'label'    => __( 'Social Media', 'divi_areas' ),
					'disables' => [ 'external' ],
				],
			],
			'condition'   => [
				[ "cond_use_referrer$suffix", '=', 'present' ],
				[ "cond_use_referrer$suffix", '=', 'absent' ],
			],
		],
		"cond_custom_referrer$suffix" => [
			'label'       => __( 'Custom Referrer URLs', 'divi_areas' ),
			'description' => __( 'Define custom referrer rules by entering URLs here; you can also use the <code>*</code> wildcard. For example <code>*google.com*</code>. Separate multiple URLs with a newline.', 'divi_areas' ),
			'type'        => 'text',
			'multiline'   => true,
			'default'     => '',
			'condition'   => [
				[ "cond_use_referrer$suffix", '=', 'present' ],
				[ "cond_use_referrer$suffix", '=', 'absent' ],
			],
		],
		"cond_schedule$suffix"        => [
			'label'       => __( 'Use Schedule', 'divi_areas' ),
			'description' => [
				__( 'Decide, if you want to keep the Area enabled permanently or automatically enable/disable the Area based on certain rules.', 'divi_areas' ),
				__( 'No Scheduling - The Area is not restricted by date or time.', 'divi_areas' ),
				__( 'Specific Dates: Enter a list of start- and end-dates.', 'divi_areas' ),
				__( 'Repeat Weekly: Define start- and end-days that repeat weekly.', 'divi_areas' ),
			],
			'read_more'   => [
				'https://divimode.com/knowledge-base/area-scheduling/' => __( 'Area Scheduling Options', 'divi_areas' ),
			],
			'default'     => 'off',
			'type'        => 'select',
			'options'     => [
				'off'      => sprintf(
					'<span>%s</span><span class="__detail">%s</span>',
					__( 'Off', 'divi_areas' ),
					__( 'Do not use Scheduling', 'divi_areas' )
				),
				'on_date'  => __( 'Enable on Dates', 'divi_areas' ),
				'not_date' => __( 'Disable on Dates', 'divi_areas' ),
				'weekly'   => __( 'Weekly Schedule', 'divi_areas' ),
			],
			'condition'   => $customize_cond,
		],
		"cond_schedule_dates$suffix"  => [
			'label'             => __( 'Specific Dates', 'divi_areas' ),
			'description'       => __( 'Here you can define a list of start- and end-dates when this Area should be displayed.', 'divi_areas' ),
			'type'              => 'itemlist',
			'condition'         => [
				[ "cond_schedule$suffix", '=', 'on_date' ],
				[ "cond_schedule$suffix", '=', 'not_date' ],
			],
			'label_create_item' => __( 'New Date Range', 'divi_areas' ),
			'label_edit_item'   => __( 'Edit Date Range', 'divi_areas' ),
			'label_id'          => "schedule_label$suffix",
			'form'              => [
				'section_schedule_dates' => [
					'label'       => __( 'Date Range Details', 'divi_areas' ),
					'type'        => 'section',
					'collapsable' => false,
					'items'       => [
						"schedule_range$suffix" => [
							'label'       => __( 'Date Range', 'divi_areas' ),
							'description' => __( 'Choose the date range during which the Area should be enabled or disabled.', 'divi_areas' ),
							'type'        => 'timestamp',
							'has_date'    => true,
							'has_time'    => false,
							'time_mode'   => 'range',
							'allow_input' => false, // range-mode + allow-input is buggy.
							'default'     => '',
						],
						"schedule_start$suffix" => [
							'label'       => __( 'Start Time', 'divi_areas' ),
							'description' => __( 'Select the exact time on the start day, in 24-hour notation.', 'divi_areas' ),
							'type'        => 'timestamp',
							'has_date'    => false,
							'has_time'    => true,
							'time_mode'   => 'single',
							'default'     => '00:00:00',
						],
						"schedule_end$suffix"   => [
							'label'       => __( 'End Time', 'divi_areas' ),
							'description' => __( 'Select the exact time on the end day, in 24-hour notation.', 'divi_areas' ),
							'type'        => 'timestamp',
							'has_date'    => false,
							'has_time'    => true,
							'time_mode'   => 'single',
							'default'     => '23:59:59',
						],
						"schedule_label$suffix" => [
							'label'       => __( 'Admin Label', 'divi_areas' ),
							'description' => __( 'An internal name, so you can identify this entry later.', 'divi_areas' ),
							'type'        => 'text',
							'default'     => "{{schedule_range$suffix}}",
						],
					],
				],
			],
		],
		"cond_schedule_weekly$suffix" => [
			'label'             => __( 'Weekly Interval', 'divi_areas' ),
			'description'       => __( 'Create custom rules that repeat every week.', 'divi_areas' ),
			'type'              => 'itemlist',
			'condition'         => [
				[ "cond_schedule$suffix", '=', 'weekly' ],
			],
			'label_create_item' => __( 'New Rule', 'divi_areas' ),
			'label_edit_item'   => __( 'Edit Rule', 'divi_areas' ),
			'label_id'          => "weekly_label$suffix",
			'form'              => [
				'section_schedule_weekly' => [
					'label'       => __( 'Weekly Interval Details', 'divi_areas' ),
					'type'        => 'section',
					'collapsable' => false,
					'items'       => [
						"weekly_days$suffix"  => [
							'label'       => __( 'Show Area on Days', 'divi_areas' ),
							'description' => __( 'Here you can define during which days of the week the Area should be displayed.', 'divi_areas' ),
							'type'        => 'checkbox',
							'options'     => [
								'Su' => __( 'Sunday', 'divi_areas' ),
								'Mo' => __( 'Monday', 'divi_areas' ),
								'Tu' => __( 'Tuesday', 'divi_areas' ),
								'We' => __( 'Wednesday', 'divi_areas' ),
								'Th' => __( 'Thursday', 'divi_areas' ),
								'Fr' => __( 'Friday', 'divi_areas' ),
								'Sa' => __( 'Saturday', 'divi_areas' ),
							],
						],
						"weekly_start$suffix" => [
							'label'       => __( 'Start Time', 'divi_areas' ),
							'description' => __( 'At this time, the Area will become active on each day, in 24-hour notation.', 'divi_areas' ),
							'type'        => 'timestamp',
							'has_date'    => false,
							'has_time'    => true,
							'time_mode'   => 'single',
							'default'     => '00:00:00',
						],
						"weekly_end$suffix"   => [
							'label'       => __( 'End Time', 'divi_areas' ),
							'description' => __( 'At this time, the Area will become inactive on each day, in 24-hour notation.', 'divi_areas' ),
							'type'        => 'timestamp',
							'has_date'    => false,
							'has_time'    => true,
							'time_mode'   => 'single',
							'default'     => '23:59:59',
						],
						"weekly_label$suffix" => [
							'label'       => __( 'Admin Label', 'divi_areas' ),
							'description' => __( 'An internal name, so you can identify this entry later.', 'divi_areas' ),
							'type'        => 'text',
							'default'     => "{{weekly_days$suffix}}",
						],
					],
				],
			],
		],
	];

	$result = array_filter( $result );

	/**
	 * Filter the built-in trigger conditions.
	 *
	 * @since 2.2.0
	 *
	 * @param array  $fields List of condition-fields.
	 * @param string $type   Defines where the conditions are displayed/used for.
	 */
	$result = apply_filters( 'divi_areas_trigger_conditions', $result, $type );

	return $result;
};

$form = [];

$form['section_triggers'] = [
	'label'     => __( 'Automatic Triggers', 'divi_areas' ),
	'type'      => 'section',
	'condition' => [ 'layout_type', '!=', 'hover' ],
	'items'     => [
		'triggers'    => [
			'label'             => __( 'Trigger Rules', 'divi_areas' ),
			'description'       => __( 'Here you can define when this Area should be displayed.', 'divi_areas' ),
			'read_more'         => [
				'https://divimode.com/knowledge-base/divi-area-triggers/' => __( 'Overview of Triggers', 'divi_areas' ),
			],
			'condition'         => [ 'is_static', '!=', 'on' ],
			'type'              => 'itemlist',
			'label_create_item' => __( 'Create Trigger', 'divi_areas' ),
			'label_edit_item'   => __( 'Edit Trigger', 'divi_areas' ),
			'label_item'        => __( '{{trigger_type}} Trigger', 'divi_areas' ),
			'label_id'          => 'trigger_label',
			'form'              => [
				'section_triggers_settings'  => [
					'label'       => __( 'Trigger Details', 'divi_areas' ),
					'type'        => 'section',
					'collapsable' => false,
					'items'       => [
						'trigger_type'        => [
							'label'       => __( 'Trigger Type', 'divi_areas' ),
							'description' => __( 'Define the type of this trigger.', 'divi_areas' ),
							'type'        => 'select',
							'default'     => 'click',
							'options'     => $trigger_types,
						],
						'trigger_scroll_type' => [
							'label'       => __( 'Scroll logic', 'divi_areas' ),
							'description' => __( 'Decide, if you want to trigger the Area after a certain distance was scrolled, or when a specified element becomes visible on the screen.', 'divi_areas' ),
							'type'        => 'switcher',
							'multiple'    => false,
							'default'     => 'distance',
							'options'     => [
								'distance' => __( 'Distance', 'divi_areas' ),
								'element'  => __( 'Element', 'divi_areas' ),
							],
							'condition'   => [ 'trigger_type', '=', 'scroll' ],
						],
						'trigger_selector'    => [
							'label'       => __( 'CSS Selector', 'divi_areas' ),
							'description' => __( 'This selector defines the element (or elements) that trigger the Area.', 'divi_areas' ),
							'type'        => 'text',
							'multiline'   => true,
							'default'     => '',
							'condition'   => [
								[ [ 'trigger_type', '=', 'click' ] ],
								[ [ 'trigger_type', '=', 'hover' ] ],
								[
									[ 'trigger_type', '=', 'scroll' ],
									[ 'trigger_scroll_type', '=', 'element' ],
								],
							],
						],
						'trigger_delay'       => [
							'label'             => __( 'Delay', 'divi_areas' ),
							'description'       => __( 'Define the delay of the Trigger.', 'divi_areas' ),
							'type'              => 'text',
							'responsive'        => false,
							'has_select'        => true,
							'number_validation' => true,
							'zero_string'       => 'instant',
							'numeric_strings'   => [ 'instant' ],
							'numeric_units'     => [ 'sec' ],
							'max_precission'    => 1,
							'limit_min'         => 0,
							'slider_min'        => 0,
							'slider_max'        => 10,
							'default'           => 'instant',
							'condition'         => [
								[ [ 'trigger_type', '=', 'time' ] ],
								[ [ 'trigger_type', '=', 'inactive' ] ],
							],
						],
						'trigger_distance'    => [
							'label'             => __( 'Scroll Distance', 'divi_areas' ),
							'description'       => __( 'Define the distance that needs to be scrolled to trigger the Area.', 'divi_areas' ),
							'type'              => 'text',
							'responsive'        => false,
							'has_select'        => true,
							'number_validation' => true,
							'numeric_strings'   => false,
							'numeric_units'     => true,
							'limit_min'         => 0,
							'slider_min'        => 0,
							'slider_max'        => 100,
							'default'           => '50%',
							'condition'         => [
								[
									[ 'trigger_type', '=', 'scroll' ],
									[ 'trigger_scroll_type', '=', 'distance' ],
								],
							],
						],
						'trigger_label'       => [
							'label'       => __( 'Admin Label', 'divi_areas' ),
							'description' => __( 'Give this trigger an internal name, so you can identify it later.', 'divi_areas' ),
							'type'        => 'text',
							'default'     => __( '{{trigger_type}} Trigger', 'divi_areas' ),
						],
					],
				],
				'section_triggers_condition' => [
					'label' => __( 'Conditions', 'divi_areas' ),
					'type'  => 'section',
					'items' => $generate_conditions( 'area' ),
				],
				'help_automatic_triggers'    => [
					'type'       => 'footer',
					'help_url'   => 'https://divimode.com/knowledge-base/area-automatic-triggers/',
					'help_label' => __( 'Help', 'divi_areas' ),
				],
			],
		],
		'static_info' => [
			'label'     => __( 'Static Area', 'divi_areas' ),
			'condition' => [ 'is_static', '=', 'on' ],
			'type'      => 'html',
			'html'      => sprintf(
				'<em>%s</em><br><em>%s</em>',
				__( 'To use triggers, please make this Area dynamic.', 'divi_areas' ),
				__( 'Go to "Settings > Type" and set "Is Static Area" to "No"', 'divi_areas' )
			),
		],
	],
];

$form['section_triggers_hover'] = [
	'label'     => __( 'Automatic Hover Triggers', 'divi_areas' ),
	'type'      => 'section',
	'condition' => [ 'layout_type', '=', 'hover' ],
	'items'     => [
		'triggers_hover' => [
			'label'             => __( 'Open Triggers', 'divi_areas' ),
			'description'       => __( 'Here you can define when this Area should be displayed.', 'divi_areas' ),
			'read_more'         => [
				'https://divimode.com/knowledge-base/area-triggers/' => __( 'Overview of Triggers', 'divi_areas' ),
			],
			'type'              => 'itemlist',
			'label_create_item' => __( 'Create Trigger', 'divi_areas' ),
			'label_edit_item'   => __( 'Edit Trigger', 'divi_areas' ),
			'label_item'        => __( '{{trigger_type_hover}} Trigger', 'divi_areas' ),
			'label_id'          => 'trigger_label_hover',
			'form'              => [
				'section_triggers_settings_hover'  => [
					'label'       => __( 'Trigger Details', 'divi_areas' ),
					'type'        => 'section',
					'collapsable' => false,
					'items'       => [
						'trigger_type_hover'     => [
							'label'       => __( 'Trigger Type', 'divi_areas' ),
							'description' => __( 'Define the type of this trigger.', 'divi_areas' ),
							'type'        => 'select',
							'default'     => 'click',
							'options'     => $hover_triggers,
						],
						'trigger_selector_hover' => [
							'label'       => __( 'CSS Selector', 'divi_areas' ),
							'description' => __( 'This selector defines the element (or elements) that trigger the Area.', 'divi_areas' ),
							'type'        => 'text',
							'multiline'   => true,
							'default'     => '',
							'condition'   => [
								[ [ 'trigger_type_hover', '=', 'click' ] ],
								[ [ 'trigger_type_hover', '=', 'hover' ] ],
							],
						],
						'trigger_label_hover'    => [
							'label'       => __( 'Admin Label', 'divi_areas' ),
							'description' => __( 'Give this trigger an internal name, so you can identify it later.', 'divi_areas' ),
							'type'        => 'text',
							'default'     => __( '{{trigger_type_hover}} Trigger', 'divi_areas' ),
						],
					],
				],
				'section_triggers_condition_hover' => [
					'label' => __( 'Conditions', 'divi_areas' ),
					'type'  => 'section',
					'items' => $generate_conditions( 'hover' ),
				],
				'help_automatic_hover_triggers'    => [
					'type'       => 'footer',
					'help_url'   => 'https://divimode.com/knowledge-base/area-automatic-hover-triggers/',
					'help_label' => __( 'Help', 'divi_areas' ),
				],
			],
		],
		'close_trigger'  => [
			'label'       => __( 'Close Trigger', 'divi_areas' ),
			'description' => __( 'Decide, if you want to enable an event when the visible Area should be hidden again.', 'divi_areas' ),
			'type'        => 'radio',
			'default'     => 'hover',
			'options'     => [
				'none'  => [
					'label' => __( 'None', 'divi_areas' ),
					'icon'  => DIVI_AREAS_PATH . 'img/event-none.svg',
				],
				'click' => [
					'label' => __( 'Outside Click', 'divi_areas' ),
					'icon'  => DIVI_AREAS_PATH . 'img/event-click.svg',
				],
				'hover' => [
					'label' => __( 'Mouse Leave', 'divi_areas' ),
					'icon'  => DIVI_AREAS_PATH . 'img/event-hover.svg',
				],
			],
			'condition'   => [ 'layout_type', '=', 'hover' ],
		],
		'close_delay'    => [
			'label'             => __( 'Closing Delay', 'divi_areas' ),
			'description'       => __( 'By default, an Area is instantly hidden on the Close-Event (outside click or mouse leave). Here you can define a delay, between the event and hiding the Area.', 'divi_areas' ),
			'type'              => 'text',
			'responsive'        => false,
			'has_select'        => false,
			'number_validation' => true,
			'zero_string'       => 'instant',
			'numeric_strings'   => [ 'instant' ],
			'numeric_units'     => [ 'sec' ],
			'max_precission'    => 1,
			'limit_min'         => 0,
			'slider_min'        => 0,
			'slider_max'        => 10,
			'default'           => 'instant',
			'condition'         => [
				[
					[ 'layout_type', '=', 'hover' ],
					[ 'close_trigger', '!=', 'none' ],
				],
			],
		],
	],
];

/**
 * Filter the metabox tab contents.
 *
 * @since 2.3.0
 *
 * @param array $form The metabox form definition.
 */
$form = apply_filters( 'divi_areas_metabox_area_triggers_after_triggers', $form );

$form['section_trigger_conditions'] = [
	'label'     => __( 'Default Conditions', 'divi_areas' ),
	'type'      => 'section',
	'condition' => [ 'is_static', '!=', 'on' ],
	'items'     => $generate_conditions( 'default' ),
];

/**
 * Filter the metabox tab contents.
 *
 * @since 2.3.0
 *
 * @param array $form The metabox form definition.
 */
$form = apply_filters( 'divi_areas_metabox_area_triggers_after_conditions', $form );

$form['section_trigger_infos'] = [
	'label'     => __( 'Custom Triggers', 'divi_areas' ),
	'type'      => 'section',
	'condition' => [ 'is_static', '!=', 'on' ],
	'items'     => [
		'url_triggers' => [
			'label'       => __( 'Link Triggers', 'divi_areas' ),
			'description' => [
				__( 'Enter a Link Trigger value into any <strong>Link field</strong> in Divi (or use it in an "href" attribute) to turn that element into a <em>Click Trigger</em>.', 'divi_areas' ),
				__( 'Tip: If you also add the <strong>CSS Class</strong> "<code>on-hover</code>" the element it will become a <em>Hover Trigger</em>.', 'divi_areas' ),
			],
			'read_more'   => [
				'https://divimode.com/knowledge-base/how-to-trigger-an-area/' => __( 'Ways to trigger an Area', 'divi_areas' ),
			],
			'type'        => 'html',
			'html'        => sprintf(
				'<div class="code-line"><label>%2$s</label><code>%1$s</code></div>
					<div class="code-line" data-html-need-var="post-name"><label>%4$s</label><code>%3$s</code></div>',
				'<strong>#</strong>' . esc_html( DIVI_AREAS_ID_PREFIX ) . '<strong data-html-var="post-id"></strong>',
				__( 'Link or href:', 'divi_areas' ),
				'<strong>#</strong>' . esc_html( DIVI_AREAS_ID_PREFIX ) . '<strong data-html-var="post-name"></strong>',
				__( 'Link or href:', 'divi_areas' )
			),
		],
		'css_triggers' => [
			'label'       => __( 'CSS Triggers', 'divi_areas' ),
			'description' => __( 'Turn any element into a <em>Click Trigger</em> or a <em>Hover Trigger</em> for this Area by adding the specified <strong>CSS Classes</strong> to the element.', 'divi_areas' ),
			'read_more'   => [
				'https://divimode.com/knowledge-base/how-to-trigger-an-area/' => __( 'Ways to trigger an Area', 'divi_areas' ),
			],
			'type'        => 'html',
			'html'        => sprintf(
				'<div class="code-line"><label>%2$s</label><code>%1$s on-<strong>click</strong></code></div>
					<div class="code-line"><label>%3$s</label><code>%1$s on-<strong>hover</strong></code></div>
					<div class="code-line" data-html-need-var="post-name"><label>%3$s</label><code>%4$s on-<strong>click</strong></code></div>
					<div class="code-line" data-html-need-var="post-name"><label>%3$s</label><code>%4$s on-<strong>hover</strong></code></div>',
				esc_html( DIVI_AREAS_ID_PREFIX ) . '<strong data-html-var="post-id"></strong>',
				__( 'Class:', 'divi_areas' ),
				__( 'Class:', 'divi_areas' ),
				esc_html( DIVI_AREAS_ID_PREFIX ) . '<strong data-html-var="post-name"></strong>'
			),
		],
		'js_triggers'  => [
			'label'       => __( 'JS API', 'divi_areas' ),
			'description' => [
				__( 'Build your own logic by using the flexible JS API!', 'divi_areas' ),
				__( 'Insert a Code module into your page or use the "Integration" options of the Divi settings to add your JS code.', 'divi_areas' ),
			],
			'read_more'   => [
				'https://divimode.com/article-categories/js-api/'                  => __( 'Codex: JS API', 'divi_areas' ),
				'https://divimode.com/how-to-use-the-js-api/'                      => __( 'Tutorial: How to use the JS API', 'divi_areas' ),
				'https://divimode.com/knowledge-base/divi-areas-script-generator/' => __( 'Divi Areas Script Generator', 'divi_areas' ),
			],
			'type'        => 'html',
			'html'        => sprintf(
				'<div class="code-line"><label>%2$s</label><code>DiviArea.show( &quot;%1$s&quot; )</code></div>
					<div class="code-line" data-html-need-var="post-name"><label>%4$s</label><code>DiviArea.show( &quot;%3$s&quot; )</code></div>',
				esc_html( DIVI_AREAS_ID_PREFIX ) . '<strong data-html-var="post-id"></strong>',
				__( 'JS Code', 'divi_areas' ),
				esc_html( DIVI_AREAS_ID_PREFIX ) . '<strong data-html-var="post-name"></strong>',
				__( 'JS Code', 'divi_areas' )
			),
		],
	],
];

/**
 * Filter the metabox tab contents.
 *
 * @since 2.3.0
 *
 * @param array $form The metabox form definition.
 */
$form = apply_filters( 'divi_areas_metabox_area_triggers_after_infos', $form );

$form['help_triggers'] = [
	'type'       => 'footer',
	'help_url'   => 'https://divimode.com/knowledge-base/section-area-triggers/',
	'help_label' => __( 'Trigger-Help', 'divi_areas' ),
];

/**
 * Filter the metabox tab contents.
 *
 * @since 2.0.0
 *
 * @param array $form The metabox form definition.
 */
return apply_filters( 'divi_areas_metabox_area_triggers', $form );
