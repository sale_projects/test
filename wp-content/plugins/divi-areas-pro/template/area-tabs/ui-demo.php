<?php
/**
 * Demo contents to test the UI controls.
 *
 * @since   1.5.0
 * @package Divi_Areas_Pro
 */

return [
	'section1' => [
		'label' => '1) Basic Controls',
		'type'  => 'section',
		'items' => [
			'demo_text'   => [
				'label'       => 'Title',
				'type'        => 'text',
				'default'     => '',
				'description' => 'Input a title for the circle counter.',
			],
			'demo_number' => [
				'label'             => 'Number',
				'type'              => 'text',
				'number_validation' => true,
				'value_min'         => 0,
				'value_max'         => 100,
				'default'           => 'auto',
				'description'       => "Define a number for the circle counter. (Don't include the percentage sign, use the option below.). <strong>Note: You can use only natural numbers from 0 to 100</strong>",
			],
			'demo_toggle' => [
				'label'       => 'Percent Sign',
				'type'        => 'yes_no_button',
				'options'     => [
					'on'  => 'On',
					'off' => 'Off',
				],
				'description' => 'Here you can choose whether the percent sign should be added after the number set above.',
			],
		],
	],
	'section2' => [
		'label' => '2) Conditions',
		'type'  => 'section',
		'items' => [
			'toggle-1' => [
				'label'     => 'Option 1',
				'type'      => 'yes_no_button',
				'options'   => [
					'on'  => 'On',
					'off' => 'Off',
				],
				'hints'     => [
					'on' => 'Only displays for the "on" state',
				],
				'condition' => [ 'toggle-3', '=', 'on' ],
			],
			'toggle-2' => [
				'label'   => 'Option 2',
				'type'    => 'yes_no_button',
				'default' => 'on',
				'options' => [
					'on'  => 'I',
					'off' => 'O',
				],
				'hints'   => [
					'on'  => 'The toggle is on',
					'off' => 'You turned it off',
				],
			],
			'toggle-3' => [
				'label'     => 'Option 3',
				'type'      => 'yes_no_button',
				'default'   => 'off',
				'options'   => [
					'on'  => 'On',
					'off' => 'Off',
				],
				'condition' => [ 'toggle-2', '=', 'on' ],
			],
		],
	],
	'section3' => [
		'label' => '3) More controls',
		'type'  => 'section',
		'items' => [
			'select_short'      => [
				'label'       => 'Select Demo, short',
				'type'        => 'select',
				'description' => 'Select any option. It really does not make any difference.',
				'options'     => [
					'none'     => 'Default',
					'relative' => 'Relative',
					'absolute' => 'Absolute',
					'fixed'    => 'Fixed',
					'static'   => 'Static',
					'sticky'   => 'Sticky',
				],
			],
			'select_item'       => [
				'label'       => 'Select Demo',
				'type'        => 'select',
				'description' => 'Select any option. It really does not make any difference.',
				'default'     => 'eta',
				'options'     => [
					'alpha'   => 'Alpha - α',
					'beta'    => 'Beta - β',
					'gamma'   => 'Gamma - γ',
					'delta'   => 'Delta - δ',
					'epsilon' => 'Epsilon - ε',
					'zeta'    => 'Zêta - ζ',
					'eta'     => 'Êta - η',
					'theta'   => 'Thêta - θ',
					'iota'    => 'Iota - ι',
					'kappa'   => 'Kappa - κ',
					'lambda'  => 'Lambda - λ',
					'mu'      => 'Mu - μ',
					'nu'      => 'Nu - ν',
					'xi'      => 'Xi - ξ',
					'omikron' => 'Omikron - ο',
					'pi'      => 'Pi - π',
					'rho'     => 'Rho - ρ',
					'sigma'   => 'Sigma - σ',
					'tau'     => 'Tau - τ',
					'upsilon' => 'Upsilon - υ',
					'phi'     => 'Phi - φ',
					'chi'     => 'Chi - χ',
					'psi'     => 'Psi - ψ',
					'omega'   => 'Omega - ω',

				],
			],
			'switcher_item'     => [
				'label'       => 'Switcher Demo',
				'type'        => 'switcher',
				'description' => 'This is displayed line a tab-bar.',
				'default'     => 'tablet',
				'options'     => [
					'desktop' => 'Desktop',
					'tablet'  => 'Tablet',
					'phone'   => 'Phone',
				],
			],
			'radio_item'        => [
				'label'       => 'Radio List',
				'type'        => 'radio',
				'description' => 'Description of the radio list.',
				'options'     => [
					'popup'  => [
						'label' => 'Popup',
						'icon'  => DIVI_AREAS_PATH . 'img/type-popup.svg',
					],
					'flyin'  => [
						'label' => 'Fly-In',
						'icon'  => DIVI_AREAS_PATH . 'img/type-flyin.svg',
					],
					'hover'  => [
						'label' => 'Hover',
						'icon'  => DIVI_AREAS_PATH . 'img/type-hover.svg',
					],
					'inline' => [
						'label' => 'Inline',
						'icon'  => DIVI_AREAS_PATH . 'img/type-inline.svg',
					],
				],
			],
			'checkbox_list'     => [
				'label'       => 'Checkbox List',
				'description' => 'Description of the checkbox list.',
				'type'        => 'checkbox',
				'options'     => [
					'a' => 'Option Alpha',
					'b' => [
						'label'    => 'Option Bravo',
						'disables' => 'c,d',
					],
					'c' => 'Option Charlie',
					'd' => 'Option Delta',
				],
			],
			'html_field'        => [
				'label'       => __( 'Custom HTML', 'divi_areas' ),
				'description' => 'This field shows custom, static HTML which is intended for instructional fields that are non-interactive.',
				'type'        => 'html',
				'html'        => 'This is <strong>simple, static HTML</strong>. Click inside the following code box to select its contents:<br><code>DiviArea.show("my-popup")</code>',
			],
			'responsive_number' => [
				'label'             => 'Responsive Number',
				'type'              => 'text',
				'responsive'        => true,
				'has_select'        => true,
				'numeric_units'     => true,
				'numeric_strings'   => true,
				'number_validation' => true,
				'value_min'         => 0,
				'value_max'         => 100,
				'default'           => '25%',
				'description'       => 'Turn on the responsive mode and try to enter a different value for each device',
			],
		],
	],
	'section4' => [
		'label' => '4) Post-Selector',
		'type'  => 'section',
		'items' => [
			'postlist_item' => [
				'label'       => 'Post-Selector Demo',
				'type'        => 'post_list',
				'description' => 'Select some posts.',
			],
		],
	],
	'section5' => [
		'label' => '5) Option fields (multiple vs single)',
		'type'  => 'section',
		'items' => [
			'single_radio'    => [
				'label'   => 'Radio (single)',
				'type'    => 'radio',
				'options' => [
					'popup'  => [
						'label' => 'Popup',
						'icon'  => DIVI_AREAS_PATH . 'img/type-popup.svg',
					],
					'flyin'  => [
						'label' => 'Fly-In',
						'icon'  => DIVI_AREAS_PATH . 'img/type-flyin.svg',
					],
					'hover'  => [
						'label' => 'Hover',
						'icon'  => DIVI_AREAS_PATH . 'img/type-hover.svg',
					],
					'inline' => [
						'label' => 'Inline',
						'icon'  => DIVI_AREAS_PATH . 'img/type-inline.svg',
					],
				],
			],
			'multi_radio'     => [
				'label'    => 'Radio (multiple)',
				'type'     => 'radio',
				'multiple' => true,
				'options'  => [
					'popup'  => [
						'label' => 'Popup',
						'icon'  => DIVI_AREAS_PATH . 'img/type-popup.svg',
					],
					'flyin'  => [
						'label' => 'Fly-In',
						'icon'  => DIVI_AREAS_PATH . 'img/type-flyin.svg',
					],
					'hover'  => [
						'label' => 'Hover',
						'icon'  => DIVI_AREAS_PATH . 'img/type-hover.svg',
					],
					'inline' => [
						'label' => 'Inline',
						'icon'  => DIVI_AREAS_PATH . 'img/type-inline.svg',
					],
				],
			],
			'single_checkbox' => [
				'label'    => 'Checkbox (single)',
				'type'     => 'checkbox',
				'multiple' => false,
				'options'  => [
					'popup'  => [
						'label' => 'Popup',
						'icon'  => DIVI_AREAS_PATH . 'img/type-popup.svg',
					],
					'flyin'  => [
						'label' => 'Fly-In',
						'icon'  => DIVI_AREAS_PATH . 'img/type-flyin.svg',
					],
					'hover'  => [
						'label' => 'Hover',
						'icon'  => DIVI_AREAS_PATH . 'img/type-hover.svg',
					],
					'inline' => [
						'label' => 'Inline',
						'icon'  => DIVI_AREAS_PATH . 'img/type-inline.svg',
					],
				],
			],
			'multi_checkbox'  => [
				'label'   => 'Checkbox (multiple)',
				'type'    => 'checkbox',
				'options' => [
					'popup'  => [
						'label' => 'Popup',
						'icon'  => DIVI_AREAS_PATH . 'img/type-popup.svg',
					],
					'flyin'  => [
						'label' => 'Fly-In',
						'icon'  => DIVI_AREAS_PATH . 'img/type-flyin.svg',
					],
					'hover'  => [
						'label' => 'Hover',
						'icon'  => DIVI_AREAS_PATH . 'img/type-hover.svg',
					],
					'inline' => [
						'label' => 'Inline',
						'icon'  => DIVI_AREAS_PATH . 'img/type-inline.svg',
					],
				],
			],
			'single_switcher' => [
				'label'   => 'Switcher (single)',
				'type'    => 'switcher',
				'options' => [
					'desktop' => 'Desktop',
					'tablet'  => 'Tablet',
					'phone'   => 'Phone',
				],
			],
			'multi_switcher'  => [
				'label'    => 'Switcher (multiple)',
				'type'     => 'switcher',
				'multiple' => true,
				'options'  => [
					'desktop' => 'Desktop',
					'tablet'  => 'Tablet',
					'phone'   => 'Phone',
				],
			],
			'single_select'   => [
				'label'   => 'Select (single)',
				'type'    => 'select',
				'options' => [
					'none'     => 'Default',
					'relative' => 'Relative',
					'absolute' => 'Absolute',
					'fixed'    => 'Fixed',
					'static'   => 'Static',
					'sticky'   => 'Sticky',
				],
			],
			'single_select2'  => [
				'label'   => 'Select (single, 2)',
				'type'    => 'select',
				'extra'   => [
					'rows' => 3,
				],
				'options' => [
					'none'     => 'Default',
					'relative' => 'Relative',
					'absolute' => 'Absolute',
					'fixed'    => 'Fixed',
					'static'   => 'Static',
					'sticky'   => 'Sticky',
				],
			],
			'multi_select'    => [
				'label'    => 'Select (multiple)',
				'type'     => 'select',
				'multiple' => true,
				'options'  => [
					'none'     => 'Default',
					'relative' => 'Relative',
					'absolute' => 'Absolute',
					'fixed'    => 'Fixed',
					'static'   => 'Static',
					'sticky'   => 'Sticky',
				],
			],
		],
	],
];
