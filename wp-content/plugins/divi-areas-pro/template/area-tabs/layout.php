<?php
/**
 * Content of the "Area Layout" metabox for the Divi Areas post type.
 *
 * @since   0.2.0
 * @package Divi_Areas_Pro
 */

$form = [];

$form['section_dimensions'] = [
	'label' => __( 'Dimensions', 'divi_areas' ),
	'type'  => 'section',
	'items' => [
		'size'       => [
			'label'       => __( 'Area Size', 'divi_areas' ),
			'description' => __( 'Customize the appearance of the Area.', 'divi_areas' ),
			'type'        => 'select',
			'default'     => 'auto',
			'options'     => [
				'auto'        => __( 'Default', 'divi_areas' ),
				'full-width'  => __( 'Full Width', 'divi_areas' ),
				'full-height' => __( 'Full Height', 'divi_areas' ),
				'full-screen' => __( 'Full Screen', 'divi_areas' ),
			],
			'condition'   => [ 'layout_type', '!=', 'inline' ],
		],
		'min_width'  => [
			'label'                => __( 'Min Width', 'divi_areas' ),
			'description'          => __( 'An Area will never be smaller than the defined minimum width. When the contents of the Area are wider than the minimum width, the Area will also grow wider.', 'divi_areas' ),
			'type'                 => 'text',
			'default'              => 'none',
			'responsive'           => true,
			'has_select'           => true,
			'number_validation'    => true,
			'numeric_strings'      => [ 'auto', 'none' ],
			'numeric_units'        => true,
			'numeric_default_unit' => 'px',
			'limit_min'            => 0,
			'slider_min'           => 0,
			'slider_max'           => 1280,
			'condition'            => [
				[
					[ 'size', '!=', 'full-screen' ],
					[ 'size', '!=', 'full-width' ],
				],
			],
		],
		'width'      => [
			'label'                => __( 'Width', 'divi_areas' ),
			'description'          => [
				__( 'By default, the Area width will match the width of the Section inside it.', 'divi_areas' ),
				__( 'However, if you choose to set a custom static width of the Area then the Sections inside the Area will match the Area width.', 'divi_areas' ),
			],
			'type'                 => 'text',
			'default'              => 'auto',
			'responsive'           => true,
			'has_select'           => true,
			'number_validation'    => true,
			'numeric_strings'      => [ 'auto' ],
			'numeric_units'        => true,
			'numeric_default_unit' => 'px',
			'limit_min'            => 0,
			'slider_min'           => 0,
			'slider_max'           => 1280,
			'condition'            => [
				[
					[ 'size', '!=', 'full-screen' ],
					[ 'size', '!=', 'full-width' ],
				],
			],
		],
		'max_width'  => [
			'label'                => __( 'Max Width', 'divi_areas' ),
			'description'          => [
				__( 'Setting a maximum width will prevent your Area from surpassing that maximum width. It is often used in combination with the width setting.', 'divi_areas' ),
			],
			'type'                 => 'text',
			'default'              => 'none',
			'responsive'           => true,
			'has_select'           => true,
			'number_validation'    => true,
			'numeric_strings'      => [ 'auto', 'none' ],
			'numeric_units'        => true,
			'numeric_default_unit' => 'px',
			'limit_min'            => 0,
			'slider_min'           => 0,
			'slider_max'           => 1280,
			'condition'            => [
				[
					[ 'size', '!=', 'full-screen' ],
					[ 'size', '!=', 'full-width' ],
				],
			],
		],
		'min_height' => [
			'label'                => __( 'Min Height', 'divi_areas' ),
			'description'          => __( 'An Area will never be smaller than the defined minimum height. When the contents of the Area are taller than the minimum height, the Area will also grow taller.', 'divi_areas' ),
			'type'                 => 'text',
			'default'              => 'none',
			'responsive'           => true,
			'has_select'           => true,
			'number_validation'    => true,
			'numeric_strings'      => [ 'auto', 'none' ],
			'numeric_units'        => true,
			'numeric_default_unit' => 'px',
			'limit_min'            => 0,
			'slider_min'           => 0,
			'slider_max'           => 1280,
			'condition'            => [
				[
					[ 'size', '!=', 'full-screen' ],
					[ 'size', '!=', 'full-height' ],
				],
			],
		],
		'height'     => [
			'label'                => __( 'Height', 'divi_areas' ),
			'description'          => __( 'Set a static height for this Area, if you want it to have a fixed height regardless of the Area contents.', 'divi_areas' ),
			'type'                 => 'text',
			'default'              => 'auto',
			'responsive'           => true,
			'has_select'           => true,
			'number_validation'    => true,
			'numeric_strings'      => [ 'auto' ],
			'numeric_units'        => true,
			'numeric_default_unit' => 'px',
			'limit_min'            => 0,
			'slider_min'           => 0,
			'slider_max'           => 1280,
			'condition'            => [
				[
					[ 'size', '!=', 'full-screen' ],
					[ 'size', '!=', 'full-height' ],
				],
			],
		],
		'max_height' => [
			'label'                => __( 'Max Height', 'divi_areas' ),
			'description'          => [
				__( 'By default, every Area can take the full screen height - that is equal to <code>100vh</code>. If you want to use a different the maximum height, you can set it here.', 'divi_areas' ),
			],
			'type'                 => 'text',
			'default'              => 'none',
			'responsive'           => true,
			'has_select'           => true,
			'number_validation'    => true,
			'numeric_strings'      => [ 'auto', 'none' ],
			'numeric_units'        => true,
			'numeric_default_unit' => 'px',
			'limit_min'            => 0,
			'slider_min'           => 0,
			'slider_max'           => 1280,
			'condition'            => [
				[
					[ 'size', '!=', 'full-screen' ],
					[ 'size', '!=', 'full-height' ],
				],
			],
		],
		'overflow'   => [
			'label'       => __( 'Overflow', 'divi_areas' ),
			'description' => [
				__( 'You can automatically display a scrollbar when the contents of your Area is higher than the Area itself. Overflow clipping affects any content that is displayed outside the Areas rectangle, like custom box shadows, or elements that stick outside the edge of an area (e.g. when using "Transform Translate"), etc.', 'divi_areas' ),
				__( '"Only Full-Height" will only clip the Area when it enters the full-height mode - i.e., when the screen height is smaller than the Area content.', 'divi_areas' ),
				__( 'The "Never Clip" option will never clip the Area and gives you the greatest design freedom. It may result in hidden content, when you do not manually add scrollbars to a section, row or module.', 'divi_areas' ),
			],
			'read_more'   => [
				'https://divimode.com/knowledge-base/area-overflow-layout/' => __( 'See the differences', 'divi_areas' ),
			],
			'type'        => 'select',
			'default'     => 'clip',
			'options'     => [
				'clip'        => __( 'Always Clip', 'divi_areas' ),
				'full-height' => __( 'Clip Full-Height', 'divi_areas' ),
				'show'        => __( 'Never Clip', 'divi_areas' ),
			],
			'condition'   => [ 'layout_type', '!=', 'inline' ],
		],
	],
];

/**
 * Filter the metabox tab contents.
 *
 * @since 2.3.0
 *
 * @param array $form The metabox form definition.
 */
$form = apply_filters( 'divi_areas_metabox_area_layout_after_dimensions', $form );

$form['section_position'] = [
	'label' => __( 'Position', 'divi_areas' ),
	'type'  => 'section',
	'items' => [
		'position'     => [
			'label'       => __( 'Area Alignment', 'divi_areas' ),
			'description' => [
				__( 'Choose, where you want the Area to appear.', 'divi_areas' ),
				__( 'A Popup can be aligned to an corner of the screen, or positioned in the center.', 'divi_areas' ),
				__( 'A Fly-In can be aligned to any corner of the screen.', 'divi_areas' ),
				__( 'A Hover Area is always aligned <em>relative to the trigger element</em> on the page.', 'divi_areas' ),
			],
			'type'        => 'position',
			'default'     => 'center_center',
			'extra'       => [
				'name'     => 'position',
				'align_to' => 'frame',
			],
			'condition'   => [ 'layout_type', '!=', 'inline' ],
		],
		'z_index'      => [
			'label'             => __( 'Z-Index', 'divi_areas' ),
			'description'       => [
				__( 'If you want to position this area behind other elements on the page, use this option to define a custom z-index.', 'divi_areas' ),
				__( 'Elements with a higher z-index are positioned above those elements with a lower z-index.', 'divi_areas' ),
			],
			'type'              => 'text',
			'default'           => 'auto',
			'responsive'        => false,
			'has_select'        => false,
			'number_validation' => true,
			'numeric_strings'   => [ 'auto' ],
			'numeric_units'     => false,
			'limit_min'         => - 1,
			'slider_min'        => 0,
			'slider_max'        => 100,
			'condition'         => [ 'layout_type', '!=', 'popup' ],
		],
		'push_content' => [
			'label'       => __( 'Push Content', 'divi_areas' ),
			'description' => [
				__( 'By default the Fly-In will overlap the content on your page. If you enable the "Push Content" flag, the Area will push the content away instead of overlapping it.', 'divi_areas' ),
				__( 'A full-width Fly-In will push the content up or down.', 'divi_areas' ),
				__( 'A full-height Fly-In will push the content left or right.', 'divi_areas' ),
			],
			'type'        => 'yes_no_button',
			'default'     => 'off',
			'options'     => [
				'on'  => __( 'Yes', 'divi_areas' ),
				'off' => __( 'No', 'divi_areas' ),
			],
			'hints'       => [
				'on'  => __( 'Push Content Inwards', 'divi_areas' ),
				'off' => __( 'Overlap Content', 'divi_areas' ),
			],
			'condition'   => [
				[
					[ 'layout_type', '=', 'flyin' ],
					[ 'size', '=', 'full-width' ],
				],
				[
					[ 'layout_type', '=', 'flyin' ],
					[ 'size', '=', 'full-height' ],
				],
			],
		],
	],
];

/**
 * Filter the metabox tab contents.
 *
 * @since 2.3.0
 *
 * @param array $form The metabox form definition.
 */
$form = apply_filters( 'divi_areas_metabox_area_layout_after_position', $form );

$form['section_design'] = [
	'label' => __( 'Design', 'divi_areas' ),
	'type'  => 'section',
	'items' => [
		'box_shadow'  => [
			'label'       => __( 'Add a Default Shadow', 'divi_areas' ),
			'description' => [
				__( 'Decide whether you want to add a default shadow to your Area. You should disable this option, when you set a custom Box-Shadow to the Divi Section.', 'divi_areas' ),
			],
			'type'        => 'yes_no_button',
			'default'     => 'on',
			'options'     => [
				'on'  => __( 'Yes', 'divi_areas' ),
				'off' => __( 'No', 'divi_areas' ),
			],
			'hints'       => [
				'on'  => __( 'Add default shadow', 'divi_areas' ),
				'off' => __( 'Do not add shadow', 'divi_areas' ),
			],
		],
		'with_loader' => [
			'label'       => __( 'Show Loader', 'divi_areas' ),
			'description' => [
				__( 'Decide whether to display a loading animation inside the Popup. This should be turned on, when the Popup contains an iframe or other content that is loaded dynamically.', 'divi_areas' ),
			],
			'type'        => 'yes_no_button',
			'default'     => 'off',
			'options'     => [
				'on'  => __( 'Yes', 'divi_areas' ),
				'off' => __( 'No', 'divi_areas' ),
			],
			'hints'       => [
				'on'  => __( 'Loading Animation', 'divi_areas' ),
				'off' => __( 'No Loading Animation', 'divi_areas' ),
			],
		],
	],
];

/**
 * Filter the metabox tab contents.
 *
 * @since 2.3.0
 *
 * @param array $form The metabox form definition.
 */
$form = apply_filters( 'divi_areas_metabox_area_layout_after_design', $form );

$form['section_close_button'] = [
	'label'     => __( 'Close Button', 'divi_areas' ),
	'type'      => 'section',
	'condition' => [ 'show_close', '=', 'on' ],
	'items'     => [
		'dark_close' => [
			'label'       => __( 'Button Color', 'divi_areas' ),
			'description' => [
				__( 'Here you can choose whether the Close button should be dark or light?. If the section has a light background, use a dark button. When the background is dark, use a light button.', 'divi_areas' ),
			],
			'type'        => 'select',
			'default'     => 'off',
			'options'     => [
				'on'  => __( 'Light', 'divi_areas' ),
				'off' => __( 'Dark', 'divi_areas' ),
			],
			// 'condition'   => [ 'default_close', '=', 'on' ],
		],
		'alt_close'  => [
			'label'       => __( 'Transparent Background', 'divi_areas' ),
			'description' => [
				__( 'The default close button is displayed in the top right corner, inside the Area box.', 'divi_areas' ),
			],
			'type'        => 'yes_no_button',
			'default'     => 'off',
			'options'     => [
				'on'  => __( 'Yes', 'divi_areas' ),
				'off' => __( 'No', 'divi_areas' ),
			],
			'hints'       => [
				'on'  => __( 'No Background Color', 'divi_areas' ),
				'off' => __( 'Default Background', 'divi_areas' ),
			],
			// 'condition'   => [ 'default_close', '=', 'on' ],
		],
	],
];

/**
 * Filter the metabox tab contents.
 *
 * @since 2.3.0
 *
 * @param array $form The metabox form definition.
 */
$form = apply_filters( 'divi_areas_metabox_area_layout_after_close', $form );

$form['help_layout'] = [
	'type'       => 'footer',
	'help_url'   => 'https://divimode.com/knowledge-base/section-area-layout/',
	'help_label' => __( 'Layout-Help', 'divi_areas' ),
];

/**
 * Filter the metabox tab contents.
 *
 * @since 2.0.0
 *
 * @param array $form The metabox form definition.
 */
return apply_filters( 'divi_areas_metabox_area_layout', $form );
