<?php
/**
 * Content of the "Popup Conditions" metabox for the Divi Areas post type.
 *
 * @since   0.1.0
 * @package Divi_Areas_Pro
 */

/**
 * User roles by which an Area can be limited.
 *
 * @since 2.0.0
 */
$roles = [
	':loggedin' => [
		'label'    => __( 'Any logged in user', 'divi_areas' ),
		'disables' => [ ':guest' ],
	],
	':guest'    => __( 'Guests', 'divi_areas' ),
];

foreach ( wp_roles()->roles as $key => $item ) {
	$roles[ $key ] = [
		'label'    => $item['name'],
		'disables' => [ ':loggedin' ],
	];
}

/**
 * Filter the user role options.
 *
 * @since 0.3.0
 * @var   array $roles List of available user roles.
 */
$roles = apply_filters( 'divi_areas_user_role_types', $roles );

$locations = [
	'css'        => __( 'CSS Selector', 'divi_areas' ),
	'header'     => __( 'Header', 'divi_areas' ),
	'navigation' => __( 'Main Menu', 'divi_areas' ),
	'post'       => __( 'Post Content', 'divi_areas' ),
	'comments'   => __( 'Comment Form', 'divi_areas' ),
	'footer'     => __( 'Footer', 'divi_areas' ),
];

/**
 * Filter the location types.
 *
 * @since 0.3.0
 * @var   array $locations List of location types (and labels).
 */
$locations = apply_filters( 'divi_areas_location_types', $locations );

$form = [];

$form['section_area_type'] = [
	'label' => __( 'Layout Type', 'divi_areas' ),
	'type'  => 'section',
	'items' => [
		'layout_type' => [
			'label'       => __( 'Area Type', 'divi_areas' ),
			'description' => __( 'Each Area Type has distinct features and behaves a little different.', 'divi_areas' ),
			'read_more'   => [
				'https://divimode.com/knowledge-base/area-types/' => __( 'Overview of Area Types', 'divi_areas' ),
			],
			'type'        => 'radio',
			'default'     => 'popup',
			'options'     => [
				'popup'  => [
					'label' => __( 'Popup', 'divi_areas' ),
					'icon'  => DIVI_AREAS_PATH . 'img/type-popup.svg',
				],
				'flyin'  => [
					'label' => __( 'Fly-In', 'divi_areas' ),
					'icon'  => DIVI_AREAS_PATH . 'img/type-flyin.svg',
				],
				'hover'  => [
					'label' => __( 'Hover', 'divi_areas' ),
					'icon'  => DIVI_AREAS_PATH . 'img/type-hover.svg',
				],
				'inline' => [
					'label' => __( 'Inline', 'divi_areas' ),
					'icon'  => DIVI_AREAS_PATH . 'img/type-inline.svg',
				],
			],
		],
		'is_static'   => [
			'label'       => __( 'Static Inline Area', 'divi_areas' ),
			'description' => __( 'Static Inline Areas cannot be triggered or hidden. They are either always visible or always hidden.', 'divi_areas' ),
			'type'        => 'yes_no_button',
			'default'     => 'off',
			'options'     => [
				'on'  => __( 'Yes', 'divi_areas' ),
				'off' => __( 'No', 'divi_areas' ),
			],
			'hints'       => [
				'on'  => __( 'Static Area', 'divi_areas' ),
				'off' => __( 'Dynamic Area', 'divi_areas' ),
			],
			'condition'   => [ 'layout_type', '=', 'inline' ],
		],
	],
];

/**
 * Filter the metabox tab contents.
 *
 * @since 2.3.0
 *
 * @param array $form The metabox form definition.
 */
$form = apply_filters( 'divi_areas_metabox_area_settings_after_layout_type', $form );

$form['section_user_type'] = [
	'label' => __( 'User Roles', 'divi_areas' ),
	'type'  => 'section',
	'items' => [
		'role_limitation' => [
			'label'       => __( 'User Role Limitation', 'divi_areas' ),
			'description' => __( 'Turn on the User Role Limitation if you want to show this Area only to certain users. Turn it off, to show this Area to every visitor.', 'divi_areas' ),
			'type'        => 'yes_no_button',
			'default'     => 'off',
			'options'     => [
				'on'  => __( 'Yes', 'divi_areas' ),
				'off' => __( 'No', 'divi_areas' ),
			],
			'hints'       => [
				'on'  => __( 'Specific users', 'divi_areas' ),
				'off' => __( 'Every user', 'divi_areas' ),
			],
		],
		'limit_role'      => [
			'label'       => __( 'User Roles', 'divi_areas' ),
			'description' => __( 'Choose which users can see this Area.', 'divi_areas' ),
			'type'        => 'checkbox',
			'options'     => $roles,
			'condition'   => [ 'role_limitation', '=', 'on' ],
		],
	],
];

/**
 * Filter the metabox tab contents.
 *
 * @since 2.3.0
 *
 * @param array $form The metabox form definition.
 */
$form = apply_filters( 'divi_areas_metabox_area_settings_after_user_type', $form );

$form['section_post_page'] = [
	'label' => __( 'Page Rules', 'divi_areas' ),
	'type'  => 'section',
	'items' => [
		'page_limitation' => [
			'label'       => __( 'Page Limitation', 'divi_areas' ),
			'description' => __( 'Turn on the Page Limitation if you want to load this Area only on certain pages. Turn it off, to load this Area to every page on your website.', 'divi_areas' ),
			'type'        => 'yes_no_button',
			'default'     => 'off',
			'options'     => [
				'on'  => __( 'Yes', 'divi_areas' ),
				'off' => __( 'No', 'divi_areas' ),
			],
			'hints'       => [
				'on'  => __( 'Specific pages', 'divi_areas' ),
				'off' => __( 'Entire website', 'divi_areas' ),
			],
		],
		'postlist'        => [
			'label'       => __( 'Page Rules', 'divi_areas' ),
			'description' => __( 'Choose the pages that should include this Area.', 'divi_areas' ),
			'type'        => 'post_list',
			'condition'   => [ 'page_limitation', '=', 'on' ],
		],
	],
];

/**
 * Filter the metabox tab contents.
 *
 * @since 2.3.0
 *
 * @param array $form The metabox form definition.
 */
$form = apply_filters( 'divi_areas_metabox_area_settings_after_page_rules', $form );

$form['section_inline_location'] = [
	'label'     => __( 'Inline Location', 'divi_areas' ),
	'type'      => 'section',
	'condition' => [ 'layout_type', '=', 'inline' ],
	'items'     => [
		'inline_location'   => [
			'label'       => __( 'Area Location', 'divi_areas' ),
			'description' => __( 'Choose the location where the Inline Area should be displayed.', 'divi_areas' ),
			'type'        => 'select',
			'default'     => 'css',
			'options'     => $locations,
		],
		'location_position' => [
			'label'       => __( 'Position', 'divi_areas' ),
			'description' => __( 'Choose, whether the Divi Area should be inserted before/after the above defined location. You can also decide to completely replace the locations content with the Area.', 'divi_areas' ),
			'type'        => 'select',
			'default'     => 'before',
			'options'     => [
				'before'  => __( 'Before', 'divi_areas' ),
				'after'   => __( 'After', 'divi_areas' ),
				'replace' => __( 'Replace', 'divi_areas' ),
			],
		],
		'location_selector' => [
			'label'       => __( 'CSS Selector', 'divi_areas' ),
			'description' => [
				__( 'Define the element by using a CSS selector.', 'divi_areas' ),
				__( 'You can specify multiple selectors separated by a comma. The Area will display at the first selector that is found on the current page.', 'divi_areas' ),
			],
			'type'        => 'text',
			'multiline'   => true,
			'default'     => '',
			'condition'   => [ 'inline_location', '=', 'css' ],
		],
	],
];

/**
 * Filter the metabox tab contents.
 *
 * @since 2.3.0
 *
 * @param array $form The metabox form definition.
 */
$form = apply_filters( 'divi_areas_metabox_area_settings_after_inline', $form );

$form['help_settings'] = [
	'type'       => 'footer',
	'help_url'   => 'https://divimode.com/knowledge-base/section-area-settings/',
	'help_label' => __( 'Settings-Help', 'divi_areas' ),
];

/**
 * Filter the metabox tab contents.
 *
 * @since 2.0.0
 *
 * @param array $form The metabox form definition.
 */
return apply_filters( 'divi_areas_metabox_area_settings', $form );
