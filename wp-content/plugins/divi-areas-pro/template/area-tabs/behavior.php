<?php
/**
 * Content of the "Area Behavior" metabox for the Divi Areas post type.
 *
 * @since   0.2.0
 * @package Divi_Areas_Pro
 */

$form = [];

$form['section_close'] = [
	'label' => __( 'Close Options', 'divi_areas' ),
	'type'  => 'section',
	'items' => [
		'show_close'   => [
			'label'       => __( 'Show Close Button', 'divi_areas' ),
			'description' => [
				__( 'Do you want to display the default Close Button in the top-right corner of the Popup?', 'divi_areas' ),
				__( 'Tip: You can always create a Custom Close Button inside the Area by adding the CSS Class "close" to a button, link or other module.', 'divi_areas' ),
			],
			'read_more'   => [
				'https://divimode.com/knowledge-base/custom-close-buttons/' => __( 'Custom Close Buttons', 'divi_areas' ),
			],
			'type'        => 'yes_no_button',
			'default'     => 'on',
			'options'     => [
				'on'  => __( 'Yes', 'divi_areas' ),
				'off' => __( 'No', 'divi_areas' ),
			],
			'hints'       => [
				'on'  => __( 'Show Close Button', 'divi_areas' ),
				'off' => __( 'Hide Close Button', 'divi_areas' ),
			],
			'condition'   => [ 'is_static', '!=', 'on' ],
		],
		'static_info'  => [
			'label'     => __( 'Static Area', 'divi_areas' ),
			'condition' => [ 'is_static', '=', 'on' ],
			'type'      => 'html',
			'html'      => sprintf(
				'<em>%s</em><br><em>%s</em>',
				__( 'To enable the Close Button, please make this Area dynamic.', 'divi_areas' ),
				__( 'Go to "Settings > Type" and set "Is Static Area" to "No"', 'divi_areas' )
			),
		],
		'not_modal'    => [
			'label'       => __( 'Close on Background-Click', 'divi_areas' ),
			'description' => [
				__( 'Here you can decide whether the Popup can be closed by clicking somewhere outside the Popup. When this option is disabled, the Popup can only be closed via a Close Button or pressing the ESC key on the keyboard.', 'divi_areas' ),
			],
			'type'        => 'yes_no_button',
			'default'     => 'on',
			'options'     => [
				'on'  => __( 'Yes', 'divi_areas' ),
				'off' => __( 'No', 'divi_areas' ),
			],
			'hints'       => [
				'on'  => __( 'Background-Click closes Area', 'divi_areas' ),
				'off' => __( 'Ignore click on background', 'divi_areas' ),
			],
			'condition'   => [ 'layout_type', '=', 'popup' ],
		],
		'not_blocking' => [
			'label'       => __( 'Close on Escape Key', 'divi_areas' ),
			'description' => [
				__( 'By default, every Popup can be closed by pressing the Escape-key on the keyboard.', 'divi_areas' ),
				__( 'You can choose to disable this feature to force users to click on the Close-Button inside the Popup.', 'divi_areas' ),
			],
			'type'        => 'yes_no_button',
			'default'     => 'on',
			'options'     => [
				'on'  => __( 'Yes', 'divi_areas' ),
				'off' => __( 'No', 'divi_areas' ),
			],
			'hints'       => [
				'on'  => __( 'Enable Escape Key', 'divi_areas' ),
				'off' => __( 'Disable Escape Key', 'divi_areas' ),
			],
			'condition'   => [ 'layout_type', '=', 'popup' ],
		],
		'singleton'    => [
			'label'       => __( 'Close Other Popups', 'divi_areas' ),
			'description' => __( 'Here you can decide whether this Popup should automatically close all other Popups when it is opened.', 'divi_areas' ),
			'type'        => 'yes_no_button',
			'default'     => 'off',
			'options'     => [
				'on'  => __( 'Yes', 'divi_areas' ),
				'off' => __( 'No', 'divi_areas' ),
			],
			'hints'       => [
				'on'  => __( 'Close all Popups', 'divi_areas' ),
				'off' => __( 'Leave other Popups open', 'divi_areas' ),
			],
			'condition'   => [ 'layout_type', '=', 'popup' ],
		],
	],
];

/**
 * Filter the metabox tab contents.
 *
 * @since 2.3.0
 *
 * @param array $form The metabox form definition.
 */
$form = apply_filters( 'divi_areas_metabox_area_behavior_after_close', $form );

$form['section_keep_closed'] = [
	'label'     => __( 'Keep Closed', 'divi_areas' ),
	'type'      => 'section',
	'condition' => [
		[ 'layout_type', '=', 'popup' ],
		[ 'layout_type', '=', 'flyin' ],
		[
			[ 'layout_type', '=', 'inline' ],
			[ 'show_close', '=', 'on' ],
			[ 'is_static', '!=', 'on' ],
		],
	],
	'items'     => [
		'keep_closed' => [
			'label'       => __( 'Keep Area Closed', 'divi_areas' ),
			'description' => [
				__( 'Set this to "No" to display the Area again on every page load, regardless of whether the user closed it. Turn it on, to keep the Area closed for a certain period.', 'divi_areas' ),
			],
			'read_more'   => [
				'https://divimode.com/knowledge-base/section-area-behavior/#keep-closed' => __( 'How does Keep Closed work?', 'divi_areas' ),
			],
			'type'        => 'yes_no_button',
			'default'     => 'off',
			'options'     => [
				'on'  => __( 'Yes', 'divi_areas' ),
				'off' => __( 'No', 'divi_areas' ),
			],
			'hints'       => [
				'on'  => __( 'Keep Area closed', 'divi_areas' ),
				'off' => __( 'Display on next page load', 'divi_areas' ),
			],
		],
		'close_for'   => [
			'label'             => __( 'Keep Closed For', 'divi_areas' ),
			'description'       => __( 'Define, how long the Area should stay closed.', 'divi_areas' ),
			'type'              => 'text',
			'responsive'        => false,
			'has_select'        => true,
			'number_validation' => true,
			'numeric_strings'   => false,
			'numeric_units'     => [
				'min',
				'hour',
				'day',
				'week',
			],
			'max_precission'    => 1,
			'limit_min'         => 0.1,
			'slider_min'        => 0,
			'slider_max'        => 60,
			'default'           => '1day',
			'condition'         => [ 'keep_closed', '=', 'on' ],
		],
		'closed_flag' => [
			'label'       => __( 'Start Closed-Timer', 'divi_areas' ),
			'description' => [
				__( 'Decide, if you want to start the Keep-Closed timer when the Area is displayed or hidden', 'divi_areas' ),
				__( '"On Hide" is the default behavior. It starts the timer when the Area is closed. When the user does not close the Area and reloads the page, the Area is displayed again, until the user actually closes the Area.', 'divi_areas' ),
				__( '"On Show" starts the timer in the moment the Area is displayed. In that case the Area is not displayed again on the next page load, regardless of whether the user closed the Area or not.', 'divi_areas' ),
			],
			'read_more'   => [
				'https://divimode.com/knowledge-base/keep-closed-behavior/' => __( 'How does Keep Closed work?', 'divi_areas' ),
			],
			'type'        => 'select',
			'default'     => 'close',
			'options'     => [
				'open'  => __( 'On Show', 'divi_areas' ),
				'close' => __( 'On Hide', 'divi_areas' ),
			],
			'condition'   => [ 'keep_closed', '=', 'on' ],
		],
	],
];

/**
 * Filter the metabox tab contents.
 *
 * @since 2.3.0
 *
 * @param array $form The metabox form definition.
 */
$form = apply_filters( 'divi_areas_metabox_area_behavior_after_keep_closed', $form );

$form['help_behavior'] = [
	'type'       => 'footer',
	'help_url'   => 'https://divimode.com/knowledge-base/section-area-behavior/',
	'help_label' => __( 'Behavior-Help', 'divi_areas' ),
];

/**
 * Filter the metabox tab contents.
 *
 * @since 2.0.0
 *
 * @param array $form The metabox form definition.
 */
return apply_filters( 'divi_areas_metabox_area_behavior', $form );
