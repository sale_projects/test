== Divimode shared libraray ==

= 0.2.3 =
-   Additional parameters for the filter `divimode_value_changed_{box}`
-   Additional parameter for the filter `divimode_value_changed_{box}_{field}`
-   New action for responsive components `divimode_ui_responsive_changed_`
-   The main app polls the VB view-mode and toggles responsive components as needed.

= 0.2.2 =
-   Display error code inside the license nag on the plugin settings page.
-   Collapsed metabox is fixed at the window-top.
-   New component: Timestamp, which uses the flatpickr library.
-   Possible to open an unlimited number of dialog windows in a sequence.
-   Conditions can check for the presence of a value in an array, i.e., they work with checkbox lists.
-   jQuery elements created via `setNode()` now link to the component instance, via the `_component` attribute.
-   Fixed a problem with missing default values due to components returning null/undefined instead of an empty string.
-   New hook for the input trait: `component.afterResetValue()`
-   Item lists require the new property `label_id` with the field-id of the label field.
-   Multiline text fields start with the correct initial height.
-   Add a 24-hour grace period for license validation timestamp.

= 0.2.1 =

-   Add new action to process verified ajax requests: `divimode_ajax_{action}`
-   Fix display bug in the post-list dialog that did not display selected items after page load.

= 0.2.0 =

-   Use namespace instead of a class-name prefix
-   New: Metabox API with new UI
-   New: Metabox contents are processed/saved by the library
-   New: Ajax handler and nonce verification handled by the library

= 0.1.3 =

-   Optionally read the license key from wp-config constant `DM_{SLUG}_LICENSE`.

= 0.1.2 =

-   Re-check for updates when the user clicks "Check again" on the WordPress updates page
-   Automatically check for updates every 12 hours instead of 24 hours
-   Clean up the auto-updater class
-   Provide details about plugin updates to the settings page template

= 0.1.1 =

-   PHP 7.3 compatibility
-   New filter to initialize plugin options: `divimode_load_options_{$option_name}`

= 0.1.0 =

-   Initial module version, extracted from Divi Areas Pro
