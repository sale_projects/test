<?php
/**
 * Trait for Divimode UI, used by DM_UI_Component classes.
 *
 * @package Divimode_UI
 */

namespace DiviAreasPro;

/**
 * Defines accessors for the draggable feature of the component. This trait is used by
 * the metabox to enable/disable the floating state where the metabox can be detached,
 * dragged on the screen and resized.
 *
 * @since 0.2.0
 */
trait DM_UI_Prop_Draggable {
	/**
	 * Defines, whether the metabox has a detach/attach icon in the toolbar.
	 *
	 * @since 0.2.0
	 * @var bool
	 */
	protected $prop_draggable = true;

	/**
	 * Sets the draggable flag.
	 *
	 * @since 0.2.0
	 * @param bool $state Whether the box can be detached and dragged.
	 * @return static Reference for chaining.
	 */
	public function set_draggable( $state ) {
		$this->prop_draggable = ! ! $state;
		$this->set_data( 'draggable', ! ! $state );

		return $this;
	}

	/**
	 * Returns the components draggable state.
	 *
	 * @since 0.2.0
	 * @return array
	 */
	public function get_draggable() {
		return $this->prop_draggable;
	}
}
