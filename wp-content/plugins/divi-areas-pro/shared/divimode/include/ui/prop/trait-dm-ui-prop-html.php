<?php
/**
 * Trait for Divimode UI, used by DM_UI_Component classes.
 *
 * @package Divimode_UI
 */

namespace DiviAreasPro;

/**
 * Defines accessors for a HTML content of the component.
 *
 * @since 0.2.0
 */
trait DM_UI_Prop_Html {
	/**
	 * The component html.
	 *
	 * @var string
	 */
	protected $prop_html = '';

	/**
	 * Sets the HTML code of the component.
	 *
	 * @since 0.2.0
	 * @param string $code The html code.
	 * @return static Reference for chaining.
	 */
	public function set_html( $code ) {
		$this->prop_html = $code;
		$this->set_data( 'html', $code );

		return $this;
	}

	/**
	 * Returns the component html.
	 *
	 * @since 0.2.0
	 * @return string
	 */
	public function get_html() {
		return $this->prop_html;
	}
}
