<?php
/**
 * Trait for Divimode UI, used by DM_UI_Component classes.
 *
 * @package Divimode_UI
 */

namespace DiviAreasPro;

/**
 * Defines accessors for the date and time options.
 *
 * @since 0.2.0
 */
trait DM_UI_Prop_DateTime {
	/**
	 * Whether the date is displayed and can be changed.
	 *
	 * @var bool
	 */
	protected $prop_has_date = true;

	/**
	 * Whether the time is displayed and can be changed.
	 *
	 * @var bool
	 */
	protected $prop_has_time = true;

	/**
	 * The input-mode of the timestamp field: single, multiple, range.
	 *
	 * @var string
	 */
	protected $prop_time_mode = 'single';

	/**
	 * Whether the user can type in a new value using the text field.
	 * When this is false, the user can only select a date using the timepicker UI.
	 *
	 * @var string
	 */
	protected $prop_allow_input = true;

	/**
	 * Sets the has-date of the component.
	 *
	 * @since 0.2.2
	 * @param bool $state The new has-date.
	 * @return static Reference for chaining.
	 */
	public function set_has_date( $state ) {
		$this->prop_has_date = $state;
		$this->set_data( 'has_date', $state );

		return $this;
	}

	/**
	 * Returns the has-date value.
	 *
	 * @since 0.2.2
	 * @return bool
	 */
	public function get_has_date() {
		return $this->prop_has_date;
	}

	/**
	 * Sets the has-time of the component.
	 *
	 * @since 0.2.2
	 * @param bool $state The new has-time.
	 * @return static Reference for chaining.
	 */
	public function set_has_time( $state ) {
		$this->prop_has_time = $state;
		$this->set_data( 'has_time', $state );

		return $this;
	}

	/**
	 * Returns the has-time value.
	 *
	 * @since 0.2.2
	 * @return bool
	 */
	public function get_has_time() {
		return $this->prop_has_time;
	}

	/**
	 * Sets the input-mode of the component.
	 *
	 * @since 0.2.2
	 * @param string $mode The new input-mode.
	 * @return static Reference for chaining.
	 */
	public function set_time_mode( $mode ) {
		$mode = trim( strtolower( $mode ) );
		if ( ! in_array( $mode, [ 'single', 'multiple', 'range' ], true ) ) {
			$mode = 'single';
		}
		$this->prop_time_mode = $mode;
		$this->set_data( 'time_mode', $mode );

		return $this;
	}

	/**
	 * Returns the input-mode value.
	 *
	 * @since 0.2.2
	 * @return string
	 */
	public function get_time_mode() {
		return $this->prop_time_mode;
	}

	/**
	 * Sets the allow-input flag of the component.
	 *
	 * @since 0.2.2
	 * @param bool $state The new allow-input.
	 * @return static Reference for chaining.
	 */
	public function set_allow_input( $state ) {
		$this->prop_allow_input = $state;
		$this->set_data( 'allow_input', $state );

		return $this;
	}

	/**
	 * Returns the allow-input value.
	 *
	 * @since 0.2.2
	 * @return bool
	 */
	public function get_allow_input() {
		return $this->prop_allow_input;
	}
}
