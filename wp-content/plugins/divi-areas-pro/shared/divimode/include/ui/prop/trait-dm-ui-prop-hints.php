<?php
/**
 * Trait for Divimode UI, used by DM_UI_Component classes.
 *
 * @package Divimode_UI
 */

namespace DiviAreasPro;

/**
 * Defines accessors for component hints; i.e. the hints in a yes-no-button.
 *
 * @since 0.2.0
 */
trait DM_UI_Prop_Hints {
	/**
	 * The component hints.
	 *
	 * @var array
	 */
	protected $prop_hints = [];

	/**
	 * Sets a new hints of the component.
	 *
	 * @since 0.2.0
	 * @param array $list The hints list.
	 * @return static Reference for chaining.
	 */
	public function set_hints( $list ) {
		if ( ! is_array( $list ) ) {
			$list = [];
		}

		$this->prop_hints = $list;
		$this->set_data( 'hints', $list );

		return $this;
	}

	/**
	 * Returns the component hints.
	 *
	 * @since 0.2.0
	 * @return array
	 */
	public function get_hints() {
		return $this->prop_hints;
	}
}
