<?php
/**
 * Trait for Divimode UI, used by DM_UI_Component classes.
 *
 * @package Divimode_UI
 */

namespace DiviAreasPro;

/**
 * Defines accessors to customize the footers Help-Button.
 *
 * @since 0.2.0
 */
trait DM_UI_Prop_Help {
	/**
	 * The component help url.
	 *
	 * @var string
	 */
	protected $prop_help_url = '';

	/**
	 * The component help button label.
	 *
	 * @var string
	 */
	protected $prop_help_label = '';

	/**
	 * Sets the help URL of the component.
	 *
	 * @since 0.2.0
	 * @param string $url The help URL.
	 * @return static Reference for chaining.
	 */
	public function set_help_url( $url ) {
		$url = esc_url( $url );

		$this->prop_help_url = $url;
		$this->set_data( 'help_url', $url );

		return $this;
	}

	/**
	 * Returns the component help url.
	 *
	 * @since 0.2.0
	 * @return string
	 */
	public function get_help_url() {
		return $this->prop_help_url;
	}

	/**
	 * Sets the help button label of the component.
	 *
	 * @since 0.2.0
	 * @param string $label The button label.
	 * @return static Reference for chaining.
	 */
	public function set_help_label( $label ) {
		$this->prop_help_label = $label;
		$this->set_data( 'help_label', $label );

		return $this;
	}

	/**
	 * Returns the components help button label.
	 *
	 * @since 0.2.0
	 * @return string
	 */
	public function get_help_label() {
		return $this->prop_help_label;
	}
}
