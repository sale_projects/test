<?php
/**
 * Trait for Divimode UI, used by DM_UI_Component classes.
 *
 * @package Divimode_UI
 */

namespace DiviAreasPro;

/**
 * Defines accessors for the multiline feature.
 * The feature is only available in text fields same as the numeric-values trait.
 *
 * @since 0.2.0
 */
trait DM_UI_Prop_Multiline {
	/**
	 * The multiline-state.
	 *
	 * @var bool
	 */
	protected $prop_multiline = false;

	/**
	 * Sets the multiline-state of the component.
	 *
	 * @since 0.2.0
	 * @param bool $state The new multiline-state.
	 * @return static Reference for chaining.
	 */
	public function set_multiline( $state ) {
		$this->prop_multiline = $state;
		$this->set_data( 'multiline', $state );

		return $this;
	}

	/**
	 * Returns the multiline-state.
	 *
	 * @since 0.2.0
	 * @return bool
	 */
	public function get_multiline() {
		return $this->prop_multiline;
	}
}
