<?php
/**
 * Trait for Divimode UI, used by DM_UI_Component classes.
 *
 * @package Divimode_UI
 */

namespace DiviAreasPro;

/**
 * Defines accessors for the read-only-state.
 *
 * @since 0.2.0
 */
trait DM_UI_Prop_ReadOnly {
	/**
	 * The read-only-state.
	 *
	 * @var bool
	 */
	protected $prop_readonly = false;

	/**
	 * Sets the read-only-state of the component.
	 *
	 * @since 0.2.0
	 * @param bool $state The new readonly state.
	 * @return static Reference for chaining.
	 */
	public function set_readonly( $state ) {
		$this->prop_readonly = $state;
		$this->set_data( 'readonly', $state );

		return $this;
	}

	/**
	 * Returns the read-only-state.
	 *
	 * @since 0.2.0
	 * @return bool
	 */
	public function get_readonly() {
		return $this->prop_readonly;
	}
}
