<?php
/**
 * Trait for Divimode UI, used by DM_UI_Component classes.
 *
 * @package Divimode_UI
 */

namespace DiviAreasPro;

/**
 * Defines accessors for the responsive-state.
 *
 * @since 0.2.0
 */
trait DM_UI_Prop_Responsive {
	/**
	 * The responsive-state.
	 *
	 * @var bool
	 */
	protected $prop_responsive = false;

	/**
	 * Sets the responsive-state of the component.
	 *
	 * @since 0.2.0
	 * @param bool $state The new responsive state.
	 * @return static Reference for chaining.
	 */
	public function set_responsive( $state ) {
		$this->prop_responsive = $state;
		$this->set_data( 'responsive', $state );

		return $this;
	}

	/**
	 * Returns the responsive-state.
	 *
	 * @since 0.2.0
	 * @return bool
	 */
	public function get_responsive() {
		return $this->prop_responsive;
	}
}
