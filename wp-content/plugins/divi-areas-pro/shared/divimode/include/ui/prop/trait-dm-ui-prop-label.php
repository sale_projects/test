<?php
/**
 * Trait for Divimode UI, used by DM_UI_Component classes.
 *
 * @package Divimode_UI
 */

namespace DiviAreasPro;

/**
 * Defines accessors for a component label (i.e., the "title").
 *
 * @since 0.2.0
 */
trait DM_UI_Prop_Label {
	/**
	 * The component label.
	 *
	 * @var string
	 */
	protected $prop_label = '';

	/**
	 * Sets the label of the component.
	 *
	 * @since 0.2.0
	 * @param string $label The component label.
	 * @return static Reference for chaining.
	 */
	public function set_label( $label ) {
		$this->prop_label = $label;
		$this->set_data( 'label', $label );

		return $this;
	}

	/**
	 * Returns the component label.
	 *
	 * @since 0.2.0
	 * @return string The component label.
	 */
	public function get_label() {
		return $this->prop_label;
	}
}
