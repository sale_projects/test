<?php
/**
 * Trait for Divimode UI, used by DM_UI_Component classes.
 *
 * @package Divimode_UI
 */

namespace DiviAreasPro;

/**
 * Defines accessors for a dialog form container.
 *
 * @since 0.2.0
 */
trait DM_UI_Prop_Form {

	/**
	 * Tell the DM_UI_Container parent that the form attribute contains
	 * child-component definitions.
	 *
	 * The default value would be 'items', but that's semantically wrong in this case.
	 * So we use the 'form' attribute here.
	 *
	 * @since 0.2.0
	 * @var string
	 */
	protected $child_attribute = 'form';

	/**
	 * The component form.
	 *
	 * @var array
	 */
	protected $prop_form = '';

	/**
	 * Sets the form details of the component.
	 *
	 * The form is a container that holds other components, even sections.
	 *
	 * @since 0.2.0
	 * @param array $form The component form.
	 * @return static Reference for chaining.
	 */
	public function set_form( $form ) {
		$this->prop_form = $form;
		$this->set_data( 'form', $form );

		return $this;
	}

	/**
	 * Returns the component form.
	 *
	 * @since 0.2.0
	 * @return array The component form.
	 */
	public function get_form() {
		return $this->prop_form;
	}
}
