<?php
/**
 * Trait for Divimode UI, used by DM_UI_Component classes.
 *
 * @package Divimode_UI
 */

namespace DiviAreasPro;

/**
 * Defines accessors for a component icon.
 *
 * @since 0.2.0
 */
trait DM_UI_Prop_Icon {
	/**
	 * The component icon.
	 *
	 * @var string
	 */
	protected $prop_icon = '';

	/**
	 * Sets a new icon of the component.
	 *
	 * @since 0.2.0
	 * @param string $name The icon name.
	 * @return static Reference for chaining.
	 */
	public function set_icon( $name ) {
		$this->prop_icon = $name;
		$this->set_data( 'icon', $name );

		return $this;
	}

	/**
	 * Returns the component icon.
	 *
	 * @since 0.2.0
	 * @return string
	 */
	public function get_icon() {
		return $this->prop_icon;
	}
}
