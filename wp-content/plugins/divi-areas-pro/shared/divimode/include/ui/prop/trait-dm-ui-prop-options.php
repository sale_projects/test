<?php
/**
 * Trait for Divimode UI, used by DM_UI_Component classes.
 *
 * @package Divimode_UI
 */

namespace DiviAreasPro;

/**
 * Defines accessors for component options; i.e. the options in a select list, radio
 * group, etc.
 *
 * @since 0.2.0
 */
trait DM_UI_Prop_Options {
	/**
	 * The component options.
	 *
	 * @var array
	 */
	protected $prop_options = [];


	/**
	 * Single select or multiple values possible?
	 *
	 * @var array
	 */
	protected $prop_multiple = false;

	/**
	 * Sets a new options of the component.
	 *
	 * @since 0.2.0
	 * @param array $list The options list.
	 * @return static Reference for chaining.
	 */
	public function set_options( $list ) {
		if ( ! is_array( $list ) ) {
			$list = [];
		}

		$this->prop_options = $list;
		$this->set_data( 'options', $list );

		return $this;
	}

	/**
	 * Returns the component options.
	 *
	 * @since 0.2.0
	 * @return array
	 */
	public function get_options() {
		return $this->prop_options;
	}

	/**
	 * Sets the multiple-options flag of the component.
	 *
	 * @since 0.2.0
	 * @param bool $multiple The new multiple flag.
	 * @return static Reference for chaining.
	 */
	public function set_multiple( $multiple ) {
		$multiple = ( true === $multiple || 'yes' === $multiple || 'on' === $multiple );

		$this->prop_multiple = $multiple;
		$this->set_data( 'multiple', $multiple );

		return $this;
	}

	/**
	 * Returns the component multiple.
	 *
	 * @since 0.2.0
	 * @return bool
	 */
	public function get_multiple() {
		return $this->prop_multiple;
	}
}
