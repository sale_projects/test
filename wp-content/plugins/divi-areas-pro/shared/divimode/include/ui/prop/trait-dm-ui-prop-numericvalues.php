<?php
/**
 * Trait for Divimode UI, used by DM_UI_Component classes.
 *
 * @package Divimode_UI
 */

namespace DiviAreasPro;

/**
 * Defines accessors to configure numeric input values for the component.
 * The feature is only available in text fields same as the has-select trait.
 *
 * @since 0.2.0
 */
trait DM_UI_Prop_NumericValues {
	/**
	 * Whether to expect a numeric value that should be validated.
	 *
	 * @var bool
	 */
	protected $prop_number_validation = false;

	/**
	 * The minimum allowed slider value (soft cap).
	 *
	 * @var number
	 */
	protected $prop_slider_min = null;

	/**
	 * The maximum allowed slider value (soft cap).
	 *
	 * @var number
	 */
	protected $prop_slider_max = null;

	/**
	 * The minimum allowed value (hard cap).
	 *
	 * @var number
	 */
	protected $prop_limit_min = null;

	/**
	 * The maximum allowed value (hard cap).
	 *
	 * @var number
	 */
	protected $prop_limit_max = null;

	/**
	 * Minimum floating-point precission.
	 * Default is 0, which allows integers (1, 2, etc).
	 * Set to 1 or higher to force float values, such as 1.0, 1.3, etc.
	 *
	 * @since 0.2.0
	 * @var string
	 */
	protected $prop_min_precission = 0;

	/**
	 * Maximum floating-point precission.
	 * Default is 2, which will round 1.234 to 1.23
	 *
	 * @since 0.2.0
	 * @var string
	 */
	protected $prop_max_precission = 2;

	/**
	 * Default unit for numeric values.
	 *
	 * @since 2.0.0
	 * @var string
	 */
	protected $prop_numeric_default_unit = '';

	/**
	 * List of numeric values, or ''/null/false to disable units.
	 * Default values are: %, em, rem, px, cm, mm, in, pt, pc, ex, vh, vw
	 *
	 * @var array
	 */
	protected $prop_numeric_units = [
		'%',
		'px',
		'vh',
		'vw',
		'em',
		'rem',
		'ch',
		'ex',
		'cm',
		'mm',
		'in',
		'pc',
		'pt',
	];

	/**
	 * List of non-numeric strings that are allowed, or ''/null/false to enforce
	 * numbers only.
	 * Default values are: auto, none, unset, initial, inherit
	 *
	 * @var array
	 */
	protected $prop_numeric_strings = [
		'none',
		'auto',
		'inherit',
		'initial',
		'unset',
	];

	/**
	 * Enables or disables the numeric-input behavior of the component.
	 *
	 * @since 0.2.0
	 * @param bool $value Whether the component only allows numeric values.
	 * @return static Reference for chaining.
	 */
	public function set_number_validation( $value ) {
		$value = ! ! $value;

		$this->prop_number_validation = $value;
		$this->set_data( 'number_validation', $value );

		return $this;
	}

	/**
	 * Returns the component numeric-input behavior value.
	 *
	 * @since 0.2.0
	 * @return array
	 */
	public function get_number_validation() {
		return $this->prop_number_validation;
	}


	/**
	 * Sets a new minimum slider value of the component.
	 *
	 * @since 0.2.0
	 * @param number $value The minimum slider value.
	 * @return static Reference for chaining.
	 */
	public function set_slider_min( $value ) {
		if ( ! is_numeric( $value ) ) {
			$value = null;
		}

		$this->prop_slider_min = $value;
		$this->set_data( 'slider_min', $value );

		return $this;
	}

	/**
	 * Returns the component minimum slider value.
	 *
	 * @since 0.2.0
	 * @return array
	 */
	public function get_slider_min() {
		return $this->prop_slider_min;
	}

	/**
	 * Sets a new maximum slider value of the component.
	 *
	 * @since 0.2.0
	 * @param number $value The maximum slider value.
	 * @return static Reference for chaining.
	 */
	public function set_slider_max( $value ) {
		if ( ! is_numeric( $value ) ) {
			$value = null;
		}

		$this->prop_slider_max = $value;
		$this->set_data( 'slider_max', $value );

		return $this;
	}

	/**
	 * Returns the component maximum slider value.
	 *
	 * @since 0.2.0
	 * @return array
	 */
	public function get_slider_max() {
		return $this->prop_slider_max;
	}



	/**
	 * Sets a new minimum allowed value of the component.
	 *
	 * @since 0.2.0
	 * @param number $value The minimum allowed value.
	 * @return static Reference for chaining.
	 */
	public function set_limit_min( $value ) {
		if ( ! is_numeric( $value ) ) {
			$value = null;
		}

		$this->prop_limit_min = $value;
		$this->set_data( 'limit_min', $value );

		return $this;
	}

	/**
	 * Returns the component minimum allowed value.
	 *
	 * @since 0.2.0
	 * @return array
	 */
	public function get_limit_min() {
		return $this->prop_limit_min;
	}

	/**
	 * Sets a new maximum allowed value of the component.
	 *
	 * @since 0.2.0
	 * @param number $value The maximum allowed value.
	 * @return static Reference for chaining.
	 */
	public function set_limit_max( $value ) {
		if ( ! is_numeric( $value ) ) {
			$value = null;
		}

		$this->prop_limit_max = $value;
		$this->set_data( 'limit_max', $value );

		return $this;
	}

	/**
	 * Returns the component maximum allowed value.
	 *
	 * @since 0.2.0
	 * @return array
	 */
	public function get_limit_max() {
		return $this->prop_limit_max;
	}

	/**
	 * Sets available numeric-units for the control.
	 *
	 * @since 0.2.0
	 * @param array $units List of units, or true for defaults.
	 * @return static Reference for chaining.
	 */
	public function set_numeric_units( $units ) {
		if ( true !== $units && ! is_array( $units ) ) {
			$units = '';
		}

		$this->prop_numeric_units = $units;
		$this->set_data( 'numeric_units', $units );

		return $this;
	}

	/**
	 * Returns the list of accepted number units.
	 *
	 * @since 0.2.0
	 * @return array
	 */
	public function get_numeric_units() {
		return $this->prop_numeric_units;
	}

	/**
	 * Defines a list of allowed non-numeric input values.
	 *
	 * @since 0.2.0
	 * @param array $values List of valid strings, or true for defaults.
	 * @return static Reference for chaining.
	 */
	public function set_numeric_strings( $values ) {
		if ( true !== $values && ! is_array( $values ) ) {
			$values = '';
		}

		$this->prop_numeric_strings = $values;
		$this->set_data( 'numeric_strings', $values );

		return $this;
	}

	/**
	 * Returns the list of allowed non-numeric input values.
	 *
	 * @since 0.2.0
	 * @return array
	 */
	public function get_numeric_strings() {
		return $this->prop_numeric_strings;
	}

	/**
	 * Sets a new display string to show instead of number "0".
	 *
	 * @since 0.2.0
	 * @param string $label The alias for "0".
	 * @return static Reference for chaining.
	 */
	public function set_zero_string( $label ) {
		$this->prop_zero_string = $label;
		$this->set_data( 'zero_string', $label );

		return $this;
	}

	/**
	 * Returns the display string to show instead of number "0".
	 *
	 * @since 0.2.0
	 * @return string
	 */
	public function get_zero_string() {
		return $this->prop_zero_string;
	}
}
