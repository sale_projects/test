<?php
/**
 * Trait for Divimode UI, used by DM_UI_Component classes.
 *
 * @package Divimode_UI
 */

namespace DiviAreasPro;

/**
 * Defines accessors for a component description or help text.
 *
 * @since 0.2.0
 */
trait DM_UI_Prop_Description {
	/**
	 * The component description.
	 *
	 * @var string
	 */
	protected $prop_description = '';


	/**
	 * Additional related-info links.
	 *
	 * @var array
	 */
	protected $prop_read_more = [];

	/**
	 * Sets a new description of the component.
	 *
	 * @since 0.2.0
	 * @param string|array $text The description text.
	 * @return static Reference for chaining.
	 */
	public function set_description( $text ) {
		if ( is_array( $text ) ) {
			$text = '<p>' . implode( '</p><p>', $text ) . '</p>';
		}

		$this->prop_description = $text;
		$this->set_data( 'description', $text );

		return $this;
	}

	/**
	 * Returns the component description.
	 *
	 * @since 0.2.0
	 * @return string
	 */
	public function get_description() {
		return $this->prop_description;
	}

	/**
	 * Sets a new description of the component.
	 *
	 * @since 0.2.0
	 * @param array $links Each array key is treated as link URL, the value as label.
	 * @return static Reference for chaining.
	 */
	public function set_read_more( $links ) {
		$this->prop_read_more = $links;
		$this->set_data( 'read_more', $links );

		return $this;
	}

	/**
	 * Returns the component description.
	 *
	 * @since 0.2.0
	 * @return array
	 */
	public function get_read_more() {
		return $this->prop_read_more;
	}
}
