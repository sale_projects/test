<?php
/**
 * Trait for Divimode UI, used by DM_UI_Component classes.
 *
 * Conditions can be defined in the following forms:
 *   $component->set_condition( $rule|$condition );
 *
 * RULE:
 *   Always an array with three elements: field, operator, value
 *   [ 'field', '=', 'value' ]
 *
 * RULESET
 *   A list of multiple rules.
 *   If ALL rules pass, the element is displayed (= AND condition)
 *   [ $rule1, $rule2 ]
 *
 * CONDITION
 *   A list of multiple Rulesets or Rules.
 *   If ONE Ruleset or Rule passes, the element is displayed (= OR condition)
 *   [ $ruleset1, $rule2, $ruleset3 ]
 *
 * Attention: Because a single ruleset can also be confused with a condition, you
 * always have to specify a single ruleset (rule1 AND rule2) as condition. Example:
 *
 *   ->set_condition( [ [ 'field1', '=', 'value1' ], [ 'field2', '=', 'value2' ] ] )
 *   This is treated as a CONDITION (field1 = value1 OR field2 = value2)
 *
 *   ->set_condition( [ [ [ 'field1', '=', 'value1' ], [ 'field2', '=', 'value2' ] ] ] )
 *   This is treated as a single RULESET (field1 = value1 AND field2 = value2)
 *
 * @package Divimode_UI
 */

namespace DiviAreasPro;

/**
 * Defines accessors for component conditions.
 *
 * @since 0.2.0
 */
trait DM_UI_Prop_Conditions {
	/**
	 * The component conditions.
	 *
	 * @var array
	 */
	protected $prop_conditions = [];

	/**
	 * Sets a new conditions of the component.
	 *
	 * @since 0.2.0
	 * @param array $rules Described in add_condition().
	 * @return static Reference for chaining.
	 */
	public function set_condition( $rules ) {
		$rules = $this->normalize_ruleset( $rules );

		$this->prop_conditions = $rules;
		$this->set_data( 'conditions', $rules );

		return $this;
	}

	/**
	 * Adds a new ruleset to the current condition list.
	 *
	 * Each ruleset is connected via OR operator and might contain multiple rules that
	 * are connected via AND logic.
	 *
	 * @since 0.2.0
	 * @param array $rules New rules. Each rule is defined by an array of 3 values:
	 *                     ['key', 'operator', 'value'] while the operator can be
	 *                     "=" or "!=", e.g. ['field', '=', 'on'].
	 * @return static Reference for chaining.
	 */
	public function add_condition( $rules ) {
		$rules = $this->normalize_rule( $rules );

		$this->prop_conditions[] = $rules;
		$this->set_data( 'conditions', $this->prop_conditions );

		return $this;
	}

	/**
	 * Returns the component conditions.
	 *
	 * @since 0.2.0
	 * @return array
	 */
	public function get_condition() {
		return $this->prop_conditions;
	}

	/**
	 * Sanitizes an entire rule-set, which contains multiple rules.
	 *
	 * @since 0.2.0
	 * @param array $ruleset The initial ruleset.
	 * @return array The normalized ruleset.
	 */
	private function normalize_ruleset( $ruleset ) {
		$normalized = [];

		if ( is_array( $ruleset ) ) {
			if ( $this->is_rule_cond( $ruleset ) ) {
				$ruleset = [ [ $ruleset ] ];
			}

			foreach ( $ruleset as $rule ) {
				$value = $this->normalize_rule( $rule );

				if ( $value ) {
					$normalized[] = $value;
				}
			}
		}

		return $normalized;
	}

	/**
	 * Normalizes a rule.
	 *
	 * @since 0.2.0
	 * @param array $rule The initial rule details.
	 * @return array The normalized rule, always a 2-dimensional array.
	 */
	private function normalize_rule( $rule ) {
		$normalized = [];

		if ( $this->is_rule_cond( $rule ) ) {
			$normalized = [ $rule ];
		} elseif ( $this->is_rule( $rule ) ) {
			$normalized = $rule;
		}

		return $normalized;
	}

	/**
	 * Checks, if the given value is a valid rule-condition.
	 *
	 * A rule condition is always an array with 3 values.
	 *
	 * @since 0.2.0
	 * @param mixed $value The value to check.
	 * @return bool True, if the value is a valid rule.
	 */
	private function is_rule_cond( $value ) {
		return is_array( $value )
			&& 3 === count( $value )
			&& ! empty( $value[0] )
			&& isset( $value[1] )
			&& isset( $value[2] )
			&& is_string( $value[0] )
			&& in_array( $value[1], [ '=', '!=' ], true );
	}

	/**
	 * Checks, if the given value is a rule with at least one rule condition.
	 *
	 * @since 0.2.0
	 * @param mixed $value The value to check.
	 * @return bool True, if the value is a rule with at least one rule-condition.
	 */
	private function is_rule( $value ) {
		return is_array( $value )
			&& isset( $value[0] )
			&& is_array( $value[0] )
			&& $this->is_rule_cond( $value[0] );
	}
}
