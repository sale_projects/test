<?php
/**
 * Trait for Divimode UI, used by DM_UI_Component classes.
 *
 * @package Divimode_UI
 */

namespace DiviAreasPro;

/**
 * Defines accessors for the collapsable feature of the component. This feature is
 * generally applied to containers and defines, whether the container has a toggle
 * icon in the header.
 *
 * @since 0.2.0
 */
trait DM_UI_Prop_Collapsable {
	/**
	 * The component collapsable flag.
	 *
	 * @var array
	 */
	protected $prop_collapsable = true;

	/**
	 * Sets the collapsable flag.
	 *
	 * @since 0.2.0
	 * @param bool $state Whether the box can be collapsed.
	 * @return static Reference for chaining.
	 */
	public function set_collapsable( $state ) {
		$this->prop_collapsable = ! ! $state;
		$this->set_data( 'collapsable', ! ! $state );

		return $this;
	}

	/**
	 * Returns the components collapsable state.
	 *
	 * @since 0.2.0
	 * @return array
	 */
	public function get_collapsable() {
		return $this->prop_collapsable;
	}
}
