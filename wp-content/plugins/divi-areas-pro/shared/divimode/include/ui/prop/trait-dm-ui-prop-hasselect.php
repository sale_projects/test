<?php
/**
 * Trait for Divimode UI, used by DM_UI_Component classes.
 *
 * @package Divimode_UI
 */

namespace DiviAreasPro;

/**
 * Defines accessors for the has-select feature.
 * The feature is only available in text fields same as the numeric-values trait.
 *
 * @since 0.2.0
 */
trait DM_UI_Prop_HasSelect {
	/**
	 * The has-select-state.
	 *
	 * @var bool
	 */
	protected $prop_has_select = false;

	/**
	 * Sets the has-select-state of the component.
	 *
	 * @since 0.2.0
	 * @param bool $state The new has-select-state.
	 * @return static Reference for chaining.
	 */
	public function set_has_select( $state ) {
		$this->prop_has_select = $state;
		$this->set_data( 'has_select', $state );

		return $this;
	}

	/**
	 * Returns the has-select-state.
	 *
	 * @since 0.2.0
	 * @return bool
	 */
	public function get_has_select() {
		return $this->prop_has_select;
	}
}
