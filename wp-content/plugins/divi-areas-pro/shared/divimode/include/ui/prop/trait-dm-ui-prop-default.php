<?php
/**
 * Trait for Divimode UI, used by DM_UI_Component classes.
 *
 * @package Divimode_UI
 */

namespace DiviAreasPro;

/**
 * Defines accessors for a default value of the component. This default value can by
 * any type (scalar or array), but defaults to an empty string when not set.
 *
 * @since 0.2.0
 */
trait DM_UI_Prop_Default {
	/**
	 * The component default value.
	 *
	 * @var array
	 */
	protected $prop_default = [];

	/**
	 * Sets a new default value of the component.
	 *
	 * @since 0.2.0
	 * @param mixed $value The default value.
	 * @return static Reference for chaining.
	 */
	public function set_default( $value ) {
		$this->prop_default = $value;
		$this->set_data( 'default', $value );

		return $this;
	}

	/**
	 * Returns the component default.
	 *
	 * @since 0.2.0
	 * @return mixed
	 */
	public function get_default() {
		return $this->prop_default;
	}
}
