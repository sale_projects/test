<?php
/**
 * Metabox UI helper class
 *
 * @package Divimode_UI
 */

namespace DiviAreasPro;

/**
 * Template base class, used to output templates and data of a divimode component.
 *
 * @since 0.2.0
 */
abstract class DM_UI_Component {

	/**
	 * List of all registered component types.
	 *
	 * @since 0.2.0
	 * @var array
	 */
	private static $registered_classes = [];

	/**
	 * An internal index that is used to generate unique component keys.
	 *
	 * @see self::set_internal_key()
	 *
	 * @since 0.2.0
	 * @var int
	 */
	private static $component_key_index = 100;

	/**
	 * Internal reference to all used public components IDs.
	 *
	 * @see self::set_id()
	 *
	 * @since 0.2.0
	 * @var array
	 */
	private static $used_ids = [];

	/**
	 * List of all assigned internal component keys. Used to prevent manual
	 * assignment of an internal component key to a different component.
	 *
	 * @see self::set_internal_key()
	 * @see self::set_id()
	 *
	 * @since 0.2.0
	 * @var array
	 */
	private static $used_keys = [];

	/**
	 * The internal ID of the component is assigned by the constructor and cannot be
	 * changed later.
	 *
	 * @since 0.2.0
	 * @var string
	 */
	private $component_key = '';

	/**
	 * The component ID.
	 *
	 * @since 0.2.0
	 * @var string
	 */
	protected $id = '';

	/**
	 * Additional component configuration to set internal flags or overwrite default
	 * values.
	 *
	 * @since 0.2.0
	 * @var array
	 */
	protected $prop_extra = [];

	/**
	 * Defines the component type for the JS state object.
	 *
	 * @since 0.2.0
	 * @var string
	 */
	protected $component_type = 'component';

	/**
	 * Holds component data which is passed to the JS API.
	 *
	 * @since 0.2.0
	 * @var array
	 */
	private $data = [];

	/**
	 * List of all JS templates in the current application.
	 *
	 * @since 0.2.0
	 * @var array
	 */
	private static $templates = [];

	/**
	 * List of templates that were already output.
	 *
	 * @since 0.2.0
	 * @var array
	 */
	private static $output_templates = [];

	/**
	 * Component constructor assigns a unique ID to the new component.
	 *
	 * @since 0.2.0
	 */
	public function __construct() {
		$this->set_internal_key();
		$this->set_id( false );

		// Add the component type to our registered-classes list.
		self::$registered_classes[ get_class( $this ) ] = true;
	}

	/**
	 * Generates a new component of the specified type and returns the instance.
	 *
	 * @throws DM_Exception_Component The component type is invalid.
	 *
	 * @since 0.2.0
	 * @param string $type Type of the component.
	 * @param string $id   The component ID.
	 * @return DM_UI_Component The newly created component instance.
	 */
	public static function create( $type, $id ) {
		$component  = null;
		$type       = esc_attr( str_replace( ' ', '', ucwords( str_replace( '_', ' ', $type ) ) ) );
		$class_name = __NAMESPACE__ . "\DM_UI_Component_{$type}";

		if ( class_exists( $class_name ) ) {
			$component = new $class_name();

			if ( $component instanceof DM_UI_Component ) {
				$component->set_id( $id );
			}
		}

		if ( ! $component ) {
			throw new DM_Exception_Component( "Unknown component type: {$class_name} / type '{$type}'" );
		}

		return $component;
	}

	/**
	 * Generic importer to configure the component based on the given config-array.
	 *
	 * @throws DM_Exception_Configure When attemptying to change the
	 * component type.
	 *
	 * @since 0.2.0
	 * @param array $details The configuration to import.
	 * @return void
	 */
	public function configure( array $details ) {
		foreach ( $details as $key => $value ) {
			$key = sanitize_key( strtolower( $key ) );

			if ( ! $key ) {
				continue;
			}

			if ( 'type' === $key ) {
				// Type is already defined while the component is created.
				if ( $value !== $this->component_type ) {
					throw new DM_Exception_Configure( 'Cannot change the component type after initialization' );
				} else {
					continue;
				}
			}

			$setter    = "set_{$key}";
			$prop_name = "prop_{$key}";

			if ( method_exists( $this, $setter ) ) {
				// Explicit setter.
				$this->{$setter}( $value );
			} elseif ( isset( $this->{$prop_name} ) ) {
				// Implicit setter.
				$this->{$prop_name} = $value;
				$this->set_data( $key, $value );
			} else {
				// phpcs:ignore WordPress.PHP.DevelopmentFunctions.error_log_error_log
				error_log( "Unknown configuration: {$this->component_type}.{$key}." );
			}
		}
	}

	/**
	 * Give this component a unique and immutable component key.
	 *
	 * @since 0.2.0
	 * @return void
	 */
	private function set_internal_key() {
		if ( ! $this->component_key ) {
			do {
				self::$component_key_index++;
				$this->component_key = 'dm' . self::$component_key_index;
			} while ( isset( self::$used_keys[ $this->component_key ] ) );

			self::$used_keys[ $this->component_key ] = $this->component_key;
		}
	}

	/**
	 * Assigns a new ID to the component. When the newId is empty or false, then
	 * a unique internal ID will be assigned.
	 *
	 * When the ID is already used by a different component, nothing happens.
	 *
	 * @since 0.2.0
	 * @param string|false $new_id The new ID to assign, or false to assign the
	 *                             internal ID.
	 * @return static
	 */
	public function set_id( $new_id ) {
		$old_id = $this->id;

		// When no ID is specified, reset the component ID to the internal ID.
		if ( is_string( $new_id ) ) {
			// Sanitize the new component ID.
			$new_id = trim( strtolower( esc_attr( $new_id ) ) );
		} else {
			$new_id = false;
		}

		/*
		Do nothing, when (#1) the new ID is already used by a different component,
		or (#2) the new ID is a protected component key.
		*/
		if (
			( $new_id && $new_id !== $this->id )
			&& (
				isset( self::$used_ids[ $new_id ] ) // #1
				|| isset( self::$used_keys[ $new_id ] ) // #2
			)
		) {
			$new_id = false;
		}

		if ( $new_id ) {
			// When we have a valid ID, assign it to the component.
			$this->id = $new_id;
		} elseif ( ! $this->id ) {
			// Fallback to the unique internal ID when no newId was specified.
			$this->id = $this->component_key;
		}

		// Track the ID to avoid duplicates.
		if ( $old_id ) {
			unset( self::$used_ids[ $old_id ] );
		}
		self::$used_ids[ $this->id ] = $this->id;

		// Store the component id in the JS data object.
		$this->data['id'] = $this->id;

		return $this;
	}

	/**
	 * Returns the current component ID.
	 *
	 * @since 0.2.0
	 * @return string The component ID.
	 */
	public function get_id() {
		return $this->id;
	}

	/**
	 * Sets additional configuration details for the component.
	 *
	 * This can be used to add specific details that cannot be set via
	 * $this->configure() - i.e., add properties that have no getter/setter.
	 *
	 * @since 0.2.0
	 * @param array $data Additional configuration details.
	 * @return static Reference for chaining.
	 */
	public function set_extra( $data ) {
		$this->prop_extra = $data;
		$this->set_data( 'extra', $data );

		return $this;
	}

	/**
	 * Returns the additional component configuration.
	 *
	 * @since 0.2.0
	 * @return array The configuration array.
	 */
	public function get_extra() {
		return $this->prop_extra;
	}

	/**
	 * Returns the internal component ID which uniquely identifies this component.
	 *
	 * @since 0.2.0
	 * @return string
	 */
	final public function get_component_key() {
		return $this->component_key;
	}

	/**
	 * Returns a list of reserved component property-names.
	 *
	 * @since 0.2.0
	 * @return array
	 */
	protected function get_reserved_props() {
		// Default list reserved props. Can be extended by child classes.
		return [ 'id', 'component_type' ];
	}

	/**
	 * Sets a single property of the component.
	 *
	 * The following reserved properties cannot be set with this function:
	 * - "id" .. can only be changed via set_id()
	 * - "component_type" .. cannot be changed
	 * - "children" .. cannot be changed
	 *
	 * @since 0.2.0
	 * @param string $key   The property name or an array of key-value pairs to set
	 *                      multiple props at once.
	 * @param mixed  $value Optional. The property value; only used when $key is a
	 *                      string.
	 * @return Divimode_UI_DAP_Component
	 */
	public function set_data( $key, $value ) {
		if ( is_array( $key ) ) {
			foreach ( $key as $_key => $_value ) {
				$this->set_data( $_key, $_value );
			}
		} else {
			$reserved = $this->get_reserved_props();

			if ( ! in_array( $key, $reserved, true ) ) {
				$this->data[ $key ] = $value;
			}
		}

		return $this;
	}

	/**
	 * Returns the current components internal data i.e. the state.
	 *
	 * @since 0.2.0
	 * @return array
	 */
	public function get_data() {
		$this->data['component_type'] = $this->component_type;

		return $this->sanitize_data_array( $this->data );
	}

	/**
	 * Expects an array of any depth and sanitizes values for before they are output
	 * for the JS app. The function returns a new array and does not change the input
	 * array in any way.
	 *
	 * Rule 1: Search for the 'icon' attribute in every level. When an icon attribute
	 * is found that holds a valid path, the icon attribute is replaced with the
	 * specified files content.
	 *
	 * Rule 2: Search for the attributes `help_url` and `read_more` and remove invalid
	 * URLs. This ensures that the resulting array only contains valid URLs
	 *
	 * @since 0.2.0
	 * @param array $data The original data.
	 * @return array A copy of the original array with all icon-paths replaced.
	 */
	protected function sanitize_data_array( array $data ) {
		$new_data = [];

		foreach ( $data as $key => $value ) {
			if ( 'icon' === $key && is_string( $value ) ) {
				$icon_data = $this->get_encoded_icon( $value );

				if ( $icon_data ) {
					$new_data[ $key ] = $icon_data;
				}
			} elseif ( 'help_url' === $key && is_string( $value ) ) {
				if ( ! $this->is_url_available( $value ) ) {
					$new_data[ $key ] = '';
				}
			} elseif ( 'read_more' === $key && is_array( $value ) ) {
				$new_data[ $key ] = [];
				foreach ( $value as $read_more_url => $read_more_label ) {
					if ( $this->is_url_available( $read_more_url ) ) {
						$new_data[ $key ][ $read_more_url ] = $read_more_label;
					}
				}
			} elseif ( is_array( $value ) ) {
				$new_data[ $key ] = $this->sanitize_data_array( $value );
			}

			if ( ! isset( $new_data[ $key ] ) ) {
				$new_data[ $key ] = $value;
			}
		}

		return $new_data;
	}

	/* ----------------------- JS templates for components ---------------------- */

	/**
	 * Adds a JS template with the given name.
	 *
	 * All templates will be output by the function output_templates() and result in a
	 * tag with the following format:
	 *
	 * <script type="text/html" id="tmpl-divimode-{name}">{code}</script>
	 *
	 * Those templates are parsed by the Divimode javascript library and are
	 * automatically detected and initialized by the Divimode JS API via
	 * `Divimode.Templates[ {name} ]`. Example: `var $title =
	 * Divimode.Templates['titlebar']({ title: 'Demo' });`
	 *
	 * @throws DM_Exception_Template When the specified template name was
	 *         defined before.
	 *
	 * @since 0.2.0
	 * @param string          $name The template identifier. This must be a valid
	 *                              attribute value in lower-case as it is used in
	 *                              the script tag that defines the template.
	 * @param string|callback $code The HTML code of the template. More details about
	 *                              logic and placeholders inside this template code
	 *                              is found at https://codex.wordpress.org/Javascript_Reference/wp.template.
	 */
	final public static function set_template( $name, $code ) {
		// Validate the name.
		$valid_name = strtolower( esc_attr( $name ) );
		if ( $valid_name !== $name ) {
			throw new DM_Exception_Template( "The template name is invalid. Expected '{$valid_name}' but found '{$name}'" );
		}

		// Make sure the template is not overwritten accidentally.
		if ( isset( self::$templates[ $name ] ) ) {
			throw new DM_Exception_Template( "The template '{$name}' is already defined and cannot be overwritten" );
		}

		if ( is_callable( $code ) ) {
			ob_start();
			$code();
			$code = ob_get_clean();
		}

		// Make sure the template is not overwritten accidentally.
		if ( ! is_string( $code ) ) {
			$type = gettype( $code );
			throw new DM_Exception_Template( "The template '{$name}' must be a string value, but {$type} given" );
		}

		self::$templates[ $name ] = trim( $code );
	}

	/**
	 * Determines, whether the given template is already defined.
	 *
	 * @since 0.2.0
	 * @param string $name See set_template() for details.
	 * @return bool
	 */
	final public static function has_template( $name ) {
		return isset( self::$templates[ $name ] );
	}

	/**
	 * Clears an existing template so it can be set again.
	 *
	 * @since 0.2.0
	 * @param string $name See set_template() for details.
	 */
	final public static function clear_template( $name ) {
		if ( ! isset( self::$output_templates[ $name ] ) ) {
			unset( self::$templates[ $name ] );
		}
	}

	/**
	 * Outputs all registered custom JS templates.
	 *
	 * @since 0.2.0
	 * @return void
	 */
	final public function output_custom_templates() {
		// Allow child classes to register their custom templates.
		$this->prepare_templates();

		foreach ( self::$templates as $key => $code ) {
			if ( isset( self::$output_templates[ $key ] ) ) {
				continue;
			}
			self::$output_templates[ $key ] = true;

			printf(
				'<script type="text/html" id="tmpl-divimode-%1$s" data-divimode-template="%1$s">%2$s</script>',
				esc_attr( $key ),
				$code // phpcs:ignore
			);
		}
	}

	/**
	 * Prompts all used components to register their JS templates.
	 *
	 * @since 0.2.0
	 * @return void
	 */
	final private function prepare_templates() {
		$classes = array_keys( self::$registered_classes );

		foreach ( $classes as $component_type ) {
			$component_type::setup_templates();
		}
	}

	/**
	 * Event hook for child classes to register their custom templates.
	 *
	 * @since 0.2.0
	 * @return void
	 */
	protected static function setup_templates() {}


	/* -------------------------- Misc helper functions ------------------------- */


	/**
	 * Returns a base-64 encoded version of the icon when the file exists. Otherwise,
	 * an empty string is returned.
	 *
	 * When the icon is an SVG image, the SVG code is returned unencoded.
	 *
	 * @since 0.2.0
	 * @param string $icon_value The icon path (absolute).
	 * @return string Either an encoded icon or an empty string.
	 */
	protected function get_encoded_icon( $icon_value ) {
		$result = '';

		if ( file_exists( $icon_value ) ) {
			// phpcs:ignore WordPress.WP.AlternativeFunctions.file_get_contents_file_get_contents
			$image = file_get_contents( $icon_value );
			$type  = strtolower( substr( $icon_value, strrpos( $icon_value, '.' ) + 1 ) );

			if ( 'svg' === $type ) {
				$result = $image;
			} else {
				$mime_type = '';

				if ( 'png' === $type ) {
					$mime_type = 'image/png';
				} elseif ( 'jpg' === $type || 'jpeg' === $type ) {
					$mime_type = 'image/jpeg';
				}

				if ( ! empty( $mime_type ) ) {
					// phpcs:ignore WordPress.PHP.DiscouragedPHPFunctions.obfuscation_base64_encode
					$encoded = base64_encode( $image );
					$result  = "data:{$mime_type};base64,{$encoded}";
				}
			}
		}

		return $result;
	}

	/**
	 * Checks, if the given URL returns a valid 200 status code.
	 *
	 * URLs that failed will be checked again after 24 hours. URLs that passed will be
	 * checked again after 2 weeks.
	 *
	 * This method allows us to insert links for read-more/help content that are still
	 * in draft state during release and have them magically appear in the UI once we
	 * publish them. That way, we do not need to wait for all documentation/editing to
	 * push a release, but can deliver those help links whenever they are ready.
	 *
	 * @since 0.2.0
	 * @param string $url The URL to check.
	 * @return bool
	 */
	public function is_url_available( $url ) {
		// Max length is 172 characters.
		$cache_key     = 'divimode_url_states';
		$url_hash      = md5( $url );
		$state_found   = (array) get_option( $cache_key );
		$state_missing = (array) get_transient( $cache_key );

		if (
			! isset( $state_found[ $url_hash ] )
			&& ! isset( $state_missing[ $url_hash ] )
		) {
			$request = wp_remote_get( $url );
			$status  = wp_remote_retrieve_response_code( $request );

			if ( is_wp_error( $request ) || ! is_numeric( $status ) || ! $status ) {
				$status = 500;
			}

			if ( 200 === $status ) {
				$state_found[ $url_hash ] = $status;
				unset( $state_missing[ $url_hash ] );
			} else {
				unset( $state_found[ $url_hash ] );
				$state_missing[ $url_hash ] = $status;
			}

			update_option( $cache_key, $state_found );
			set_transient( $cache_key, $state_missing, DAY_IN_SECONDS );
		}

		return isset( $state_found[ $url_hash ] );
	}
}
