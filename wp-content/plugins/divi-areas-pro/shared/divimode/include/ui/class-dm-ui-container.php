<?php
/**
 * Metabox UI helper class
 *
 * @package Divimode_UI
 */

namespace DiviAreasPro;

/**
 * Abstract base class for all containers.
 * A Container can hold and display multiple components.
 *
 * @since 0.2.0
 */
abstract class DM_UI_Container extends DM_UI_Component {

	/**
	 * List of child components.
	 *
	 * @var DM_UI_Component[]
	 */
	private $children = [];

	/**
	 * Returns a list of reserved component property-names.
	 *
	 * @since 0.2.0
	 * @return array
	 */
	protected function get_reserved_props() {
		$reserved   = parent::get_reserved_props();
		$reserved[] = 'children';

		return $reserved;
	}

	/**
	 * Returns the internal data (i.e. the state) of the current component and all
	 * its children.
	 *
	 * @since 0.2.0
	 * @return array
	 */
	public function get_data() {
		if ( empty( $this->child_attribute ) ) {
			$child_attribute = 'children';
		} else {
			$child_attribute = $this->child_attribute;
		}

		$this->props[ $child_attribute ] = [];

		$data = parent::get_data();

		foreach ( $this->children as $component ) {
			$data[ $child_attribute ][] = $component->get_data();
		}

		return $data;
	}

	/**
	 * Adds a new child component to the container.
	 *
	 * @since 0.2.0
	 * @param DM_UI_Component $component The component to add.
	 * @return DM_UI_Component Returns the child object for initialization.
	 */
	protected function add_child( DM_UI_Component $component ) {
		$child_key = $component->get_component_key();

		// Track the child component by its unique key.
		$this->children[ $child_key ] = $component;

		return $component;
	}

	/**
	 * Defines the source file with the predefined container contents.
	 * The template file will be used to initialize the container children.
	 *
	 * $container->container_source( '', '/template/src' )
	 *   => will load "/template/src/{id}.php"
	 *
	 * $container->container_source( 'some-file.php', '/template/src' )
	 *   => will load "/template/src/some-file.php"
	 *
	 * $container->container_source( '/template/alt/file.php', '/template/src' )
	 *   => will load "/template/alt/file.php"
	 *
	 * @since 0.2.0
	 * @param string $template_file The template file path. Either an absolute path
	 *                              or a file path that is relative to $source_dir.
	 * @param string $source_dir    Optional. When $template_file is a relative path
	 *                              this param defines the root folder to search.
	 * @return static
	 */
	public function container_source( $template_file, $source_dir = '' ) {
		$source_path = '';

		if ( path_is_absolute( $template_file ) ) {
			if ( file_exists( $template_file ) ) {
				$source_path = $template_file;
			}
		} else {
			$source_dir  = trailingslashit( $source_dir );
			$source_data = [];

			if ( ! $template_file ) {
				$template_file = $this->get_id() . '.php';
			}

			$source_path = "{$source_dir}{$template_file}";
		}

		/**
		 * The container source file should return a single array that
		 * defines all components inside the container.
		 *
		 * A basic source file could look like the following:
		 *
		 * return [
		 *     'section_key' => [
		 *         'label' => 'Section Name',
		 *         'type'  => 'section',
		 *         'items' => [
		 *             'field_key'   => [
		 *                 'label'       => 'Field Label',
		 *                 'type'        => 'text',
		 *                 'description' => 'Field description.',
		 *             ], // end of field configuration.
		 *         ], // end of section items.
		 *     ], // end of section configuration.
		 * ]; // end of container item list.
		 */
		if ( $source_path && file_exists( $source_path ) ) {
			$source_data = require $source_path;
		}

		if ( $source_data && is_array( $source_data ) ) {
			$this->import_children( $source_data );
		}

		return $this;
	}

	/**
	 * Import child components from the given configuration array.
	 *
	 * @since 0.2.0
	 * @param array $details The configuration to import.
	 * @return void
	 */
	public function import_children( array $details ) {
		foreach ( $details as $key => $item ) {
			if ( ! $item || ! is_array( $item ) || empty( $item['type'] ) ) {
				continue;
			}

			$component = DM_UI_Component::create( $item['type'], $key );
			$component->configure( $item );
			$this->add_child( $component );
		}
	}

	/**
	 * Generic importer to configure the component based on the given config-array.
	 *
	 * @since 0.2.0
	 * @param array $details  The configuration to import.
	 * @return void
	 */
	public function configure( array $details ) {
		if ( empty( $this->child_attribute ) ) {
			$child_attribute = 'items';
		} else {
			$child_attribute = $this->child_attribute;
		}

		if ( ! empty( $details[ $child_attribute ] ) ) {
			$this->import_children( $details[ $child_attribute ] );
		}
		unset( $details[ $child_attribute ] );

		parent::configure( $details );
	}
}
