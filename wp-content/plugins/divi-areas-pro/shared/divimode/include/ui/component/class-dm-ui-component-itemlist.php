<?php
/**
 * UI helper class
 *
 * @package Divimode_UI
 */

namespace DiviAreasPro;

/**
 * Item List component
 *
 * @trait DM_UI_Prop_Label
 * @trait DM_UI_Prop_Description
 * @trait DM_UI_Prop_Form
 * @trait DM_UI_Prop_Conditions
 *
 * @since 0.2.0
 */
class DM_UI_Component_Itemlist extends DM_UI_Container {

	/**
	 * Defines the title of the control.
	 */
	use DM_UI_Prop_Label;

	/**
	 * Defines the description of the field.
	 */
	use DM_UI_Prop_Description;

	/**
	 * Defines the form contents of the dialog.
	 */
	use DM_UI_Prop_Form;

	/**
	 * Defines conditions of the component.
	 */
	use DM_UI_Prop_Conditions;

	/**
	 * Defines the component type for the JS state object.
	 *
	 * @var string
	 */
	protected $component_type = 'itemlist';

	/**
	 * Caption of the "add new" button.
	 *
	 * @since 0.2.0
	 * @var string
	 */
	protected $prop_label_add_item = '';

	/**
	 * Title of the "remove item" form.
	 *
	 * @since 0.2.0
	 * @var string
	 */
	protected $prop_label_remove_item = '';

	/**
	 * Title of the "create item" dialog.
	 *
	 * @since 0.2.0
	 * @var string
	 */
	protected $prop_label_create_item = '';

	/**
	 * Title of the "edit item" dialog.
	 *
	 * @since 0.2.0
	 * @var string
	 */
	protected $prop_label_edit_item = '';

	/**
	 * Initial label of new items.
	 *
	 * @since 0.2.0
	 * @var string
	 */
	protected $prop_label_item = '';

	/**
	 * Field ID of the label field inside the form.
	 *
	 * @since 0.2.2
	 * @var string
	 */
	protected $prop_label_id = 'label';

	/**
	 * Whether the "add item" button is displayed.
	 *
	 * @since 0.2.0
	 * @var bool
	 */
	protected $prop_enable_add = true;

	/**
	 * Whether items can be deleted from the list.
	 *
	 * @since 0.2.0
	 * @var bool
	 */
	protected $prop_enable_delete = true;

	/**
	 * Whether item names can be changed in the list.
	 *
	 * @since 0.2.0
	 * @var bool
	 */
	protected $prop_enable_rename = true;

	/**
	 * Whether items have an edit dialog (also require a form).
	 *
	 * @since 0.2.0
	 * @var bool
	 */
	protected $prop_enable_edit = true;

	/**
	 * Whether items can be sorted by drag-and-drop.
	 *
	 * @since 0.2.0
	 * @var bool
	 */
	protected $prop_enable_sort = true;

}
