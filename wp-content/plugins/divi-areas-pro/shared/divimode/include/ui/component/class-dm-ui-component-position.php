<?php
/**
 * UI helper class
 *
 * @package Divimode_UI
 */

namespace DiviAreasPro;

/**
 * Position selector component.
 *
 * @trait DM_UI_Prop_Label
 * @trait DM_UI_Prop_Description
 * @trait DM_UI_Prop_Default
 * @trait DM_UI_Prop_Options
 * @trait DM_UI_Prop_Responsive
 * @trait DM_UI_Prop_Conditions
 *
 * @since 0.2.0
 */
class DM_UI_Component_Position extends DM_UI_Component {

	/**
	 * Defines the title of the field.
	 */
	use DM_UI_Prop_Label;

	/**
	 * Defines the description of the field.
	 */
	use DM_UI_Prop_Description;

	/**
	 * Defines the default value of the field.
	 */
	use DM_UI_Prop_Default;

	/**
	 * Defines the toggle options of the field.
	 */
	use DM_UI_Prop_Options;

	/**
	 * Defines the responsive-state of the field.
	 */
	use DM_UI_Prop_Responsive;

	/**
	 * Defines conditions of the component.
	 */
	use DM_UI_Prop_Conditions;

	/**
	 * Defines the component type for the JS state object.
	 *
	 * @var string
	 */
	protected $component_type = 'position';
}
