<?php
/**
 * UI helper class
 *
 * @package Divimode_UI
 */

namespace DiviAreasPro;

/**
 * Timestamp component (date and time picker).
 *
 * @trait DM_UI_Prop_Label
 * @trait DM_UI_Prop_Description
 * @trait DM_UI_Prop_Default
 * @trait DM_UI_Prop_DateTime
 * @trait DM_UI_Prop_Conditions
 *
 * @since 0.2.2
 */
class DM_UI_Component_Timestamp extends DM_UI_Component {

	/**
	 * Defines the title of the field.
	 */
	use DM_UI_Prop_Label;

	/**
	 * Defines the description of the field.
	 */
	use DM_UI_Prop_Description;

	/**
	 * Defines the default value of the field.
	 */
	use DM_UI_Prop_Default;

	/**
	 * Defines the date and time configuration.
	 */
	use DM_UI_Prop_DateTime;

	/**
	 * Defines conditions of the component.
	 */
	use DM_UI_Prop_Conditions;

	/**
	 * Defines the component type for the JS state object.
	 *
	 * @var string
	 */
	protected $component_type = 'timestamp';


	/**
	 * Remembers, whether the js-data hook was added already.
	 *
	 * @since 0.2.2
	 * @var bool
	 */
	private static $added_hook = false;

	/**
	 * The post-selector requires additional data to work.
	 *
	 * @since 0.2.2
	 */
	public function __construct() {
		parent::__construct();

		$this->add_hooks();
	}

	/**
	 * Hooks into the js-data filter to provide additional details to the JS app.
	 *
	 * @since 0.2.2
	 * @return void
	 */
	private function add_hooks() {
		// We only need to add the filter-hook once.
		if ( self::$added_hook ) {
			return;
		}

		// Retrieve the slug from our own namespace.
		$slug             = DM_Library::inst()->get_settings()->get_slug();
		self::$added_hook = true;

		add_filter(
			"divimode_js_config_{$slug}",
			[ $this, 'javascript_dm_config' ]
		);
	}

	/**
	 * Filter for the global JS object `dm_config` where we add configuration for the
	 * timestamp component.
	 *
	 * @since 0.2.2
	 * @param array $config Settings that will be written to `window.dm_config`.
	 * @return array Modified settings array.
	 */
	public function javascript_dm_config( $config ) {
		$date_form = get_option( 'date_format' );
		$time_form = get_option( 'time_format' );

		/**
		 * Translate PHP format token to JS/Flatpickr options.
		 *
		 * @see https://flatpickr.js.org/formatting/
		 * @see https://www.php.net/manual/en/function.date.php
		 */
		$date_form = str_replace(
			[ 'jS', 'dS', 'o' ],
			[ 'J', 'J', 'Y' ],
			$date_form
		);
		$time_form = str_replace(
			[ 'a', 'A', 'g', 'G', 'h', 'H', 'i', 's' ],
			[ 'K', 'K', 'h', 'H', 'G', 'H', 'i', 's' ],
			$time_form
		);

		$config['i18n']['timestamp_date']      = $date_form;
		$config['i18n']['timestamp_date_time'] = "$date_form ($time_form)";
		$config['i18n']['timestamp_time']      = $time_form;

		return $config;
	}
}
