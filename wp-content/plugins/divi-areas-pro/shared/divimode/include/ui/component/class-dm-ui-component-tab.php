<?php
/**
 * UI helper class
 *
 * @package Divimode_UI
 */

namespace DiviAreasPro;

/**
 * Tab component
 *
 * @trait DM_UI_Prop_Label
 * @trait DM_UI_Prop_Icon
 * @trait DM_UI_Prop_Conditions
 *
 * @since 0.2.0
 */
class DM_UI_Component_Tab extends DM_UI_Container {

	/**
	 * Defines the title of the tab.
	 */
	use DM_UI_Prop_Label;

	/**
	 * Defines the icon of the tab.
	 */
	use DM_UI_Prop_Icon;

	/**
	 * Defines conditions of the component.
	 */
	use DM_UI_Prop_Conditions;

	/**
	 * Defines the component type for the JS state object.
	 *
	 * @var string
	 */
	protected $component_type = 'tab';
}
