<?php
/**
 * UI helper class
 *
 * @package Divimode_UI
 */

namespace DiviAreasPro;

/**
 * Section Footer component
 *
 * @trait DM_UI_Prop_Label
 * @trait DM_UI_Prop_Conditions
 *
 * @since 0.2.0
 */
class DM_UI_Component_Footer extends DM_UI_Container {

	/**
	 * Defines HTML contents of the component.
	 */
	use DM_UI_Prop_Html;

	/**
	 * Defines conditions of the component.
	 */
	use DM_UI_Prop_Conditions;

	/**
	 * Defines the Help-Button of the component.
	 */
	use DM_UI_Prop_Help;

	/**
	 * Defines the component type for the JS state object.
	 *
	 * @var string
	 */
	protected $component_type = 'footer';
}
