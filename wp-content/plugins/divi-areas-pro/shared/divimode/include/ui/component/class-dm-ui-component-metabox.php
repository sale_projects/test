<?php
/**
 * Metabox UI helper class
 *
 * @package Divimode_UI
 */

namespace DiviAreasPro;

/**
 * UI Metabox Tab.
 * The metabox holds a list of multiple tabs.
 * Each tab contains sections.
 * Each section contains fields.
 *
 * @trait DM_UI_Prop_Collapsable
 * @trait DM_UI_Prop_Draggable
 *
 * @since 0.2.0
 */
class DM_UI_Component_Metabox extends DM_UI_Container {

	/**
	 * Defines the collapsable feature.
	 */
	use DM_UI_Prop_Collapsable;

	/**
	 * Defines the collapsable feature.
	 */
	use DM_UI_Prop_Draggable;

	/**
	 * Absolute path to the folder that contains the tab template files.
	 *
	 * @var string
	 */
	private $source_dir = '';

	/**
	 * Toolbar items.
	 *
	 * @var array
	 */
	protected $prop_tools = [];

	/**
	 * Defines the component type for the JS state object.
	 *
	 * @since 0.2.0
	 * @var string
	 */
	protected $component_type = 'metabox';

	/**
	 * Hooks into the WP Metabox API to register the custom metabox.
	 *
	 * @since  0.2.0
	 * @param  string $id           The metabox ID.
	 * @param  string $title        The metabox title.
	 * @param  string $screen       Screen (post type) that uses this metabox.
	 * @param  string $source_dir   Absolute path to the template files directory.
	 * @param  string $icon         Optional. Absolute URL to the metabox icon.
	 *                              Defaults to ''.
	 */
	public function init( $id, $title, $screen, $source_dir, $icon = '' ) {
		$this->set_id( $id );
		$this->source_dir = $source_dir;

		if ( $icon ) {
			/*
			SVG files are loaded and displayed as inline objects. Other files are
			displayed as img-tag. We use the "://" check, to make sure that only local
			files are loaded (via file_get_contents) and URLs are ignored. In that
			regards, the order of the conditions is important: file_exists() will also
			query remote URLs, so it should be the last check in the condition.
			*/
			if (
				stripos( $icon, '.svg' )
				&& false === stripos( $icon, '://' )
				&& file_exists( $icon )
			) {
				// phpcs:ignore WordPress.WP.AlternativeFunctions.file_get_contents_file_get_contents
				$icon = file_get_contents( $icon );

				if ( false !== stripos( $icon, '<svg' ) ) {
					$icon = substr( $icon, stripos( $icon, '<svg' ) );
				}
				$title = sprintf(
					'<span class="metabox-icon" style="display:none">%s</span><span class="metabox-title">%s</span>',
					substr( $icon, stripos( $icon, '<svg' ) ),
					$title
				);
			} else {
				$title = sprintf(
					'<span class="metabox-icon" style="display:none"><img src="%s" /></span><span class="metabox-title">%s</span>',
					$icon,
					$title
				);
			}
		}

		add_meta_box(
			'divimode_' . $this->get_id(),
			$title,
			[ $this, 'render' ],
			$screen,
			'normal'
		);
	}

	/**
	 * Imports component configuration from an config array.
	 *
	 * @throws DM_Exception_Configure Always throws an exception.
	 *
	 * @since 0.2.0
	 * @param array $details The configuration to import.
	 * @return void
	 */
	public function configure( array $details ) {
		throw new DM_Exception_Configure( 'A metabox can only be configured via the init() method' );
	}

	/**
	 * Add a new section to the current metabox.
	 *
	 * List of available glyphs for the icon value:
	 * https://www.elegantthemes.com/blog/resources/elegant-icon-font#glyphs
	 *
	 * @since 0.2.0
	 * @param string $id       The tab ID.
	 * @param string $title    The tab title.
	 * @param string $template Optional. Absolute path to the template file of the
	 *                         tab. Default template-path is built from
	 *                         $this->source_dir and $id.
	 * @return static Reference for chaining.
	 */
	public function add_tab( $id, $title, $template = '' ) {
		/**
		 * The new tab component.
		 *
		 * @var DM_UI_Component_Tab
		 */
		$component = self::create( 'tab', 'tab_' . $id );

		if ( $component ) {
			if ( ! $template ) {
				$template = "{$id}.php";
			}

			$component->set_label( $title )
				->container_source(
					$template,
					"{$this->source_dir}/{$this->id}-tabs/"
				);

			$this->add_child( $component );
		}

		return $this;
	}

	/**
	 * Registers a custom toolbar icon that will open an URL when clicked.
	 *
	 * @since 2.0.0
	 * @param string $icon       The tool-button icon.
	 * @param string $url        The target URL.
	 * @param string $label      Optional. The tooltip label.
	 * @param string $class_name Optional. A CSS class to apply to the button.
	 * @return static Reference for chaining.
	 */
	public function add_tool( $icon, $url, $label = '', $class_name = '' ) {
		$this->prop_tools[] = [
			'icon'  => $icon,
			'url'   => esc_url( $url ),
			'label' => $label,
			'class' => $class_name,
		];
		$this->set_data( 'tools', $this->prop_tools );

		return $this;
	}

	/**
	 * Returns a list of reserved component property-names.
	 *
	 * @since 0.2.0
	 * @return array
	 */
	protected function get_reserved_props() {
		$reserved   = parent::get_reserved_props();
		$reserved[] = 'selector';

		return $reserved;
	}

	/**
	 * Returns the internal data (i.e. the state) of the current component and all
	 * its children.
	 *
	 * @since 0.2.0
	 * @return array
	 */
	public function get_data() {
		$data = parent::get_data();

		/*
		Add the JS selector to identify the WP postbox.
		The class "postbox" is added by WordPress. The ID "#divimode_{id}" is defined
		in the function self::init()
		*/
		$data['selector'] = '.postbox#divimode_' . $this->get_id();

		return $data;
	}

	/**
	 * Render a metabox in the Divi Areas editor screen.
	 *
	 * @since  0.2.0
	 * @param  WP_Post $post The rendered post.
	 * @param  array   $box  The metabox details.
	 * @return void
	 */
	public function render( $post, $box ) {
		$data     = [];
		$data_key = DM_Library::inst()->get_settings()->get_db_name( $this->get_id() );

		if ( $post && is_a( $post, 'WP_Post' ) ) {
			$data = get_post_meta( $post->ID, $data_key, true );
		} elseif ( 'options' === $post ) {
			$data = get_option( $data_key, [] );
		}

		$this->set_data( 'data', $data );

		// Output a nonce.
		wp_nonce_field(
			"nonce:metabox-{$this->get_id()}",
			"dm_{$this->get_id()}_nonce"
		);

		// Register this metabox for the save-handler.
		printf(
			'<input type="hidden" name="dm_boxes[]" value="%s" />',
			esc_attr( $this->get_id() )
		);

		// Add an empty container that will hold/dispaly the metabox component.
		echo '<div class="inner-content" role="form"></div>';

		// Outputs the JS data of this metabox (and all children).
		$this->output_data();

		// Output custom JS templates that are used by any registered component.
		$this->output_custom_templates();
	}

	/**
	 * Outputs the JS Data of the current component and all its children.
	 *
	 * The data is passed to the JS application by extending the return value of the
	 * js filter "dm_register_metaboxes".
	 *
	 * @since 0.2.0
	 * @return void
	 */
	protected function output_data() {
		$data = $this->get_data();

		printf(
			'<script>(function(){wp.hooks.addFilter("dm_register_metaboxes","divimode",function(res){res["%s"]=%s;return res;});})()</script>',
			esc_js( $this->id ),
			wp_json_encode( $data )
		);
	}
}
