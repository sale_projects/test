<?php
/**
 * UI helper class
 *
 * @package Divimode_UI
 */

namespace DiviAreasPro;

/**
 * Textfield component.
 *
 * @trait DM_UI_Prop_Label
 * @trait DM_UI_Prop_Description
 * @trait DM_UI_Prop_Default
 * @trait DM_UI_Prop_Multiline
 * @trait DM_UI_Prop_NumericValues
 * @trait DM_UI_Prop_HasSelect
 * @trait DM_UI_Prop_Responsive
 * @trait DM_UI_Prop_ReadOnly
 * @trait DM_UI_Prop_Conditions
 *
 * @since 0.2.0
 */
class DM_UI_Component_Text extends DM_UI_Component {

	/**
	 * Defines the title of the field.
	 */
	use DM_UI_Prop_Label;

	/**
	 * Defines the description of the field.
	 */
	use DM_UI_Prop_Description;

	/**
	 * Defines the default value of the field.
	 */
	use DM_UI_Prop_Default;

	/**
	 * Allows configuration of numeric input values of the field.
	 */
	use DM_UI_Prop_Multiline;

	/**
	 * Allows configuration of numeric input values of the field.
	 */
	use DM_UI_Prop_NumericValues;

	/**
	 * Allows to enable a select field with additional text options.
	 */
	use DM_UI_Prop_HasSelect;

	/**
	 * Defines the responsive-state of the field.
	 */
	use DM_UI_Prop_Responsive;

	/**
	 * Defines the readonly-state of the field.
	 */
	use DM_UI_Prop_ReadOnly;

	/**
	 * Defines conditions of the component.
	 */
	use DM_UI_Prop_Conditions;

	/**
	 * Defines the component type for the JS state object.
	 *
	 * @var string
	 */
	protected $component_type = 'text';
}
