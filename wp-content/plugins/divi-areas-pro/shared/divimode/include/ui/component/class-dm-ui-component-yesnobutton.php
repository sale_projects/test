<?php
/**
 * UI helper class
 *
 * @package Divimode_UI
 */

namespace DiviAreasPro;

/**
 * Yes-No Toggler component.
 *
 * @trait DM_UI_Prop_Label
 * @trait DM_UI_Prop_Description
 * @trait DM_UI_Prop_Default
 * @trait DM_UI_Prop_Options
 * @trait DM_UI_Prop_Hints
 * @trait DM_UI_Prop_Responsive
 * @trait DM_UI_Prop_Conditions
 *
 * @since 0.2.0
 */
class DM_UI_Component_YesNoButton extends DM_UI_Component {

	/**
	 * Defines the title of the field.
	 */
	use DM_UI_Prop_Label;

	/**
	 * Defines the description of the field.
	 */
	use DM_UI_Prop_Description;

	/**
	 * Defines the default value of the field.
	 */
	use DM_UI_Prop_Default;

	/**
	 * Defines the toggle options of the field.
	 */
	use DM_UI_Prop_Options;

	/**
	 * Defines the hint texts of the field.
	 */
	use DM_UI_Prop_Hints;

	/**
	 * Defines the responsive-state of the field.
	 */
	use DM_UI_Prop_Responsive;

	/**
	 * Defines conditions of the component.
	 */
	use DM_UI_Prop_Conditions;

	/**
	 * Defines the component type for the JS state object.
	 *
	 * @var string
	 */
	protected $component_type = 'yes_no_button';
}
