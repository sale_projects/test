<?php
/**
 * UI helper class
 *
 * @package Divimode_UI
 */

namespace DiviAreasPro;

/**
 * Post-List selector component.
 *
 * @trait DM_UI_Prop_Label
 * @trait DM_UI_Prop_Description
 * @trait DM_UI_Prop_Conditions
 *
 * @since 0.2.0
 */
class DM_UI_Component_PostList extends DM_UI_Component {

	/**
	 * Defines the title of the field.
	 */
	use DM_UI_Prop_Label;

	/**
	 * Defines the description of the field.
	 */
	use DM_UI_Prop_Description;

	/**
	 * Defines conditions of the component.
	 */
	use DM_UI_Prop_Conditions;

	/**
	 * Defines the component type for the JS state object.
	 *
	 * @var string
	 */
	protected $component_type = 'post_list';

	/**
	 * Remembers, whether the js-data hook was added already.
	 *
	 * @since 0.2.0
	 * @var bool
	 */
	private static $added_hook = false;

	/**
	 * The post-selector requires additional data to work.
	 *
	 * @since 0.2.0
	 */
	public function __construct() {
		parent::__construct();

		$this->add_hooks();
	}

	/**
	 * Hooks into the js-data filter to provide additional details to the JS app.
	 *
	 * @since 0.2.0
	 * @return void
	 */
	private function add_hooks() {
		// We only need to add the filter-hook once.
		if ( self::$added_hook ) {
			return;
		}

		// Retrieve the slug from our own namespace.
		$slug             = DM_Library::inst()->get_settings()->get_slug();
		self::$added_hook = true;

		add_filter(
			"divimode_js_config_{$slug}",
			[ $this, 'javascript_dm_config' ],
			10,
			2
		);
	}

	/**
	 * Filter for the global JS object `dm_config` where we add configuration for the
	 * post-list component.
	 *
	 * @uses et_theme_builder_get_template_settings_options()
	 *
	 * @since 0.2.0
	 * @param array  $config Settings that will be written to `window.dm_config`.
	 * @param string $db_prefix Prefix for postmeta/option keys.
	 * @return array Modified settings array.
	 */
	public function javascript_dm_config( $config, $db_prefix ) {
		if ( divi_area_vb_version( '4.0.0' ) ) {
			// We use Divi's Theme-Builder logic to do the hard work.
			$default_settings = [];

			// Remove extra details from the post-list settings.
			$whitelist_group    = [
				'label'    => true,
				'settings' => true,
			];
			$whitelist_settings = [
				'id'      => true,
				'label'   => true,
				'options' => true,
			];
			foreach ( et_theme_builder_get_template_settings_options() as $group_key => $group ) {
				$group = array_intersect_key( $group, $whitelist_group );

				foreach ( $group['settings'] as $setting_key => $setting ) {
					$group['settings'][ $setting_key ] = array_intersect_key(
						$setting,
						$whitelist_settings
					);
				}

				$default_settings[ $group_key ] = $group;
			}

			/**
			 * Meta Infos is a list of all posts and their label that need to be displayed
			 * immediately when the page is rendered.
			 *
			 * @since 0.2.0
			 * @var array
			 */
			$meta_infos = [];

			if ( ! empty( $config['post'] ) && is_array( $config['post'] ) ) {
				$templates    = $this->get_template_ids_for_preloading(
					$config['post']['ID'],
					$db_prefix
				);
				$template_obj = $this->get_template_settings_options_for_preloading(
					$templates
				);

				foreach ( $template_obj as $obj ) {
					$meta_infos[ $obj['id'] ] = $obj['label'];
				}
			}

			$default_settings['_meta']                     = $meta_infos;
			$config['components'][ $this->component_type ] = $default_settings;
		}

		return $config;
	}

	/**
	 * Parses the given posts postmeta fields and returns a list of all template IDs that are detected which need preloading.
	 *
	 * @since 0.2.0
	 * @param int    $post_id   The post ID to process.
	 * @param string $db_prefix Prefix for postmeta keys.
	 * @return string[] List of template IDs.
	 */
	private function get_template_ids_for_preloading( $post_id, $db_prefix = '_dm_' ) {
		$res       = [];
		$post_meta = get_post_meta( $post_id );

		if ( ! $post_meta || ! is_array( $post_meta ) ) {
			return $res;
		}

			// Find all divimode metabox values in the postmeta array.
		foreach ( $post_meta as $meta_key => $meta_value ) {
			if ( 0 !== strpos( $meta_key, $db_prefix ) || ! is_array( $meta_value ) ) {
				continue;
			}

			$data = maybe_unserialize( $meta_value[0] );

			if ( ! $data || ! is_array( $data ) ) {
				continue;
			}

			// Find all post-list values in the postmeta value object.
			foreach ( $data as $field_name => $field_value ) {
				if ( ! is_array( $field_value ) ) {
					continue;
				}

				foreach ( $field_value as $item  => $mode ) {
					if (
						! is_string( $item )
						|| false === strpos( $item, \ET_THEME_BUILDER_SETTING_SEPARATOR )
					) {
						continue;
					}
					$parts = explode( \ET_THEME_BUILDER_SETTING_SEPARATOR, $item );

					if ( count( $parts ) < 5 || 'id' !== $parts[3] ) {
						continue;
					}

					$res[] = $item;
				}
			}
		}

		return $res;
	}

	/**
	 * Get flat array of template setting options from the selected postlist items of
	 * the given post.
	 *
	 * @since 0.2.0
	 * @param array $templates List of template IDs to preload.
	 * @return array[] List of post-names.
	 */
	protected function get_template_settings_options_for_preloading( $templates ) {
		return et_theme_builder_load_template_setting_options( array_unique( $templates ) );
	}

	/**
	 * Ajax handler that returns a list of posts, terms, users, etc.
	 *
	 * The nonce was already validated before this method was called, so we do not
	 * need/want an additional validation here. Those validations are done by
	 * DM_Library.
	 *
	 * @see DM_Library::ajax_add_handler()
	 * @see DM_Library::ajax_check_nonce()
	 *
	 * @action divimode_component_post_list_get_items
	 *
	 * @uses et_theme_builder_get_flat_template_settings_options()
	 * @uses et_theme_builder_get_template_setting_child_options()
	 *
	 * @since 0.2.0
	 */
	public static function ajax_get_items() {
		// phpcs:disable WordPress.Security.NonceVerification.Missing
		// phpcs:disable WordPress.Security.ValidatedSanitizedInput.MissingUnslash
		$parent   = isset( $_POST['parent'] ) ? sanitize_text_field( $_POST['parent'] ) : '';
		$per_page = isset( $_POST['per_page'] ) ? absint( $_POST['per_page'] ) : 30;
		$settings = et_theme_builder_get_flat_template_settings_options();

		if ( ! isset( $settings[ $parent ] ) || empty( $settings[ $parent ]['options'] ) ) {
			wp_send_json_error(
				[ 'message' => 'Invalid parent setting specified.' ]
			);
		}

		$per_page = max( 5, $per_page );

		$results = et_theme_builder_get_template_setting_child_options(
			$settings[ $parent ],
			[],
			isset( $_POST['search'] ) ? sanitize_text_field( $_POST['search'] ) : '',
			isset( $_POST['page'] ) ? max( 1, (int) $_POST['page'] ) : 1,
			$per_page
		);
		// phpcs:enable

		wp_send_json_success(
			[
				'results'  => array_values( $results ),
				'per_page' => $per_page,
			]
		);
	}
}
