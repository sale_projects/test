<?php
/**
 * Main access point to the shared divimode library.
 *
 * @package Divimode_UI
 */

namespace DiviAreasPro;

use phpseclib\System\SSH\Agent\Identity;

/**
 * Library class that acts as an API for other elements and functions in this library.
 *
 * @since 0.2.0
 */
class DM_Library {

	/**
	 * UI module configuration that contains details related to the parent plugin,
	 * such as translations or URLs.
	 * Can be set by `$this->config($key, $value)`.
	 *
	 * @since 0.2.0
	 * @var array
	 */
	protected $config = [];

	/**
	 * Settings module instance, created by `$this->get_settings()`
	 *
	 * @since 0.2.0
	 * @var DM_Settings
	 */
	protected $settings = null;

	/**
	 * Logger instance, created by `$this->get_logger()`
	 *
	 * @since 0.2.0
	 * @var DM_Logger
	 */
	protected $logger = null;

	/**
	 * Holds a list of script handles that require the dm_config global object.
	 *
	 * @since 0.2.0
	 * @var array
	 */
	private $config_scripts = [];

	/**
	 * Remembers, if we already called the `divimode_register_components` action.
	 *
	 * @since 0.2.0
	 * @var bool
	 */
	private $components_done = false;

	/**
	 * List dependencies (JS assets) that need to be loaded when starting the JS app.
	 *
	 * @since 0.2.0
	 * @var array
	 */
	private $dependencies = [];

	/**
	 * The Helpscout beacon ID to load on the current admin page. This property
	 * is set and used by the method `$this->load_helpscout()`.
	 *
	 * @var array
	 */
	private $hs_beacon = [
		'id'      => '',
		'config'  => [
			'mode'    => 'neutral',
			'display' => [
				'style'     => 'icon',
				'iconImage' => 'buoy',
				'zIndex'    => '160001',
			],
		],
		'form'    => [],
		'session' => [],
	];

	/**
	 * Singleton accessor.
	 *
	 * @since 0.1.0
	 * @return DM_Library
	 */
	public static function inst() {
		static $_inst = null;

		if ( null === $_inst ) {
			$_inst = new DM_Library();
			$_inst->setup();
		}

		// Load the ajax handlers _after_ the DM_Library instance was created, i.e.,
		// not in the constructor.
		require_once __DIR__ . '/ajax-handler.php';

		return $_inst;
	}

	/**
	 * Initializes the settings page API.
	 *
	 * @since 0.1.0
	 */
	private function __construct() {
	}

	/**
	 * Set up hooks.
	 *
	 * @since 0.2.0
	 */
	public function setup() {
		add_action( 'init', [ $this, 'process_save_form' ], 20 );
	}

	/**
	 * Creates and returns the settings instance.
	 *
	 * @since 0.2.0
	 * @return DM_Settings
	 */
	public function get_settings() {
		if ( null === $this->settings ) {
			$this->settings = new DM_Settings();
		}

		return $this->settings;
	}

	/**
	 * Creates and returns the logger instance.
	 *
	 * @since 2.0.0
	 * @return DM_Logger
	 */
	public function get_logger() {
		if ( null === $this->logger ) {
			$this->logger = new DM_Logger();
		}

		return $this->logger;
	}

	/**
	 * Creates a new metabox component.
	 *
	 * @since 0.2.0
	 * @return DM_UI_Component_Metabox
	 */
	public function new_metabox() {
		$this->register_custom_components();

		return new DM_UI_Component_Metabox();
	}

	/**
	 * Indicates, that the plugin was activated and needs to be installed.
	 * We prepare the website to flush the permalink cache and redirect the user
	 * to the license page, when no license is present.
	 * The options are parsed by the DM_Settings module.
	 *
	 * @since 0.2.0
	 *
	 * @param string $option_name The plugin options-key. Same value as passed to the
	 *                            load_options() function.
	 */
	public function install_plugin( $option_name ) {
		$option_name = sanitize_key( strtolower( trim( $option_name ) ) );

		add_option( "{$option_name}_flush_permalinks", true );
		add_option( "{$option_name}_license_redirect", true );
	}

	/**
	 * Configures the Divimode UI library.
	 * When the $value parameter is omitted, the function returns the current config
	 * value. When even the $key parameter is omitted, the entire config list is
	 * returned.
	 *
	 * @since 0.1.0
	 *
	 * @param string $key   The config key to change.
	 * @param mixed  $value The config value to set.
	 *
	 * @return mixed Returns the original value.
	 */
	public function config( $key = null, $value = null ) {
		if ( null === $key ) {
			return $this->config;
		}

		if ( is_string( $key ) && isset( $this->config[ $key ] ) ) {
			$orig_value = $this->config[ $key ];
		} else {
			$orig_value = null;
		}

		if ( is_object( $key ) ) {
			$key = (array) $key;
		}
		if ( is_array( $key ) ) {
			foreach ( $key as $conf => $val ) {
				$this->config[ $conf ] = $val;
			}
		} elseif ( is_string( $key ) && ! is_null( $value ) ) {
			$this->config[ $key ] = $value;
		}

		return $orig_value;
	}

	/**
	 * Registers a custom JS component that can be used by the JS app later.
	 * Custom components always consist of the following files:
	 *   source_dir/component.php
	 *   source_dir/component.js
	 *   source_dir/component.css
	 * Alternative file names:
	 *    source_dir/class-dm-ui-component-my-element.php
	 *    source_dir/class-dm-ui-component-my_element.php
	 *    source_dir/class-dm-ui-component-myelement.php
	 *    source_dir/my-element.php
	 *    source_dir/my_element.php
	 *    source_dir/myelement.php
	 *    source_dir/component.php
	 *    source_dir/my-element.js
	 *    source_dir/my_element.js
	 *    source_dir/myelement.js
	 *    source_dir/component.js
	 *    source_dir/script.js
	 *    source_dir/my-element.css
	 *    source_dir/my_element.css
	 *    source_dir/myelement.css
	 *    source_dir/component.css
	 *    source_dir/style.css
	 * The PHP file must meet the following criteria:
	 *   1. Use the same namespace as the shared library
	 *   2. Declare the class class `DM_UI_Component_<name>`
	 *   3. The class must extend `DM_UI_Component`
	 *   4. The class defines `protected $component_type = '<name>'`
	 *
	 * @throws DM_Exception_Component When a component file is missing.
	 * @since 0.2.0
	 *
	 * @param string $slug        Slug of the component.
	 * @param string $source_path Path to the folder with the php component.
	 * @param string $source_url  URL to the folder with the component sources.
	 *
	 * @return void
	 */
	public function register_component( $slug, $source_path, $source_url ) {
		$source_path = trailingslashit( $source_path );
		$source_url  = trailingslashit( $source_url );

		if ( ! is_dir( $source_path ) ) {
			throw new DM_Exception_Component( "Component folder does not exist: {$source_path}" );
		}

		// Step 1: Detect the component class and asset files.
		$class_paths = [
			'class-dm-ui-component-' . str_replace( [ '_', '-' ], '-', $slug ) . '.php',
			'class-dm-ui-component-' . str_replace( [ '_', '-' ], '_', $slug ) . '.php',
			'class-dm-ui-component-' . str_replace( [ '_', '-' ], '', $slug ) . '.php',
			str_replace( [ '_', '-' ], '-', $slug ) . '.php',
			str_replace( [ '_', '-' ], '_', $slug ) . '.php',
			str_replace( [ '_', '-' ], '', $slug ) . '.php',
			'component.php',
		];
		$asset_paths = [
			str_replace( [ '_', '-' ], '-', $slug ),
			str_replace( [ '_', '-' ], '_', $slug ),
			str_replace( [ '_', '-' ], '', $slug ),
			'component',
			'script',
			'style',
		];

		$class_found  = '';
		$style_found  = '';
		$script_found = '';

		foreach ( $class_paths as $path ) {
			if ( file_exists( $source_path . $path ) ) {
				$class_found = $path;
				break;
			}
		}

		foreach ( $asset_paths as $path ) {
			if ( ! $script_found && file_exists( $source_path . $path . '.js' ) ) {
				$script_found = $path . '.js';
			}
			if ( ! $style_found && file_exists( $source_path . $path . '.css' ) ) {
				$style_found = $path . '.css';
			}
			if ( $script_found && $style_found ) {
				break;
			}
		}

		// Step 2: Load the detected component class.
		if ( $class_found ) {
			require_once $source_path . $class_found;
		} else {
			throw new DM_Exception_Component( "Component class does not exist: {$source_path}component.php" );
		}

		// Step 3: Enqueue the detected component assets.
		if ( $script_found ) {
			$this->add_dependency( $source_url . $script_found, 'script' );
		} else {
			throw new DM_Exception_Component( "Component script does not exist: {$source_path}component.js" );
		}

		if ( $style_found ) {
			$this->add_dependency( $source_url . $style_found, 'style' );
		} else {
			throw new DM_Exception_Component( "Component style does not exist: {$source_path}component.css" );
		}
	}

	/**
	 * Adds the given URL as dependency for the JS app and outputs a link-tag to
	 * preload a script or style asset without injecting it into the DOM or executing
	 * it.
	 * The preloader tag is output as soon as possible, but not before the
	 * "admin_enqueue_scripts" action is fired, because that's the first action that
	 * happens in the admin head-tag - we cannot output HTML code earlier.
	 *
	 * @since 0.2.0
	 *
	 * @param string $url  URL of the resource to preload.
	 * @param string $type Either "script" or "style".
	 *
	 * @return void
	 */
	public function add_dependency( $url, $type = 'script' ) {
		// Mark the asset as dependency for our JS app.
		if ( ! isset( $this->dependencies[ $type ] ) ) {
			$this->dependencies[ $type ] = [];
		}
		$this->dependencies[ $type ][] = $url;

		// Preload it as soon as possible.
		$output_asset = function () use ( $url, $type ) {
			printf(
				'<link rel="preload" href="%1$s" as="%2$s">',
				esc_url( $url ),
				esc_attr( $type )
			);
		};

		if ( did_action( 'admin_enqueue_scripts' ) ) {
			$output_asset();
		} else {
			add_action( 'admin_enqueue_scripts', $output_asset, 1 );
		}
	}

	/**
	 * Instantly loads the wp-hooks library, which is a core dependency of our
	 * library.
	 * The wp-hooks library is a small js file + we are on wp-admin, which is why we
	 * load it as soon as possible instead of delaying library initialization.
	 *
	 * @global WP_Scripts $wp_scripts
	 * @since 0.2.0
	 * @return void
	 */
	public function instantly_load_hooks() {
		global $wp_scripts;

		if ( ! did_action( 'admin_enqueue_scripts' ) ) {
			add_action( 'admin_enqueue_scripts', [ $this, 'instantly_load_hooks' ], 1 );

			return;
		}

		/**
		 * Logic taken from class.wp-dependencies.php to output a single script and
		 * mark it as done. This avoids outputting duplicate scripts on the same page.
		 *
		 * @see WP_Dependencies::do_items()
		 */
		if (
			! in_array( 'wp-hooks', $wp_scripts->done, true )
			&& $wp_scripts->do_item( 'wp-hooks' )
		) {
			$wp_scripts->done[] = 'wp-hooks';
		}
	}

	/**
	 * Enqueues the divimode shared library assets.
	 *
	 * @since 0.1.0
	 * @return void
	 */
	public function enqueue_lib_assets() {
		$this->instantly_load_hooks();

		$asset_root    = rtrim( $this->config( 'lib_url' ), ' /' );
		$cache_version = $this->config( 'version' );

		wp_enqueue_script(
			'divimode-lib',
			"{$asset_root}/js/lib.js",
			[
				'jquery',
				'wp-hooks',
				'wp-util',
			],
			$cache_version,
			true
		);

		wp_enqueue_style(
			'divimode-lib',
			"{$asset_root}/css/lib.css",
			[],
			$cache_version
		);

		$this->enqueue_js_data( 'divimode-lib' );
	}

	/**
	 * Instructs the plugin to generate and output the JS data object with the
	 * specified script handle.
	 *
	 * @since 0.2.0
	 *
	 * @param string $for_handle The enqueued script handle that requires this data.
	 *
	 * @return void
	 */
	private function enqueue_js_data( $for_handle ) {
		if ( ! count( $this->config_scripts ) ) {
			add_action( 'admin_footer', [ $this, 'inject_js_data' ], 10 );
		}

		if ( ! in_array( $for_handle, $this->config_scripts, true ) ) {
			$this->config_scripts[] = $for_handle;
		}
	}

	/**
	 * Fires the action `divimode_register_components` once. On second or later call,
	 * this method will not do anything.
	 *
	 * @since 0.2.0
	 * @return void
	 */
	private function register_custom_components() {
		if ( $this->components_done ) {
			return;
		}

		$this->components_done = true;

		/**
		 * Notifies other modules that the current request needs to generate the JS
		 * App with a metabox. At this point, custom components must be registered!
		 *
		 * @since 0.2.0
		 *
		 * @param DM_Library $library A reference to the current instance.
		 */
		do_action( 'divimode_register_components', $this );
	}

	/**
	 * Localizes specific scripts that require the global JS data object dm_config.
	 *
	 * @since 0.2.0
	 * @return void
	 */
	public function inject_js_data() {
		$js_data = $this->get_settings()->get_script_js_config();

		/**
		 * Add details about preloaded dependencies JS to the configuration.
		 *
		 * @since 0.2.0
		 * @var array
		 */
		$js_data['dependencies'] = $this->dependencies;

		foreach ( $this->config_scripts as $handle ) {
			wp_localize_script( $handle, 'dm_config', $js_data );
		}
	}

	/**
	 * Registers an ajax handler with the given name.
	 * The nonce checks are intentionally added as anonymous functions, so they
	 * cannot be removed without also removing the callback handler.
	 *
	 * @since 0.2.0
	 *
	 * @param string   $action The ajax action string.
	 * @param callable $method A callback that handles the action.
	 * @param bool     $public Optional. Set to true, to enable the endpoint for
	 *                         guests. Default value: false.
	 *
	 * @return void
	 */
	public function ajax_add_handler( $action, $method, $public = false ) {
		// Register the nonce and action in dm_config.
		self::inst()->get_settings()->add_nonce( $action );

		// Register an endpoint for logged-in users.
		add_action(
			"wp_ajax_{$action}",
			function () use ( $action ) {
				$this->ajax_check_nonce( $action, false );
			},
			1
		);
		add_action( "wp_ajax_{$action}", $method, 10 );

		// Optionally, register an endpoint for guest users.
		if ( $public ) {
			add_action(
				"wp_ajax_{$action}",
				function () use ( $action ) {
					$this->ajax_check_nonce( $action, true );
				},
				1
			);
			add_action( "wp_ajax_nopriv_{$action}", $method, 10 );
		}
	}

	/**
	 * Validates the ajax nonce, and ends the request when an invalid token is found.
	 * Expected the nonce to come from an authenticated user.
	 *
	 * @since 0.2.0
	 *
	 * @param string $action   The action.
	 * @param bool   $as_guest Whether to expect a guest or authenticated request.
	 *
	 * @return void
	 */
	public function ajax_check_nonce( $action, $as_guest ) {
		if ( is_user_logged_in() === $as_guest ) {
			wp_send_json_error( [ 'msg' => 'Invalid user session' ] );
		}

		// phpcs:ignore WordPress.Security.NonceVerification.Missing
		if ( ! isset( $_POST['_wpnonce'] ) ) {
			wp_send_json_error( [ 'msg' => 'Missing security token' ] );
		}

		// phpcs:ignore WordPress.Security.NonceVerification.Missing
		if ( ! self::inst()->get_settings()->verify_nonce( sanitize_key( $_POST['_wpnonce'] ), $action ) ) {
			wp_send_json_error( [ 'msg' => 'Invalid security token' ] );
		}
	}

	/**
	 * Checks the POST data for valid metabox details and saves the form data to the
	 * defined table.
	 *
	 * @since 0.2.0
	 * @return void
	 */
	public function process_save_form() {
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return;
		}

		if ( empty( $_POST ) || empty( $_POST['dm_boxes'] ) ) {
			return;
		}

		$post_id = 0;
		$options = false;

		// Determine the save location (post, option, etc).
		if ( isset( $_POST['post_ID'] ) ) {
			$post_id = (int) $_POST['post_ID'];
		} elseif ( isset( $_POST['dm_options'] ) && 'yes' === $_POST['dm_options'] ) {
			$options = true;
		}

		if ( ! $post_id && ! $options ) {
			wp_die( 'Cannot save the metabox, unknown page.' );
		}

		// Check permissions.
		if ( $post_id ) {
			$post_type_name = get_post_type( $post_id );
			$post_type_obj  = get_post_type_object( $post_type_name );

			if ( ! current_user_can( $post_type_obj->cap->edit_post, $post_id ) ) {
				return;
			}
		}

		/**
		 * List of all divimode metabox names in the submitted form.
		 *
		 * @since 0.2.0
		 * @var array
		 */
		$boxes = array_map( 'sanitize_key', (array) wp_unslash( $_POST['dm_boxes'] ) );

		foreach ( $boxes as $box ) {
			$nonce_field = "dm_{$box}_nonce";
			$data_field  = "dm_{$box}_data";

			if (
				empty( $_POST[ $nonce_field ] )
				|| ! wp_verify_nonce( sanitize_key( $_POST[ $nonce_field ] ), "nonce:metabox-{$box}" )
				|| empty( $_POST[ $data_field ] )
			) {
				continue;
			}

			if ( $post_id ) {
				$type = 'post';
			} elseif ( $options ) {
				$type = 'option';
			}

			// phpcs:disable WordPress.Security.ValidatedSanitizedInput.InputNotSanitized
			// phpcs:disable ET.Sniffs.ValidatedSanitizedInput.InputNotSanitized
			// phpcs:disable WordPress.Security.ValidatedSanitizedInput.MissingUnslash

			/*
			 * The post-data is not sanitized, because the save_metabox_data method
			 * treats it as a JSON string that is parsed with json_decode. Sanitation
			 * is not possible at this point.
			 */
			$this->save_metabox_data(
				$box,
				$_POST[ $data_field ],
				$type,
				$post_id
			);
			// phpcs:enable
		}
	}

	/**
	 * Saves the specified metabox details to the database without any nonce or
	 * security checks. Always check nonces and do other relevant security checks
	 * before calling this function.
	 * The $raw_post_data string must not be `wp_unslash`ed or `stripslashes`ed! This
	 * method handles the unslashing, and by passing in an already unslashed
	 * $raw_post_data string, some necessary slashes can be removed, which can create
	 * an invalid and un-decodeable JSON string.
	 *
	 * @since 2.0.0
	 *
	 * @param string $box           The metabox name.
	 * @param string $raw_post_data Original POST data (string; not unslashed).
	 * @param string $target        The target DB table: post, option.
	 * @param string $the_id        Post-ID; not used for "option" target.
	 *
	 * @return void
	 */
	public function save_metabox_data( $box, $raw_post_data, $target, $the_id ) {
		$plugin_slug = $this->get_settings()->get_slug();

		if ( ! $raw_post_data || ! is_string( $raw_post_data ) ) {
			return;
		}

		$json = wp_unslash( $raw_post_data );
		try {
			$data = json_decode( $json, true );
		} catch ( \Exception $exception ) {
			$data = [];
		}

		if ( $data && is_array( $data ) ) {
			$data_key = DM_Library::inst()->get_settings()->get_db_name( $box );

			// Save the data to the determined DB table.
			if ( 'post' === $target && $the_id ) {
				// Fetch the old value, for usage in the WP filter later.
				$old_data = get_post_meta( $the_id, $data_key, true );

				/**
				 * Filter the post data before it's saved to the post meta table.
				 *
				 * @since 0.2.0
				 *
				 * @param array $data    The form data that will be saved to the DB.
				 * @param int   $post_id The target post.
				 */
				$data = apply_filters(
					"divimode_save_post_data_{$plugin_slug}_{$box}",
					$data,
					$the_id
				);

				update_post_meta( $the_id, $data_key, $data, $old_data );

				/**
				 * After the data was saved to the DB.
				 * The dynamic portion of the hook combines the plugin slug and the
				 * metabox-slug, e.g. "divi-areas_config".
				 *
				 * @since 2.0.0
				 *
				 * @param mixed $data     The saved data.
				 * @param mixed $old_data The meta data entry before the update.
				 * @param int   $post_id  The post ID.
				 */
				do_action( 'divimode_saved_post_data', $plugin_slug, $box, $data, $old_data, $the_id );
				do_action( "divimode_saved_post_data_{$plugin_slug}_{$box}", $data, $old_data, $the_id );
			} elseif ( 'option' === $target ) {
				// Fetch the old value, for usage in the WP filter later.
				$old_data = get_option( $data_key );

				/**
				 * Filter the post data before it's saved to the options table.
				 *
				 * @since 0.2.0
				 *
				 * @param array $data The form data that will be saved to the DB.
				 */
				$data = apply_filters(
					"divimode_save_options_{$plugin_slug}_{$box}",
					$data
				);

				if ( get_option( $data_key ) === $old_data ) {
					update_option( $data_key, $data, false );
				}

				/**
				 * After the data was saved to the DB.
				 * The dynamic portion of the hook combines the plugin slug and the
				 * metabox-slug, e.g. "divi-areas_settings".
				 *
				 * @since 2.0.0
				 *
				 * @param mixed $data     The saved data.
				 * @param mixed $old_data The option value entry before the update.
				 */
				do_action( 'divimode_saved_options', $plugin_slug, $box, $data, $old_data );
				do_action( "divimode_saved_options_{$plugin_slug}_{$box}", $data, $old_data );
			}
		}
	}

	/**
	 * Initializes the helpscout beacon on the current page.
	 *
	 * @param string $beacon_id        The helpscout beacon ID.
	 * @param array  $identity         An array containing customer identity details.
	 * @param array  $session_data     Optional details about the current session.
	 *                                 Those infos are transmitted when a new chat
	 *                                 is started but are not stored in the
	 *                                 Helpscout customer profile.
	 */
	public function load_helpscout( $beacon_id, $identity, $session_data = [] ) {
		if ( ! $beacon_id || $this->hs_beacon['id'] ) {
			return;
		}

		$user = wp_get_current_user();

		$this->hs_beacon['id']                         = $beacon_id;
		$this->hs_beacon['session']                    = $session_data;
		$this->hs_beacon['form']['name']               = $user->first_name;
		$this->hs_beacon['form']['email']              = $user->user_email;
		$this->hs_beacon['form']['subject']            = 'In-App Support Request';
		$this->hs_beacon['identity']                   = [
			'email'     => isset( $identity['email'] ) ? $identity['email'] : '',
			'signature' => isset( $identity['signature'] ) ? $identity['signature'] : '',
		];
		$this->hs_beacon['config']['messagingEnabled'] = ! empty( $identity['license'] );

		add_action( 'admin_footer', [ $this, 'priv__load_helpscout' ] );
	}

	/**
	 * Action handler for the `admin_footer` hook. Outputs the helpscout JS code
	 * in the page footer, when a beacon-ID is present.
	 *
	 * @private
	 */
	public function priv__load_helpscout() {
		global $wp_version;

		if ( ! $this->hs_beacon['id'] ) {
			return;
		}

		$curr_theme = wp_get_theme();

		$this->hs_beacon['session']['WordPress'] = $wp_version;
		if ( $curr_theme->stylesheet !== $curr_theme->template ) {
			$curr_theme = wp_get_theme( $curr_theme->template );

			$this->hs_beacon['session']['Child Theme'] = 'Yes';
		} else {
			$this->hs_beacon['session']['Child Theme'] = 'No';
		}
		$this->hs_beacon['session']['Theme']         = $curr_theme->stylesheet;
		$this->hs_beacon['session']['Theme Version'] = $curr_theme->version;

		if ( false === strpos( DIVI_AREAS_STORE_URL, 'elegantthemes' ) ) {
			$this->hs_beacon['session']['Store'] = 'divimode.com';
		} else {
			$this->hs_beacon['session']['Store'] = 'Divi Marketplace';
		}

		printf(
			'<script type="text/javascript">!function(e,t,n){function a(){var e=t.getElementsByTagName("script")[0],n=t.createElement("script");n.type="text/javascript",n.async=!0,n.src="https://beacon-v2.helpscout.net",e.parentNode.insertBefore(n,e)}if(e.Beacon=n=function(t,n,a){e.Beacon.readyQueue.push({method:t,options:n,data:a})},n.readyQueue=[],"complete"===t.readyState)return a();e.attachEvent?e.attachEvent("onload",a):e.addEventListener("load",a,!1)}(window,document,window.Beacon||function(){});</script>
<script type="text/javascript">Beacon("init","%s");Beacon("identify",%s);Beacon("config",%s);Beacon("session-data",%s);Beacon("prefill",%s)</script>',
			esc_attr( $this->hs_beacon['id'] ),
			wp_json_encode( $this->hs_beacon['identity'] ),
			wp_json_encode( $this->hs_beacon['config'] ),
			wp_json_encode( $this->hs_beacon['session'] ),
			wp_json_encode( $this->hs_beacon['form'] )
		);
	}
}
