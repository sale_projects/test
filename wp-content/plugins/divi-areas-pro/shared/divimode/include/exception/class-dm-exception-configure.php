<?php
/**
 * Custom exceptions.
 *
 * @package Divimode_UI
 */

namespace DiviAreasPro;

/**
 * Custom exception that is thrown by a DM_UI_Component objects that
 * receives invalid configuration details.
 *
 * @since 0.2.0
 */
class DM_Exception_Configure extends \Exception {}
