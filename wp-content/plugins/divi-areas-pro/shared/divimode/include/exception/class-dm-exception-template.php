<?php
/**
 * Custom exceptions.
 *
 * @package Divimode_UI
 */

namespace DiviAreasPro;

/**
 * Custom exception that is thrown by the template module of DM_UI_Component
 * objects.
 *
 * @since 0.2.0
 */
class DM_Exception_Template extends \Exception {}
