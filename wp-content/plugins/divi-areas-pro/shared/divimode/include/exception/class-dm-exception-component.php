<?php
/**
 * Custom exceptions.
 *
 * @package Divimode_UI
 */

namespace DiviAreasPro;

/**
 * Custom exception that is thrown when trying to create an invalid component type.
 *
 * @since 0.2.0
 */
class DM_Exception_Component extends \Exception {}
