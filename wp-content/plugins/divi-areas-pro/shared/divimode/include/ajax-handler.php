<?php
/**
 * Registers ajax handlers for divimode-library components.
 *
 * @since 0.2.0
 * @package Divimode_UI
 */

namespace DiviAreasPro;

DM_Library::inst()->ajax_add_handler(
	'divimode_component_post_list_get_items',
	[ __NAMESPACE__ . '\DM_UI_Component_PostList', 'ajax_get_items' ]
);
