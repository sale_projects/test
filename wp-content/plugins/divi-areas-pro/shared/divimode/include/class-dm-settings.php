<?php
/**
 * Admin settings page API
 *
 * @package Divimode_UI
 */

namespace DiviAreasPro;

/**
 * Settings page API.
 */
class DM_Settings {

	/**
	 * Label of the menu item. Usually "Settings".
	 *
	 * @since 0.1.0
	 * @var string
	 */
	private $menu_label = '';

	/**
	 * The prefix for all keys that are saved/read from the DB (post-meta, options,
	 * etc).
	 *
	 * @since 2.0.0
	 * @var string
	 */
	private $db_prefix = '_dm_';

	/**
	 * Menu parent element. Usually the CPT URL, like "edit.php?post_type=cpt-slug".
	 *
	 * @since 0.1.0
	 * @var string
	 */
	private $menu_parent = '';

	/**
	 * The parent plugin file name. Used to hook into the plugin page to display
	 * additional details, such as the license state and a Settings Link.
	 *
	 * @since 0.1.0
	 * @var string
	 */
	private $plugin_file = '';

	/**
	 * Menu slug, like "cpt-settings".
	 *
	 * @since 0.1.0
	 * @var string
	 */
	private $slug = '';

	/**
	 * Name of the plugin.
	 *
	 * @since 0.1.0
	 * @var string
	 */
	private $plugin_name = '';

	/**
	 * Stores the current plugin version.
	 *
	 * @since 2.0.0
	 * @var string
	 */
	private $plugin_version = '';

	/**
	 * Name of the options entry in wp_options, like "cpt_name".
	 *
	 * @since 0.1.0
	 * @var string
	 */
	private $options_field = '';

	/**
	 * Name of the license entry in wp_options, like "cpt_name_license".
	 *
	 * @since 0.1.0
	 * @var string
	 */
	private $license_field = '';

	/**
	 * Required capabilities to access the menu item. Default "manage_options".
	 *
	 * @since 0.1.0
	 * @var string
	 */
	private $caps = 'manage_options';

	/**
	 * URL to the EDD license server.
	 *
	 * @since 0.1.0
	 * @var string
	 */
	private $license_store_url = '';

	/**
	 * Product ID of this plugin in the EDD license server.
	 *
	 * @since 0.1.0
	 * @var string
	 */
	private $license_store_id = '';

	/**
	 * Full path to the plugin file, for the automatic updater.
	 *
	 * @since 0.1.0
	 * @var string
	 */
	private $license_plugin_file = '';

	/**
	 * Current plugin version, for the automatic updater.
	 *
	 * @since 0.1.0
	 * @var string
	 */
	private $license_plugin_ver = '';

	/**
	 * Plugin author name, used by the automatic updater.
	 *
	 * @since 0.1.0
	 * @var string
	 */
	private $license_plugin_author = '';

	/**
	 * The absolute URL to the settings page.
	 *
	 * @since 0.1.0
	 * @var string
	 */
	private $settings_url = '';

	/**
	 * URL to the plugins sales page with information, samples and the download link.
	 *
	 * @since 2.0.0
	 * @var string
	 */
	private $sales_page_url = '';

	/**
	 * The actual settings menu hook.
	 *
	 * @since 0.1.0
	 * @var string
	 */
	private $hook = '';

	/**
	 * Internal cache of plugin options.
	 *
	 * @since 0.1.0
	 * @var array
	 */
	private $options = [];

	/**
	 * List of translations used by this module.
	 *
	 * @since 0.1.0
	 * @var array
	 */
	private $lang = [];

	/**
	 * List of nonces that are handled by our plugin. The array key is the action
	 * name, and the value the nonce.
	 *
	 * Nonces can be registered via $this->add_nonce().
	 *
	 * @since 2.0.0
	 * @var array
	 */
	private $nonces = [];

	/**
	 * License key that was originally stored in the DB, before it was overwritten
	 * with a wp-config constant.
	 *
	 * @since 0.1.3
	 * @var string
	 */
	private $orig_license_key = null;

	/**
	 * Initializes the settings page API.
	 *
	 * @since 0.1.0
	 */
	public function __construct() {
		add_action(
			'init',
			[ $this, 'priv__flush_permalinks' ],
			9999
		);

		// Automatic updates and License handling.
		add_action(
			'admin_init',
			[ $this, 'priv__init_updater' ],
			1
		);

		/**
		 * Check if the plugin version changed (display a welcome screen, etc).
		 *
		 * @since 2.0.0
		 */
		add_action(
			'admin_init',
			[ $this, 'priv__was_plugin_updated' ],
			9
		);

		// Settings page integration.
		add_action(
			'admin_menu',
			[ $this, 'priv__admin_menu' ],
			100 // Ensure that settings is the last entry.
		);

		add_action(
			'admin_enqueue_scripts',
			[ $this, 'priv__admin_enqueue_scripts' ]
		);

		add_action(
			'admin_head',
			[ $this, 'priv__show_license_info' ]
		);
	}

	/**
	 * Defines the admin menu details.
	 *
	 * @since 0.1.0
	 * @param string $db_prefix      Prefix for all postmeta/options keys.
	 * @param string $slug           The plugin slug, like "divi-areas".
	 * @param string $plugin_name    Name of the current plugin.
	 * @param string $menu_label     Label of the menu item. Usually "Settings".
	 * @param string $menu_parent    Menu parent-URL or the parent-CPT name.
	 *                               Either "edit.php?post_type=cpt-slug" or "cpt-slug".
	 * @param string $plugin_file    The main plugin file, used to hook into the
	 *                               "Installed Plugins" page and display additional
	 *                               details there for the plugin.
	 * @param string $plugin_version Name of the current plugin.
	 * @param string $sales_page_url URL to the sales page of the plugin.
	 * @param string $caps           Optional. Minimum capabilities to access the
	 *                               menu item. Default "manage_options".
	 * @return DM_Settings Reference to $this for chaining.
	 */
	public function configure(
		$db_prefix,
		$slug,
		$plugin_name,
		$menu_label,
		$menu_parent,
		$plugin_file,
		$plugin_version,
		$sales_page_url,
		$caps = 'manage_options'
	) {
		$db_prefix = sanitize_key( strtolower( trim( $db_prefix ) ) );
		$slug      = sanitize_key( strtolower( trim( $slug ) ) );

		$this->db_prefix      = $db_prefix;
		$this->slug           = $slug;
		$this->plugin_name    = $plugin_name;
		$this->plugin_version = $plugin_version;
		$this->menu_label     = $menu_label;
		$this->menu_parent    = $menu_parent;
		$this->plugin_file    = $plugin_file;
		$this->caps           = $caps;
		$this->sales_page_url = $sales_page_url;

		// Load the plugin settings.
		$this->load_options( $this->db_prefix . 'setting' );

		// Display additional infos on the plugins page.
		if ( $this->plugin_file ) {
			add_filter(
				"plugin_action_links_{$this->plugin_file}",
				[ $this, 'priv__plugin_action_links' ]
			);

			add_filter(
				'plugin_row_meta',
				[ $this, 'priv__plugin_row_meta' ],
				10,
				4
			);
		}

		DM_Library::inst()->ajax_add_handler(
			"divimode_set_license_{$this->slug}",
			[ $this, 'ajax_set_license' ]
		);

		return $this;
	}

	/**
	 * Provide license and update information to enable automatic plugin updates via
	 * Easy Digital Downloads (Software Licensing).
	 *
	 * @since 0.1.0
	 * @param string $store_url      The EDD store URL.
	 * @param int    $product_id     EDD product ID of this plugin.
	 * @param string $plugin_file    Absolute path to the main plugin file.
	 * @param string $plugin_version Current plugin version.
	 * @param string $plugin_author  The plugin author.
	 * @return DM_Settings Reference to $this for chaining.
	 */
	public function license(
		$store_url,
		$product_id,
		$plugin_file,
		$plugin_version,
		$plugin_author
	) {
		$this->license_store_url     = $store_url;
		$this->license_store_id      = $product_id;
		$this->license_plugin_file   = $plugin_file;
		$this->license_plugin_ver    = $plugin_version;
		$this->license_plugin_author = $plugin_author;

		return $this;
	}

	/**
	 * Provide or update a translation for this module.
	 *
	 * @since 0.1.0
	 * @param string|array $key  Either the language ID to translate, or a key/value pair
	 *                     collection of multiple translations.
	 * @param string       $text The translation. Only used when $key is a string.
	 * @return DM_Settings Reference to $this for chaining.
	 */
	public function localize( $key, $text = '' ) {
		if ( is_string( $key ) && is_string( $text ) ) {
			$key = strtolower( trim( $key ) );

			$this->lang[ $key ] = trim( $text );
		} else {
			if ( is_object( $key ) ) {
				$key = (array) $key;
			}

			if ( is_array( $key ) ) {
				foreach ( $key as $k => $v ) {
					$this->localize( $k, $v );
				}
			}
		}

		return $this;
	}

	/**
	 * Getter for the private $slug member. Other objects in the same namespace can
	 * identify each other using that slug:
	 *
	 * DM_Library::inst()->get_settings()->get_slug()
	 *
	 * @since 0.2.0
	 * @return string
	 */
	public function get_slug() {
		return $this->slug;
	}

	/**
	 * Returns the specified field name with the DB-prefix.
	 *
	 * @since 0.2.0
	 * @param string $name The field name/options-key.
	 * @return string The prefixed field name.
	 */
	public function get_db_name( $name ) {
		return $this->db_prefix . strtolower( trim( $name ) );
	}

	/**
	 * Returns the absolute URL to the settings page.
	 *
	 * @since 0.1.0
	 * @return string
	 */
	public function get_url() {
		return $this->settings_url;
	}

	/**
	 * Tests, if the current user is logged in and has specified permissions. When no
	 * parameters are passed to this function, it tests against the capability defined
	 * in `$this->caps` (which defaults to `manage_options`).
	 *
	 * Samples:
	 *
	 * >     $this->has_permission()
	 * >     $this->has_permission('update_plugins')
	 * >     $this->has_permission('edit_post', 100)
	 *
	 * @since 2.0.0
	 * @param string $caps    Optional. An arbitrary capability to check. Default is
	 *                        the capability defined in `$this->caps`.
	 * @param mixed  ...$args Optional. Additional parameters, like a post ID.
	 * @return bool
	 */
	public function has_permission( $caps = false, ...$args ) {
		$result = false;
		if ( ! $caps ) {
			$caps = $this->caps;
		}

		if ( is_user_logged_in() ) {
			$result = current_user_can( $caps, ...$args );
		}

		return $result;
	}

	/**
	 * Loads the plugin options from the DB into the plugin cache.
	 *
	 * @since 0.1.0
	 * @param string $options_name Optional. Name of the wp_options entry, like
	 *                             "cpt_name". Only used when the function is called
	 *                             for the first time in a request and defaults to
	 *                             the slug, when omitted.
	 * @return object Reference to $this.
	 */
	public function load_options( $options_name = '' ) {
		// Only set options name once!
		if ( ! $this->options_field ) {
			if ( empty( $options_name ) || ! is_string( $options_name ) ) {
				$options_name = $this->slug;
			}

			$this->options_field = sanitize_key( strtolower( trim( $options_name ) ) );
			$this->license_field = $this->options_field . '_license';
		}

		$this->options = [];

		if ( $this->options_field ) {
			$this->options = get_option( $this->options_field, [] );
			$license       = get_option( $this->license_field, [] );

			if ( ! is_array( $this->options ) ) {
				$this->options = [];
			}

			if ( is_array( $license ) ) {
				$this->options = array_merge( $this->options, $license );
			}
		}

		/**
		 * Filter the options that were loaded from the DB.
		 * Allows other modules to initialize missing options with default values.
		 *
		 * @since 0.1.1
		 * @param array $options The plugin options loaded from DB.
		 */
		$this->options = apply_filters(
			"divimode_load_options_{$this->options_field}",
			$this->options
		);

		/**
		 * Optionally read the license key from a wp-config constant.
		 *
		 * @since 0.1.3
		 */
		$license_const = strtoupper( "DM_{$this->options_field}_LICENSE" );
		if ( defined( $license_const ) ) {
			$this->orig_license_key       = (string) $this->get_option( 'license_key' );
			$this->options['license_key'] = constant( $license_const );
		}

		return $this;
	}

	/**
	 * Returns the specific plugin configuration.
	 *
	 * @since  0.1.0
	 * @param  string $key The configuration key.
	 * @return mixed The plugin configuration value.
	 */
	public function get_option( $key ) {
		$value = false;

		// The final value of those settings is sanitized/dynamic.
		$need_sanitization = [
			'has_license_key',
			'license_status',
			'license_validated',
		];

		if ( isset( $this->options[ $key ] ) ) {
			$value = $this->options[ $key ];
		}

		if ( in_array( $key, $need_sanitization, true ) ) {
			if ( false === strpos( $this->sales_page_url, 'elegantthemes' ) ) {
				switch ( $key ) {
					case 'has_license_key':
						if ( isset( $this->options['license_key'] ) ) {
							$value = (bool) $this->options['license_key'];
						} else {
							$value = false;
						}
						break;

					case 'license_status':
						$value = (array) $value;
						break;

					case 'license_validated':
						/**
						 * Subtract 24 hours from the return value, to avoid some
						 * strange issues with "invalid_time" errors due to timezone
						 * settings.
						 */
						$value = (int) $value - DAY_IN_SECONDS;
						break;
				}
			} else {
				switch ( $key ) {
					case 'has_license_key':
						$value = true;
						break;

					case 'license_status':
						$value = [ 'license' => 'valid' ];
						break;

					case 'license_validated':
						$value = time() - DAY_IN_SECONDS;
						break;
				}
			}
		}

		return $value;
	}

	/**
	 * Saves the specified plugin configuration.
	 *
	 * @since  0.1.0
	 * @param  string $key   The configuration key.
	 * @param  mixed  $value The plugin configuration value.
	 * @return void
	 */
	public function set_option( $key, $value ) {
		if ( 'license_key' === $key ) {
			if ( is_null( $this->orig_license_key ) ) {
				$this->options[ $key ] = trim( $value );
			} else {
				$this->orig_license_key = $value;
			}
		} else {
			if ( 'license_status' === $key && is_object( $value ) ) {
				$value = (array) $value;
			}

			$this->options[ $key ] = $value;
		}

		/**
		 * Defines the fields that are saved in the license field.
		 *
		 * @since 0.2.0
		 */
		$license_fields = [
			'license_validated' => true,
			'license_status'    => true,
			'license_key'       => true,
		];

		$metabox = array_diff_key( $this->options, $license_fields );

		update_option( $this->options_field, $metabox );

		if ( false === strpos( $this->sales_page_url, 'elegantthemes' ) ) {
			$license = array_intersect_key( $this->options, $license_fields );

			if ( ! is_null( $this->orig_license_key ) ) {
				$license['license_key'] = $this->orig_license_key;
			}

			update_option( $this->license_field, $license );
		}
	}


	/*
	 * *****************************************************************************
	 * *****************************************************************************
	 *
	 *  Internal functions
	 *
	 * *****************************************************************************
	 * *****************************************************************************
	 */


	/**
	 * Get the translation of the specified language ID.
	 *
	 * @since 0.1.0
	 * @param string $id The language ID.
	 * @return string
	 */
	private function lang( $id ) {
		$text = '';
		$vars = func_get_args();
		$id   = array_shift( $vars );

		if ( isset( $this->lang[ $id ] ) ) {
			$text = $this->lang[ $id ];
		}

		if ( $vars ) {
			if ( is_array( $vars[0] ) ) {
				$vars = $vars[0];
			}

			$text = vsprintf( $text, $vars );
		}

		return $text;
	}

	/**
	 * Output the translated string.
	 *
	 * @since 0.1.0
	 * @param string $id The language ID.
	 * @return void
	 */
	private function show( $id ) {
		$vars = func_get_args();
		$id   = array_shift( $vars );
		$text = $this->lang( $id, $vars );

		// phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
		echo $text;
	}

	/**
	 * Registers a new nonce that will be output in the dm_config object.
	 *
	 * @since 0.2.0
	 * @param string $action The ajax action name.
	 * @return DM_Settings Reference to $this for chaining.
	 */
	public function add_nonce( $action ) {
		$action = sanitize_key( $action );

		$this->nonces[ $action ] = wp_create_nonce( "nonce:$action" );
		return $this;
	}

	/**
	 * Returns the nonce for the given action. The nonce must be first registered via $this->add_nonce().
	 *
	 * @since 0.2.0
	 * @param string $action The nonce action name.
	 * @return string The nonce (or an empty string).
	 */
	public function get_nonce( $action ) {
		$action = sanitize_key( $action );

		if ( ! isset( $this->nonces[ $action ] ) ) {
			return '';
		}

		return $this->nonces[ $action ];
	}

	/**
	 * Returns HTML code of the two hidden input fields that are required to process
	 * the given dm_action: "dm_action" and "_wpnonce".
	 *
	 * @since 0.2.0
	 * @param string $action The nonce action. Must be first registered with
	 *                       $this->add_nonce().
	 * @return string
	 */
	public function nonce_fields( $action ) {
		$action = sanitize_key( $action );
		$nonce  = $this->get_nonce( $action );

		if ( ! $nonce ) {
			return '';
		}

		return sprintf(
			'<input type="hidden" name="dm_action" value="%s" /><input type="hidden" name="_wpnonce" value="%s" />',
			$action,
			$nonce
		);
	}

	/**
	 * Verifies the given nonce based on previously registered nonces.
	 *
	 * @since 0.2.0
	 * @param string $nonce  The nonce.
	 * @param string $action  The nonce action (unencoded).
	 * @return bool True, when the nonce action matches the nonce.
	 */
	public function verify_nonce( $nonce, $action ) {
		// Nonces should be sanitized already, but it cannot hurt to double-check.
		$nonce  = sanitize_key( $nonce );
		$action = sanitize_key( $action );

		if ( ! $this->get_nonce( $action ) ) {
			return false;
		}

		return wp_verify_nonce( $nonce, "nonce:$action" );
	}

	/**
	 * Generates a nonce URL with the specified action. The nonce must be registered
	 * first using $this->add_nonce().
	 *
	 * @since 0.2.0
	 * @param string $url    Optional. The URL (without a nonce). When no URL is
	 *                       specified, the current URL is used.
	 * @param string $action The action to perform. This determines the nonce.
	 * @return string The URL with action and nonce appended.
	 */
	public function nonce_url( $url, $action = false ) {
		if ( false === $action ) {
			$action = $url;
			$url    = add_query_arg( 'dm_action', $action );
		} else {
			$url = add_query_arg( 'dm_action', $action, $url );
		}

		if ( ! $this->get_nonce( $action ) ) {
			return false;
		}

		return wp_nonce_url( $url, "nonce:$action" );
	}

	/**
	 * Generates an array with details for the shared JS library and returns it.
	 *
	 * This function is called by the DM_Library. Any module or component that needs
	 * to filter the result to insert custom data must do so, before the
	 * `admin_footer` hook (priority 10) is called.
	 *
	 * @internal
	 *
	 * @since 0.1.0
	 * @return array The JS data.
	 */
	public function get_script_js_config() {
		$all_details = DM_Library::inst()->config();
		unset( $all_details['lib_url'] );
		unset( $all_details['version'] );

		/**
		 * The JS data object with empty structure elements that can be filtered.
		 *
		 * @type string pluginName    The name of the current plugin.
		 * @type string pluginVersion The current plugin version.
		 * @type string api           URL to the ajax endpoint.
		 *                            We use WP ajax, no REST yet.
		 * @type array post           Details about the currently edited post.
		 * @type array components     Custom component config should go here.
		 * @type array i18n           All translations go here.
		 * @type array nonces         Holds nonces for ajax requests.
		 * @type array extra          Additional config options that were set via
		 *                            DM_Library::inst()->config().
		 * @type bool debug           Enable debug-mode in the admin editor.
		 */
		$js_data = [
			'pluginName'    => $this->plugin_name,
			'pluginVersion' => DM_Library::inst()->config( 'version' ),
			'post'          => $this->get_script_js_postdata(),
			'components'    => [],
			'i18n'          => [],
			'nonces'        => $this->nonces,
			'debug'         => defined( 'WP_DEBUG' ) ? WP_DEBUG : false,
			'extra'         => $all_details,
		];

		/**
		 * Filter the js data object to allow individual components or modules to add
		 * required details.
		 *
		 * @since 0.2.0
		 * @param array  $js_data   Data that is available in JS as `window.dm_config`.
		 * @param string $db_prefix Prefix for postmeta/option keys.
		 * @param string $slug      The current plugin slug.
		 */
		$js_data = apply_filters(
			'divimode_js_config',
			$js_data,
			$this->db_prefix,
			$this->slug
		);

		$js_data = apply_filters(
			"divimode_js_config_{$this->slug}",
			$js_data,
			$this->db_prefix
		);

		// Core data is intentionally inserted after the filters.
		$js_data['i18n'] = array_merge( $js_data['i18n'], $this->lang );

		$js_data['licenseHasKey']    = $this->get_option( 'has_license_key' );
		$js_data['licenseStatus']    = $this->get_option( 'license_status' );
		$js_data['licenseLastCheck'] = time() - $this->get_option( 'license_validated' );

		return $js_data;
	}

	/**
	 * Returns an array with details about the current post. Those details are added
	 * to the global dm_config javascript variable for usage by the JS app.
	 *
	 * @since 0.2.0
	 * @return array Post details.
	 */
	public function get_script_js_postdata() {
		global $pagenow;

		if ( 'post.php' === $pagenow || 'post-new.php' === $pagenow ) {
			$post = get_post();
			$data = [
				'ID'            => $post->ID,
				'post_name'     => $post->post_name,
				'post_title'    => $post->post_title,
				'post_status'   => $post->post_status,
				'post_type'     => $post->post_type,
				'post_date'     => $post->post_date,
				'post_modified' => $post->post_modified,
				'post_parent'   => $post->post_parent,
			];

			/**
			 * Filter the post-data, so plugins can add custom post-details as needed.
			 *
			 * @since 0.2.0
			 * @param array $data Post details to include in `window.dm_config`.
			 * @param string $slug The current plugin slug.
			 */
			$data = apply_filters( 'divimode_js_postdata', $data, $this->slug );

			$data = apply_filters( "divimode_js_postdata_{$this->slug}", $data );
		} else {
			$data = [];
		}

		return $data;
	}

	/**
	 * Adds the settings menu to the defined main menu.
	 *
	 * @internal
	 *
	 * @since 0.1.0
	 * @return void
	 */
	public function priv__admin_menu() {
		if ( ! $this->menu_parent ) {
			return;
		}
		if ( post_type_exists( $this->menu_parent ) ) {
			$parent_url = "edit.php?post_type={$this->menu_parent}";
		} else {
			$parent_url = $this->menu_parent;
		}

		$this->settings_url = add_query_arg(
			'page',
			"{$this->slug}-settings",
			admin_url( $parent_url )
		);

		// Redirect to license page after plugin activation.
		$this->priv__maybe_show_license_page();

		// Add the Settings menu.
		$this->hook = add_submenu_page(
			$parent_url,
			$this->menu_label,
			$this->menu_label,
			$this->caps,
			"{$this->slug}-settings",
			[ $this, 'priv__render_settings' ]
		);

		// Integrate custom hooks for our settings page.
		add_action(
			"load-{$this->hook}",
			[ $this, 'priv__prepare_settings_page' ]
		);

		add_action(
			"admin_footer-{$this->hook}",
			[ $this, 'priv__hook_admin_footer' ]
		);
	}

	/**
	 * Enqueue our custom plugin JS and CSS assets for the admin editor.
	 *
	 * @internal
	 *
	 * @since  0.1.0
	 * @param  string $hook The current admin page hook name.
	 * @return void
	 */
	public function priv__admin_enqueue_scripts( $hook ) {
		if ( $this->hook !== $hook ) {
			return;
		}

		DM_Library::inst()->enqueue_lib_assets();

		do_action( 'divimode_settings_enqueue_scripts', $this->slug, $hook );
		do_action( "divimode_settings_enqueue_scripts_{$this->slug}", $hook );
	}

	/**
	 * After plugin activation flush all rewrite rules to avoid errors in admin
	 * post editor.
	 *
	 * @since 0.2.0
	 */
	public function priv__flush_permalinks() {
		$option_key = "{$this->options_field}_flush_permalinks";

		if ( get_option( $option_key, false ) ) {
			flush_rewrite_rules( true );
			delete_option( $option_key );
		}
	}

	/**
	 * Force a upgrade-check and license validation.
	 *
	 * @since 0.1.2
	 * @return void
	 */
	public function process_version_check() {
		if ( false === strpos( $this->sales_page_url, 'elegantthemes' ) ) {
			// Set "last validated" to 0, which means "never".
			$this->set_option( 'license_validated', 0 );

			// Re-check for updates.
			DM_Updater::inst()->clear_cache();
			delete_site_transient( 'update_plugins' );
			wp_update_plugins();
		}
	}


	/**
	 * Save plugin settings.
	 *
	 * @since  0.1.0
	 * @return void
	 */
	private function process_set_license() {
		/*
		 * This is a private method that's called after a nonce check.
		 * No need to check again here.
		 */

		if ( false === strpos( $this->sales_page_url, 'elegantthemes' ) ) {

			// phpcs:disable WordPress.Security.NonceVerification.Missing
			if ( empty( $_POST['license_key'] ) ) {
				return;
			}

			$license_key = sanitize_text_field( wp_unslash( $_POST['license_key'] ) );
			$license_key = trim( $license_key, '*' );
			// phpcs:enable

			if ( $license_key ) {
				$this->set_option( 'license_key', $license_key );
			}

			// Activate the license, when the user entered one.
			$license = $this->get_option( 'license_key' );
			$this->license_status_activate( $license );
		}
	}

	/**
	 * Remove the license key.
	 *
	 * @since  0.1.0
	 * @return void
	 */
	private function process_remove_license() {
		$license = $this->get_option( 'license_key' );
		$this->license_status_deactivate( $license );
	}

	/**
	 * Runs during admin_init and saves the plugin configuration, when a valid
	 * nonce is present.
	 *
	 * @internal
	 *
	 * @since  0.1.0
	 * @return void
	 */
	private function process_settings_request() {
		// We do several other checks before validating the nonce.
		// phpcs:ignore WordPress.Security.NonceVerification.Recommended
		$data = wp_unslash( $_REQUEST );

		if (
			empty( $data['_wpnonce'] ) // No nonce? Stop.
			|| empty( $data['dm_action'] ) // No action? Stop.
			|| ! $this->verify_nonce( // Invalid nonce? Stop.
				sanitize_key( $data['_wpnonce'] ),
				sanitize_key( $data['dm_action'] )
			)
			|| ! $this->has_permission() // Missing capabilities? Stop.
		) {
			return;
		}

		$action = sanitize_key( $data['dm_action'] );

		if ( 'remove_license' === $action ) {
			$this->process_remove_license();
		} elseif ( 'set_license' === $action ) {
			$this->process_set_license();
		} elseif ( 'version_check' === $action ) {
			$this->process_version_check();
		}

		wp_safe_redirect( $this->get_url() );
		exit;
	}

	/**
	 * Prepares the settings page: Process the submitted form (if any), register
	 * nonces, etc.
	 *
	 * @since 0.2.0
	 * @return void
	 */
	public function priv__prepare_settings_page() {
		$this->add_nonce( 'set_license' );
		$this->add_nonce( 'remove_license' );
		$this->add_nonce( 'version_check' );

		/**
		 * Fires during the load-{hook} action when the plugins settings page is
		 * loaded, but before any settings are saved.
		 *
		 * The dynamic portion of the hook is the plugin slug.
		 *
		 * @since 0.2.2
		 */
		do_action( 'divimode_load_settings', $this->slug );
		do_action( "divimode_load_settings_{$this->slug}" );

		$this->process_settings_request( false );
	}

	/**
	 * Processes an Ajax request to modify the license key (set or remove).
	 *
	 * This handler is called by `DM_Library::ajax_add_handler()` which already does a
	 * nonce-check and confirmed that the user is logged-in before calling this
	 * function. We still want to check the users permissions before processing the
	 * request.
	 *
	 * @since 2.0.0
	 * @return void
	 */
	public function ajax_set_license() {
		if ( $this->has_permission() ) {
			$this->process_set_license();

			/*
			 * The license status does not contain the license key, but only details
			 * about the actual status (valid/invalid) and some messages for display
			 * (plugin name, status-reason, etc.).
			 */
			wp_send_json_success( $this->get_option( 'license_status' ) );
		}

		wp_send_json_error();
	}

	/**
	 * Renders the admin settings page.
	 *
	 * @internal
	 *
	 * @since 0.1.0
	 * @return void
	 */
	public function priv__render_settings() {
		if ( ! $this->has_permission() ) {
			//phpcs:ignore
			wp_die( $this->lang( 'no_permission' ) );
		}

		/**
		 * Fires right before the settings page metaboxes are rendered.
		 *
		 * The dynamic portion of the hook is the plugin slug.
		 *
		 * @since 0.2.0
		 */
		do_action( 'divimode_add_settings_meta_boxes', $this->slug );
		do_action( "divimode_add_settings_meta_boxes_{$this->slug}" );

		?>
		<div class="wrap divimode-settings-page">
			<form id="setting" method="post" enctype="multipart/form-data">
				<div id="poststuff">
					<input type="hidden" name="dm_options" value="yes" />

					<?php
					// Render our custom metabox.
					do_meta_boxes( 'divimode_settings', 'normal', 'options' );
					?>

				</div>

				<div id="settings-bottom">
					<button type="submit" class="dm-save-button" name="save">
						<?php echo esc_html( $this->lang( 'save_changes' ) ); ?>
					</button>
				</div>
			</form>
		</div>
		<?php
	}


	/**
	 * Display a custom link in the left column of the "Plugins" list.
	 *
	 * @internal
	 *
	 * @since  0.1.0
	 * @param  array $links List of plugin links.
	 * @return array New list of plugin links.
	 */
	public function priv__plugin_action_links( $links ) {
		$links[] = sprintf(
			'<a href="%s">%s</a>',
			$this->get_url(),
			$this->lang( 'settings_link' )
		);

		return array_reverse( $links );
	}

	/**
	 * Display additional details in the right column of the "Plugins" page.
	 *
	 * @internal
	 *
	 * @since 0.1.0
	 * @param string[] $plugin_meta An array of the plugin's metadata,
	 *                              including the version, author,
	 *                              author URI, and plugin URI.
	 * @param string   $plugin_file Path to the plugin file relative to the plugins directory.
	 * @param array    $plugin_data An array of plugin data.
	 * @param string   $status      Status of the plugin. Defaults are 'All', 'Active',
	 *                              'Inactive', 'Recently Activated', 'Upgrade', 'Must-Use',
	 *                              'Drop-ins', 'Search'.
	 */
	public function priv__plugin_row_meta( $plugin_meta, $plugin_file, $plugin_data, $status ) {
		if ( $this->plugin_file !== $plugin_file ) {
			return $plugin_meta;
		}

		$data = $this->get_option( 'license_status' );
		$data = array_filter( $data );

		if ( $data && 'valid' === $data['license'] ) {
			$plugin_meta[] = sprintf(
				'<span style="color:#080">%s</span>',
				$this->lang( 'license_active' )
			);
		} elseif ( ! $data ) {
			$plugin_meta[] = sprintf(
				'<span style="color:#800">%s</span>',
				$this->lang( 'no_license' )
			);
		} else {
			$plugin_meta[] = sprintf(
				'<span style="color:#800">%s</span>',
				$this->lang( 'license_inactive' )
			);
		}

		return $plugin_meta;
	}

	/**
	 * Trigger a custom action hook.
	 *
	 * @since 0.1.0
	 * @return void
	 */
	public function priv__hook_admin_footer() {
		do_action( 'divimode_settings_admin_footer', $this->slug );
		do_action( "divimode_settings_admin_footer_{$this->slug}" );
	}

	/**
	 * When a plugin update is available, this function returns an array with plugin
	 * update information.
	 *
	 * @return array Update information.
	 */
	public function get_available_update_infos() {
		$infos       = [
			'available'    => false,
			'last_checked' => 0,
			'changelog'    => '',
			'new_version'  => '',
			'upgrade_link' => '',
			'recheck_link' => '',
		];
		$plugin_data = get_site_transient( 'update_plugins' );

		if (
			$plugin_data
			&& is_object( $plugin_data )
			&& ! empty( $plugin_data->response )
			&& ! empty( $plugin_data->response[ $this->plugin_file ] )
		) {
			$item = $plugin_data->response[ $this->plugin_file ];

			$log_parts = array_filter(
				explode(
					'<p><strong>',
					implode(
						'',
						$item->changelog
					)
				)
			);

			$changelog = [];
			foreach ( $log_parts as $part ) {
				list( $item_version, $item_text ) = explode( '</strong>', $part, 2 );

				if ( empty( $item_version ) ) {
					continue;
				}

				if ( version_compare( $item_version, $this->license_plugin_ver, '<=' ) ) {
					break;
				}

				$full_entry  = "<p><strong>{$item_version}</strong>{$item_text}";
				$changelog[] = [
					'version' => $item_version,
					'text'    => $full_entry,
				];
			}

			$infos['new_version'] = $item->new_version;
			$infos['available']   = true;
			$infos['changelog']   = $changelog;
		}

		// Generate URLs.
		$infos['upgrade_link'] = wp_nonce_url(
			self_admin_url( 'update.php?action=upgrade-plugin&plugin=' ) . $this->plugin_file,
			'upgrade-plugin_' . $this->plugin_file
		);
		$infos['recheck_link'] = $this->nonce_url(
			$this->get_url(),
			'version_check'
		);

		$infos['last_checked'] = $plugin_data->last_checked + get_option( 'gmt_offset' ) * HOUR_IN_SECONDS;

		return $infos;
	}


	/*
	 * *****************************************************************************
	 * *****************************************************************************
	 *
	 *  License handling
	 *
	 * *****************************************************************************
	 * *****************************************************************************
	 */


	/**
	 * Redirect to the license page after plugin activation.
	 *
	 * @since  0.2.0
	 * @return void
	 */
	public function priv__maybe_show_license_page() {
		$option_key = "{$this->options_field}_license_redirect";

		if ( get_option( $option_key, false ) ) {
			delete_option( $option_key );

			$license_key_present = $this->get_option( 'has_license_key' );
			$license_status      = $this->get_option( 'license_status' );

			/**
			 * Action fires after the one-time-redirect flag was deleted, but before
			 * the license status was validated.
			 *
			 * This action can take over and redirect to a welcome page that should be
			 * displayed regardless of the license status.
			 *
			 * The dynamic portion of the hook is the plugin slug.
			 *
			 * @since 2.0.0
			 */
			do_action( "divimode_activation_redirect_{$this->slug}" );

			if (
				$license_key_present &&
				! empty( $license_status['license'] ) &&
				in_array( $license_status['license'], [ 'valid', 'expired' ], true )
			) {
				// License is valid.
				return;
			}

			/**
			 * Filters the plugin-activation license URL which is displayed once,
			 * directly after plugin activation when no valid license key exists.
			 *
			 * Return false or an empty string to disable the redirect.
			 *
			 * The dynamic portion of the hook is the plugin slug.
			 *
			 * @since 2.0.0
			 * @param string $license_url The URL that displays the license form.
			 */
			$redirect_to = apply_filters(
				"divimode_license_redirect_{$this->slug}",
				$this->settings_url
			);

			if ( $redirect_to ) {
				wp_safe_redirect( $redirect_to );
				exit;
			}
		}
	}

	/**
	 * Initialize the Updater module to check for updates, provide changelog links,
	 * and validate the license status.
	 *
	 * @action admin_init [1]
	 *
	 * @since  0.1.0
	 * @return void
	 */
	public function priv__init_updater() {
		$license = $this->get_option( 'license_key' );

		if ( false === strpos( $this->sales_page_url, 'elegantthemes' ) ) {
			DM_Updater::inst()->init(
				$this->license_store_url,
				$this->license_plugin_file,
				[
					'version' => $this->license_plugin_ver,
					'license' => $license,
					'item_id' => $this->license_store_id,
					'author'  => $this->license_plugin_author,
					'beta'    => false,
				]
			);

			$this->license_status_check( $license );
		}
	}

	/**
	 * Checks, if the plugin version changed. When a new version is detected, a action
	 * is broadcasted and a temporary option is added to the DB.
	 *
	 * @action admin_init [9]
	 *
	 * @since 2.0.0
	 * @return void
	 */
	public function priv__was_plugin_updated() {
		if ( ! $this->plugin_version ) {
			return;
		}

		$db_version = $this->get_db_version();

		if (
			$db_version !== $this->plugin_version
			&& ! $this->has_plugin_version_changed()
		) {
			/**
			 * Broadcast the plugin version change.
			 *
			 * The dynamic portion of the hook is the plugin slug.
			 *
			 * @since 2.0.0
			 * @param string $old_version The previous plugin version.
			 * @param string $new_version The new plugin version.
			 */
			do_action(
				"divimode_version_changed_{$this->slug}",
				$db_version,
				$this->plugin_version
			);

			/**
			 * Add a new option to the DB that will be deleted, once the plugin
			 * confirmed that the version change was processed (DB updates completed,
			 * welcome page displayed, and so on).
			 *
			 * We use an option instead of a transient here, because we want to make
			 * 100% certain, that the flag is manually deleted and not removed due to
			 * expiration.
			 *
			 * @since 2.0.0
			 */
			update_option( "{$this->db_prefix}_version_changed", $this->plugin_version );
		}
	}

	/**
	 * Returns the plugin version stored in the DB.
	 *
	 * @since 2.0.1
	 * @param string $default_version A default return value, when no version is
	 *                                stored in the DB.
	 * @return string
	 */
	public function get_db_version( $default_version = '0.0.1' ) {
		$result = (string) get_option( "{$this->db_prefix}_version" );

		if ( ! $result ) {
			$result = $default_version;
		}

		return $result;
	}

	/**
	 * Changes the plugin version which is stored in the DB.
	 *
	 * @since 2.0.1
	 * @param string $new_version The new version to store in the DB.
	 * @return void
	 */
	public function set_db_version( $new_version ) {
		update_option( "{$this->db_prefix}_version", $new_version );
	}

	/**
	 * Checks, if the last plugin update is still "unprocessed" or already fully
	 * completed by the host plugin.
	 *
	 * This function will return `true`, once the `priv__was_plugin_updated()` method
	 * detects a new plugin version (during admin_init priority 9). After manually
	 * calling `plugin_update_done()`, this method will return `false` again.
	 *
	 * @since 2.0.0
	 * @return bool True, when the last plugin update is not marked as completed yet.
	 */
	public function has_plugin_version_changed() {
		$change_flag = get_option( "{$this->db_prefix}_version_changed", '' );
		return $this->plugin_version === $change_flag;
	}

	/**
	 * Deletes the temporary version-changed flag again from the DB and sets the DB
	 * plugin version to the latest version number.
	 *
	 * @since 2.0.0
	 * @return void
	 */
	public function plugin_update_done() {
		delete_option( "{$this->db_prefix}_version_changed" );

		$this->set_db_version( $this->plugin_version );
	}

	/**
	 * Display a notice when the license is missing or expired.
	 *
	 * @since  0.1.0
	 * @return void
	 */
	public function priv__show_license_info() {
		/**
		 * Since we cannot pass `$this` into a callback function, we need to create a
		 * reference to the current DM_Settings object first.
		 *
		 * @var DM_Settings
		 */
		$conf = $this;

		/**
		 * The hook name of the settings page.
		 *
		 * @var string
		 */
		$settings_hook = $this->hook;

		add_action(
			'admin_notices',
			function() use ( $conf, $settings_hook ) {
				global $hook_suffix;

				$license_key_present = $conf->get_option( 'has_license_key' );
				$license_status      = $conf->get_option( 'license_status' );
				$last_validation     = $conf->get_option( 'license_validated' );

				/*
				When a license key exists that is either valid or expired AND the
				last-check timestamp confirms, that the license-status is up-to-date
				then do not show a nag.
				*/
				$valid_until = $last_validation + DAY_IN_SECONDS + WEEK_IN_SECONDS;
				$valid_time  = $last_validation <= time() && $valid_until >= time();

				if (
					$license_key_present
					&& ! empty( $license_status['license'] )
					&& in_array( $license_status['license'], [ 'valid', 'expired' ], true )
					&& $valid_time
				) {
					return;
				}

				/**
				 * Generate an error-code that explains, why the message is displayed
				 * to the current user.
				 *
				 * @since 0.2.2
				 * @var string
				 */
				$error_infos = '';

				if ( ! $license_key_present ) {
					$error_infos = 'no_license';
				} else {
					if ( empty( $license_status['license'] ) ) {
						$error_infos = 'no_license_state';
					} elseif ( ! in_array( $license_status['license'], [ 'valid', 'expired' ], true ) ) {
						$error_infos = 'status_' . $license_status['license'];
					}
				}
				if ( ! $error_infos ) {
					if ( $last_validation <= time() ) {
						$error_infos = sprintf(
							'invalid_time (%d)',
							time() - $last_validation
						);
					} elseif ( $valid_until >= time() ) {
						$error_infos = sprintf(
							'old_data (%d)',
							$valid_until - time()
						);
					}
				}
				if ( ! $error_infos ) {
					$error_infos = 'unknown';
				}

				?>
				<div class="notice error is-dismissible">
					<p><strong>
						<?php $conf->show( 'unlicensed_title', $conf->plugin_name ); ?>
					</strong><br />
					<?php $conf->show( 'unlicensed_text' ); ?>
					<?php if ( $settings_hook !== $hook_suffix ) : ?>
					<a href="<?php echo esc_url( $conf->settings_url ); ?>">
						<?php $conf->show( 'goto_settings' ); ?>
					</a>
					<?php else : ?>
					<code class="dm-license-error-code">
						<i class="dashicons dashicons-warning"></i>
						<?php echo esc_html( trim( $error_infos ) ); ?>
					</code>
					<?php endif; ?>
					</p>
				</div>
				<?php
			}
		);
	}

	/**
	 * Activate a new license.
	 *
	 * @since  0.1.0
	 * @param  string $license The license key.
	 * @return string A human readable status message.
	 */
	private function license_status_activate( $license ) {
		$message = false;

		// API call.
		$api_params = [
			'edd_action' => 'activate_license',
			'license'    => $license,
			'item_id'    => rawurlencode( $this->license_store_id ),
			'url'        => home_url(),
		];

		// Call the custom API.
		$response = wp_remote_post(
			$this->license_store_url,
			[
				'timeout'   => 15,
				'sslverify' => false,
				'body'      => $api_params,
			]
		);

		// Validate the response.
		$license_data = false;

		if (
			is_wp_error( $response ) ||
			200 !== wp_remote_retrieve_response_code( $response )
		) {
			if ( is_wp_error( $response ) ) {
				$message = $response->get_error_message();
			} else {
				$message = $this->lang( 'unknown_error' );
			}
		} else {
			$license_data = json_decode( wp_remote_retrieve_body( $response ) );

			/**
			 * Fired, after the license was activated.
			 *
			 * The dynamic portion of the hook is the plugin slug.
			 *
			 * @since 0.2.0
			 * @param array $license_data The license details array.
			 */
			do_action( 'divimode_license_activated', $this->slug, $license_data );
			do_action( "divimode_license_activated_{$this->slug}", $license_data );
		}

		$this->license_parse( $license_data );

		return $message;
	}

	/**
	 * Deactivate the current license.
	 *
	 * @since  0.1.0
	 * @param  string $license The license key.
	 * @return string A human readable status message.
	 */
	private function license_status_deactivate( $license ) {
		$message = false;

		if ( false === strpos( $this->sales_page_url, 'elegantthemes' ) ) {
			// data to send in our API request.
			$api_params = [
				'edd_action' => 'deactivate_license',
				'license'    => $license,
				'item_id'    => rawurlencode( $this->license_store_id ),
				'url'        => home_url(),
			];

			// Call the custom API.
			$response = wp_remote_post(
				$this->license_store_url,
				[
					'timeout'   => 15,
					'sslverify' => false,
					'body'      => $api_params,
				]
			);

			if (
				is_wp_error( $response ) ||
				200 !== wp_remote_retrieve_response_code( $response )
			) {
				if ( is_wp_error( $response ) ) {
					$message = $response->get_error_message();
				} else {
					$message = $this->lang( 'unknown_error' );
				}
			} else {
				$this->set_option( 'license_key', false );
				$this->set_option( 'license_status', false );

				/**
				 * Fired, after the license was deactivated.
				 *
				 * The dynamic portion of the hook is the plugin slug.
				 *
				 * @since 0.2.0
				 */
				do_action( 'divimode_license_deactivated', $this->slug );
				do_action( "divimode_license_deactivated_{$this->slug}" );
			}
		}

		return $message;
	}

	/**
	 * Check the details of the current license.
	 *
	 * @since 0.1.0
	 * @param string $license The license key.
	 */
	private function license_status_check( $license ) {
		if ( ! $license ) {
			return;
		}

		$last_check = $this->get_option( 'license_validated' );
		$next_check = $last_check + 36 * HOUR_IN_SECONDS;

		if ( time() >= $last_check && time() <= $next_check ) {
			return '';
		}

		$api_params = [
			'edd_action' => 'check_license',
			'license'    => $license,
			'item_id'    => rawurlencode( $this->license_store_id ),
			'url'        => home_url(),
		];

		// Call the custom API.
		$response = wp_remote_post(
			$this->license_store_url,
			[
				'timeout'   => 15,
				'sslverify' => false,
				'body'      => $api_params,
			]
		);

		if ( is_wp_error( $response ) ) {
			return false;
		}

		$this->set_option( 'license_validated', time() );

		$license_data = json_decode( wp_remote_retrieve_body( $response ) );
		$this->license_parse( $license_data );
	}

	/**
	 * Parse the response from the license server and store it in the options table.
	 *
	 * @since 0.1.0
	 * @param object $license_data Data returned from license server.
	 */
	private function license_parse( $license_data ) {
		$message = false;

		if ( ! $license_data ) {
			$this->set_option( 'license_status', false );
			return;
		}

		if ( empty( $license_data->success ) ) {
			if ( ! isset( $license_data->error ) ) {
				$license_data->error = 'missing';
			}

			$gmt_offset = get_option( 'gmt_offset' ) * HOUR_IN_SECONDS;

			switch ( $license_data->error ) {
				case 'expired':
					$message = sprintf(
						$this->lang( 'status_expired' ),
						date_i18n(
							get_site_option( 'date_format' ),
							strtotime( $license_data->expires ) + $gmt_offset
						)
					);
					break;

				case 'disabled':
				case 'revoked':
					$message = $this->lang( 'status_disabled' );
					break;

				case 'missing':
					$message = $this->lang( 'status_missing' );
					break;

				case 'invalid':
				case 'site_inactive':
					$message = $this->lang( 'status_invalid' );
					break;

				case 'key_mismatch':
				case 'invalid_item_id': // thrown when item_id is used in API params.
				case 'item_name_mismatch': // thrown when item_name is used in API params.
					$message = $this->lang( 'status_mismatch', $this->plugin_name );
					break;

				case 'no_activations_left':
					$message = $this->lang( 'status_limit' );
					break;

				default:
					$message = $this->lang( 'unknown_error' );
					break;
			}
		}

		if ( is_object( $license_data ) ) {
			if ( $message ) {
				$license_data->msg = $message;
			} elseif ( 'valid' === $license_data->license ) {
				$license_data->msg = $this->lang( 'status_okay', $this->plugin_name );
			}
		}

		// The `license` attribute will be either "valid" or "invalid".
		$this->set_option( 'license_status', $license_data );
	}
}
