<?php
/**
 * Plugin updater class.
 *
 * This class was provided by EDD and minor changes were made for divimode.com.
 *
 * @package Divimode_UI
 */

namespace DiviAreasPro;

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

/**
 * Allows plugins to use their own update API.
 *
 * This class is a modified version of the EDD updater class.
 *
 * @author Easy Digital Downloads, divimode.com
 * @version 1.6.18
 */
class DM_Updater {

	/**
	 * URL to our license server.
	 *
	 * @since 0.1.0
	 * @var string
	 */
	private $api_url = '';

	/**
	 * API configuration (beta-access, license code, etc.)
	 *
	 * @since 0.1.0
	 * @var array
	 */
	private $api_data = [];

	/**
	 * Name of the plugin, e.g. "my-plugin-dir/my-plugin-file.php"
	 *
	 * @since 0.1.0
	 * @var string
	 */
	private $name = '';

	/**
	 * The filename of the plugin without the extension, e.g. "my-plugin-file"
	 *
	 * @since 0.1.2
	 * @var string
	 */
	private $basename = '';

	/**
	 * The dirname of the plugin, e.g. "my-plugin-dir"
	 *
	 * @since 0.1.2
	 * @var string
	 */
	private $dirname = '';

	/**
	 * Current plugin version, should be a string like "1.2.3"
	 *
	 * @since 0.1.0
	 * @var string
	 */
	private $version = '';

	/**
	 * Override wordpress.org repository data with our self hosted plugin data.
	 * This is only used when we have a free wordpress.org plugin with the exact
	 * same name as our self hosted plugin.
	 *
	 * @since 0.1.0
	 * @var bool
	 */
	private $wp_override = false;

	/**
	 * Identifier that contains a unique cache-key to store plugin details in the
	 * options table. This field includes details like the latest version, the
	 * changelog, etc.
	 *
	 * The cache key is generated from
	 *  1. The plugin dirname and filename
	 *  2. The users license key
	 *  3. The beta-program opt-in
	 *
	 * @since 0.1.0
	 * @var string
	 */
	private $cache_key = '';

	/**
	 * API Request timeout (in seconds) that is applied to all requests that are made
	 * towards our software server.
	 *
	 * @since 0.1.0
	 * @var int
	 */
	private $health_check_timeout = 5;

	/**
	 * Singleton getter that creates and returns the object instance.
	 *
	 * @since 0.1.2
	 * @return Divimode_DAP_Updater The singleton instance.
	 */
	public static function inst() {
		static $_inst = null;

		if ( null === $_inst ) {
			$_inst = new self();
		}

		return $_inst;
	}

	/**
	 * Private constructor to enforce singleton pattern.
	 *
	 * @since 0.1.2
	 */
	private function __construct() {
		// Empty constructor.
	}

	/**
	 * Set up WordPress filters to hook into WP´s update process.
	 *
	 * @uses add_filter()
	 * @uses plugin_basename()
	 * @uses hook()
	 *
	 * @since 0.1.0
	 * @param string $_api_url     The URL pointing to the custom API endpoint.
	 * @param string $_plugin_file Path to the plugin file.
	 * @param array  $_api_data    Optional data to send with API calls.
	 */
	public function init( $_api_url, $_plugin_file, $_api_data = null ) {
		global $edd_plugin_data;

		$this->api_url  = trailingslashit( $_api_url );
		$this->api_data = $_api_data;
		$this->name     = plugin_basename( $_plugin_file );
		$this->dirname  = dirname( $this->name );
		$this->basename = basename( $this->name, '.php' );
		$this->version  = $_api_data['version'];
		$this->beta     = ! empty( $this->api_data['beta'] ) ? true : false;

		$this->cache_key = $this->get_cache_key( 'edd_sl' );

		$edd_plugin_data[ $this->dirname ] = $this->api_data;

		if ( isset( $_api_data['wp_override'] ) ) {
			$this->wp_override = (bool) $_api_data['wp_override'];
		}

		/**
		 * Fires after the $edd_plugin_data is setup.
		 *
		 * @since 0.1.0
		 *
		 * @param array $edd_plugin_data Array of EDD SL plugin data.
		 */
		do_action( 'post_edd_sl_plugin_updater_setup', $edd_plugin_data );

		/*
		Hook into WordPress core events.
		*/

		add_filter(
			'pre_set_site_transient_update_plugins',
			[ $this, 'check_update' ]
		);

		add_filter(
			'plugins_api',
			[ $this, 'plugins_api_filter' ],
			10,
			3
		);

		remove_action(
			"after_plugin_row_{$this->name}",
			'wp_plugin_update_row',
			10
		);

		add_action(
			"after_plugin_row_{$this->name}",
			[ $this, 'show_update_notification' ],
			10,
			2
		);
	}

	/**
	 * Generates a cache key to access license details in the options table.
	 *
	 * @since 0.1.2
	 * @param string $prefix A string to prepent to the key.
	 * @return string The full cache key.
	 */
	public function get_cache_key( $prefix ) {
		$cache_key_raw = [
			$this->name,
			$this->api_data['license'],
			(int) $this->beta,
		];

		return $prefix . '_' . md5( wp_json_encode( $cache_key_raw ) );
	}

	/**
	 * Check for Updates at the defined API endpoint and modify the update array.
	 *
	 * This function dives into the update API just when WordPress creates its update array,
	 * then adds a custom API call and injects the custom plugin data retrieved from the API.
	 * It is reassembled from parts of the native WordPress plugin update code.
	 * See wp-includes/update.php line 121 for the original wp_update_plugins() function.
	 *
	 * @uses api_request()
	 *
	 * @since 0.1.0
	 * @param array $_transient_data Update array build by WordPress.
	 * @return array Modified update array with custom plugin data.
	 */
	public function check_update( $_transient_data ) {
		global $pagenow;

		if ( ! is_object( $_transient_data ) ) {
			$_transient_data = new \stdClass();
		}

		if ( 'plugins.php' === $pagenow && is_multisite() ) {
			return $_transient_data;
		}

		if (
			! empty( $_transient_data->response )
			&& ! empty( $_transient_data->response[ $this->name ] )
			&& false === $this->wp_override
		) {
			return $_transient_data;
		}

		$version_info = $this->get_cached_version_info();

		if ( false === $version_info ) {
			$version_info = $this->api_request(
				'get_version',
				[
					'slug' => $this->dirname,
					'beta' => $this->beta,
				]
			);

			$this->set_cached_version_info( $version_info );
		}

		if (
			false !== $version_info
			&& is_object( $version_info )
			&& isset( $version_info->new_version )
		) {
			if ( version_compare( $this->version, $version_info->new_version, '<' ) ) {
				$_transient_data->response[ $this->name ] = $version_info;

				// Make sure the plugin property is set to the plugin's name/location.
				// @see issue 1463 on Software Licensing´s GitHub repo.
				$_transient_data->response[ $this->name ]->plugin = $this->name;
			}

			$_transient_data->last_checked           = time();
			$_transient_data->checked[ $this->name ] = $this->version;
		}

		return $_transient_data;
	}

	/**
	 * Show update notification row -- needed for multisite subsites, because WP
	 * won't tell you otherwise!
	 *
	 * This method is only used on multisite installations when logged in as a
	 * network admin user (i.e., a user with "update_plugins" capability).
	 * It's also only used in the /wp-admin area (not /network area).
	 *
	 * @action after_plugin_row_{slug}
	 *
	 * @since 0.1.0
	 * @param string $file   Path to the plugin file relative to the plugins folder.
	 * @param array  $plugin An array of plugin data.
	 */
	public function show_update_notification( $file, $plugin ) {
		if ( ! is_multisite() || is_network_admin() ) {
			return;
		}

		if ( ! current_user_can( 'update_plugins' ) ) {
			return;
		}

		// Remove our filter on the site transient.
		remove_filter(
			'pre_set_site_transient_update_plugins',
			[ $this, 'check_update' ],
			10
		);

		$update_cache = get_site_transient( 'update_plugins' );

		$update_cache = is_object( $update_cache ) ? $update_cache : new \stdClass();

		if (
			empty( $update_cache->response ) ||
			empty( $update_cache->response[ $this->name ] )
		) {
			$version_info = $this->get_cached_version_info();

			if ( false === $version_info ) {
				$version_info = $this->api_request(
					'get_version',
					[
						'slug' => $this->dirname,
						'beta' => $this->beta,
					]
				);

				// Since we disabled our filter for the transient, we aren't running
				// our object conversion on banners, sections, or icons. Do this now.
				if ( isset( $version_info->banners ) && ! is_array( $version_info->banners ) ) {
					$version_info->banners = $this->convert_object_to_array(
						$version_info->banners
					);
				}

				if ( isset( $version_info->sections ) && ! is_array( $version_info->sections ) ) {
					$version_info->sections = $this->convert_object_to_array(
						$version_info->sections
					);
				}

				if ( isset( $version_info->icons ) && ! is_array( $version_info->icons ) ) {
					$version_info->icons = $this->convert_object_to_array(
						$version_info->icons
					);
				}

				$this->set_cached_version_info( $version_info );
			}

			if ( ! is_object( $version_info ) ) {
				return;
			}

			if ( version_compare( $this->version, $version_info->new_version, '<' ) ) {
				$update_cache->response[ $this->name ] = $version_info;
			}

			$update_cache->last_checked           = time();
			$update_cache->checked[ $this->name ] = $this->version;

			set_site_transient( 'update_plugins', $update_cache );
		} else {
			$version_info = $update_cache->response[ $this->name ];
		}

		// Restore our filter.
		add_filter(
			'pre_set_site_transient_update_plugins',
			[ $this, 'check_update' ]
		);

		if (
			! empty( $update_cache->response[ $this->name ] ) &&
			version_compare( $this->version, $version_info->new_version, '<' )
		) {
			_get_list_table( 'WP_Plugins_List_Table' );

			// Build a plugin list row, with update notification.
			printf(
				'<tr class="plugin-update-tr active" id="%1$s-update" data-slug="%1$s" data-plugin="%1$s/%2$s.php">
				<td colspan="3" class="plugin-update colspanchange">
				<div class="update-message notice inline notice-warning notice-alt"><p>',
				esc_attr( $this->dirname ),
				esc_attr( $this->basename )
			);

			$changelog_link = add_query_arg(
				[
					'tab'       => 'plugin-information',
					'plugin'    => $this->dirname,
					'section'   => 'changelog',
					'TB_iframe' => 'true',
					'width'     => '600',
					'height'    => '800',
				],
				network_admin_url( 'plugin-install.php' )
			);

			if ( empty( $version_info->download_link ) ) {
				$upgrade_link = '';
			} else {
				$upgrade_link = wp_nonce_url(
					network_admin_url( 'update.php?action=upgrade-plugin&plugin=' ) . $this->name,
					'upgrade-plugin_' . $this->name
				);
			}

			if ( ! $upgrade_link ) {
				// When the user cannot install updates then only display the changelog.
				// This happens, when the plugin is not activated or the license expired.

				printf(
					/* translators: 1: Plugin name, 2: Details URL, 3: Additional link attributes, 4: Version number. */
					__( // phpcs:ignore
						'There is a new version of %1$s available. <a href="%2$s" %3$s>View version %4$s details</a>.',
						'divi_areas'
					),
					esc_html( $version_info->name ),
					esc_url( $changelog_link ),
					sprintf(
						'class="thickbox open-plugin-details-modal" aria-label="%s"',
						esc_attr(
							sprintf(
								/* translators: 1: Plugin name, 2: Version number. */
								__( 'View %1$s version %2$s details', 'divi_areas' ),
								$version_info->name,
								$version_info->new_version
							)
						)
					),
					esc_attr( $version_info->new_version )
				);
			} else {
				// With a valid license, the admin can also install the update.

				printf(
					/* translators: 1: Plugin name, 2: Details URL, 3: Additional link attributes, 4: Version number, 5: Update URL, 6: Additional link attributes. */
					__( // phpcs:ignore
						'There is a new version of %1$s available. <a href="%2$s" %3$s>View version %4$s details</a> or <a href="%5$s" %6$s>update now</a>.',
						'divi_areas'
					),
					esc_html( $version_info->name ),
					esc_url( $changelog_link ),
					sprintf(
						'class="thickbox open-plugin-details-modal" aria-label="%s"',
						esc_attr(
							sprintf(
								/* translators: 1: Plugin name, 2: Version number. */
								__( 'View %1$s version %2$s details', 'divi_areas' ),
								$version_info->name,
								$version_info->new_version
							)
						)
					),
					esc_attr( $version_info->new_version ),
					esc_url( $upgrade_link ),
					sprintf(
						'class="update-link" aria-label="%s"',
						esc_attr(
							sprintf(
								/* translators: %s: Plugin name. */
								__( 'Update %s now', 'divi_areas' ),
								$version_info->name
							)
						)
					)
				);
			}

			// phpcs:ignore WordPress.NamingConventions.ValidHookName.UseUnderscores
			do_action( "in_plugin_update_message-{$file}", $plugin, $version_info );

			echo '</p></div></td></tr>';
			printf(
				'<script>jQuery(function($){$(\'tr[data-slug="%s"]\').not(".plugin-update-tr").addClass("update")})</script>',
				esc_attr( $this->dirname )
			);
		}
	}

	/**
	 * Updates information on the "View version x.x details" page with custom data.
	 *
	 * @filter plugins_api
	 * @uses api_request()
	 *
	 * @since 0.1.0
	 * @param  mixed  $_data    The result object or array. Default false.
	 * @param  string $_action  The type of information being requested from the
	 *                          Plugin Installation API.
	 * @param  object $_args    Plugin API arguments.
	 * @return mixed Passing a non-false value will effectively short-circuit the
	 *               WordPress.org API request.
	 */
	public function plugins_api_filter( $_data, $_action = '', $_args = null ) {
		if ( 'plugin_information' !== $_action ) {
			return $_data;
		}

		if ( ! isset( $_args->slug ) || ( $_args->slug !== $this->dirname ) ) {
			return $_data;
		}

		$to_send = [
			'slug'   => $this->dirname,
			'is_ssl' => is_ssl(),
			'fields' => [
				'banners' => [],
				'reviews' => false,
				'icons'   => [],
			],
		];

		$api_cache_key = $this->get_cache_key( 'edd_api_request' );

		// Get the transient where we store the api request for this plugin.
		// The version cache has a lifespan of 3 hours.
		$edd_api_request_transient = $this->get_cached_version_info( $api_cache_key );

		// If we have no transient-saved value, run the API, set a fresh transient
		// with the API value, and return that value too right now.
		if ( empty( $edd_api_request_transient ) ) {
			$api_response = $this->api_request( 'get_version', $to_send );

			// Expires in 3 hours.
			$this->set_cached_version_info( $api_response, $api_cache_key );

			if ( false !== $api_response ) {
				$_data = $api_response;
			}
		} else {
			$_data = $edd_api_request_transient;
		}

		// Convert sections into an associative array, since we're getting an object,
		// but Core expects an array.
		if ( isset( $_data->sections ) && ! is_array( $_data->sections ) ) {
			$_data->sections = $this->convert_object_to_array( $_data->sections );
		}

		// Convert banners into an associative array, since we're getting an object,
		// but Core expects an array.
		if ( isset( $_data->banners ) && ! is_array( $_data->banners ) ) {
			$_data->banners = $this->convert_object_to_array( $_data->banners );
		}

		// Convert icons into an associative array, since we're getting an object,
		// but Core expects an array.
		if ( isset( $_data->icons ) && ! is_array( $_data->icons ) ) {
			$_data->icons = $this->convert_object_to_array( $_data->icons );
		}

		if ( ! isset( $_data->plugin ) ) {
			$_data->plugin = $this->name;
		}

		return $_data;
	}

	/**
	 * Convert some objects to arrays when injecting data into the update API.
	 *
	 * Some data like sections, banners, and icons are expected to be an associative
	 * array, however due to the JSON decoding, they are objects. This method allows
	 * us to pass in the object and return an associative array.
	 *
	 * @since 0.1.0
	 * @param object $data The object to convert.
	 * @return array Array representation of the object.
	 */
	private function convert_object_to_array( $data ) {
		$new_data = [];
		foreach ( $data as $key => $value ) {
			$new_data[ $key ] = $value;
		}

		return $new_data;
	}

	/**
	 * Calls the API and, if successfull, returns the object delivered by the API.
	 *
	 * @uses get_bloginfo()
	 * @uses wp_remote_post()
	 * @uses is_wp_error()
	 *
	 * @since 0.1.0
	 * @param string $_action The API action.
	 * @param array  $_data   Parameters for the API action.
	 * @return false|object
	 */
	private function api_request( $_action, $_data ) {
		global $edd_plugin_url_available;

		$verify_ssl = $this->verify_ssl();

		// Do a quick status check on this domain if we haven't already checked it.
		$store_hash = md5( $this->api_url );
		if (
			! is_array( $edd_plugin_url_available ) ||
			! isset( $edd_plugin_url_available[ $store_hash ] )
		) {
			$test_url_parts = wp_parse_url( $this->api_url );

			$scheme = ! empty( $test_url_parts['scheme'] ) ? $test_url_parts['scheme'] : 'http';
			$host   = ! empty( $test_url_parts['host'] ) ? $test_url_parts['host'] : '';
			$port   = ! empty( $test_url_parts['port'] ) ? ':' . $test_url_parts['port'] : '';

			if ( empty( $host ) ) {
				$edd_plugin_url_available[ $store_hash ] = false;
			} else {
				$test_url = $scheme . '://' . $host . $port;
				$response = wp_remote_get(
					$test_url,
					[
						'timeout'   => $this->health_check_timeout,
						'sslverify' => $verify_ssl,
					]
				);

				$edd_plugin_url_available[ $store_hash ] = ! is_wp_error( $response );
			}
		}

		if ( false === $edd_plugin_url_available[ $store_hash ] ) {
			return false;
		}

		$data = array_merge( $this->api_data, $_data );

		if ( $data['slug'] !== $this->dirname ) {
			return false;
		}

		// Don't allow a plugin to ping itself, unless the API URL is divimode.com.
		if (
			'https://divimode.com/' !== $this->api_url
			&& trailingslashit( home_url() ) === $this->api_url
		) {
			return false;
		}

		$api_params = [
			'edd_action' => $_action,
			'license'    => ! empty( $data['license'] ) ? $data['license'] : '',
			'item_name'  => isset( $data['item_name'] ) ? $data['item_name'] : false,
			'item_id'    => isset( $data['item_id'] ) ? $data['item_id'] : false,
			'version'    => isset( $data['version'] ) ? $data['version'] : false,
			'slug'       => $data['slug'],
			'author'     => $data['author'],
			'url'        => home_url(),
			'beta'       => ! empty( $data['beta'] ),
		];

		$request = wp_remote_post(
			$this->api_url,
			[
				'timeout'   => 15,
				'sslverify' => $verify_ssl,
				'body'      => $api_params,
			]
		);

		if ( ! is_wp_error( $request ) ) {
			$request = json_decode( wp_remote_retrieve_body( $request ) );
		}

		if ( $request && isset( $request->sections ) ) {
			$request->sections = maybe_unserialize( $request->sections );
		} else {
			$request = false;
		}

		if ( $request && isset( $request->banners ) ) {
			$request->banners = maybe_unserialize( $request->banners );
		}

		if ( $request && isset( $request->icons ) ) {
			$request->icons = maybe_unserialize( $request->icons );
		}

		if ( ! empty( $request->sections ) ) {
			foreach ( $request->sections as $key => $section ) {
				$request->$key = (array) $section;
			}
		}

		return $request;
	}

	/**
	 * Returns version details that were previously stored in the options table.
	 *
	 * @since 0.1.0
	 * @param  string $cache_key Optional, the options-key to retrieve.
	 * @return mixed  The previously stored version details.
	 */
	public function get_cached_version_info( $cache_key = '' ) {
		if ( empty( $cache_key ) ) {
			$cache_key = $this->cache_key;
		}

		$cache = get_option( $cache_key );

		if ( empty( $cache['timeout'] ) || time() > $cache['timeout'] ) {
			return false; // Cache is expired.
		}

		$cache['value'] = json_decode( $cache['value'] );
		if ( ! empty( $cache['value']->icons ) ) {
			$cache['value']->icons = (array) $cache['value']->icons;
		}

		return $cache['value'];
	}

	/**
	 * Stores the given version details in the options table, for three hours.
	 *
	 * @since 0.1.0
	 * @param  string $value     Version details to store.
	 * @param  string $cache_key Optional, the option-key to use.
	 * @return void
	 */
	public function set_cached_version_info( $value = '', $cache_key = '' ) {
		if ( empty( $cache_key ) ) {
			$cache_key = $this->cache_key;
		}

		$data = [
			'timeout' => strtotime( '+3 hours', time() ),
			'value'   => wp_json_encode( $value ),
		];

		update_option( $cache_key, $data, 'no' );
	}

	/**
	 * Clears the internal cache.
	 *
	 * @since 0.1.2
	 * @return void
	 */
	public function clear_cache() {
		$cache_keys = [
			$this->cache_key,
			$this->get_cache_key( 'edd_sl' ),
			$this->get_cache_key( 'edd_api_request' ),
		];

		$cache_keys = array_unique( $cache_keys );
		$cache_keys = array_filter( $cache_keys );

		foreach ( $cache_keys as $key ) {
			delete_option( $key );
		}
	}

	/**
	 * Returns if the SSL of the store should be verified.
	 *
	 * @since 0.1.0
	 * @return bool
	 */
	private function verify_ssl() {
		return (bool) apply_filters( 'edd_sl_api_request_verify_ssl', true, $this );
	}
}
