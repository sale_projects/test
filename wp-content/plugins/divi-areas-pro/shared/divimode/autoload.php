<?php
/**
 * The divimode.com shared library.
 *
 * Provides functions that are common across divimode plugins, to increase
 * consistency and stability.
 *
 * @package     Divimode
 * @author      Philipp Stracker
 */

namespace DiviAreasPro;

defined( 'ABSPATH' ) || die();

/**
 * Simple handler for spl_autoload_register (autoload classes on demand).
 *
 * @since  0.1.0
 * @param  string $symbol The symbol to load, e.g., a class, interface or trait.
 * @return bool True if the symbol-file was found and loaded.
 */
function divimode_autoloader( $symbol ) {
	if ( 0 !== strpos( $symbol, __NAMESPACE__ . '\DM_' ) ) {
		// Not part of this library, ignore it.
		return false;
	}

	$symbol = substr( strtolower( $symbol ), strlen( __NAMESPACE__ ) + 1 );

	// Load a "DM" symbol from the "include" folder.
	$symbol_file = str_replace( '_', '-', $symbol ) . '.php';

	/*
	Get the relative path of the symbol by removing the prefix "dm" (which stans
	for "divimode") and the last element form the name.

	Examples:
	- DM_Settings --> "" (empty string, folder is "/")
	- DM_Settings_Page --> "Settings" (folder is "/settings")
	*/
	$dir_segments = explode( '_', substr( $symbol, 3 ) );
	array_pop( $dir_segments );
	$dir_segments[] = '';

	$symbol_dir = 'include/' . implode( '/', $dir_segments );

	// First assume that we have a class name.
	$symbol_path = __DIR__ . "/{$symbol_dir}class-{$symbol_file}";

	// When there is no class with the given name, maybe it's a trait.
	if ( ! file_exists( $symbol_path ) ) {
		$symbol_path = __DIR__ . "/{$symbol_dir}trait-{$symbol_file}";
	}

	if ( ! file_exists( $symbol_path ) ) {
		return false;
	}

	include_once $symbol_path;
	return true;
}

// Initialize the autoloader.
spl_autoload_register( __NAMESPACE__ . '\divimode_autoloader' );
