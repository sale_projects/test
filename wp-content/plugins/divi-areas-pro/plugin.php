<?php
/**
 * Divi Areas Pro.
 *
 * Officially this is the premium version of Popups for Divi.
 * But actually it's much more!
 *
 * @formatter:off
 * @package     Divi_Areas_Pro
 * @author      Philipp Stracker
 *
 * Plugin Name: Divi Areas Pro
 * Plugin URI:  https://divimode.com/divi-areas-pro/?utm_source=wpadmin&utm_medium=link&utm_campaign=divi-areas-pro
 * Description: Add a global footer, popups, hover-menus, tooltips and more to any Divi page!
 * Author:      Philipp Stracker
 * Author URI:  https://divimode.com/?utm_source=wpadmin&utm_medium=link&utm_campaign=divi-areas-pro
 * Created:     04.08.2019
 * Version:     2.3.1
 * Text Domain: divi_areas
 * Domain Path: /lang
 * License:     GPL v2 or later
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 *
 * Copyright (C) 2020 Philipp Stracker
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 *
 * @formatter:on
 */

defined( 'ABSPATH' ) || die();

// Avoid errors when installing two versions of the plugin (mainly for development).
if ( defined( 'DIVI_AREAS_PLUGIN_FILE' ) || defined( 'DIVI_AREAS_VERSION' ) ) {
	return;
}

/**
 * A new version string will force a refresh of CSS and JS files for all users.
 *
 * @var string
 */
define( 'DIVI_AREAS_VERSION', '2.3.1' );

/**
 * Absolute path and file name of the main plugin file.
 *
 * @var string
 */
define( 'DIVI_AREAS_PLUGIN_FILE', __FILE__ );

/**
 * Basename of the WordPress plugin. I.e., "plugin-dir/plugin-file.php".
 *
 * @var string
 */
define( 'DIVI_AREAS_PLUGIN', plugin_basename( DIVI_AREAS_PLUGIN_FILE ) );

/**
 * Absolute path to the plugin folder, with trailing slash.
 *
 * @var string
 */
define( 'DIVI_AREAS_PATH', plugin_dir_path( DIVI_AREAS_PLUGIN_FILE ) );

/**
 * Absolute URL to the plugin folder, with trailing slash.
 *
 * @var string
 */
define( 'DIVI_AREAS_URL', plugin_dir_url( DIVI_AREAS_PLUGIN_FILE ) );

/**
 * The website where the plugin can be downloaded. Either the sales page on
 * divimode.com or the product URL on the Divi Marketplace.
 *
 * @since 2.0.0
 * @var string
 */
define( 'DIVI_AREAS_STORE_URL', 'https://www.elegantthemes.com/marketplace/divi-areas-pro/' );

/**
 * Store URL, used to serve plugin updates.
 *
 * @var string
 */
define( 'DIVI_AREAS_SL_STORE_URL', 'https://license.divimode.com' );

/**
 * ID of the plugin in the store, used to serve plugin updates.
 *
 * @var int
 */
define( 'DIVI_AREAS_SL_ITEM_ID', 14 );

/**
 * The Helpscout beacon used by this plugin.
 *
 * @var string
 */
define( 'DIVI_AREAS_HS_BEACON', '32e2562c-4f95-42af-b6db-23cd4e007c49' );

/**
 * Plugin activation hook.
 *
 * The plugin activation workflow will first load all active plugins, but it does not
 * trigger the 'plugins_loaded' action! Instead, the activation hook is called,
 * followed by immediate 'shutdown' (i.e., refresh the page).
 *
 * That means, we can be sure that this whole file was processed (most importantly,
 * the `divi_areas_load_core` function was called), but not a single action was fired
 * when we arrive here.
 *
 * @since  0.1.0
 * @return void
 */
function divi_areas_install() {
	DAP_App::plugin_activated();
}

/**
 * Initialize and return the global DAP_App which is the official php API of the Divi
 * Areas Pro plugin.
 *
 * @since 1.2.3
 * @return DAP_App The main application instance.
 */
function divi_areas_pro() {
	if ( empty( $GLOBALS['divi_areas_pro'] ) ) {
		$GLOBALS['divi_areas_pro'] = new DAP_App();
		$GLOBALS['divi_areas_pro']->setup_on( 'plugins_loaded' );
	}

	return $GLOBALS['divi_areas_pro'];
}

/**
 * Instead of using an autoloader that dynamically loads our classes, we have decided
 * to include all dependencies during initialization.
 *
 * We have the following reasons for this:
 *
 * 1. It makes the plugin structure more transparent: We can see all used files here.
 * 2. The number of files is so small that autoloading does not save a lot of
 *    resources.
 * 3. In a production build we want to make sure that files are always loaded in the
 *    same order, at the same time.
 * 4. Every file is treated equal: No different treatment for classes vs function
 *    files.
 *
 * @since 2.0.0
 */
function divi_areas_load_core() {
	// Load helpers.
	require_once DIVI_AREAS_PATH . 'include/helper/plugin-compatibility.php';
	require_once DIVI_AREAS_PATH . 'include/helper/functions.php';
	require_once DIVI_AREAS_PATH . 'include/helper/files.php';
	require_once DIVI_AREAS_PATH . 'include/helper/template-functions.php';

	require_once DIVI_AREAS_PATH . 'include/helper/class-dap-integrate-wpml.php';

	// Load application modules.
	require_once DIVI_AREAS_PATH . 'include/core/class-dap-component.php';
	require_once DIVI_AREAS_PATH . 'include/core/class-dap-app.php';
	require_once DIVI_AREAS_PATH . 'include/core/class-dap-asset.php';
	require_once DIVI_AREAS_PATH . 'include/core/class-dap-post-type.php';
	require_once DIVI_AREAS_PATH . 'include/core/class-dap-category.php';
	require_once DIVI_AREAS_PATH . 'include/core/class-dap-layouts.php';
	require_once DIVI_AREAS_PATH . 'include/core/class-dap-library.php';
	require_once DIVI_AREAS_PATH . 'include/core/class-dap-editor.php';
	require_once DIVI_AREAS_PATH . 'include/core/class-dap-admin.php';
	require_once DIVI_AREAS_PATH . 'include/core/class-dap-front.php';
	require_once DIVI_AREAS_PATH . 'include/core/class-dap-updater.php';

	// Load object definition.
	require_once DIVI_AREAS_PATH . 'include/class/class-dap-area.php';

	// Load the shared library.
	require_once DIVI_AREAS_PATH . 'shared/divimode/autoload.php';
}

// Run a custom function every time the plugin is activated.
register_activation_hook( DIVI_AREAS_PLUGIN_FILE, 'divi_areas_install' );

// Load all dependencies.
divi_areas_load_core();

// Initialize the application.
divi_areas_pro();
