The full changelog is also available at https://divimode.com/divi-area-pro/changelog/

= 2.3.1 =
* New: Integrated plugin support on all Divi Areas pages! Look for the help-icon in the bottom corner.
* Fix: WordPress 5.6 compatibility (minor CSS changes in admin UI).
* Fix: Exit-Intent Popups do not need a Popup ID (but still it's recommended).
* Fix: More robust plugin initialization. Popups will work, even when Divi does not initialize correctly.
* Fix: Sections are not randomly renamed to "Popup - #undefined" anymore.
* Fix: The Theme Builder could sometimes miss Area layout settings, that's a thing of the past.
* Fix: Rare ReCaptcha bug that happened when no site_key was present for some reason.
* Fix: PHP Notice in plugin update module.
* Fix: JS error with the "Push Content" flag.

= Version 2.3.0 =
* New: Area Dimensions now support responsive values, to set custom widths based on the user's device.
* New: A new Size option in the Dimension tab enables you to quickly turn an Area to "full-width", "full-height", or "full-screen".
* New: Fly-In Areas can enable the "Push Content" layout to avoid overlapping of the Area and the page contents.
* New: WPML support to display Areas only for specific languages.
* Improve: Areas do not cover the Admin Toolbar anymore for logged in users.
* Improve: Introduce a new license server with a less restrictive firewall to make license activation more reliable.
* Improve: More robust plugin update logic and cache clearing mechanism.
* Change: New JS filter to customize local data storage - `storage_method` (and option `DiviAreaConfig.storageMethod`)
* Change: New JS filter to extend the push-content layout - `push_fixed_elements_{edge}`
* Change: New JS action to customize Area positioning - `position_boundary`
* Change: Many new PHP filters in the Divi Area metabox contents.
* Change: When a Popup, Fly-In, or Hover Area is visible, a new CSS class is added to the body tag.
* Fix: Open the Divi Lightbox in front of Popups and Fly-Ins.
* Fix: Compatibility with wpDataTables.
* Fix: Forminator ReCaptcha is supported inside Popups.
* Fix: Fix rare issue with size calculated, caused by incompatible Row settings.
* Fix: IE 11 support for Area conditions, like date schedule or referrer check.

= Version 2.2.1.1 =
* Fix: In rare cases, the Popups did not open due to Divi initializing the ReCaptcha module without a site-key.

= Version 2.2.1 =
* Fix: Typo in ReCaptcha code is fixed.
* Fix: Fix a rare issue on iOS that would reload the website when a Popup was triggered.
* Fix: The `background-repeat` CSS rule is now correctly applied to the background image of a Popup section.
* Fix: Compatibility with the new responsive mode of Divi 4.7

= Version 2.2.0 =
* New: Define default conditions for an Area to trigger it only when all conditions are met. Each trigger can overwrite those conditions with custom values.
* New: Schedule conditions to trigger an Area only on certain dates or days of a week.
* New: Referrer condition to trigger an Area only when the visitor arrives from a certain referrer.
* New: URL Param condition to trigger Area only when a certain URL param is present (or not present).
* New: The "On Scroll" trigger can receive an element selector to show the Area when an element becomes visible.
* Improve: We have made the Visual Builder integration (the "Popup-Tab") faster and more stable.
* Improve: Do not include the JS API for certain 404 results, such as missing images.
* Improve: The collapsed Divi Area meta box now sticks to the top of the window.
* Improve: The Area position is now reflected by the Visual Builder (left/center/right).
* Change: Removed the trigger type "URL Param" in favor of the new trigger condition with the same name.
* Change: Refactored automatic trigger code that results in up to 80% less inline JavaScript code.
* Change: New JS filter to add custom initialization code - `pre_init_area`
* Change: New JS API function `DiviAreaItem.isPrepared()` to check if an Area is fully initialized.
* Change: New JS filter to customize conditions - `area_general_conditions` and `area_trigger_conditions`
* Fix: Inline Areas can be triggered via Exit-Intent.
* Fix: Prevent the full-height mode for Areas that use the layout flag "Overflow: Show".
* Fix: Fix a bug where the "After Delay" trigger did not work with the value "instant" (after 0 seconds).
* Fix: The content of Code modules in Layout Library templates is now correctly imported.
* Fix: Calculate the correct height of multiline text fields in the Divi Area meta box.
* Fix: With the "Never Clip" layout, you can scroll the Areas content via the mouse wheel again.
* Fix: The layout of Areas with a custom height is now corrected.
* Fix: The Layout Library is now also available for new/unsaved Divi Areas.
* Fix: The very first Layout Library sync will now start right after activating the plugin.
* Fix: CF7 ReCaptcha is supported inside Popups.

= Version 2.1.2 =
* Improve: A smarter Area initialization allows the use of dynamic content (such as Ninja Forms).
* Improve: The welcome page shows an accurate changelog after updates.
* Improve: The preview of Divi Areas is more accurate.
* Improve: If licensing fails, a new error code is displayed inside the license nag on the settings page.
* Change: New JS filter to dynamically change an Areas z-index - `apply_z_index`
* Change: New JS filter to adjust Area initialization - `area_preparation_time`
* Change: The JS API function `DiviAreaItem.getData()` does not require a parameter anymore.
* Fix: Fixed the bug with the empty Page Rules dialog when editing Divi Areas!
* Fix: The z-index is correctly applied again.

= Version 2.1.1 =
* Improve: Areas can be triggered via URL hashes.
* Improve: The "Overflow" layout option is previewed in the Visual Builder.
* Fix: The Layout Library is now displayed correctly when creating a new Divi Area before saving it the first time.
* Fix: Improve the full-height calculation of Popups.

= Version 2.1.0 =
* New: We have launched our layout library! Use any of our pre-built Areas that come fully configured 🎨
* New: Portability support for Divi Areas! You can export and import Area settings (type, triggers, behavior, etc.)
* New: Added the "Overflow" layout option for Divi Areas.
* Improve: Area sizes are more accurate when using Divis responsive sizes.
* Improve: Images inside Areas are instantly loaded in Chrome (fixed a lazy-load bug).
* Improve: Area size is re-calculated when the Area contents change, e.g., when an accordion is opened or closed.
* Improve: When a Popup is opened, scrolling is disabled in all browsers without shifting the content!
* Change: New WP filter to define "active" Divi Areas `divi_areas_pre_get_all_areas`
* Change: New WP hooks for Area preview - `divi_areas_can_preview_area`, `divi_areas_preview_redirect_url`, `divi_areas_show_preview`.
* Change: New WP hooks to customize Area output - `divi_areas_get_area_flag`, `divi_areas_prepare_area_js_code`, `divi_areas_prepare_area_triggers`, `divi_areas_prepare_area_js_parts`.
* Change: New JS action that fires when an Area was resized `resize_area`.
* Change: New JS action to customize screen-position of an Area `position_area` (not available for Inline Areas).
* Fix: Full-Height Popups can be scrolled #again in Safari/iPhones.
* Fix: Hover Areas are positioned correctly on all pages.
* Fix: The error "Invalid position: center-center" was thrown by Hover Areas in some cases.
* Fix: Remove a duplicate toggle-button from the Divi Area meta boxes.
* Fix: Correct the full-height mode for Fly-Ins.
* Fix: Bug in the Divi Area Page-Rules selector is fixed, which displayed an empty page-list when editing an existing Area.

= Version 2.0.2 =
* New: Add a toggle in "Settings > Front End" to disable the plugin cache for development or debugging.
* Fix: Page Rules are working again.
* Fix: Lifetime licenses do not display an expiration date in the plugin settings anymore.
* Fix: WooCommerce pages now display all available Popups, not only the first one.

= Version 2.0.1 =
* Fix: Plugin does not break your website when Divi is deactivated.
* Fix: Correct a Page-Rule logic that only evaluated the first rule of a given type.
* Fix: Scroll to percent logic works again.
* Fix: Device conditions in automatic triggers are now working with minified js.
* Fix: Divi Areas work with IE 11.
* Fix: On-Page Popups were accidentally hidden inside the Visual Builder after saving the page the first time.
* Change: New template function to validate the current Visual Builder version: `divi_area_vb_version()`
* Change: New JS API function to identify an Area: `DiviAreaItem.hasId()`

= Version 2.0.0 =
* New: We have refactored the entire plugin!
* New: A new welcome page will be displayed after every plugin update to inform you about changes
* New: All Areas will now generate truly unique CSS classes to avoid any layout caching conflicts
* New: We have introduced new trigger types: On Inactivity, On Browser Focus, With URL Parameter
* New: The Visual Builder displays a preview of the Close Button (only in the Divi Areas editor)
* New: New configuration options for Area width and height for even more flexibility
* New: The plugin now uses Divis Theme-Builder logic to let you choose which pages should include (or exclude) an Area
* New: Use the brand-new Categories for grouping and filtering your Divi Areas!
* New: A new dynamic script generator generates highly optimized and reliable JS trigger code on-demand
* New: Improved inline documentation and help
* New: The Keep-Closed behavior has a new "Start Closed-Timer" option
* Improved: The Inline Area injection logic has become more reliable and flexible
* Improved: New logic to calculate Area size and position
* Fix: Popup text is no longer blurry in Windows browsers
* Change: Deprecated the "full-width" class, because it's not used anymore

= Version 1.4.6 =
* Fix: WordPress 5.5 compatibility changes in Area editor and settings page
* Fix: In some cases the Google reCaptcha inside a Popup would throw a JS error upon page load

= Version 1.4.5 =
* Fix: The close button does not trigger any scrollbars when hovered
* Fix: Popups are now always hidden when the page loads - in some cases, Popups inside Headers/Footers were visible right when the page loaded
* Fix: Click inside an open Popup does not try to re-open that Popup - i.e., fixed the "flickering issue."
* Fix: Accordions and other interactive elements inside Areas are working again
* Fix: Plugin is compatible with Gravity Forms 2.4.18+
* Fix: Bullet lists now display bullets inside Popups
* Improve: Plugin now plays nice with SG Optimizer and WP Rocket
* Improve: The close button is now outside the Popup container and can be positioned anywhere, via CSS
* Improve: Minor performance optimizations in the JS code

= Version 1.4.4 =
* New: Inline Areas can now be triggered like any other Area. And yes, they can dynamically replace any section on the page!
* New: The default shadow can now be disabled for Hover Areas
* New: All Areas can be triggered via CSS class. Not only Popups but everything!
* New: Some new methods in the JS class `DiviAreaItem` that are used by the JS API
* Improve: Click- and Hover-Triggers now support complex selectors, such as `header [href="/shop"]`
* Improve: Positioning of Hover Areas is much better; you could say, it's correct now
* Improve: Some UI changes in the "Area Trigger" meta box
* Fixed: The close button now works in Inline Areas
* Fixed: We found and fixed a problem with the Visual Builder when inserting Popups into blog posts
* Fixed: Fixed some nasty typos that could cause bugs
* Minor: Of course, improved some parts of the code, especially lots of comments

= Version 1.4.3 =
* New: Click triggers can be added to a Row or Section. The plugin now supports virtually any Divi "Link" field!
* New: Inline Areas are now injected into custom layouts that were designed with Divis Theme Builder!
* Improve: Divi Cache is now automatically cleared when a Divi Area is modified
* Improve: The loader code is now an inline script in the header. That adds compatibility with caching plugin, such as SG Optimizer or WP Rocket
* Improve: Popup content always expands downwards when a scrollbar is visible - for example when using Accordions inside a Popup
* Minor: Code cleanup and improvements

= Version 1.4.2 =
* Change: Default z-index of Popups now is "1000000" (that's one "0" more than before) to display Popups above a Full-Page menu
* Change: The DiviAreaConfig object is output in the page header so that values can be overwritten via JS in the page body
* Improve: When a popup is triggered inside a Full-Page menu, the menu is closed while the Popup is opened
* Improve: Optimize the Area display code for faster and cleaner display logic
* Improve: Now we support Popup triggers on any page (like shop pages or blog archive) and any element (like menus or footers)
* Improve: Full support for the Newsletter Module - the module did not work when used inside a Divi Area, though it worked in Popups for Divi
* Fix: Inline Areas are displayed correctly again
* Fix: The default Popup fade-in animation works again
* Fix: A Bug that would permanently disable scrolling of the page, when a Popup was closed while the background was still fading in

= Version 1.4.1 =
* Fix: The minified JS snippet for Divi Area injection threw a JS error
* Fix: When editing a Divi Area, the "Delete" icon in the post/page list did not work
* Fix: Editing a Divi Area will now hide invalid position options, depending on current Area type
* Fix: Custom Popup width now works on WooCommerce pages
* Fix: A PHP 5.6 error in the updater module is fixed
* Improve: Hide the "Saved templates" menu item, when Divi is not active
* Improve: License key can be set via the wp-config constant `DM_DIVI_AREAS_LICENSE`
* Improve: Many small changes throughout the plugin

= Version 1.4.0 - major developer release =
* Change: Fully refactored JS API that is fully documented on https://divimode.com/knowledge-base/
* Change: The JS configuration object changed its name to `DiviAreaConfig` (formerly `DiviPopupData`)
* Change: Some CSS class names have changed, e.g. "evr_fb_popup_modal" is now "da-overlay"
* New: JS class `DiviAreaItem` that represents a single Area (e.g., a Popup)
* New: JS API function: `DiviArea.getArea()`
* New: JS API hooks: `area_wrap`, `area_close_button`, `refresh_area`, `init_overlay`, `reorder_areas`
* Improve: Split the JS API into two files - a minimal loader that is enqueued in the header, and the rest of the API which is enqueued in the footer
* Improve: Support for the Divi Builder Plugin is even better, cases of missing CSS styles are fixed
* Improve: When a Popup is opened, the scroll bars of the page are not removed anymore
* Fix: Some script debugging options were incompatible with WordPress' new block editor

Here is a full list of all API changes in this update: https://divimode.com/api-1-2-0-changes/

= Version 1.3.1 =
* Change: JS API does not include deprecated function `observe()`! Use `addAction()` or `addFilter()` instead
* New: Details in the Updates tab of the plugin settings give you clear details on available updates
* New: Integration into the "Check Again" button of WordPress to manually check for plugin updates
* Improve: The updater-module to check for updates more frequently and smoothly work with the latest WordPress version
* Improve: The JS API further and expose additional functions
* Improve: Triggers on Modules: Links inside modules are treated as normal links, not as popup triggers
* Fix: The blur effect of the background overlay after the WooCommerce changes broke it
* Fix: Exit-intent Popup logic to recognize the "Keep Closed" setting
* Fix: Bugs on some WooCommerce and Divi Builder archive pages

= Version 1.3.0 =
* Add: Correct Area layout on WooCommerce pages
* Add: Compatibility with the Divi Builder plugin
* Improve: Exit-Intent logic so that multiple exit-intent popups are displayed one by one instead of all at once
* Improve: JS API: The `DiviArea` object exposes additional functions
* Improve: The minification of CSS files to generate ~16% smaller files
* Fix: Console error that said `could not load style.css.map`
* Fix: The "Close other Popups" behavior, so now it will really close other popups
* Fix: Some minor bugs in the popup behavior

= Version 1.2.4 =
* Add: New JS API: `DiviArea.addActionOnce()`
* Fix: A warning that showed up for logged in users when WordPress runs on a Windows Server

= Version 1.2.3 =
* Add: The new option "Show Loader" to the Popup Tab to better support iframes inside the Popup
* Improve: Input of Popup ID in Visual Builder to prevent invalid characters
* Improve: Code structure for better unit testing and quality assurance
* Fix: Some more JS errors that happened with specific versions of PHP/WordPress

= Version 1.2.2 =
* Improve: Onboarding: Redirect the user to license page on first plugin activation
* Improve: JavaScript error reporting in the dev console
* Fix: A PHP 5.6 error on the plugins settings page
* Fix: A rare JS error that was caused by wrong load-order of JS libraries
* Fix: Error that was raised when the plugin was activated on a site that did not use the Divi theme or Divi Builder plugin

= Version 1.2.1 =
* Fix: PHP notices and an error in the plugins settings page

= Version 1.2.0 =
* Add: A brand new Tab to the Visual Builder that allows you to configure all popup details using Divi! No more class names in the Frontend Builder 🥳
* Fix: An error when saving an element to the Divi Template Library in the Frontend Builder

= Version 1.1.0 =
* Improve: Admin UI in WordPress 5.3
* Improve: Developer integrations by providing new filters/actions
* Improve: And refactor the admin UI as preparation for future updates
* Fix: Scroll bars in Popups that are taller than the screen height
* Fix: Position of full-height popups that were displayed too far up on the screen
* Fix: A JS error in the options panel "Display: On Posts/Pages"
* Fix: The JS trigger description to display the correct snippets
* Fix: Some typos in the admin UI and help

= Version 1.0.0 =
* Add: Support for Hover Areas
* Add: Support to save any Divi Area to the Divi Template Library
* Add: Full editing support to the Divi Template Library: Edit all Area settings in the template
* Add: Close-button-support for Inline Areas
* Add: Two custom close-conditions with an optional close-delay for Hover Areas
* Add: Four positioning options and a new z-index field for Hover Areas
* Add: A new "Max-Height" option for all Areas
* Add: A new option on the Settings page to enable the script-debug mode for un-minified JS
* Improve: The admin UI stability (e.g., the number input fields had a rare rounding bug)
* Improve: The outputted JS code further
* Improve: Developer integration by adding more WP action hooks/filters and API functions
* Fix: A bug where the Popup overlay was hidden too early when displaying more than one Popup at once
* Fix: Wrong zIndex values when displaying multiple Areas at the same time

= Version 0.5.0 =
* Add: Support for the brand new Fly-in Areas
* Add: A new "Area Position" meta box for Popups and Fly-ins
* Add: A new tab on the Settings page to enable the JS debug-mode
* Add: Support for SCRIPT_DEBUG to output un-minified JS code in the page footer
* Improve: The JS API. We rewrote the API from ground-up with tons of features and enhancements. Detailed debug logging, an all-new DiviArea base object with support for WordPress-like hooks, and much more!
* Improve: The positioning and display of meta boxes in the Divi Area editor
* Improve: The display order of Divi Areas in the front end
* Fix: Some glitches in the JS script that triggers Divi Areas
* Fix: The User-Role conditions

= Version 0.4.2 =
* Fix: An issue that prevented advanced Popup triggers from firing (timed, on scroll)
* Fix: Animation glitch in Safari/iPhone that displayed the Popup too small when using Divis "Zoom" open animation.
* Fix: Logic that did not recognize Popups with upper-case letters in the Popup IDs.
* Fix: Issue with transparent Popup background.
* Fix: The CSS rule that allows custom box-shadow styles.

= Version 0.4.1 =
* Fix: A JS error that prevented Popups from opening in Firefox, Safari, Edge

= Version 0.4.0 =
* Change: Popup behavior: The size now matches the width of your Divi section! 🤩
* Change: the default layout of Popups to have no box-shadow. If you want a shadow, either add the class `with-shadow` to your section or use Divi to apply a box-shadow to the section. Style it your way!
* Add: Full support for the Inline-Area type!
* Add: New meta box to configure the Inline-Areas location
* Add: Option to inject Inline-Areas into Theme Locations: Header, Main-Menu, Post-Content, Comment-Form, Footer
* Add: Option to inject Inline-Areas before/after certain Divi sections inside the post/page content - via class or ID selector
* Add: New meta box to customize the Divi Area layout
* Add: Option to customize the Area width, enable the box-shadow and dark-mode (Close Button)
* Add: Support for URL triggers via the Blurb "Content Link"
* Add: The Popup behavior "Close other Popups"
* Add: Two new settings to customize the Divi Area editor
* Add: New WP filter options to customize dark-mode and box-shadow style
* Fix: Select list behavior in Firefox
* Fix: Popup width on iPhone 6 and earlier
* Improve: The description and inline-help for some options
* Minor improvements of the Admin UI

= Version 0.3.0 =
* Add: Option to hide the close button and make the popup modal
* Add: Option to keep a popup closed for s certain time span
* Add: Inline help for all Area options and settings
* Add: An option to use the Area slug as an alternate trigger ID
* Improved: Popup triggers, esp the scroll and exit-intent
* Many small improvements and fixes behind the scenes

= Version 0.2.0 =
* Add: A settings-page to the plugin
* Add: Automatic plugin updates via the WordPress plugin updater
* Add: The Popups for Divi JS library to the plugin to use it independently
* Add: A new Behavior meta box to control the Close Button
* Improve: The admin UI to feel more like Divi

= Version 0.1.0 =
* Add: A new admin section to create Divi Areas
* Add: UI to customize Area display conditions and triggers
* Add: Display conditions to target page/posts
* Add: Display conditions to target devices (mobile, tablet, desktop)
* Add: Display conditions to target user roles
* Add: Triggers to show popups on click, hover, scroll, exit-intent and time delay
