<?php
/*
Plugin Name: Divi Mega Pro
Plugin URL: https://divilife.com/
Description: Create mega menus and tooltips from Divi Builder
Version: 1.1.1
Author: Divi Life — Tim Strifler
Author URI: https://divilife.com
*/

// Make sure we don't expose any info if called directly or may someone integrates this plugin in a theme
if ( class_exists('DiviMegaPro') || !defined('ABSPATH') || !function_exists( 'add_action' ) ) {
	
	return;
}

define( 'DIVI_MEGA_PRO_VERSION', '1.1.1');
define( 'DIVI_MEGA_PRO_PLUGIN_BASENAME', plugin_basename( __FILE__ ) );
define( 'DIVI_MEGA_PRO_PLUGIN_NAME', trim( dirname( DIVI_MEGA_PRO_PLUGIN_BASENAME ), '/' ) );
define( 'DIVI_MEGA_PRO_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
define( 'DIVI_MEGA_PRO_PLUGIN_URL', plugin_dir_url( __FILE__ ));
define( 'DIVI_MEGA_PRO_SERVER_TIMEZONE', 'UTC');
define( 'DIVI_MEGA_PRO_SCHEDULING_DATETIME_FORMAT', 'm\/d\/Y g:i A');

require_once( DIVI_MEGA_PRO_PLUGIN_DIR . '/class.divi-mega-pro.core.php' );

add_action( 'init', array( 'DiviMegaPro', 'init' ) );
	
if ( is_admin() || ( defined( 'WP_CLI' ) && WP_CLI ) ) {
	
	require_once( DIVI_MEGA_PRO_PLUGIN_DIR . '/class.divi-mega-pro.admin.core.php' );
	add_action( 'init', array( 'DiviMegaPro_Admin', 'init' ) );
}


// Load the API Key library if it is not already loaded
if ( ! class_exists( 'edd_divimegapro' ) ) {
	
	require_once( plugin_dir_path( __FILE__ ) . 'updater.php' );
	require_once( plugin_dir_path( __FILE__ ) . 'updater-admin.php' );
}