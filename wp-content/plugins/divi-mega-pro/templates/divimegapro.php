<?php

	$divibuilder = false;
	
	// is divi theme builder ?
	if ( is_singular() && 'on' === get_post_meta( $post_data->ID, '_et_pb_use_builder', true ) ) {
		$divibuilder = true;
	}
?>
<div id="divimegapro-container-<?php print $divimegapro_id;?>" class="divimegapro-container" data-animation="<?php print $dmp_animation ?>"
	data-bgcolor="<?php print $dmp_bg_color;?>" data-fontcolor="<?php print $dmp_font_color;?>" data-placement="<?php print $dmp_placement ?>" 
	data-margintopbottom="<?php print $dmp_margintopbottom ?>" data-megaprowidth="<?php print $dmp_megaprowidth ?>" data-megaprowidthcustom="<?php print $dmp_megaprowidth_custom ?>"
	data-triggertype="<?php print $dmp_triggertype ?>" data-exittype="<?php print $dmp_exittype ?>" data-exitdelay="<?php print $dmp_exitdelay ?>"
	data-enable_arrow="<?php print $dmp_enable_arrow ?>" data-arrowfeature_type="<?php print $dmp_arrowfeature_type ?>" 
	data-arrowfeature_width="<?php print $dmp_arrowfeature_width ?>" data-arrowfeature_height="<?php print $dmp_arrowfeature_height ?>">
	<div id="divimegapro-<?php print $post_data->ID;?>" class="divimegapro<?php if ( !$divibuilder ) { print ' divimegapro-nob'; } ?>">
		<!--
		<svg class="divimegapro-shape" width="100%" height="100%" viewBox="0 0 400 300">
			<path d="M 79.5,66 C 79.5,66 128,106 202,105 276,104 321,66 321,66 321,66 287,84 288,155 289,226 318,232 318,232 318,232 258,198 200,198 142,198 80.5,230 80.5,230 80.5,230 112,222 111,152 110,82 79.5,66 79.5,66 Z"/>
		</svg>
		-->
		<div class="divimegapro-pre-body">
			<div class="divimegapro-body">
				<?php print $body; ?>
			</div>
		</div>
		<?php if ( $dmp_enabledesktop == 1 || $dmp_enablemobile == 1 ) { ?>
		<div class="divimegapro-close-container<?php if ( $dmp_enabledesktop ) { print ' dmp_enabledesktop'; } ?><?php if ( $dmp_enablemobile ) { print ' dmp_enablemobile'; } ?>">
			<button type="button" class="divimegapro-close divimegapro-customclose-btn-<?php print $post_data->ID ?>">
				<span class="<?php if ( $dmp_customizeclosebtn[0] == 1 ) { ?>dmm-custom-btn<?php } ?>">&times;</span>
			</button>
		</div>
		<?php } ?>
	</div>
</div>