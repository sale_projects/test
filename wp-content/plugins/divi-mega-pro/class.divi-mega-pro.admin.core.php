<?php

	class DiviMegaPro_Admin {
		
		private static $_show_errors = FALSE;
		private static $initiated = FALSE;
		private static $helper_admin = NULL;
		
		public static $helper = NULL;
		
		/**
		 * Holds the values to be used in the fields callbacks
		 */
		public static $options;
		
		public static function init() {
			
			if ( ! self::$initiated ) {
				
				self::load_resources();
				
				self::init_hooks();
			}
		}
		
		
		private static function init_hooks() {
			
			self::$initiated = true;
			
			self::$helper = new DiviMegaPro_Helper();
			
			self::$helper_admin = new DiviMegaPro_Admin_Helper();
			
			// Admin styles/scripts
			add_action( 'admin_init', array( 'DiviMegaPro_Admin', 'register_assets' ) );
			add_action( 'admin_enqueue_scripts', array( 'DiviMegaPro_Admin', 'include_assets'), '999');
			
			// Add meta boxes
			add_action( 'add_meta_boxes', array( 'DiviMegaPro_Admin_Controller', 'add_meta_boxes') );
			
			// Add Divi Theme Builder
			add_filter( 'et_builder_post_types', array( 'DiviMegaPro_Admin', 'enable_divi_builder') );
			
			// Save post fields
			add_action( 'save_post', array( 'DiviMegaPro_Admin_Controller', 'save_post' ), 10, 2 );
			
			add_action( 'admin_menu', array( 'DiviMegaPro_Admin_Controller', 'add_admin_submenu' ), 5 );
			
			add_action( 'wp_ajax_nopriv_ajax_dmp_listposts', array( 'DiviMegaPro_Admin_Ajax', 'call_get_posts' ) );
			add_action( 'wp_ajax_ajax_dmp_listposts', array( 'DiviMegaPro_Admin_Ajax', 'call_get_posts' ) );
			
			// Register settings
			add_action( 'admin_init', array( 'DiviMegaPro_Admin', 'register_divimegapro_settings' ) );
		}
		
		
		protected static function load_resources() {
			
			require_once( DIVI_MEGA_PRO_PLUGIN_DIR . '/includes/class.divi-mega-pro.admin.controller.php' );
			require_once( DIVI_MEGA_PRO_PLUGIN_DIR . '/includes/class.divi-mega-pro.admin.helper.php' );
			require_once( DIVI_MEGA_PRO_PLUGIN_DIR . '/includes/class.divi-mega-pro.admin.model.php' );
			require_once( DIVI_MEGA_PRO_PLUGIN_DIR . '/includes/class.divi-mega-pro.admin.ajax.php' );
			require_once( DIVI_MEGA_PRO_PLUGIN_DIR . '/includes/class.divi-mega-pro.helper.php' );
		}
		
		
		public static function register_assets( $hook ) {
			
			wp_register_style( 'divi-mega-pro-wp-color-picker', DIVI_MEGA_PRO_PLUGIN_URL . 'assets/css/admin/cs-wp-color-picker.min.css', array( 'wp-color-picker' ), '1.0.0', 'all' );
			wp_register_script( 'divi-mega-pro-wp-color-picker', DIVI_MEGA_PRO_PLUGIN_URL . 'assets/js/admin/cs-wp-color-picker.min.js', array( 'wp-color-picker' ), '1.0.0', true );
			
			wp_register_style( 'divi-mega-pro-select2', DIVI_MEGA_PRO_PLUGIN_URL . 'assets/css/admin/select2.min.css', array(), '4.0.6', 'all' );
			wp_register_script( 'divi-mega-pro-select2', DIVI_MEGA_PRO_PLUGIN_URL . 'assets/js/admin/select2.full.min.js', array('jquery'), '4.0.6', true );
			wp_register_style( 'divi-mega-pro-admin-bootstrap', DIVI_MEGA_PRO_PLUGIN_URL . 'assets/css/admin/bootstrap.css', array(), '1.0.0', 'all' );
			wp_register_style( 'divi-mega-pro-admin-bootstrap-select', '//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/css/bootstrap-select.min.css', array(), '1.0.0', 'all' );
			wp_register_style( 'divi-mega-pro-select2-bootstrap', DIVI_MEGA_PRO_PLUGIN_URL . 'assets/css/admin/select2-bootstrap.min.css', array('divi-mega-pro-admin-bootstrap'), '1.0.0', 'all' );
			
			wp_register_style( 'divi-mega-pro-admin', DIVI_MEGA_PRO_PLUGIN_URL . 'assets/css/admin/admin.css', array(), '1.0.0', 'all' );
			wp_register_script( 'divi-mega-pro-admin-functions', DIVI_MEGA_PRO_PLUGIN_URL . 'assets/js/admin/admin-functions.js', array( 'jquery', 'divi-mega-pro-select2' ), '1.0.0', true );
		}
		
		
		public static function include_assets( $hook ) {
			
			$screen = get_current_screen();
			
			if ( $screen->post_type != 'divi_mega_pro' ) {
				return;
			}
			
			wp_enqueue_style( 'wp-color-picker' );
			wp_enqueue_style( 'divi-mega-pro-wp-color-picker');
			wp_enqueue_script( 'wp-color-picker' );
			wp_enqueue_script( 'divi-mega-pro-wp-color-picker');
			
			wp_enqueue_style( 'divi-mega-pro-select2' );
			wp_enqueue_style( 'divi-mega-pro-select2-bootstrap' );
			wp_enqueue_script( 'divi-mega-pro-select2' );
			
			wp_enqueue_style( 'divi-mega-pro-admin-bootstrap' );
			wp_enqueue_style( 'divi-mega-pro-admin-bootstrap-select' );
			
			wp_enqueue_style( 'divi-mega-pro-admin' );
			wp_enqueue_script( 'divi-mega-pro-admin-functions' );
		}
		
		
		public static function enable_divi_builder( $post_types ) {
			
			$post_types[] = 'divi_mega_pro';
			
			return $post_types;
		}
		
		
		public static function register_divimegapro_settings( $args ) {
			
			register_setting( 
				'divimegapro_settings', 
				'dmp_settings', 
				array( 'DiviBars_Admin', 'sanitize' ) 
			);
		}
		
		
		public static function print_description_settings() {
			
			print '';
		}
		
		/**
		 * Sanitize each setting field as needed
		 *
		 * @param array $input Contains all settings fields as array keys
		 */
		public static function sanitize( $input ) {
			
			$new_input = array();
			
			if ( isset( $input['dmp_custom_elems'] ) ) {
				
				$new_input['dmp_custom_elems'] = sanitize_text_field( $input['dmp_custom_elems'] );
			}
			
			if ( isset( $input['dmp_timezone'] ) ) {
				
				$new_input['dmp_timezone'] = sanitize_text_field( $input['dmp_timezone'] );
			}
			
			return $new_input;
		}
		
		public static function parse_fields_callback( $options ) {
			
			$field_type = isset( $options['type'] ) ? $options['type'] : '';
			
			$field_name = $optionname = isset( $options['name'] ) ? $options['name'] : '';
			
			$field_default_value = isset( $options['default_value'] ) ? $options['default_value'] : '';
			
			if ( 'text' == $field_type ) {
				
				printf(
					'<input type="text" id="' . $field_name . '" name="dmp_settings[' . $field_name . ']" value="%s" />',
					isset( self::$options[ $field_name ] ) ? esc_attr( self::$options[ $field_name ] ) : $field_default_value
				);
			}
			else if ( 'select' == $field_type ) {
				
				$valid_options = array();
				
				$selected = isset( self::$options[ $field_name ] ) ? esc_attr( self::$options[ $field_name ] ) : $field_default_value;
				
				if ( $selected != $field_default_value ) {
					
					$field_default_value = $selected;
				}
				
				?>
				<select name="dmp_settings[<?php print $field_name; ?>]" data-defaultvalue="<?php print $field_default_value ?>" class="select-<?php print $options['name'] ?>">
				<?php
				
				if ( isset( $options['options'] ) ) {
				
					foreach ( $options['options'] as $option ) {
						
						?>
						<option <?php selected( $selected, $option['value'] ); ?> value="<?php print $option['value']; ?>"><?php print $option['title']; ?></option>
						<?php
					}
				}
				
				?>
				</select>
				<?php
			}
		}
		
	} // end DiviMegaPro_Controller