/**
 * Plugin Name: Divi Mega Pro
 * Plugin URL: https://divilife.com/
 * Description: Create mega menus and tooltips from Divi Builder
 * Version: 1.0
 * Author: Divi Life — Tim Strifler
 * Author URI: https://divilife.com
*/
;(function ( $, window, document, undefined ) {
  'use strict';
  
    $.fn.mainDiviMegaPro = function( options ) {
		
		var divimegapro_body
		, divimegapro
		, idx_divimegapro
		, divi_divimegapro_container_selector
		, $divimegapro = this
		, contentLengthcache
		, divimegaproHeightCache
		, diviMobile
		, themesBreakpoint = { Divi:980, Extra:1024 }
		, styleTagID = 'divi-mega-pro-styles'
		, vw
		, fixedElements
		, scrollCheck
		, diviElement_togglePro
		, dmps = []
		, animations = {
			cora: {
				in: {
					base: {
						duration: 600,
						easing: 'easeOutQuint',
						scale: [0,1],
						rotate: [-180,0],
						opacity: {
							value: 1,
							easing: 'linear',
							duration: 100
						}
					},
					content: {
						duration: 300,
						delay: 250,
						easing: 'easeOutQuint',
						translateY: [20,0],
						opacity: {
							value: 1,
							easing: 'linear',
							duration: 100
						}
					},
					trigger: {
						duration: 300,
						easing: 'easeOutExpo',
						scale: [1,0.9],
						color: '#6fbb95'
					}
				},
				out: {
					base: {
						duration: 150,
						delay: 50,
						easing: 'easeInQuad',
						scale: 0,
						opacity: {
							delay: 100,
							value: 0,
							easing: 'linear'
						}
					},
					content: {
						duration: 100,
						easing: 'easeInQuad',
						translateY: 20,
						opacity: {
							value: 0,
							easing: 'linear'
						}
					},
					trigger: {
						duration: 300,
						delay: 50,
						easing: 'easeOutExpo',
						scale: 1,
						color: '#666'
					}
				}
			},
			smaug: {
				in: {
					base: {
						duration: 200,
						easing: 'easeOutQuad',
						rotate: [35,0],
						opacity: {
							value: 1,
							easing: 'linear',
							duration: 100
						}
					},
					content: {
						duration: 1000,
						delay: 50,
						easing: 'easeOutElastic',
						translateX: [50,0],
						rotate: [10, 0],
						opacity: {
							value: 1,
							easing: 'linear',
							duration: 100
						}
					},
					trigger: {
						translateX: [
							{value: '-30%', duration: 130, easing: 'easeInQuad'},
							{value: ['30%','0%'], duration: 900, easing: 'easeOutElastic'}
						],
						opacity: [
							{value: 0, duration: 130, easing: 'easeInQuad'},
							{value: 1, duration: 130, easing: 'easeOutQuad'}
						],
						color: [
							{value: '#6fbb95', duration: 1, delay: 130, easing: 'easeOutQuad'}
						]
					}
				},
				out: {
					base: {
						duration: 200,
						delay: 100,
						easing: 'easeInQuad',
						rotate: -35,
						opacity: 0
					},
					content: {
						duration: 200,
						easing: 'easeInQuad',
						translateX: -30,
						rotate: -10,
						opacity: 0
					},
					trigger: {
						translateX: [
							{value: '-30%', duration: 200, easing: 'easeInQuad'},
							{value: ['30%','0%'], duration: 200, easing: 'easeOutQuad'}
						],
						opacity: [
							{value: 0, duration: 200, easing: 'easeInQuad'},
							{value: 1, duration: 200, easing: 'easeOutQuad'}
						],
						color: [
							{value: '#666', duration: 1, delay: 200, easing: 'easeOutQuad'}
						]
					}
				}
			},
			uldor: {
				in: {
					base: {
						duration: 400,
						easing: 'easeOutExpo',
						scale: [0.5,1],
						opacity: {
							value: 1,
							easing: 'linear',
							duration: 100
						}
					},
					path: {
						duration: 900,
						easing: 'easeOutElastic',
						d: 'M 33.5,31 C 33.5,31 145,31 200,31 256,31 367,31 367,31 367,31 367,110 367,150 367,190 367,269 367,269 367,269 256,269 200,269 145,269 33.5,269 33.5,269 33.5,269 33.5,190 33.5,150 33.5,110 33.5,31 33.5,31 Z'
					},
					content: {
						duration: 900,
						easing: 'easeOutElastic',
						delay: 100,
						scale: [0.8,1],
						opacity: {
							value: 1,
							easing: 'linear',
							duration: 100
						}
					},
					trigger: {
						translateY: [
							{value: '-50%', duration: 100, easing: 'easeInQuad'},
							{value: ['50%','0%'], duration: 100, easing: 'easeOutQuad'}
						],
						opacity: [
							{value: 0, duration: 100, easing: 'easeInQuad'},
							{value: 1, duration: 100, easing: 'easeOutQuad'}
						],
						color: {
							value: '#6fbb95', 
							duration: 1, 
							delay: 100, 
							easing: 'easeOutQuad'
						}
					}
				},
				out: {
					base: {
						duration: 200,
						easing: 'easeInExpo',
						scale: 0.5,
						opacity: {
							value: 0,
							duration: 75,
							easing: 'linear'
						}
					},
					path: {
						duration: 200,
						easing: 'easeOutQuint',
						d: 'M 79.5,66 C 79.5,66 128,106 202,105 276,104 321,66 321,66 321,66 287,84 288,155 289,226 318,232 318,232 318,232 258,198 200,198 142,198 80.5,230 80.5,230 80.5,230 112,222 111,152 110,82 79.5,66 79.5,66 Z'
					},
					content: {
						duration: 100,
						easing: 'easeOutQuint',
						scale: 0.8,
						opacity: {
							value: 0,
							duration: 50,
							easing: 'linear'
						}
					},
					trigger: {
						translateY: [
							{value: '50%', duration: 100, easing: 'easeInQuad'},
							{value: ['-50%','0%'], duration: 100, easing: 'easeOutQuad'}
						],
						opacity: [
							{value: 0, duration: 100, easing: 'easeInQuad'},
							{value: 1, duration: 100, easing: 'easeOutQuad'}
						],
						color: {
							value: '#666', 
							duration: 1, 
							delay: 100, 
							easing: 'easeOutQuad'
						}
					}
				}
			},
			dori: {
				in: {
					base: {
						duration: 800,
						easing: 'easeOutElastic',
						translateY: [60,0],
						scale: [0.5,1],
						opacity: {
							value: 1,
							easing: 'linear',
							duration: 100
						}
					},
					path: {
						duration: 1200,
						delay: 50,
						easing: 'easeOutElastic',
						elasticity: 700,
						d: 'M 22,74.2 22,202 C 22,202 82,202 103,202 124,202 184,202 184,202 L 200,219 216,202 C 216,202 274,202 297,202 320,202 378,202 378,202 L 378,74.2 C 378,74.2 318,73.7 200,73.7 82,73.7 22,74.2 22,74.2 Z'
					},
					content: {
						duration: 300,
						delay: 100,
						easing: 'easeOutQuint',
						translateY: [20,0],
						opacity: {
							value: 1,
							easing: 'linear',
							duration: 100
						}
					},
					trigger: {
						translateY: [
							{value: '-50%', duration: 100, easing: 'easeInQuad'},
							{value: ['50%','0%'], duration: 100, easing: 'easeOutQuad'}
						],
						opacity: [
							{value: 0, duration: 100, easing: 'easeInQuad'},
							{value: 1, duration: 100, easing: 'easeOutQuad'}
						],
						color: {
							value: '#6fbb95', 
							duration: 1, 
							delay: 100, 
							easing: 'easeOutQuad'
						}
					}
				},
				out: {
					base: {
						duration: 200,
						easing: 'easeInQuad',
						translateY: 60,
						scale: 0.5,
						opacity: {
							value: 0,
							delay: 100,
							duration: 100,
							easing: 'linear'
						}
					},
					path: {
						duration: 200,
						easing: 'easeInQuad',
						d: 'M 22,108 22,236 C 22,236 64,216 103,212 142,208 184,212 184,212 L 200,229 216,212 C 216,212 258,207 297,212 336,217 378,236 378,236 L 378,108 C 378,108 318,83.7 200,83.7 82,83.7 22,108 22,108 Z'
					},
					content: {
						duration: 100,
						easing: 'easeInQuad',
						translateY: 20,
						opacity: {
							value: 0,
							easing: 'linear'
						}
					},
					trigger: {
						translateY: [
							{value: '-50%', duration: 100, easing: 'easeInQuad'},
							{value: ['50%','0%'], duration: 100, easing: 'easeOutQuad'}
						],
						opacity: [
							{value: 0, duration: 100, easing: 'easeInQuad'},
							{value: 1, duration: 100, easing: 'easeOutQuad'}
						],
						color: {
							value: '#666', 
							duration: 1, 
							delay: 100, 
							easing: 'easeOutQuad'
						}
					}
				}
			},
			walda: {
				in: {
					base: {
						duration: 100,
						easing: 'linear',
						opacity: 1
					},
					deco: {
						duration: 500,
						easing: 'easeOutExpo',
						scaleY: [0,1]
					},
					content: {
						duration: 125,
						easing: 'easeOutExpo',
						delay: function(t,i) {
							return i*15;
						},
						easing: 'linear',
						translateY: ['50%','0%'],
						opacity: [0,1]
					},
					trigger: {
						translateX: [
							{value: '30%', duration: 100, easing: 'easeInQuad'},
							{value: ['-30%','0%'], duration: 100, easing: 'easeOutQuad'}
						],
						opacity: [
							{value: 0, duration: 100, easing: 'easeInQuad'},
							{value: 1, duration: 100, easing: 'easeOutQuad'}
						],
						color: [
							{value: '#6fbb95', duration: 1, delay: 100, easing: 'easeOutQuad'}
						]
					}
				},
				out: {
					base: {
						duration: 100,
						delay: 100,
						easing: 'linear',
						opacity: 0
					},
					deco: {
						duration: 400,
						easing: 'easeOutExpo',
						scaleY: 0
					},
					content: {
						duration: 1,
						easing: 'linear',
						opacity: 0
					},
					trigger: {
						translateX: [
							{value: '30%', duration: 100, easing: 'easeInQuad'},
							{value: ['-30%','0%'], duration: 100, easing: 'easeOutQuad'}
						],
						opacity: [
							{value: 0, duration: 100, easing: 'easeInQuad'},
							{value: 1, duration: 100, easing: 'easeOutQuad'}
						],
						color: [
							{value: '#666', duration: 1, delay: 100, easing: 'easeOutQuad'}
						]
					}
				},	
			},
			gram: {
				in: {
					base: {
						duration: 400,
						easing: 'easeOutQuint',
						scaleX: [1.2,1],
						opacity: {
							value: 1,
							easing: 'linear',
							duration: 50
						}
					},
					path: {
						duration: 600,
						easing: 'easeOutQuint',
						d: 'M 92.4,79 C 136,79 156,79.1 200,79.4 244,79.7 262,79 308,79 354,79 381,111 381,150 381,189 346,220 308,221 270,222 236,221 200,221 164,221 130,222 92.4,221 54.4,220 19,189 19,150 19,111 48.6,79 92.4,79 Z'
					},
					content: {
						delay: 100,
						scale: {
							value: [0.8,1],
							duration: 300,
							easing: 'easeOutQuint'
						},
						opacity: {
							value: [0,1],
							easing: 'linear',
							duration: 100
						}
					},
					trigger: {
						duration: 300,
						easing: 'easeOutQuint',
						scale: [1,0.9],
						color: '#6fbb95'
					}
				},
				out: {
					base: {
						duration: 200,
						easing: 'easeInQuint',
						scaleX: 1.1,
						scaleY: 0.9,
						opacity: {
							value: 0,
							delay: 100,
							duration: 150,
							easing: 'linear'
						}
					},
					path: {
						duration: 200,
						easing: 'easeInQuint',
						d: 'M 92.4,79 C 136,79 154,115 200,116 246,117 263,80.4 308,79 353,77.6 381,111 381,150 381,189 346,220 308,221 270,222 236,188 200,188 164,188 130,222 92.4,221 54.4,220 19,189 19,150 19,111 48.6,79 92.4,79 Z'
					},
					content: {
						duration: 150,
						easing: 'easeInQuint',
						scale: 0.8,
						opacity: {
							value: 0,
							duration: 100,
							easing: 'linear'
						}
					},
					trigger: {
						duration: 200,
						easing: 'easeInQuint',
						scale: 1,
						color: '#666'
					}
				}
			},
			narvi: {
				in: {
					base: {
						duration: 1,
						easing: 'linear',
						opacity: 1
					},
					path: {
						duration: 800,
						easing: 'easeOutQuint',
						rotate: [0,90],
						opacity: {
							value: 1,
							duration: 200,
							easing: 'linear'
						}
					},
					content: {
						duration: 600,
						easing: 'easeOutQuint',
						translateX: [50,0],
						opacity: {
							value: 1,
							duration: 100,
							easing: 'linear'
						}
					},
					trigger: {
						translateX: [
							{value: '-30%', duration: 100, easing: 'easeInQuint'},
							{value: ['30%','0%'], duration: 250, easing: 'easeOutQuint'}
						],
						opacity: [
							{value: 0, duration: 100, easing: 'easeInQuint'},
							{value: 1, duration: 250, easing: 'easeOutQuint'}
						],
						color: [
							{value: '#6fbb95', duration: 1, delay: 100, easing: 'easeOutQuint'}
						]
					}
				},
				out: {
					base: {
						duration: 100,
						delay: 400,
						easing: 'linear',
						opacity: 0
					},
					path: {
						duration: 500,
						delay: 0,
						easing: 'easeInOutQuint',
						rotate: 0,
						opacity: {
							value: 0,
							duration: 50,
							delay: 210,
							easing: 'linear'
						}
					},
					content: {
						duration: 500,
						easing: 'easeInOutQuint',
						translateX: 100,
						opacity: {
							value: 0,
							duration: 50,
							delay: 210,
							easing: 'linear'
						}
					},
					trigger: {
						translateX: [
							{value: '30%', duration: 200, easing: 'easeInQuint'},
							{value: ['-30%','0%'], duration: 200, easing: 'easeOutQuint'}
						],
						opacity: [
							{value: 0, duration: 200, easing: 'easeInQuint'},
							{value: 1, duration: 200, easing: 'easeOutQuint'}
						],
						color: [
							{value: '#666', duration: 1, delay: 200, easing: 'easeOutQuint'}
						]
					}
				}
			},
			indis: {
				in: {
					base: {
						duration: 500,
						easing: 'easeOutQuint',
						translateY: [100,0],
						opacity: {
							value: 1,
							duration: 50,
							easing: 'linear'
						}
					},
					shape: {
						duration: 350,
						easing: 'easeOutBack',
						scaleY:  {
							value: [1.3,1],
							duration: 1300,
							easing: 'easeOutElastic',
							elasticity: 500
						},
						scaleX: {
							value: [0.3,1],
							duration: 1300,
							easing: 'easeOutElastic',
							elasticity: 500
						},
					},
					path: {
						duration: 450,
						easing: 'easeInOutQuad',
						d: 'M 44.5,24 C 148,24 252,24 356,24 367,24 376,32.9 376,44 L 376,256 C 376,267 367,276 356,276 252,276 148,276 44.5,276 33.4,276 24.5,267 24.5,256 L 24.5,44 C 24.5,32.9 33.4,24 44.5,24 Z'
					},	
					content: {
						duration: 300,
						delay: 50,
						easing: 'easeOutQuad',
						translateY: [10,0],
						opacity: {
							value: 1,
							easing: 'linear',
							duration: 100
						}
					},
					trigger: {
						translateY: [
							{value: '-50%', duration: 100, easing: 'easeInQuad'},
							{value: ['50%','0%'], duration: 100, easing: 'easeOutQuad'}
						],
						opacity: [
							{value: 0, duration: 100, easing: 'easeInQuad'},
							{value: 1, duration: 100, easing: 'easeOutQuad'}
						],
						color: {
							value: '#6fbb95', 
							duration: 1, 
							delay: 100, 
							easing: 'easeOutQuad'
						}
					}
				},
				out: {
					base: {
						duration: 320,
						delay: 50,
						easing: 'easeInOutQuint',
						scaleY: 1.5,
						scaleX: 0,
						translateY: -100,
						opacity: {
							value: 0,
							duration: 100,
							delay: 130,
							easing: 'linear'
						}
					},
					path: {
						duration: 300,
						delay: 50,
						easing: 'easeInOutQuint',
						d: 'M 44.5,24 C 138,4.47 246,-6.47 356,24 367,26.9 376,32.9 376,44 L 376,256 C 376,267 367,279 356,276 231,240 168,241 44.5,276 33.8,279 24.5,267 24.5,256 L 24.5,44 C 24.5,32.9 33.6,26.3 44.5,24 Z'
					},
					content: {
						duration: 300,
						easing: 'easeInOutQuad',
						translateY: -40,
						opacity: {
							value: 0,
							duration: 100,
							delay: 135,
							easing: 'linear'
						}
					},
					trigger: {
						translateY: [
							{value: '-50%', duration: 100, easing: 'easeInQuad'},
							{value: ['50%','0%'], duration: 100, easing: 'easeOutQuad'}
						],
						opacity: [
							{value: 0, duration: 100, easing: 'easeInQuad'},
							{value: 1, duration: 100, easing: 'easeOutQuad'}
						],
						color: {
							value: '#666', 
							duration: 1, 
							delay: 100, 
							easing: 'easeOutQuad'
						}
					}
				}
			},
			amras: {
				in: {
					base: {
						duration: 1,
						delay: 50,
						easing: 'linear',
						opacity: 1
					},
					path: {
						duration: 800,
						delay: 100,
						easing: 'easeOutElastic',
						delay: function(t,i) {
							return i*20;
						},
						scale: [0,1],
					},	
					content: {
						duration: 300,
						delay: 250,
						easing: 'easeOutExpo',
						scale: [0.7,1],
						opacity: {
							value: 1,
							easing: 'linear',
							duration: 100
						}
					},
					trigger: {
						translateY: [
							{value: '50%', duration: 100, easing: 'easeInQuad'},
							{value: ['-50%','0%'], duration: 100, easing: 'easeOutQuad'}
						],
						opacity: [
							{value: 0, duration: 100, easing: 'easeInQuad'},
							{value: 1, duration: 100, easing: 'easeOutQuad'}
						],
						color: {
							value: '#6fbb95', 
							duration: 1, 
							delay: 100, 
							easing: 'easeOutQuad'
						}
					}
				},
				out: {
					base: {
						duration: 1,
						delay: 450,
						easing: 'linear',
						opacity: 0
					},
					path: {
						duration: 500,
						easing: 'easeOutExpo',
						delay: function(t,i,c) {
							return (c-i-1)*40;
						},
						scale: 0
					},
					content: {
						duration: 300,
						easing: 'easeOutExpo',
						scale: 0.7,
						opacity: {
							value: 0,
							duration: 100,
							easing: 'linear'
						}
					},
					trigger: {
						translateY: [
							{value: '-50%', duration: 100, easing: 'easeInQuad'},
							{value: ['50%','0%'], duration: 100, easing: 'easeOutQuad'}
						],
						opacity: [
							{value: 0, duration: 100, easing: 'easeInQuad'},
							{value: 1, duration: 100, easing: 'easeOutQuad'}
						],
						color: {
							value: '#666', 
							duration: 1, 
							delay: 100, 
							easing: 'easeOutQuad'
						}
					}
				}
			},
			hador: {
				in: {
					base: {
						duration: 1,
						delay: 100,
						easing: 'linear',
						opacity: 1
					},
					path: {
						duration: 1000,
						easing: 'easeOutExpo',
						delay: function(t,i) {
							return i*150;
						},
						scale: [0,1],
						translateY: function(t,i,c) {
							return i === c-1 ? ['50%','0%'] : 0;
						},
						rotate: function(t,i,c) {
							return i === c-1 ? [90,0] : 0;
						}
					},	
					content: {
						duration: 600,
						delay: 750,
						easing: 'easeOutExpo',
						scale: [0.5,1],
						opacity: {
							value: 1,
							easing: 'linear',
							duration: 400
						}
					},
					trigger: {
						translateX: [
							{value: '30%', duration: 200, easing: 'easeInExpo'},
							{value: ['-30%','0%'], duration: 200, easing: 'easeOutExpo'}
						],
						opacity: [
							{value: 0, duration: 200, easing: 'easeInExpo'},
							{value: 1, duration: 200, easing: 'easeOutExpo'}
						],
						color: [
							{value: '#6fbb95', duration: 1, delay: 200, easing: 'easeOutExpo'}
						]
					}
				},
				out: {
					base: {
						duration: 1,
						delay: 450,
						easing: 'linear',
						opacity: 0
					},
					path: {
						duration: 300,
						easing: 'easeOutExpo',
						delay: function(t,i,c) {
							return (c-i-1)*50;
						},
						scale: 0
					},	
					content: {
						duration: 200,
						easing: 'easeOutExpo',
						scale: 0.7,
						opacity: {
							value: 0,
							duration: 50,
							easing: 'linear'
						}
					},
					trigger: {
						translateX: [
							{value: '30%', duration: 100, easing: 'easeInQuad'},
							{value: ['-30%','0%'], duration: 100, easing: 'easeOutQuad'}
						],
						opacity: [
							{value: 0, duration: 100, easing: 'easeInQuad'},
							{value: 1, duration: 100, easing: 'easeOutQuad'}
						],
						color: [
							{value: '#666', duration: 1, delay: 100, easing: 'easeOutQuad'}
						]
					}
				}
			},
			malva: {
				in: {
					base: {
						translateX: [
							{value: 3, duration: 100, delay: 150, easing: 'linear'},
							{value: -3, duration: 100, easing: 'linear'},
							{value: 3, duration: 100, easing: 'linear'},
							{value: -3, duration: 100, easing: 'linear'},
							{value: 3, duration: 100, easing: 'linear'},
							{value: -3, duration: 100, easing: 'linear'},
							{value: 3, duration: 100, easing: 'linear'},
							{value: -3, duration: 100, easing: 'linear'},
							{value: 0, duration: 100, easing: [0.1,1,0.3,1]},
						],
						translateY: [
							{value: -3, duration: 100, delay: 150, easing: 'linear'},
							{value: 3, duration: 100, easing: 'linear'},
							{value: -3, duration: 100, easing: 'linear'},
							{value: 3, duration: 100, easing: 'linear'},
							{value: -3, duration: 100, easing: 'linear'},
							{value: 3, duration: 100, easing: 'linear'},
							{value: -3, duration: 100, easing: 'linear'},
							{value: 3, duration: 100, easing: 'linear'},
							{value: 0, duration: 100, easing: [0.1,1,0.3,1]},
						],
						scale: [
							{value: [0,1.1], duration: 150,easing: [0.1,1,0.3,1]},
							{value: 1.2, duration: 800,easing: 'linear'},
							{value: 1, duration: 150, easing: [0.1,1,0.3,1] },
						],
						opacity: {
							value: 1,
							easing: 'linear',
							duration: 1
						}
					},
					content: {
						duration: 100,
						easing: 'linear',
						opacity: 1
					},
					trigger: {
						duration: 300,
						easing: 'easeOutExpo',
						scale: [1,0.9],
						color: '#6fbb95'
					}
				},
				out: {
					base: {
						duration: 150,
						delay: 50,
						easing: 'easeInQuad',
						scale: 0,
						opacity: {
							delay: 100,
							value: 0,
							easing: 'linear'
						}
					},
					content: {
						duration: 100,
						easing: 'easeInQuad',
						opacity: {
							value: 0,
							easing: 'linear'
						}
					},
					trigger: {
						duration: 300,
						delay: 50,
						easing: 'easeOutExpo',
						scale: 1,
						color: '#666'
					}
				}
			},
			sadoc: {
				in: {
					base: {
						duration: 1,
						delay: 100,
						easing: 'linear',
						opacity: 1,
						translateY: {
							value: [-40,0],
							duration: 800,
							easing: 'easeOutElastic'
						}
					},
					path: {
						duration: 600,
						easing: 'easeInOutSine',
						strokeDashoffset: [anime.setDashoffset, 0],
						fill: {
							value: '#141514',
							duration: 400,
							delay: 500,
							easing: 'linear'
						}
					},
					content: {
						duration: 800,
						delay: 420,
						easing: 'easeOutElastic',
						translateY: [20,0],
						opacity: {
							value: 1,
							easing: 'linear',
							duration: 100
						}
					},
					trigger: {
						translateY: [
							{value: '50%', duration: 100, easing: 'easeInQuad'},
							{value: ['-50%','0%'], duration: 100, easing: 'easeOutQuad'}
						],
						opacity: [
							{value: 0, duration: 100, easing: 'easeInQuad'},
							{value: 1, duration: 100, easing: 'easeOutQuad'}
						],
						color: {
							value: '#6fbb95', 
							duration: 1,
							delay: 100, 
							easing: 'easeOutQuad'
						}
					}
				},
				out: {
					base: {
						duration: 1,
						delay: 400,
						easing: 'linear',
						opacity: 0
					},
					path: {
						duration: 300,
						easing: 'easeInOutSine',
						strokeDashoffset: anime.setDashoffset,
						fill: {
							value: '#2a2c2b',
							duration: 400,
							easing: 'linear'
						}
					},
					content: {
						duration: 200,
						easing: 'easeOutExpo',
						translateY: 20,
						opacity: {
							value: 0,
							easing: 'linear',
							duration: 50
						}
					},
					trigger: {
						translateY: [
							{value: '50%', duration: 100, easing: 'easeInQuad'},
							{value: ['-50%','0%'], duration: 100, easing: 'easeOutQuad'}
						],
						opacity: [
							{value: 0, duration: 100, easing: 'easeInQuad'},
							{value: 1, duration: 100, easing: 'easeOutQuad'}
						],
						color: {
							value: '#666', 
							duration: 1, 
							delay: 100, 
							easing: 'easeOutQuad'
						}
					}
				}
			},
			cloud_bubble: {
				in: {
					base: {
						duration: 1,
						delay: 50,
						easing: 'linear',
						opacity: 1
					},
					path: {
						duration: 800,
						delay: 100,
						easing: 'easeOutElastic',
						delay: function(t,i) {
							return i*20;
						},
						scale: [0,1],
					},	
					content: {
						duration: 300,
						delay: 250,
						easing: 'easeOutExpo',
						scale: [0.7,1],
						opacity: {
							value: 1,
							easing: 'linear',
							duration: 100
						}
					},
					trigger: {
						translateY: [
							{value: '50%', duration: 100, easing: 'easeInQuad'},
							{value: ['-50%','0%'], duration: 100, easing: 'easeOutQuad'}
						],
						opacity: [
							{value: 0, duration: 100, easing: 'easeInQuad'},
							{value: 1, duration: 100, easing: 'easeOutQuad'}
						],
						color: {
							value: '#6fbb95', 
							duration: 1, 
							delay: 100, 
							easing: 'easeOutQuad'
						}
					}
				},
				out: {
					base: {
						duration: 1,
						delay: 450,
						easing: 'linear',
						opacity: 0
					},
					path: {
						duration: 500,
						easing: 'easeOutExpo',
						delay: function(t,i,c) {
							return (c-i-1)*40;
						},
						scale: 0
					},
					content: {
						duration: 300,
						easing: 'easeOutExpo',
						scale: 0.7,
						opacity: {
							value: 0,
							duration: 100,
							easing: 'linear'
						}
					},
					trigger: {
						translateY: [
							{value: '-50%', duration: 100, easing: 'easeInQuad'},
							{value: ['50%','0%'], duration: 100, easing: 'easeOutQuad'}
						],
						opacity: [
							{value: 0, duration: 100, easing: 'easeInQuad'},
							{value: 1, duration: 100, easing: 'easeOutQuad'}
						],
						color: {
							value: '#666', 
							duration: 1, 
							delay: 100, 
							easing: 'easeOutQuad'
						}
					}
				}
			}
		};
		
		
		if (typeof options == 'function') {
			options = { success: options };
		}
		else if ( options === undefined ) {
			options = {};
		}
		
		// Trying to prevent others plugins override Divi Mega Pro styles
		$( '<style id="' + styleTagID + '"></style>' ).appendTo( 'head' );
		
		if ( $('div.divimegapro-container').length ) {
			
			// Viewing from a non-large device?
			diviMobile = isDiviMobile();
			
			if ( diviMobile ) {
				
				diviElement_togglePro = $('.mobile_menu_bar_toggle');
				
				if ( diviElement_togglePro.length ) {
					
					diviElement_togglePro.on( 'click touchstart', function(e) {
						
						if ( $('.tippy-popper').length ) {
							
							for ( const popper of document.querySelectorAll('.tippy-popper') ) {
								
								const instance = popper._tippy;
								
								if ( instance.state.visible ) {
									
									instance.hide();
								}
							}
						}
					});
				}
			}
			
			var divimegapro_container = $( 'div.divimegapro-wrapper' )
			, container = $( 'div#page-container' )
			, positionTimer = 0
			, removeMonarchTimer = 0
			, addBodyMobileClassTimer = 0;
			
			// Remove any duplicated divimegapro
			$( divimegapro_container ).each(function () {
				$('[id="' + this.id + '"]:gt(0)').remove();
			});
			
			
			$( 'body [rel^="divimegapro"]' ).each(function() {
				
				var divimegaproArr = $(this).attr('rel').split('-')
				, divimegapro_id = parseInt( divimegaproArr[1] )
				, selector = this;
				
				if ( divimegapro_id ) {
				
					$(this).attr('data-divimegaproid', divimegapro_id);
					
					dmps[ divimegapro_id ] = '';
					createDiviMegaPro( divimegapro_id, selector );
				}
			});
			
			
			$( 'body [class*="divimegapro"]' ).each(function() {
			
				var divimegaproArr = $(this).attr('class')
				, divimegapro_match = divimegaproArr.match(/divimegapro-(\d+)/)
				, selector = this
				, divimegapro_id = null;
				
				if ( null != divimegapro_match ) {
					
					divimegapro_id = divimegapro_match[1];
					
					if ( divimegapro_id ) {
						
						$(this).attr('data-divimegaproid', divimegapro_id);
						
						dmps[ divimegapro_id ] = '';
						createDiviMegaPro( divimegapro_id, selector );
					}
				}
			});
			
			
			$('.nav a, .mobile_nav a').each(function( index,value ) {
				
				var href = $( value ).attr('href');
				
				if ( href !== undefined ) {
				
					idx_divimegapro = href.indexOf('divimegapro');
					
					if ( idx_divimegapro !== -1 ) {
						
						var idx_divimegaproArr = href.split('-');
						
						if ( idx_divimegaproArr.length > 1 ) {
							
							var divimegapro_id = parseInt( idx_divimegaproArr[1] )
							, selector = this;
							
							if ( divimegapro_id ) {
								
								$(this).attr('data-divimegaproid', divimegapro_id);
								
								if ( !( divimegapro_id in dmps ) ) {
									
									dmps[ divimegapro_id ] = '';
									createDiviMegaPro( divimegapro_id, selector );
								}
							}
						}
					}
				}
			});
			
			if ( typeof divimegapros_with_css_trigger !== 'undefined' ) {
				
				var dmpTriggerType = ''
				, dmp_container_selector
				, dmp_container
				, dmp_options;
				
				if ( $( divimegapros_with_css_trigger ).length > 0 ) {
					
					$.each( divimegapros_with_css_trigger, function( divimegapro_id, selector ) {
						
						dmps[ divimegapro_id ] = '';
						createDiviMegaPro( divimegapro_id, selector );
					});
				}
			}
			
			
			$('body').on( 'click touchstart', '.divimegapro-close', function(e) {
				
				if ( $( '.tippy-popper' ).is(':visible') ) {
					
					$.each( $( '[data-tippy]' ), function(e) {
						
						this._tippy.hide();
						
					});
				}
			});
			
			
			function createDiviMegaPro( divimegapro_id, selector ) {
				
				var divimegapro_selector = '#divimegapro-' + divimegapro_id
				, divimegapro = $( divimegapro_selector )
				, divimegapro_container_selector = '#divimegapro-container-' + divimegapro_id
				, divimegapro_container = $( divimegapro_container_selector );
				
				if ( typeof divimegapro_container.data() == 'undefined' ) {
					
					return;
				}
				
				var options = getOptions( divimegapro_container )
				, triggerType = options['triggertype']
				, trigger = triggerType
				, eventType =  'mouseenter focus'
				, hideOnClick = true
				, refElement = $( selector )
				, flip = true;
				
				if ( options['bgcolor'] != '' ) {
					
					$( divimegapro_selector + ' .divimegapro-pre-body' ).css( { 'background-color': options['bgcolor'] } );
				}
				
				
				if ( options['fontcolor'] != '' ) {
					
					$( divimegapro_selector + ' .divimegapro-pre-body *' ).css( 'color', options['fontcolor'] );
				}
				
				
				if ( !diviMobile ) {
					
					if ( triggerType == 'mouseenter focus' ) {
						
						triggerType = 'manual';
					}
					
					if ( options['exittype'] == 'hover' ) {
						
						if ( trigger == 'click' ) {
							
							eventType = 'click touchstart';
						}
						
						triggerType = 'manual';
						
						if ( trigger != 'hover' && options['exittype'] == 'hover' ) {
							
							hideOnClick = false;
						}
						
						$('body').on( 'mousemove click touchstart', function(e) {
							
							if ( undefined !== refElement[0]._tippy && refElement[0]._tippy.state.visible == true  ) {
								
								var tippyRef = $( '#tippy-' + refElement[0]._tippy.id );
								
								if ( !tippyRef.is(e.target) && tippyRef.has(e.target).length === 0 ) {
									
									if ( !refElement.is(e.target) && refElement.has(e.target).length === 0 ) {
										
										if ( e.type == 'click' || e.type == 'touchstart' ) {
											
											clearTimeout( dmps[ divimegapro_id ].mouseTimeout );
											refElement[0]._tippy.hide();
										}
										
										if ( options['delay'] > 0 ) {
											
											if ( dmps[ divimegapro_id ].hiding == 0 ) {
												
												dmps[ divimegapro_id ].hiding = 1;
													
												dmps[ divimegapro_id ].mouseTimeout = setTimeout(() => {
													
													refElement[0]._tippy.hide();
													
												}, options['delay']);
											}
											
										} else {
											
											refElement[0]._tippy.hide();
										}
									}
								}
								else if ( dmps[ divimegapro_id ].hiding == 1 ) {
									
									clearTimeout( dmps[ divimegapro_id ].mouseTimeout );
									dmps[ divimegapro_id ].hiding = 0;
								}
							}
						});
					}
				}
				else {
					
					triggerType = 'manual';
					eventType = 'click';
					flip = false;
				}
				
				
				if ( triggerType == 'manual' ) {
					
					refElement.on( eventType, function(e) {
						
						if ( undefined !== this._tippy ) {
						
							if ( this._tippy.state.visible == false ) {
								
								if ( $( '.tippy-popper' ).is(':visible') ) {
									
									$.each( $( '[data-tippy]' ), function(e) {
										
										this._tippy.hide();
										
									});
								}
								
								this._tippy.show();
							}
							else {
								
								if ( eventType == 'click touchstart' ) {
									
									this._tippy.hide();
								}
							}
						}
						
						if ( refElement.parents('.et_mobile_menu').is(':visible') ) {
							
							refElement.parents('.et_mobile_menu').addClass('dmp-divimobilemenu-visible');
							refElement.parents('.mobile_nav').removeClass('closed');
							refElement.parents('.mobile_nav').addClass('opened');
						}
						
						e.preventDefault();
						return false;
					});
				}
				
				removeMonarch();
				
				addBodyMobileClass();
				
				setTimeout( function() {
					
					dmps[ divimegapro_id ] = tippy( selector, {
						arrow: options['arrowEnabled'],
						arrowType: options['arrowType'],
						arrowTransform: 'scale(' + options['arrowWidth'] + ',' + options['arrowHeight'] + ')',
						placement: options['placement'],
						maxWidth: options['megaprowidth'],
						html: divimegapro_container_selector,
						animation: options['animation'],
						animateFill: false,
						distance: options['distance'],
						performance: true,
						interactive: true,
						zIndex: 16777271,
						delay: [ 0, options['delay'] ],
						trigger: triggerType,
						hideOnClick: hideOnClick,
						theme: 'dmmbasic',
						createPopperInstanceOnInit: true,
						flip: flip,
						onShow( instance ) {
							
							/* Advanced Animation 
							let dmmPathsSelector = '.divimegapro-shape path'
							, animOpts = { targets: dmmPathsSelector };
							anime.remove( dmmPathsSelector );
							anime(Object.assign(animOpts, animations['uldor']['in'].path));
							*/
							
							toggleSrcInPlayableTags( divimegapro );
							
							$( "html,body" ).addClass('divimegapro-open');
							
							divimegapro.addClass( 'dmp-open' );
						},
						onShown( instance ) {
							
							initDiviElements();
							
							if ( !refElement.is(':visible') ) {
								
								if ( instance.popper.className.indexOf('topfixed') == -1 ) {
									
									instance.popper.className = instance.popper.className + ' topfixed';
								}
							}
							
							if ( $( divimegapro_container_selector + ' ' + divimegapro_selector ).length ) {
							
								$( '.tippy-popper .tippy-content' ).html( '' );
							}
							
							$( divimegapro_selector ).appendTo( '.tippy-popper .tippy-content' );
							divimegapro.addClass( 'divimegapro-opened' );
						},
						onHide() {
							
							/* Advanced Animation 
							let dmmPathsSelector = '.divimegapro-shape path'
							, animOpts = { targets: dmmPathsSelector };
							anime.remove( dmmPathsSelector );
							anime(Object.assign(animOpts, animations['uldor']['out'].path));
							*/
							
							if ( refElement.parents('.et_mobile_menu').is(':visible') ) {
								
								refElement.parents('.mobile_nav').removeClass('closed');
								refElement.parents('.mobile_nav').addClass('opened');
								refElement.parents('.et_mobile_menu').removeClass('dmp-divimobilemenu-visible');
								refElement.parents('.et_mobile_menu').attr( 'style', 'display:block');
							}
							
							divimegapro.removeClass( 'dmp-open' );
							divimegapro.addClass( 'dmp-close' );
							
							divimegapro.removeClass( 'divimegapro-opened' );
						},
						onHidden() {
							
							divimegapro.removeClass( 'dmp-close' );
							
							$( "html,body" ).removeClass( 'divimegapro-open' );
							
							dmmTogglePlayableTags( divimegapro_selector );
						},
					});
					
					dmps[ divimegapro_id ].mouseTimeout = '';
					dmps[ divimegapro_id ].hiding = 0;
					
				}, 1);
			}
			
			
			function initDiviElements() {
				
				// Set Divi Elements
				var $et_pb_circle_counter = $(".divimegapro-body .et_pb_circle_counter"),
					$et_pb_number_counter = $(".divimegapro-body .et_pb_number_counter"),
					$et_pb_countdown_timer = $(".divimegapro-body .et_pb_countdown_timer");
					
				$('.divimegapro-body .et_animated').each(function() {
					et_remove_animation( $( this ) );
				});
				
				// Init Divi Elements
				setTimeout( function() {
					
					window.et_fix_testimonial_inner_width(), 
					$et_pb_circle_counter.length && window.et_pb_reinit_circle_counters($et_pb_circle_counter), 
					$et_pb_number_counter.length && window.et_pb_reinit_number_counters($et_pb_number_counter), 
					$et_pb_countdown_timer.length && window.et_pb_countdown_timer_init($et_pb_countdown_timer),
					window.et_reinit_waypoint_modules();
					
				}, 1);
			}
			
			
			function et_get_animation_classes() {
				return ["et_animated", "infinite", "fade", "fadeTop", "fadeRight", "fadeBottom", "fadeLeft", "slide", "slideTop", "slideRight", "slideBottom", "slideLeft", "bounce", "bounceTop", "bounceRight", "bounceBottom", "bounceLeft", "zoom", "zoomTop", "zoomRight", "zoomBottom", "zoomLeft", "flip", "flipTop", "flipRight", "flipBottom", "flipLeft", "fold", "foldTop", "foldRight", "foldBottom", "foldLeft", "roll", "rollTop", "rollRight", "rollBottom", "rollLeft"]
			}
			
			function et_remove_animation($element) {
				var animation_classes = et_get_animation_classes();
				$element.removeClass(animation_classes.join(" ")), $element.removeAttr("style")
			}
			
			
			function getOptions( divimegapro_container ) {
				
				var dmmdataObject = divimegapro_container.data()
				, options = [];
				
				options['animation'] = dmmdataObject['animation'];
				
				options['triggertype'] = 'mouseenter focus';
				if ( dmmdataObject['triggertype'] == 'click' ) {
					
					options['triggertype'] = 'click';
				}
				
				options['placement'] = dmmdataObject['placement'];
				options['distance'] = parseInt( dmmdataObject['margintopbottom'] );
				
				if ( !isInt( dmmdataObject['megaprowidth'] ) ) {
					
					options['megaprowidth'] = dmmdataObject['megaprowidthcustom'];
					
				} else {
					
					options['megaprowidth'] = dmmdataObject['megaprowidth'] + '%';
				}
				
				if ( dmmdataObject['exittype'] == 'click' ) {
					
					options['exittype'] = 'click';
				
				} else {
					
					options['exittype'] = 'hover';
				}
				
				if ( dmmdataObject['exitdelay'] != 0 
					&& dmmdataObject['exitdelay'] != '' 
					&& options['exittype'] == 'hover' ) {
				
					options['delay'] = dmmdataObject['exitdelay'] * 1000;
				
				} else {
					
					options['delay'] = 0;
				}
				
				
				/* Arrow Feature */
				if ( dmmdataObject['enable_arrow'] == 1 ) {
				
					options['arrowEnabled'] = 'true';
				}
				
				options['arrowType'] = dmmdataObject['arrowfeature_type'];
				options['arrowWidth'] = dmmdataObject['arrowfeature_width'] * 0.1;
				options['arrowHeight'] = dmmdataObject['arrowfeature_height'] * 0.1;
				
				
				options['bgcolor'] = dmmdataObject['bgcolor'];
				options['fontcolor'] = dmmdataObject['fontcolor'];
				
				return options;
			}
			
			
			function getScrollTop() {
				
				if ( typeof pageYOffset!= 'undefined' ) {
					
					// most browsers except IE before #9
					return pageYOffset;
				}
				else {
					
					var B = document.body; // IE 'quirks'
					var D = document.documentElement; // IE with doctype
					D = ( D.clientHeight ) ? D: B;
					
					return D.scrollTop;
				}
			}
			
			
			function toggleSrcInPlayableTags( str ) {
				
				str.find("iframe").each(function() { 
					var src = $(this).data('src');
					$(this).attr('src', src);  
				});
				
				return str;
			}
			
			
			function getActiveDiviMegaPro() {
				
				var divimegapro = null
				, divimegapro_id = null
				, elemID = null
				, placement = null;
				
				// find active divimegapro only in top
				divimegapro = $( 'body' ).find( '.divimegapro.open' );
				
				if ( !divimegapro ) {
					
					divimegapro = $( 'body' ).find( '.divimegapro.close' );
				}
				
				if ( divimegapro.length ) {
					
					var divimegaproArr = divimegapro.attr('id').split('-')
					, divimegapro_id = divimegaproArr[1];
				}
				
				return divimegapro_id;
			}
			
			
			function isInt(value) {
				var x;
				return isNaN(value) ? !1 : (x = parseFloat(value), (0 | x) === x);
			}
			
			
			function removeMonarch() {
				
				if ( $( '.divimegapro .et_social_inline' ).length ) {
					
					$( '.divimegapro .et_social_inline' ).parents('.et_pb_row').remove();
				}
				
				removeMonarchTimer = setTimeout(removeMonarch, 500);
			}
			
			function addBodyMobileClass() {
				
				var diviMobile = isDiviMobile();
				
				if ( diviMobile ) {
					
					$( 'body' ).addClass('divi-mega-pro-mobile');
				}
				else {
					
					$( 'body' ).removeClass('divi-mega-pro-mobile');
				}
				
				addBodyMobileClassTimer = setTimeout(addBodyMobileClass, 500);
			}
			
			
			function stopAutoPositionTimer() {
				
				if ( positionTimer ) {
					
					clearInterval( positionTimer );
					positionTimer = 0;
				}
			}
			
			
			// Belongs to https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions
			function escapeRegExp(string) {
				return string.replace(/[.*+?^${}()|[\]\\]/g, '\\$&'); // $& means the whole matched string
			}
			
			
			function isDiviMobile() {
				
				var currentTheme = detectTheme();
				
				// Get current device width
				vw = actual('width', 'px');
				
				diviMobile = ( vw < themesBreakpoint[ currentTheme ] ) ? true : false;
				
				return diviMobile;
			}
			
			
			function detectTheme() {
				
				var currentTheme = 'Divi';
				
				if ( $('body').hasClass('et_extra') ) {
					
					currentTheme = 'Extra';
				}
				
				return currentTheme;
			}
		}
		
	} // end mainDiviMegaPro
	
	
	// Find all iframes inside the divimegapros
	var $dibiframes = $( ".divimegapro-wrapper .divimegapro iframe" );
	
	setTimeout( function() { 
	
		$dibiframes.each( function() {
			
			var iframeHeight = this.height;
			
			if ( iframeHeight == '' ) {
				
				iframeHeight = $( this ).height();
			}
			
			iframeWidth = this.width;
			
			if ( iframeWidth == '' ) {
				
				iframeWidth = $( this ).width();
			}
			
			iframeHeight = parseInt( iframeHeight );
			iframeWidth = parseInt( iframeWidth );
			
			var ratio = iframeHeight / iframeWidth;
			
			$( this ).attr( "data-ratio", ratio )
			// Remove the hardcoded width & height attributes
			.removeAttr( "width" )
			.removeAttr( "height" );
			
			// Get the parent container's width
			var width = $( this ).parent().width();
			
			$( this ).width( width ).height( width * ratio );
		});
		
	}, 200);
	
	// Resize the iframes when the window is resized
	$(window).on('resize orientationchange', function() {
		
		$dibiframes.each( function() {
			// Get the parent container's width
			var width = $( this ).parent().width();
			$( this ).width( width )
			.height( width * $( this ).data( "ratio" ) );
		});
	});
	
	$(window).ready(function() {
		
		// Divi Cascade Fix
		if ( $('#et-builder-module-design-cached-inline-styles').length && !$('#cloned-et-builder-module-design-cached-inline-styles').length ) {
			
			// Divi Cached Inline Styles
			var divicachedcss = $( '#et-builder-module-design-cached-inline-styles' );
			var divicachedcsscontent = divicachedcss.html();
			
			// Remove #page-container from Divi Cached Inline Styles tag and cloning it to prevent issues
			divicachedcsscontent = divicachedcsscontent.replace(/\#page-container/g, ' ');
			
			$( divicachedcss ).after( '<style id="cloned-et-builder-module-design-cached-inline-styles">' + divicachedcsscontent + '</style>' );
		}
		
		$('body').prepend( $( "#sidebar-divimegapro" ) );
		
		setTimeout(function() {
			
			$('.divimegapro-wrapper .divimegapro').mainDiviMegaPro();
			
		}, 1);
	});
	
})( jQuery, window, document );