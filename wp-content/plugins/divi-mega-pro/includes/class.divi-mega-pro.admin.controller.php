<?php

	class DiviMegaPro_Admin_Controller {
		
		protected static $_show_errors = FALSE;
		
		public static function get_posts( $args = null ) {
			
			$post_types = get_post_types( array( 'public' => true ) );
			
			$excluded_post_types = array( 'attachment', 'revision', 'nav_menu_item', 'custom_css', 'et_pb_layout', 'divi_bars', 'divi_overlay', 'divi_mega_pro', 'customize_changeset' );
			
			$post_types = array_diff( $post_types, $excluded_post_types );
			
			if ( isset( $args['q'] ) ) {
				
				$q = $args['q'];
			}
			else {
				
				$q = '';
			}
			
			if ( isset( $args['page'] ) ) {
				
				$page = $args['page'];
			}
			else {
				
				$page = 1;
			}
			
			$args = array(
				'post_title_like' => $q,
				'post_type' => $post_types,
				'cache_results'  => false,
				'posts_per_page' => 7,
				'paged' => $page,
				'orderby' => 'id',
				'order' => 'DESC'
			);
			
			$posts = array();
			
			$total_count = 0;
			
			$query = new WP_Query( $args );
			
			$get_posts = $query->get_posts();
			
			$posts = array_merge( $posts, $get_posts );
			
			$total_count = (int) $query->found_posts;
			
			$posts = self::keysToLower( $posts );
			
			return array( 'total_count' => $total_count, 'items' => $posts );
		}
		
		
		private static function keysToLower( &$obj )
		{
			$type = (int) is_object($obj) - (int) is_array($obj);
			
			if ($type === 0) return $obj;
			
			foreach ($obj as $key => &$val) {
				
				$element = self::keysToLower($val);
				
				switch ($type) {
					
					case 1:
					
						if (!is_int($key) && $key !== ($keyLowercase = strtolower($key)))
						{
							unset($obj->{$key});
							$key = $keyLowercase;
						}
						$obj->{$key} = $element;
						
						break;
						
					case -1:
					
						if (!is_int($key) && $key !== ($keyLowercase = strtolower($key)))
						{
							unset($obj[$key]);
							$key = $keyLowercase;
						}
						$obj[$key] = $element;
						
						break;
				}
			}
			return $obj;
		}
		
		
		public static function add_meta_boxes() {
			
			$screen = get_current_screen();
			
			if ( $screen->post_type == 'divi_mega_pro' ) {
			
				$status = get_option( 'divilife_edd_divimegapro_license_status' );
				$check_license = divilife_edd_divimegapro_check_license( TRUE );
				if ( ( isset( $check_license->license ) && $check_license->license != 'valid' && 'add' == $screen->action )
					|| ( isset( $check_license->license ) && $check_license->license != 'valid' && 'edit' == $_GET['action'] ) 
					|| ( $status === false && 'add' == $screen->action )
					|| ( $status === false && 'edit' == $_GET['action'] ) 
					) {
					
					$message = '';
					$base_url = admin_url( 'edit.php?post_type=divi_mega_pro&page=divimegapro-settings' );
					$redirect = add_query_arg( array( 'sl_activation' => 'false', 'message' => urlencode( $message ) ), $base_url );
					
					wp_redirect( $redirect );
					exit();
				}
				
				add_meta_box( 
					'divimegapros_displaylocations_metabox1', 
					esc_html__( 'Display Locations', 'DiviMegaPro' ), 
					array( 'DiviMegaPro_Admin_Controller', 'divimegapros_displaylocations_callback' ),
					'divi_mega_pro', 
					'side'
				);		
				
				add_meta_box( 
					'divimegapros_animation_metabox2', 
					esc_html__( 'Mega Pro Animation', 'DiviMegaPro' ), 
					array( 'DiviMegaPro_Admin_Controller', 'divimegapros_animation_callback' ), 
					'divi_mega_pro', 
					'side'
				);
				
				add_meta_box( 
					'divimegapros_manualtriggers3', 
					esc_html__( 'Mega Pro Triggers', 'DiviMegaPro' ), 
					array( 'DiviMegaPro_Admin_Controller', 'divimegapros_triggers_callback' ), 
					'divi_mega_pro', 
					'side'
				);
				
				add_meta_box( 
					'divimegapros_displaysettings_metabox4', 
					esc_html__( 'Mega Pro Display Settings', 'DiviMegaPro' ), 
					array( 'DiviMegaPro_Admin_Controller', 'divimegapros_displaysettings_callback' ), 
					'divi_mega_pro', 
					'side'
				);
				
				add_meta_box( 
					'divimegapros_closecustoms_meta_box5', 
					esc_html__( 'Close Button Customizations', 'DiviMegaPro' ), 
					array( 'DiviMegaPro_Admin_Controller', 'divimegapros_closecustoms_callback' ), 
					'divi_mega_pro', 
					'side' 
				);
				
				add_meta_box( 
					'divimegapros_moresettings_metabox6', 
					esc_html__( 'Mega Pro Additional Settings', 'DiviMegaPro' ), 
					array( 'DiviMegaPro_Admin_Controller', 'divimegapros_moresettings_callback' ),
					'divi_mega_pro', 
					'side' 
				);
				
				add_meta_box( 
					'divimegapros_color_picker', 
					esc_html__( 'Mega Pro Background', 'DiviMegaPro' ), 
					array( 'DiviMegaPro_Admin_Controller', 'divimegapros_colorbox_callback' ), 
					'divi_mega_pro'
				);
			}
		}
		
		
		public static function divimegapros_animation_callback( $post ) {
			
			wp_nonce_field( 'divimegapros_animation', 'divimegapros_animation_nonce' );
			
			$dmp_animation = get_post_meta( $post->ID, 'dmp_animation', true );
			$dmp_animation_list = array(
				'shift-away'   => esc_html__( 'Shift Away', 'DiviMegaPro' ),
				'shift-toward'    => esc_html__( 'Shift Toward', 'DiviMegaPro' ),
				'perspective' => esc_html__( 'Perspective', 'DiviMegaPro' ),
				'fade' => esc_html__( 'Fade', 'DiviMegaPro' ),
				'scale' => esc_html__( 'Scale', 'DiviMegaPro' )
			);
			?>
			<div class="custom_meta_box">
				<p>
					<label for="dmp_animation" class="dmp-label-animation"><?php esc_html_e( 'Choose Animation', 'DiviMegaPro' ); ?>: </label>
					<select id="dmp_animation" name="dmp_animation" class="chosen">
					<?php
					foreach ( $dmp_animation_list as $animation_value => $animation_name ) {
						printf( '<option value="%2$s"%3$s>%1$s</option>',
							esc_html( $animation_name ),
							esc_attr( $animation_value ),
							selected( $animation_value, $dmp_animation, false )
						);
					} ?>
					</select>
				</p>
			</div> 
			<?php
		}
		
		
		public static function divimegapros_displaylocations_callback( $post ) {
		
			wp_nonce_field( 'divimegapros_displaylocations', 'divimegapros_displaylocations_nonce' );
			
			$at_pages = get_post_meta( $post->ID, 'dmp_css_selector_at_pages', true );
			$selectedpages = get_post_meta( $post->ID, 'dmp_css_selector_at_pages_selected' );
			$selectedexceptpages = get_post_meta( $post->ID, 'dmp_css_selector_at_pagesexception_selected' );
			
			if ( $at_pages == '' ) {
				
				$at_pages = 'all';
			}
			
			?>
			<div class="custom_meta_box">
				<div class="at_pages">
					<select name="dmp_css_selector_at_pages" class="at_pages chosen dmp-filter-by-pages" data-dropdownshowhideblock="1">
						<option value="all"<?php if ( $at_pages == 'all' ) { ?> selected="selected"<?php } ?> data-showhideblock=".do-list-exceptionpages-container">All pages</option>
						<option value="specific"<?php if ( $at_pages == 'specific' ) { ?> selected="selected"<?php } ?> data-showhideblock=".do-list-pages-container">Only specific pages</option>
					</select>
					<div class="do-list-pages-container<?php if ( $at_pages == 'specific' ) { ?> do-show<?php } ?>">
						<select name="dmp_css_selector_at_pages_selected[]" class="do-list-pages" data-placeholder="Choose posts or pages..." multiple tabindex="3">
						<?php
							if ( isset( $selectedpages[0] ) ) {
								
								foreach( $selectedpages[0] as $selectedidx => $selectedvalue ) {
									
									$post_title = get_the_title( $selectedvalue );
									
									print '<option value="' . $selectedvalue . '" selected="selected">' . $post_title . '</option>';
								}
							}
						?>
						</select>
					</div>
					<div class="do-list-exceptionpages-container<?php if ( $at_pages == 'all' ) { ?> do-show<?php } ?>">
						<h4 class="db-exceptedpages">Add Exceptions:</h4>
						<select name="dmp_css_selector_at_pagesexception_selected[]" class="do-list-pages" data-placeholder="Choose posts or pages..." multiple tabindex="3">
						<?php
							if ( isset( $selectedexceptpages[0] ) ) {
								
								foreach( $selectedexceptpages[0] as $selectedidx => $selectedvalue ) {
									
									$post_title = get_the_title( $selectedvalue );
									
									print '<option value="' . $selectedvalue . '" selected="selected">' . $post_title . '</option>';
								}
							}
						?>
						</select>
					</div>
				</div>
				<div class="clear"></div> 
			</div>
			<?php
		}
		
		
		public static function divimegapros_triggers_callback( $post ) {
			
			wp_nonce_field( 'divimegapros_triggers', 'divimegapros_triggers_nonce' );
			
			$dmp_css_selector = get_post_meta( $post->ID, 'dmp_css_selector', true );
			
			$screen = get_current_screen();
			
			if ( 'add' != $screen->action ) {
			?>
			<div class="custom_meta_box">
				<p>
					<label class="label-color-field"><p>Unique Mega Pro Class:</label> divimegapro-<?php print $post->ID ?></p>
				</p>
			</div> 
			<?php
			}
			?>
			<div class="custom_meta_box">
				<p>
					<label>CSS Selector Trigger:</label>
					<input class="dmp_css_selector" type="text" name="dmp_css_selector" value="<?php echo $dmp_css_selector; ?>"/>
				</p>
				<div class="clear"></div> 
			</div>
			<?php
		}
		
		
		public static function divimegapros_displaysettings_callback( $post ) {
		
			wp_nonce_field( 'divimegapros_displaysettings', 'divimegapros_displaysettings_nonce' );
			
			$dmp_margintopbottom = get_post_meta( $post->ID, 'dmp_margintopbottom', true );
			$dmp_placement = get_post_meta( $post->ID, 'dmp_placement', true );
			$dmp_megaprowidth = get_post_meta( $post->ID, 'dmp_megaprowidth', true );
			$dmp_megaprowidth_custom = get_post_meta( $post->ID, 'dmp_megaprowidth_custom', true );
			$enable_arrow = get_post_meta( $post->ID, 'dmp_enable_arrow' );
			$arrowtype = get_post_meta( $post->ID, 'dmp_arrowfeature_type', true );
			$arrowcolor = get_post_meta( $post->ID, 'dmp_arrowfeature_color', true );
			$arrowwidth = get_post_meta( $post->ID, 'dmp_arrowfeature_width', true );
			$arrowheight = get_post_meta( $post->ID, 'dmp_arrowfeature_height', true );
			
			if ( $dmp_placement == '' ) {
				
				$dmp_placement = 'bottom';
			}
			
			$dmp_ats = array(
				'top' => esc_html__( 'Up', 'DiviMegaPro' ),
				'bottom' => esc_html__( 'Down', 'DiviMegaPro' )
			);
			?>
			<div class="custom_meta_box">
				<p class="dmp_placement et_pb_single_title">
					<label for="dmp_placement">Display Direction:</label>
					<select id="dmp_placement" name="dmp_placement" class="dmp_placement chosen">
					<?php
					foreach ( $dmp_ats as $at_value => $at_name ) {
						printf( '<option value="%2$s"%3$s>%1$s</option>',
							esc_html( $at_name ),
							esc_attr( $at_value ),
							selected( $at_value, $dmp_placement, false )
						);
					} ?>
					</select>
				</p>
			</div>
			<div class="custom_meta_box">
				<p>
					<label>Margin Top/Bottom:</label>
					<input class="dmp_margintopbottom" type="text" name="dmp_margintopbottom" value="<?php echo $dmp_margintopbottom; ?>"/>
				</p>
				<div class="clear"></div> 
			</div>
			<?php
			
			if ( !isset( $dmp_megaprowidth ) ) {
				
				$dmp_megaprowidth = '100';
			}
			
			if ( $dmp_megaprowidth == '' ) {
				
				$dmp_megaprowidth = '100';
			}
			
			$dmp_ats = array(
				'100' => esc_html__( '100%', 'DiviMegaPro' ),
				'75' => esc_html__( '75%', 'DiviMegaPro' ),
				'50' => esc_html__( '50%', 'DiviMegaPro' ),
				'25' => esc_html__( '25%', 'DiviMegaPro' )
			);
			
			
			if ( !isset( $enable_arrow[0] ) ) {
				
				$enable_arrow[0] = '0';
			}
			
			if ( !isset( $arrowtype ) ) {
				
				$arrowtype = 'sharp';
			}
			
			if ( $arrowtype == '' ) {
				
				$arrowtype = 'sharp';
			}
			
			if ( $arrowcolor == '' ) {
				
				$arrowcolor = '#333';
			}
			
			if ( $arrowwidth == '' ) {
				
				$arrowwidth = 10;
			}
			
			if ( $arrowheight == '' ) {
				
				$arrowheight = 10;
			}
			?>
			<div class="custom_meta_box">
				<p>
					<label for="dmp_megaprowidth">Mega Pro Width:</label>
					<select id="dmp_megaprowidth" name="dmp_megaprowidth" class="dmp_megaprowidth chosen" data-dropdownshowhideblock="1">
						<?php
						foreach ( $dmp_ats as $at_value => $at_name ) {
							printf( '<option value="%2$s"%3$s>%1$s</option>',
								esc_html( $at_name ),
								esc_attr( $at_value ),
								selected( $at_value, $dmp_megaprowidth, false )
							);
						} ?>
						<option value="custom"<?php if ( $dmp_megaprowidth == 'custom' ) { ?> selected="selected"<?php } ?> data-showhideblock=".mmw-container">Custom</option>
					</select>
				</p>
				<div class="mmw-container hide-container<?php if ( $dmp_megaprowidth == 'custom' ) { ?> do-show<?php } ?>">
					<input class="dmp_megaprowidth_custom" type="text" name="dmp_megaprowidth_custom" value="<?php echo $dmp_megaprowidth_custom; ?>"/>
				</div>
				<div class="clear"></div> 
			</div>
			<div class="custom_meta_box">
				<p>
					<input name="dmp_enable_arrow" type="checkbox" id="dmp_enablearrow" value="1" data-showhideblock=".enable_arrow" <?php checked( $enable_arrow[0], 1 ); ?> /> Enable Arrow
				</p>
				<div class="enable_arrow hide-container<?php if ( $enable_arrow[0] == 1 ) { ?> do-show<?php } ?>">
					<div class="custom_meta_box">
						<p>
							<label for="dmp_arrowfeature_type">Type:</label>
							<select id="dmp_arrowfeature_type" name="dmp_arrowfeature_type" class="dmp_arrowfeature-type chosen" data-dropdownshowhideblock="1">
								<option value="sharp"<?php if ( $arrowtype == 'sharp' ) { ?> selected="selected"<?php } ?> data-showhideblock=".dmp_arrowfeature-preview-sharp">Triangle</option>
								<option value="round"<?php if ( $arrowtype == 'round' ) { ?> selected="selected"<?php } ?> data-showhideblock=".dmp_arrowfeature-preview-round">Round</option>
							</select>
						</p>
						<div class="clear"></div> 
					</div>
					<div class="custom_meta_box">
						<p>
							<label>Color:</label>
							<input class="dmp_arrowfeature-color" type="text" name="dmp_arrowfeature_color" value="<?php echo $arrowcolor; ?>"/>
						</p>
						<div class="clear"></div> 
					</div> 
					<div class="custom_meta_box">
						<p>
							<label>Width:</label>
							<input class="dmp_arrowfeature-width" type="text" name="dmp_arrowfeature_width" value="<?php echo $arrowwidth; ?>" readonly="readonly" >
						</p>
						<div id="dmp_slider-arrowfeature-width" class="dmp_slider-bar"></div>
					</div>
					<div class="custom_meta_box">
						<p>
							<label>Height:</label>
							<input class="dmp_arrowfeature-height" type="text" name="dmp_arrowfeature_height" value="<?php echo $arrowheight; ?>" readonly="readonly" >
						</p>
						<div id="dmp_slider-arrowfeature-height" class="dmp_slider-bar"></div>
					</div>
					<div class="custom_meta_box">
						<p>
							<label>Preview:</label>
						</p>
						<div class="dmp_arrowfeature-preview">
							<div class="dmp_arrowfeature-preview-sharp hide-container<?php if ( $arrowtype == 'sharp' ) { ?> do-show<?php } ?>">
								<div class="tippy-arrow"></div>
							</div>
							<div class="dmp_arrowfeature-preview-round hide-container<?php if ( $arrowtype == 'round' ) { ?> do-show<?php } ?>">
								<svg viewBox="0 0 24 8" xmlns="http://www.w3.org/2000/svg">
									<path d="M1 8s4.577-.019 7.253-4.218c2.357-3.698 5.175-3.721 7.508 0C18.404 7.997 23 8 23 8H1z"></path>
								</svg>
							</div>
							<div class="dmp_arrowfeature-emptycontent"></div>
						</div>
					</div>
				</div>
				<div class="clear"></div> 
			</div>
			<?php
		}
		
		
		public static function divimegapros_closecustoms_callback( $post ) {
			
			wp_nonce_field( 'divimegapros_closecustoms', 'divimegapros_closecustoms_nonce' );
			
			$textcolor = get_post_meta( $post->ID, 'dmp_closebtn_text_color', true );
			$bgcolor = get_post_meta( $post->ID, 'dmp_closebtn_bg_color', true );
			$fontsize = get_post_meta( $post->ID, 'dmp_closebtn_fontsize', true );
			$borderradius = get_post_meta( $post->ID, 'dmp_closebtn_borderradius', true );
			$padding = get_post_meta( $post->ID, 'dmp_closebtn_padding', true );
			
			if ( $fontsize == '' ) {
				
				$fontsize = 25;
			}
			
			$dmp_enabledesktop = get_post_meta( $post->ID, 'dmp_enabledesktop' );
			if ( !isset( $dmp_enabledesktop[0] ) ) {
				
				$dmp_enabledesktop[0] = '0';
			}
			
			$dmp_enablemobile = get_post_meta( $post->ID, 'dmp_enablemobile' );
			if ( !isset( $dmp_enablemobile[0] ) ) {
				
				$dmp_enablemobile[0] = '0';
			}
			
			$customizeclosebtn = get_post_meta( $post->ID, 'dmp_customizeclosebtn' );
			if ( !isset( $customizeclosebtn[0] ) ) {
				
				$customizeclosebtn[0] = '0';
			}
			
			?>
			<div class="custom_meta_box">
				<p>
					<input name="dmp_enabledesktop" type="checkbox" id="dmp_enabledesktop" value="1" <?php checked( $dmp_enabledesktop[0], 1 ); ?> /> Enable on Desktop
				</p>
			</div>
			
			<div class="custom_meta_box">
				<p>
					<input name="dmp_enablemobile" type="checkbox" id="dmp_enablemobile" value="1" <?php checked( $dmp_enablemobile[0], 1 ); ?> /> Enable on Mobile
				</p>
			</div>
			
			<div class="custom_meta_box">
				<p>
					<input name="dmp_customizeclosebtn" type="checkbox" id="dmp_customizeclosebtn" value="1" data-showhideblock=".enable_customizations" <?php checked( $customizeclosebtn[0], 1 ); ?> /> Customize Close Button
				</p>
				<div class="enable_customizations<?php if ( $customizeclosebtn[0] == 1 ) { ?> do-show<?php } ?>">
					<div class="custom_meta_box">
						<p>
							<label class="label-color-field">Text color:</label>
							<input class="dmp_closebtn-text-color" type="text" name="dmp_closebtn_text_color" value="<?php echo $textcolor; ?>"/>
						</p>
						<div class="clear"></div> 
					</div> 
					<div class="custom_meta_box">
						<p>
							<label class="label-color-field">Background color:</label>
							<input class="dmp_closebtn-bg-color" type="text" name="dmp_closebtn_bg_color" value="<?php echo $bgcolor; ?>"/>
						</p>
						<div class="clear"></div> 
					</div>
					<div class="custom_meta_box">
						<p>
							<label>Font size:</label>
							<input class="dmp_closebtn_fontsize" type="text" name="dmp_closebtn_fontsize" value="<?php echo $fontsize; ?>" readonly="readonly" > px
						</p>
						<div id="dmp_slider-closebtn-fontsize" class="dmp_slider-bar"></div>
					</div>
					<div class="custom_meta_box">
						<p>
							<label>Border radius:</label>
							<input class="dmp_closebtn_borderradius" type="text" name="dmp_closebtn_borderradius" value="<?php echo $borderradius; ?>" readonly="readonly" > %
						</p>
						<div id="dmp_slider-closebtn-borderradius" class="dmp_slider-bar"></div>
					</div>
					<div class="custom_meta_box">
						<p>
							<label>Padding:</label>
							<input class="dmp_closebtn_padding" type="text" name="dmp_closebtn_padding" value="<?php echo $padding; ?>" readonly="readonly" > px
						</p>
						<div id="dmp_slider-closebtn-padding" class="dmp_slider-bar"></div>
					</div>
					<div class="custom_meta_box">
						<p>
							<label>Preview:</label>
						</p>
						<button type="button" class="divimegapro-customclose-btn"><span>&times;</span></button>
					</div>
				</div>
				<div class="clear"></div> 
			</div>
			<?php
		}
		
		
		public static function divimegapros_moresettings_callback( $post ) {
		
			wp_nonce_field( 'divimegapros_moresettings', 'divimegapros_moresettings_nonce' );
			
			$dmp_triggertype = get_post_meta( $post->ID, 'dmp_triggertype', true );
			$dmp_exittype = get_post_meta( $post->ID, 'dmp_exittype', true );
			$dmp_exitdelay = get_post_meta( $post->ID, 'dmp_exitdelay', true );
			$dmp_css_selector = get_post_meta( $post->ID, 'dmp_css_selector', true );
			
			$divimegapro_placement = get_post_meta( $post->ID, 'dmp_post_placement', true );
			
			if ( $dmp_triggertype == '' ) {
				
				$dmp_triggertype = 'hover';
			}
			
			if ( $dmp_exittype == '' ) {
				
				$dmp_exittype = 'hover';
			}
			?>
			<div class="custom_meta_box">
				<p class="dmp_triggertype et_pb_single_title">
					<label for="dmp_triggertype">Trigger Type:</label>
					<select id="dmp_triggertype" name="dmp_triggertype" class="dmp_triggertype chosen">
						<option value="hover"<?php if ( $dmp_triggertype == 'hover' ) { ?> selected="selected"<?php } ?>>Hover</option>
						<option value="click"<?php if ( $dmp_triggertype == 'click' ) { ?> selected="selected"<?php } ?>>Click</option>
					</select>
				</p>
				<p class="dmp_exittype et_pb_single_title">
					<label for="dmp_exittype">Exit Type:</label>
					<select id="dmp_exittype" name="dmp_exittype" class="dmp_exittype chosen" data-dropdownshowhideblock="1">
						<option value="hover"<?php if ( $dmp_exittype == 'hover' ) { ?> selected="selected"<?php } ?> data-showhideblock=".ed-container">Hover</option>
						<option value="click"<?php if ( $dmp_exittype == 'click' ) { ?> selected="selected"<?php } ?>>Click</option>
					</select>
				</p>
				<div class="ed-container hide-container<?php if ( $dmp_exittype == 'hover' ) { ?> do-show<?php } ?>">
					<label>Exit Delay:</label>
					<input class="dmp_exitdelay" type="text" name="dmp_exitdelay" value="<?php echo $dmp_exitdelay; ?>"/>
				</div>
			</div>
			<?php
		}
		
		
		public static function divimegapros_colorbox_callback( $post ) {
			
			wp_nonce_field( 'divimegapros_color_box', 'divimegapros_color_box_nonce' );
			$color = get_post_meta( $post->ID, 'dmp_bg_color', true );
			$fontcolor = get_post_meta( $post->ID, 'dmp_font_color', true );
			?>
			<div class="custom_meta_box">
				<p>
					<label class="label-color-field">Select Background Color: </label>
					<input class="cs-wp-color-picker" type="text" name="dmp_bg_color" value="<?php echo $color; ?>"/>
				</p>
				<div class="clear"></div> 
			</div> 
			<div class="custom_meta_box">
				<p>
					<label class="label-color-field">Select Font Color: </label>
					<input class="color-field" type="text" name="dmp_font_color" value="<?php echo $fontcolor; ?>"/>
				</p>
				<div class="clear"></div> 
			</div> 
			<script>
				(function( $ ) {
					// Add Color Picker to all inputs that have 'color-field' class
					$(function() {
						$('.color-field').wpColorPicker();
					});
				})( jQuery );
			</script>
			<?php
		}
		
		
		public static function save_post( $post_id, $post ) {
			
			global $pagenow;
			
			if ( 'post.php' != $pagenow ) return $post_id;
			
			if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
				return $post_id;
			
			$post_type = get_post_type_object( $post->post_type );
			if ( ! current_user_can( $post_type->cap->edit_post, $post_id ) )
				return $post_id;
			
			$post_value = '';
			
			
			/* Display Locations */
			if ( isset( $_POST['dmp_css_selector_at_pages'] ) ) {
				
				$post_at_pages = sanitize_text_field( $_POST['dmp_css_selector_at_pages'] );
				update_post_meta( $post_id, 'dmp_css_selector_at_pages', $post_at_pages );
			}
			
			if ( $post_at_pages == 'specific' ) {
				
				if ( isset( $_POST['dmp_css_selector_at_pages_selected'] ) ) {
					update_post_meta( $post_id, 'dmp_css_selector_at_pages_selected', $_POST['dmp_css_selector_at_pages_selected'] );
				}
			}
			else {
				
				update_post_meta( $post_id, 'dmp_css_selector_at_pages_selected', '' );
			}
			
			if ( isset( $_POST['dmp_css_selector_at_pagesexception_selected'] ) ) {
			
				update_post_meta( $post_id, 'dmp_css_selector_at_pagesexception_selected', $_POST['dmp_css_selector_at_pagesexception_selected'] );
			}
			
			
			/* Mega Pro Animation */
			if ( isset( $_POST['dmp_animation'] ) ) {
				
				$post_value = sanitize_text_field( $_POST['dmp_animation'] );
				update_post_meta( $post_id, 'dmp_animation', $post_value );
			}
			
			
			/* Mega Pro Triggers */
			if ( isset( $_POST['dmp_css_selector'] ) ) {
				update_post_meta( $post_id, 'dmp_css_selector', sanitize_text_field( $_POST['dmp_css_selector'] ) );
			}
			
			
			/* Mega Pro Display Settings */
			if ( isset( $_POST['dmp_placement'] ) ) {
				
				$post_value = sanitize_text_field( $_POST['dmp_placement'] );
				update_post_meta( $post_id, 'dmp_placement', $post_value );
			}
			
			if ( isset( $_POST['dmp_margintopbottom'] ) ) {
				update_post_meta( $post_id, 'dmp_margintopbottom', sanitize_text_field( $_POST['dmp_margintopbottom'] ) );
			}
			
			if ( isset( $_POST['dmp_megaprowidth'] ) ) {
				
				$post_value = sanitize_text_field( $_POST['dmp_megaprowidth'] );
				update_post_meta( $post_id, 'dmp_megaprowidth', $post_value );
			}
			
			if ( isset( $_POST['dmp_megaprowidth_custom'] ) ) {
				update_post_meta( $post_id, 'dmp_megaprowidth_custom', sanitize_text_field( $_POST['dmp_megaprowidth_custom'] ) );
			}
			
			
			/* Close Button Customizations */
			if ( isset( $_POST['dmp_enabledesktop'] ) ) {
				
				$dmp_enabledesktop = 1;
				
			} else {
				
				$dmp_enabledesktop = 0;
			}
			update_post_meta( $post_id, 'dmp_enabledesktop', $dmp_enabledesktop );
			
			if ( isset( $_POST['dmp_enablemobile'] ) ) {
				
				$dmp_enablemobile = 1;
				
			} else {
				
				$dmp_enablemobile = 0;
			}
			update_post_meta( $post_id, 'dmp_enablemobile', $dmp_enablemobile );
			
			if ( isset( $_POST['dmp_customizeclosebtn'] ) ) {
				
				$dmp_customizeclosebtn = 1;
				
			} else {
				
				$dmp_customizeclosebtn = 0;
			}
			update_post_meta( $post_id, 'dmp_customizeclosebtn', $dmp_customizeclosebtn );
			
			if ( isset( $_POST['dmp_closebtn_text_color'] ) ) {
				update_post_meta( $post_id, 'dmp_closebtn_text_color', sanitize_text_field( $_POST['dmp_closebtn_text_color'] ) );
			}
			
			if ( isset( $_POST['dmp_closebtn_bg_color'] ) ) {
				update_post_meta( $post_id, 'dmp_closebtn_bg_color', sanitize_text_field( $_POST['dmp_closebtn_bg_color'] ) );
			}
			
			if ( isset( $_POST['dmp_closebtn_fontsize'] ) ) {
				update_post_meta( $post_id, 'dmp_closebtn_fontsize', sanitize_text_field( $_POST['dmp_closebtn_fontsize'] ) );
			}
			
			if ( isset( $_POST['dmp_closebtn_borderradius'] ) ) {
				update_post_meta( $post_id, 'dmp_closebtn_borderradius', sanitize_text_field( $_POST['dmp_closebtn_borderradius'] ) );
			}
			
			if ( isset( $_POST['dmp_closebtn_padding'] ) ) {
				update_post_meta( $post_id, 'dmp_closebtn_padding', sanitize_text_field( $_POST['dmp_closebtn_padding'] ) );
			}
			
			
			/* Mega Pro Arrow Feature */
			if ( isset( $_POST['dmp_enable_arrow'] ) ) {
				
				$dmp_enable_arrow = 1;
				
			} else {
				
				$dmp_enable_arrow = 0;
			}
			update_post_meta( $post_id, 'dmp_enable_arrow', $dmp_enable_arrow );
			
			if ( isset( $_POST['dmp_arrowfeature_type'] ) ) {
				update_post_meta( $post_id, 'dmp_arrowfeature_type', sanitize_text_field( $_POST['dmp_arrowfeature_type'] ) );
			}
			
			if ( isset( $_POST['dmp_arrowfeature_color'] ) ) {
				update_post_meta( $post_id, 'dmp_arrowfeature_color', sanitize_text_field( $_POST['dmp_arrowfeature_color'] ) );
			}
			
			if ( isset( $_POST['dmp_arrowfeature_width'] ) ) {
				update_post_meta( $post_id, 'dmp_arrowfeature_width', sanitize_text_field( $_POST['dmp_arrowfeature_width'] ) );
			}
			
			if ( isset( $_POST['dmp_arrowfeature_height'] ) ) {
				update_post_meta( $post_id, 'dmp_arrowfeature_height', sanitize_text_field( $_POST['dmp_arrowfeature_height'] ) );
			}
			
			
			/* Mega Pro Additional Settings */
			if ( isset( $_POST['dmp_triggertype'] ) ) {
				
				$post_value = sanitize_text_field( $_POST['dmp_triggertype'] );
				update_post_meta( $post_id, 'dmp_triggertype', $post_value );
			}
			
			if ( isset( $_POST['dmp_exittype'] ) ) {
				
				$post_value = sanitize_text_field( $_POST['dmp_exittype'] );
				update_post_meta( $post_id, 'dmp_exittype', $post_value );
			}
			
			if ( isset( $_POST['dmp_exitdelay'] ) ) {
				update_post_meta( $post_id, 'dmp_exitdelay', sanitize_text_field( $_POST['dmp_exitdelay'] ) );
			}
			
			
			/* Mega Pro Background */
			if ( isset( $_POST['dmp_bg_color'] ) ) {
				update_post_meta( $post_id, 'dmp_bg_color', sanitize_text_field( $_POST['dmp_bg_color'] ) );
			}
			if ( isset( $_POST['dmp_font_color'] ) ) {
				update_post_meta( $post_id, 'dmp_font_color', sanitize_text_field( $_POST['dmp_font_color'] ) );
			}
		}
		
		
		public static function add_admin_submenu() {
			
			// Admin page
			add_submenu_page( 'edit.php?post_type=divi_mega_pro', 'Divi Mega Pro', 'Settings', 'edit_posts', 'divimegapro-settings', array( 'DiviMegaPro_Admin_Controller', 'admin_settings' ) );
		}
		
		
		public static function admin_settings() {
			
			self::display_configuration_page();
		}
		
		
		public static function display_configuration_page() {
			
			DiviMegaPro_Admin::$options = get_option( 'dmp_settings' );
			
			$license = get_option( 'divilife_edd_divimegapro_license_key' );
			$status  = get_option( 'divilife_edd_divimegapro_license_status' );
			$check_license = divilife_edd_divimegapro_check_license( TRUE );
			
			if ( isset( $check_license->expires ) ) {
				
				$license_expires = strtotime( $check_license->expires );
				$now = strtotime('now');
				$timeleft = $license_expires - $now;
				
				$daysleft = round( ( ( $timeleft / 24 ) / 60 ) / 60 );
				if ( $daysleft > 0 ) {
					
					$daysleft = '( ' . ( ( $daysleft > 1 ) ? $daysleft . ' days left' : $daysleft . ' day left' ) . ' )';
					
				} else {
					
					$daysleft = '';
				}
			}
			?>
			<div class="wrap">
				<h2><?php _e('Divi Mega Pro - Settings'); ?></h2>
				<form method="post" action="options.php">

					<?php settings_fields('divilife_edd_divimegapro_license'); ?>

					<table class="form-table">
						<tbody>
							<tr valign="top">
								<th scope="row" valign="top">
									<?php _e('License Key'); ?>
								</th>
								<td>
									<input id="divilife_edd_divimegapro_license_key" name="divilife_edd_divimegapro_license_key" type="password" class="regular-text" value="<?php esc_attr_e( $license ); ?>" />
									<label class="description" for="divilife_edd_divimegapro_license_key"><?php _e('Enter your license key'); ?></label>
								</td>
							</tr>
							<?php if ( FALSE !== $license ) { ?>
							
								<tr valign="top">
									<th scope="row" valign="top">
										<?php _e('License Status'); ?>
									</th>
									<td>
										<?php if ( $status === false ) {
											wp_nonce_field( 'divilife_edd_divimegapro_nonce', 'divilife_edd_divimegapro_nonce' ); ?>
											<input type="submit" class="button-secondary" name="divilife_edd_divimegapro_license_activate" value="<?php _e('Activate License'); ?>"/>
										<?php } ?>
										
										<h3 class="dl-edm-heading-msg no-margin">
											<?php 
												if ( $status !== false && $check_license->license == 'valid' ) {
													
													print '<span class="dashicons dashicons-yes dashicons-success dashicons-large"></span><br><br>';
												}
												else {
													
													if ( $check_license->license == 'expired' ) {
													
														print '<span class="dashicons dashicons-no-alt dashicons-fail dashicons-large"></span>';
														print '&nbsp;&nbsp;<span class="small-text">( Expired on ' . date( 'F d, Y', strtotime( $check_license->expires ) ) . ' )</span>';
													}
													
													if ( $check_license->license == NULL && $status !== false ) {
														
														print '<span class="dashicons dashicons-no-alt dashicons-fail dashicons-large"></span>';
														print '&nbsp;&nbsp;<span class="small-text">( Cannot retrieve license status from Divi Life server. Please contact Divi Life support. )</span>';
													}
												}
											?>
										</h3>
										<br>
										
										<?php if ( $status !== false && $check_license->license == 'valid' ) { ?>
											<?php wp_nonce_field( 'divilife_edd_divimegapro_nonce', 'divilife_edd_divimegapro_nonce' ); ?>
											<input type="submit" class="button-secondary" name="divilife_edd_divimegapro_license_deactivate" value="<?php _e('Deactivate License'); ?>"/>
										<?php } ?>
									</td>
								</tr>
								
								<?php
								
								if ( $status !== false ) { 
									
									if ( $daysleft != '' && $check_license->license == 'valid' ) { ?>
									<tr valign="top">
										<th scope="row" valign="top">
											<?php _e('License Expires on'); ?>
										</th>
										<td>
											<h4 class="no-margin">
												<?php print date( 'F d, Y', strtotime( $check_license->expires ) ); ?>
												<?php print $daysleft; ?>
											</h4>
										</td>
									<?php
									}
								}
								?>
								
							<?php } ?>
						</tbody>
					</table>
					<?php submit_button(); ?>

				</form>
			<?php
		
		}
		
	} // end DiviMegaPro_Admin_Controller