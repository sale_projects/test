<?php

	class DiviMegaPro_Controller extends DiviMegaPro {
		
		protected static $_show_errors = FALSE;
		
		public function __construct() {
			
		}
		
		public static function showDiviMegaPro( $params ) {
			
			// Settings
			self::$helper = new DiviMegaPro_Helper;
			
			try {
				
				print '<div id="divimegapro-template"></div>';
				print '<div class="divimegapro-wrapper">';
				
				
				/* Search CSS Triggers in all Divi divimegapros */
				$divimegapros_with_css_trigger = array();
				
				$posts = DiviMegaPro::$divimegaproList['css_trigger'];
				
				if ( !empty( $posts ) ) {
					
					print '<script type="text/javascript">var divimegapros_with_css_trigger = {';
					
					foreach( $posts as $post_id => $css_selector ) {
						
						print '\'' . $post_id . '\': \'' . $css_selector . '\',';
					}
					
					print '};</script>';
				}
				
				
				/* Search Divi divimegapros with Custom Close Buttons */
				$posts = DiviMegaPro_Model::getDiviMegaPros('customizeclosebtn');
				if ( isset( $posts[0] ) ) {
					
					print '<style type="text/css">';
					
					foreach( $posts as $dmm_post ) {
						
						$post_id = $dmm_post->ID;
						
						$cbc_textcolor = get_post_meta( $post_id, 'dmp_closebtn_text_color', true );
						$cbc_bgcolor = get_post_meta( $post_id, 'dmp_closebtn_bg_color', true );
						$cbc_fontsize = get_post_meta( $post_id, 'dmp_closebtn_fontsize', true );
						$cbc_borderradius = get_post_meta( $post_id, 'dmp_closebtn_borderradius', true );
						$cbc_padding = get_post_meta( $post_id, 'dmp_closebtn_padding', true );
						
						$customizeclosebtn = get_post_meta( $post_id, 'dmp_customizeclosebtn' );
						if ( isset( $customizeclosebtn[0] ) ) {
							
							$customizeclosebtn = $customizeclosebtn[0];
							
						} else {
							
							continue;
						}
						
						if ( $customizeclosebtn ) {
							
							print '
							.divimegapro-customclose-btn-' . $post_id . ' {
								top:5px !important;
								color:' . $cbc_textcolor . ' !important;
								background-color:' . $cbc_bgcolor . ' !important;
								font-size:' . $cbc_fontsize . 'px !important;
								padding:' . $cbc_padding . 'px !important;
								-moz-border-radius:' . $cbc_borderradius . '% !important;
								-webkit-border-radius:' . $cbc_borderradius . '% !important;
								-khtml-border-radius:' . $cbc_borderradius . '% !important;
								border-radius:' . $cbc_borderradius . '% !important;
							}
							';
						}
					}
					
					print '</style>';
				}
				
				
				/* Search Divi divimegapros with Arrow Features */
				$posts = DiviMegaPro_Model::getDiviMegaPros('enable_arrow');
				if ( isset( $posts[0] ) ) {
					
					print '<style type="text/css">';
					
					foreach( $posts as $dmm_post ) {
						
						$post_id = $dmm_post->ID;
						
						$dmp_arrowfeature_color = get_post_meta( $post_id, 'dmp_arrowfeature_color', true );
						
						$dmp_enable_arrow = get_post_meta( $post_id, 'dmp_enable_arrow' );
						if ( isset( $dmp_enable_arrow[0] ) ) {
							
							$dmp_enable_arrow = $dmp_enable_arrow[0];
							
						} else {
							
							continue;
						}
						
						if ( $dmp_enable_arrow ) {
							
							print '
							[data-template-id="#divimegapro-container-' . $post_id . '"] .tippy-roundarrow {
								fill:' . $dmp_arrowfeature_color . ' !important;
							}
							.tippy-popper[x-placement^=top] [data-template-id="#divimegapro-container-' . $post_id . '"] .tippy-arrow {
								border-top-color:' . $dmp_arrowfeature_color . ' !important;
							}
							.tippy-popper[x-placement^=bottom] [data-template-id="#divimegapro-container-' . $post_id . '"] .tippy-arrow {
								border-bottom-color:' . $dmp_arrowfeature_color . ' !important;
							}
							.tippy-popper[x-placement^=left] [data-template-id="#divimegapro-container-' . $post_id . '"] .tippy-arrow {
								border-left-color:' . $dmp_arrowfeature_color . ' !important;
							}
							.tippy-popper[x-placement^=right] [data-template-id="#divimegapro-container-' . $post_id . '"] .tippy-arrow {
								border-right-color:' . $dmp_arrowfeature_color . ' !important;
							}
							';
						}
					}
					
					print '</style>';
				}
				
				$divimegapros = DiviMegaPro::$divimegaproList['ids'];
				if ( is_array( $divimegapros ) && count( $divimegapros ) > 0 ) {
					
					global $post;
					
					$display_in_current = false;
					
					$current_post_id = (int) get_option( 'page_on_front' );
					
					$is_home = is_home();
					
					if ( $current_post_id == 0 && !$is_home ) {
						
						$current_post_id = get_the_ID();
					}
					
					foreach( $divimegapros as $divimegapro_id => $idx ) {
						
						if ( get_post_status ( $divimegapro_id ) == 'publish' ) {
						
							$at_pages = get_post_meta( $divimegapro_id, 'dmp_css_selector_at_pages' );
							
							$display_in_posts = ( !isset( $at_pages[0] ) ) ? 'all' : $at_pages[0];
							
							if ( $display_in_posts == 'specific' ) {
								
								$display_in_current = false;
								
								$in_posts = get_post_meta( $divimegapro_id, 'dmp_css_selector_at_pages_selected' );
								
								if ( isset( $in_posts[0] ) ) {
								
									foreach( $in_posts[0] as $in_post => $the_id ) {
										
										if ( $the_id == $current_post_id ) {
											
											$display_in_current = true;
											
											break;
										}
									}
								}
							}
							
							if ( $display_in_posts == 'all' ) {
								
								$display_in_current = true;
								
								$except_in_posts = get_post_meta( $divimegapro_id, 'dmp_css_selector_at_pagesexception_selected' );
								
								if ( isset( $except_in_posts[0] ) ) {
									
									foreach( $except_in_posts[0] as $in_post => $the_id ) {
										
										if ( $the_id == $current_post_id ) {
											
											$display_in_current = false;
											
											break;
										}
									}
								}
							}
							
							if ( $display_in_current ) {
							
								print self::divimegapro_render( $divimegapro_id );
							}
						}
					}
				}
				
				print '</div>';
				
				?>
				<script type="text/javascript">
				var ajaxurl = "<?php echo admin_url('admin-ajax.php'); ?>";
				var diviAjaxUrl = '<?php print plugins_url( 'ajax-handler-wp.php' , __FILE__ ); ?>';
				</script>
				<?php
			
			} catch (Exception $e) {
			
				self::log( $e );
			}
			
			
			wp_register_script('exit-intent', DIVI_MEGA_PRO_PLUGIN_URL . 'assets/js/jquery.exitintent.min.js', array('jquery') );
			
			wp_enqueue_script('exit-intent');
		}
		
		
		public static function divimegapro_render( $divimegapro_id = NULL ) {
			
			if ( !is_numeric( $divimegapro_id ) ) {
				
				throw new InvalidArgumentException( 'divimegapro_render > $divimegapro_id is not numeric');
			}
			
			$divimegapro_id = (int) $divimegapro_id;
			
			$post_data = get_post( $divimegapro_id );
			
			
			$divimegapro_effect = get_post_meta( $post_data->ID,'_et_pb_divimegapro_effect', true );
			
			if ( $divimegapro_effect == '' ) {
				
				$divimegapro_effect = 'divimegapro-hugeinc';
			}
			
			global $wp_embed;
			
			$wp_embed->post_ID = $post_data->ID;
			
			// [embed] shortcode
			$wp_embed->run_shortcode( $post_data->post_content );
			
			// plain links on their own line
			$wp_embed->autoembed( $post_data->post_content );
			
			// Enable shortcodes
			$output = do_shortcode( $post_data->post_content );
			
			$output = self::$helper->formatContent( $output );
			
			$data_path_to = null;
			$svg = null;
			
			if ( $divimegapro_effect == 'divimegapro-cornershape' ) {
				
				$data_path_to = 'data-path-to = "m 0,0 1439.999975,0 0,805.99999 -1439.999975,0 z"';
				$svg = '<svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 1440 806" preserveAspectRatio="none">
						<path class="divimegapro-path" d="m 0,0 1439.999975,0 0,805.99999 0,-805.99999 z"/>
					</svg>';
			}
			if ( $divimegapro_effect == 'divimegapro-boxes' ) {
				
				$svg = '<svg xmlns="http://www.w3.org/2000/svg" width="100%" height="101%" viewBox="0 0 1440 806" preserveAspectRatio="none">
						<path d="m0.005959,200.364029l207.551124,0l0,204.342453l-207.551124,0l0,-204.342453z"/>
						<path d="m0.005959,400.45401l207.551124,0l0,204.342499l-207.551124,0l0,-204.342499z"/>
						<path d="m0.005959,600.544067l207.551124,0l0,204.342468l-207.551124,0l0,-204.342468z"/>
						<path d="m205.752151,-0.36l207.551163,0l0,204.342437l-207.551163,0l0,-204.342437z"/>
						<path d="m204.744629,200.364029l207.551147,0l0,204.342453l-207.551147,0l0,-204.342453z"/>
						<path d="m204.744629,400.45401l207.551147,0l0,204.342499l-207.551147,0l0,-204.342499z"/>
						<path d="m204.744629,600.544067l207.551147,0l0,204.342468l-207.551147,0l0,-204.342468z"/>
						<path d="m410.416046,-0.36l207.551117,0l0,204.342437l-207.551117,0l0,-204.342437z"/>
						<path d="m410.416046,200.364029l207.551117,0l0,204.342453l-207.551117,0l0,-204.342453z"/>
						<path d="m410.416046,400.45401l207.551117,0l0,204.342499l-207.551117,0l0,-204.342499z"/>
						<path d="m410.416046,600.544067l207.551117,0l0,204.342468l-207.551117,0l0,-204.342468z"/>
						<path d="m616.087402,-0.36l207.551086,0l0,204.342437l-207.551086,0l0,-204.342437z"/>
						<path d="m616.087402,200.364029l207.551086,0l0,204.342453l-207.551086,0l0,-204.342453z"/>
						<path d="m616.087402,400.45401l207.551086,0l0,204.342499l-207.551086,0l0,-204.342499z"/>
						<path d="m616.087402,600.544067l207.551086,0l0,204.342468l-207.551086,0l0,-204.342468z"/>
						<path d="m821.748718,-0.36l207.550964,0l0,204.342437l-207.550964,0l0,-204.342437z"/>
						<path d="m821.748718,200.364029l207.550964,0l0,204.342453l-207.550964,0l0,-204.342453z"/>
						<path d="m821.748718,400.45401l207.550964,0l0,204.342499l-207.550964,0l0,-204.342499z"/>
						<path d="m821.748718,600.544067l207.550964,0l0,204.342468l-207.550964,0l0,-204.342468z"/>
						<path d="m1027.203979,-0.36l207.550903,0l0,204.342437l-207.550903,0l0,-204.342437z"/>
						<path d="m1027.203979,200.364029l207.550903,0l0,204.342453l-207.550903,0l0,-204.342453z"/>
						<path d="m1027.203979,400.45401l207.550903,0l0,204.342499l-207.550903,0l0,-204.342499z"/>
						<path d="m1027.203979,600.544067l207.550903,0l0,204.342468l-207.550903,0l0,-204.342468z"/>
						<path d="m1232.659302,-0.36l207.551147,0l0,204.342437l-207.551147,0l0,-204.342437z"/>
						<path d="m1232.659302,200.364029l207.551147,0l0,204.342453l-207.551147,0l0,-204.342453z"/>
						<path d="m1232.659302,400.45401l207.551147,0l0,204.342499l-207.551147,0l0,-204.342499z"/>
						<path d="m1232.659302,600.544067l207.551147,0l0,204.342468l-207.551147,0l0,-204.342468z"/>
						<path d="m-0.791443,-0.360001l207.551163,0l0,204.342438l-207.551163,0l0,-204.342438z"/>
					</svg>';
			}
			
			if ( $divimegapro_effect == 'divimegapro-genie' ) {
				
				$data_path_to = 'data-steps = "m 701.56545,809.01175 35.16718,0 0,19.68384 -35.16718,0 z;m 698.9986,728.03569 41.23353,0 -3.41953,77.8735 -34.98557,0 z;m 687.08153,513.78234 53.1506,0 C 738.0505,683.9161 737.86917,503.34193 737.27015,806 l -35.90067,0 c -7.82727,-276.34892 -2.06916,-72.79261 -14.28795,-292.21766 z;m 403.87105,257.94772 566.31246,2.93091 C 923.38284,513.78233 738.73561,372.23931 737.27015,806 l -35.90067,0 C 701.32034,404.49318 455.17312,480.07689 403.87105,257.94772 z;M 51.871052,165.94772 1362.1835,168.87863 C 1171.3828,653.78233 738.73561,372.23931 737.27015,806 l -35.90067,0 C 701.32034,404.49318 31.173122,513.78234 51.871052,165.94772 z;m 52,26 1364,4 c -12.8007,666.9037 -273.2644,483.78234 -322.7299,776 l -633.90062,0 C 359.32034,432.49318 -6.6979288,733.83462 52,26 z;m 0,0 1439.999975,0 0,805.99999 -1439.999975,0 z"';
				$svg = '<svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 1440 806" preserveAspectRatio="none">
						<path class="divimegapro-path" d="m 701.56545,809.01175 35.16718,0 0,19.68384 -35.16718,0 z"/>
					</svg>';
			}
			
			$is_mobile = self::$isMobileDevice;
			
			if ( !$is_mobile ) {
				
				$is_mobile = 0;
			}
			
			
			/* Close Button Customizations */
			$dmp_enabledesktop = get_post_meta( $post_data->ID, 'dmp_enabledesktop', true );
			if ( !isset( $dmp_enabledesktop ) ) {
				
				$dmp_enabledesktop = 0;
			}
			
			$dmp_enablemobile = get_post_meta( $post_data->ID, 'dmp_enablemobile', true );
			if ( !isset( $dmp_enablemobile ) ) {
				
				$dmp_enablemobile = 0;
			}
			
			$dmp_customizeclosebtn = get_post_meta( $post_data->ID, 'dmp_customizeclosebtn' );
			if( !isset( $dmp_customizeclosebtn[0] ) ) {
				
				$dmp_customizeclosebtn[0] = '0';
			}
			
			
			/* Arrow Feature */
			$dmp_enable_arrow = get_post_meta( $post_data->ID, 'dmp_enable_arrow', true );
			if ( !isset( $dmp_enable_arrow ) ) {
				
				$dmp_enable_arrow = 0;
			}
			
			$dmp_arrowfeature_type = get_post_meta( $post_data->ID, 'dmp_arrowfeature_type', true );
			if ( !isset( $dmp_arrowfeature_type ) ) {
				
				$dmp_arrowfeature_type = 0;
			}
			
			$dmp_arrowfeature_width = get_post_meta( $post_data->ID, 'dmp_arrowfeature_width', true );
			if ( !isset( $dmp_arrowfeature_width ) ) {
				
				$dmp_arrowfeature_width = 0;
			}
			
			$dmp_arrowfeature_height = get_post_meta( $post_data->ID, 'dmp_arrowfeature_height', true );
			if ( !isset( $dmp_arrowfeature_height ) ) {
				
				$dmp_arrowfeature_height = 0;
			}
			
			
			/* Mega Menu Settings */
			$dmp_animation = get_post_meta( $post_data->ID, 'dmp_animation', true );
			if ( !isset( $dmp_animation ) ) {
				
				$dmp_animation = 'shift-away';
			}
			
			$dmp_placement = get_post_meta( $post_data->ID, 'dmp_placement', true );
			if ( !isset( $dmp_placement ) ) {
				
				$dmp_placement = 'down';
			}
			
			$dmp_margintopbottom = get_post_meta( $post_data->ID, 'dmp_margintopbottom', true );
			if ( !isset( $dmp_margintopbottom ) ) {
				
				$dmp_margintopbottom = 0;
			}
			
			$dmp_megaprowidth = get_post_meta( $post_data->ID, 'dmp_megaprowidth', true );
			if ( !isset( $dmp_megaprowidth ) ) {
				
				$dmp_megaprowidth = '100';
			}
			
			$dmp_megaprowidth_custom = get_post_meta( $post_data->ID, 'dmp_megaprowidth_custom', true );
			if ( !isset( $dmp_megaprowidth_custom ) ) {
				
				$dmp_megaprowidth_custom = '100';
			}
			
			$dmp_triggertype = get_post_meta( $post_data->ID, 'dmp_triggertype', true );
			if ( !isset( $dmp_triggertype ) ) {
				
				$dmp_triggertype = 'hover';
			}
			
			$dmp_exittype = get_post_meta( $post_data->ID, 'dmp_exittype', true );
			if ( !isset( $dmp_exittype ) ) {
				
				$dmp_exittype = 'hover';
			}
			
			$dmp_exitdelay = get_post_meta( $post_data->ID, 'dmp_exitdelay', true );
			if ( !isset( $dmp_exitdelay ) ) {
				
				$dmp_exitdelay = 0;
			}
			
			$dmp_bg_color = get_post_meta( $post_data->ID, 'dmp_bg_color', true );
			$dmp_font_color = get_post_meta( $post_data->ID, 'dmp_font_color', true );
			
			$body = $output;
			
			require( DIVI_MEGA_PRO_PLUGIN_DIR . '/templates/divimegapro.php');
		}
		
		
		/**
		 * Log debugging infoormation to the error log.
		 *
		 * @param string $e The Exception object
		 */
		public static function log( $e = FALSE ) {
			
			$data_log = $e;
			
			if ( is_object( $e ) ) {
				
				$data_log = sprintf( "Exception: \n %s \n", $e->getMessage() . "\r\n\r\n" . $e->getFile() . "\r\n" . 'Line:' . $e->getLine() );
			}
			
			if ( apply_filters( 'divimegapros_log', defined( 'WP_DEBUG' ) && WP_DEBUG && defined( 'WP_DEBUG_LOG' ) && WP_DEBUG_LOG ) ) {
				error_log( print_r( compact( 'data_log' ), true ) );
			}
		}
		
	} // end DiviMegaPro_Controller