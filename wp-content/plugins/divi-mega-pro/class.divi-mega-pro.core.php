<?php

	class DiviMegaPro {
		
		private static $initiated = false;
		
		/**
		 * Holds an instance of DiviMegaPro Helper class
		 *
		 * @since 1.0
		 * @var DiviMegaPro_Helper
		 */
		public static $helper;
		
		protected static $divimegaproList;
		
		protected static $isMobileDevice = NULL;
		
		public static function init() {
			
			if ( ! self::$initiated ) {
				
				self::load_resources();
				
				self::$helper = new DiviMegaPro_Helper();
				
				self::init_hooks();
				
				// Register the Custom Divi Mega Pro Post Type
				self::register_cpt();
			}
		}
		
		/**
		 * Initializes WordPress hooks
		 */
		protected static function init_hooks() {
			
			self::$initiated = true;
			
			// Add custom column in post type
			add_filter( 'manage_edit-divi_mega_pro_columns', array( 'DiviMegaPro', 'setup_divimegapros_columns') ) ;
			add_action( 'manage_divi_mega_pro_posts_custom_column', array( 'DiviMegaPro', 'manage_divimegapros_columns' ), 10, 2 );
			
			// Add styles
			add_action( 'wp_print_styles', array( 'DiviMegaPro', 'add_styles') );
			
			// Add scripts
			add_action( 'wp_enqueue_scripts', array( 'DiviMegaPro', 'add_scripts') );
			
			// Ajax ready
			add_action( 'wp_head', array( 'DiviMegaPro', 'add_var_ajaxurl') );
			add_action( 'wp_ajax_ajax_divimegapros_callback', array( 'DiviMegaPro_Ajax', 'ajax_divimegapros_callback') );
			add_action( 'wp_ajax_nopriv_ajax_divimegapros_callback', array( 'DiviMegaPro_Ajax', 'ajax_divimegapros_callback') );
			
			// Register widget
			add_action( 'widgets_init', array( 'DiviMegaPro', 'register_widget') );
			
			add_action( 'wp_head', array( 'DiviMegaPro', 'prevent_playable_tags') );
			add_action( 'wp_head', array( 'DiviMegaPro', 'getAllDiviMegaPros') );
			
			// Divi Mega Pro call
			add_action( 'wp_footer', array( 'DiviMegaPro_Controller', 'showDiviMegaPro' ) );
		}
		
		
		public static function getAllDiviMegaPros() {
			
			$function_exception = 'showDiviMegaPro';
			
			$divimegapros = array();
			
			try {
				
				/* Search Divi divimegapro in current post */
				global $post;
				$post_content = $post->post_content;
				$matches = array();
				$pattern = '/id="(.*?divimegapro\-[0-9]+)"/';
				preg_match_all($pattern, $post_content, $matches);
				
				$divimegapros_divimegapro_ = $matches[1];
				
				$matches = array();
				$pattern = '/class="(.*?divimegapro\-[0-9]+)"/';
				preg_match_all($pattern, $post_content, $matches);
				
				$divimegapros_class_divimegapro = $matches[1];
				
				$divimegapros_in_post = $divimegapros_divimegapro_ + $divimegapros_class_divimegapro;
				
				$divimegapros_in_post = array_filter( array_map( 'DiviMegaPro_Helper::prepareMenu', $divimegapros_in_post ) );
				
				if ( is_array( $divimegapros_in_post ) && count( $divimegapros_in_post ) > 0 ) {
					
					$divimegapros_in_post = array_flip( $divimegapros_in_post );
				}
				
				
				/* Search Divi divimegapro in active menus */
				$theme_locations = get_nav_menu_locations();
				
				$divimegapros_in_menus = array();
				
				$divimegapros['divibars_in_menus'] = array();
				
				if ( is_array( $theme_locations ) && count( $theme_locations ) > 0 ) {
					
					$divimegapros_in_menus = array();
					
					foreach( $theme_locations as $theme_location => $theme_location_value ) {
						
						$menu = get_term( $theme_locations[$theme_location], 'nav_menu' );
						
						// menu exists?
						if( !is_wp_error($menu) ) {
							
							$menu_items = wp_get_nav_menu_items($menu->term_id);
							
							foreach ( (array) $menu_items as $key => $menu_item ) {
								
								$url = $menu_item->url;
								
								if ( $url ) {
									
									$extract_id = self::$helper->prepareMenu( $url );
									
									if ( $extract_id ) {
										
										$divimegapros_in_menus[ $extract_id ] = 1;
									}
								}
								
								/* Search Divi divimegapro in menu classes */
								if ( count( $menu_item->classes ) > 0 && $menu_item->classes[0] != '' ) {
									
									foreach ( $menu_item->classes as $key => $class ) {
										
										if ( $class != '' ) {
											
											$extract_id = self::$helper->prepareMenu( $class );
											
											if ( $extract_id ) {
											
												$divimegapros_in_menus[ $extract_id ] = 1;
											}
										}
									}
								}
								
								/* Search Divi divimegapro in Link Relationship (XFN) */
								if ( !empty( $menu_item->xfn ) ) {
									
									$extract_id = self::$helper->prepareMenu( $menu_item->xfn );
									
									if ( $extract_id ) {
									
										$divimegapros_in_menus[ $extract_id ] = 1;
									}
								}
							}
						}
						else {
							
							
						}
					}
				}
				$divimegapros_in_menus = array_filter( $divimegapros_in_menus );
				$divimegapros['divimegapros_in_menus'] = $divimegapros_in_menus;
				
				
				/* Search CSS Triggers in all Divi divimegapros */
				$divimegapros_with_css_trigger = array();
				
				$divimegapros['css_trigger'] = array();
				
				$posts = DiviMegaPro_Model::getDiviMegaPros('css_trigger');
				
				if ( isset( $posts[0] ) ) {
					
					foreach( $posts as $dmm_post ) {
						
						$post_id = $dmm_post->ID;
						
						$get_css_selector = get_post_meta( $post_id, 'dmp_css_selector' );
						
						$css_selector = $get_css_selector[0];
						
						if ( $css_selector != '' ) {
							
							$divimegapros_with_css_trigger[ $post_id ] = $css_selector;
						}
					}
					
					$divimegapros['css_trigger'] = $divimegapros_with_css_trigger;
				}
				
				
				$divimegapros['ids'] = $divimegapros_in_post + $divimegapros_in_menus + $divimegapros_with_css_trigger;
				
				if ( is_array( $divimegapros['ids'] ) && count( $divimegapros['ids'] ) > 0 ) {
					
					add_filter( 'body_class', function ( $classes )
					{
						$classes[] = 'divimegapro-active';
						return $classes;
						
					}, 20, 2 );
				}
			
			} catch (Exception $e) {
			
				self::log( $e );
			}
			
			self::$divimegaproList = $divimegapros;
		}
		
		
		protected static function load_resources() {
			
			require_once( DIVI_MEGA_PRO_PLUGIN_DIR . '/includes/class.divi-mega-pro.controller.php' );
			require_once( DIVI_MEGA_PRO_PLUGIN_DIR . '/includes/class.divi-mega-pro.model.php' );
			require_once( DIVI_MEGA_PRO_PLUGIN_DIR . '/includes/class.divi-mega-pro.helper.php' );
			require_once( DIVI_MEGA_PRO_PLUGIN_DIR . '/includes/class.divi-mega-pro.ajax.php' );
		}
		
		
		public static function register_cpt() {
			
			$labels = array(
				'name' => _x( 'Divi Mega Pro', 'divi_mega_pro' ),
				'singular_name' => _x( 'Divi Mega Pro', 'divi_mega_pro' ),
				'add_new' => _x( 'Add New', 'divi_mega_pro' ),
				'add_new_item' => _x( 'Add New Divi Mega Pro', 'divi_mega_pro' ),
				'edit_item' => _x( 'Edit Divi Mega Pro', 'divi_mega_pro' ),
				'new_item' => _x( 'New Divi Mega Pro', 'divi_mega_pro' ),
				'view_item' => _x( 'View Divi Mega Pro', 'divi_mega_pro' ),
				'search_items' => _x( 'Search Divi Mega Pro', 'divi_mega_pro' ),
				'not_found' => _x( 'No Divi Mega Pro found', 'divi_mega_pro' ),
				'not_found_in_trash' => _x( 'No Divi Mega Pro found in Trash', 'divi_mega_pro' ),
				'parent_item_colon' => _x( 'Parent Divi Mega Pro:', 'divi_mega_pro' ),
				'menu_name' => _x( 'Divi Mega Pro', 'divi_mega_pro' ),
			);
			
			$args = array(
				'labels' => $labels,
				'hierarchical' => true,
				'supports' => array( 'title', 'editor', 'author' ),
				'public' => true,
				'show_ui' => true,
				'show_in_menu' => true,
				'menu_position' => 5,
				'show_in_nav_menus' => true,
				'exclude_from_search' => true,
				'has_archive' => true,
				'query_var' => true,
				'can_export' => true,
				'rewrite' => true,
				'capability_type' => 'post'
			);
			
			register_post_type( 'divi_mega_pro', $args );
		}
		
		
		public static function setup_divimegapros_columns( $columns ) {

			$columns = array(
				'cb' => '<input type="checkbox" />',
				'title' => __( 'Title' ),
				'unique_identifier' => __( 'Unique Mega Pro Class' ),
				'author' => __( 'Author' ),
				'date' => __( 'Date' )
			);

			return $columns;
		}
		
		
		public static function manage_divimegapros_columns( $column, $post_id ) {
			
			global $post;
			
			switch( $column ) {
				
				case 'unique_identifier':
				
					$unique_class = 'divimegapro-' . $post->ID;
					
					print $unique_class;
					
					break;
					
				default:
				
					break;
			}
		}
		
		
		public static function register_widget() {
			
			register_sidebar( array(
				'name' => __( 'Divi Mega Pro - Global', 'theme-slug' ),
				'id' => 'divi-mega-pro_global_widget',
				'description' => __( '', 'theme-slug' )
			) );
		}
		
		
		// Register all needed stylesheets
		public static function add_styles() {
			
			wp_register_style('normalize_css', plugins_url('assets/css/normalize.css', __FILE__));
			wp_enqueue_style('normalize_css');
			
			wp_register_style('DiviMegaPro-main', DIVI_MEGA_PRO_PLUGIN_URL . 'assets/css/main.css' );
			wp_enqueue_style('DiviMegaPro-main');
		}
		
		
		// Register all needed scripts
		public static function add_scripts() {
			
			wp_enqueue_script('jquery');
			
			wp_register_script('DiviMegaPro-tippy', plugins_url('assets/js/tippy-2.5.3.all.min.js', __FILE__), array('jquery'));
			wp_enqueue_script('DiviMegaPro-tippy');
			
			wp_register_script('DiviMegaPro-anime', plugins_url('assets/js/anime.min.js', __FILE__), array('jquery'));
			wp_enqueue_script('DiviMegaPro-anime');
			
			wp_register_script('DiviMegaPro-main', DIVI_MEGA_PRO_PLUGIN_URL . 'assets/js/main.js', array('jquery'), DIVI_MEGA_PRO_VERSION, TRUE);
			wp_enqueue_script('DiviMegaPro-main');
			
			wp_register_script('DiviMegaPro-main-helper', DIVI_MEGA_PRO_PLUGIN_URL . 'assets/js/main.helper.js', array('jquery', 'DiviMegaPro-main'), DIVI_MEGA_PRO_VERSION, TRUE );
			wp_enqueue_script('DiviMegaPro-main-helper');
			
			wp_register_script('actual', plugins_url('assets/js/actual.min.js', __FILE__),array("jquery"));
			wp_enqueue_script('actual');
		}
		
		
		public static function add_var_ajaxurl() {
		?>
		<script type="text/javascript">
		var ajax_url = '<?php print admin_url('admin-ajax.php'); ?>';
		</script>
		<?php
		}
		
		
		public static function prevent_playable_tags() {
			?>
			<script type="text/javascript">
				function dmmTogglePlayableTags( divimegapro_id, wait ) {
				
					var $ = jQuery;
					
					if ( !divimegapro_id  ) {
						
						divimegapro_id = "";
					}
					
					if ( !wait  ) {
						
						wait = 1;
					}
					
					/* Prevent playable tags load content before divimegapro call */
					setTimeout(function() {
						
						$( divimegapro_id + ".divimegapro").find("iframe").not( '[id^="gform"]' ).each(function() { 
						
							var iframeParent = $(this).parent();
							var iframe = $(this).prop("outerHTML");
							var src = iframe.match(/src=[\'"]?((?:(?!\/>|>|"|\'|\s).)+)"/)[0];
							
							src = src.replace("src", "data-src");
							iframe = iframe.replace(/src=".*?"/i, "src=\"about:blank\" data-src=\"\"" );
							
							if ( src != "data-src=\"about:blank\"" ) {
								iframe = iframe.replace("data-src=\"\"", src );
							}
							
							$( iframe ).insertAfter( $(this) );
							
							$(this).remove();
						});
						
					}, wait);
					
					$( divimegapro_id + ".divimegapro").find("video").each(function() {
						$(this).get(0).pause();
					});
					
					$( divimegapro_id + ".divimegapro").find("audio").each(function() {
						
						this.pause();
						this.currentTime = 0;
					});
				}
				
				dmmTogglePlayableTags( '', 1000 );
			</script>
			<?php
		}
	}